$(function () {

    $("#accordion2").collapse();

    //$('#myPopup').modal('show');
    /*if (window.location.pathname != '/' && window.location.pathname != '/ru' && window.location.pathname != '/en') {
        var yetVisited = sessionStorage ? sessionStorage['visited'] : $.cookie('visited');

        if (!yetVisited) {
            $('#myPopup').modal('show');

            sessionStorage ? sessionStorage['visited'] = 'yes' : $.cookie('visited', 'yes');
        }

        var date = new Date();
        function track_user() {
            setInterval(function() {
                date.setTime(date.getTime()+(2*1000));
                document.cookie = "visited=yes; expires="+ date.toGMTString() + "; path=/";
            }, 1000);
        }
        track_user();
    }*/

// Адаптивные манипуляции
    var $Win = $(window).width();

    $('.wrap-lang').on('click', function(){
        $(this).toggleClass('open');
    });

    var flag = false;

    $('.btn-mobile').on('click', function(){
        if(flag == false) {
            $('#nav_main .js-menu').stop().slideDown("slow");
            flag = true;
        } else {
            $('#nav_main .js-menu').stop().slideUp("slow");
            flag = false;
        }
    });

    var langText = $('.block-lang .active a').text();
    $('.wrap-lang').prepend('<span>'+ langText +'</span>');

    // Для менюхи

    if ($Win < 999) {
            $('.js-menu').css('display', 'none').removeClass('menu');

            $('.js-menu>li').on('click', function(){
                
                var $this = $(this);

                $('.js-menu>li').removeClass('open');

                $this.addClass('open');

            });

        } else {

            $('.js-menu').css('display', 'block').addClass('menu');
            $('.js-menu>li').removeClass('open');

    }


    $(window).resize(function() {

        var $win = $(window).width();

        if ($win < 999) {
            $('.js-menu').css('display', 'none').removeClass('menu');

            $('.js-menu>li').on('click', function(){
                
                var $this = $(this);

                $('.js-menu>li').removeClass('open');

                $this.addClass('open');

            });

        } else {

            $('.js-menu').css('display', 'block').addClass('menu');
            $('.js-menu>li').removeClass('open');

        }

    });

// Конец
    


    var star = $('.star');
    star.raty({
        starOff: '/images/raty/star-off-big.png',
        starOn: '/images/raty/star-on-big.png',
        starHalf: '/images/raty/star-half-big.png',
        round: { down: .26, half: .6, up: .9 },
        width: 150,
        half: true,
        noRatedMsg: $('#advance_options').attr('data-no-rating-hint'),
        hints: ['', '', '', '', ''],
        readOnly: function () {
            return $(this).attr('data-read-only') == 1;
        },
        score: function () {
            return $(this).attr('data-score');
        }
    });


    $('#total_score').raty({
        starOff: '/images/raty/star-off-big.png',
        starOn: '/images/raty/star-on-big.png',
        starHalf: '/images/raty/star-half-big.png',
        round: { down: .26, half: .6, up: .9 },
        width: 150,
        half: true,
        noRatedMsg: $('#advance_options').attr('data-no-rating-hint'),
        hints: ['', '', '', '', ''],
        readOnly: true,
        score: function () {
            return $(this).attr('data-score');
        }
    });

    $('.star.view').on('click', function (e) {
        $.get($('#advance_options').attr('data-url'), {'score': $(this).raty('score'), 'type': $(this).attr('data-type'), 'target': $(this).attr('data-id')});
    });

    function checkWinSize() {
        if ($(window).width() < 1000) {
            $('a.link.size_link').show();
        } else {
            $('a.link.size_link').hide();
        }
    }

    $(window).resize(checkWinSize);
    checkWinSize();

    $.fn.smartColumns = function (_minW) {
        this.each(function () {
            var myBlock = $(this);
            var babyBlock = myBlock.find(".column");
            var minW = _minW || 180;
            myBlock.css({ 'width': "100%"});
            var colWrap = myBlock.width();
            var colNum = babyBlock.length;
            var colFloat = Math.floor(colWrap / colNum);
            var colNum2 = Math.floor(colWrap / minW);
            var colFixed = Math.floor(colWrap / colNum2);
            myBlock.css({ 'width': colWrap});
            if (colFloat > minW) {
                babyBlock.css({ 'width': colFloat});
            }
            else {
                babyBlock.css({ 'width': colFixed});
            }
        });
    };

    $('a[data-toggle="tab"]').on('shown', function () {
       resizeImg();
        recalcColumns();
        $('.img-circle img').each(resizeImg);

    });


    var resizeImg = function () {
        var img = $(this);
        if (img.attr('src') == undefined) return;
        if (img.data('changed')) {
            return;
        }
        var imgW = img.width();
        var imgH = img.height();
        var block = img.parents('.img-circle');
       var img = block.find('img');
        var blockW = block.width();
        var blockH = block.height();
        if (!img.is(':visible')) {
            var c = new Image();

            c.onload = function () {
                imgW = c.width;
                imgH = c.height;
                if (blockW > 0) {
                    changeAttr()
                }
            }

            c.src = img.attr('src');
        } else {
            if (blockW > 0) {
                changeAttr();
            }
        }


        function changeAttr() {
            if (imgW > imgH) {
                img.height(blockH);
                var newW = blockH / imgH * imgW;
                img.css('margin-left', (blockW - newW) / 2);
            }
            else {
                img.width(blockW);
                var newH = blockW / imgW * imgH;
                img.css('margin-top', (blockH - newH) / 2);
            }
            img.data('changed', true);
        }


    };
    $('.img-circle img').one('load', resizeImg);

    $('.img-circle img').each(function () {
        if (this.complete) {
            $(this).load();
        }
    });


    function recalcColumns() {
        // $(".list_federation").smartColumns();
        /*$(".footer_list_link").smartColumns();*/
        $(".coach_list").smartColumns(100);
        // $(".column_partner").smartColumns(200);
        $(".nav_column.diet_menu_list").smartColumns(210);
        $(".teams_list").smartColumns(265);
        $(".top_week .coach_list").smartColumns(140);
        $(".photo_list_result").smartColumns(240);
    }

    $(window).resize(recalcColumns);

    recalcColumns();

    $('.auto_submit').on('change', function () {
        $(this).parents('form').submit();
    });


    /*  Input placeholder*/
    $('.input_placeholder').each(function () {
        if ($(this).val() != '')    $(this).prev().addClass('hide');
    });
    $('.input_placeholder').blur(function () {
        if ($(this).val() == '') $(this).prev().removeClass('hide');
    });
    $('.input_placeholder').focus(function () {
        $(this).prev().addClass('hide');
    });
    $('.input_placeholder').mouseover(function () {
        if ($(this).val() != '') $(this).prev().addClass('hide');
    });
    /*   end   Input placeholder*/


    $('.custom_select').customSelect();


    window.equalHeight = function setEqualHeight(group) {
        var tallest = 0;
        group.each(function () {
            var thisHeight = $(this).height();
            if (thisHeight > tallest) {
                tallest = thisHeight;
            }
        });
        group.height(tallest);
    }

    equalHeight($(".program_slider .program_item"));


    $('.input_search').focusin(function () {
        if (this.value == this.defaultValue) {
            this.value = '';
            $(this).css("font-style", "normal");
        }
    }).focusout(function () {
            if (this.value == '') {
                this.value = this.defaultValue;
                $(this).css("font-style", "italic");
            }
        });


    $('.styled_radio_button').customRadioCheck();
    if ($('input[name=hiding]').val() == '1') {
        $('.custom-check').addClass('checked');
    }
//    $('input[name=hiding]').on('click', function () {
//        console.log($('.styled_radio_button').is(':checked'));
//    })


    /*$('#myCarousel').carousel({
     interval: 10000
     });

     $('#myCarousel .carousel-nav li:first-child').addClass('active');

     $('#myCarousel .carousel-nav a').click(function (q) {
     q.preventDefault();
     $('#myCarousel .carousel-nav li').removeClass('active');
     $(this).parent().addClass('active');
     });
     $('#myCarousel').bind('slid', function () {
     var index = $('#myCarousel .item').index($('#myCarousel .item.active'));
     $('#myCarousel .carousel-nav li.active').removeClass('active');
     $($('#myCarousel .carousel-nav li').get(index)).addClass('active');
     });*/


    $('#myCarousel').each(function () {
        var self = $(this);
        var stop = false;
        var images = self.find('.carousel-inner .item');
        var links = self.find('.carousel-nav li');

        function slide(link) {
            var index = links.index(link);
            images.hide();
            $(images.get(index)).fadeIn();
            links.removeClass('active');
            link.addClass('active');
        }

        self.on('mouseenter', '.carousel-nav li',function (e) {
            e.preventDefault();
            var link = $(this);
            slide(link);
            stop = true;
        }).on('mouseleave', '.carousel-nav li', function () {
                stop = false;
            });

        self.on('mouseenter', 'img',function () {
            stop = true;
        }).on('mouseleave', 'img', function () {
                stop = false;
            });

        links.first().addClass('active');
        images.first().show();
        var cycle = function () {
            setTimeout(function () {
                if (stop) {
                    cycle();
                    return;
                }
                var active = links.filter('.active');
                var link = active.next();
                if (!link.length) {
                    link = links.first();
                }
                slide(link);
                cycle();
            }, 5000);
        }
        cycle();
    });

    var cite_slider = $('.bx-slider').bxSlider({
        pager: true,
        // slideWidth: 420,
        auto: true,
        pause: 8000,
        // controls: false,
        slideSelector: 'div.bx-slide',
        // slideMargin: 0,
        // adaptiveHeight: false,
        infiniteLoop: true,
        // hideControlOnEnd: true,
        // responsive: true,
        // preloadImages: 'all'
        mode: 'horizontal'
    });

    $('.bx-news').bxSlider({
        pager: true,
        // slideWidth: 630,
        auto: true,
        // controls: false,
        slideSelector: 'div.bx-slide',
//        moveSlides: 1,
//        maxSlides: 1,
//        minSlides: 1,
        // slideMargin: 0,
        // adaptiveHeight: false,
        // mode: 'fade',
        infiniteLoop: true,
        // hideControlOnEnd: true,
        // responsive: true,
        mode: 'horizontal'
    });

    $('.spr-slider').bxSlider({
        pager: false,
        slideWidth: 300,
        auto: true,
        controls: true,
        slideSelector: 'div.slide',
       moveSlides: 1,
       maxSlides: 3,
       minSlides: 3,
        slideMargin: 30,
        // adaptiveHeight: false,
        // mode: 'fade',
        infiniteLoop: true,
        hideControlOnEnd: true,
        // responsive: true,
        mode: 'horizontal'
    });


    $('#lslider-one').lightSlider({
        autoWidth:false,
        slideMove: 1,
        slideMargin: 10,
        item: 1,
        loop:true,
        controls: false,
        pager: false,
        auto: true,
        speed: 1000,
        pause: 7000,
        onSliderLoad: function(el) {
            for (var i =0; i < el[0].childElementCount; i++){
                $(el[0].children[i]).css('line-height', el[0].clientHeight+'px');
            }
        }
    });

    $('#lslider-two').lightSlider({
        autoWidth:false,
        loop:true,
        slideMove: 1,
        slideMargin: 10,
        item: 1,
        auto: true,
        speed: 1000,
        controls: false,
        pager: false,
        pause: 7000,
        onSliderLoad: function(el) {
            for (var i =0; i < el[0].childElementCount; i++){
                $(el[0].children[i]).css('line-height', el[0].clientHeight+'px');
            }
        }
    }), $('#lslider-three').lightSlider({
        autoWidth:false,
        loop:true,
        slideMove: 1,
        slideMargin: 10,
        item: 1,
        auto: true,
        speed: 1000,
        controls: false,
        pager: false,
        pause: 7000,
        onSliderLoad: function(el) {
            // for (var i =0; i < el[0].childElementCount; i++){
                // $(el[0].children[i]).css('line-height', el[0].clientHeight +'px');
            // }

        }
    }), $('#lslider-head').lightSlider({
        autoWidth:true,
        // item: 2,
        loop: true,
        // slideMove: 1,
        slideMargin: 20,
        // auto: true,
        speed: 1000,
        controls: false,
        pager: false,
        pause: 7000,
        onSliderLoad: function(el) {
                
                var $win = $(window).width();

                if ($win > 999) {
                    $(el).css('width', '100%');
                }
                
        }
    });

    $('.block-federation a').tooltip({placement: 'top'});


//    $("#passfield").passField();
    lang = $('#date-picker').val();
    $.datepicker.setDefaults($.datepicker.regional[lang]);
    $(".has_datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "1940:2013",
        dateFormat: "dd.mm.yy",
        maxDate: 0
    });

    $('.dropdown_menu .input_search,.dropdown #map').click(function (e) {
        e.stopPropagation();
    });

    $('#social_tab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

//       VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "400", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 52891963);

});
$(function () {

    new Ya.share({
        element: 'social_block',
        elementStyle: {
            type: 'icon'},
//            quickServices:['vkontakte', 'twitter', 'facebook']

        popupStyle: {
            blocks: ['friendfeed', 'gplus', 'pinterest', 'surfingbird', 'moikrug']

        }
    });
});

function Load() {
    text = document.getElementById('result').innerHTML;
//    text2 = document.getElementById('result2').innerHTML;
    printwin = open('', 'printwin', 'width=500,height=400');
    printwin.document.open();
    printwin.document.writeln('<html><head><title></title></head><body onload=print();close()>');
    printwin.document.writeln(text);
    printwin.document.writeln('<div style="page-break-before:always;">');
    printwin.document.writeln('</div>');
    printwin.document.writeln('</body></html>');
    printwin.document.close();
}

$(function () {
    var sex = null;
    $('#sex').on('change', function () {
        sex = $('#sex option:selected').val();
        if (sex == 1) {
            $('#haunch').css('display', 'none');
        }
        if (sex == 2) {
            $('#haunch').css('display', 'table-row');
        }
    })
});

$(function () {
    var date = new Date()
    var name = $('#city').val();
    $('#city').focusin(function () {
        if ($('#city').val() == name) {
            $('#city').val('');
        }
    });

    $('#city').focusout(function () {
        if ($('#city').val() == '') {
            $('#city').val(name);
        }
    });

    $('#city').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: BASE_URL + "city/source",
                dataType: "json",
                data: {
                    featureClass: "P",
                    style: "full",
                    maxRows: 12,
                    q: request.term,
                    lang: $('#lang').attr('data-lang')
                },
                success: function (data) {
                    response(data);
                    response($.map(data.geonames, function (item) {
                        return {
                            label: item.name + ", " + item.country,
                            value: item.name,
                            sef: item.sef
                        };
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            var href = window.location.href.split('/');
            if ($('#lang').attr('data-lang') == 'kz') {
                if (!href[5]) {
                    href[5] = date.getFullYear();
                    href[6] = 'all';
                    href[8] = 'all';
                }
                href[7] = ui.item.sef;
            } else {
                if (!href[6]) {
                    href[6] = date.getFullYear();
                    href[7] = 'all';
                    href[9] = 'all';
                }
                href[8] = ui.item.sef;
            }
            window.location.href = href.join('/');
        }
    });
});

$(function () {
    document.onkeydown = NavigateThrough;
    function NavigateThrough(event) {
        if (window.event) event = window.event;
        if (event.ctrlKey) {
            var link = null;
            var href = null;
            switch (event.keyCode ? event.keyCode : event.which ? event.which : null) {
                case 0x27:
                    link = document.getElementById('NextLink');
                    break;
                case 0x25:
                    link = document.getElementById('PrevLink');
            }
            if (link && link.href) document.location = link.href;
            if (href) document.location = href;
        }
    }
})

$(function () {
    $('.subscribe').on('click', function (e) {
        e.preventDefault();
        self = $(this);
        if (self.attr('data-change') == 0) {
            self.attr('data-change', 1);
        } else {
            self.attr('data-change', 0);
        }
        id = self.attr('data-id');
        sub = $('#sub').val();
        unsub = $('#unsub').val();
        $.get(self.attr('data-url'), {'id': id}, function (data) {
            if (data && data == 'success') {
                if (self.attr('data-change') == 1) {
                    self.attr('data-url', BASE_URL + "privatecoach/subscribe/delete");
                    self.text(unsub);
                } else {
                    self.attr('data-url', BASE_URL + "privatecoach/subscribe/add");
                    self.text(sub);
                }
            }
        });
    });
});

/*Функция подсчета символов в textarea*/
$(function () {
    $('#annotation').keyup(function countRemainingChars() {
        maxchars = 500;
        number = $('#annotation').val().length;
        if (number <= maxchars) {
            $('#text_counter').html(maxchars - number + " символов осталось");
        }
    });
});

$(function () {
    var remove = $('.delete_photo');
    var url = remove.attr('data-url');
    remove.on('click', function (e) {
        e.preventDefault();
        if (confirm($('#alert_message').text())) {
            var self = $(this);
            $.get(url, {'id': self.attr('data-id')}, function (data) {
                if (data && data == 'success') {
                    self.parents('li').remove();
                }
            });
        }
    })
});

$(function () {
    var delete_event = $('.delete_event');
    var delete_url = delete_event.attr('data-url');

    delete_event.on('click', function (i) {
        i.preventDefault();
        if (confirm($('#confirm').text())) {
            var self = $(this);
            $.get(delete_url, {'id': self.attr('data-id')}, function (data) {
                if (data) {
                    if (data == 'success' || data == 'hide') {
                        self.parents('li').remove();
                    }
                }
            });
        }
    });
});

$(function () {
    var datapicker = $('#datepicker').datepicker({
        format: 'yyyy/mm/dd',
        language: $('#datepicker').attr('data-lang')
//        language: 'kk'
    });
    datapicker.on('changeDate', function (i) {
        var newDate = new Date(i.date);
        var d = newDate.getDate();
        var m = newDate.getMonth();
        var y = newDate.getFullYear();
        var monthName = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
        $("#advanced_form").val($.datepicker.formatDate(d + "." + (m + 1) + "." + y, newDate));
        m = monthName[m];
        $("#datepicker").text($.datepicker.formatDate(d + " " + m + ", " + y, newDate));

//            $('#datepicker').text(newDate);
    });
});

$(function () {
    // ya.share
    for (var i = 1; i < parseInt($('.count').attr('data-count')) + 1; i++) {
        new Ya.share({
            "element": 'ya_share' + i,
            "elementStyle": {'type': 'none', 'border': false,
                'quickServices': "yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,gplus".split(",")},
            "link": $('#ya_share' + i).attr('data-yashareLink'),
            "title": $('#ya_share' + i).attr('data-yashareTitle'),
            "description": $('#ya_share' + i).attr('data-yashareDescription'),
            "image": $('#ya_share' + i).attr('data-yashareImage'),
            "L10n": $('#ya_share' + i).attr('data-yashareL10n')
        });
    }
});

$(function () {
    var $el = $('#program_slider'),
        $inner = $('.program_inner', $el),
        $wrapper = $('.program_item_wrap', $el),
        $prev = $('a.prev', $el),
        $next = $('a.next', $el),
        bothBtn = $('a.next, a.prev', $el),
        itemsWidth = 0,
        offsetDiff = 0,
        mouseDown = false,
        timeout
        ;

    if ($el.length == 0) return;

    $inner.find('.program_item').each(function () {
        itemsWidth += $(this).width();
    });

    offsetDiff = itemsWidth - $inner.width();

    function slide(direction) {
        var itemsOffset = parseInt($wrapper.css('margin-left')),
            newItemsOffset = itemsOffset + (5 * direction);

        if (newItemsOffset > 0 || newItemsOffset < (-offsetDiff)) {
            newItemsOffset = itemsOffset;
        }

        $wrapper.css('margin-left', newItemsOffset + 'px');
        if (mouseDown) {
            timeout = setTimeout(function () {
                slide(direction);
            }, 5);
        }
    }

    bothBtn.on('mouseout mouseup', function () {
        mouseDown = false;
    });

    $prev.on('mousedown', function () {
        mouseDown = true;
        slide(1);
    });

    $next.on('mousedown', function () {
        mouseDown = true;
        slide(-1);
    });


});


$(function () {
    var id = null;
    $('.page_program').on('click', '.exTitle', function () {
        id = $(this).attr('data-id');
    });
    $('.table_add_program').on('click', '.col1.ng-binding', function () {
        id = $(this).attr('data-id');
        console.log(id)
    });

    $('#myExercise').on('show', function () {
        var url = $(this).attr('data-url');
        if (id) {
            $.get(url + '/' + id, function (data) {
                if (data) {
                    $('#modal_exercise').html(data);
                }
            })
        }
    })
});

$(function () {
    $('#bday').on('change', initDays);
    $('#bmonth').on('change', initDays);
    $('#byear').on('change', initDays);
});

$(function () {
    $('.promt').on('click', function () {
        var url = $('#myModal').attr('data-url');
        var id = $(this).attr('data-id');
        var action = $(this).attr('data-action');
        $.get(url, {'id': id, 'action': action});
    });
});

function initDays() {
    var month = $('#bmonth').val();
    var year = $('#byear').val();
    var day = $('select[name=bday]').val();
    var available_days = new Date(year, month, 0).getDate();
    var days = $('select[name=bday]').children('option').length;
    if (available_days > days) {
        for (var a = days; a < available_days; a++) {
            $('select[name=bday]').append($('<option value="' + (a + 1) + '">' + (a + 1) + '</option>'));
        }
    } else if (available_days < days) {
        for (var d = days; d > available_days; d--) {
            $('select[name=bday] option[value="' + d + '"]').remove();
        }
    }
    $('#advanced_form').val(year + '-' + month + '-' + day);
}