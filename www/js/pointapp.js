/**
 * Created with JetBrains PhpStorm.
 * User: igor
 * Date: 20.12.12
 * Time: 10:10
 * To change this template use File | Settings | File Templates.
 */
(function(){

    var pointData=null;
    var pointMarker=null;
    var closerTarlan=-1;
    var markers={};
    var map=null;

    $(function(){
        //Подгрузить город, инициализировать карту в соотвествии с городом

        ymaps.ready(initmap)
        //Подгрузить точки, нанести на карту
        //Вывести список точек
        //Инициализировать фильтры
    });

    function initmap(){
        var lat,lng=null;
        var el = $('#map');
        var id= el.attr('data-id');
        map=MapLib.init(el,CITY_LAT,CITY_LNG);
        $.getJSON(BASE_URL+'wpoint/point_data/'+id,function(data){
            pointData=data;
            showPoint();
            showTarlan();
            initEvents();
//            showPoints();
//            renderList();
        });

    }

    function initEvents(){
       $('.view_point_tc').click(function(e){
           e.preventDefault();
           showCloserTarlan();
       });

       $(document).on('click','.vote_rating a',function(e){
           e.preventDefault();
           var self=$(this);
           var vote=self.html();
           $.getJSON(BASE_URL+'vote/vote/'+pointMarker.pointId+'?target=wpoint&vote='+vote,function(data){
               alert(data.msg);
               if (data.status=='ok'){
                   window.location.reload();
               }
           });

       });
    }

    function showPoint(){
        var point=pointData.point;
        var coords=[parseFloat(point.lat),parseFloat(point.lng)];
        var marker = new ymaps.Placemark(coords,{

            balloonContentHeader: point.title,
            balloonContentBody: point.description,
            balloonContentFooter: point.footer
        },{
            iconImageHref:BASE_URL+'images/wifi_point.png', // картинка иконки
            iconImageSize: [53, 70], // размеры картинки
            iconImageOffset: [-15, -70] // смещение картинки
        });
        marker.pointId=point.id;
        marker.pointCoords=coords;
        marker.pointInfo=point;
        map.geoObjects.add(marker);
        pointMarker=marker;
        moveToPoint(coords);
    }

    function moveToPoint(coords){
        map.panTo(coords, {
            flying: true,
            duration: 3000
        });
    }

    function showTarlan(){
       var points=pointData.tarlans;
       for (var i in points){
           var point=points[i];
           var coords=[parseFloat(point.lat),parseFloat(point.lng)];
           var marker = new ymaps.Placemark(coords,{

               balloonContentHeader: point.title,
               balloonContentBody: point.description,
               balloonContentFooter: point.footer
           },{
               iconImageHref:BASE_URL+'images/tc_point.png', // картинка иконки
               iconImageSize: [38, 42], // размеры картинки
               iconImageOffset: [-13, -42] // смещение картинки
           });
           marker.pointId=point.id;
           marker.pointCoords=coords;
           marker.pointInfo=point;
           marker.distance=ymaps.coordSystem.geo.getDistance(coords,pointMarker.pointCoords);
           if (closerTarlan===-1){
               closerTarlan=marker;
           } else if(marker.distance<closerTarlan.distance){
               closerTarlan=marker;
           }
           map.geoObjects.add(marker);
           markers[point.id]=marker;
       }
    }

    function showCloserTarlan(){
       moveToPoint(closerTarlan.pointCoords);
    }

})();