///**
// * Created by id541rak on 14.04.14.
// */
var timeout;
$(function () {
    var submit = $('#submit');
    var add_recipe = $('#add_recipe');
    var url = submit.attr('data-url');
    var el_url = $('#ingredient_result').attr('data-url');
    var items = [];
    var target = $('.typeahead');
    var input_upload = $('.input_upload');
//    timeout = null;
    button = null;

    function recalcColumns() {
        $(".nav_column.diet_menu_list").smartColumns(210);
    }


    target.popover({
        content: $(this).attr('data-content'),
        placement: 'bottom'
    });

    input_upload.change(function (e) {
        $('#upload').click();
    });

    $('#ing_form').ajaxForm({
        complete: function (tr) {
            var response = JSON.parse(tr.responseText);
            $('.preview_img').html('<img src="' + response.src + '" style="height:220px">');
            $('input[name=image_file]').val(parseInt(response.file));
        }
    });

    target.typeahead({
        minLength: 3,
        source: function (query, callback) {
            var src = target.attr('data-source');
            $.getJSON(src, {query: query}, function (data) {
                var sourceArr = [];
                for (var i in data) {
                    var item = data[i];
                    items[item.name] = item.id;
                    items['calories'] = removeDecimals(item.calories);
                    items['protein'] = removeDecimals(item.protein);
                    items['carbohydrate'] = removeDecimals(item.carbohydrate);
                    items['fat'] = removeDecimals(item.fat);
                    sourceArr.push(item.name);
                }
                callback(sourceArr)
            });
        },
        updater: function (el) {
            $('.product').removeClass('hidden');
            $('#product_calories').text(items['calories']);
            $('#product_protein').text(items['protein']);
            $('#product_carbohydrate').text(items['carbohydrate']);
            $('#product_fat').text(items['fat']);
            $('#product_id').val(items[el]);
            return el;
        }
    });

    $('.add_data').on('click', function (e) {
        $.get(el_url, {q: items[$('.typeahead').val()]}, function (data) {
            if (data) {
                if ($('.new_element').length > 0) {
                    $('.new_element:last').after(data);
                    recalcColumns();
                    $('.delete_ing').on('click', function (e) {
                        e.preventDefault();
                        $(this).parents('.new_element').remove();
                    });
                } else {
                    $('#ingredient_result').html(data);
                    recalcColumns();
                    $('.delete_ing').on('click', function (e) {
                        e.preventDefault();
                        $(this).parents('.new_element').remove();
                    });
                }
            } else {
                e.preventDefault();
                target.popover('show');
            }
        })
    });

    var object = $('#search').val();
    $('#search').focusin(function () {
        if ($('#search').val() == object) {
            $('#search').val('');
        }
    });
    $('#search').focusout(function () {
        if ($('#search').val() == '') {
            $('#search').val(object);
        }
    });

    add_recipe.on('click', function (a) {
        a.preventDefault();
    });
    submit.on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: submit.attr('data-url'),
            data: $('#diet_filter').serialize(),
            success: function (data) {
                $('.dish_list').html(data);
                modal();
                $('#dishes_tabs a').click(function (e) {
                    e.preventDefault();
                    $('.tab-pane').removeClass('active');
                    $(this).tab('show');
                })
            }
        });
    });

    $('.fast_search_block').on('click', function (e) {
        e.preventDefault();
        var category = $(this).attr('data-category');
        var url = $('.fast_search').attr('data-url');
        $.get(url, {category: category}, function (data) {
            $('.dish_list').html(data);
            modal();
        });
    });

//    if ($('.dish_list').length > 0) {
//        $.ajax({
//            type: "POST",
//            url: submit.attr('data-url'),
//            data: $('#diet_filter').serialize(),
//            success: function (data) {
//                $('.dish_list').html(data);
//                $('#dishes_tabs a').click(function (e) {
//                    e.preventDefault();
//                    $('.tab-pane').removeClass('active');
//                    $(this).tab('show');
//                })
//            }
//        });
//    }

    var calories_range = $("#calories_range");
    var protein_range = $("#protein_range");
    var carbohydrate_range = $("#carbohydrate_range");
    var fat_range = $("#fat_range");

    calories_range.slider({
        range: true,
        min: 0,
        max: 900,
        values: [ 0, 900 ],
        slide: function (event, ui) {
            $('#min_cal').val(ui.values[ 0 ]);
            $('#max_cal').val(ui.values[ 1 ]);
        }
    });
    protein_range.slider({
        range: true,
        min: 0,
        max: 50,
        values: [ 0, 50 ],
        slide: function (event, ui) {
            $('#min_prot').val(ui.values[ 0 ]);
            $('#max_prot').val(ui.values[ 1 ]);
        }
    });
    carbohydrate_range.slider({
        range: true,
        min: 0,
        max: 100,
        values: [ 0, 100 ],
        slide: function (event, ui) {
            $('#min_carb').val(ui.values[ 0 ]);
            $('#max_carb').val(ui.values[ 1 ]);
        }
    });
    fat_range.slider({
        range: true,
        min: 0,
        max: 100,
        values: [ 0, 100 ],
        slide: function (event, ui) {
            $('#min_fat').val(ui.values[ 0 ]);
            $('#max_fat').val(ui.values[ 1 ]);
        }
    });


    $('#min_cal').val(calories_range.slider("values", 0));
    $('#max_cal').val(calories_range.slider("values", 1));
    $('#min_prot').val(protein_range.slider("values", 0));
    $('#max_prot').val(protein_range.slider("values", 1));
    $('#min_carb').val(carbohydrate_range.slider("values", 0));
    $('#max_carb').val(carbohydrate_range.slider("values", 1));
    $('#min_fat').val(fat_range.slider("values", 0));
    $('#max_fat').val(fat_range.slider("values", 1));

    switch (LANG) {
        case 'ru':
            var monthName = ['', 'Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
            break;
        case 'kz':
            var monthName = ['', "Қаңтар", "Ақпан", "Наурыз", "Сәуір", "Мамыр", "Маусым", "Шілде", "Тамыз", "Қыркүйек", "Қазан", "Қараша", "Желтоқсан"];
            break;
        case 'en':
            var monthName = ['', "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            break;
    }
    ;

    $('.fast_search_block').on('click', function (e) {
        e.preventDefault();
        var category = $(this).attr('data-category');
        var url = $('.fast_search').attr('data-url');
        $.get(url, {category: category}, function (data) {
            $('.dish_list').html(data);
            modal();
        });
    });

//    if ($('.dish_list').length > 0) {
//        $.ajax({
//            type: "POST",
//            url: submit.attr('data-url'),
//            data: $('#diet_filter').serialize(),
//            success: function (data) {
//                $('.dish_list').html(data);
//                $('#dishes_tabs a').click(function (e) {
//                    e.preventDefault();
//                    $('.tab-pane').removeClass('active');
//                    $(this).tab('show');
//                })
//            }
//        });
//    }

    var calories_range = $("#calories_range");
    var protein_range = $("#protein_range");
    var carbohydrate_range = $("#carbohydrate_range");
    var fat_range = $("#fat_range");

    calories_range.slider({
        range: true,
        min: 0,
        max: 900,
        values: [ 0, 900 ],
        slide: function (event, ui) {
            $('#min_cal').val(ui.values[ 0 ]);
            $('#max_cal').val(ui.values[ 1 ]);
        }
    });
    protein_range.slider({
        range: true,
        min: 0,
        max: 50,
        values: [ 0, 50 ],
        slide: function (event, ui) {
            $('#min_prot').val(ui.values[ 0 ]);
            $('#max_prot').val(ui.values[ 1 ]);
        }
    });
    carbohydrate_range.slider({
        range: true,
        min: 0,
        max: 100,
        values: [ 0, 100 ],
        slide: function (event, ui) {
            $('#min_carb').val(ui.values[ 0 ]);
            $('#max_carb').val(ui.values[ 1 ]);
        }
    });
    fat_range.slider({
        range: true,
        min: 0,
        max: 100,
        values: [ 0, 100 ],
        slide: function (event, ui) {
            $('#min_fat').val(ui.values[ 0 ]);
            $('#max_fat').val(ui.values[ 1 ]);
        }
    });


    $('#min_cal').val(calories_range.slider("values", 0));
    $('#max_cal').val(calories_range.slider("values", 1));
    $('#min_prot').val(protein_range.slider("values", 0));
    $('#max_prot').val(protein_range.slider("values", 1));
    $('#min_carb').val(carbohydrate_range.slider("values", 0));
    $('#max_carb').val(carbohydrate_range.slider("values", 1));
    $('#min_fat').val(fat_range.slider("values", 0));
    $('#max_fat').val(fat_range.slider("values", 1));

    switch (LANG) {
        case 'ru':
            var monthName = ['', 'Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
            break;
        case 'kz':
            var monthName = ['', "Қаңтар", "Ақпан", "Наурыз", "Сәуір", "Мамыр", "Маусым", "Шілде", "Тамыз", "Қыркүйек", "Қазан", "Қараша", "Желтоқсан"];
            break;
        case 'en':
            var monthName = ['', "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            break;
    }

    if ($('.dish_list').attr('data-edited')) {
        var NewDate = new Date();
        var d = NewDate.getDate();
        var m = NewDate.getMonth();
        var y = NewDate.getFullYear();
        var today = y + "-" + addZero(m + 1) + "-" + addZero(d);
        console.log(today)

        $('#changeDate').text($.datepicker.formatDate(d + " " + monthName[(m + 1)] + ", " + y, NewDate));
        $('#changeDate').attr('data-picker-date', today);
        get_data($('.dish_list').attr('data-url'), today);
    }

    $('.nav_date').on('click', function (e) {
        e.preventDefault();
        var newDate = new Date($('#changeDate').attr('data-picker-date'));
        var d = newDate.getDate();
        var m = newDate.getMonth();
        var y = newDate.getFullYear();
        var date = y + "-" + addZero(m + 1) + "-" + addZero(d);
        var url = $('.dish_list').attr('data-url');

        if ($(this).hasClass('prev')) {
            newDate.setDate(d - 1);
        } else {
            newDate.setDate(d + 1);
        }
        d = newDate.getDate();
        m = newDate.getMonth();
        y = newDate.getFullYear();
        date = y + "-" + addZero(m + 1) + "-" + addZero(d);
        $('#changeDate').text($.datepicker.formatDate(d + " " + monthName[(m + 1)] + ", " + y, newDate));
        $('#changeDate').attr('data-picker-date', date);
        $('#norm_date').val(date);
        get_data(url, date);
    });

    $('#changeDate').datepicker({
//        date: $('#changeDate').attr('data-picker-date'),
        language: $('#changeDate').attr('data-lang'),
        class: 'dtt'
    }).on('changeDate', function (i) {
            var newDate = new Date(i.date);
            var d = newDate.getDate();
            var m = newDate.getMonth();
            var y = newDate.getFullYear();
            var date = y + "-" + addZero(m + 1) + "-" + addZero(d);
            var dataUrl = $('.dish_list').attr('data-url');
            var normUrl = $('.table_result').attr('data-url');

            $(this).text($.datepicker.formatDate(d + " " + monthName[(m + 1)] + ", " + y, newDate));
            $(this).attr('data-picker-date', date);
            $('#norm_date').val(date);
            get_data(dataUrl, date);
            get_norms(normUrl, date);

            $(this).datepicker('hide');
        });

    $('.add_my_product').on('click', function (e) {
        e.preventDefault();
        var form = $('#new_product');
        $.ajax({
            type: "POST",
            url: form.attr('data-url'),
            data: form.serialize(),
            success: function (data) {
                if (button.parent().prev().find($('.ingestion_item')).length > 0) {
                    button.parent().prev().children('tbody').children('.ingestion_item:last').after(data);
                } else {
                    button.parent().prev().children('tbody').html(data);
                }
                $('#myDish').modal('hide');
                delete_product();
                change_amount();
            }
        });
    });

    $('.dish_list').on('click', '.delete_ingestion', function (e) {
        e.preventDefault();
        var url = $('.dish_list').attr('data-url-remove');
        var id = $(this).parents('table').attr('data-id');
        var table = $(this);
        $.get(url, {id: id, model: 'Ration'}, function (data) {
            if (data == 'success') {
                table.parents('table').next().remove();
                table.parents('table').remove();
                change_amount();
            }
        });

    });

    $('.load_level').on('click', function (e) {
        e.preventDefault();
        var select_day = $('.select_day');
        var levels = [];
        var cal_levels = [1.2, 1.4, 1.6, 1.8, 2.0];
        var prt_levels = [1, 1.25, 1.5, 1.75, 2];
        var mission = select_day.attr('data-mission');
        levels['cal'] = cal_levels;
        levels['prt'] = prt_levels;

        var k_cal = levels['cal'][$(this).attr('data-level')];
        var k_prt = levels['prt'][$(this).attr('data-level')];
        var bmr = select_day.attr('data-bmr');
        var weight = select_day.attr('data-weight');
        var cal_result = Math.round(bmr * k_cal);
        var prt_result = Math.round(weight * k_prt);
        var fat_result = Math.round(cal_result / 27.5);
        var crb_result = Math.round((cal_result - 4.1 * prt_result - 9 * fat_result) / 3.5);
        if (mission == 1) {
            cal_result = Math.round(cal_result * 1.2);
            prt_result = Math.round(prt_result * 1.2);
            fat_result = Math.round(fat_result * 1.2);
            crb_result = Math.round(crb_result * 1.2);
        } else if (mission == 2) {
            cal_result = Math.round(cal_result * 0.8);
            prt_result = Math.round(prt_result * 0.8);
            fat_result = Math.round(fat_result * 0.8);
            crb_result = Math.round(crb_result * 0.8);
        }
        select_day.children('.dropdown-toggle').text($(this).text());

        $('.norm_cal').children('span').text(cal_result);
        $('.norm_prt').children('span').text(prt_result);
        $('.norm_crb').children('span').text(crb_result);
        $('.norm_fat').children('span').text(fat_result);

        $('#norm_calories').val(cal_result);
        $('#norm_protein').val(prt_result);
        $('#norm_carbohydrate').val(crb_result);
        $('#norm_fat').val(fat_result);

        var url = $('#user_norm').attr('data-url');
        $.post(url, $("#user_norm").serialize());
    });

});


function removeDecimals(num) {
    var decimal = num - Math.floor(num);
    if (decimal > 0) {
        return num;
    } else {
        return Math.floor(num);
    }
}

function change_weight() {
    $('.table_diet').on('change', '.dish_weight', function () {
        var obj = $(this);
        var id = obj.attr('data-id');
//        $.ajax({
//            type: "POST",
//            url: $('.date_my_diet').attr('data-changeweight-url'),
//            data: obj.parent('.dish_form').serialize(),
//            success: function (data) {
//                obj.parents('.ingestion_item').children('.calories').text(text.calories);
//                obj.parents('.ingestion_item').children('.protein').text(text.protein);
//                obj.parents('.ingestion_item').children('.carbohydrate').text(text.carbohydrate);
//                obj.parents('.ingestion_item').children('.fat').text(text.fat);
//            }
//        });
        $.getJSON($('.date_my_diet').attr('data-changeweight-url'), {id: id, weight: obj.val()}, function (text) {
            obj.parents('.ingestion_item').children('.calories').text(text.calories);
            obj.parents('.ingestion_item').children('.protein').text(text.protein);
            obj.parents('.ingestion_item').children('.carbohydrate').text(text.carbohydrate);
            obj.parents('.ingestion_item').children('.fat').text(text.fat);
            change_amount();
        });

    })
}

function get_norms(url, date) {
    $.getJSON(url, {date: date}, function (data) {
        if (data) {

            $('.norm_levels span').text(data.level);
            $('.norm_cal span').text(data.calories);
            $('.norm_prt span').text(data.protein);
            $('.norm_crb span').text(data.carbohydrate);
            $('.norm_fat span').text(data.fat);
        }
    });
}

function get_data(url, date) {
    $.get(url, {date: date}, function (data) {
        if (data) {
            $('.dish_list').html(data);
            $('.dish_timepicker').datetimepicker({
                pickDate: false,
                pickSeconds: false
            }).on('changeDate', function () {
                    var input_value = $(this).children('.input_date_picker').val();
                    var time = input_value.match(/..:../i);
                    var id = $(this).parents('.ingestion').attr('data-id');
                    $(this).children('span').children('.add-on').text(time[0]);
                    clearTimeout(timeout);
                    timeout = setTimeout(function () {
                        changeTime(time, id)
                    }, 3000);
                });
            $('.add_ingestion').on('click', function (e) {
                e.preventDefault();
                add_ingestion($(this));
            });
            $('.table_result').children('tbody').removeClass('hidden');
            add_product();
            delete_product();
            change_weight();
            change_amount();
        }
    });
}

function add_ingestion(add_ingestion) {
    var url = $('.add_ingestion').attr('data-url');
    var date = $('#changeDate').attr('data-picker-date');
    $.get(url, {date: date}, function (data) {
        add_ingestion.parents('div.link_control').before(data);
        add_product();
        change_weight();
        change_amount();
        $('.dish_timepicker').datetimepicker({
            pickDate: false,
            pickSeconds: false
        }).on('changeDate', function () {
                var input_value = $(this).children('.input_date_picker').val();
                var time = input_value.match(/..:../i);
                var id = $(this).parents('.ingestion').attr('data-id');
                $(this).children('span').children('.add-on').text(time[0]);
                clearTimeout(timeout);
                timeout = setTimeout(function () {
                    changeTime(time, id)
                }, 3000);
            });
    })
}

function change_amount() {
    var calories = 0;
    var proteins = 0;
    var carbs = 0;
    var fats = 0;
    $('.calories').each(function () {
        calories += parseInt($(this).text());
    });
    $('.protein').each(function () {
        proteins += parseInt($(this).text());
    });
    $('.carbohydrate').each(function () {
        carbs += parseInt($(this).text());
    });
    $('.fat').each(function () {
        fats += parseInt($(this).text());
    });
    $('#cal_amount').children('span').text(calories + ' ');
    $('#prt_amount').children('span').text(proteins + ' ');
    $('#crb_amount').children('span').text(carbs + ' ');
    $('#fat_amount').children('span').text(fats + ' ');
}

function changeTime(time, id) {
    var url = $('.date_my_diet').attr('data-changetime-url');
    $.get(url, {time: time, id: id});
}

function add_product() {
    //открыть модал для добавления продукта
    $('.add_new_product').on('click', function (e) {
        e.preventDefault();
        button = $(this);
        var id = $(this).parent().prev().attr('data-id');
        $('#ration_id').val(id);
        $('#myDish').modal('show');
    });
}

function query(url) {
    var q = $('#search').val();
    $.get(url, {'q': q}, function (data) {
        if (data) {
            $('.dish_list').html(data);
            $('#dishes_tabs a').click(function (e) {
                e.preventDefault();
                $('.tab-pane').removeClass('active');
                $(this).tab('show');
            })
        }
    })
}
function delete_product() {
    $('.delete_dish').on('click', function (e) {
        e.preventDefault();
        var delete_dish = $(this);
        var url = $(this).parents('.dish_list').attr('data-url-remove');
        var id = $(this).attr('data-id');
        $.get(url, {id: id, model: 'Dish_Ration'}, function (data) {
            if (data == 'success') {
                delete_dish.parents('.ingestion_item').remove();
                change_amount();
            }
        });
    });
}

function modal() {
    var id = null;
    var category = null;
    $('.modal_anchor').on('click', function () {
        id = $(this).attr('data-id');
        category = $(this).attr('data-category');

    });
    $('#myDish').on('show', function () {
        var url = $(this).attr('data-url');
        if (id) {
            $.get(url + '/' + id, {category: category}, function (data) {
                if (data) {
                    $('#modal_dish').html(data);
                    $('.form_add_datepicker').datetimepicker({
                        pickTime: false
                    }).on('dp.change', function (i) {
                            var date = new Date(i.date._d);
                            var day = date.getDate();
                            var month = date.getMonth();
                            var year = date.getFullYear();
                            $(this).children('a').text(addZero(day) + '.' + addZero(month + 1) + '.' + year);
                            $('[name=date]').val(year + '-' + addZero(month + 1) + '-' + addZero(day));
                        });
                    $('.datetime').datetimepicker({
                        format: 'hh:ii',
                        pickDate: false
                    }).on('dp.change', function (i) {
                            var date = new Date(i.date._d);
                            var hours = date.getHours();
                            var minutes = date.getMinutes();
                            $(this).children('a').text(hours + ':' + addZero(minutes))
                            $('[name=date]').val(hours + ':' + addZero(minutes) + ':00')
                        });
                    postNewDish();
                }
            })
        }
    });
}

$(document).on("click", function (e) {
    var button = $(e.target).is('.add_data'),
        inPopover = $(e.target).closest('.popover').length > 0,
        inPicker = ($(e.target).closest('.bootstrap-datetimepicker-widget').length > 0);
    if (!button && !inPopover) $('.typeahead').popover('hide');
    if (!inPicker && $('.bootstrap-datetimepicker-widget').css('display') == 'none') {
        $('.dish_timepicker').datetimepicker('hide');
    }
});

function postNewDish() {
    var submit = $('.form_submit');
    submit.on('click', function () {
        $.ajax({
            type: "POST",
            url: submit.attr('data-url'),
            data: $('[name=new_my_dish]').serialize(),
            success: function () {
                $('#myDish').modal('hide')
            }
        });
    })
}

function addZero(i) {
    return ("" + i).length == 1 ? "0" + i : i;
}