$(function () {
    var category = [];
    var url = $('.event_star_list').attr('data-url');
    var score = 0;
    filter(url, category, score, null);
    $('.filter_articles').on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            delete category[searchElement(category, $(this).attr('data-category'))];
        } else {
            category.push($(this).attr('data-category'));
            $(this).addClass('active');
        }
        var score = $('#ratingFilter').hasClass('active') ? 1 : 0;
        counter = 1;
        filter(url, category, score, counter);

    });

    $('#ratingFilter').on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
//        category = $(this).attr('data-category');
        var score = $('#ratingFilter').hasClass('active') ? 1 : 0;
        filter(url, category, score, null);
    });

    $('#clearFilters').on('click', function (e) {
        e.preventDefault();
        filter(url, null, null, null);
        $('#ratingFilter').removeClass('active');
        $('.filter_articles').removeClass('active');
    });
    var more = $('.more');
    $('.event_star_list').on('click','#more', function (e) {
        e.preventDefault();
        var counter =  $('.event_star_list').attr('data-counter');
        counter++;
        console.log(counter)
        $('.event_star_list').attr('data-counter', counter);
        var more_url = $('#more').attr('data-url') + '/' + randomByLength(1, 3);
        $.get(more_url, {counter: counter}, function (data) {
//            $('#more').remove();
            $('#over').remove();
            $('.pagination_ajax.text-center').before(data);
            $('.star').raty({
                starOff: '/images/raty/star-off-big.png',
                starOn: '/images/raty/star-on-big.png',
                starHalf: '/images/raty/star-half-big.png',
                round: { down: .26, half: .6, up: .9 },
                width: 150,
                half: true,
                noRatedMsg: $('#advance_options').attr('data-no-rating-hint'),
                hints: ['', '', '', '', ''],
                readOnly: function () {
                    return $(this).attr('data-read-only') == 1;
                },
                score: function () {
                    return $(this).attr('data-score');
                }
            });
            if ($('#over').attr('data-over') == 1) {
                $('#more').remove();
                $('.data-over').removeClass('hidden');
            }

        });
    });

});

function randomByLength(min, max) {
    var a = 0;
    while (!a || a.toString().length < min)
        a = parseInt(Math.random().toString().substr(2, max));
    return a;
}

function filter(url, category, score, counter) {
    $.get(url, {category: category, score: score, counter: counter}, function (data) {
        $('.event_star_list').html(data);
        $('.star').raty({
            starOff: '/images/raty/star-off-big.png',
            starOn: '/images/raty/star-on-big.png',
            starHalf: '/images/raty/star-half-big.png',
            round: { down: .26, half: .6, up: .9 },
            width: 150,
            half: true,
            noRatedMsg: $('#advance_options').attr('data-no-rating-hint'),
            hints: ['', '', '', '', ''],
            readOnly: function () {
                return $(this).attr('data-read-only') == 1;
            },
            score: function () {
                return $(this).attr('data-score');
            }
        });
    });
}

function searchElement(category, element) {
    for (var i = 0; i < category.length; i++) {
        if (category[i] == element) {
            return i;
        }
    }
}