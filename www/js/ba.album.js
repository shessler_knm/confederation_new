$(function () {
    $('#add_photo').on('click', function (e) {
        e.preventDefault();
        $('#myfile').click();

    });
    $('#myfile').change(function () {
        $('.progress').removeClass('hidden');
        $('#upload').click();
    });
    $('#submit_result').on('click', function (e) {
        e.preventDefault();
        var fat = calc();
        if (fat) {
            if (fat < 0 || fat > 85) {
                alert('Ошибка вычислений');
            } else {
                $('input[name=fat]').val(fat);
                $.ajax({
                    type: "POST",
                    url: $('#userImage').attr('data-result'),
                    data: $('#userImage').serialize(),
                    success: function () {
                        disablePopup();
                        $('#edit_photo').css('display','none');
                        $('.progress').addClass('hidden');
                        $('#add_photo').css('display','inline');
                        refresh_result();
                    }
//                dataType: dataType
                });
//                disablePopup();
            }
        } else {
            alert('Неопределен жир. Проверьте пол и дату рождения.');
        }
    });

    $('.remove_result').on('click', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        $(this).parents('li').remove();
        $.get($('.photo_result').attr('data-remove'), {'id': id})
    });

    var bar = $('.bar');
    var percent = $('.percent');
    var status = $('#status');

    $('#album_picture').ajaxForm({
        beforeSend: function () {
            status.empty();
            var percentVal = '0%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        uploadProgress: function (event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        success: function () {
            var percentVal = '100%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        complete: function (tr) {
            var response = JSON.parse(tr.responseText);
            $('#edit_photo').html('<img src="' + response.src + '" style="width:75px">');
            $('#edit_photo').css('display','inline');
            $('#add_photo').css('display','none');
            $('input[name=file]').val(parseInt(response.file));
            $("#submit_result").removeAttr("disabled");
        }
    });
    var nowTemp = new Date();
    var adv_form = $('#advanced_form');
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var date_start = new Date(adv_form.attr('data-year'), adv_form.attr('data-month') - 1, adv_form.attr('data-day'), 0, 0, 0, 0);
    var datepicker = $('#changeDate').datepicker({
        startDate: date_start,
        endDate: now,
        language: $('#changeDate').attr('data-lang')
//Ориентация датапикера зависит от размера контейнера.
//Чем больше размер, тем выше вероятность того, что датапикер влезет в это пространство, иначе будет тупить
//        orientation: "right"
    }).on('changeDate', function (i) {
            var newDate = new Date(i.date);
            var d = newDate.getDate();
            var m = newDate.getMonth();
            var y = newDate.getFullYear();
            var monthName = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
            $("#advanced_form").val($.datepicker.formatDate(addZero(d) + "." + addZero((m + 1)) + "." + y, newDate));
            $("#changeDate").text($.datepicker.formatDate(addZero(d) + "." + addZero((m + 1)) + "." + y, newDate));
            $('#changeDate').datepicker('hide');
        });

});


//0 means disabled; 1 means enabled;
var popupStatus = 0;

//loading popup with jQuery magic!
function loadPopup() {
    //loads popup only if it is disabled
    if (popupStatus == 0) {
        $("#backgroundPopup").css({
            "opacity": "0.7"
        });
        $("#backgroundPopup").fadeIn("slow");
        $("#popup").fadeIn("slow");
        popupStatus = 1;
    }
}

//disabling popup with jQuery magic!
function disablePopup() {
    //disables popup only if it is enabled
    if (popupStatus == 1) {
        $("#backgroundPopup").fadeOut("slow");
        $("#popup").fadeOut("slow");
        popupStatus = 0;
    }
}

//centering popup
function centerPopup() {
    //request data for centering
    var windowWidth = document.documentElement.clientWidth;
    var windowHeight = document.documentElement.clientHeight;
    var popupHeight = $("#popup").height();
    var popupWidth = $("#popup").width();
    //centering
    $("#popup").css({
        "position": "fixed",
        "top": windowHeight / 2 - popupHeight / 2,
        "left": windowWidth / 2 - popupWidth / 2
    });
    //only need force for IE6

    $("#backgroundPopup").css({
        "height": windowHeight
    });

}


//CONTROLLING EVENTS IN jQuery
$(document).ready(function () {

    //LOADING POPUP
    //Click the button event!
    $("#add_result").click(function (e) {
        e.preventDefault();
        //centering with css
        centerPopup();
        //load popup
        loadPopup();
    });

    //CLOSING POPUP
    //Click the x event!
    $("#popupClose").click(function () {
        disablePopup();
    });
    //Click out event!
    $('#popup').click(function (event) {
        if($(event.target)[0].outerHTML==$('#popup')[0].outerHTML){
            disablePopup();
        }
    });
    //Press Escape event!
    $(document).keypress(function (e) {
        if (e.keyCode == 27 && popupStatus == 1) {
            disablePopup();
        }
    });

});

function calc() {

    var age = parseInt($('input[name=age]').val());
    var weight = parseInt($('input[name=weight]').val());
    var chest = parseInt($('input[name=chest]').val());
    var stomach = parseInt($('input[name=stomach]').val());
    var haunch = parseInt($('input[name=haunch]').val());
    var triceps = parseInt($('input[name=triceps]').val());
    var blade = parseInt($('input[name=blade]').val());
    var ilium = parseInt($('input[name=ilium]').val());
    var armpit = parseInt($('input[name=armpit]').val());
    var error = [];

    if (!weight) {
        error.push('weight');
    }
    if (!chest) {
        error.push('chest');
    }
    if (!stomach) {
        error.push('stomach');
    }
    if (!haunch) {
        error.push('haunch');
    }
    if (!triceps) {
        error.push('triceps');
    }
    if (!blade) {
        error.push('blade');
    }
    if (!ilium) {
        error.push('ilium');
    }
    if (!armpit) {
        error.push('armpit');
    }

    if (error.length > 0) {
        return error;
    }

    var y = 495;
    var z = 450;
    var bd = null;
    var m = chest + stomach + haunch + triceps + blade + ilium + armpit;
    var n = Math.pow(m, 2);
    console.log(m);
    var sex = parseInt($('input[name=sex]').val());
    if (sex == 1) {
        bd = 1.1120 - (0.00043499 * m) + (0.00000055 * n) - (0.00028826 * age);
    } else {
        bd = 1.0970 - (0.00046971 * m) + (0.00000056 * n) - (0.00012828 * age);
    }

    var perfat = (y / (bd)) - z;
    var pleanmass = 1 - (perfat / 100);

    var remain = perfat % 10;
    var bfint = Math.round(perfat);
    remain = perfat - bfint;
    var suff = Math.round(remain * 10);
    var finbf = bfint + suff / 10;

    var leanmass = pleanmass * weight;
    var remain2 = leanmass % 10;
    var lmint = Math.round(leanmass);
    remain2 = leanmass - lmint;
    var suff2 = Math.round(remain * 10);
    var finlm = lmint + suff2 / 10;
    if (finbf < 0) {
        alert('Ошибка вычислений. Отрицательное значение');
    } else if (finbf > 85) {
        alert('Ошибка вычислений. Большое значение');
    } else {
        return finbf;

    }

}

function refresh_result() {
    $.get($('.photo_result').attr('data-refresh'), function (data) {
        if (data == 'error') {
            $('#add_result').before($('.photo_result').attr('data-error'));
        } else {
            console.log('1');
            $('#before_data').before(data);
        }
    });
}

function addZero(i) {
    return ("" + i).length == 1 ? "0" + i : i;
}