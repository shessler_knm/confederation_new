$(function () {

    var more = $('.more');
    var counter = more.attr('data-counter');
    var id = more.attr('data-id');
    var delete_event = $('.delete_event');
    var delete_url = delete_event.attr('data-url');

    delete_event.on('click', function (i) {
        i.preventDefault();
        if (confirm($('#confirm').text())) {
            var self = $(this);
            $.get(delete_url, {'id': self.attr('data-id')}, function (data) {
                if (data) {
                    if (data == 'success' || data == 'hide') {
                        self.parents('li').remove();
                    }
                }
            });
        }
    });

    more.on('click', function (e) {
        e.preventDefault();
        counter++;
        more.attr('data-counter', counter);
        var url = $('.event_privatecoach_list').attr('data-url') + '/' + id + '/' + randomByLength(1, 3) + '?i=' + counter;
        $.get(url, function (data) {
            $('#over').remove();
            if (data) {
                more.parent('div').before(data);
                $('.delete_event').on('click', function (i) {
                    i.preventDefault();
                    var delete_event = $('.delete_event');
                    var delete_url = delete_event.attr('data-url');
                    if (confirm($('#confirm').text())) {
                        var self = $(this);
                        $.get(delete_url, {'id': self.attr('data-id')}, function (data) {
                            if (data) {
                                if (data == 'success' || data == 'hide') {
                                    self.parents('li').remove();
                                }
                            }
                        });
                    }
                });
            }
            $('.img-circle img').each(resizeImg)
//            $('mask55').one('load', resizeImg());
            if ($('#over').attr('data-over') == 1) {
                more.parent().remove();
                $('.data-over').removeClass('hidden');
            }
        });
    });
});

function randomByLength(min, max) {
    var a = 0;
    while (!a || a.toString().length < min)
        a = parseInt(Math.random().toString().substr(2, max));
    return a;
}

resizeImg = function () {
    var img = $(this);
    if (img.attr('src') == undefined) return;
    if (img.data('changed')) {
        return;
    }
    var imgW = img.width();
    var imgH = img.height();
    var block = img.parents('.img-circle');
    var blockW = block.width();
    var blockH = block.height();
    if (!img.is(':visible')) {
        var c = new Image();
        c.onload = function () {
            imgW = c.width;
            imgH = c.height;
            if (blockW > 0) {
                changeAttr();
            }
        };

        c.src = img.attr('src');
    } else {
        if (blockW > 0) {
            changeAttr();
        }
    }

    function changeAttr() {
        if (imgW > imgH) {
            img.height(blockH);
            var newW = blockH / imgH * imgW;
            img.css('margin-left', (blockW - newW) / 2);
        }
        else {
            img.width(blockW);
            var newH = blockW / imgW * imgH;
            console.log('newH:' + newH);
            img.css('margin-top', (blockH - newH) / 2);
        }
        img.data('changed', true);
    }


};
