$(function () {
    function getCenterAction(lat, lng, zoom, myCallback) {
        var CityAction = new ymaps.map.action.Single({
            center: [lat, lng],
            zoom: zoom,
            callback: myCallback
        });
        return CityAction;
    }

    var object = $('#section_object').val();
    $('#section_object').focusin(function () {
        if ($('#section_object').val() == object) {
            $('#section_object').val('');
        }
    });
    $('#section_object').focusout(function () {
        if ($('#section_object').val() == '') {
            $('#section_object').val(object);
        }
    });


    ymaps.ready(function () {
        // Создание экземпляра карты
        var myMap = new ymaps.Map('map', {
            center: [48.905082, 68.854065],
            zoom: 5
        });
        myMap.controls.add(
            new ymaps.control.ZoomControl()
        );
        myMap.setType('yandex#publicMap');
//        myMap.behaviors.enable('scrollZoom');

        // Загрузка ymapsML-файла
        var currentTime = new Date();
        var citySelect = $('#city_select').val();
        var currentMinutes = currentTime.getMinutes();
        ymaps.geoXml.load('http://cnfd.kz/box/section/xmlgeoobject?id=' + citySelect + '&min=' + currentMinutes)
            .then(function (res) {
                myMap.geoObjects.add(res.geoObjects); // Добавление геообъектов на карту
            }, function (error) {   // Вызывается в случае неудачной загрузки ymapsML
                console.log('Ошибка: ' + error);
            });

        var myCallback = function (err) {
            if (err) {
                throw err;
            }
        };
        $('#submit').on('click', function () {
            citySelect = $('#city_select').val();
            $('#lng').val(citySelect);
            $('#lat').val(citySelect);
            city = $('#place option:selected').text();
            var zoom = 11;
            var CityMap = getCenterAction(parseFloat($('#lat option:selected').text()), parseFloat($('#lng option:selected').text()), zoom, myCallback);
            var city_id = $('#city_select :selected').val();
            var sport_id = $('#sport_select :selected').val();
            var q = $('#section_object').val();
            var fed_id = $('.fed_id').val();
            var list = $('.sections_list');
            if (!q) {
                if ($('#city_select option:selected').val() != "0") {
                    myMap.action.execute(CityMap);
                }
                $.get(list.attr('data-url') + '/' + city_id + '/' + sport_id, {'t': (new Date()).getTime()}, function (data) {
                    $('.sections_list').html(data);
                });
            }
            else {
                $.get(list.attr('data-url') + '/' + city_id + '/' + sport_id, {'section_object': q, 't': (new Date()).getTime()}, function (data) {
                    list.html(data);
                    var lat = parseFloat(list.find('li.media').attr('data-lat'));
                    var lng = parseFloat(list.find('li.media').attr('data-lng'));
                    zoom = 17;
                    var act = getCenterAction(lat, lng, zoom);
                    if (lat && lng && ($('#city_select option:selected').val() != "0")) {
                        myMap.action.execute(act);
                    }
                });
            }
        });
        $.get($('.sections_list').attr('data-url'), {'t': (new Date()).getTime()}, function (data) {
            $('.sections_list').html(data);
        });
    });
    $('.sections_list').on('click', '.my_pagination a', function (e) {
        var self = $(this);
        e.preventDefault();
        $.get(self.attr('href'), {'t': (new Date()).getTime()}, function (data) {
            $('.sections_list').html(data);
        });

    });

});