$(function () {
    var left = $('.nav_slider.prev');
    var next = $('.nav_slider.next');
    var iID = false;
    var mouseStillDown = false;

    var clear_interval = function () {
        clearInterval(iID);
        iID = false;
        mouseStillDown = false;
    }

    var turn_slide = function (op) {
        if (!mouseStillDown) {
            clear_interval();
            return;
        }
        var wrapper = $('.program_item_wrap'),
            margin = parseInt(wrapper.css('margin-left')),
            val,
            totalWidth = 0;
        $('.program_item').each(function () {
            totalWidth += $(this).width();
        });
        val = op ? margin + 5 : margin - 5;

        var diff = totalWidth - $('.program_item').width() - 5;
        if (diff < 0) {
            clear_interval();
            return;
        }

        if (op) {
            margin += diff;
        }
        if (margin < 0) margin *= -1;

        if (margin <= diff) {
            wrapper.css('margin-left', val + "px");
        } else {
            clear_interval();
        }
    }

    var go = function (op) {
        mouseStillDown = true;
        if (iID === false) {
            iID = setInterval(function () {
                turn_slide(op);
            }, 5);
        }
    }


    left.on("mousedown", function (ev) {
        go(true);
    });
    left.on("mouseout mouseup", function (ev) {
        if (mouseStillDown) {
            mouseStillDown = false;
        }
    });
    next.on("mousedown", function (ev) {
        go(false);
    });
    next.on("mouseout mouseup", function (ev) {
        if (mouseStillDown) {
            mouseStillDown = false;
        }
    });

    if ($('.gallery_page_program').attr('data-id')) {
        var program_id = $('.gallery_page_program').attr('data-id');
    }else{
        var program_id = $('.program_results').attr('data-id');
    }

        var url = $('.gallery_page_program').attr('data-content') + '/' + program_id;
        $.get(url, function (data) {
            if (data) {
                $('.program_logs').html(data);
                $('.program_results[data-id="' + program_id + '"]').addClass('active');
                $('.program_photo').on('click', function (e) {
                    e.preventDefault();
                    $('.photo').css('background-image', 'url('+$(this).attr('href')+')');
                    $('.date').html($(this).attr('data-date'));
                    $('.weight').html($(this).attr('data-weight'));
                    if ($(this).attr('data-fat')) {
                        $('.fat').html($(this).attr('data-fat'));
                    }
                    $('.program_log').attr('data-index', $(this).attr('data-index'));
                });
                $('.photo_slide').on('click', function (e) {
                    e.preventDefault();
                    var maxSlides = parseInt($('.program_log').attr('data-max-slides'));
                    if ($(this).attr('data-slide') == "prev") {
                        var index = parseInt($('.program_log').attr('data-index')) - 1;
                        if (index == 0) {
                            index = maxSlides;
                        }
                    } else {
                        var index = parseInt($('.program_log').attr('data-index')) + 1;
                        if (index > maxSlides) {
                            index = 1;
                        }

                    }
                    $('.photo').css('background-image', 'url('+$('.program_photo[data-index=' + index + ']').attr('href')+')');
                    $('.date').html($('.program_photo[data-index=' + index + ']').attr('data-date'));
                    $('.weight').html($('.program_photo[data-index=' + index + ']').attr('data-weight'));
                    if ($('.program_photo[data-index=' + index + ']').attr('data-fat')) {
                        $('.fat').html($('.program_photo[data-index=' + index + ']').attr('data-fat'));
                    }
                    $('.program_log').attr('data-index', $('.program_photo[data-index=' + index + ']').attr('data-index'));
                })

            }
        });


    $('.program_results').on('click', function (e) {
        e.preventDefault();
        var result = $(this);
        var url = $('.gallery_page_program').attr('data-content') + '/' + $(this).attr('data-id');
        $.get(url, function (data) {
            if (data) {
                $('.program_logs').html(data);
                $('.program_results').removeClass('active');
                result.addClass('active');
                $('.program_photo').on('click', function (e) {
                    e.preventDefault();
                    console.log($(this).attr('href'))
                    $('.photo').css('background-image', 'url('+$(this).attr('href')+')');
                    $('.date').html($(this).attr('data-date'));
                    $('.weight').html($(this).attr('data-weight'));
                    if ($(this).attr('data-fat')) {
                        $('.fat').html($(this).attr('data-fat'));
                    }
                    $('.program_log').attr('data-index', $(this).attr('data-index'));
                });
                $('.photo_slide').on('click', function (e) {
                    e.preventDefault();
                    var maxSlides = parseInt($('.program_log').attr('data-max-slides'));
                    if ($(this).attr('data-slide') == "prev") {
                        var index = parseInt($('.program_log').attr('data-index')) - 1;
                        if (index == 0) {
                            index = maxSlides;
                        }
                    } else {
                        var index = parseInt($('.program_log').attr('data-index')) + 1;
                        if (index > maxSlides) {
                            index = 1;
                        }

                    }
                    $('.photo').css('background-image', 'url('+$('.program_photo[data-index=' + index + ']').attr('href')+')');
                    $('.date').html($('.program_photo[data-index=' + index + ']').attr('data-date'));
                    $('.weight').html($('.program_photo[data-index=' + index + ']').attr('data-weight'));
                    if ($('.program_photo[data-index=' + index + ']').attr('data-fat')) {
                        $('.fat').html($('.program_photo[data-index=' + index + ']').attr('data-fat'));
                    }
                    $('.program_log').attr('data-index', $('.program_photo[data-index=' + index + ']').attr('data-index'));
                })

            }
        });
    });

});