$(function(){
    var points=null;
    var container=$('.mypoint_section');
    var columns=container.find('ul');

    function nextUl(){
        var min=columns.first();
        columns.each(function(i,u){
            u=$(u);
            if (u.height()<min.height()){
                min=u;
            }
        });
        return min;
    }
    $.getJSON(container.attr('data-src'),function(data){
        for (var i in data){
            var el=$(data[i]);
            el.hide();
            nextUl().append(el);
            el.fadeIn(500);
        }
    });
});