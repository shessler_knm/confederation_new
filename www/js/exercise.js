$(function () {
    var object = $('#search').val();
    $('#search').focusin(function () {
        if ($('#search').val() == object) {
            $('#search').val('');
        }
    });
    $('#search').focusout(function () {
        if ($('#search').val() == '') {
            $('#search').val(object);
        }
    });

    $('#clearFilters').on('click', function (e) {
        e.preventDefault();
        $('#goals').val(0);
        $('#place').val(0);
        $('#muscles').val(0);
        $('#sessions').val(0);
        $('#search').val($('#search').attr('data-default'));
        $('.customSelectInner').text('');
    });

    $('#submit').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: $('#submit').attr('data-url'),
            data: $('#exercise_filter').serialize(),
            success: function (data) {
                $('.news_list').html(data);
                var star = $('.star');
                star.raty({
                    starOff: '/images/raty/star-off-big.png',
                    starOn: '/images/raty/star-on-big.png',
                    starHalf: '/images/raty/star-half-big.png',
                    round: { down: .26, half: .6, up: .9 },
                    width: 150,
                    half: true,
                    noRatedMsg: $('#advance_options').attr('data-no-rating-hint'),
                    hints: ['', '', '', '', ''],
                    readOnly: function () {
                        return $(this).attr('data-read-only') == 1;
                    },
                    score: function () {
                        return $(this).attr('data-score');
                    }
                });
            }
        });
    });

    $.ajax({
        type: "POST",
        url: $('#submit').attr('data-url'),
        data: $('#exercise_filter').serialize(),
        success: function (data) {
            $('.news_list').html(data);
            var star = $('.star');
            star.raty({
                starOff: '/images/raty/star-off-big.png',
                starOn: '/images/raty/star-on-big.png',
                starHalf: '/images/raty/star-half-big.png',
                round: { down: .26, half: .6, up: .9 },
                width: 150,
                half: true,
                noRatedMsg: $('#advance_options').attr('data-no-rating-hint'),
                hints: ['', '', '', '', ''],
                readOnly: function () {
                    return $(this).attr('data-read-only') == 1;
                },
                score: function () {
                    return $(this).attr('data-score');
                }
            });
        }

    });

    $('.exercise_list').on('click', '.my_pagination a', function (e) {
        var self = $(this);
        e.preventDefault();
        $.get(self.attr('href'), {'t': (new Date()).getTime()}, function (data) {
            $('.exercise_list').html(data);
            window.location.href='#back';
            var star = $('.star');
            star.raty({
                starOff: '/images/raty/star-off-big.png',
                starOn: '/images/raty/star-on-big.png',
                starHalf: '/images/raty/star-half-big.png',
                round: { down: .26, half: .6, up: .9 },
                width: 150,
                half: true,
                noRatedMsg: $('#advance_options').attr('data-no-rating-hint'),
                hints: ['', '', '', '', ''],
                readOnly: function () {
                    return $(this).attr('data-read-only') == 1;
                },
                score: function () {
                    return $(this).attr('data-score');
                }
            });
        });

    });
});