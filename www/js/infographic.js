$(function () {
    $(".infographic").fancybox({
        helpers: {
            title: {
                type: 'outside',
                position: 'bottom'
            }
        },
        "fitToView": false
    });
});