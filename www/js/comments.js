$(function () {

    $(document).on('click', '.add_comment', function (e) {
        e.preventDefault();
        var self = $(this);
        var container = self.parents('.comments_section');
        var form = $('.comment_form');
        var sending = false;
        form.show()
        self.hide();
        form.on('submit.comment', function (e) {
            e.preventDefault();
            if (sending) {
                return false;
            }
            sending = true;
            var data = form.serialize();
            $.ajax({
                type:'POST',
                url:form.attr('action'),
                data:data,
                success:function (d) {
                    d = $(d);
                    sending = false;
                    form.find('textarea').val('');
                    form.hide();
                    self.show();
                    container.find('.comments.toplevel').append($(d));
                    $("html, body").animate({ scrollTop:d.offset().top }, "slow");
                    form.unbind('.comment');
                },
                error:function () {
                    sending = false;
                },
                dataType:'html'
            });

        });

        form.on('reset.comment', function () {
            form.unbind('.comment');
            form.hide();
            self.show();
        });

    })

    $(document).on('click', '.delete_comment', function (e) {
        e.preventDefault();
        var self = $(this);
        var id = $(this).attr('data-id');
        $.post($('.delete_comment').attr('data-url'), {id:id}, function (data) {
            self.parent('div').find('.txt').html("<p>" + data + "</p>");
        });
    });

    $(document).on('click', '.comments_section .link.ans', function (e) {
        e.preventDefault();
        var self = $(this);
        var container = self.parents('.comments_section');
        var sending = false;
        var form = $('.comment_form').first().clone();
        form.find('input[name="parent_id"]').val(self.attr('data-id'));
        form.find('textarea').css('width', '95%');
        self.after(form);
        form.show()
        self.hide();
        form.on('submit.comment', function (e) {
            e.preventDefault();
            if (sending) {
                return false;
            }
            sending = true;
            var data = form.serialize();
            $.ajax({
                type:'POST',
                url:form.attr('action'),
                data:data,
                success:function (d) {
                    sending = false;
                    d = $(d);
                    form.find('textarea').val('');
                    form.remove();
                    self.show();
                    var parent = self.parent().parent();
//                    var comments = parent.find('>.comments');
//                    if (comments.length == 0) {
//                        comments = $('<div class="comments"></div>');
//                        parent.append(comments);
//                    }
                    var level=parseInt(self.attr('data-level'))+1;
                    d.removeClass('level1').addClass('level'+level);
                    d.find('.link').attr('data-level',level);
                    parent.append(d);
                    $("html, body").animate({ scrollTop:d.offset().top }, "slow");
//                    form.unbind('.comment');
                },
                error:function () {
                    sending = false;
                },
                dataType:'html'
            });

        });

        form.on('reset.comment', function () {
            form.remove();
            self.show();
        });

    });

});