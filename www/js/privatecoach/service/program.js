angular.module('privatecoach').service('Program',function($resource,$rootScope,USER_ID,CUR_USER_ID){
    var Program=$resource(BASE_URL+LANG + '/api/program/:id',{'id':'@id'});
    var Log=$resource(BASE_URL+LANG + '/api/ProgramLog/:id',{'id':'@id'});
    this.factory=function(id,callback){
        return new Program.get({'id':id},callback);
    };

    this.my=function(user_id,cb) {
        return Program.query({'t':(new Date).getTime(),'user_id':user_id},cb);
    };

    this.myLog=function(user_id,cb,sd,ed) {
        if (sd && ed) {
            return Log.query({'user_id':user_id,'start_date':sd,'end_date':ed,'t':(new Date).getTime()},cb);
        } else {
            return Log.query({'user_id':user_id,'t':(new Date).getTime()},cb);
        }
    };

    function saveLog(ex) {
        var r=Log.query({'program_id': ex.program_id, 'exercise_id': ex.exercise_id, 'date': ex.date}, function () {
            if (r.length>0){
                var m= r.pop();
                if (m.id) {
                    m.status = ex.flag ? 1 : 0;
                    m.$save();
                }
            }  else {
                m = new Log();
                m.exercise_id = ex.exercise_id;
                m.program_id = ex.program_id;
                m.date = ex.date;
                m.status = ex.flag ? 1 : 0;
                m.$save();
            }

        });
    }

    $rootScope.$on('exerciseCompleted',function(e,ex){
        saveLog(ex);
    });

    $rootScope.$on('exerciseUncompleted',function(e,ex){
        saveLog(ex);
    });
});