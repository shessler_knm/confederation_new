angular.module('privatecoach').service('Calendar',function(){
    var YEAR_LEN=365,
        WEEK_LEN= 7,
        MONTH_LEN=[
            31,28,31,30,31,30,31,31,30,31,30,31
        ],
        MONTH_NAME=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
        WEEK_NAME=['Mon','Tue','Wen','Thu','Fri','Sut','Sun'];

    //конвертирует дату в количество дней прошедших от РХ
    function countDays(year,month,day){
        var msum=0;
        if (month>1){
            for (var i=0;i<month-1;i++){
                msum+=MONTH_LEN[i];
            }
        }
        var dsum=year*YEAR_LEN+Math.floor(year/4)+msum+day-YEAR_LEN-1;
        return dsum;
    }
    //Преобразует количество дней прошедших от РХ в дату
    function convertDaysToDate(days){
        var year=Math.floor((days+YEAR_LEN+1)/(YEAR_LEN*4+1))*4+(Math.floor(((days+YEAR_LEN+1)%(YEAR_LEN*4+1))/(YEAR_LEN)));
        var dd=(days+YEAR_LEN+1)%(YEAR_LEN*4+1)%YEAR_LEN;
        var msum=0;
        var month=1;
        var day=1;
        for (var i=0; i< MONTH_LEN.length;i++){
            msum+=MONTH_LEN[i];
            if (dd<=msum){
                month=i+1;
                msum-=MONTH_LEN[i];
                day=dd-msum;
                break;
            }
        }
        return {'year':year,'month':month,'day':day};
    }

    function getWeekDay(year,month,day){
        var wd=countDays(year,month,day)%7;
        return wd==0?7:wd;
    }

    function prevMonthLen(month,year){
        var prev=month-1;
        if (month==1){
            prev=12;
        }
        var len = MONTH_LEN[prev - 1];
        if (prev==2 && isLeapYear(year)){
            len++;
        }
        return  len;
    }

    function nextMonthLen(month,year){
        var next=month+1;
        if (month==12){
            next=1;
        }
        var len = MONTH_LEN[next - 1];
        if (next==2 && isLeapYear(year)){
            len++;
        }
        return  len;
    }

    function isLeapYear(year){
        return !(year)%4;
    }

    function monthLen(month,year){
        var len=MONTH_LEN[month-1];
        if (month==2 && isLeapYear(year)){
            len++;
        }
        return len;
    }

    function makeDayArray(year,month){
        var fd=getWeekDay(year,month,1);
        var mlen = monthLen(month,year);
        var ld=getWeekDay(year,month, mlen);
        var darr=[];
        var week=[];
        if (fd!=1){
            var prev=prevMonthLen(month,year);
            for (var k=1;k<fd;k++){
                week.push(prev-(fd-k-1));
            }
        }
        for (var d=1;d<=mlen;d++){
            week.push(d);
            if (week.length==7){
                darr.push(week);
                week=[];
            }
        }
        if (ld!=7){
//            var next=nextMonthLen(month);
            for (var j=ld;j<7;j++){
                week.push(j-ld+1);
            }
            darr.push(week)
        }
        return darr;
    }

    this.getByDate=function(year,month){
        return makeDayArray(year,month);
    };

    this.getWeekPeriod=function(year,month,day){
        var wd=getWeekDay(year,month,day);
        var sd=countDays(year,month,day)-wd+1;
        var ed=sd+6;
        return {"start":convertDaysToDate(sd),"end":convertDaysToDate(ed)};
    };

    this.getWeekDay=function(year,month,day){
        return getWeekDay(year,month,day);
    };

    this.dayNum=function(year,month,day){
        return countDays(year,month,day);
    };

    this.getDateByNum=function(n){
        return convertDaysToDate(n);
    }


});