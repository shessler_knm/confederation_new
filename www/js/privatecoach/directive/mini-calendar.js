angular.module('privatecoach').directive('miniCalendar',function(Calendar){
    return {
        template:'<div class="datepicker">'+
                    '<div class="datepicker-days">'+
                '<table class=" table-condensed">'+
                    '<thead>'+
                        '<tr>'+
                            '<th class="prev"><a href="#" ng-click="prevMonth($event)" title="Назад">&larr;</a></th>'+
                            '<th colspan="5" class="switch">{{ monthName[month] }} {{ year }}</th>'+
                            '<th class="next"><a href="#" ng-click="nextMonth($event)" title="Вперед">&rarr;</a></th>'+
                        '</tr>'+
                        '<tr>'+
                            '<th class="dow" ng-repeat="name in weekName">{{ name }}</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody>'+
                        '<tr ng-repeat="w in days">'+
                            '<td class="{{ getClass(d,$index+$parent.$index*7,$index+1) }}" ng-repeat="d in w">{{ d }}</td>'+
                        '</tr>'+

                    '</tbody>'+
                '</table>'+
                '</div>'+
            '<div class="datepicker-months" style="display: none;">'+
                '<table class="table-condensed">'+
                    '<thead>'+
                        '<tr>'+
                            '<th class="prev">‹</th>'+
                            '<th colspan="5" class="switch">2012</th>'+
                            '<th class="next">›</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody>'+
                        '<tr>'+
                            '<td colspan="7"><span class="month">Jan</span><span class="month active">Feb</span><span'+
                            'class="month">Mar</span><span class="month">Apr</span><span'+
                            'class="month">May</span><span class="month">Jun</span><span'+
                            'class="month">Jul</span><span class="month">Aug</span><span'+
                            'class="month">Sep</span><span class="month">Oct</span><span'+
                            'class="month">Nov</span><span class="month">Dec</span></td>'+
                        '</tr>'+
                    '</tbody>'+
                '</table>'+
            '</div>'+
            '<div class="datepicker-years" style="display: none;">'+
                '<table class="table-condensed">'+
                    '<thead>'+
                        '<tr>'+
                            '<th class="prev">‹</th>'+
                            '<th colspan="5" class="switch">2010-2019</th>'+
                            '<th class="next">›</th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody>'+
                        '<tr>'+
                            '<td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span'+
                            'class="year">2011</span><span class="year active">2012</span><span'+
                            'class="year">2013</span><span class="year">2014</span><span'+
                            'class="year">2015</span><span class="year">2016</span><span'+
                            'class="year">2017</span><span class="year">2018</span><span'+
                            'class="year">2019</span><span class="year old">2020</span></td>'+
                        '</tr>'+
                    '</tbody>'+
                '</table>'+
                '</div>'+
            '</div>',
        controller:function($scope,Calendar,$filter){
            var dt=new Date();
            $scope.monthName=['','Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];
            $scope.weekName=['Пн','Вт','Ср','Чт','Пт','Сб','Вс'];
            $scope.recalc=function(){
                $scope.days=Calendar.getByDate($scope.year,$scope.month);
            };
            $scope.$watch('year',$scope.recalc);
            $scope.$watch('month',$scope.recalc);
            $scope.$watch('day',$scope.recalc);

            function getDate(y,m,d){
               return ""+y + addZero(m) + addZero(d);
            }

            function addZero(i){
                return (""+i).length==1?"0"+i:i;
            }

            $scope.getClass=function(d,index,weekD){
                var clasess=['day'];
                if (d==dt.getDate() && $scope.month==dt.getMonth()+1 &&  $scope.year==dt.getFullYear()){
                    clasess.push('active');
                }
                var isPrev = (d - index) > 20;
                if (isPrev){
                    clasess.push('old');
                }
                var isNext = (d - index) < -20;
                if (isNext){
                    clasess.push('old')
                }

                var f=$filter('filter');
                $scope.weekDays=f($scope.weekDays,function(val){
                    return !d[val.value];
                });

                if ($scope.programs && $scope.programs.length>0){
                    for (var i=0;i<$scope.programs.length;i++){
                        var p = $scope.programs[i];
                        var found=f(p.days,function(val){
                            return val.value==weekD;
                        });
                        var curDt = getDate($scope.year,$scope.month,d);
                        if (
                            found && found.length>0 //День недели найден
                                && p.activity==1  //Программа активна
                                && p.period.start <= curDt && p.period.end>curDt //Текущая дата внутри периода
                                && (!isPrev || isNext)) //Просматриваемый день относится к текущему месяцу
                        {
                            clasess.push('program'+(i+1));
                        }
                    }
                }

                return clasess.join(' ');
            };

            $scope.nextMonth=function(e){
                e.preventDefault();
                var next=$scope.month+1;
                if (next==13){
                    next=1;
                    $scope.year++;
                }
                $scope.month=next;
            };

            $scope.prevMonth=function(e){
                e.preventDefault();
                var prev=$scope.month-1;
                if (prev==0){
                    prev=12;
                    $scope.year--;
                }
                $scope.month=prev;
            };

        },
        'replace':true,
        'scope':{
            'year':'=',
            'month':'=',
            'day':'=',
            'programs':'='
        }
    };
});