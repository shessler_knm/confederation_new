angular.module('privatecoach').directive('lentaCalendar',function () {
    return {
        'template': '<div class="program_section">' +
            '<div class="program_date">{{ textualPeriod(week) }} <a class="min_nav_slider prev">&larr;</a> <a class="min_nav_slider next">&rarr;</a></div>' +
            '<div class="program_slider">' +
            '<div class="program_inner">' +
            '<div class="program_item_wrap">' +
            '<div class="{{ getClass(item) }}" ng-style="item.style" ng-repeat="item in data">' +
            '<div class="list_checkbox">' +
            '<div class="date">{{ textualDay(item.date.year,item.date.month,item.date.day) }}</div>' +
            '<div class="inner">' +
            '<label ng-repeat="ex in item.list"  p-checkbox readonly="readonly" flag="ex.flag" title="ex.title" ex="ex"></label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<a class="nav_slider prev">&larr;</a>' +
            '<a class="nav_slider next">&rarr;</a>' +
            '</div>' +
            '</div>',
        'controller': function ($scope, Calendar, $filter) {
            switch (LANG) {
                case 'ru':
                    $scope.monthName = ['', 'Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
                    $scope.weekName = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'];
                    break;
                case 'kz':
                    $scope.monthName = ['', "Қаңтар", "Ақпан", "Наурыз", "Сәуір", "Мамыр", "Маусым", "Шілде", "Тамыз", "Қыркүйек", "Қазан", "Қараша", "Желтоқсан"];
                    $scope.weekName = ["Дүйсенбі", "Сейсенбі", "Сәрсенбі", "Бейсенбі", "Жұма", "Сенбі", "Жексенбі"];
                    break;
                case 'en':
                    $scope.monthName = ['', "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    $scope.weekName = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
                    break;
            }
            $scope.week = Calendar.getWeekPeriod($scope.year, $scope.month, $scope.day);
            $scope.data = [];
            var f = $filter('filter');

            function getDate(y, m, d, sep) {
                sep = sep || '';
                return "" + y + sep + addZero(m) + sep + addZero(d);
            }

            function addZero(i) {
                return ("" + i).length == 1 ? "0" + i : i;
            }

            var render = function () {
                if ($scope.logs && $scope.programs && $scope.programs.length > 0) {
                    recalcData(null, function () {
                        if ($scope.data.length == 0) {
                            var next_week = $scope.week;
                            next_week.start.day = $scope.week.end.day + 1;
                            next_week.end.day += 7;
                            recalcData(next_week);
                        } else if ($scope.data.length < 7) {
                            var next_week = $scope.week;
                            next_week.end.day += 7 - $scope.data.length;
                            recalcData(next_week);
                        } else {
                            recalcData()
                        }
                    });

                }
//                if ($scope.data.length<=3&&$scope.data.length>0){
//                    $('.nav_slider').addClass('hidden')
//                }

            };


            function recalcData(nextWeek, callback) {
                var week = nextWeek || $scope.week;
                $scope.data = [];
                var start = Calendar.dayNum(week.start.year, week.start.month, week.start.day),
                    end = Calendar.dayNum(week.end.year, week.end.month, week.end.day);
                for (var n = start; n <= end; n++) {
                    var dt = Calendar.getDateByNum(n);
                    var wd = Calendar.getWeekDay(dt.year, dt.month, dt.day);
                    var todayPrograms = f($scope.programs, function (p) {
                        var found = f(p.days, function (day) {
                            return day.value == wd;
                        });
                        return (found && found.length > 0);
                    });
                    if (todayPrograms && todayPrograms.length > 0) {

                        angular.forEach(todayPrograms, function (p) {
                            if (
                                p.activity == 1 &&
                                    p.period.start <= getDate(dt.year, dt.month, dt.day) &&
                                    p.period.end >= getDate(dt.year, dt.month, dt.day)
                                ) {
                                var item = {'date': dt, 'list': [], 'program_id': p.id, completed: false};
                                angular.forEach(p.days, function (d) {
                                    if (d.value == wd) {
                                        var len = d.exercises.length;
                                        var completed = 0;
                                        angular.forEach(d.exercises, function (ex) {
                                            var logs = f($scope.logs, {'program_id': p.id, 'exercise_id': ex.id, 'date': getDate(dt.year, dt.month, dt.day, '-')})
                                            var flag = 0;
                                            if (logs && logs.length > 0) {
                                                flag = logs[0].status == '1';
                                                completed += flag ? 1 : 0;
                                            }
                                            var efforts = [];
                                            angular.forEach(ex.efforts, function (val) {
                                                efforts.push(val.repeat);
                                            });
                                            var i = {
                                                'title': '<span data-target="#myExercise" data-toggle="modal" class="title exTitle" title="' + $('#hint').attr('data-hint') + '" data-id="' + ex.id + '">' + ex.title + ', ' + efforts.join(" / ") + '</span>',
                                                'flag': flag,
                                                'program_id': p.id,
                                                'exercise_id': ex.id,
                                                'date': getDate(dt.year, dt.month, dt.day, '-')
                                            };
                                            item.list.push(i);
                                        });
                                        item.completed = len == completed;
                                    }
                                });

                                $scope.data.push(item);
                            }

                        });
                    }

                }
                if (callback) {
                    callback();
                }

            }

            $scope.getClass = function (item) {
                var cl = ['program_item'];

                var itemDt = getDate(item.date.year, item.date.month, item.date.day);
                var today = getDate($scope.year, $scope.month, $scope.day);
                if (itemDt == today) {
                    cl.push('today_item');
                } else if (itemDt < today) {
                    cl.push('past_item');
                    if (item.completed) {
                        cl.push('finished_item');
                    } else {
                        cl.push('unfinished_item');
                    }
                }

                cl.push('program' + ($scope.programs.indexOf(f($scope.programs, {'id': item.program_id})[0]) + 1));

                return cl.join(' ');

            };


            $scope.$watch('programs', render, true);
            $scope.$watch('logs', render, true);

            $scope.textualPeriod = function (week) {
                var text = [];
                text.push(week.start.day);
                if (week.start.month != week.end.month) {
                    text.push($scope.monthName[week.start.month].toLowerCase());
                }

                text.push('-');
                text.push(week.end.day);
                text.push($scope.monthName[week.end.month].toLowerCase() + ',');
                text.push(week.end.year);
                return text.join(" ");

            };

            $scope.textualDay = function (year, month, day) {
                var d = Calendar.getWeekDay(year, month, day);
                var text = [];
                text.push($scope.weekName[d - 1] + ',');
                text.push(day, $scope.monthName[month].toLowerCase());
                return text.join(' ');
            };

            $scope.$watch('data', function () {
                setTimeout(function () {
                    window.equalHeight($(".program_slider .program_item"));
                }, 500);
            }, true);

        },
        'link': function (scope, el, attr) {

            var left = el.find('a.prev');
            var next = el.find('a.next');
            var iID = false;
            var mouseStillDown = false;
            var recalcHeight = function () {
                window.equalHeight($(".program_slider .program_item"));
            };
            scope.$watch('programs', recalcHeight, true);
            scope.$watch('logs', recalcHeight, true);

            var calcLimit = function (op, margin, totalWidth) {

            }

            var clear_interval = function () {
                clearInterval(iID);
                iID = false;
                mouseStillDown = false;
            }

            var turn_slide = function (op) {
                if (!mouseStillDown) {
                    clear_interval();
                    return;
                }
                var wrapper = el.find('.program_item_wrap'),
                    margin = parseInt(wrapper.css('margin-left')),
                    val,
                    totalWidth = 0;
                el.find('.program_item').each(function () {
                    totalWidth += $(this).width();
                });
                val = op ? margin + 5 : margin - 5;

                var diff = totalWidth - el.width();

                if (diff < 0) {
                    clear_interval();
                    return;
                }

                if (op) {
                    margin += diff;
                }
                if (margin < 0) margin *= -1;

                if (margin <= diff) {
                    wrapper.css('margin-left', val + "px");
                } else {
                    clear_interval();
                }
            }

            var go = function (op) {
                if (scope.data.length > 3) {
                    mouseStillDown = true;
                    if (iID === false) {
                        iID = setInterval(function () {
                            turn_slide(op);

                        }, 5);
                    }
                }
            }


            left.on("mousedown", function (ev) {
                go(true);
            });
            left.on("mouseout mouseup", function (ev) {
                if (mouseStillDown) {
                    mouseStillDown = false;
                }
            });
            next.on("mousedown", function (ev) {
                go(false);
            });
            next.on("mouseout mouseup", function (ev) {
                if (mouseStillDown) {
                    mouseStillDown = false;
                }
            });
        },
        'replace': true,
        'scope': {
            'year': '=',
            'month': '=',
            'day': '=',
            'programs': '=',
            'logs': '=',
            'readonly': '='
        }
    };
}).directive('pCheckbox', function () {
        return {
            'controller': function ($scope, Calendar, $filter) {
                $scope.day_ex = function () {
                    today = new Date;
                    var dd = today.getDate();
                    var mm = today.getMonth() + 1; //January is 0!
                    var yyyy = today.getFullYear();
                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    today = yyyy + '-' + mm + '-' + dd;
                    if ($scope.ex.date == today) {
                        return "label_checkbox";
                    }else{
                        return "empty"
                    }
                }
            },
            'template': $('#own_program').attr('data-own-profile') ?
                '<label ng-class="{uncompleted:flag==0}"><input type="checkbox" ng-model="flag" ng-checked="flag==1" ng-disabled="readonly">' +
                    '<span ng-bind-html-unsafe="title"></span>' +
                    '<span class="{{day_ex()}}"></span>' +
                    '</label>' :
                '<label ng-class="{uncompleted:flag==0}"><input type="checkbox" ng-model="flag" ng-checked="flag==1" ng-disabled="readonly">' +
                    '<span ng-bind-html-unsafe="title"></span>' +
                    '</label>',
            'scope': {
                'flag': '=',
                'title': '=',
                'ex': '=',
                'readonly': '='
            },
            'replace': true,
            'link': function (scope, el, attr) {
                console.log(scope.ex.date)
                scope.$watch('flag', function (newVal, oldVal) {
                    if (newVal === oldVal) {
                        return;
                    }
                    if (scope.flag == 1) {
//                    el.removeClass('uncompleted');
                        scope.$emit('exerciseCompleted', scope.ex);
                    } else {
//                    el.addClass('uncompleted');
                        scope.$emit('exerciseUncompleted', scope.ex);
                    }
                });
            }
        };
    });