angular.module('privatecoach').controller('scheduleCtrl',function($scope,Calendar,Program,USER_ID,CUR_USER_ID){
    var dt=new Date();
    var user_id=USER_ID;
    $scope.readonly=false;
    if (CUR_USER_ID!='undefined'){
        user_id=CUR_USER_ID;
    }
    if (USER_ID!=user_id){
        $scope.readonly=true;
    }
    $scope.days=Calendar.getByDate(2013,8);
    Program.my(user_id,function(d){
        $scope.programs=d;
    });
    Program.myLog(user_id,function(d){
        $scope.logs=d;
    });

//    $scope.$on('exerciseCompleted',function(){
//        Program.myLog(function(d){
//            $scope.logs=d;
//        });
//    });

    $scope.dt={'year':dt.getFullYear(),'month':dt.getMonth()+1,'day':dt.getDate()};
    $scope.dtLenta={'year':dt.getFullYear(),'month':dt.getMonth()+1,'day':dt.getDate()};
    $scope.changeActivity=function(m){
        m.activity=m.activity==1?0:1;
        m.$save();
    }
});