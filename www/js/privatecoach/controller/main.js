angular.module('privatecoach').controller('mainCtrl', function ($scope, Program, ID, $filter, Calendar) {
    $scope.model = Program.factory(ID, function () {
        var d = [];
        $scope.Math = window.Math;
        angular.forEach($scope.model.days, function (value, index) {
            d[parseInt(value.value)] = true;
        });
        var f = $filter('filter');

        $scope.weekDays = f($scope.weekDays, function (val) {
            return !d[val.value];
        });
        if (U_LEVEL) {
            $scope.user_level = U_LEVEL;
        }
        $scope.parent = Program.factory($scope.model.parent_id);
    });

//    $scope.parent = Program.factory($scope.model.parent);
    switch (LANG) {
        case 'ru':
            $scope.weekDays = [
                {value: 1, "title": 'Понедельник'},
                {value: 2, "title": 'Вторник'},
                {value: 3, "title": 'Среда'},
                {value: 4, "title": 'Четверг'},
                {value: 5, "title": 'Пятница'},
                {value: 6, "title": 'Суббота'},
                {value: 7, "title": 'Воскресенье'}
            ].reverse();
            break;
        case 'kz':
            $scope.weekDays = [
                {value: 1, "title": 'Дүйсенбі'},
                {value: 2, "title": 'Сейсенбі'},
                {value: 3, "title": 'Сәрсенбі'},
                {value: 4, "title": 'Бейсенбі'},
                {value: 5, "title": 'Жұма'},
                {value: 6, "title": 'Сенбі'},
                {value: 7, "title": 'Жексенбі'}
            ].reverse();
            break;
        case 'en':
            $scope.weekDays = [
                {value: 1, "title": 'Monday'},
                {value: 2, "title": 'Tuesday'},
                {value: 3, "title": 'Wednesday'},
                {value: 4, "title": 'Thursday'},
                {value: 5, "title": 'Friday'},
                {value: 6, "title": 'Saturday'},
                {value: 7, "title": 'Sunday'}
            ].reverse();
            break;
    }
    var dump = $scope.weekDays;
    $scope.durations = [
        1, 2, 3, 4
    ];

    $scope.sortWeek = 'value';

    $scope.save = function () {
//        console.log($scope.model.parent_id)

        if (U_PRO) {
            if ($('.empty_data')[0]) {
                alert('Перед сохранением программы необходимо добавить Ваши исходные данные');
            } else {
                $scope.model.activity = 1;
                $scope.parent.subscribers = parseInt($scope.parent.subscribers) + 1;
                $scope.parent.$save();
                $scope.model.$save(function (data) {
                    if (data) {
                        window.location.href = $('#saveProgram').attr('data-url');
                    }
                });
            }
        } else {
            if ($scope.model.duration) {
                $scope.model.activity = 1;
                $scope.model.$save(function (data) {
                    var d = [];
                    angular.forEach(data.days, function (value) {
                        d[parseInt(value.value)] = true;
                    });
                    var f = $filter('filter');
                    $scope.weekDays = f(dump, function (val) {
                        return !d[val.value];
                    });
                });
            } else {
                alert('Не указана длительность');
            }
        }
    };

    $scope.selectStartDate = function (s) {
        s.preventDefault();
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
        var el = $(s.target);
        el.datepicker({
            startDate: now,
            language: $('.start_date').attr('data-lang')
        });
        el.datepicker("show").one('changeDate', function (s) {
            var newDate = new Date(s.date);
            var d = newDate.getDate();
            var m = newDate.getMonth() + 1;
            var y = newDate.getFullYear();

            $scope.model.start_date = d + '.' + m + '.' + y;
            newDate.setDate(newDate.getDate() + ($scope.model.duration * 7));
            d = newDate.getDate() < 10 ? "0" + newDate.getDate() : newDate.getDate();
            m = (newDate.getMonth() + 1) < 10 ? "0" + (newDate.getMonth() + 1) : (newDate.getMonth() + 1);
            y = newDate.getFullYear();
            $scope.model.end_date = d + '.' + m + '.' + y;
            $scope.model.$save();
            el.datepicker('hide');

        });
    };

    $scope.addExercise = function (day, event) {
        event.preventDefault();
        day.exercises = day.exercises || [];
//        day.exercises.push({"title":"Упраженение","efforts":[{"repeat":10}]});

        var off = $scope.$on("exerciseSelected", function (e, item) {
            day.exercises.push({"id": item.id, "title": item.title, "efforts": [
                {"repeat": 10}
            ]});
            angular.element("#exerciseModal").modal("hide");
            off();
        });

//        var cancel=$scope.$on("exerciseCanceled",function(){
//            off();
//            cancel();
//        });

        angular.element("#exerciseModal").modal().one('hidden', function () {
            off();
        });

    };

    $scope.removeExercise = function (day, ex, event) {
        event.preventDefault();
        day.exercises = day.exercises || [];
        if (day.exercises.length > 0) {
            var index = day.exercises.indexOf(ex);
            day.exercises.splice(index, 1);
        }
    };

    $scope.addDay = function () {
        $scope.model.days = $scope.model.days || [];
        if ($scope.weekDays.length > 0) {
            $scope.model.days.push($scope.weekDays.pop());
        }
    };

    $scope.removeDay = function ($index, day, e) {
        e.preventDefault();
        var index = $scope.model.days.indexOf(day);
        $scope.model.days.splice(index, 1);
        $scope.weekDays.push({"title": day.title, value: day.value});

    };

    $scope.changeDay = function (d, day, event) {
        event.preventDefault();
        var item = {"value": day.value, "title": day.title};
        var index = $scope.weekDays.indexOf(d);
        var newDay = $scope.weekDays.splice(index, 1, item)[0];
        day.title = newDay.title;
        day.value = newDay.value;


    };

    $scope.copyDay = function (origDay, event) {
        event.preventDefault();
        var day = angular.copy(origDay);
        if ($scope.weekDays.length < 1) {
            return;
        }
        var newDay = $scope.weekDays.pop();
        day.title = newDay.title;
        day.value = newDay.value;
        $scope.model.days.push(day);

    };

    $scope.addEffort = function (exercise, event) {
        exercise.efforts = exercise.efforts || [];
        event.preventDefault();
        exercise.efforts.push({"repeat": 10});
    };

    $scope.removeEffort = function (exercise, event) {
        event.preventDefault();
        if (exercise.efforts.length > 1) {
            exercise.efforts.pop();
        }
    };

    $scope.upRepeat = function (effort, event) {
        event.preventDefault();
        effort.repeat++;
    };

    $scope.downRepeat = function (effort, event) {
        event.preventDefault();
        if (effort.repeat > 1) {
            effort.repeat--;
        }
    }

//    console.log(Calendar.getByDate(2013,8));
});