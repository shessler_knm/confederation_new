angular.module('privatecoach').controller('exerciseCtrl',function($scope,$resource){
    var Exercise=$resource(BASE_URL + LANG +'/api/exercise');

    $scope.items=Exercise.query();
    $scope.select=function(item,event){
        event.preventDefault();
        $scope.$emit('exerciseSelected',item);
    }

    $scope.cancel=function(){
        $scope.$emit('exerciseCanceled');
    }
});