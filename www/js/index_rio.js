$(function () {
    var austDay = new Date(2016, 7, 5, 0, 0, 0, 0);//год, месяц с 0, день
    $('#timer').countdown({
        until: austDay,
        format: 'dHMS',
        description: '',
        padZeroes: true,
        labels: ['Год', 'Месяцев', 'Недель', 'Дней', 'Часов', 'Мин', 'Сек']
    });
});
