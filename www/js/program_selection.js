/**
 * Created with JetBrains PhpStorm.
 * User: Almas
 * Date: 22.12.13
 * Time: 11:36
 * To change this template use File | Settings | File Templates.
 */
$(function () {
    var object = $('#search').val();
    $('#search').focusin(function () {
        if ($('#search').val() == object) {
            $('#search').val('');
        }
    });
    $('#search').focusout(function () {
        if ($('#search').val() == '') {
            $('#search').val(object);
        }
    });

    $('#clearFilters').on('click', function (e) {
        e.preventDefault();
        $('#goals').val(0);
        $('#place').val(0);
        $('#muscles').val(0);
        $('#sessions').val(0);
        $('#search').val($('#search').attr('data-default'));
        $('.customSelectInner').text('');
    });

    $('#goals').on('change', function () {
        if ($('#goals').val() == 'MUSCLE') {
            $('#muscles').val(0);
            $('.muscles').removeClass('hidden');
        } else {
            $('.muscles').addClass('hidden');
            $('#muscles').val(0);
        }
    });

    $('#submit').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: $('#submit').attr('data-url'),
            data: $('#program_filter').serialize(),
            success: function (data) {
                $('.news_list').html(data);
                $('.add_program').on('click', function (e) {
                    e.preventDefault();
                    query($(this).attr('data-url'));
                });
                var star = $('.star');
                star.raty({
                    starOff: '/images/raty/star-off-big.png',
                    starOn: '/images/raty/star-on-big.png',
                    starHalf: '/images/raty/star-half-big.png',
                    round: { down: .26, half: .6, up: .9 },
                    width: 150,
                    half: true,
                    noRatedMsg: $('#advance_options').attr('data-no-rating-hint'),
                    hints: ['', '', '', '', ''],
                    readOnly: function () {
                        return $(this).attr('data-read-only') == 1;
                    },
                    score: function () {
                        return $(this).attr('data-score');
                    }
                });
            }
        });
    });


    $.ajax({
        type: "POST",
        url: $('#submit').attr('data-url'),
        data: $('#program_filter').serialize(),
        success: function (data) {
            $('.news_list').html(data);
            $('.add_program').on('click', function (e) {
                e.preventDefault();
                query($(this).attr('data-url'));
            });
            var star = $('.star');
            star.raty({
                starOff: '/images/raty/star-off-big.png',
                starOn: '/images/raty/star-on-big.png',
                starHalf: '/images/raty/star-half-big.png',
                round: { down: .26, half: .6, up: .9 },
                width: 150,
                half: true,
                noRatedMsg: $('#advance_options').attr('data-no-rating-hint'),
                hints: ['', '', '', '', ''],
                readOnly: function () {
                    return $(this).attr('data-read-only') == 1;
                },
                score: function () {
                    return $(this).attr('data-score');
                }
            });
        }

    });

    $('.programm_list').on('click', '.my_pagination a', function (e) {
        var self = $(this);
        e.preventDefault();
        $.get(self.attr('href'), {'t': (new Date()).getTime()}, function (data) {
            $('.programm_list').html(data);
            window.location.href='#back';
            var star = $('.star');
            star.raty({
                starOff: '/images/raty/star-off-big.png',
                starOn: '/images/raty/star-on-big.png',
                starHalf: '/images/raty/star-half-big.png',
                round: { down: .26, half: .6, up: .9 },
                width: 150,
                half: true,
                noRatedMsg: $('#advance_options').attr('data-no-rating-hint'),
                hints: ['', '', '', '', ''],
                readOnly: function () {
                    return $(this).attr('data-read-only') == 1;
                },
                score: function () {
                    return $(this).attr('data-score');
                }
            });
        });

    });

//    $.get($('#submit').attr('data-url'), function (data) {
//        $('.news_list').html(data);
//    });
    $('.add_program').on('click', function (e) {
        e.preventDefault();
        query($(this).attr('data-url'));
    });

});

function query(url) {
    var mission = $('#goals').val();
    var muscle = $('#muscles').val();
    var place = $('#place').val();
    var session = $('#sessions').val();
    var q = $('#search').val();
    $.get(url, {'mission': mission, 'muscle': muscle, 'place': place, 'session': session, 'q': q}, function (success) {
        if (success) {
            window.location.href = success;
        }
    });
}
