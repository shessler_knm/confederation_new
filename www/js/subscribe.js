$(function () {
    var requests_counter =  parseInt($('.requests_counter').text());
    sub_delete();
    $('.sub_accept').on('click', function (i) {
        i.preventDefault();
        self = $(this);
        id = self.attr('data-id');
        $.get(self.attr('data-url') + '/' + id, {id: id}, function (data) {
            if (data) {
                self.parents('li').remove();
                $('#following').find('li').last().after(data);
                requests_counter--;
                $('.requests_counter').text(requests_counter);
            }
        });
    });
    $('.unsubscribe').on('click', function (e) {
        e.preventDefault();
        self = $(this);
        id = self.attr('data-id');
        type = self.attr('data-type');
        $.get(self.attr('data-url') + '/' + id, {id: id, type: type}, function (data) {
            if (data && data == 'success') {
                self.parents('li').remove();
            }
        });
    });
});

function sub_delete(){
    $('.sub_delete').on('click', function (i) {
        i.preventDefault();
        self = $(this);
        id = self.attr('data-id');
        $.get(self.attr('data-url') + '/' + id, {id: id}, function (data) {
            if (data && data == 'success') {
                self.parents('li').remove();
            }
        });
    });
}
$('#Subscribes a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
})