$(function() {
    ymaps.ready(function () {
        // Создание экземпляра карты
        lat = $('#point').attr('data-lat');
        lng = $('#point').attr('data-lng');
        var myMap = new ymaps.Map('map', {
            center:[lat, lng],
            zoom:15
        });
        myMap.controls.add(
            new ymaps.control.ZoomControl()
        );
        myMap.setType('yandex#publicMap');
        myMap.behaviors.enable('scrollZoom');


        // Загрузка ymapsML-файла
        var id = $('#gym_id').val();
        var currentTime = new Date();
        var currentMinutes = currentTime.getMinutes();
        ymaps.geoXml.load('http://confederation.kz/privatecoach/gym/xmlgeoobject?id=' + id + '&min='+currentMinutes)
            .then(function (res) {
                myMap.geoObjects.add(res.geoObjects); // Добавление геообъектов на карту
            }, function (error) {   // Вызывается в случае неудачной загрузки ymapsML
                console.log('Ошибка: ' + error);
            });

        var myCallback = function (err) {
            if (err) {
                throw err;
            }
        };
    });
})