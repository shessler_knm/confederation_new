/**
 *
 * Для работы нужен JQuery 1.9 >=
 * только для получения width() height().
 * PS: И мне похуй на Ваше мнение относительно этого.
 *
 */

/**
 *
 * @param data {{week: Array, ves: Array, percent: Array}}
 * @param elID : tag id
 *
 */

$(function () {
    var user_id = $('#panel').data('user_id');

    if (user_id) {
        $.getJSON(window.BASE_URL + "privatecoach/profile/chartData/" + user_id, function (resp) {
            var week = [];
            var ves = [];
            var percent = [];
            appChart = $('#appChart');
            angular.forEach(resp, function (val, key) {
                week.push(val.week);
                ves.push(val.weight);
                if (val.fat != 0) {
                    percent.push(val.fat);
                }else{
                    percent.push(null)
                }
            });

            var data = {
                week: week,
                ves: ves,
                percent: percent
            };

            if (data.week.length > 1) {
                renderChart(data, "panel");
            }
        });
    }
});

function renderChart(data, elID) {

    function excFunc(val) {
        if (val != "max" && val != "min" && val != "half") return true;
        else return false;
    }

    var maxFunction = function () {
        return Math.max.apply(Math, this);
    }
    var minFunction = function () {
        return Math.min.apply(Math, this) != 0 ? Math.min.apply(Math, this) : null;
    }
    var halfFunction = function () {
        return parseInt(this.max() / 2);
    }

    function createMyArray(arr) {
        arr.max = maxFunction;
        arr.min = minFunction;
        arr.half = halfFunction;
    }

    for (var i in data) {
        createMyArray(data[i]);
    }
    var htmlElement = document.getElementById("panel");


    var topOffset = 30;
    var leftOffset = 30;
    var bottomOffset = 30;
    var rightOffset = 30;

    var coof = 3.5;//getCoofY();
    var coofTime = 60;//getCoofX();

    function _coof(val) {
        return startCoord.y - (val * coof);
    }

    function _coofTime(val) {
        return val * coofTime + leftOffset;
    }

    var maxVes = data.ves.min() * coof;
    var minVes = data.ves.max() * coof;
//        var maxPercent = data.percent.min() * coof;
//        var minPercent = data.percent.max() * coof;
    var maxTime = (data.week.max() + 1) * coofTime;
    var minTime = data.week.min() * coofTime;


    var optHeight = data.ves.max() > data.percent.max() ? data.ves.max() : data.percent.max();
    var YAxis = optHeight * coof;
    var monWidth = 500;
    var monHeight = YAxis + optHeight;
    $("#" + elID).width(monWidth);
    $("#" + elID).height(monHeight);

    var maxYAxis = topOffset;
    var maxXAxis = leftOffset + maxTime - rightOffset;

    var startCoord = {x: leftOffset, y: maxYAxis + YAxis};

    /* Instantiate JSGL Panel. */
    myPanel = new jsgl.Panel(htmlElement);

    function DrawXAxis() {

        for (var i in data.week) {
            if (excFunc(i)) {
                var el = data.week[i];
                var l = myPanel.createLabel();
                l.setText(el);
                l.setY(startCoord.y);
                l.setX(startCoord.x + (el * coofTime));
                l.setFontColor("rgb(151,151,151)");
                myPanel.addElement(l);
            }
        }
        var l = myPanel.createLabel();
        l.setText("нед.");
        l.setFontColor("rgb(151,151,151)");
        l.setY(startCoord.y);
        l.setX(startCoord.x + (el * coofTime) + 20);
        myPanel.addElement(l);
    }

    function DrawLine(x1, y1, x2, y2, color) {
        var line = myPanel.createLine()
        line.getStroke().setWeight(1);
        line.getStroke().setColor(color);
        line.setStartPointXY(x1, y1);
        line.setEndPointXY(x2, y2);
        myPanel.addElement(line);
    }

    function setNumPos(key, arrY) {
        if (arrY[parseInt(key) + 1]) {
            var curY = _coof(arrY[key]);
            var nextY = _coof(arrY[parseInt(key) + 1]);
            if (nextY > curY) {
                return true;
            } else {
                return false;
            }
        }
    }

    function DrawGraph(weeks, graphArr, color) {
        var myStroke = new jsgl.stroke.SolidStroke();
        myStroke.setWeight(3);
        myStroke.setJoinStyle(jsgl.JoinStyles.BEVEL);
        myStroke.setColor(color);
        myStroke.setEndcapType(jsgl.EndcapTypes.SQUARE);


        var poly = myPanel.createPolyline();
        poly.setStroke(myStroke);

//        poly.addPointXY(startCoord.x, startCoord.y - graphArr[0] * coof);
        for (var i in weeks) {
            if (excFunc(i)) {
                var label = myPanel.createLabel();
                label.setText(graphArr[i]);
                var t = _coofTime(weeks[i]);
                if (graphArr[i]) {
                    var v = _coof(graphArr[i]);
                    poly.addPointXY(t, v);
                }
                label.setX(t);
                if (setNumPos(i, graphArr)) {
                    label.setY(v - 15);
                } else {
                    label.setY(v);
                }
                label.setFontSize(12);
                label.setFontColor(color);
                myPanel.addElement(label);
            }
        }
        poly.getStroke().setColor(color);
        myPanel.addElement(poly);
    }

    function createHUINISHKA(x, y, text, color) {
        c = myPanel.createCircle();
        var textOffset = 5;
        var radius = 4;
        c.setRadius(radius);
        c.setCenterX(x + leftOffset);
        c.setCenterY(y + topOffset);
        c.getStroke().setColor(color);
        c.getFill().setColor(color);
        myPanel.addElement(c);

        var label = myPanel.createLabel();
        label.setText(text);
        label.setFontColor(color);
        label.setX(c.getCenterX() + radius + textOffset);
        label.setY(c.getCenterY() - 8);
        myPanel.addElement(label);
    }

    function drawGrid() {
        var recWidth = coofTime;
        var recHeight = coof * 5;
        for (var i = 0; i < (maxXAxis / coofTime); i++) {
            var x = startCoord.x + (i * coofTime);
            var NumHeight = 0;
            for (var y = (startCoord.y - recHeight); y >= maxYAxis; y -= recHeight) {

                var rect = myPanel.createRectangle();
                rect.setX(x);
                rect.setY(y);
                rect.setWidth(recWidth);
                rect.setHeight(recHeight);
                rect.getStroke().setColor("#E8E8E8");
                rect.getFill().setColor("#E8E8E8");
                rect.getFill().setOpacity("0.2");
                myPanel.addElement(rect);

                NumHeight += recHeight;
            }
        }
        return NumHeight;
    }

    DrawXAxis();

    createHUINISHKA(10, -10, appChart.attr('data-weight'), "rgb(253,155,32)");
    createHUINISHKA(70, -10, appChart.attr('data-fat'), "rgb(55,167,253)");

    var CorrectedYAxisHeight = drawGrid();


    DrawGraph(data.week, data.ves, "rgb(253,155,32)");
    DrawGraph(data.week, data.percent, "rgb(55,167,253)");
    //Draw coordinate axis
    DrawLine(startCoord.x, startCoord.y, startCoord.x, startCoord.y - CorrectedYAxisHeight, "rgb(151,151,151)");
    DrawLine(startCoord.x, startCoord.y, startCoord.x + maxXAxis, startCoord.y, "rgb(151,151,151)");
}