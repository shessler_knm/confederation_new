if (window.P_ID==undefined){
    P_ID=null;
}

angular.module('privatecoach',[
    'ngResource',
    'ngSanitize'
],function($httpProvider){
    $httpProvider.defaults.headers.common['x-csrf-token']=CSRF_TOKEN;
}).constant('ID',P_ID).constant('USER_ID',USER_ID).constant('CUR_USER_ID',CUR_USER_ID);