$(function () {
    var city_select = 0;
    var url = $('.gyms_list').attr('data-url');
    var score = 0;
    var alphabet = 0;
    filter(url, city_select, score, null);
    $('#select_id').on('change', function () {
        city_select = $('#select_id option:selected').val();
        var score = $('#ratingFilter').hasClass('active')?1:0;
        var date = $('#dateFilter').hasClass('active')?1:0;
        console.log(score,date)
        filter(url, city_select, score, date);
        counter = 1;
    });
    var more = $('#more');
    var counter = more.attr('data-counter');

    $('.gyms_list').on('click', '#more', function (e) {
        e.preventDefault();
        var counter = $('#more').attr('data-counter');
        counter++;
        $('#more').attr('data-counter', counter);
        city_select = $('#select_id option:selected').val();
        var score = $('#ratingFilter').hasClass('active')?1:0;
        var alphabet = $('#alphabetFilter').hasClass('active')?1:0;
        var more_url = $('.gyms_list').attr('data-url') + '/' + city_select + '/' + randomByLength(1, 3);
        $.get(more_url,{city: city_select, score: score, alphabet: alphabet,counter: counter}, function (data) {
            $('#over').remove();
            $('#more').parent().before(data);
            $('.star').raty({
                starOff: '/images/raty/star-off-big.png',
                starOn: '/images/raty/star-on-big.png',
                starHalf: '/images/raty/star-half-big.png',
                round: { down: .26, half: .6, up: .9 },
                width: 150,
                half: true,
                noRatedMsg: $('#advance_options').attr('data-no-rating-hint'),
                hints: ['', '', '', '', ''],
                readOnly: function () {
                    return $('#advance_options').attr('data-read-only') == 1;
                },
                score: function () {
                    return $(this).attr('data-score');
                }
            });
            if (!$('#over').attr('data-over')) {
                $('#more').parent().remove();
                $('.data-over').removeClass('hidden');
            }

        });
    });

    $('#ratingFilter').on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
            $('#alphabetFilter').removeClass('active');
        }
        city_select = $('#select_id option:selected').val();
        var score = $('#ratingFilter').hasClass('active')?1:0;
        var alphabet = null;
        filter(url,city_select,score,alphabet);
    });

    $('#alphabetFilter').on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
            $('#ratingFilter').removeClass('active');
        }
        city_select = $('#select_id option:selected').val();
        var score = null;
        var alphabet = $('#alphabetFilter').hasClass('active')?1:0;
        filter(url,city_select,score,alphabet);
    })

    $('#clearFilters').on('click', function (e) {
        e.preventDefault();
        filter(url,null,null,null);
        $('#ratingFilter').removeClass('active');
        $('#alphabetFilter').removeClass('active');
        $('#select_id').val(0).change();
    });

});

function randomByLength(min, max) {
    var a = 0;
    while (!a || a.toString().length < min)
        a = parseInt(Math.random().toString().substr(2, max));
    return a;
}

function filter(url, city, score, alphabet) {
    $.get(url, {city: city, score: score, alphabet: alphabet}, function (data) {
        $('.gyms_list').html(data);
        $('.star').raty({
            starOff: '/images/raty/star-off-big.png',
            starOn: '/images/raty/star-on-big.png',
            starHalf: '/images/raty/star-half-big.png',
            round: { down: .26, half: .6, up: .9 },
            width: 150,
            half: true,
            noRatedMsg: $('#advance_options').attr('data-no-rating-hint'),
            hints: ['', '', '', '', ''],
            readOnly: function () {
                return $('#advance_options').attr('data-read-only') == 1;
            },
            score: function () {
                return $(this).attr('data-score');
            }
        });
    });
}