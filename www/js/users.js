$(function () {
    more_url = $('.user_top_list').attr('data-url');
    $.get(more_url, {'t': (new Date()).getTime()}, function (data) {
        advanced_data(data);
    });
    $('.user_top_list').on('click', '#more', function (e) {
        e.preventDefault();
        var counter = $('#more').attr('data-counter');
        counter++;
        $('#more').attr('data-counter', counter);
        $.get(more_url, {counter: counter}, function (data) {
            $('#over').remove();
            $('#more').parent().before(data);
            $('.img-circle img').each(resizeImg);
            $('.subscribe').on('click', function (e) {
                e.preventDefault();
                self = $(this);
                if (self.attr('data-change') == 0) {
                    self.attr('data-change', 1);
                } else {
                    self.attr('data-change', 0);
                }
                id = self.attr('data-id');
                sub = $('#sub').val();
                unsub = $('#unsub').val();
                $.get(self.attr('data-url'), {'id': id}, function (data) {
                    if (data && data == 'success') {
                        if (self.attr('data-change') == 1) {
                            self.attr('data-url', BASE_URL + "privatecoach/subscribe/delete");
                            self.children().text(unsub);
                        } else {
                            self.attr('data-url', BASE_URL + "privatecoach/subscribe/add");
                            self.children().text(sub);
                        }
                    }
                });
            });
            if (!$('#over').attr('data-over')) {
                $('#more').parent().remove();
                $('.data-over').removeClass('hidden');
            }

        });
    });
    $('.submit').on('click',function (e) {
        e.preventDefault();
        $.get(more_url, {'q': $('.input_top').val(), 't': (new Date()).getTime()}, function (data) {
            advanced_data(data);
        })
    })
});

resizeImg = function () {
    console.log('resizeImg');
    var img = $(this);
    var imgW = img.width();
    var imgH = img.height();
    var block = img.parents('.img-circle');
    var blockW = block.width();
    var blockH = block.height();
    if (!img.is(':visible')) {
        var c = new Image();
        console.log('if');
        c.onload = function () {
            imgW = c.width;
            imgH = c.height;
            if (blockW > 0) {
                changeAttr();
            }
        };

        c.src = img.attr('src');
    } else {
        console.log('else');
        if (blockW > 0) {
            changeAttr();
        }
    }


    function changeAttr() {
        if (imgW > imgH) {
            img.height(blockH);
            img.width('');
            var newW = blockH / imgH * imgW;
            img.css('margin-left', (blockW - newW) / 2);
            img.css('margin-top', '');
        }
        else {
            img.width(blockW);
            img.height('');
            var newH = blockW / imgW * imgH;
            img.css('margin-top', (blockH - newH) / 2);
            img.css('margin-left', '');
        }
        img.data('changed', true);
    }


};

function advanced_data(data) {
    $('.user_top_list').html(data);
    $('.img-circle img').each(resizeImg);
    $('.subscribe').on('click', function (e) {
        e.preventDefault();
        self = $(this);
        if (self.attr('data-change') == 0) {
            self.attr('data-change', 1);
        } else {
            self.attr('data-change', 0);
        }
        id = self.attr('data-id');
        sub = $('#sub').val();
        unsub = $('#unsub').val();
        $.get(self.attr('data-url'), {'id': id}, function (data) {
            if (data && data == 'success') {
                if (self.attr('data-change') == 1) {
                    self.attr('data-url', BASE_URL + "privatecoach/subscribe/delete");
                    self.children().text(unsub);
                } else {
                    self.attr('data-url', BASE_URL + "privatecoach/subscribe/add");
                    self.children().text(sub);
                }
            }
        });
    });
}