$(function () {
    var hint = $('.hint');

    hint.popover({
        title: function () {
            return '<img src="' + $(this).attr('data-img') + '" />';
        },
        html: true
    });
    hint.on('click', function (e) {
        e.preventDefault();
        $('.active').popover('hide');
        if (!$(this).hasClass('active')) {
            $(this).addClass('active');
            return false;
        } else {
            $(this).removeClass('active');
            return false;
        }

    });

});

jQuery(document).click(function () {
    $('.hint').popover('hide');
});



