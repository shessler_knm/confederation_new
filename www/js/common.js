$(window).load(function() {
    $('#status').fadeOut();
    $('#preloader').delay(350).fadeOut('slow');
    $('body').delay(350).css({'overflow':'visible'});
});
$(document).ready(function() {
	// slider1
    $(".slider1").owlCarousel({
        dots: false,
        loop: true,
        items : 1,
        nav: true,
        navText: ["",""],
        // autoplay: 8000,
        navRewind: true,
        responsive: true,
        responsiveRefreshRate: 200
    });

    var owl2 = $(".slider2").owlCarousel({
        dots: false,
        loop: true,
        items : 1,
        nav: true,
        navText: ["",""],
        // autoplay: 8000,
        navRewind: true,
        responsive: true,
        responsiveRefreshRate: 200
    });
    owl2.on('translated.owl.carousel', function(event) {
    	var textSlide = owl2.find('.active').find('.item').data('text')
    	// console.log(event.item.index)
    	console.log(textSlide)
    	$('.js-slider2-text')
    		.hide(0)
    		.filter('.text' + textSlide)
    		.fadeIn(250)
    })
});