$(function () {


    $('#add_photo').on('click', function (e) {
        e.preventDefault();
        $('#myfile').click();

    });
    $('#myfile').change(function () {
        $('.progress').removeClass('hidden');
        $('#upload').click();
    });

    $('#submit_result').on('click', function () {
        var fat = calc();
        if (fat) {
            if (fat < 0 || fat > 85) {
                alert('Ошибка вычислений');
            } else {
                $('input[name=fat]').val(fat);
                $.ajax({
                    type: "POST",
                    url: $('#userImage').attr('data-result'),
                    data: $('#userImage').serialize(),
                    success: function (data) {

                        if (data) {
                            alert('Ошибка');
                        } else {
                            disablePopup();
                            refresh_result();

                        }
                    }
//                dataType: dataType
                });
//                disablePopup();
            }
        } else {
            alert('Неопределен жир. Проверьте пол и дату рождения.');
        }
    });


//    $('#save_results').on('click', function (e) {
//        e.preventDefault();
//        $.get($(this).attr('data-url'));
//        location.reload();
//    });

    var bar = $('.bar');
    var percent = $('.percent');
    var status = $('#status');

    $('form').ajaxForm({
        beforeSend: function () {
            status.empty();
            var percentVal = '0%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        uploadProgress: function (event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        success: function () {
            var percentVal = '100%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        complete: function (tr) {
            var response = JSON.parse(tr.responseText);
            $('#add_photo').html('<img src="' + response.src + '" style="width:75px">');
            $('input[name=file]').val(parseInt(response.file));
            $("#submit_result").removeAttr("disabled");
        }
    });

    var nowTemp = new Date();
    var adv_form = $('#advanced_form');
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var date_start = new Date(adv_form.attr('data-year'), adv_form.attr('data-month') - 1, adv_form.attr('data-day'), 0, 0, 0, 0);
    var datepicker = $('#changeDate').datepicker({
        endDate: now,
        language: $('#changeDate').attr('data-lang')
//Ориентация датапикера зависит от размера контейнера.
//Чем больше размер, тем выше вероятность того, что датапикер влезет в это пространство, иначе будет тупить
//        orientation: "right"
    }).on('changeDate', function (i) {
            var newDate = new Date(i.date);
            var d = newDate.getDate();
            var m = newDate.getMonth();
            var y = newDate.getFullYear();
            var monthName = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
            $("#advanced_form").val($.datepicker.formatDate(d + "." + (m + 1) + "." + y, newDate));
            $("#changeDate").text($.datepicker.formatDate(d + "." + (m + 1) + "." + y, newDate));
            $('#changeDate').datepicker('hide');
        });

});

function resizeImg(img) {
    if (img.attr('src') == undefined) return;
    if (img.data('changed')) return;

    var imgW = img.width();
    var imgH = img.height();
    var block = img.parents('.img-circle');
    var blockW = block.width();
    var blockH = block.height();
//    if (imgW == 0) imgW = blockW;
    if (!img.is(':visible')) {
        var c = new Image();

        c.onload = function () {
            imgW = c.width;
            imgH = c.height;
            if (blockW > 0) {
                changeAttr()
            }
        }

        c.src = img.attr('src');
    } else {
        if (blockW > 0) {
            changeAttr();
        }
    }


    function changeAttr() {
        console.log('W-' + imgW);
        console.log('H-' + imgH);
        console.log(imgW > imgH)
        if (imgW > imgH) {
            img.height(blockH);
            var newW = blockH / imgH * imgW;
            img.css('margin-left', (blockW - newW) / 2);
        } else {
            img.width(blockW);
            var newH = blockW / imgW * imgH;
            img.css('margin-top', (blockH - newH) / 2);
        }
        img.data('changed', true);
    }


}

//0 means disabled; 1 means enabled;
var popupStatus = 0;

//loading popup with jQuery magic!
function loadPopup() {
    //loads popup only if it is disabled
    if (popupStatus == 0) {
        $("#backgroundPopup").css({
            "opacity": "0.7"
        });
        $("#backgroundPopup").fadeIn("slow");
        $("#popup").fadeIn("slow");
        popupStatus = 1;
    }
}

//disabling popup with jQuery magic!
function disablePopup() {
    //disables popup only if it is enabled
    if (popupStatus == 1) {
        $("#backgroundPopup").fadeOut("slow");
        $("#popup").fadeOut("slow");
        popupStatus = 0;
    }
}

//centering popup
function centerPopup() {
    //request data for centering
    var windowWidth = document.documentElement.clientWidth;
    var windowHeight = document.documentElement.clientHeight;
    var popupHeight = $("#popup").height();
    var popupWidth = $("#popup").width();
    //centering
    $("#popup").css({
        "position": "fixed",
        "top": windowHeight / 2 - popupHeight / 2,
        "left": windowWidth / 2 - popupWidth / 2
    });
    //only need force for IE6

    $("#backgroundPopup").css({
        "height": windowHeight
    });

}


//CONTROLLING EVENTS IN jQuery
$(function () {
    //LOADING POPUP
    //Click the button event!
    $("#add_data").click(function () {
        //centering with css
        centerPopup();
        //load popup
        loadPopup();
    });

    //CLOSING POPUP
    //Click the x event!
    $("#popupClose").click(function () {
        disablePopup();
    });
    //Click out event!
    $('#popup').click(function (event) {
        if ($(event.target)[0].outerHTML == $('#popup')[0].outerHTML) {
            disablePopup();
        }
    });
    //Press Escape event!
    $(document).keypress(function (e) {
        if (e.keyCode == 27 && popupStatus == 1) {
            disablePopup();
        }
    });

});

function calc() {

    var age = parseInt($('input[name=age]').val());
    var weight = parseInt($('input[name=weight]').val());
    var chest = parseInt($('input[name=chest]').val());
    var stomach = parseInt($('input[name=stomach]').val());
    var haunch = parseInt($('input[name=haunch]').val());
    var triceps = parseInt($('input[name=triceps]').val());
    var blade = parseInt($('input[name=blade]').val());
    var ilium = parseInt($('input[name=ilium]').val());
    var armpit = parseInt($('input[name=armpit]').val());
    var error = [];
    if (!weight) {
        error.push('weight');
    }
    if (!chest) {
        error.push('chest');
    }
    if (!stomach) {
        error.push('stomach');
    }
    if (!haunch) {
        error.push('haunch');
    }
    if (!triceps) {
        error.push('triceps');
    }
    if (!blade) {
        error.push('blade');
    }
    if (!ilium) {
        error.push('ilium');
    }
    if (!armpit) {
        error.push('armpit');
    }
    if (error.length > 0) {
        return error;
    }

    var y = 495;
    var z = 450;
    var bd = null;
    var m = chest + stomach + haunch + triceps + blade + ilium + armpit;
    var n = Math.pow(m, 2);
    var sex = parseInt($('input[name=sex]').val());
    if (sex == 1) {
        bd = 1.1120 - (0.00043499 * m) + (0.00000055 * n) - (0.00028826 * age);
    } else {
        bd = 1.0970 - (0.00046971 * m) + (0.00000056 * n) - (0.00012828 * age);
    }
    var perfat = (y / (bd)) - z;
    var pleanmass = 1 - (perfat / 100);

    var remain = perfat % 10;
    var bfint = Math.round(perfat);
    remain = perfat - bfint;
    var suff = Math.round(remain * 10);
    var finbf = bfint + suff / 10;

    var leanmass = pleanmass * weight;
    var remain2 = leanmass % 10;
    var lmint = Math.round(leanmass);
    remain2 = leanmass - lmint;
    var suff2 = Math.round(remain * 10);
    var finlm = lmint + suff2 / 10;
    if (finbf < 0) {
        alert('Ошибка вычислений. Отрицательное значение');
    } else if (finbf > 85) {
        alert('Ошибка вычислений. Большое значение');
    } else {
        return finbf;
    }

}

function refresh_result() {
    $.get($('.gray_block').attr('data-data'), function (data) {
        if (data == 'error') {
            $('.gray_block').html($('.gray_block').attr('data-error'));
        } else {
            $('.gray_block').html(data);
            resizeImg($('.img-circle img'));
        }
    });
}

function get_data() {
    $.get($('.gray_block').attr('data-data'), function (data) {
        $('.gray_block').html(data);
    });
}