function template($scope, $http) {
    $scope.$watch('federation', function () {
        if (!$scope.federation) return;
        $http.post('http://sport.dev/team/source', {federation_id:$scope.federation}).success(function (data) {
            $scope.teams = data;
            $scope.team = null;
            $scope.coachs = [];
        });
    });
}