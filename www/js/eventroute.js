$(function () {
    var fed_id = $('.fed_id').val() || 0;
    var year = $('.year').val();
    var month = $('#month').val();
    var city = $('#city').val();


    $('#month').on('change', function () {
        month = $(this).val();
        if (fed_id) {
            window.location.href = BASE_URL + fed_id + '/event/' + year + '/' + month + '/' + city;;
        } else {
            window.location.href = BASE_URL + 'confederation/event/' + year + '/' + month + '/' + city;;
        }
    });
});