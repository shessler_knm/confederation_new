(function($){
    var settings={
        autoplay: true,
        state: true,
        delay: 4000,
        speed: 1000,
        timer: null,
        i:0
    }
    $.fn.slider = function(options){
            var set = $.extend(settings,options);
            var sliding=false;
            var self = this;
            var list = self.find('.slider_news .slider_content .hide_news');
            var active=null;
            var slider_news = self.find('.slider_news');
            var btn_autoplay = self.find('.slider_news .slider_btn .btn_autoplay');
            self.on('click','.slider_news .slider_btn a.prev',function(e){
                 e.preventDefault();
                 set.state = true;
                 reset('prev');
            });
            self.on('click','.slider_news .slider_btn a.next',function(e){
                 e.preventDefault();
                 set.state = true;
                 reset('next');
            });
            self.on('click','.slider_news .i_arr',function(e){
                 e.preventDefault();

                if(set.state == true){
                     set.state = false;
                     list.each(function(){
                         $(this).show()
                     });
                     slider_news.css({'height' : 'auto'});
                     self.css({'height':'auto'});
                 } else{
                     set.state = true;
                     list.each(function(){$(this).hide()});
                     slider_news.css({'height' : ''});
                     self.css({'height':''});
                     if(set.autoplay == false){
                         set.autoplay = true;
                         reset();
                         set.autoplay = false;
                     }
                     reset();
                 }                        
            });
            self.on('click','.slider_news .slider_btn a.pause,.slider_news .slider_btn a.play',function(e){
                 e.preventDefault();

                if(set.autoplay == true) {
                     set.autoplay = false;
                     btn_autoplay.removeClass('pause').addClass('play');
                 } else { 
                     set.autoplay = true;
                     btn_autoplay.removeClass('play').addClass('pause');
                 }
                 reset();
            });
            var slide = function(){
                if (sliding){
                    return false;
                }
                sliding=true;
                list.hide();
                active.fadeIn(1500,function(){
                    sliding=false;
                    set.timer = setTimeout(function(){reset()}, 5000);
                });
            }
            var reset = function(action){
                if(set.autoplay == true && set.state == true || action == 'next' || action == 'prev'){
                    clearTimeout(set.timer);
                    action = (!action) ? '' : action;
                    updateClass(action);
                    slide();
                }
            }
            var updateClass = function(action){
                action = (!action) ? '' : action;
                if(action == 'prev'){
                 set.i = ((set.i-1) < 0 ) ? list.length-1 : set.i-1;           
                }
                else {
                 set.i = ((set.i+1) > list.length-1 ) ? 0 : set.i+1;   
                }
                active.removeClass('active').addClass('hide_news');
                active=$(list.get(set.i)).removeClass('hide_news').addClass('active');
               
            }
            active=this.find(".slider_news .slider_content div:first").removeClass('hide_news').addClass('active');
            if(set.autoplay == true){
                slide();
            }
       
                       
      }
})(jQuery)