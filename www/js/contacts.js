$(function() {
    var lat=$('#coor').attr('data-lat');
    var lng=$('#coor').attr('data-lng');
    var color=$('#coor').attr('data-color');
    var title=$('.media_text').text();
    ymaps.ready(function () {
        var map = new ymaps.Map ("map", {
            center: [lng, lat],
            zoom: 16
        });
        myPlacemark = new ymaps.Placemark([lng, lat], {
            content: title,
            balloonContent: title
        },
            {preset:color});

        map.geoObjects.add(myPlacemark);
    });
});
