$(function () {
    $('.text-left img').each(function () {
        $(this).wrap('<a></a>').parent().attr("class", "fancybox").attr("rel", "gallery1").attr('href', $(this).attr('src')).fancybox({
            helpers:{
                title:{
                    type:'outside',
                    position:'top',
                    openEffect	: 'none',
                    closeEffect	: 'none'
                }
            },
            padding:0
        });
    });
});