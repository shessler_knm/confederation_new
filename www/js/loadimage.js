$(function () {
    reader = new FileReader(), rFilter = /^image\/(?:bmp|cis\-cod|gif|ief|jpeg|pipeg|png|svg\+xml|tiff|x\-cmu\-raster|x\-cmx|x\-icon|x\-portable\-anymap|x\-portable\-bitmap|x\-portable\-graymap|x\-portable\-pixmap|x\-rgb|x\-xbitmap|x\-xpixmap|x\-xwindowdump)$/i;

    if ($('#currentImage')[0]){
        $('#user_photo').val('1');
    }
    reader.onload = function (reader) {
        $('#currentImage').remove();
        $('#uploadPreview').attr('src', reader.target.result);
        $('#uploadPreview').one('load', resizeImg);
        $('#user_photo').val('1');
        $('#uploadPreview').each(function () {
            if (this.complete) {
                $(this).load();
            }
        });
    };
    $('#uploadImage').on('change', function loadImageFile() {
        if (document.getElementById("uploadImage").files.length === 0) {
            return;
        }
        var oFile = document.getElementById("uploadImage").files[0];
        if (!rFilter.test(oFile.type)) {
            alert($('#alert').attr('data-text'));
            $('#frm').each(function () {
                this.reset();
            });
            return;
        }
        reader.readAsDataURL(oFile);
        document.uploadForm.submit();
    });

   $('#changeAvatar').on('click',function(e){
       e.preventDefault();
       $('#uploadImage').click();
   });


});

resizeImg = function () {
    console.log('resizeImg');
    var img = $(this);
    var imgW = img.width();
    var imgH = img.height();
    var block = img.parents('.img-circle');
    var blockW = block.width();
    var blockH = block.height();
        if (!img.is(':visible')) {
            var c = new Image();
            console.log('if');
            c.onload = function () {
                imgW = c.width;
                imgH = c.height;
                if (blockW > 0) {
                    changeAttr();
                }
            };

            c.src = img.attr('src');
        } else {
            console.log('else');
            if (blockW > 0) {
                changeAttr();
            }
        }


    function changeAttr() {
        if (imgW > imgH) {
            img.height(blockH);
            img.width('');
            var newW = blockH / imgH * imgW;
            img.css('margin-left', (blockW - newW) / 2);
            img.css('margin-top', '');
        }
        else {
            img.width(blockW);
            img.height('');
            var newH = blockW / imgW * imgH;
            img.css('margin-top', (blockH - newH) / 2);
            img.css('margin-left', '');
        }
        img.data('changed', true);
    }


};