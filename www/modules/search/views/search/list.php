<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <h1><?= __('Результаты поиска') ?></h1>

            <form action="<?= URL::site($fed_id ? $fed_id . '/search/list' : 'confederation/search/list') ?>"
                  class="row-fluid">
                <input type="text" name="q" class="span8" value="<?= HTML::chars($q) ?>"
                       placeholder="<?= __('поиск') ?>" autocomplete="off">
                <input type="submit" class="btn color" value="<?= __('Найти') ?>">
            </form>

            <ul class="unstyled event_list">
                <?php
                foreach ($model as $row):
                    ?>
                    <? if ($row->{'sef_' . I18n::$lang}): ?>
                    <li class="hr_top media">
                        <div class="media-body">
                            <div class="media-heading">
                                <? if ($fed_id || $row->target == 'coach' || $row->target == 'player'): ?>
                                    <a href="<?= URL::site((I18n::$lang != 'kz' ? I18n::$lang : null) . '/' . ($fed_id ? $fed_id : $fed_array[$row->id]) . $row->view_url(true, true)) ?>"
                                       class="h5 link"><span class="ls"><?= $row->title ?></span></a>
                                    <!--                                --><? // elseif (strstr($row->view_url(true), 'news')): ?>
                                    <!--                                    <a href="--><?//= URL::site(I18n::$lang . '/confederation/infocenter' . $row->view_url(true)) ?><!--"-->
                                    <!--                                       class="h5 link"><span class="ls">--><?//= $row->title ?><!--</span></a>-->
                                <? else: ?>
                                    <a href="<?= URL::site(I18n::$lang . '/confederation' . $row->view_url(true, false)) ?>"
                                       class="h5 link"><span class="ls"><?= $row->title ?></span></a>
                                <? endif ?>
                            </div>
                            <!--                            <div class="date_news">--><? //=Date::textdate($row->created,'d m Y')?>
                            <!--                                -->
                            <?//=Date::reformat($row->created,'h:m')?><!--</div>-->
                            <div class="media_text"><?= $row->text ?></div>
                        </div>


                    </li>
                <? endif ?>
                <?php
                endforeach;
                ?>
            </ul>
            <?= $pagination; ?>

        </div>
    </div>
    <div class="aside">
        <!-- <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div> -->
        <?= View::factory('social_networks') ?>

    </div>
</div>


