<div class="page_section">
    <div class="aside_left">
        <?=View::factory('search/left')?>
    </div>
    <div class="content_right">
        <div class="head_section">
            <h3><?=__('Расширенный поиск')?></h3>

            <div class="crumbs">
                <span><a href="<?=Url::site()?>"><?=__('Главная')?></a></span>
                ->
                <span><a href="<?=Url::site('search/list')?>"><?=__('Поиск')?></a></span>
                ->
                <span><?=__('Расширенный поиск')?></span>
            </div>
        </div>


        <div class="news_list_section list_section">
<!--            <div class="extended_search">-->
<!--                <ul class="extended-tabs">-->
<!--                    <li><a href="#publication">--><?//=__('Публикации')?><!--</a></li>-->
<!--                    <li><a href="#debate">--><?//=__('Дебаты')?><!--</a></li>-->
<!--                    <li><a href="#bproject">--><?//=__('Бизнес проекты')?><!--</a></li>-->
<!--                    <li><a href="#user">--><?//=__('Пользователи')?><!--</a></li>-->
<!--                    <li><a href="#news">--><?//=__('Новости')?><!--</a></li>-->
<!--                    <li><a href="#comment">--><?//=__('Комментарии')?><!--</a></li>-->
<!--                </ul>-->
<!--                <div class="panes">-->
<!--                    <div class="tab" id="publication">-->
<!--                        -->
<!--                    </div>-->
<!--                    <div class="tab" id="debate"></div>-->
<!--                    <div class="tab" id="bproject"></div>-->
<!--                    <div class="tab" id="user"></div>-->
<!--                    <div class="tab" id="news"></div>-->
<!--                    <div class="tab" id="comment"></div>-->
<!--                </div>-->
<!--            </div>-->
            <?php
                $title= array(
                    'debate'=>'Дебаты',
                    'publication'=>'Публикации',
                    'user'=>'Пользователи',
                    'news'=>'Новости',
                    'bproject'=>'Бизнес проекты',
                    'comment'=>'Комментарии',
                    'session'=>'Виртуальный проект',
                    'expert'=>'Экспертные обзоры'
                );
            ?>
            <div class="static_page txt">
                <h2 class="title"><?=__($title[$target])?></h2>
                <?=View::factory('search/extended/'.$target)?>
            </div>


            <ul class="vlist">
                <?php
                foreach ($model as $row):
                    ?>
                    <li class="list_item">
                        <!--                        <div class="img">-->
                        <!--                            <img src="--><?//=URL::site('images/1.jpg')?><!--" alt="">-->
                        <!--                        </div>-->
                        <div class="inner">
                            <h2><a href="<?=$row->view_url()?>"><?=$row->title?></a></h2>
                            <!--                            <div class="date_news">--><?//=Date::textdate($row->created,'d m Y')?>
                            <!--                                -->
                                <?//=Date::reformat($row->created,'h:m')?><!--</div>-->
                            <div class="text"><?=$row->text?></div>
                        </div>
                    </li>
                    <?php
                endforeach;
                ?>
            </ul>
            <?=$pagination;?>
        </div>


    </div>
</div>


