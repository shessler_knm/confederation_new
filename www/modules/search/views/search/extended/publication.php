<?php

$model = Arr::get($_GET, 'model', array());

$cp = Component::factory("components/form_get");

$cp->manual_call('strings', 'Текст', null, 'q', array(), Arr::get($model, 'q', ''));

$cp->manual_call(
    'select',
    'Язык',
    null,
    'lng',
    array(
        'options' => Orm::factory('lang')->select_options('name', 'title')
    ),
    Arr::get($model, 'lng', I18n::$lang)
);

$cp->manual_call(
    'select',
    'Тип публикации',
    null,
    'type',
    array(
        'options' => array('all' => __('Все')) + Model_Publication::possible_types(),

    ),
    Arr::get($model, 'type', 'all')
);


$authors = Arr::get($model, 'author', array());
$authors = array_diff($authors, array(0));
if (count($authors) > 0) {
    $authors = ORM::factory('user')->where('id', 'IN', $authors)->select_full_name()->select_options('fullname', 'id');
}
$cp->manual_call(
    'multi_typeaheads',
    'Авторы',
    null,
    'author',
    array(
        'value' => $authors,
        'options' => Url::site("publication/authors")
    ),
    array()
);

$themes = Arr::get($model, 'cat', array());
$themes = array_diff($themes, array(0));
if (count($themes) > 0) {
    $themes = ORM::factory('category')->where('id', 'IN', $themes)->selected_values('themes');
}
$cp->manual_call(
    'select_extended',
    'Тематики',
    null,
    'cat',
    array(
        'options' => Orm::factory('category')->select_options('themes'),
//            'need_help' => true,
        'value' => $themes,
        'id' => 1,
    ),
    array()
);

$cp->form_button_save('search', 'Поиск');
echo $cp->render();
?>