<?php
/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 05.11.12
 * Time: 15:59
 * To change this template use File | Settings | File Templates.
 */
class Controller_Admin_Search extends Admin
{
    public function action_reindex()
    {
        set_time_limit(0);
        $models = array(
            'News',
            'Video', //Тут альбомы и фото
            'Event',
            'Coach',
//            'Referee',
            'Player',
        );
        $content = '<p>Переиндексируемые модели: ' . implode(', ', $models) . '</p>';
        foreach ($models as $name) {
            $content .= "<p>$name ...</p>";
//            if ($name == 'Gallery') {
//                $result = ORM::factory('Photovideo')->find_all();
//            } else {
                $result = ORM::factory($name)->find_all();
//            }
            $x = 0;
            foreach ($result as $model) {
                $model->save();
                $x++;
            }
            $content .= "<p>$name просмотрено: $x</p>";
        }

        $this->page->content($content);

    }
}
