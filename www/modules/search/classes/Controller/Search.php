<?php
/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 06.11.12
 * Time: 9:20
 * To change this template use File | Settings | File Templates.
 */
class Controller_Search extends Site
{

    public function action_list()
    {
        $this->page->content(
            View::factory('search/list')
                ->bind('model', $model)
                ->bind('pagination', $pagination)
                ->bind('q', $q)
                ->bind('fed_array', $fed_array)
        );

        $q = $this->request->query('q');
        $extended[] = Search::escape_extend('fed_id', ($this->fed_id) ? $this->fed_id : 'confederation');
        $collection = Search::find($q, $extended);
        $collection->where('sef_' . I18n::$lang, '!=', null);
        $config = Kohana::$config->load('search_pagination.default');
        $config['total_items'] = $collection->reset(false)->count_all();
        $pagination = Pagination::factory($config);
        $pagination->apply($collection);
        $model = $collection->order_by('id','DESC')->find_all();
        foreach ($model as $row) {
            if ($row->target == 'coach' || $row->target == 'player') {
                if (preg_match('#{{fed_id:(.*?)}}#', $row->extended, $match)) {
                    $fed_array[$row->id] = $match[1];
                }
            }
        }
    }

    public function action_extended()
    {
        $target = $this->request->param('id');

        if (!in_array($target, $this->allowed_targets)) {
            $target = 'publication';
        }

        $this->page->content(
            View::factory('search/extended')
                ->bind('model', $model)
                ->bind('pagination', $pagination)
                ->set('target', $target)
        );

        $_GET['model'] = Arr::get($_GET, 'model', array());
        $_GET['model']['q'] = Arr::get($_GET['model'], 'q', Arr::get($_GET, 'q', ''));

        $model = Arr::get($_GET, 'model', array());

        $q = Arr::get($model, 'q');
        // lng потому что lang меняет язык интерфейса
        $extended_raw = Arr::extract($model, array('lng', 'cat', 'author', 'stage', 'type'));
        $extended = array();
        if ($cats = Arr::get($extended_raw, 'cat')) {

            if (Arr::is_array($cats)) {
                $cats = array_diff($cats, array(0));
                foreach ($cats as $cat) {
                    $extended[] = Search::escape_extend('cat', $cat);
                }
            } else {
                $extended[] = Search::escape_extend('cat', $cats);
            }
        }
        if ($lng = Arr::get($extended_raw, 'lng')) {
            $extended[] = Search::escape_extend('lang', $lng);
        }

        if ($authors = Arr::get($extended_raw, 'author')) {
            if (Arr::is_array($authors)) {
                $authors = array_diff($authors, array(0));
                foreach ($authors as $author) {
                    $extended[] = Search::escape_extend('author_id', $author);
                }
            } else {
                $extended[] = Search::escape_extend('author_id', $authors);
            }
        }

        if ($stage = Arr::get($extended_raw, 'stage')) {
            if ($stage != 'all') {
                $extended[] = Search::escape_extend('stage', $stage);
            }
        }

        if ($type = Arr::get($extended_raw, 'type')) {
            if ($type != 'all') {
                $extended[] = Search::escape_extend('type', $type);
            }
        }
        $is_expert = $target == 'expert' || false;
        if ($is_expert) {
            $extended[] = Search::escape_extend('type', 'expert');
        }


        $collection = Search::find($q, $extended);

        $target = ORM::factory($is_expert ? 'publication' : $target);

        $collection->join($target->table_name(), 'LEFT')
            ->on('search.target_id', '=', $target->table_name() . '.id');
        if ($target->object_name() == 'debate') {
            $collection->and_where_open();
            $collection->or_where('search.target', '=', $target->object_name());
            $collection->or_where('search.target', '=', 'debate_reply');
            $collection->and_where_close();
        } //        elseif($is_expert){
//            $collection->where('search.target', '=', 'expert');
//        }
        else {
            $collection->where('search.target', '=', $target->object_name());
        }

//        $ext=array('lng'=>I18n::$lng);

//        if ()
        $pagination = Pagination::factory(array(
            'total_items' => $collection->reset(false)->count_all()
        ));
        $pagination->apply($collection);
        $model = $collection->find_all();
    }

}
