<?php
/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 05.11.12
 * Time: 14:23
 * To change this template use File | Settings | File Templates.
 */
class Search
{
    public static function update(ISearchable $model)
    {
        Model_Search::update_data($model);
    }

    public static function delete(ISearchable $model)
    {
        Model_Search::delete_data($model);
    }

    public static function find($fulltext, $extend = null)
    {
        /** @var Model_Search $model */
        $model = ORM::factory('Search');
        $model->where_fulltext($fulltext);
        if (Arr::is_array($extend) && count($extend) > 0) {
            $model->where_extend_index($extend);
        }

        return $model;

    }

    public static function escape_extend($key, $value = null)
    {
        if (!$value) {
            return "{{{$key}}}";
        } else {
            return "{{{$key}:{$value}}}";
        }
    }


}
