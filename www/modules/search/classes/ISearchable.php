<?php
/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 05.11.12
 * Time: 13:05
 * To change this template use File | Settings | File Templates.
 */
interface ISearchable
{
    /**
     * Возвращает информацию для  полнотекстового поиска
     * @return string
     */
    public function search_fulltext_index();

    /**
     * Возвращает специально сформированную строку для поиска в ней точных значений
     * Например: {{cat:1}}{{cat:2}}{{lang:ru}}
     * Данная строка говорит что данная запись имеет связь с некой моделью cat по ключам 1 и 2, а так же
     * имеет язык ru
     * @return string
     */
    public function search_extended_index();

    /**
     * Возвращает имя модели
     * @return string
     */
    public function search_target();

    /**
     * Возвращает индификатор модели. В теории это может быть не просто числовой ID а чтото более сложное
     * например ЧПУ
     * @return string
     */
    public function search_target_id();

    /**
     * Заголовок результата
     * @return string
     */
    public function title();

    /**
     * Короткий текст
     * @return string
     */
    public function short_text();

    /**
     * URI для просмотра
     * @param bool $uri
     * @return string
     */
    //public function view_url($uri=false);
}
