<?php
/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 05.11.12
 * Time: 13:03
 * To change this template use File | Settings | File Templates.
 */
class Model_Search extends ORM
{
    protected $_table_name = 'search';

    public static function update_data(ISearchable $model)
    {
        Register::instance()->search = true;
        $data_key = array(
            'target' => $model->search_target(),
            'target_id' => $model->search_target_id(),
        );
        $data = array(
            'fulltext' => $model->search_fulltext_index(),
            'extended' => $model->search_extended_index(),
            'title_ru' => $model->title('ru'),
            'title_kz' => $model->title('kz'),
            'title_en' => $model->title('en'),
            'text_kz' => $model->short_text('kz'),
            'text_ru' => $model->short_text('ru'),
            'text_en' => $model->short_text('en'),
            'sef_ru' => $model->sef_ru,
            'sef_kz' => $model->sef_kz,
            'sef_en' => $model->sef_en,
        );
        switch ($model->search_target()) {
            case 'news':
                $category = $model->categories->find();
                $data = $data + array(
                        //$url,$lang,$category,$is_federation
                        'fed_url' => $model->view_url(true, false, $category->slug, true),
                        'conf_url' => $model->view_url(true, false, $category->slug, false),
                    );

                break;
            case 'video':
                $data = $data + array(
                        //$url,$lang,$is_federation
                        'fed_url' => $model->view_url(true, false, true),
                        'conf_url' => $model->view_url(true, false, false),
                    );
                break;
            case 'event':
                $data = $data + array(
                        //$url,$lang,$is_federation
                        'fed_url' => $model->view_url(true, false),
                        'conf_url' => $model->view_url(true, false),
                    );
                break;
            default:
                $data = $data + array(
                        //$url,$lang
                        'fed_url' => $model->view_url(true, false),
                        'conf_url' => null

                    );
        }
        $search = ORM::factory('Search', $data_key);
        if ($search->loaded()) {
            $search->values($data);
        } else {
            $search->values($data_key + $data);
        }
        $search->save();
    }

    public static function delete_data(ISearchable $model)
    {
        $data_key = array(
            'target' => $model->search_target(),
            'target_id' => $model->search_target_id(),
        );
        $search = ORM::factory('Search', $data_key);
        $search = ORM::factory('Search', $data_key);
        if ($search->loaded()) {
            $search->delete();
        }

    }

    public function view_url($uri = false, $is_federation = false)
    {
        if ($is_federation) {
            $_uri = $this->fed_url . $this->{'sef_' . I18n::$lang};
        } else {
            $_uri = $this->conf_url . $this->{'sef_' . I18n::$lang};
        }
        return $uri ? $_uri : Url::site($_uri);
    }

    public function where_extend_index($extends)
    {
        $this->where_open();

        foreach ($extends as $el) {
            $this->where('extended', 'LIKE', "%$el%");
        }

        $this->where_close();
        return $this;
    }

    public function where_fulltext($text)
    {
        $db = Database::instance();
        $this->where(DB::expr('MATCH (`fulltext`)'), 'AGAINST ', DB::expr('(' . $db->quote($text) . ' IN BOOLEAN MODE)'));
        $this->order_by(DB::expr('MATCH (`fulltext`) AGAINST ' . DB::expr('(' . $db->quote($text) . ' IN BOOLEAN MODE)')), 'desc');
        return $this;
    }
}
