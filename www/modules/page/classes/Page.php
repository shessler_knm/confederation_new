<?php defined('SYSPATH') OR die('No direct access allowed.');

class Page {

    protected static  $instance;


    public static  function get_instance(){
        if(!self::$instance){
            self::$instance = new Page();
        }
        return self::$instance;
    }

    protected  function model(){
        $fed_id=Request::$current->param('fed_id');
        if ($fed_id=='confederation') {
            $model=ORM::factory('News');
            return $model;
        }
        else{
        $model=ORM::factory('Federation',array('sef'=>$fed_id))->news->order_by('id','DESC')->limit(10);
        return $model;
        }
    }

    public function widget($view){
        $model = $this->model();
        return View::factory($view)->bind('model',$model)->render();
    }
}