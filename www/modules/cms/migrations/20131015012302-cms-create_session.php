<?php defined('SYSPATH') or die('No direct script access.');

class create_session extends Migration
{
    public function up()
    {
        $this->create_table(
            "sessions",
            array(
                "session_id" => array(0 => "string[24]", "null" => FALSE),
                "last_active" => array(0 => "integer", "null" => FALSE, "unsigned" => TRUE),
                "contents" => array(0 => "text", "null" => FALSE)
            ),
            array("session_id")
        );
        $this->add_index("sessions", "last_active", array("last_active"), "normal");
        $this->add_index("sessions", "session_id", array("session_id"), "normal");
    }

    public function down()
    {
         $this->drop_table('sessions');

        // $this->remove_column('table_name', 'column_name');
    }
}