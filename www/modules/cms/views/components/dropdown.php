<div class="btn-group">
  <div class="btn-group">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
      <? if($active_item && Arr::get($items, $active_item)) {
          $item = Arr::get($items, $active_item);
          echo $item['title'];
          unset($items[$active_item]);
      } else {
          echo $label_menu;
      }
      ?>
      <span class="caret"></span>
    </button>
      
    <ul class="dropdown-menu">
      <?  foreach ($items as $key => $value):?>
            <?if(Arr::get($value, 'type') == 'submit'):?>
                <li><input class="submit_in_dropdown" type="submit" name="<?=$key?>" value="<?=Arr::get($value, 'title','1')?>"></li>
            <?else:?>
                <li><a href="<?=URL::site($value['uri'])?>"><?=$value['title']?></a></li>
            <?  endif;?>
      <?  endforeach;?>
    </ul>
      
  </div>
</div>