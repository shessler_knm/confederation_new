<div class="control-group">
    <label for="<?=$name?>" class="control-label"><?=$label?></label>

    <div class="controls">
        <?=Form::select("model[$name]",$options,$selected,array(
        'id'=>$name,
        'class'=>'span4'
    ))?>
        <div class="err"><?=$error?></div>
    </div>
</div>