<div class="control-group">
    <?=Form::label($field, $label,array('class'=>'control-label'))?>
    <div class="controls">
        <?=Form::password("model[$field]", $value,array(
        'id'=>$field,
        'class'=>'span4'
    ))?>
        <div class="err"><?=$error?></div>
    </div>
</div>