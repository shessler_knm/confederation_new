<div class="control-group">
    <label for="<?=$field?>" class="control-label"><?=$label?></label>

    <div class="controls">
        <textarea id="<?=$field?>" name="<?="model[$field]"?>" rows="4" class="span4"><?=$value?></textarea>

        <div class="err"><?=$error?></div>
    </div>
</div>