<div class="control-group">
    <label for="<?=$name?>" class="control-label"><?=$label?></label>

    <div class="controls">
        <?=Form::select($name,$options,$selected,array(
        'id'=>$name,
        'multiple'=>'multiple',
        'size'=>$size,
        'class'=>'span4'
    ))?>
        <div class="err"><?=$error?></div>
    </div>
</div>