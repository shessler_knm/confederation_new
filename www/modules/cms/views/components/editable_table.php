<table class="table table-hover table-striped">
    <thead>
    <tr>
        <th><input type="checkbox" class="check-all"></th>
        <th>&nbsp;</th>
        <?foreach ($head as $td): ?>
        <th><?=__($td)?></th>
        <? endforeach;?>

    </tr>
    </thead>
    <tbody>
    <?foreach ($model as $row): ?>
        <?php
        if (!($row instanceof IC_Editable)) {
            continue;
        }
        ?>
    <tr>
        <td><input type="checkbox" value="<?=$row->id?>" name="model[]"></td>
        <td>
            <?php
            $items = $row->actions($user);
            ?>
            <?if(count($items)>0): ?>
            <div class="btn-group">
                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#" title="<?=__('Действия')?>">
                    <i class="icon-pencil"></i>

                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <?php

                    dropdown($items);
                    ?>
                </ul>
            </div>
            <?endif;?>

        </td>
        <?foreach ($head as $field => $label): ?>
        <td><?=$row->get_field($field)?></td>
        <? endforeach;?>

    </tr>
        <? endforeach;?>
    </tbody>
</table>