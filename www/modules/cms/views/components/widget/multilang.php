<?
    $errors=Components_Widget_Multilang::$errors;
?>
    <ul class="nav nav-tabs" id="<?=$prefix?>">
        <?foreach($langs as $lang):?>

                <li><a href="#<?=$prefix.$lang?>" data-toggle="tab"><span class="<?=Arr::get($errors,$lang)?'error':null?>">Версия сайта: <?=$lang?></span></a></li>
        <?endforeach?>

    </ul>

    <div class="tab-content">
        <?php
            foreach($langs as $lang){
                ?>

                <div class="tab-pane" id="<?=$prefix?><?=$lang?>">
                    <?foreach($elements[$lang] as $v):?>
                    <?=$v?>
                    <?endforeach;?>
                </div>


                <?php
            }

        ?>



    </div>


    <script type="text/javascript">
        $(function(){
            $("#<?=$id?> a").click(function(e){
                e.preventDefault();
                $(this).tab('show');
            });
            $('#<?=$id?> a:first').tab('show');
        });
    </script>
