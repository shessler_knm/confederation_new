
    <ul class="nav nav-tabs" id="<?=$prefix?>">
        <?php
            $lang_tabs=Components_Widget_Partial::$langs;
            $error_tabs=Components_Widget_Partial::$errors;
            foreach($langs as $lang){

                echo "<li><a href=\"#{$prefix}{$lang}\" data-toggle=\"tab\"><span class='".(Arr::get($error_tabs,$lang)?'error':null)."'>{$lang_tabs[$lang]}</span></a></li>";
            }
        ?>

    </ul>

    <div class="tab-content">
        <?php
            foreach($langs as $lang){
                ?>

                <div class="tab-pane" id="<?=$prefix?><?=$lang?>">
                    <?foreach($elements[$lang] as $v):?>
                    <?=$v?>
                    <?endforeach;?>
                </div>


                <?php
            }

        ?>



    </div>


    <script type="text/javascript">
        $(function(){
            $("#<?=$id?> a").click(function(e){
                e.preventDefault();
                $(this).tab('show');
            });
            $('#<?=$id?> a:first').tab('show');
        });
    </script>
