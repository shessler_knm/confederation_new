<div class="control-group <?=$error ? 'error' : ''?>">
    <?=Form::label($field, __($label),array('class'=>'control-label'))?>
    <?php
    $attr = Arr::get($params,'attr', array());
    $attr = array_merge($attr, array(
        'id'=>$field,
        'class'=>'span8'
    ));
    ?>
    <div class="controls">
        <?=Form::password("model[$field]",NULL, $attr)?>
    </div>
</div>