<div class="control-group <?=$error ? 'error' : ''?>">
    <?=Form::label($field, __($label),array('class'=>'control-label'))?>
    <?php
        $attr = Arr::get($params,'attr', array());
        $attr = $attr+array(
            'id'=>$field,
            'class'=>'span8'
        );
    ?>
    <div class="controls">
        <?=Form::input("model[$field]", $value, $attr)?>
        <span class="help-inline"><?=$error?></span>
    </div>
</div>