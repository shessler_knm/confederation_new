<div class="control-group <?php echo $error ? 'error' : ''?>">
    <?php echo Form::label($field, $label,array('class'=>'control-label'))?>
    <div class="controls">
        <div class="input-append date">
            <?php echo Form::input("model[$field]", Date::formatted_time($value,'d-m-Y'),array(
                'id'=>$field,
                'class'=>'span8',
                'type'=>'text',
                'data-format'=>'dd-MM-yyyy'
            ))?>
            <span class="add-on">
                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                  </i>
                </span>
        </div>

        <span class="help-inline"><?php echo $error?></span>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        var id="<?=$field?>";
        $('#'+id).parent().datetimepicker({
            language: 'ru',
            'pickTime':false
        });
    });
</script>