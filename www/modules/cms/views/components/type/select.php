<?php
    if ($params['options'] instanceof ORM){
        $params['options']=array(null=>'Не выбрано')+$params['options']->find_all()->as_array($params['key'],$params['value']);
    }

?>

<div class="control-group <?=$error ? 'error' : ''?>">
    <label for="<?=$field?>" class="control-label"><?=__($label)?></label>

    <div class="controls">
        <?=Form::select("model[$field]",$params['options'],$value,array(
        'id'=>$field,
        'class'=>'span4',
        "".(Arr::get($params,'disabled')?" disabled":'')
    ))?>
        <span class="help-inline"><?=$error?></span>
    </div>
</div>