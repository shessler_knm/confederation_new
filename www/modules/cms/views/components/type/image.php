<?php
$field_s = $model->{$field."_s"};
$edit_url = Url::site(isset($params) ? Arr::get($params, 'edit_url', 'admin/storage/storage') : 'admin/storage/storage');
$del_url = Url::site(isset($params) ? Arr::get($params, 'del_url', 'admin/storage/delimg') : 'admin/storage/delimg');
$size = isset($params) ? Arr::get($params, 'size', 100) : 100;
$aspect = Arr::get($params, 'aspect', 0)
?>
<div id="<?=$field?>" class="storage control-group <?=$error ? 'error' : ''?>">
    <label for="<?=$field?>" class="control-label"><?=__($label)?></label>
    <?=Form::hidden("model[{$field}]", $model->$field)?>

    <div class="controls">
        <div class="preview">
            <?php
            if (($model->loaded() && !empty($model->{$field})) || Arr::get($_POST, $field, false)){
                $sz = isset($size) ? $size : 100;
                $img=$field_s->html_cropped_img(
                    $sz,
                    null,
                    array(
                        'alt' => HTML::chars($field_s->original_name),
                        'data-src'=>$field_s->url('http'),
                        'data-size'=>$size
                    )
                );
                ?>
                    <ul class="thumbnails">
                        <li class="span8">
                            <div class="thumbnail">
                                <?= $img ?>
                            </div>
                            <div class="navbar crop-panel" style="display: none">
                                <div class="navbar-inner">
                                    <div class="navbar-form pull-left img-data"  >
                                        <input type="text" class="span1" name="<?= $field ?>[x]">
                                        <input type="text" class="span1" name="<?= $field ?>[y]">
                                        <input type="text" class="span1" name="<?= $field ?>[w]">
                                        <input type="text" class="span1" name="<?= $field ?>[h]">
                                        <input type="hidden" class="span1" name="<?= $field ?>[r]" value="<?= $aspect ?>">
                                        <a href="#" class="btn end-crop">Готово</a>
                                        <a href="#" class="btn cancel-crop">Отмена</a>
                                    </div>
                                </div>
                            </div>

                            <a href="#" class="btn start-crop">Обрезать</a>


                        </li>
                    </ul>
                <?
            } else {
                ?>
                <ul class="thumbnails">
                    <li class="span8">
                        <div class="thumbnail">
                            изображение не загружено
                        </div>
                        <div class="navbar crop-panel" style="display: none">
                            <div class="navbar-inner">
                                <div class="navbar-form pull-left img-data"  >
                                    <input type="text" class="span1" name="<?= $field ?>[x]">
                                    <input type="text" class="span1" name="<?= $field ?>[y]">
                                    <input type="text" class="span1" name="<?= $field ?>[w]">
                                    <input type="text" class="span1" name="<?= $field ?>[h]">
                                    <input type="hidden" class="span1" name="<?= $field ?>[r]" value="<?= $aspect ?>">
                                    <a href="#" class="btn end-crop">Готово</a>
                                    <a href="#" class="btn cancel-crop">Отмена</a>
                                </div>
                            </div>
                        </div>

                        <a href="#" class="btn start-crop">Обрезать</a>


                    </li>
                </ul>
                <?
            }
            ?>
        </div>

        <a href="<?=$edit_url.Url::query(array(
            'id' => $field,
            'title' => __($label),
            'type' => 'Img'
        ))?>" class="btn edit" title="<?=__('Изменить')?>"
                data-title="<?=$label?>"
                data-type="Img"
                data-field="<?=$field?>"
                ><i class="icon-edit"></i></a>

        <a href="<?=$del_url."/{$model->$field}"?>" class="btn delete" title="<?=__('Удалить')?>
        "><i class="icon-remove"></i></a>
        <span class="help-inline"><?=$error?></span>
    </div>
</div>

<script>


    $(function(){
        var id="<?=$field?>";
        var $parent=$('#'+id);
        var img=$parent.find('img');
        var preview=img.attr('src');
        var original=img.attr('data-src');
        var size=img.attr('data-size');
        var x=$parent.find('input[name="'+id+'[x]"]');
        var y=$parent.find('input[name="'+id+'[y]"]');
        var h=$parent.find('input[name="'+id+'[h]"]');
        var w=$parent.find('input[name="'+id+'[w]"]');
        var r=$parent.find('input[name="'+id+'[r]"]');
        var model=$parent.find('input[name="model['+id+']"]');
        var img_s=false;

        $parent.on('click','.start-crop',function(e){
            e.preventDefault();
            img=$parent.find('img');
            if (img.length==0){
                return; //Изображение не загружено
            }
            preview=img.attr('src');
            original=img.attr('data-src');
            img.attr('src',original);
            img.css({'width':'auto'}); //Ставим авто ширину, чтобы изображение заняло доступное пространство
            var options={
                onSelect:showCoords,
                onChange:showCoords
            };
            if (r.val()!=0){
                options.aspectRatio= r.val();
            }
            if (img.data('Jcrop'))
            {
                img.data('Jcrop').destroy(); //Объект API Jcrop, не документированные возможности
            }
            img.Jcrop(options);

            $parent.find('.start-crop').hide();
            $parent.find('.crop-panel').show();
        });

        $parent.on('click','.end-crop',function(e){
            e.preventDefault();
            if (img.data('Jcrop')) //Если юзер во время кропа изменил изображение, то эта проверка поможет
            {
                var params={
                    x: x.val(),
                    y: y.val(),
                    w: w.val(),
                    h: h.val(),
                    r: r.val()
                };
                $.post(BASE_URL+'storage/crop/'+model.val(),params,function(data){
                    preview=data;
                    img.attr('src',preview);
                    img.data('Jcrop').destroy();
                    img.css({'width':size+'px','height':'auto'});
                    $parent.find('.start-crop').show();
                    $parent.find('.crop-panel').hide();
                });
            } else {
                img.css({'width':size+'px','height':'auto'});
                $parent.find('.start-crop').show();
                $parent.find('.crop-panel').hide();
            }



        });

        $parent.on('click','.cancel-crop',function(e){
            e.preventDefault();

            if (img.data('Jcrop')) //Если юзер во время кропа изменил изображение, то эта проверка поможет
            {
                img.attr('src',preview);
                img.data('Jcrop').destroy(); //Объект API Jcrop, не документированные возможности
            }
            img.css({'width':size+'px','height':'auto'});
            $parent.find('.start-crop').show();
            $parent.find('.crop-panel').hide();
        });

        $parent.on("change",'input',function(){
            //todo: сделать изменение рамки при вводе в инпуты
        });

        function showCoords(c){
            img_s = $parent.find('.jcrop-holder img'); //Необходимо для вычисления разницы масштабирования
            var scaleX=img_s[0].naturalWidth/img_s.width(); //Изображение может быть больше, чем на экране
            var scaleY=img_s[0].naturalHeight/img_s.height();
            x.val(c.x*scaleX);
            y.val(c.y*scaleY);
            w.val(c.w*scaleX);
            h.val(c.h*scaleY);
        }
    });
</script>
