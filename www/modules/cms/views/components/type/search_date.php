<div class="input-append date span4">
    <?php echo Form::input("$field", Date::formatted_time($value,'d-m-Y'),array(
        'id'=>$field,
//        'class'=>'',
        'type'=>'text',
        'data-format'=>'dd-MM-yyyy'
    ))?>
    <span class="add-on">
                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                  </i>
                </span>
</div>

<script type="text/javascript">
    $(function() {
        var id="<?=$field?>";
        $('#'+id).parent().datetimepicker({
            language: 'ru',
            'pickTime':false
        });
    });
</script>