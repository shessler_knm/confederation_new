<?php
$field_s = $model->{$field."_s"};
$edit_url = Url::site(isset($params) ? Arr::get($params, 'edit_url', 'admin/storage/storage') : 'admin/storage/storage');
$del_url = Url::site(isset($params) ? Arr::get($params, 'del_url', 'admin/storage/delimg') : 'admin/storage/delimg');
$siz = isset($params) ? Arr::get($params, 'size', 100) : 100;
?>
<div id="<?=$field?>" class="storage control-group <?=$error ? 'error' : ''?>">
    <label for="<?=$field?>" class="control-label"><?=__($label)?></label>
    <?=Form::hidden("model[{$field}]", $model->$field)?>

    <div class="controls">


        <div class="preview">
            <?
            echo $model->loaded() && !empty($model->{$field}) && $field_s->loaded()
                ? HTML::anchor($field_s->url(), "Скачать", array(
                    'title' => __('Скачать'),
                    'class' => 'btn'
                ))
                : __("Файл не загружен");
            ?>

        </div>

        <a href="<?=$edit_url.Url::query(array(
            'id' => $field,
            'title' => __($label),
            'type' => 'File'
        ))?>" class="btn edit" title="<?=__('Изменить')?>
        "  data-title="<?=$label?>"
           data-type="File"
           data-field="<?=$field?>"><i class="icon-edit"></i></a>
        <a href="<?=$del_url."/{$model->$field}"?>" class="btn delete" title="<?=__('Удалить')?>
        "><i class="icon-remove"></i></a>
        <span class="help-inline"><?=$error?></span>
    </div>
</div>