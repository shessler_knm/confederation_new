<?php
/**
 * Данный компонент требует jQuery 1.7 + и Twitter Bootstrap JavaScript
 * @$params['options'] должен создержать uri до экшена с данными
 * $params['value'] содержит список уже установленных значений
 *
 */

$size = Arr::get($params, 'size', 10);
?>
<div class="control-group <?=$error ? 'error' : ''?>">
    <label for="<?=$field?>"
           class="control-label"><?=__($label)
        .(Arr::get($params, 'need_help')
            ? View::factory('components/help', array('model' => $model->object_name(), 'field' => $field))
            : null)?></label>

    <div class="controls">
        <input type="hidden" name="<?="model[$field][]"?>" value="0">
        <?php
        $attr = Arr::get($params, 'attr', array());
        $attr = array_merge($attr, array(
            'id' => $field,
            'class' => 'span4 select_extended',

        ));
        ?>
        <?=Form::select(null, Arr::get($params, 'options'), null, $attr)?> <a href="#" id="<?=$field?>"
                                                                              class="add_value"><i
            class="icon-plus"></i><?=__('Выбрать')?></a>

        <?php
        foreach ($params['value'] as $key => $value) {
            echo "<p>";
//                echo Form::hidden("model[$field][$key][key]",$key);
            echo Form::hidden("model[$field][]", $key, array(
                'id' => $key,
                'rel' => $value,
            ));
            echo '<span class="help-inline">'.$value.' <a rel="'.$key.'" rel_id="'.$field.'" href="#delete"><i class="icon-remove"></i></a></span>';
            echo "</p>";
        }
        ?>
        <span class="help-inline"><?=$error?></span>
    </div>
</div>
<script type="text/javascript">

    $(function () {
        var f = "<?=$field?>";
        var items = {};
        var source;
        var target = $('.add_value');
        var ids = [];
        var enabled = true;
        var create = function (name, field, target, id) {
            var value = $('<input type="hidden" />');
            value.attr('name', 'model[' + field + '][]');
            if (id) {
                value.attr('value', id);

                value.attr('id', 'value_' + id);
            } else {
                value.attr('value', name);

                value.attr('id', 'value_' + name);
            }
            value.attr('rel', name);

            var label = $('<span class="help-inline">' + name + ' <a rel_id="' + field + '" rel="' + value.attr('id') + '" href="#delete"><i class="icon-remove"></i></a></span>');
            var p = $('<p></p>');
            p.append(value).append(label);
            p.insertAfter(target);
        }
        var back = function (id, source) {
            var value = $('#' + id).val();
            var name = $('#' + id).attr('rel');
            var option = $('<option>', {
                value:value,
                text:name
            });
            source.append(option);

        };


        $('.select_extended option').each(function () {

            if ($('input[name="model[' + $(this).parent().attr('id') + '][]"][value="' + $(this).val() + '"]').val()) {
                $(this).remove();

            }

        });

        target.off('click');
        target.on('click', function (e) {
            e.preventDefault();
            source = $('#' + $(this).attr('id'));
            if (source.val()) {
                create(source.find(':selected').text(), $(this).attr('id'), $(this), source.val());
                source.find(':selected').remove();
            }
            ;
        });

        target.parent().on('click', 'p a[href="#delete"]', function (e) {
            e.preventDefault();
            source = $('#' + $(this).attr('rel_id'));
            back(this.rel, source);
            $(this).parents('p').remove();
        });

    });
</script>