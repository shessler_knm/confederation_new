<div class="control-group <?=$error ? 'error' : ''?>">
    <?=Form::label($field, __($label),array('class'=>'control-label'))?>

    <div class="controls">
        <a href="<?=$model->{$field}->edit_url(false)?>/<?=$model->id?>" class="btn"><?=__('Редактировать')?></a>
        <span class="help-inline"><?=$error?></span>
    </div>
</div>