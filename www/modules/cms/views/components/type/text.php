<div class="control-group <?=$error ? 'error' : ''?>">
    <label for="<?=$field?>" class="control-label"><?=__($label)?></label>

    <div class="controls">
        <textarea id="<?=$field?>" name="<?="model[$field]"?>" rows="4" class="editor"><?=$value?></textarea>

        <span class="help-inline"><?=$error?></span>
    </div>
</div>