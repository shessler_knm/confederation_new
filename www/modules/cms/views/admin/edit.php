<?php
//echo Form::open(null,array('class'=>'form-horizontal'));
$cp = Component::factory();
$outer = Component::factory('components/plain');


foreach ($fields as $field => $info) {
    if (Arr::get($info, 'edit') && (!Arr::get($info, 'loaded', false) || $model->loaded())) {
        $call = $info['type'];
        if ($call == 'embeded') {
            $outer->$call($info['label'], $model, $field, Arr::get($info, 'params'));
        } else {
            $cp->$call($info['label'], $model, $field, Arr::get($info, 'params'));
        }
    }
}

$cp->form_button_save();
$cp->form_button_save('cancel', "Назад");
if($model->loaded() && Authority::can('view',$model->object_name())) $cp->button_link($model->view_url(true), __('Просмотр'), array('class' => 'btn btn-info', 'target' => '_blank'));
if($model->loaded() && Authority::can('delete',$model->object_name())) $cp->button_link($model->delete_url(true), __('Удалить'), array('class' => 'btn btn-danger'));
$ref = new ReflectionClass($model);
if ($ref->hasMethod('form_button')) {
    $model->form_button($cp);
}


echo $cp->render();
echo $outer->render();
//echo Form::close();