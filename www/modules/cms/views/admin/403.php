<!DOCTYPE html>
<html>
<head>
    <?=$page->get_head()?>
</head>
<body>
<div class="hero-unit">
    <h1>Отказано в доступе</h1>
    <p>У вас не достаточно прав для выполнения этого действия</p>
    <p>Данный инцидент будет записан в журнал</p>
    <p>
        <a class="btn btn-primary btn-large" href="<?=$back_url ?>">
            Вернутся назад
        </a>
    </p>
</div>
<?=$page->content()?>
</body>
</html>