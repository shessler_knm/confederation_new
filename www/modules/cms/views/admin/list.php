<?php

$cp = Component::factory('components/form_search');

$need_search = false;
foreach ($fields as $field => $info) {
    if (Arr::get($info, 'search')) {
        $need_search = true;
        $query_field = $field;
        if (strpos($field, '.')) {
            $query_field = preg_replace('/[.]/', '_', $field);
        }
        $call = "search_".$info['type'];
        if (isset($info['params']) && isset($info['params']['widget'])) unset($info['params']['widget']);
        $cp->$call($info['label'], $collection, $field, Arr::get($info, 'params'), $request->query($query_field));
    }
}
$cp->form_button_save('search', 'Найти');
$cp->button_link(Request::current()->uri(), 'Сбросить');
//$cp->form_button_reset('cancel');
if($need_search){
    echo $cp->render();
}


$head = array();
foreach ($fields as $field => $info) {
    if (Arr::get($info, "head")) {
        $head[$field] = $info['label'];
    }
}

echo Form::open(Request::current()->url('http') , array('class' => 'form form-horizontal'));
echo "<div class='btn-group'>";

$cp = Component::factory('components/plain');

if (Authority::can('create',$collection->object_name())) {
    $cp->button_link($collection->edit_url(true), "Добавить", array('class' => 'btn btn-success'));
}
if (Authority::can('delete',$collection->object_name())) {
    $cp->form_button_save('delete', "Удалить выделенное", array('class' => 'btn btn-danger'));
}

$ref = new ReflectionClass($collection);
if ($ref->hasMethod('form_list_button')) {
    $collection->form_list_button($cp);
}

echo $cp->render();

echo "</div>";
echo Component::factory()->editable_table($model, $head);
echo Form::close();
echo $pagination;
