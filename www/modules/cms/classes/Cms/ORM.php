<?php defined('SYSPATH') OR die('No direct access allowed.');


class Cms_ORM extends Kohana_ORM implements IC_Editable, IC_Actions{

    const ACTION_CREATE="CREATE";
    const ACTION_UPDATE="UPDATE";
    const ACTION_DELETE="DELETE";

    /**
     * @var string Язык по умолчанюи для моделей
     */
    protected $_default_lang='ru';

    public function default_lang(){
        return $this->_default_lang;
    }
    /**
     * Признак необходимости логирования изменений
     * @deprecated
     * @var bool
     */
    protected $_need_log=false;

    protected $_action_type;

    protected $_route;
    /**
     * Массив значений которые будут сохранены после создания модели
     * Используется при присваивании значений к has_many полям
     * @var array
     */
    protected $_deffered_values=array();
    /**
     * Массив "частичных моделей" это модели для хранения переводимых полей
     * Например если таблица называется pages то ее переводимые поля должны хранится в page_partials
     * Имя частичной модели формируется путем добавления к имени текущей модели суфикса _Partial
     * Каждый ключ в данном массиве соотвествует языковому коду, а его значение загруженная модель с этим языком
     * @var array
     */
    protected $_partial_models=array();
    protected $_partial_models_temp=array();

    public function partials(){
        return $this->_partial_models;
    }

    public $pagination;

    /**
     * Получить значения поля по его имени
     * Данный метод используется при формировании списка элементов в админке, а также в Cms_ORM::as_array_extended()
     * Применяется для подмены оригнального значения с целью улучшения вывода, например подмена полного текста на обрезанный
     * или подмена ключа на название
     * @param string $field
     * @return mixed
     */
    public function get_field($field)
    {
        return $this->__get($field);
    }

    /**
     * Переопределенный метод, поддерживает два механизма мультязычности на осное "частичных моделей"
     * и на основе языковых суфиксов у моделей
     * @param string $column
     * @return mixed
     * @throws Exception
     */
    public function __get($column) {
        /** @var Cms_ORM $this */
        if ($this instanceof Cms_Interface_Partial){
            $desc=$this->fields_description();
            if (isset($desc[$column]) && isset($desc[$column]['partial']) && $desc[$column]['partial']){
                $value=$this->get_partial_field($column,I18n::$lang);
                if (empty($value)){
                    $value=$this->get_partial_field($column,$this->_default_lang);
                }

                return $value;
            }

        }

        try
        {
            return parent::__get($column);
        } catch (Exception $e)
        {
            try
            {
                $value=parent::__get($column."_".I18n::lang());
                if (empty($value))
                {
                    $value=parent::__get($column."_".$this->_default_lang);
                }

                return $value;
            } catch (Exception $e2)
            {
                throw $e;
            }
        }
    }

    public function save(Validation $validation = NULL)
    {
        parent::save($validation);
        if (count($this->_deffered_values)>0){
            $this->values($this->_deffered_values);
            $this->_deffered_values=array();
            parent::save(); //чтобы метод сам себя не вызывал, а просто сохранял
        }
        if ($this instanceof ISearchable) {
            Search::update($this);
        }
        if ($this instanceof Cms_Interface_Partial){
            foreach ($this->_partial_models as $model){
                $model->object_id=$this->id; //На случай если айди не был раньше установлен по причине не загруженности модели
                $model->save();
            }
        }
        if ($this instanceof Cms_Interface_Tree){
            $this->tree_update(); //Обновление структуры дерева
        }
        Event::instance()->dispatch('orm.after.save',$this);
        return $this;
    }

    public function delete()
    {
        $id=$this->pk();
        try {
        if ($this instanceof ISearchable) {
            Search::delete($this);
        }
        if ($this instanceof Cms_Interface_Tree){
            $this->tree_delete();//Обновление структуры дерева
        }
            Database::instance()->begin();
            $r=parent::delete();
            Database::instance()->commit();
        } catch (Exception $e){
            Database::instance()->rollback();
            throw $e;
        }
        Event::instance()->dispatch('orm.after.delete',$id,$this);
        return $r;

    }

    public function update(Validation $validation = NULL)
    {
        $this->_action_type = ORM::ACTION_UPDATE;
        //Фикс бага. Нужно выполнять валидацию в случае если не было изменений в модели, но внешний валидатор пришел
        if (empty($this->_changed) && $validation!=NULL)
        {
            // Nothing to update
            $this->check($validation);
        }elseif(empty($this->_changed) && $this instanceof Cms_Interface_Partial){
            // Nothing to update
            $this->check($validation);
        }


        parent::update($validation);


        return $this;
    }

    public function check(Validation $extra_validation = NULL)
    {
        if ($this instanceof Cms_Interface_Partial){
//            if(!count($this->_partial_models)){
//                $validation = $this->_validation();
//                $array = $this->_validation;
//
////                $array->_data[] = 'не заполненно ни одно языковое поле';
//                $exception = new ORM_Validation_Exception($this->errors_filename(), $array);
////                $exception->add_object()
////                return new Kohana_Validation_Exception(Validation::factory(array()),'не заполненно ни одно языковое поле');
//                return null;
//            }
            foreach ($this->_partial_models as $model){
                    $model->check(); //Побочный эфект: не произойдет валидации основной модели, только частичной

            }

        }
        return parent::check($extra_validation);

    }


    /**
     * Возвращает тип действия которое произошло c моделью (UPDATE, CREATE, DELETE)
     * Работает только если исползуется метод ORM::save() или ORM::delete()
     * @deprecated Метод юзался при логировании, логирование будет вынесено из класса
     * @return string
     */
    public function action_type()
    {
        return $this->_action_type;

    }

    /**
     * Возвращает ORM модель последней записи в логе связанной с данной моделью
     * или FALSE в случаи отсутсвия таковой
     * @deprecated Логирование будет вынесено за пределы класса
     * @return bool|ORM
     */
    public function get_last_change_log()
    {
        if (!$this->_need_log) return false;
        $log=ORM::factory('user_log')->where('class','=',get_class($this))->where('pk','=',$this->pk())->order_by('datetime','DESC')->find();
        if (!$log->loaded()) return false;
        return $log;
    }
    /**
     * Доступ к логам модели, вернет не загруженную ORM модель, сделанно для того чтобы можно было установить до условия и лимиты
     * @deprecated Логирование будет вынесено за пределы класса
     * @return bool|ORM
     */
    public function get_logs()
    {
        if (!$this->_need_log) return false;
        $log=ORM::factory('user_log')->where('class','=',get_class($this))->where('pk','=',$this->pk());
        return $log;
    }

    /**
     * Урл для просмотра
     */
    public function view_url($uri = false)
    {
        $_uri = $this->_route."/view/{$this->id}";
        return $uri ? $_uri : URL::site($_uri);
    }

    /**
     * Урл для редактирования
     */
    public function edit_url($uri = false)
    {
        $id=intval($this->id);
        $_uri = $this->route()."/edit/{$id}";
        if ($this instanceof Cms_Interface_Tree){
            $parent=$this->{$this->tree_parent_field()};
            $_uri.='/'.$parent;
        }

        return $uri ? $_uri : URL::site($_uri);
    }

    /**
     * Урл для вызова внешнего списка, используется в external_select
     * @param bool $uri
     * @return string
     */
    public function external_list($uri = false){
        $_uri = $this->route()."/external_list/{$this->id}";
        return $uri ? $_uri : URL::site($_uri);
    }

    /**
     * Урл для удаления
     */
    public function delete_url($uri = false)
    {
        $_uri = $this->route()."/delete/{$this->id}";
        return $uri ? $_uri : URL::site($_uri);
    }

    /**
     * Урл списка элементов
     * @param bool $uri
     * @return string
     */
    public function list_url($uri = false)
    {

        $_uri = $this->route()."/list";

        return $uri ? $_uri : URL::site($_uri);
    }

    /**
     * Проверка прав
     * @param $action Действие для которого проверяются права (edit|delete|view)
     * @param $user Пользователь для которого идет проверка (тип зависит от реализации)
     * @deprecated
     * @return boolean
     */
    public function permission($action, $user)
    {
        return true;
    }

    /**
     * Кэш метаданных описывающих модель
     * @var array
     */
    protected static $_field_description;

    /**
     * Кэшируемые метаданные модели. Не переопределяйте эту фунцию,
     * для описани метданных используйте get_fields_description
     * @return array
     */
    public function fields_description()
    {
        $class = get_class($this);
        if (!isset(self::$_field_description[$class])){
            self::$_field_description[$class]=null;
        }
        if (self::$_field_description[$class]!=null){
            return self::$_field_description[$class];
        }
        self::$_field_description[$class]=$this->get_fields_description();
        return self::$_field_description[$class];
    }

    /**
     * Метаданные модели, поисывающие модель
     * @return array
     */
    protected function get_fields_description(){
        return array();
    }

    /**
     * Автогенератор подписей к полям для ошибок валидации
     * @return array
     */
    public function labels()
    {
        $fields = $this->fields_description();
        $data = array();
        foreach ($fields as $name => $value) {
            if (isset($value['label'])) {
                if (isset($value['params']) && isset($value['params']['widget']) && $value['params']['widget']=='multilang'){
                    $data[$name."_ru"] = $value['label'];
                    $data[$name."_kz"] = $value['label'];
                    $data[$name."_en"] = $value['label'];
                } else {
                    $data[$name] = $value['label'];
                }
            }
        }
        return $data;
    }

    /**
     * Расширенный метод присваивания, умеет работать с has_many молями и частичными моделями
     * @param string $column
     * @param mixed $value
     * @return $this|ORM
     */
    public function set($column, $value)
    {
        if (isset($this->_has_many[$column]) && isset($this->_has_many[$column]['through'])) {
            $value = $this->run_filter($column, $value);
            $this->set_multiple($column, $value);
            return $this;
        }
        /** @var Cms_ORM $this */
        if ($this instanceof Cms_Interface_Partial){
            if ($column=='partials'){
                foreach ($value as $lang=>$values){
                    foreach ($values as $f=>$v){

                        $this->set_partial_field($f,$v,$lang);
                    }
                }
                return $this;
            }
        }

        return parent::set($column, $value);
    }

    /**
     * Расширенный метод для работы с has_many полями и частичными моделями
     * @param array $values
     * @param array $expected
     * @return ORM
     */
    public function values(array $values, array $expected = NULL)
    {
        if ($expected === NULL) {
            $expected = array_merge(array_keys($this->_table_columns), array_keys($this->fields_description()));

            if ($this instanceof Cms_Interface_Partial){
                $expected[]="partials";
            }

            // Don't set the primary key by default
            unset($values[$this->_primary_key]);
        }
        return parent::values($values, $expected);
    }

    /**
     * Метод для присвоения значений has_many алисасам, содержамщим through таблицу
     * @param $field
     * @param $values
     */
    public function set_multiple($field, $values)
    {
        if (!$this->loaded()){
            $this->_deffered_values[$field]=$values;
            return;
        }
        $this->remove($field);
        $values = array_diff($values, array(0)); //Удаляет элемент со значением 0

        if ($values)
            $this->add($field, $values);
    }

    /**
     * Генерация роута, меняет поведение в зависимости от нахождения в админки или публичной части
     * @return string
     */
    public function route()
    {
        if(Request::current()){
            return (Request::current()->directory() == 'Admin') ? "admin/{$this->_route}" : "{$this->_route}";
        }else{
            return $this->_route;
        }

    }

    /**
     * Возрвщает массив ключ значения для для таблицы, пригодно для использвания внутри Form::select
     * @param string $id Значения для ключе массива
     * @param null $title Значение для значений массива
     * @param string $order_by направление сортировки, в качестве поля используется $title
     * @return array
     */
    public function select_options($id = 'id', $title = null, $order_by = 'ASC')
    {
        $order_field = $title ? $title : $id;
        return $this->reset(false)->order_by($order_field,$order_by)->find_all()->as_array($id, $title);
    }

    /**
     * Массив списка действий над моделью в админке
     * Структура:
     * array(
            array(
                'title'=> //Название действия
     *          'uri'=> //uri для выполения действия
     *          'type'=> //не обязательный параметр, может принимать значения submenu (подменю) или divider (разделитель)
     *      ),
     * )
     * @param null $user
     * @return array
     */
    public function actions($user=null)
    {
        $view = array(
            'title' => 'Просмотр',
            'uri' => $this->view_url(true)
        );
        $edit = array(
            'title' => 'Редактировать',
            'uri' => $this->edit_url(true)
        );
        $delete = array(
            'title' => 'Удалить',
            'uri' => $this->delete_url(true)
        );
        $menu = array(
//            $view,
//            $edit,
//            $delete,
        );
        Authority::can('view',$this->object_name(),$this) && $menu[]=$view;
        Authority::can('edit',$this->object_name(),$this) && $menu[]=$edit;
        Authority::can('delete',$this->object_name(),$this) && $menu[]=$delete;
        return $menu;
    }

    /**
     * @deprecated вместо данного метода нужно использовать Pagination::apply() подробнее в Cms_Admin::action_list()
     * @param int $per_page
     * @param bool $lang
     */
    public function pagination($per_page = 10, $lang = true)
    {
        if ($lang) $this->lang();

        $pagin = Pagination::factory(array(
                'items_per_page' => $per_page,
                'total_items' => $this->reset(false)->count_all()
            )
        );
        $pagin->apply($this);
        $this->pagination = $pagin->render();
    }


    /**
     * Расширенный метод, умеет автоматом джойнить has_many таблицу и строить условие по внешнему ключу
     * @param mixed $column
     * @param string $op
     * @param mixed $value
     * @return $this
     */
    public function where($column, $op, $value)
    {
        if (!is_object($column) && isset($this->_has_many[$column])) {
            if (isset($this->_has_many[$column]['through'])) {
                $through = $this->_has_many[$column]['through'];
                $fgk = $this->_has_many[$column]['foreign_key'];
                $fk = $this->_has_many[$column]['far_key'];
                $is_joined=false;
                foreach ($this->_db_pending as $act) {
                    if ($act['name']=='join'){
                        $table=$act['args'][0];
                        if (is_array($act['args'][0])){
                            $table=$table[0];
                        }
                        if ($table==$through){
                            $is_joined=true;

                            break;
                        }
                    }
                }
                if (!$is_joined){
                    $this->join(array($through,$column));
                    $this->on("$column.$fgk", '=', "{$this->object_name()}.{$this->primary_key()}");
                }

                return parent::where("$column.$fk", $op, $value);
            }
        }
        return parent::where($column, $op, $value);
    }

    public function or_where($column, $op, $value)
    {
        if (!is_object($column) && isset($this->_has_many[$column])) {
            if (isset($this->_has_many[$column]['through'])) {
                $through = $this->_has_many[$column]['through'];
                $fgk = $this->_has_many[$column]['foreign_key'];
                $fk = $this->_has_many[$column]['far_key'];
                $is_joined=false;
                foreach ($this->_db_pending as $act) {
                    if ($act['name']=='join'){
                        $table=$act['args'][0];
                        if (is_array($act['args'][0])){
                            $table=$table[0];
                        }
                        if ($table==$through){
                            $is_joined=true;

                            break;
                        }
                    }
                }
                if (!$is_joined){
                    $this->join(array($through,$column));
                    $this->on("$column.$fgk", '=', "{$this->object_name()}.{$this->primary_key()}");
                }

                return parent::or_where("$column.$fk", $op, $value);
            }
        }
        return parent::or_where($column, $op, $value); // TODO: Change the autogenerated stub
    }


    /**
     * Выолняет count_all() для запрос с групирвокой
     * Медленный метод, не желательно его использовать
     * @return mixed
     */
    public function group_count()
    {
        $selects = array();

        foreach ($this->_db_pending as $key => $method) {
            if ($method['name'] == 'select') {
                // Ignore any selected columns for now
                $selects[] = $method;
                unset($this->_db_pending[$key]);
            }
        }

        if (!empty($this->_load_with)) {
            foreach ($this->_load_with as $alias) {
                // Bind relationship
                $this->with($alias);
            }
        }

        $this->_build(Database::SELECT);

        $last = $this->_db_builder->from(array($this->_table_name, $this->_object_name))->select($this->_object_name.'.*')->__toString();


        // Add back in selected columns
        $this->_db_pending = array_merge($this->_db_pending,$selects);

        $this->reset();

//        Return the total number of records in a table
//        $last=$this->_db->last_query();
        $q = DB::query(Database::SELECT, "SELECT COUNT(*) as count FROM (:sub) as t");
        $q2 = Db::expr($last);
        $q->bind(':sub', $q2);
        return $q->execute()->get('count');
    }

    public function has_group_by(){
        foreach ($this->_db_pending as $act){
            if ($act['name']=='group_by'){
                return true;
            }
        }
        return false;
    }

    /**
     * @deprecated Рудимент с g-global
     */
    private function lang()
    {
    }

    /**
     * Возвращает массив ключей, преобразованных в строку
     * в строку преобразуется для использования на строне клиента в js
     * устраняет неоднозначнность, чтобы числовые значения в JSON шли как строки
     * используется в Cms_API
     * @param string $id
     * @param string $order_field
     * @param string $order_direction
     * @return array
     */
    public function select_keys($id='id',$order_field='id',$order_direction='asc'){
        $selected_options=array_keys($this->select_options($id,$order_field,$order_direction));
        return array_map('strval',$selected_options);
    }


    /**
     * Тоже самое что и as_array, но для получения значения полей используется get_field
     * используется в Cms_API
     * @return array
     */
    public function as_array_ext(){
        $object = array();

        $fields_description = $this->fields_description();
        $fields_description = count($fields_description)>0?$fields_description:$this->_object;
        foreach ($fields_description as $column => $value)
        {
            // Call __get for any user processing
            $object[$column] = $this->get_field($column);
        }

//        foreach ($this->_related as $column => $model)
//        {
//            // Include any related objects that are already loaded
//            $object[$column] = $model->as_array();
//        }

        return $object;
    }

    /**
     * Метод используется в классе Cms_API для получения списка кнопок
     */
    public function buttons(){
        return array(
            'list'=>array(
                array('type'=>'link','title'=>'Добавить','link'=>$this->edit_url(),'class'=>'btn-success'),
                array('type'=>'button','title'=>'Удалить','click'=>'$deleteModels','class'=>'btn-danger'),
            ),
            'edit'=>array(
                array('type'=>'button','title'=>'Сохранить','click'=>'$saveModel','class'=>'btn btn-success'),
                array('type'=>'button','title'=>'Назад','click'=>'$cancelModel','class'=>'btn'),
                array('type'=>'button','title'=>'Удалить','click'=>'$deleteModel','class'=>'btn btn-danger')
            ),
            'search'=>array(
                array('type'=>'button','title'=>'Поиск','click'=>'$search','class'=>'btn btn-success'),
                array('type'=>'button','title'=>'Сбросить','click'=>'$searchReset','class'=>'btn'),
            ),
        );
    }
    
     /**
     * Фильтр для фильтрации html
     * c исп. сторонней либы
     * htmlpurifier
     */

    public static function clear_html($dirty_html)
    {
        require_once Kohana::find_file('vendors/htmlpurifier/library/','HTMLPurifier.auto');

        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.SafeIframe', true);
        $config->set('URI.SafeIframeRegexp', '%^http://(www.youtube(?:-nocookie)?.com/embed/|player.vimeo.com/video/)%'); //allow YouTube and Vimeo
        $config->set('Attr.AllowedFrameTargets', array("_blank"));
        $purifier = new HTMLPurifier($config);
        $clean_html = $purifier->purify($dirty_html);
        return $clean_html;
    }

    public function ng_editRoles(){
        return array();
    }

    public function ng_listRoles(){
        return array();
    }

    public function ng_edit_url($uri = false)
    {
        return $this->_object_name."/edit/"; // TODO: Change the autogenerated stub
    }

    public function ng_list_url($uri = false)
    {
        return $this->_object_name."/list/"; // TODO: Change the autogenerated stub
    }

    public function ng_buttons(){
        return array(
            'list'=>array(
                array('type'=>'button','title'=>'Добавить','link'=>'#','click'=>'$editModel','class'=>'btn-success'),
                array('type'=>'button','title'=>'Удалить','link'=>'#','click'=>'$deleteModels','class'=>'btn-danger'),
            ),
            'edit'=>array(
                array('type'=>'button','title'=>'Сохранить','link'=>'#','click'=>'$saveModel','class'=>'btn btn-success'),
                array('type'=>'button','title'=>'В список','link'=>'#','click'=>'$cancelModel','class'=>'btn'),
                array('type'=>'button','title'=>'К родителю','link'=>'#','click'=>'$parentModel','class'=>'btn'),
                array('type'=>'button','title'=>'Удалить','link'=>'#','click'=>'$deleteModel','class'=>'btn btn-danger')
            ),
            'search'=>array(
                array('type'=>'button','title'=>'Поиск','link'=>'#','click'=>'$search','class'=>'btn btn-success'),
                array('type'=>'button','title'=>'Сбросить','link'=>'#','click'=>'$searchReset','class'=>'btn'),
            ),
        );
    }

    public function ng_actions($user=null)
    {
        $menu = array(
            array(
                'title' => 'Просмотр',
//                'link' => '#'
            ),
            array(
                'title' => 'Редактировать',
                'click' => '$editModel',
//                'link'=>'',
            ),
            array(
                'title' => 'Удалить',
//                'link'=>'',
//                'link' => $this->delete_url()
                'click' => '$deleteModel'
            ),
        );
        return $menu;
    }

    /**
     * Возвращает "частичную модель" для текущей модели в случае если модель реализует Cms_Interface_Partial
     * @return ORM
     * @throws Kohana_Exception
     */
    public function partial_model(){
        /** @var Cms_ORM $this */
        if (!($this instanceof Cms_Interface_Partial)){
            throw new Kohana_Exception("This object doesn't implement Cms_Interface_Partial");
        }
        $name=substr(get_class($this),6);
        return ORM::factory($name."_Partial");

    }

    /**
     * Возвращает поле из частичной модели в зависимости от языка
     * !!! сохранения при этом не происходит, для сохранения нужно вызывать метод save у текущей модели
     * @param $field Имя поля в частичной модели
     * @param $lang Язык
     * @return mixed
     */
    public function get_partial_field($field,$lang,$take_partial_field = false){
        //на тот случай если мультиязычное поле было заджойнено
        if (isset($this->_object[$field]) && Valid::not_empty($this->_object[$field]) && !$take_partial_field){
            return $this->_object[$field];
        }

        if (isset($this->_partial_models[$lang])){
            return $this->_partial_models[$lang]->{$field};
        }


        $this->_partial_models[$lang]=$this
            ->partial_model()
            ->where('object_id','=',$this->pk())
            ->where('lang','=',$lang)
            ->find()
        ;
        $value = $this->_partial_models[$lang]->{$field};
        if(!$value){
            unset($this->_partial_models[$lang]);
        }
        return $value;
    }

    /**
     * Устанавливает значение в поле частичной модели с учетом языка
     * !!! сохранения при этом не происходит, для сохранения нужно вызывать метод save у текущей модели
     * @param $field
     * @param $value
     * @param $lang
     */
    public function set_partial_field($field,$value,$lang){

        if (!isset($this->_partial_models[$lang])){
            if (!Valid::not_empty($value)) {
//                return; //Значение пустое, модели нет, вероятно и не надо
            }
            $this->_partial_models[$lang]=$this
                ->partial_model()
                ->where('object_id','=',$this->pk())
                ->where('lang','=',$lang)
                ->find()
            ;
        }

        if (!$this->_partial_models[$lang]->loaded()){
            $this->_partial_models[$lang]->object_id=$this->id;
            $this->_partial_models[$lang]->lang=$lang;
        }
        $this->_partial_models[$lang]->{$field}=$value;
    }


    /**
     * Родительское поле в структуре дерева
     * @return string
     */
    public function tree_parent_field(){
        return "parent_id";
    }

    /**
     * Текстовое поле играющее роль "навзания" в дереве
     * @return string
     */
    public function tree_title_field(){
        return "title";
    }

    /**
     * Возвращает модель хранящее дерево для текущей модели
     * @return ORM
     */
    public function tree_model(){
        $name=substr(get_class($this),6);
        $name="{$name}_Tree";
        return ORM::factory($name);
    }

    /**
     * Обновляет структуру дерева для текущего узла
     */
    public function tree_update(){
        /** @var Cms_Model_Tree $model */
        $model=$this->tree_model();
        $model->insert_node($this->id,$this->{$this->tree_parent_field()});
    }

    /**
     * Удалеят текущий узел из дерева, не трогая при этом его дочернии
     */
    public function tree_delete(){
        /** @var Cms_Model_Tree $model */
        $model=$this->tree_model();
        $model->remove_node($this->id);
        $my_parent=$this->{$this->tree_parent_field()};
        $class=get_class($this);
        $my_model=new $class;
        $collection = $my_model->where($this->tree_parent_field(),'=',$this->id)->find_all();
        foreach ($collection as $child){
            $child->{$this->tree_parent_field()}=$my_parent;
            $child->save();
        }
    }

    /**
     * Вернет всех предков для текущего узла
     * @return mixed
     */
    public function tree_ancestors(){
        $model=$this->tree_model();
        $class=get_class($this);
        $my_model=new $class;
        return $my_model
            ->join(array($model->table_name(),'path'))->on("path.a",'=',$this->object_name().".id")
            ->where('path.d','=',$this->pk())
            ->where('path.a','!=',$this->pk())
            ->order_by('path.l','desc');
    }

    /**
     * Вернет всех потомков для текущего узла
     * @return mixed
     */
    public function tree_descendant(){
        $model=$this->tree_model();
        $class=get_class($this);
        $my_model=new $class;
        return $my_model
            ->join(array($model->table_name(),'path'))->on("path.d",'=',$this->object_name().".id")
            ->where('path.a','=',$this->pk())
            ->where('path.d','!=',$this->pk())
            ->order_by('path.l','asc');
    }

    /**
     * Урл для просмотра дерева
     * @param bool $uri
     * @return string
     */
    public function tree_url($uri=false){
        $_uri = $this->route()."/tree".($this->id?"/{$this->id}":'');

        return $uri ? $_uri : URL::site($_uri);
    }

    public function tree_parent_url($uri=false){
        $_uri = $this->route()."/tree".($this->{$this->tree_parent_field()}?"/{$this->{$this->tree_parent_field()}}":'');

        return $uri ? $_uri : URL::site($_uri);
    }

    /**
     * Исползьзуется в качестве фильтра, для преобразования пустого значения в null
     * Применяется когда внешнему ключу нужно установить NULL, но форма может прислать только 0 или пустую строку
     * @param $value
     * @return null
     */
    public static function to_null($value){
        if (empty($value)){
            return null;
        }
        return $value;
    }

    /**
     * Джойним партиалы, и выкидываем все у чего нет перевода на текущий язык
     * @param null $lang
     * @return $this
     */
    public function join_partial($lang=null){
        if ($lang==null){
            $lang=I18n::$lang;
        }
        $object_name = $this->partial_model()->object_name();
        $this->join_partial_all()
            ->where($object_name.".lang",'=',$lang);
        return $this;
    }

    /**
     * Джойним таблицу партиалов, без уазания языка
     * @return $this
     */
    public function join_partial_all(){
        $table_name = $this->partial_model()->table_name();
        $object_name = $this->partial_model()->object_name();
        $this->join(array($table_name, $object_name),'LEFT')
            ->on($object_name.'.object_id','=',$this->object_name().".".$this->primary_key());
        return $this;
    }

}