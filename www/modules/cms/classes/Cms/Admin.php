<?php
/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 21.11.12
 * Time: 10:45
 * To change this template use File | Settings | File Templates.
 */
class Cms_Admin extends Cms_Controller_Admin
{
    protected $need_auth = true;

    public $option = array();


    protected $menu = array();
    protected $values;
    /**
     * @var string URI для входа
     */
    protected $login_uri = "user/login";

    protected $crumbs = array();

    public function before()
    {
        try {
            parent::before();

        } catch (HTTP_Exception_403 $e) {
            $this->request->action('403');
        }

//
//        $this->menu['main'] = array(
//            'title' => 'Главная',
//            'active' => false,
//            'url' => URL::site('admin')
//        );

        $this->template->bind("menu", $this->menu);
        $this->template->bind("crumbs", $this->crumbs);

        $this->menu += $this->init_menu();

        foreach ($this->menu as $key => $value) {
            if (Authority::cannot('list', strtolower($key))) {
                unset($this->menu[$key]);
            }
        }

        if (isset($this->menu[Arr::get($this->option, 'model')])) {
            $this->menu[$this->option['model']]['active'] = true;
            $this->page->title($this->menu[$this->option['model']]['title']);
        } else {
            $this->page->title("Панель управления");
            $this->menu['main']['active'] = true;
        }

        $this->crumbs[] = array('title' => 'Панель управления', 'uri' => 'admin/panel/index');
        if (isset($this->menu[Arr::get($this->option, 'model')])) {
            $this->menu[$this->option['model']]['active'] = true;
            $this->page->title($this->menu[$this->option['model']]['title']);
            $this->crumbs[] = array('title' => $this->page->title(), 'uri' => $this->menu[$this->option['model']]['url']);
        } else {
            $this->page->title("Панель управления");
//            $this->menu['main']['active'] = true;
        }

        Component::errors($this->errors);
    }

    public function after()
    {
        if (!$this->request->is_external() && ($this->request->post('only_content') || $this->request->query('only_content'))) {
            $this->response->body($this->page->content());
            return;
        }
        parent::after();
    }


    public function factory_model()
    {
        return ORM::factory($this->option['model']);
    }

    /**
     * Активация текущего пункта меню
     * @param $model
     */
    public function menu($model)
    {
        if (Arr::get($this->menu, $item = $model->route())) {
            $this->page->title($this->menu[$item]['title']);
            $this->menu[$item]['active'] = true;
            $this->menu['main']['active'] = false;
        }
    }

    /**
     * Установка массива для формирования меню админки
     * @return array
     */
    public function init_menu()
    {
        return array(
            'main' => array(
                'active' => false,
                'url' => URL::site('admin'),
                'title' => 'Главная',
            )
        ) + CMS::admin_menu();

    }

    public function scripts()
    {
        return parent::scripts() + array(
            'jquery' => 'js/lib/jquery-1.9.0.min.js',
//            'jquery-ui'=>'js/lib/jquery-ui-1.9.1.custom.min.js',
            'bootstrap' => 'js/lib/bootstrap/js/bootstrap.min.js',
            'bootstrap-datetimepicker' => 'js/lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
            'bootstrap-datetimepicker-ru' => 'js/lib/bootstrap-datetimepicker/js/ru.js',
            'redactor' => 'js/lib/redactor/redactor.min.js',
            'storage' => 'js/lib/storage.js',
//            'date_time_picker' => 'js/lib/datetimepicker.js',
            'jquery.Jcrop.min.js' => 'js/lib/jcrop/js/jquery.Jcrop.min.js',
            'admin' => 'js/admin.js',
            'redactor_lang' => $this->page->find_content('js/lib/redactor/ru.js')
        );
    }

    public function styles()
    {
        return parent::styles() + array(
//            'jquery-ui'=>'js/lib/jquery-ui-1.9.1.custom/css/smoothness/jquery-ui-1.9.1.custom.min.css',
            'bootstrap' => 'js/lib/bootstrap/css/bootstrap.min.css',
            'bootstrap-datetimepicker' => 'js/lib/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
            'redactor' => 'js/lib/redactor/redactor.css',
            'jquery.Jcrop.min.css' => 'js/lib/jcrop/css/jquery.Jcrop.min.css',
            'admin' => 'css/admin.css'
        );
    }


    public function action_list()
    {
        $this->page->content(
            View::factory("admin/list")
                ->bind('model', $model)
                ->bind('collection', $collection)
                ->bind('request', $this->request)
                ->bind('fields', $fields)
                ->bind('pagination', $pagination)
        );
        $this->_mass_delete();
        $collection = $this->factory_model();
        $this->menu($collection);
        $fields = $collection->fields_description();
        Event::instance()->dispatch("admin.list.init", $collection);
        $collection = $this->_list_init($collection);
        $this->_search($collection, $fields);
//        $ruri = $_SERVER['REQUEST_URI'];
//        if (strpos(URL::base(), $ruri) === 0) {
//            $ruri = substr($ruri, strlen(URL::base()));
//        }
        $pagination = Pagination::factory(array(
            'total_items' => $collection->has_group_by() ? $collection->reset(false)->group_count() : $collection->reset(false)->count_all()
        )); //Критично для embeded полей
        $pagination->apply($collection);
        $model = $collection->find_all();
    }

    public function action_tree()
    {
        $this->page->content(
            View::factory("admin/tree")
                ->bind('model', $model)
                ->bind('collection', $collection)
                ->bind('request', $this->request)
                ->bind('fields', $fields)
                ->bind('pagination', $pagination)
                ->bind('ancestors', $ancestors)
                ->bind('tree_model', $tree_model)
        );
        $id = $this->request->param('id');
        $tree_model = $this->factory_model();
        $tree_model->where('id', '=', $id)->find();
        if (!$tree_model->loaded() && $id) {
            $this->response->status(404);
            throw new HTTP_Exception_404;
        } elseif ($tree_model->loaded()) {
            $ancestors = $tree_model->tree_ancestors()->find_all();
        } else {
            $ancestors = array();
        }
        $this->_mass_delete();
        /** @var Cms_ORM $collection */
        $collection = $this->factory_model();
        if ($this->request->query('search')) {
            if ($tree_model->loaded()) {
                $collection = $tree_model->tree_descendant();
            }
        } else {
            $collection->where($collection->tree_parent_field(), '=', $id);
        }
        $this->menu($collection);
        $fields = $collection->fields_description();
        Event::instance()->dispatch("admin.tree.init", $collection);
        $collection = $this->_list_init($collection);
        $this->_search($collection, $fields);
//        $ruri = $_SERVER['REQUEST_URI'];
//        if (strpos(URL::base(), $ruri) === 0) {
//            $ruri = substr($ruri, strlen(URL::base()));
//        }
        $pagination = Pagination::factory(array(
            'total_items' => $collection->has_group_by() ? $collection->reset(false)->group_count() : $collection->reset(false)->count_all()
        )); //Критично для embeded полей
        $pagination->apply($collection);
        $model = $collection->find_all();
    }

    public function action_external_list()
    {
        $this->render = false;
        $this->page->content(
            View::factory("admin/external_list")
                ->bind('model', $model)
                ->bind('collection', $collection)
                ->bind('request', $this->request)
                ->bind('fields', $fields)
                ->bind('pagination', $pagination)
        );
//        $this->_mass_delete();
        $collection = $this->factory_model();
//        $this->menu($collection);
        $fields = $collection->fields_description();
        Event::instance()->dispatch("admin.external_list.init", $collection);
        $collection = $this->_list_init($collection);
        $this->_search($collection, $fields);
//        $ruri = $_SERVER['REQUEST_URI'];
//        if (strpos(URL::base(), $ruri) === 0) {
//            $ruri = substr($ruri, strlen(URL::base()));
//        }
        $pagination = Pagination::factory(array(
            'total_items' => $collection->has_group_by() ? $collection->reset(false)->group_count() : $collection->reset(false)->count_all()
        )); //Критично для embeded полей
        $pagination->apply($collection);
        $model = $collection->find_all();
        $this->response->body($this->page->content());
    }

    public function _mass_delete()
    {
        $model = $this->factory_model();

        if ($this->request->post('delete')) {
            if (Authority::cannot('delete', $this->resource())) {
                $this->redirect('admin/panel/403');
            }
            $ids = $this->request->post('model');
            $count = 0;
            if (count($ids) > 0) {
                $model = $model->where("id", "IN", $ids)->find_all();
                $count = count($model);
                foreach ($model as $el) {
                    Event::instance()->dispatch("admin.delete.before", $el);
                    $el = $this->_delete_init($el);
                    $el->delete();
                }
            }
            $list = $this->last_uri;
            $this->page->message("Удалено элементов: " . $count);
//            $this->redirect($list);
        }
    }

    public function action_edit()
    {
        $this->page->content(
            View::factory("admin/edit")
                ->bind('model', $model)
                ->bind('fields', $fields)

        );

        $model = $this->factory_model();
        Event::instance()->dispatch("admin.edit.init", $model);
        $this->_edit_init($model);

        if ($this->request->param('id') != null) {
            $model->where("id", "=", (int)$this->request->param('id'))->find();
        }
        $fields = $model->fields_description();

        /** @var Cms_ORM $model */
        $model = $this->_after_edit_init($model);
        if ($model instanceof Cms_Interface_Tree) {
            $id2 = $this->request->param('id2');
            if ($id2) {
                $parent = $this->factory_model()->where('id', '=', $id2)->find();
                if (!$parent->loaded()) {
                    $this->response->status(404);
                    throw new HTTP_Exception_404;
                }
                $model->{$model->tree_parent_field()} = $parent->id;
                $this->crumbs[] = array('title' => $parent->{$parent->tree_title_field()}, 'uri' => $parent->tree_url(true));
            }
        }
        if ($model->loaded()) {
            $this->crumbs[] = array('title' => 'Редактировать', 'uri' => '#');
        } else {
            $this->crumbs[] = array('title' => 'Создать', 'uri' => '#');
            if (Authority::cannot('create', $this->resource())) {
                $this->redirect('admin/panel/403');
            }
        }
        Event::instance()->dispatch("admin.edit.init.after", $model);
        if ($this->request->post('save')) {
            Event::instance()->dispatch("admin.edit.save.before", $model);
            $this->_before_save($model);
            $this->values = $this->request->post('model');
            if ($this->_save($model, $this->values, $this->extrl_validation($model))) {
                Event::instance()->dispatch("admin.edit.save.after", $model);
                $this->_after_save($model);
                $this->page->message("Изменения сохранены");
                if ($model instanceof IC_Editable) {
                    $this->redirect($model->edit_url(true));
                } else {
                    $this->redirect($this->last_uri);
                }
            } else {
                Component::errors($this->errors);
                $this->page->message("Ошибка при сохранении", Cms_Page::PAGE_MESSAGE_ERROR);
            }
        }

        if ($this->request->post('cancel')) {
            Event::instance()->dispatch("admin.edit.cancel.before", $model);
            $this->_before_cancel($model);
            if ($model instanceof Cms_Interface_Tree) {
                $this->redirect($model->tree_url(true));
            } else {
                $this->redirect($model->list_url(true));
            }
        }
    }

    public function action_delete()
    {
        $model = $this->factory_model();
        if ($this->request->post('delete')) {
//            $ids = $this->request->post('model');
//            $count = 0;
//            if (count($ids) > 0) {
//                $model = $model->where("id", "IN", $ids)->find_all();
//                $count = count($model);
//                foreach ($model as $el) {
//                    $el = $this->_delete_init($el);
//                    $el->delete();
//                }
//            }
//            $list = $this->last_uri;
//            $this->page->message("Удалено элементов: ".$count);
//            $this->redirect($list);
        } else {
            $model->where("id", "=", $this->request->param('id'))->find();
            $list = $model->list_url(true);
            if ($model instanceof Cms_Interface_Tree){
                $list = $model->tree_parent_url();
            }
            if ($model instanceof Orm_Embeded) {
                $list = $model->parent_url(true);
            }
            if ($this->request->post('cancel')) {
                $this->redirect($list);
            }
            if ($model->loaded() && $this->request->post('save')) {
                Event::instance()->dispatch("admin.delete.before", $model);
                $model = $this->_delete_init($model);
                $model->delete();
                $this->page->message("Элемент удален");
                $this->redirect($list);
            } else {
                $this->page->content(View::factory('admin/delete'));
            }
        }


    }

    /**
     * Вызывается перед загрузкой коллекции, и перед применеия фильтра пагинации
     * @param $collection
     * @deprecated следует использовать событие
     */
    protected function _list_init($collection)
    {
        return $collection;
    }

    /**
     * Вызывается после попытки загрузить модель
     * @param $model
     * @deprecated следует использовать событие
     */
    protected function _edit_init($model)
    {
        return $model;
    }

    /**
     * Вызывается после попытки предзагрузки модели
     * @param $model
     * @deprecated следует использовать событие
     */
    protected function _after_edit_init($model)
    {
        return $model;
    }

    /**
     * Вызывается перед сохранением модели
     * @param $model
     * @deprecated следует использовать событие
     */
    protected function _before_save($model)
    {
        return $model;
    }

    /**
     * Вызывается после сохранения модели
     * @param $model
     * @param null $values
     * @return void
     * @deprecated следует использовать событие
     */
    protected function _after_save($model, $values = null)
    {
        if ($values) {
            $model->values($values)->save();
        }

    }

    /**
     * Вызывается перед удалением модели
     * @param $model
     * @deprecated следует использовать событие
     */
    protected function _delete_init($model)
    {
        return $model;
    }

    /**
     * @param $model
     * @return mixed
     * @deprecated следует использовать событие
     */
    protected function _before_cancel($model)
    {
        return $model;
    }

    /**
     * @param ORM $model
     * @param $fields
     */
    public function _search($model, $fields)
    {
        if ($model instanceof Cms_Interface_Partial) {
            $partial = $model->partial_model();

            if (!$this->request->query('lang')) {
                $model
                    ->join($partial->table_name(), 'LEFT')
                    ->on(
                        DB::expr($partial->table_name() . '.object_id' .'='. $model->object_name() . '.id'),
                        DB::expr('AND'),
                        DB::expr($partial->table_name() .'.lang' .'='. '\'ru\'')
                    );
            } else {
                $model
                    ->join($partial->table_name(), 'LEFT')
                    ->on(
                        DB::expr($partial->table_name() . '.object_id' .'='. $model->object_name() . '.id'),
                        DB::expr('AND'),
                        DB::expr($partial->table_name() .'.lang' .'='. '\''.$this->request->query('lang').'\'') //хотя разницы не будет.
                    );
            }
        }
        $max_having_count = 1;
        foreach ($fields as $name => $f) {
            if (strpos($name, '.')) {
                    $name_s = preg_replace('/[.]/', '_', $name);
                    $query = $this->request->query($name_s);
            } else {
                $query = $this->request->query($name);
            }
            if (isset($f['params']) && Arr::get($f['params'], 'widget') == 'multilang') {
                $name = $name . "_ru";
            }
            if ($query==='' || $query===NULL || $query===FALSE) continue;
            if (Arr::get($f, 'search')) {
                $column = $model->object_name() . '.' . $name;
                if (Arr::get($f,'partial')===true){
                    $column = $model->object_name() . '_partials.' . $name;
                } else {
                    $column = $model->object_name() . '.' . $name;
                }
                switch ($f['type']) {
                    case 'text':
                    case 'strings':
                        $model->where($column, 'LIKE', "%{$query}%");
                        break;

                    case 'date':
                        if (is_array($query)) {
                            if ($query[0]=='' && $query[1]==''){
                                break;
                            }
                            $start = new DateTime($query[0]);
                            $end = new DateTime($query[1]);
                            if ($query[0]!=='' && $query[1]==''){
                                $model->where($column, '>=', $start->format('Y-m-d'));
                                break;
                            }

                            if ($query[0]=='' && $query[1]!==''){
                                $model->where($column, '<=', $end->format('Y-m-d'));
                                break;
                            }

                            $model->where($column, '>=', $start->format('Y-m-d'));
                            $model->where($column, '<=', $end->format('Y-m-d'));
                        } else {
                            $date = strtotime($query);
                            $model->where($column, '>=', date('Y-m-d 00:00:00', $date));
                            $model->where($column, '<=', date('Y-m-d 23:59:59', $date));
                        }

                        break;
                    case 'datetime':
                        if (is_array($query)) {
                            $start = new DateTime($query[0]);
                            $end = new DateTime($query[1]);
                            $model->where($column, '>=', $start->format('Y-m-d H:i:s'));
                            $model->where($column, '<=', $end->format('Y-m-d H:i:s'));
                        } else {
                            $date = strtotime($query);
                            $model->where($column, '>=', date('Y-m-d 00:00:00', $date));
                            $model->where($column, '<=', date('Y-m-d 23:59:59', $date));
                        }

                        break;
                    default:
                        $op = '=';
                        if (is_array($query)) {

                            if (count($query) > 1) {
                                $data=array_keys($model->object());
                                //Если поле не является алиасом, то и групировать тут нечего
                                if (!in_array($name,$data)){
                                    $model->has_group_by()
                                        ?
                                        : $model->group_by($model->object_name() . '.' . $model->primary_key());
                                    if(!Arr::get($model->belongs_to(), substr($name,0, strpos($name, '.')),false)){
                                        $max_having_count *= count($query);
                                    }
                                }
                                $model->where_open();

                                foreach ($query as $val) {
                                    $model->or_where($column, $op, $val);
                                }
                                $model->where_close();

//                                $model->having(DB::expr('SUM(`'.$name.'`.`'.$model->$name->object_name().'_id'.'`)'),'=',$sum);
                            } else {
                                $model->where($column, $op, array_pop($query));
                            }


                        } else {
                            $model->where($column, $op, $query);
                        }
                }
            }
        }

        if ($model->has_group_by() && $max_having_count > 1) {
            $model->having(DB::expr('COUNT(*)'), '=', $max_having_count); //Для согласования мультиселектов
        }
    }

    /* Метод шаблон для установки внешней Валидации при сохранении */
    public function extrl_validation($model)
    {
        return null;
    }

    protected function init_authority()
    {
        $user = $this->user;

        $allow_admin = function ($user) {
            return $user->has('roles', ORM::factory('Role', array('name' => 'admin')));
        };

        //По умолчанию юзерам с ролью админ можно все
        //Но, т.к. действует принцип последнего правила, в методах потомках, мы можем запретить определенным ролям
        //доступ частично или полностью
        Authority::allow('access', 'all', $allow_admin);
        Authority::allow('view', 'all');
        //По умолчанию действия над моделями соответсвуют разрашениям для действия над экшенами
        //Т.е. если разрешено действия "access" к экшену "edit", значит и над моделью этого контроллера
        //разрешено это действие
        //Следовательно чтобы запретить удаление, нужно запретить доступ к экшену:
        // Authority::deny('access','delete');
        Authority::allow('create', 'all', function () use ($user) {
            return Authority::can('access', 'edit', $user);
        });
        Authority::allow('edit', 'all', function () use ($user) {
            return Authority::can('access', 'edit', $user);
        });
        Authority::allow('delete', 'all', function () use ($user) {
            return Authority::can('access', 'delete', $user);
        });

        Authority::allow('list', 'all'); //доступ ко всем меню
//        Authority::allow(,'all',$allow_admin);
    }

    protected function resource()
    {
        return strtolower(Arr::get($this->option, 'model', 'unknown'));
    }

    /**
     * Нет доступа
     */
    public function action_403()
    {
        $this->render = false;
        $view = View::factory('admin/403');
        $view->page = $this->page;
        $view->back_url = URL::site($this->last_uri);
        $this->response->body($view);
    }


}
