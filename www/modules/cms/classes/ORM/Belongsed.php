<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Saidashev Marat
 * Date: 28.12.13
 * Time: 14:40
 * To change this template use File | Settings | File Templates.
 */

class ORM_Belongsed extends ORM {

    protected $_parent_field="parent_id";
    protected $_parent_model="parent";

    public function edit_url($uri=false)
    {
        $id=$this->id?$this->id:0;
        $_uri =$this->route()."/edit/{$id}/{$this->{$this->parent_field()}}";
        return $uri?$_uri:URL::site($_uri);
    }

    public function parent_model(){
        return $this->_parent_model;
    }

    public function parent_field(){
        return $this->_parent_field;
    }

    public function list_url($uri=false){
        $_uri = $this->route()."/edit";
        if ($this->loaded()){
            $id=$this->{$this->parent_field()};
            if (!empty($id) || $id!=0){
                $_uri.="/{$id}";
            }
        }
        return $uri?$_uri:URL::site($_uri);
    }

    public function parent_url($uri=false)
    {
        $parent = ORM::factory($this->parent_model(),$this->{$this->parent_field()});

        if ($parent->loaded()==false){
            return $parent->list_url();
        }
        if(Request::current()->directory()=='Admin')
            return $parent->edit_url($uri);
        else
            return $parent->view_url($uri);
    }
}