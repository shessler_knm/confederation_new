<?php defined('SYSPATH') OR die('No direct script access.');

class Validation extends Kohana_Validation {

    public function check()
    {
        parent::check();
        if(isset($this->_bound[":model"])){
            $model = $this->_bound[":model"];
            if($model instanceof Cms_Interface_Partial){
                $fields_description = $model->fields_description();
                foreach($fields_description as $key=>$value){
                    if($rule=Arr::get($value,'must_partial')) {
                        $passed = TRUE;
                        if(is_array($rule)) {
                            $passed = $this->has_partials($model,$key,$rule);
                        } elseif($rule===TRUE) {
                            $passed = $this->has_partials($model,$key);
                        }
                        if ($passed === FALSE)
                        {
                            // Add the rule to the errors
                            $langs = is_array($rule)?array('langs'=>$rule):array('langs'=>I18n::$lang);
                            $this->error($key, 'has_partials',$langs);
                            $this->label($key,$value['label']);
                        }
                    }
                }
            }
        }

        return empty($this->_errors);

    }

    public function has_partials($model, $field, $langs = array()){
        if(!count($langs) && !count($model->partials())) {
            return false;
        }
        if(count($langs)){
            $partial_langs = array();
            foreach($model->partials() as $row){
                if(in_array($row->lang,$langs)){
                    $row->validation(Validation::factory($row->object())
                        ->bind(':model', $row)
                        ->bind(':original_values', $row->original_values())
                        ->bind(':changed', $row->changed())
                    );
                    $row->validation()->rule($field,'not_empty');
                    $fd = $model->fields_description();
                    $row->validation()->label($field,$fd[$field]['label']);
                    if(!$row->validation()->check()) {
                        return false;
                    }
                    $partial_langs[]=$row->lang;
                }
            }
        }
        return true;
    }
}
