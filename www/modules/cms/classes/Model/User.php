<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_User extends Model_Auth_User{
    protected $_created_column = array('column' => 'reg_date', 'format' => 'Y-m-d H:i:s');
    protected $_has_many = array(
        'roles' => array('model' => 'Role', 'through' => 'roles_users', 'foreign_key' => 'user_id'));
    
    /**
     * Проверка есть ли у пользователя роль "login"
     */
    public function is_active()
    {
        return $this->has('roles', $this->login_role());
    }

    public function get_full_name() {
        return $this->email;
    }
    
    /**
     *  Проверка есть ли у пользователя activation_hash
     */
    public function is_activated(){
        return (empty($this->activation_hash)) ? TRUE : FALSE;
    }
    
    /**
     * 
     * @return type object
     */
    public function login_role()
    {
        $model = ORM::factory('Role')->where('name', '=', 'login')->find();
        return $model;
    }
    
    
    
    
}
?>
