<?php
$q=DB::query(Database::INSERT,"CREATE  TABLE IF NOT EXISTS `news_categories` (
`news_id`  int(11) UNSIGNED NOT NULL ,
`category_id`  int(11) UNSIGNED NOT NULL ,
PRIMARY KEY (`news_id`, `category_id`),
CONSTRAINT `news_category_ibfk1` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
CONSTRAINT `news_category_ibfk2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT)ENGINE=InnoDB;");
$q->execute();