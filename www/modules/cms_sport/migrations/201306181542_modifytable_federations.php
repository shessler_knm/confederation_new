<?php
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM federations LIKE 'contacts_ru'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `federations`
ADD COLUMN `contacts_ru`  mediumtext NOT NULL;");
    $q->execute();
}
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM federations LIKE 'contacts_en'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `federations`
ADD COLUMN `contacts_en`  mediumtext NOT NULL;");
    $q->execute();
}
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM federations LIKE 'contacts_kz'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `federations`
ADD COLUMN `contacts_kz`  mediumtext NOT NULL;");
    $q->execute();
}
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM federations LIKE 'lat'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `federations`
ADD COLUMN `lat`  decimal(10,6) NOT NULL;");
    $q->execute();
}
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM federations LIKE 'lng'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `federations`
ADD COLUMN `lng`  decimal(10,6) NOT NULL;");
    $q->execute();
}