<?php
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM players LIKE 'federation_id'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `players` ADD COLUMN `federation_id`  int(11) UNSIGNED NOT NULL;");
    $q->execute();
    
    $q = DB::query(Database::INSERT,"SET FOREIGN_KEY_CHECKS=0;");
    $q->execute();
    
    $q = DB::query(Database::INSERT,"ALTER TABLE `players` ADD CONSTRAINT `ibfk` FOREIGN KEY (`federation_id`) REFERENCES `federations` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;");
    $q->execute();
}
