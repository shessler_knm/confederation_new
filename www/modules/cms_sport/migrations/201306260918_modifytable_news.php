<?php
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM news LIKE 'sport_id'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `news`
ADD COLUMN `sport_id`  int(11) UNSIGNED NOT NULL;");
    $q->execute();
}
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM news LIKE 'author'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `news`
ADD COLUMN `author`  varchar(200) NULL;");
    $q->execute();
}
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM news LIKE 'source'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `news`
ADD COLUMN `source`  varchar(200) NULL;");
    $q->execute();
}
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM news LIKE 'photograph'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `news`
ADD COLUMN `photograph`  varchar(200) NULL;");
    $q->execute();
}