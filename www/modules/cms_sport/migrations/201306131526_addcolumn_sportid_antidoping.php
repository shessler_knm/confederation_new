<?php
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM antidopings LIKE 'sport_id'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `antidopings`
ADD COLUMN `sport_id`  int(11) UNSIGNED NOT NULL AFTER `date`;
ALTER TABLE `antidopings` ADD CONSTRAINT `sport_id_antidoping_ibfk2` FOREIGN KEY (`sport_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;");
    $q->execute();
}