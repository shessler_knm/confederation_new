<?php
$q=DB::query(Database::INSERT,"
CREATE TABLE IF NOT EXISTS `federations_photos` (
  `federation_id` int(10) unsigned NOT NULL,
  `photo_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`federation_id`,`photo_id`),
  KEY `fk_federations_photos_1` (`photo_id`),
  KEY `fk_federations_photos_2` (`federation_id`),
  CONSTRAINT `fk_federations_photos_1` FOREIGN KEY (`photo_id`) REFERENCES `photos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_federations_photos_2` FOREIGN KEY (`federation_id`) REFERENCES `federations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
$q->execute();

$q=DB::query(Database::INSERT,"
CREATE TABLE IF NOT EXISTS `federations_videos` (
  `federation_id` int(10) unsigned NOT NULL,
  `video_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`video_id`,`federation_id`),
  KEY `fk_federations_videos_1` (`federation_id`),
  KEY `fk_federations_videos_2` (`federation_id`),
  CONSTRAINT `fk_federations_videos_1` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_federations_videos_2` FOREIGN KEY (`federation_id`) REFERENCES `federations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
$q->execute();
?>
