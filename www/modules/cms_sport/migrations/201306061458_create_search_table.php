<?php
$q=DB::query(Database::INSERT,"
 CREATE TABLE IF NOT EXISTS `search` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fulltext` mediumtext,
  `extended` mediumtext,
  `title_ru` text,
  `title_en` text,
  `title_kz` text,
  `text_ru` text,
  `text_kz` text,
  `text_en` text,
  `uri` varchar(300) DEFAULT NULL,
  `target` varchar(50) DEFAULT NULL,
  `target_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `fulltext` (`fulltext`),
  FULLTEXT KEY `extended` (`extended`)
) ENGINE=MyISAM AUTO_INCREMENT=3915 DEFAULT CHARSET=utf8;
");

$q->execute();

?>
