<?php
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM interviews LIKE 'federation_id'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `interviews`
    ADD COLUMN `federation_id`  int(11) UNSIGNED NOT NULL AFTER `title_kz`;
    ALTER TABLE `interviews` ADD CONSTRAINT `ibfk_federations` FOREIGN KEY (`federation_id`) REFERENCES `federations` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;");
    $q->execute();
}