<?php
$q=DB::query(Database::INSERT,"CREATE TABLE `albums` (
`id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT , PRIMARY KEY (`id`),
`title_ru`  varchar(200) NOT NULL ,
`title_en`  varchar(200) NULL ,
`title_kz`  varchar(200) NULL ,
`description_ru`  mediumtext NOT NULL ,
`description_en`  mediumtext NULL ,
`description_kz`  mediumtext NULL ,
`date`  datetime NOT NULL ,
`author`  varchar(200) NOT NULL,
`unique_id`  varchar(255) NOT NULL,
`federation_id`  int(11) UNSIGNED NOT NULL,
CONSTRAINT `federation_id_federation_ibfk1` FOREIGN KEY (`federation_id`) REFERENCES `federations` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT);");
    $q->execute();