<?php
$q=DB::query(Database::INSERT,"CREATE  TABLE IF NOT EXISTS `news_sports` (
`id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`news_id`  int(11) UNSIGNED NOT NULL ,
`category_id`  int(11) UNSIGNED NOT NULL ,
PRIMARY KEY (`id`),
CONSTRAINT `news_sports_ibfk_1` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
CONSTRAINT `news_sports_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT)ENGINE=InnoDB;");
$q->execute();