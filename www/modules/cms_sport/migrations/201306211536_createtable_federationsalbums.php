<?php
$q=DB::query(Database::INSERT,"CREATE  TABLE IF NOT EXISTS`dev_sport`.`federations_albums` (
  `federation_id` INT UNSIGNED NOT NULL ,
  `album_id` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`federation_id`, `album_id`) ,
  INDEX `fk_federations_albums_1` (`album_id` ASC) ,
  INDEX `fk_federations_albums_2` (`federation_id` ASC) ,
  CONSTRAINT `fk_federations_albums_1`
    FOREIGN KEY (`album_id` )
    REFERENCES `dev_sport`.`albums` (`id` )
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_federations_albums_2`
    FOREIGN KEY (`federation_id` )
    REFERENCES `dev_sport`.`federations` (`id` )
    ON DELETE CASCADE
    ON UPDATE RESTRICT)
ENGINE = InnoDB;");
$q->execute();