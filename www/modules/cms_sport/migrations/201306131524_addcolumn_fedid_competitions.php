<?php
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM competitions LIKE 'federation_id'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `competitions`
    ADD COLUMN `federation_id`  int(11) UNSIGNED NOT NULL AFTER `sport_id`;
    ALTER TABLE `competitions` ADD CONSTRAINT `competiton_federation_ibfk2` FOREIGN KEY (`federation_id`) REFERENCES `federations` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;");
    $q->execute();
}