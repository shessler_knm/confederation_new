<?php
$q=DB::query(Database::INSERT,"
 CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(700) DEFAULT NULL,
  `text` varchar(700) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `src` varchar(400) DEFAULT NULL COMMENT 'Ссылка на полное изображение',
  `minisrc` varchar(400) DEFAULT NULL COMMENT 'Ссылка на миниатюру',
  `unique_id` varchar(50) DEFAULT NULL,
  `fed_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id_UNIQUE` (`unique_id`),
  KEY `fk_photos_1` (`fed_id`),
  CONSTRAINT `fk_photos_1` FOREIGN KEY (`fed_id`) REFERENCES `federations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
");

$q->execute();

?>
