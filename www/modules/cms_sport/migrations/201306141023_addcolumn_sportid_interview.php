<?php
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM interviews LIKE 'sport_id'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `interviews`
ADD COLUMN `sport_id`  int(11) UNSIGNED NOT NULL;");
    $q->execute();
    $q=DB::query(Database::INSERT,"SET FOREIGN_KEY_CHECKS=0;");
    $q->execute();
    $q=DB::query(Database::INSERT,"ALTER TABLE `interviews` ADD CONSTRAINT `sport_id_interview_ibfk2` FOREIGN KEY (`sport_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;");
    $q->execute();
}