<?php
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM events LIKE 'address'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `events`
ADD COLUMN `address`  varchar(300) NOT NULL;");
    $q->execute();
}
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM events LIKE 'telephone'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `events`
ADD COLUMN `telephone`  varchar(200) NOT NULL;");
    $q->execute();
}
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM events LIKE 'email'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `events`
ADD COLUMN `email`  varchar(254) NOT NULL;");
    $q->execute();
}
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM events LIKE 'date_end'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `events`
ADD COLUMN `date_end`  datetime NOT NULL;");
    $q->execute();
}
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM events LIKE 'lat'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `events`
ADD COLUMN `lat`  decimal(10,6) NOT NULL;");
    $q->execute();
}
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM events LIKE 'lng'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `events`
ADD COLUMN `lng`  decimal(10,6) NOT NULL;");
    $q->execute();
}
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM events LIKE 'sef'");
$result = $q->execute();
if(!count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `events`
ADD COLUMN `sef`  varchar(200) NOT NULL;");
    $q->execute();
}
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM events LIKE 'date'");
$result = $q->execute();
if(count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `events`
CHANGE COLUMN `date` `date_begin`  datetime NOT NULL;");
    $q->execute();
}