<?php
$q=DB::query(Database::INSERT,"CREATE TABLE IF NOT EXISTS `photovideos` (
`id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT , PRIMARY KEY (`id`),
`title_ru`  varchar(255) NOT NULL ,
`title_kz`  varchar(255) NULL ,
`title_en`  varchar(255) NULL ,
`description_ru`  mediumtext NULL ,
`description_kz`  mediumtext NULL ,
`description_en`  mediumtext NULL ,
`date`  datetime NOT NULL ,
`type`  int(1) NOT NULL DEFAULT '2' ,
`content`  varchar(255) NOT NULL ,
'url' varchar(255) NULL,
`sport_id`  int(11) UNSIGNED NULL,
CONSTRAINT `ibfk_photovideos_spord_id` FOREIGN KEY (`sport_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
)ENGINE=InnoDB;");
$q->execute();