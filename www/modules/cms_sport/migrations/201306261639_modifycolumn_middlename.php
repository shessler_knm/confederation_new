<?php
$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM structures LIKE 'middlename_ru'");
$result = $q->execute();
if(count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `structures`
MODIFY COLUMN `middlename_ru`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;");
    $q->execute();
}

$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM referees LIKE 'middlename_ru'");
$result = $q->execute();
if(count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `referees`
MODIFY COLUMN `middlename_ru`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;");
    $q->execute();
}

$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM players LIKE 'middlename_ru'");
$result = $q->execute();
if(count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `players`
MODIFY COLUMN `middlename_ru`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;");
    $q->execute();
}

$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM governances LIKE 'middlename_ru'");
$result = $q->execute();
if(count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `governances`
MODIFY COLUMN `middlename_ru`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;");
    $q->execute();
}

$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM confedstructures LIKE 'middlename_ru'");
$result = $q->execute();
if(count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `confedstructures`
MODIFY COLUMN `middlename_ru`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;");
    $q->execute();
}

$q = DB::query(Database::SELECT,"SHOW COLUMNS FROM coaches LIKE 'middlename_ru'");
$result = $q->execute();
if(count($result)){
    $q=DB::query(Database::INSERT,"ALTER TABLE `coaches`
MODIFY COLUMN `middlename_ru`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;");
    $q->execute();
}