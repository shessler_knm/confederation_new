<?php
$q=DB::query(Database::INSERT,"CREATE TABLE IF NOT EXISTS `federations_photovideos` (
`federation_id`  int(11) NOT NULL ,
`photovideo_id`  int(11) NOT NULL ,
PRIMARY KEY (`federation_id`, `photovideo_id`),
CONSTRAINT `ibfk_federations_photovideos_federation_id` FOREIGN KEY (`federation_id`) REFERENCES `federations` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
CONSTRAINT `ibfk_federations_photovideos_photovideo_id` FOREIGN KEY (`photovideo_id`) REFERENCES `photovideos` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
)ENGINE=InnoDB;");
$q->execute();