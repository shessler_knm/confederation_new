<?php
/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 02.11.12
 * Time: 10:19
 * To change this template use File | Settings | File Templates.
 */
class Controller_Slider extends Site
{
    public function action_widget()
    {
        $this->render=false;
        $this->page->content(View::factory('basic')->bind('model',$model));
        $model=ORM::factory('slider')->where('status','=',1)->order_by('position')->lang->find_all();
        $this->response->body($this->page->content());
    }
}
