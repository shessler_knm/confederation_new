<?php
/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 01.11.12
 * Time: 17:22
 * To change this template use File | Settings | File Templates.
 */
class Model_Slider extends ORM
{
    protected $_route='slider';


    protected $_belongs_to=array(
        'image_s'=>array('model'=>'storage','foreign_key'=>'image')
    );

    public function rules(){
        return array(
            'title'=>array(
                array('not_empty'),
            ),
            'image'=>array(
                array('not_empty'),
            ),
            
        );
    }

    public function fields_description(){
        return array(
            'id'=>array(
                'head'=>true,
                'label'=>'#',
            ),
            'title'=>array(
                'edit'=>true,
                'type'=>'strings',
                'head'=>true,
                'search'=>true,
                'label'=>'Заголовок',
            ),
            'image'=>array(
                'edit'=>true,
                'type'=>'image',
                'label'=>'Изображение'
            ),

            'link'=>array(
                'edit'=>true,
                'type'=>'strings',
                'label'=>'Ссылка',
            ),
            'position'=>array(
                'edit'=>true,
                'head'=>true,
                'type'=>'strings',
                'label'=>'Позиция',
            ),
            'text'=>array(
                'edit'=>true,
                'type'=>'text',
                'label'=>'Описание',
            ),
            'status'=>array(
                'edit'=>true,
                'type'=>'select',
                'label'=>'status',
                'params'=>array(
                    'options'=>array(1=>'Опубликовано',0=>'Не опубликовано')
                ),
            ),
        );
    }

}
