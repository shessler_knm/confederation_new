<div class="wrap_arr widget_news">
    <a href="<?=URL::site("news")?>" class="link_news"><?=__('Новости')?>:</a>

    <div class="slider_news">
        <a href="#" class="i_arr"></a>

        <div class="slider_content">
            <?php

            foreach ($model->find_all() as $row):
                ?>
                <div class='hide_news'>
                    <div class="item"><i class="i_gradient"></i><a href="<?=$row->view_url()?>">
                        <span class="date"><?=$row->date?></span><span
                            class="event"><?=$row->title?></span></a>
                    </div>
                </div>
                <?php
            endforeach;
            ?>
        </div>
        <div class="slider_btn">
            <a href="#" class="prev" title="prev">&gt;</a>
            <a href="#" class="btn_autoplay pause" title="pause">pause</a>
            <a href="#" class="next" title="next">&lt;</a>
        </div>
    </div>

</div>
