<?php
class Controller_Admin_Category extends Admin_Embeded {
    public $option=array('model'=>'Category');


    public function action_synchronize_model() {

        $parent = ORM::factory('Category',$this->request->param('id'));
        if ($parent->loaded()) {$p_id = $parent->id;}
        else {$p_id = 'NULL';}
        $tasks = array('task' => 'synchronize',
            'env' => Helper::set_if_production(Kohana::PRODUCTION,Kohana::DEVELOPMENT));

        $federations = ORM::factory('Federation')->find_all();
        $model = $this->factory_model();

        foreach($federations as $fed) {
            Task_Synchronize::factory($tasks + array (
                'model' => $model->object_name(),
                'parent_model' => 'Category',
                'parent_hasmany' => 'children',
                'parent_field' => 'parent_id',
                'parent_id' => $p_id,
                'fed_id' => $fed->id
            ))->execute();
        }

        Minion_Task::factory($tasks)->execute();

        $this->redirect($this->last_uri);
    }

}
