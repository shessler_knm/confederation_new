<?php
/**
 * Description of category
 *
 * @author Doromor
 */
class Controller_Category extends Site {
    public $option=array('model'=>'Category');

    public function action_load_select(){
        $parent_model = ORM::factory($this->request->param('id2'),$this->request->param('id3'));
        $model = ORM::factory('Category')
            ->where('id','not in',($parent_model->{$this->request->param('id')}->find_all()->as_array())?$parent_model->{$this->request->param('id')}->find_all()->as_array():array('0','0'))
            ->select_options($this->request->param('id'));



        $this->render = false;
        $this->response->body(View::factory('category/ajax_load')
            ->bind('categories',$model));
    }

    public function action_get_tags_json(){
        $model = ORM::factory('Category')
            ->where('name', '=', 'tags')
            ->find()
            ->children
            ->find_all();
        $out = array('items'=>array());
        foreach($model as $row){
            $item = new stdClass();
            $item->name = $row->title;
            $item->value = $row->id;
            $out['items'][] = $item;
        }
        $this->render = false;
        $this->response->body(json_encode($out));
    }
}
