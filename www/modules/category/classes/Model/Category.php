<?php
/**
 * Модель поля. Поле принадлежит модели Model_Entity_Type. Модель поля содержит в себе Field_Value
 */
class Model_Category extends ORM_Embeded implements Synchronize_ISynch
{
    protected $_parent_field = "parent_id";
    protected $_parent_model = "Category";
    protected $_route = "category";
    protected $_has_many = array(
        'children' => array(
            'model' => 'Category',
            'foreign_key' => 'parent_id',
        ),
        'antidoping'=>array(
            'model'=>'Antidoping',
            'foreign_key'=>'category_id',
            'through'=>'antidopings_sports',
        ),
        'newses'=>array(
            'model'=>'News',
//            'foreign_key'=>'category_id',
            'through'=>'news_categories',
        ),
        'article'=>array(
            'model'=>'Article',
//            'foreign_key'=>'category_id',
            'through'=>'article_categories',
        ),
        'partners'  =>  array(
            'model' =>  'Partner',
            'foreign_key'   =>  'category_id'
        ),
       );
    protected $_belongs_to = array(
        'parent' => array(
            'model' => 'Category',
            'foreign_key' => 'parent_id'
        ),
        'photo'=>array(
            'model'=>'Storage',
            'foreign_key'=>'photo'
        ),
    );

//    public function title()
//    {
//        if (I18n::$lang != 'ru')
//            if ($this->parent->children->lang()->where('lang_root', '=', $this->id)->find()->id)
//                return $this->parent->children->lang()->where('lang_root', '=', $this->id)->find()->title;
//        return $this->title;
//    }

    public static function parent_null($model, $value)
    {
        if ($value == 0) {
            return NULL;
        }
        return $value;
    }

    public function filters()
    {
        return array(
            'parent_id' => array(
                array('Model_Category::parent_null', array(':model', ':value')),
            ),
            'slug' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title')),
            )
        );
    }

    public function as_array_ext($fields=array(), $action = null){
        $langs = array('ru','en','kz');
        $data = array();
        $data['id'] = $this->id;
        if ($this->{'title_'.I18n::$lang}){
            $data['name']=$this->{'title_'.I18n::$lang};
        }else{
            foreach ($langs as $lang){
                if ($this->{'title_'.$lang}){
                    $data['name']=$this->{'title_'.$lang};
                    break;
                }
            }
        }
        return $data;
    }

    public function fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#'
            ),
            'title' => array(
                'head' => true,
                'edit' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params'=>array(
                    'widget'=>'multilang'
                )
            ),
            'slug' => array(
                'head' => false,
                'edit' => true,
                'type' => 'strings',
                'label' => 'ЧПУ'
            ),
            'description' => array(
                'head' => true,
                'edit' => true,
                'search' => true,
                'type' => 'text',
                'label' => 'Описание',
                'params' => array(
                    'widget'=>'multilang'
                ),
            ),
            'parent_id' => array(
                'edit' => true,
                'type' => 'select',
                'label' => 'Родительский раздел',
                'params' => array(
                    'options' => array(0 => 'Нет') + ORM::factory('Category')->where('id', '!=', $this->id)->find_all()->as_array('id', 'title'),
                )
            ),
            'children' => array(
                'edit' => true,
                'type' => 'embeded',
                'label' => 'Дочерний раздел',
                'params' => array(
                    'target' => $this->children,
                    'foreign_key' => $this->_has_many['children']['foreign_key']
                )
            ),
            'synch_site' => array(
                'edit' => false,
                'head' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Данные с сайта',
            ),
        );
    }

    public function actions($user) {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
        );
        return $menu;
    }

    function get_sef_dishes(){
        $category = ORM::factory('Category')->where('sef','=','dishes')->find();
        $category = $category->children;
        return $category;
    }
    function get_sef_countries(){
        $category = ORM::factory('Category')->where('sef','=','countries')->find();
        $category = $category->children->order_by('title_'.I18n::lang());
        return $category;
    }
    function get_sef_muscles(){
        $category = ORM::factory('Category')->where('sef','=','muscle_group')->find();
        $category = $category->children->order_by('title_'.I18n::lang());
        return $category;
    }
    function get_sef_articles(){
        $category = ORM::factory('Category')->where('sef','=','article')->find();
        $category = $category->children;
        return $category;
    }
    function get_sef_sports(){
        $category = ORM::factory('Category')->where('sef','=','sports')->find();
        $category = $category->children->order_by('title_'.I18n::lang());
        return $category;
    }
    function get_sef_news(){
        $category = ORM::factory('Category')->where('sef','=','news')->find();
        $category = $category->children->order_by('title_'.I18n::lang());
        return $category;
    }

    function get_muscle_external(){
        $category = ORM::factory('Category')->where('sef','=','muscle_group')->find();
        return $category->external_list();
    }

    function get_news_external(){
        $category = ORM::factory('Category')->where('sef','=','news')->find();
        return $category->external_list();
    }

    function get_sef_article(){
        $article = ORM::factory('Category')->where('sef','=','article')->find();
//        $article = $article->children->order_by('title_'.I18n::lang());
        return $article->external_list();
    }

    public function form_list_button(Component $cp) {
        if ($this->parent->loaded()) {
            if ($this->parent->synch_id) {
                $cp->button_link(URL::site('admin/category/synchronize_model/'.$this->parent->synch_id), "Синхронизировать с Федерациями", array('class' => 'btn btn-primary'));
            }
        } else {
            $cp->button_link(URL::site('admin/category/synchronize_model/NULL'), "Синхронизировать с Федерациями", array('class' => 'btn btn-primary'));
        }
    }
}
