<?php defined('SYSPATH') or die('No direct script access.');

Route::set('admin/Category','admin/category((/<action>(/<id>(/<id2>(/<id3>)))))',array('action'=>'\w+', 'id'=>'\d+','id2'=>'\d+','id3'=>'\d+',))
    ->defaults(array(
    'directory'=>'admin',
    'controller'=>'Category',
    'action'=>'list',
));