<?php defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------

// Load the core Kohana class
require SYSPATH . 'classes/Kohana/Core' . EXT;

if (is_file(APPPATH . 'classes/Kohana' . EXT)) {
    // Application extends the core
    require APPPATH . 'classes/Kohana' . EXT;
} else {
    // Load empty core extension
    require SYSPATH . 'classes/Kohana' . EXT;
}

/**
 * Set the default time zone.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/timezones
 */
date_default_timezone_set('Asia/Almaty');

/**
 * Set the default locale.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/function.setlocale
 */
setlocale(LC_ALL, 'en_US.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @link http://kohanaframework.org/guide/using.autoloading
 * @link http://www.php.net/manual/function.spl-autoload-register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Optionally, you can enable a compatibility auto-loader for use with
 * older modules that have not been updated for PSR-0.
 *
 * It is recommended to not enable this unless absolutely necessary.
 */
//spl_autoload_register(array('Kohana', 'auto_load_lowercase'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @link http://www.php.net/manual/function.spl-autoload-call
 * @link http://www.php.net/manual/var.configuration#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('kz');

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
if (isset($_SERVER['KOHANA_ENV'])) {
    Kohana::$environment = constant('Kohana::' . strtoupper($_SERVER['KOHANA_ENV']));
}

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - integer  cache_life  lifetime, in seconds, of items cached              60
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 * - boolean  expose      set the X-Powered-By header                        FALSE
 */
Helper::detect_environment();



Kohana::init(array(
    'errors' => false,
    'profile' => Helper::set_if_production(false, true),
    'index_file' => FALSE,
    'profiling'=>FALSE,
    //'base_url'  =>  '/confederation/www/'
    'base_url'  =>  '/'
));

Cookie::$salt = "aosdfkjas;oifjwaoe98304ulkajfs";

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH . 'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
    'cms' => MODPATH . 'cms',
//    'auth'       => MODPATH.'auth',       // Basic authentication
    // 'cache'      => MODPATH.'cache',      // Caching with multiple backends
    // 'codebench'  => MODPATH.'codebench',  // Benchmarking tool
//    'database'   => MODPATH.'database',   // Database access
//	 'image'      => MODPATH.'image',      // Image manipulation
    // 'minion'     => MODPATH.'minion',     // CLI Tasks
//    'orm'        => MODPATH.'orm',        // Object Relationship Mapping
    // 'unittest'   => MODPATH.'unittest',   // Unit testing
    // 'userguide'  => MODPATH.'userguide',  // User guide and API documentation
//    'page'        => MODPATH.'page',        //
//    'email'       => MODPATH.'email',
));
//Kohana::$errors=false;

//Kohana::$index_file=Helper::set_if_production('','');
//Kohana::$base_url='/confederation/www/';
Kohana::$base_url='/';
/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */
Route::set('zheke-bapker','(<lang>/)zheke-bapker')
    ->defaults(array(
        'lang' => 'kz',
        'controller'=>'Zhekebapker',
        'action'=>'index',
    ));
Route::set('rest_api','(<lang>/)rest_api/<controller>(/<action>(/<id>(/<id2>)))')
    ->filter(function($route, $params, $request){
        $params['action'] = strtolower($request->method()).'_'.($request->query('action')?$request->query('action'):$params['action']);
        return $params;
    })
    ->defaults(array(
        'directory'=>'REST',
        'action'=>'index',
    ));
Route::set('xmlobject_conf', '(<lang>/)confederation/<controller>/xmlgeoobject',
    array('lang' => '(ru|kz|en)',))
    ->defaults(array(
        'action' => 'xmlgeoobject'
    ));

Route::set('xmlobject_fed', '(<lang>/)<fed_id>/<controller>/xmlgeoobject',
    array('lang' => '(ru|kz|en)', 'fed_id' => '(box|judo|taekwondo|weightlifting|wrestling)'))
    ->defaults(array(
        'action' => 'xmlgeoobject'
    ));

Route::set('redirect_view_c', '(<lang>/)confederation(/<advance>)/<model>/<action>/<id>',
    array('lang' => '(ru|kz|en)', 'id' => '(\d+)', 'advance' => '(infocenter|page)', 'action' => '(view)'))
    ->defaults(array(
        'controller' => 'redirect',
    ));

Route::set('redirect_view_f', '(<lang>/)<fed_id>/<model>/<method>/<id>',
    array('lang' => '(ru|kz|en)', 'id' => '(\d+)', 'fed_id' => '(box|judo|taekwondo|weightlifting|wrestling)'))
    ->defaults(array(
        'controller' => 'redirect',
        'action' => 'view'
    ));

Route::set('sef_id', '(<lang>/)federation/<id>', array('lang' => '(ru|kz|en)'))
    ->defaults(array(
        'controller' => 'federation',
        'action' => 'sef',
    ));

Route::set('fed_id_translit', '(<lang>/)<fed_id>/<controller>/translit_message',
    array('lang' => '(ru|kz|en)', 'fed_id' => '(box|judo|taekwondo|weightlifting|wrestling)'))
    ->defaults(array(
        'controller' => 'federation',
        'action' => 'translit_message',
    ));

Route::set('confederation_translit', '(<lang>/)confederation/<controller>/translit_message',
    array('lang' => '(ru|kz|en)',))
    ->defaults(array(
        'controller' => 'confederation',
        'action' => 'translit_message',
    ));

Route::set('search_conf', '(<lang>/)confederation/search/<action>',array('lang' => '(ru|kz|en)'))
    ->defaults(array(
        'controller' => 'search',
    ));

Route::set('search_fed', '(<lang>/)<fed_id>/search/<action>',
    array('fed_id' => '(box|judo|taekwondo|weightlifting|wrestling)','lang' => '(ru|kz|en)'))
    ->defaults(array(
        'controller' => 'search',
    ));

Route::set('api', '(<lang>/)api/<controller>(/<id>)')
    ->filter(function ($route, $params, $request) {
        $params['action'] = strtolower($request->method()) . '_' . ($request->query('method') ? $request->query('method') : $params['action']);
        return $params;
    })
    ->defaults(array(
        'directory' => 'API',
        'action' => 'index',
    ));

Route::set('synch', 'synch/<controller>(/<id>)')
    ->filter(function ($route, $params, $request) {
        $params['action'] = strtolower($request->method()) . '_' . ($request->query('method') ? $request->query('method') : $params['action']);
        return $params;
    })
    ->defaults(array(
        'directory' => 'Synchronize',
        'action' => 'index',
    ));

Route::set('user_route', 'user(/<action>(/<id>))')
    ->defaults(array(
        'controller' => 'user',
        'action' => 'login',

    ));

Route::set('comment_route', 'comment(/<action>(/<id>))')
    ->defaults(array(
        'controller' => 'comment',
        'action' => 'list',

    ));

Route::set('Storage_manage', 'Storage_Manage/download_file(/<id>)')
    ->defaults(array(
        'controller' => 'Storage_Manage',
        'action' => 'download_file',

    ));

Route::set('rio', '(<lang>/)rio2016(/<fed_id>)(/<controller>(/<action>(/<id>)))',
    array('lang' => '(ru|kz|en)', 'fed_id' => '(box|judo|taekwondo|weightlifting|wrestling)'))
    ->defaults(array(
        'controller' => 'Welcome',
        'action' => 'index',
        'directory' => 'Rio'
    ));


Route::set('privatecoach_admin', '(<lang>/)privatecoach/admin/<controller>/<action>(/<id>)',array('lang' => '(ru|kz|en)'))
    ->defaults(array(
        'directory' => 'Privatecoach/Admin',
    ));;

Route::set('privatecoach_page', '(<lang>/)privatecoach/page/<id>',array('lang' => '(ru|kz|en)'))
    ->defaults(array(
        'controller' => 'page',
        'action' => 'index',
        'directory' => 'Privatecoach'
    ));

Route::set('conf_page', '(<lang>/)confederation/page/<id>',array('lang' => '(ru|kz|en)'))
    ->defaults(array(
        'controller' => 'page',
        'action' => 'index',
    ));

Route::set('confederation_request', 'confederation/<controller>/request/<action>');

Route::set('gallery_view_pagin', '(<lang>/)confederation/infocenter/gallery/<type>(/page-<page>)',array('lang' => '(ru|kz|en)', 'type' => '(photo|video)'))
    ->defaults(array(
        'controller' => 'infocenter',
        'action'    =>  'gallery',
        'page' => 1,
    ));

Route::set('gallery_view', '(<lang>/)confederation/infocenter/gallery/<type>(/<sef>)',array('lang' => '(ru|kz|en)', 'type' => '(photo|video)'))
    ->defaults(array(
        'controller' => 'infocenter',
        'action'    =>  'gallery',
    ));

Route::set('gallery_preview', '(<lang>/)confederation/infocenter/gallery/<sef>',array('lang' => '(ru|kz|en)'))
    ->defaults(array(
        'controller' => 'infocenter',
        'action'    =>  'gallery',
    ));

Route::set('confederation_infocenter_view_pagin', '(<lang>/)confederation/infocenter/<action>(/page-<page>)',array('lang' => '(ru|kz|en)', 'action' => '(interview|pressservice|aboutus|infographic|fact)'))
    ->defaults(array(
        'controller' => 'infocenter',
        'page' => 1,
    ));

Route::set('confederation_infocenter_view', '(<lang>/)confederation/infocenter/<action>(/<sef>)',array('lang' => '(ru|kz|en)', 'action' => '(interview|pressservice|aboutus|infographic|fact)'))
    ->defaults(array(
        'controller' => 'infocenter',
    ));


Route::set('privatecoach_menu', '(<lang>/)privatecoach/menu/build/<id>',array('lang' => '(ru|kz|en)'))
    ->defaults(array(
        'controller' => 'menu',
        'action' => 'build',

    ));

Route::set('confederation_menu', '(<lang>/)confederation/menu/build/<id>',array('lang' => '(ru|kz|en)'))
    ->defaults(array(
        'controller' => 'menu',
        'action' => 'build',

    ));
Route::set('federation_menu', '(<lang>/)<fed_id>/menu/build/<id>',
    array('fed_id' => '(box|judo|taekwondo|weightlifting|wrestling)', 'lang' => '(ru|kz|en)'))
    ->defaults(array(
        'controller' => 'menu',
        'action' => 'build',

    ));

Route::set('section_ajax_list', '(<lang>/)section/ajax_list(/page-<page>)',array('lang' => '(ru|kz|en)'))
    ->defaults(array(
        'controller' => 'section',
        'action' => 'ajax_list',
        'page' => 1,
    ));

Route::set('section_ajax_list_filter', '(<lang>/)section/ajax_list(/<id>(/<id2>(/page-<page>)))',array('lang' => '(ru|kz|en)'))
    ->defaults(array(
        'controller' => 'section',
        'action' => 'ajax_list',
        'page' => 1,
    ));

Route::set('confederation_section_ajax_list', '(<lang>/)confederation/section/ajax_list(/page-<page>)',
    array('lang' => '(ru|kz|en)'))
    ->defaults(array(
        'controller' => 'section',
        'action' => 'ajax_list',
        'page' => 1,
    ));

Route::set('confederation_event', '(<lang>/)confederation/event(/<id>(/<id2>(/<city>(/<sport>))))',
    array('lang' => '(ru|kz|en)', 'id' => '(\d+)'))
    ->defaults(array(
        'controller' => 'event',
        'action' => 'index',

    ));

Route::set('confederation_index', '(<lang>/)confederation/<controller>(/page-<page>)',array('lang' => '(ru|kz|en)'))
    ->defaults(array(
        'action' => 'index',
        'page' => 1,
    ));

Route::set('confederation_view', '(<lang>/)confederation/<controller>/<sef>',array('lang' => '(ru|kz|en)'))
    ->defaults(array(
        'action' => 'view',
    ));

Route::set('profile', '(<lang>/)privatecoach/profile(/<id>)',
    array('lang' => '(ru|kz|en)','id'=>'(\d+)')
)
    ->defaults(array(
        'controller' => 'profile',
        'action' => 'index',
        'directory' => 'Privatecoach',
    ));

Route::set('profile_action', '(<lang>/)privatecoach/profile/<action>',
    array('lang' => '(ru|kz|en)')
)
    ->defaults(array(
        'controller' => 'profile',
        'directory' => 'Privatecoach',
    ));



Route::set('privatecoach', '(<lang>/)privatecoach(/<controller>(/<action>(/<id>(/<id2>))))',
    array('lang' => '(ru|kz|en)')
)
    ->defaults(array(
        'controller' => 'main',
        'action' => 'index',
        'directory' => 'Privatecoach',
    ));

Route::set('storage', 'storage(/<action>(/<id>))')
    ->defaults(array(
        'controller' => 'Storage',
        'action' => 'index',
    ));

Route::set('fed_page', '(<lang>/)<fed_id>/page/<id>',
    array('fed_id' => '(box|judo|taekwondo|weightlifting|wrestling)', 'lang' => '(ru|kz|en)'))
    ->defaults(array(
        'controller' => 'page',
        'action' => 'index',
    ));

Route::set('federation_request', '(<lang>/)<fed_id>/<controller>/request/<action>',
    array('fed_id' => '(box|judo|taekwondo|weightlifting|wrestling)'));

Route::set('request', '(<lang>/)<controller>/request/<action>');


Route::set('federation_section_ajax_list', '(<lang>/)<fed_id>/section/ajax_list(/page-<page>)',
    array('lang' => '(ru|kz|en)', 'fed_id' => '(box|judo|taekwondo|weightlifting|wrestling)'))
    ->defaults(array(
        'controller' => 'section',
        'action' => 'ajax_list',
        'page' => 1,
    ));

Route::set('federation_section_ajax_list_filter', '(<lang>/)<fed_id>/section/ajax_list(/<id>(/<id2>(/page-<page>)))',
    array('lang' => '(ru|kz|en)', 'fed_id' => '(box|judo|taekwondo|weightlifting|wrestling)'))
    ->defaults(array(
        'controller' => 'section',
        'action' => 'ajax_list',
        'page' => 1,
    ));


Route::set('confederation_section_ajax_list_filter', '(<lang>/)confederation/section/ajax_list(/<id>(/<id2>(/page-<page>)))',
    array('lang' => '(ru|kz|en)'))
    ->defaults(array(
        'controller' => 'section',
        'action' => 'ajax_list',
        'page' => 1,
    ));

Route::set('federation_list_sef_team_v2', '(<lang>/)<fed_id>/<controller>(/page-<page>)',
    array('lang' => '(ru|kz|en)', 'fed_id' => '(box|judo|taekwondo|weightlifting|wrestling)', 'controller' => '(team|coach)'))
    ->defaults(array(
        'action' => 'index',
        'page' => 1,
    ));

Route::set('federation_list_sef_team', '(<lang>/)<fed_id>/<controller>(/<sef>(/page-<page>))',
    array('lang' => '(ru|kz|en)', 'fed_id' => '(box|judo|taekwondo|weightlifting|wrestling)', 'controller' => '(team|coach|federation)'))
    ->defaults(array(
        'action' => 'index',
        'page' => 1,
    ));

Route::set('federation_list_sef_person', '(<lang>/)<fed_id>/<controller>/person/<sef>',
    array('lang' => '(ru|kz|en)', 'fed_id' => '(box|judo|taekwondo|weightlifting|wrestling)'))
    ->defaults(array(
        'action' => 'person',
    ));

Route::set('federation_event_sef', '(<lang>/)<fed_id>/event/<sef>',
    array('lang' => '(ru|kz|en)', 'fed_id' => '(box|judo|taekwondo|weightlifting|wrestling)'))
    ->defaults(array(
        'controller' => 'event',
        'action' => 'view',
    ));

Route::set('federation_event', '(<lang>/)<fed_id>/event(/<id>/<id2>/<city>/<sport>)',
    array('lang' => '(ru|kz|en)', 'fed_id' => '(box|judo|taekwondo|weightlifting|wrestling)', 'id' => '(\d+)'))
    ->defaults(array(
        'controller' => 'event',
        'action' => 'index',
    ));

Route::set('federation_list', '(<lang>/)<fed_id>(/<controller>(/page-<page>))',
    array('lang' => '(ru|kz|en)', 'fed_id' => '(box|judo|taekwondo|weightlifting|wrestling)'))
    ->defaults(array(
        'controller' => 'federation',
        'action' => 'index',
        'page' => 1,
    ));

Route::set('federation_view', '(<lang>/)<fed_id>(/<controller>(/<sef>))',
    array('lang' => '(ru|kz|en)', 'fed_id' => '(box|judo|taekwondo|weightlifting|wrestling)'))
    ->defaults(array(
        'controller' => 'federation',
        'action' => 'view',
    ));

//Route::set('for_exceptions','(<lang>(/))<fed_id>/<controller>(/<action>(/<id>))',
//    array('lang'=>'(ru|kz|en)'/*,'fed_id'=>'(box|judo|taekwondo|weightlifting|wrestling)'*/))
//    ->defaults(array(
////        'controller' => 'welcome',
//        'action' => 'index',
//    ));

Route::set('default', '(<lang>(/))(<controller>(/<action>(/<id>)))',
    array('lang' => '(ru|kz|en)')
)
    ->defaults(array(
        'controller' => 'welcome',
        'action' => 'index',
    ));