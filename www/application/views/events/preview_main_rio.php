<div class="main-col-right calendar-col">
    <h2 class="calendar-head"><?=__('Календарь');?></h2>

    <?foreach ($model as $row): ?>
    <div class="calendar-item">
        <div class="calendar-left">
        <? if(Date::formatted_time($row->date_begin,'d m')==Date::formatted_time($row->date_end,'d m')):?>
            <div class="c-date"><?=Date::textdate($row->date_begin, 'j')?></div>
            <div class="c-month"><?=Date::textdate($row->date_begin, 'm')?></div>
        <? else: ?>
            <?php
                $month_start = Date::textdate($row->date_begin, 'm');
                $month_end = Date::textdate($row->date_end, 'm');
                if ($month_start == $month_start):
            ?>
                    <div class="c-date"><?=date('d',strtotime($row->date_begin));?>-<?=date('d',strtotime($row->date_end));?></div>
                    <div class="c-month"><?=Date::textdate($row->date_end, 'm');?></div>
            <? else: ?>
                    <div class="c-date"><?=date('d',strtotime($row->date_begin)).' '.Date::textdate($row->date_end, 'm');?>-<?=date('d',strtotime($row->date_end)).' '.Date::textdate($row->date_end, 'm');?></div>
            <? endif; ?>
        <? endif; ?>
        </div>
        <div class="calendar-right">
            <? if (!($fed_id)): ?>
                <? foreach ($federations[$row->id] as $federation): ?>
                    <div class="c-item-head"><?=strip_tags($federation->title)?></div>
                <? endforeach ?>
            <? else: ?>
                <div class="c-item-head"><?= $row->city->title ?></div>
            <?endif?>
            <div class="c-item-caption">
                <a href="<?=URL::site(($fed_id) ? $fed_id . '/event/' . $row->{'sef_'.I18n::$lang} : 'rio2016/event/' . $row->{'sef_'.I18n::$lang})?>"
                   class="link"><?= $row->title ?></a>
            </div>
        </div>
    </div>
    <!-- item -->
    <? endforeach; ?>

    <?php /*<div class="calendar-item">
        <div class="calendar-left">
            <div class="c-date">24-30</div>
            <div class="c-month">марта</div>
        </div>
        <div class="calendar-right">
            <div class="c-item-head">Федерация тяжелой атлетики</div>
            <div class="c-item-caption">Чемпионат Казахстана 2015</div>
        </div>
    </div>
    <!-- item -->

    <div class="calendar-item">
        <div class="calendar-left">
            <div class="c-date">4-8</div>
            <div class="c-month">апреля</div>
        </div>
        <div class="calendar-right">
            <div class="c-item-head">Федерация бокса</div>
            <div class="c-item-caption">Международный турнир «Мемориал Влазния»</div>
        </div>
    </div>
    <!-- item -->

    <div class="calendar-item">
        <div class="calendar-left">
            <div class="c-date">18-26</div>
            <div class="c-month">апреля</div>
        </div>
        <div class="calendar-right">
            <div class="c-item-head">Федерация тяжелой атлетики</div>
            <div class="c-item-caption">ЧМ среди юниоров и девушек (U-20)</div>
        </div>
    </div>
    <!-- item -->

    <div class="calendar-item">
        <div class="calendar-left">
            <div class="c-date">22-27</div>
            <div class="c-month">апреля</div>
        </div>
        <div class="calendar-right">
            <div class="c-item-head">Федерация бокса</div>
            <div class="c-item-caption">Международный турнир памяти Феликса Штамма</div>
        </div>
    </div>
    <!-- item -->*/?>
</div>