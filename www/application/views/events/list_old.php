<input type="hidden" value="<?= $fed_id ?>" class="fed_id">
<input type="hidden" value="<?= $year ?>" class="year">
<div class="row_fluid">
    <div class="row_section">
        <div class="section calendar_section">
            <h1><?= __('Календарь') ?></h1>

            <form class="form-inline" method="post">
                <div class="pull-left">


                    <label><?= __('Время проведения') ?></label>

                    <div class="btn-group">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <?= Arr::get($month, $month_id, __('Весь год')) ?> (<?= $year ?>)
                        </a>

                        <ul class="dropdown-menu">
                            <? if (count($year_array) > 1): ?>
                                <li>
                                    <? if ($year > date('Y')): ?>
                                        <?=
                                        HTML::anchor(
                                            Route::get(
                                                Route::name(Request::current()->route())
                                            )->uri(
                                                    array(
                                                        'id' => $year - $differences[0],
                                                        'id2' => $month_id,
                                                        'city' => $city_id,
                                                        'sport' => $sport_id,
                                                        'fed_id' => $fed_id,
                                                        'action' => '',
                                                        'controller' => 'event',
                                                    )
                                                ), "&larr;", array('class' => 'btn_datepicker datepicker_prev', 'title' => 'Назад'))?>
                                    <? endif ?>
                                    <?=
                                    HTML::anchor(
                                        Route::get(
                                            Route::name(Request::current()->route())
                                        )->uri(
                                                array(
                                                    'id' => $year,
                                                    'id2' => $month_id,
                                                    'city' => $city_id,
                                                    'sport' => $sport_id,
                                                    'fed_id' => $fed_id,
                                                    'action' => '',
                                                    'controller' => 'event',
                                                )
                                            ), $year, array('class' => 'btn_datepicker datepicker_title'))?>
                                    <? if (Arr::get($differences, 1, false)): ?>
                                        <?=
                                        HTML::anchor(
                                            Route::get(
                                                Route::name(Request::current()->route())
                                            )->uri(
                                                    array(
                                                        'id' => $year + Arr::get($differences, 1),
                                                        'id2' => $month_id,
                                                        'city' => $city_id,
                                                        'sport' => $sport_id,
                                                        'fed_id' => $fed_id,
                                                        'action' => '',
                                                        'controller' => 'event',
                                                    )
                                                ), "&rarr;", array('class' => 'btn_datepicker datepicker_next', 'title' => 'Вперед'))?>
                                    <? endif ?>
                                </li>
                            <? endif ?>
                            <? foreach ($month as $m_id => $m): ?>
                                <li><a href="<?=
                                    URL::site(Route::get(
                                        Route::name(Request::current()->route())
                                    )->uri(
                                            array(
                                                'id' => $year,
                                                'id2' => $m_id,
                                                'city' => $city_id,
                                                'sport' => $sport_id,
                                                'fed_id' => $fed_id,
                                                'action' => '',
                                                'controller' => 'event',
                                            )
                                        ))?>"><?= $m ?></a></li>
                            <? endforeach; ?>
                        </ul>

                    </div>
                    <!--                --><? //=Form::select('months', $month, $month_id, array('id' => 'month', 'class' => 'custom_select'));?>
                </div>
                <div class="pull-left dropdown_mr">
                    <label><?= __('Место проведения') ?></label>
                    <?= Form::input('city', $city_array[$city_id], array('id' => 'city', 'class' => 'm10')) ?>
                </div>
                <div class="pull-left dropdown_ml">
                    <div class="btn-group">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <?= Arr::get($sport_array, $sport_id, __('Все виды спорта')) ?>
                        </a>
                        <ul class="dropdown-menu">
                            <? foreach ($sport_array as $s_id => $s): ?>
                                <li>
                                    <a href="<?=
                                    URL::site(Route::get(
                                        Route::name(Request::current()->route())
                                    )->uri(
                                            array(
                                                'id' => $year,
                                                'id2' => $month_id,
                                                'city' => $city_id,
                                                'sport' => $s_id,
                                                'fed_id' => $fed_id,
                                                'action' => '',
                                                'controller' => 'event',
                                            )
                                        )) ?>"><?= $s ?></a>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    </div>
                </div>
                <div class="clear"></div>

            </form>

            <? foreach ($avail_months as $m => $events): ?>
                <h4><?= $month[intval($m)] ?></h4>
                <table class="table table_calendar">
                    <tbody>
                    <? foreach ($events as $event): ?>
                        <tr>
                            <td class="td_date"><?= $event->text_date() ?></td>
                            <td class="td_title"><?=
                                HTML::anchor(Route::get($fed_id?'federation_event_sef':'confederation_view')->uri(
                                        array(
                                            'sef' => $event->{'sef_'.I18n::$lang},
                                            'fed_id' => $fed_id,
                                            'action' => 'view',
                                            'controller' => 'event',
                                        )
                                    ), '<strong class="ls">' . $event->title . '</strong>', array('class' => 'link'))?></td>
                            <td class="td_city"><?= $event->city->title ?></td>
                            <!--<Виды спорта>-->
                            <td class="td_sport"><? foreach ($event->categories->find_all() as $row): ?>
                                    <strong><?= $row->title ?></strong><br>
                                <? endforeach ?></td>
                            <!--</Виды спорта>-->
                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
            <? endforeach; ?>
            <? if ($count == 0): ?>
                <p><?= __('За выбранный период нет событий') ?></p>
            <? endif; ?>
            <?= $pagination ?>
        </div>
    </div>
    <div class="aside">
        <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div>
        <?= View::factory('social_networks') ?>
    </div>
</div>
<div style="display: none" id="lang" data-lang="<?=I18n::$lang?>"></div>