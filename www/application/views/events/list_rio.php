<section class="l-section-wrap">
    <div class="l-section main-inner-section clear-wrap">
        <div class="main-col-left">
            <ul class="breadcrumbs">
                <li><a href="<?=URL::site('rio2016')?>"><?=__('Главная');?></a></li>
                <li><?=$federation->title . __(' в Рио'); ?></li>
            </ul>
            <div class="typo-main">
                <h1><?=$federation->title . __(' в Рио'); ?></h1>

                <? if(count($avail_months) > 0): ?>
                <? foreach ($avail_months as $m => $events): ?>
                    <? foreach ($events as $event): ?>
                    <div class="calendar-page-item">
                        <div class="cp-col1">
                            <div class="cp-date"><?= $event->text_date() ?></div>
                            <!--<div class="cp-time">18:00</div>-->
                        </div>
                        <div class="cp-col2">
                            <? foreach ($event->categories->find_all() as $r): ?>
                                <?= $r->title ?><br>
                            <? endforeach ?></div>
                        <div class="cp-col3"><?= $event->city->title ?></div>
                    </div>
                    <? endforeach; ?>
                <? endforeach; ?>
                <? else: ?>
                    <?=__('Нет расписания');?>
                <? endif; ?>

            </div>
            <!-- /typo -->

            <div class="share-wrap clear-wrap">
                <script type="text/javascript" src="http://yastatic.net/share/share.js" charset="utf-8"></script><div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="button" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir"></div>
            </div>


        </div>
        <!-- /col -->
        <?=View::factory('rio/right_col');?>
        <!-- /col -->
    </div>
</section>