<ul class="unstyled event_c">
    <li class="event-title">
        <a class="link" href="<?= URL::site('confederation/event/') ?>" class="link">
            <?= __('Календарь') ?>
        </a>
    </li>
    <?foreach ($model as $row): ?>
    <li class="hr_top media">
        <div class="figure">
        <!-- Необходимо разбить дату число и название месяца по отдельности-->
        <?if(Date::formatted_time($row->date_begin,'d m')==Date::formatted_time($row->date_end,'d m')):?>
            <strong><?=Date::textdate($row->date_begin, 'j m')?></strong>
        <? else: ?>
            <strong><?=$row->text_date()?></strong>
        <?endif?>
        </div>
        <div class="media-body">
        <small><em>
            <? if (!($fed_id)): ?>
            <? foreach ($federations[$row->id] as $federation): ?>
                <a href="<?=URL::site($federation->sef . $fed_id)?>" class="link">
                    <span class="ls"><?=strip_tags($federation->title)?></span>
                </a>
            <? endforeach ?>
            <? else: ?>
            <?= $row->city->title ?>
            <?endif?>
        </em></small>
<!--        <div class="media_text">--><?//= $row->title; ?><!--</div>-->
        <div>
            <a href="<?=URL::site(($fed_id) ? $fed_id . '/event/' . $row->{'sef_'.I18n::$lang} : 'confederation/event/' . $row->{'sef_'.I18n::$lang})?>"
               class="link"><span class="ls"><?= $row->title ?></span></a>
        </div>
        </div>
    </li>
    <? endforeach?>
</ul>
