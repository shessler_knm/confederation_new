<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <div class="page_section">
                <?=Form::input('event_id',$model->id,array('id'=>'event_id','style'=>'visibility:hidden'))?>
                <h1><?=$model->title?></h1>
                <!--            --><?//=$model->photo_s->html_img(240,240)?>
                <div class="text-left"><?=$model->text?></div>
            </div>
            <?=__('Понравился материал? Поделитесь им с друзьями в соцсетях!')?>
            <div class="social_block" id="social_block"> </div>
        </div>

    </div>

    <div class="aside aside_sections">
        <ul class="unstyled aside_info">
            <li><?=Date::textdate($model->date_begin,'d m')?><?='-'?><?=Date::textdate($model->date_end,'d m')?></li>
            <li><?=$model->address?></li>
            <?if($model->telephone):?>
            <li><?=__('Телефоны')?> : <?=$model->telephone?></li>
            <?endif?>
            <? if ($model->email):?>
            <li><a href="mailto:<?=$model->email?>" class="link"><span class="ls"><?=$model->email?></span></a></li>
            <?endif?>
            <li><div class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?=__('Карта проезда')?></a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                <div class="map" id="map" style="height: 300px; width:300px"></div>
                    </ul>
                </div>
            </li>
        </ul>
        <div class="hr_top widget_social">
            <?=__('Мероприятия в соцсетях')?>
            <a href="https://www.facebook.com/ConfederatSport" title="facebook" class="icon_social i_fb">facebook</a>
            <a href="https://twitter.com/ConfederSport" title="twitter" class="icon_social i_tw">twitter</a>
            <a href="http://vk.com/confederationsport" title="vk" class="icon_social i_vk">vk</a>
            <a href="https://www.youtube.com/channel/UCrcVvkXdSk2HsJmNGSF812g" title="youtube" class="icon_social i_yt">youtube</a>
        </div>

        <div class="banner white_banner">
            <em><?=__('Спонсор сборной на чемпионате')?></em>
            <?= Request::factory('confederation/event/request/sponsor')->query('id',$model->id)->execute()?>
        </div>
        <div class="banner white_banner">
            <em><?=__('Участвующие организации')?></em>
            <?= Request::factory('confederation/event/request/organization')->query('id',$model->id)->execute()?>
        </div>
    </div>
</div>
<div id="point" data-lng="<?=$model->lng?>" data-lat="<?=$model->lat?>"></div>
