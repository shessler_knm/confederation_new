<ul class="unstyled event_c">
    <li class="event-title">
        <a class="link" href="<?= URL::site('confederation/event/') ?>" class="link">
            <?= __('Календарь') ?>
        </a>
    </li>
    <?foreach ($model as $row): ?>
    <li class="media">
        <div class="figure">
        <!-- Необходимо разбить дату число и название месяца по отдельности-->

        <?if(Date::formatted_time($row->date_begin,'d m')==Date::formatted_time($row->date_end,'d m')):?>
            <div><?=Date::textdate($row->date_begin, 'j m')?></div>
        <? else: ?>
            <div>
                <?php $month_start = Date::textdate($row->date_begin, 'm'); $month_end = Date::textdate($row->date_end, 'm'); ?>
                    <?php if ($month_start == $month_end): ?>
                        <div class="text-center">
                        <span class="date-count"><?=date('d',strtotime($row->date_begin));?>-<?=date('d',strtotime($row->date_end));?></span>
                        <span class="date-name"><?=Date::textdate($row->date_end, 'm');?></span>
                        </div>
                    <?php else: ?>
                        <div class="date-count text-center">
                            <?=date('d.m',strtotime($row->date_begin));?>
                        <div>-</div>
                            <?=date('d.m',strtotime($row->date_end));?>
                        </div>
                    <?php endif; ?>
                <?/*=$row->text_date()*/?>
            </div>
        <?endif?>
        </div>
        <div class="media-body">
        <em>
            <? if (!($fed_id)): ?>
            <? foreach ($federations[$row->id] as $federation): ?>
                <a <!--href="--><?/*=URL::site($federation->sef . $fed_id)*/?>" class="link">
                    <span class="lt"><?=strip_tags($federation->title)?></span>
                </a>
            <? endforeach ?>
            <? else: ?>
            <?= $row->city->title ?>
            <?endif?>
        </em>

        <div>
            <a href="<?=URL::site(($fed_id) ? $fed_id . '/event/' . $row->{'sef_'.I18n::$lang} : 'confederation/event/' . $row->{'sef_'.I18n::$lang})?>"
               class="link"><span class="ls"><?= $row->title ?></span></a>
        </div>
        </div>
    </li>
    <? endforeach?>
</ul>
