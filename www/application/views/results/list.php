<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <h1><?= __('Результаты') ?></h1>
            <? if ($check > 0): ?>
                <ul class="unstyled result_list">
                    <? foreach ($model as $row): ?>
                        <li class="hr_top media">
                            <div class="media_text">
                                <a href="<?= $row->alt_file() ?>" class="link"><span
                                        class="ls"><?= $row->title ?></span></a>
                            </div>
                            <em class="muted"><?= Date::textdate($row->date, 'd m') ?>, <?= $row->city->title ?></em>
                    <span class="widget">
                             <span
                                 class="icon_ext <?= $row->alt_icon() ?>"></span>
                             <em class="muted">
                                <?=$row->alt_size()?>
                             </em>
                    </span>
                        </li>
                    <? endforeach ?>
                </ul>
                <?= $pagination ?>
                <?else:?>
                <?=__('К сожалению, в данном разделе пока нет документов.')?>
            <? endif ?>
        </div>
    </div>
    <div class="aside">
        <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div>
        <?= View::factory('social_networks') ?>
    </div>
</div>