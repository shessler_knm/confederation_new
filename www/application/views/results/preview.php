<div class="h3"><a href="<?=URL::site($fed_id.'/result/')?>" class="link"><span class="ls"><?=__('Результаты')?></span></a></div>
<? if ($check > 0): ?>
<ul class="unstyled result_list">
    <? foreach ($model as $row): ?>
    <li class="hr_top media">
        <div class="media_text">
            <?if (I18n::lang()=='ru'):?>
                <a href="<?= $row->photoru_s->download_file_url()?>" class="link"><span class="ls"><?= $row->title ?></span></a>
            <?endif?>
            <?if (I18n::lang()=='en'):?>
                <a href="<?= $row->photoen_s->download_file_url()?>" class="link"><span class="ls"><?= $row->title ?></span></a>
            <?endif?>
            <?if (I18n::lang()=='kz'):?>
                <a href="<?= $row->photokz_s->download_file_url()?>" class="link"><span class="ls"><?= $row->title ?></span></a>
            <?endif?>
        </div>
        <small><em><?=Date::textdate($row->date, 'd m')?></em></small>
    </li>
    <? endforeach ?>
</ul>
<?else:?>
    <?=__('К сожалению, в данном разделе пока нет документов.')?>
<? endif ?>

