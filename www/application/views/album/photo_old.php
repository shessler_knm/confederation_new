<div class="row_fluid">
    <div class="row_section">
        <div class="section">
            <div class="media_section page_section">

                <h1><?=$model->title?></h1>
                <em class="muted"><?=Date::textdate($model->date, 'j m, H:i')?></em>
                <div class="media">
                    <div class="media_text"><?=$model->description?></div>
                </div>

                <div class="media_block">
                        <a href="<?=$photoSelected->largesrc?>" title=""
                           data-index="" class="open_fancybox">
                            <img class="big_preview_image" src="<?=$photoSelected->largesrc?>" alt="<?=$photoSelected->title?> <?=Date::textdate($photoSelected->date, 'd m, H:i')?>"/>
                        </a>
                </div>

                <ul class="unstyled inline media_list">
                    <?php
                    $i = 0;
                    ?>
                    <? foreach ($photos as $ph): ?>
                       <li>
                            <a class="select_item" href="<?=$ph->largesrc?>" data-index="<?=$i?>" alt="<div class='title_fancy'><?=$ph->title?></div> <em class='muted'><?=Date::textdate($ph->date, 'd m, H:i')?></em>">
                                <img src="<?=$ph->minisrc?>" alt="<div class='title_fancy'><?=$ph->title?></div> <em class='muted'><?=Date::textdate($ph->date, 'd m, H:i')?></em>"/>
                            </a>
                       </li>
                    <?php
                    $i++;
                    ?>
                    <? endforeach ?>
                </ul>

                <div class="text-right"><em class="muted"><?="Фото $model->author"?></em></div>

            </div>
            <?=__('Понравился материал? Поделитесь им с друзьями в соцсетях!')?>
            <div class="social_block" id="social_block">
                Yandex share?
            </div>
            <?=Comments::get_list($model->table_name(), $model->id)?>
        </div>
    </div>

    <div class="aside">
        <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute()?>
        </div>
        <?=View::factory('social_networks')?>

    </div>
</div>
