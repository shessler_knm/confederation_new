<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <?php
            echo View::factory('breadcrumbs')->set('_breadcrumbs', $_breadcrumbs)->render();
        ?>
        <div class="section">
            <div class="media_section page_section">        
                <h1><?=$model->title?></h1>
                <div class="muted"><?=Date::textdate($model->date, 'j m, H:i')?></div>

                <div class="media media_video">
                    <div class="video_inner">
                        <iframe width="100%" height="315" src="http://www.youtube.com/embed/<?=$model->{'content_'.I18n::$lang}?>" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
                <?=$model->description?>
            </div>

            <?=__('Понравился материал? Поделитесь им с друзьями в соцсетях!')?>
            <div class="social_block" id="social_block">
                Yandex share?
            </div>
            <?=Comments::get_list($model->table_name(),$model->id)?>
        </div>
    </div>
    <div class="aside">
        <!-- <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute()?>
        </div> -->
        <?=View::factory('social_networks')?>
    </div>
</div>
