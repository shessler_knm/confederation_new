<div class="media_section">

    <div class="h3"><a href="<?=URL::site('confederation/infocenter/gallery/')?>" class="link"><span class="ls"><?=__('Фото и Видео')?></span></a>
    </div>

    <ul class="unstyled event_list">
        <?php
        $i = 0;
        ?>
        <? foreach ($album as $ph): ?>
        <?php
        if ($i % 2 == 0) {
            echo "<li class='row-fluid'>";
        }
        ?>

        <div class="span6 item<?=$i % 2 + 1?> media">
            <div class="figure">
                <? foreach ($photo[$ph->id] as $row): ?>
                <a href="<?=URL::site('confederation/infocenter/gallery/' . $ph->{'sef_'.I18n::$lang})?>" class="link h5">
                    <img id="block" src="<?=$row->src?>">
                </a>
                <i class="icon_media icon_photo"></i>
                <?endforeach?>
            </div>

            <div class="media_text">
                <a href="<?=URL::site('confederation/infocenter/gallery/' . $ph->{'sef_'.I18n::$lang})?>" class="link h5"><span
                        class="ls"><?=$ph->title?></span></a>
            </div>
            <small><em>
                <?=Date::textdate($ph->date, 'j m')?>
            </em></small>
            <div class="span6 hr"></div>
        </div>
        <?php
        if ($i % 2 == 1) {
            echo "</li>";
        }
        $i++;
        ?>
        <? endforeach ?>
        <?php
        if ($i % 2 == 0) {
            echo "</li>";
        }
        ?>

        <? foreach ($video as $vi): ?>
        <?php
        if ($i % 2 == 0) {
            echo "<li class='row-fluid'>";
        }
        ?>

        <div class="span6 item<?=$i % 2 + 1?> media">
            <div class="figure">
                <a href="<?=URL::site('confederation/infocenter/gallery/' . $vi->{'sef_'.I18n::$lang})?>" class="link h5">
                    <img src="http://img.youtube.com/vi/<?=$vi->content?>/mqdefault.jpg">
                </a>
                <i class="icon_media icon_video"></i>
            </div>
            <div class="media_text">
                <a href="<?=URL::site('confederation/infocenter/gallery/' . $vi->{'sef_'.I18n::$lang})?>" class="link h5"><span
                        class="ls"><?=$vi->title?></span></a>
            </div>
            <small><em>
                <?=Date::textdate($vi->date, 'j m')?>
            </em></small>
            <div class="span6 hr"></div>
        </div>
        <?php
        if ($i % 2 == 1) {
            echo "</li>";
        }
        $i++;
        ?>
        <? endforeach ?>
    </ul>

</div>

