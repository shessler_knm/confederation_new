<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <?php
            echo View::factory('breadcrumbs')->set('_breadcrumbs', $_breadcrumbs)->render();
        ?>
        <div class="section media_section">


            <h1><?php
                    if ($type == 'photo') {
                        echo __('Фото');
                    } elseif ($type == 'video') {
                        echo __('Видео');
                    } else {
                        echo __('Фото и Видео');
                    }
                ?></h1>

            <ul class="unstyled event_list">
                <?php
                $i = 0;
                ?>
                <? foreach ($model as $album): ?>
                <?php
                if ($i % 2 == 0) {
                    echo "<li class='row-fluid'>";
                }
                ?>
                <div class="span6 item<?=$i % 2 + 1?> media">
                    <div class="figure">
                        <a href="<?=URL::site(($fed_id) ? $fed_id . '/gallery/' . $album->{'sef_'.I18n::$lang} : 'confederation/infocenter/gallery/' . $album->{'sef_'.I18n::$lang})?>">
                            <? if ($album->type == 1): ?>
                                    <img src="<?=$photos[$album->id]->src?>" alt="<?=$album->title?>">
                                    <i class="icon_media icon_photo"></i>
                            <? else: ?>
                                <img src="http://img.youtube.com/vi/<?=$album->content?>/mqdefault.jpg" alt="<?=$album->title?>">
                                <i class="icon_media icon_video"></i>
                            <?endif?>
                        </a>
                    </div>
                    <div class="media_text">
                        <a href="<?=URL::site(($fed_id) ? $fed_id . '/gallery/' . $album->{'sef_'.I18n::$lang} : 'confederation/infocenter/gallery/' . $album->{'sef_'.I18n::$lang})?>"
                           class="link h5"><span class="ls">
                            <?=$album->title?></span></a>
                    </div>
                    <?/*=$album->description*/?>
                    <small><em>
                        <?=Date::textdate($album->date, 'j m')?>
<!--                        --><?//=$album->sport->title?>
                            <span class="ico_eye"><?= $album->views ?></span>
                            <span class="ico_comment"><?= Comments::get_count('Photovideo', $album->id) ?></span>
<!--                        --><?//if (!$fed_id): ?>
<!--                        --><?// foreach ($federations[$album->id] as $federation): ?>
<!--                            <a href="--><?//=URL::site($federation->sef . $fed_id)?><!--" class="link">-->
<!--                                <span class="ls">--><?//=strip_tags($federation->title)?><!--</span>-->
<!--                            </a>-->
<!--                            --><?// endforeach ?>
<!--                        --><?// endif?>

                    </em></small>
                    <!-- <div class="span6 hr"></div> -->
                </div>
                <?php
                if ($i % 2 == 1) {
                    echo "</li>";
                }
                $i++;
                ?>
                <? endforeach ?>
                <?php
                if ($i % 2 == 0) {
                    echo "</li>";
                }
                ?>
            </ul>
           <?=$pagination?>
        </div>

    </div>
    <div class="aside">
        <!-- <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute()?>
        </div> -->
        <?=View::factory('social_networks')?>

    </div>
</div>