<div class="media_section clearfix">

        <div class="unit-media-photo">
        <div class="h3">
            <a href="<?=URL::site('confederation/infocenter/gallery/photo')?>" class="link"><?=__('Фото')?></a>
        </div>
        <?php
        $i = 0;
        ?>
        <? foreach ($album as $ph): ?>
<!--        <?php
//        if ($i % 2 == 0) {
//            echo "<div>";
//        }
//        ?>-->

        <div class="item<?=$i % 2 + 1?> media">
            <div class="figure">
                <? foreach ($photo[$ph->id] as $row): ?>
                <a href="<?=URL::site('confederation/infocenter/gallery/' . $ph->{'sef_'.I18n::$lang})?>" class="link h5">
                    <img id="block" src="<?=$row->src?>">
                </a>
                <!-- <i class="icon_media icon_photo"></i> -->
                <?endforeach?>
            </div>

            <div class="media_text">
            <a href="<?=URL::site('confederation/infocenter/gallery/' . $ph->{'sef_'.I18n::$lang})?>" class="link h5"><?=$ph->title?></a>
            </div>
            <small><em>
                <?=Date::textdate($ph->date, 'j m')?>
            </em></small>
        </div>
<!--        <?php
//        if ($i % 2 == 1) {
//            echo "</div>";
//        }
//        $i++;
//        ?>-->
        <? endforeach ?>
<!--        <?php
//        if ($i % 2 == 0) {
//            echo "</div>";
//        }
//        ?>-->
        </div>


        <div class="unit-media-video">
            <div class="h3">
                <a href="<?=URL::site('confederation/infocenter/gallery/video')?>" class="link"><?=__('Видео')?></a>
            </div>
        <? foreach ($video as $vi): ?>
<!--        <?php
//        if ($i % 2 == 0) {
//            echo "<div>";
//        }
//        ?>-->

        <div class="item<?=$i % 2 + 1?> media">
            <div class="figure">
                <a href="<?=URL::site('confederation/infocenter/gallery/' . $vi->{'sef_'.I18n::$lang})?>" class="link h5">
                    <img src="http://img.youtube.com/vi/<?=$vi->content?>/mqdefault.jpg">
                </a>
                <i class="icon_media icon_video"></i>
            </div>
            <div class="media_text">
                <a href="<?=URL::site('confederation/infocenter/gallery/' . $vi->{'sef_'.I18n::$lang})?>" class="link h5"><?=$vi->title?></a>
            </div>
            <small><em>
                <?=Date::textdate($vi->date, 'j m')?>
            </em></small>
            <div class="hr"></div>
        </div>
<!--        <?php
//        if ($i % 2 == 1) {
//            echo "</div>";
//        }
//        $i++;
//        ?>-->
        <? endforeach ?>
        </div>
        <div class="unit-media-social">
            <?= View::factory('social_networks') ?>
        </div>
</div>