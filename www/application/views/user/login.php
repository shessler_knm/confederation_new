<div class="row_fluid contacts_section clearfix bg-white">
<h3 class="reg_title text-center"><?=__('Вход')?></h3>
<form action="login" method="post" accept-charset="utf-8" class="form-horizontal form_ulogin form_usual">
    <div class="control-group">
        <label><?= __('Email') ?>:</label>
        <?= Form::input('username', '', array("class" => "")); ?>
    </div>
    <div class="control-group">
        <label><?= __('Пароль') ?>:</label>
        <?= Form::input('password', '', array("class" => "", "type" => "password")); ?>
        <p class="error"><?= arr::get($errors, 'password') ?></p>
    </div>
    <div class="control-group control_group_btn">
<!--        <label class="checkbox fr">
            <input type='checkbox' name='remember' id="remember"> <?/*= __('Запомнить?') */?>
        </label>-->
        <?= HTML::anchor(URL::site('user/passremind'), __('Забыли пароль?'), array("class" => "link pull-right link_forgotpass")) ?>
        <?= Form::submit('submit', __('Войти'), array('class' => 'btn btn_circle btn-primary pull-left')) ?>
        <? if (Request::$initial->uri() == 'confederation'): ?>
           <!-- <a href="<?/*= URL::site('user/reg') */?>"><?/*= __('или зарегистрироваться') */?></a>-->
        <? endif ?>
    </div>
    <div class="control-group">
        <label><?= __('Авторизация через соц.сети') ?>:</label>
        <?= $ulogin ?>
    </div>
    <div class="control-group">
        <a href="<?= URL::site('user/reg') ?>" class="link"><?= __('Регистрация') ?></a>
    </div>
</form>
</div>
