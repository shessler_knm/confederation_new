<div class="row_fluid contacts_section clearfix bg-white">
<h3 class="reg_title text-center"><?=__('Регистрация')?></h3>
<form action="reg" method="post" accept-charset="utf-8" class="form-horizontal form_usual">

    <div class="control-group">
        <label><?=__('Логин')?>:</label>
        <?=Form::input('username', arr::get($_POST, 'username'),array('class'=>''))?>
        <p class="error"> <?=__(arr::get($errors, 'username'))?></p>
    </div>

    <div class="control-group">
        <label><?=__('E-mail')?>:</label>
        <?=Form::input('email', arr::get($_POST, 'email'),array('class'=>''))?>
        <p class="error"><?=__(arr::get($errors, 'email'))?></p>
    </div>

    <div class="control-group">
        <label><?=__('Пароль')?>:</label>
        <?=Form::input('password', '',array('class'=>'','type'=>'password','id'=>'passfield'))?>
        <p class="error"><?=arr::path($errors, '_external.password')?></p>
    </div>

    <div class="control-group">
        <label><?=__('Еще раз пароль')?>:</label>
        <?=Form::input('password_confirm', '',array('class'=>'','type'=>'password'))?>
        <p class="error"><?=arr::path($errors, '_external.password_confirm')?></p>
    </div>

    <div class="control-group">
        <label><?=__('Код')?>:</label>
        <?=$recaptcha->widget()?>
        <p class="error"><?=$recaptcha->error($errors,TRUE)?></p>
    </div>


    <div class="control-group">
        <?=Form::submit('submit', __('Зарегистрироваться'),array('class'=>'btn btn_circle btn-primary '))?>
    </div>

    <div class="control-group">
        <label><?=__('Зарегистрироваться через')?>:</label>
        <?=$ulogin?>
    </div>
</form>
</div>
