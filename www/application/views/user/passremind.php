<div class="row_fluid clearfix bg-white">

<h3 class="reg_title text-center"><?=__('Восстановление пароля')?></h3>
<?=Form::open()?>

<div class="form-horizontal form_ulogin form_usual form_usual_passremind">
    <div class="control-group">
        <?=Form::label('email',__('E-mail *:'))?>
        <?=Form::input('email', arr::get($_POST, 'email'),array('size'=>'30'))?>
        <p class="error"> <?=arr::get($errors, 'email')?></p>
    </div>

    <div class="control-group">
        <?=Form::label('captcha',__('Код *:'))?>
        <?=$recaptcha->widget()?>
        <p class="error"><?=$recaptcha->error($errors,false)?></p>
    </div>

    <div class="control-group">
        <?=Form::submit('submit', __('Отправить'),array('class'=>'btn btn_circle btn-primary'))?>
    </div>

</div>



<!--        <table style="margin: 0 auto;">
            <tr>
                <td> <?/*=Form::label('email',__('E-mail *:'))*/?> </td>
                <td> <?/*=Form::input('email', arr::get($_POST, 'email'),array('size'=>'30'))*/?> </td>
                <td style="color: red"> <?/*=arr::get($errors, 'email')*/?> </td>
            </tr>
            <tr>
                <td> <?/*=Form::label('captcha',__('Код *:'))*/?></td>
                <td> <?/*=$recaptcha->widget()*/?></td>
                <td style="color: red"> <?/*=$recaptcha->error($errors,false)*/?> </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td> <?/*=Form::submit('submit', __('Отправить'),array('class'=>'i_btn'))*/?> </td>
                <td>&nbsp;</td>
            </tr>
        </table>-->
    <?=Form::close()?>
</div>