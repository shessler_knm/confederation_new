<?php
/*
	First Previous 1 2 3 ... 22 23 24 25 26 [27] 28 29 30 31 32 ... 48 49 50 Next Last
*/

// Number of page links in the begin and end of whole range
$count_out = (!empty($config['count_out'])) ? (int)$config['count_out'] : 3;
// Number of page links on each side of current page
$count_in = (!empty($config['count_in'])) ? (int)$config['count_in'] : 3;

// Beginning group of pages: $n1...$n2
$n1 = 1;
$n2 = min($count_out, $total_pages);

// Ending group of pages: $n7...$n8
$n7 = max(1, $total_pages - $count_out + 1);
$n8 = $total_pages;

// Middle group of pages: $n4...$n5
$n4 = max($n2 + 1, $current_page - $count_in);
$n5 = min($n7 - 1, $current_page + $count_in);
$use_middle = ($n5 >= $n4);

// Point $n3 between $n2 and $n4
$n3 = (int)(($n2 + $n4) / 2);
$use_n3 = ($use_middle && (($n4 - $n2) > 1));

// Point $n6 between $n5 and $n7
$n6 = (int)(($n5 + $n7) / 2);
$use_n6 = ($use_middle && (($n7 - $n5) > 1));

// Links to display as array(page => content)
$links = array();

// Generate links data in accordance with calculated numbers
for ($i = $n1; $i <= $n2; $i++) {
    $links[$i] = $i;
}
if ($use_n3) {
    $links[$n3] = '&hellip;';
}
for ($i = $n4; $i <= $n5; $i++) {
    $links[$i] = $i;
}
if ($use_n6) {
    $links[$n6] = '&hellip;';
}
for ($i = $n7; $i <= $n8; $i++) {
    $links[$i] = $i;
}

?>
<div class="my_pagination">
    <ul class="unstyled inline page_num">
        <!-- <li><?=__('Страницы') ?>:</li> -->
        <?php foreach ($links as $number => $content): ?>
        <?php if ($number === $current_page): ?>
            <li class="active"><strong><?php echo $content ?></strong></li>
            <?php else: ?>
                <?php if ($number === 1):?>
                    <?php
                    $link = HTML::chars($page->url($number));
                    $clear_link = str_replace('page-1','',$link);
                    ?>
                    <li><a href="<?php echo $clear_link ?>" class="link"><span class="ls"><?php echo $content ?></span></a></li>

                <?php else: ?>
                    <li><a href="<?php echo HTML::chars($page->url($number)) ?>" class="link"><span class="ls"><?php echo $content ?></span></a></li>
                <?php endif;?>
            <?php endif ?>
        <?php endforeach ?>
    </ul>

    <ul class="unstyled inline page_nav">
        <?php if ($previous_page !== FALSE): ?>
        <li class="first">
            <a href="<?php echo HTML::chars($page->url($previous_page)) ?>" rel="first" id="PrevLink">
                <span class="muted">&larr;Ctrl</span>
                <span class="link"><span class="ls"><?=__('Предыдущая') ?></span></span>
            </a>
        </li>
        <?php else: ?>
        <li class="first disabled">
            <span class="muted">&larr;Ctrl</span>
            <span class="link"><span class="ls"><?=__('Предыдущая') ?></span></span>
        </li>
        <?php endif ?>

        <?php if ($next_page !== FALSE): ?>
        <li class="last">
            <a href="<?php echo HTML::chars($page->url($next_page)) ?>" rel="last" id="NextLink">
                <span class="link"><span class="ls"><?=__('Следующая') ?></span></span>
                <span class="muted">Ctrl&rarr;</span>
            </a>
        </li>
        <?php else: ?>
        <li class="last disabled">
            <span class="link"><span class="ls"><?=__('Следующая') ?></span></span>
            <span class="muted">Ctrl&rarr;</span>
        </li>
        <?php endif ?>
    </ul>
</div><!-- .pagination -->