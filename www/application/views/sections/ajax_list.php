<? if ($count): ?>
    <? foreach ($model as $row): ?>
        <li class="media" data-lat="<?= $row->lat ?>" data-lng="<?= $row->lng ?>">
            <div class="pull-left">
                <a href="<?= $fed_id ? $row->view_url() : URL::site('confederation/section/' . $row->{'sef_' . I18n::$lang}) ?>"><?= $row->photo_s->html_img(100, 100, array('alt' => $row->title)) ?></a>
            </div>
            <div class="media-body">
                <div class="media-heading">
                    <div class="h4"><a
                            href="<?= $fed_id ? $row->view_url() : URL::site('confederation/section/' . $row->{'sef_' . I18n::$lang}) ?>"
                            class="link"><span
                                class="ls"><?= $row->title ?></span></a></div>
                </div>
                <div class="media_text">
                    <?= $row->city->title . ', ' ?>
                    <? if (I18n::lang() == 'ru'): ?>
                        <?= $row->address ?>
                    <? endif ?>
                    <? if (I18n::lang() == 'en'): ?>
                        <?= $row->address_en ?>
                    <? endif ?>
                    <? if (I18n::lang() == 'kz'): ?>
                        <?= $row->address_kz ?>
                    <? endif ?>
                    <br>
                    <span><?= $row->telephone ?></span>
                </div>
                <div class="event_text">
                    <?= $row->sports_description ?>
                </div>
                <!--        <a href="#" class="sectionPoint" data-lat="--><?//=$row->lat?><!--" data-lng="-->
                <?//=$row->lng?><!--">--><?//=__('Найти на карте')?><!--</a>-->
            </div>
        </li>
    <? endforeach ?>
    <?= $pagination ?>
<? else: ?>
    <div><?=__('По данному запросу поиск не дал результатов')?></div>
<?endif ?>
