<? foreach ($model as $row): ?>
<li class="hr_top media">
    <div class="pull-left"><?=$row->photo_s->html_img(100, 100,array('alt'=>$row->title))?></div>
    <div class="media-body">
        <div class="media-heading">
            <div class="h4"><a href="<?=URL::site('confederation/section/view/' . $row->id)?>" class="link"><span
                    class="ls"><?=$row->title?></span></a></div>
        </div>
        <div class="media_text">
            <?=$row->category->title?>
            <?=$row->address?>
            <br>
            <span><?=$row->telephone?></span>
        </div>
        <div class="event_text">
            <?=$row->sports_description?>
        </div>
    </div>
</li>
<? endforeach ?>
<?= $pagination ?>
