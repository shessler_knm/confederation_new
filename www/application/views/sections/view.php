<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <h1><?=$model->title?></h1>
            <div class="text-left"><?=$model->description?></div>

            <?=Comments::get_list($model->table_name(),$model->id)?>
        </div>

    </div>

    <div class="aside aside_sections">
        <ul class="unstyled aside_info">
            <li>
                <div class="figure_sections">
                    <?=$model->photo_s->html_img(200,null,array('alt'=>$model->title))?>
                </div>
            </li>
            <li>
                <?=$model->city->title.','?>
                <? if (I18n::lang() == 'ru'): ?>
                    <?=$model->address?>
                <? endif ?>
                <? if (I18n::lang() == 'en'): ?>
                    <?=$model->address_en?>
                <? endif ?>
                <? if (I18n::lang() == 'kz'): ?>
                    <?=$model->address_kz?>
                <? endif ?>
            </li>
            <li>
                <?=__('Телефоны')?>: <?=$model->telephone?>
            </li>
            <?if ($model->email):?>
            <li>
                <?=__('Эл. почта')?>: <a href="mailto:<?=$model->email?>" class="link"><span class="ls"><?=$model->email?></span></a>
            </li>
            <?endif?>
        </ul>


        <div class="hr_top widget_social">
            <?=__('Мероприятия в соцсетях')?>
            <a href="https://www.facebook.com/ConfederatSport" title="facebook" class="icon_social i_fb">facebook</a>
            <a href="https://twitter.com/ConfederSport" title="twitter" class="icon_social i_tw">twitter</a>
            <a href="http://vk.com/confederationsport" title="vk" class="icon_social i_vk">vk</a>
            <a href="https://www.youtube.com/channel/UCrcVvkXdSk2HsJmNGSF812g" title="youtube" class="icon_social i_yt">youtube</a>
        </div>
    </div>
</div>



