<input type="hidden" value="<?=$fed_id?$fed_id:0?>" class="fed_id">
<div class="row_fluid">
    <div class="row_section">
        <h1><?=__('Секции')?></h1>

        <div class="link_print">
            <div class="pull-left">
                <?=Form::select('city_select',$cityList,$selected,array('id'=>'city_select','class'=>'custom_select'))?>
            </div>
            <div class="pull-left">
                <div class="placeholder-group">
                    <label class="placeholder_label" for="section_object"><?=__('Название или адрес')?></label>
                    <?=Form::input('object_select','',array('id'=>'section_object','class'=>'m10 input_placeholder'))?>
                </div>
            </div>
            <div class="pull-left">
                <?=Form::select('sport_select',$sportList,$selected,array('id'=>'sport_select','class'=>'custom_select'))?>
            </div>
            <div class="pull-left">
                <?=Form::input('button', __('Найти'),array('type'=>'submit','id'=>'submit','class'=>'btn color'))?>
                <?=Form::select('place_select',$latList,$selected,array('id'=>'lat','style'=>'display:none'))?>
                <?=Form::select('place_select',$lngList,$selected,array('id'=>'lng','style'=>'display:none'))?>
            </div>

            <div class="pull-right text-right">
                <a href="#" onclick="print();return false"><i class="i_site i_print"></i><span class="link"><span class="ls"><?=__('Печать')?></span></span></a>
                <div class="link_dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="i_site i_link"></i> <span class="link"><span class="ls"><?=__('Ссылка')?></span></span></a>
                    <ul class="unstyled pull-right dropdown-menu" role="menu" aria-labelledby="dLabel">
                        <li>
                            <?=__('Понравился материал? Поделитесь им с друзьями в соцсетях!')?>
                            <div class="social_block" id="social_block">
                                Yandex share?
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="map" id="map" style="height: 480px"></div>

    </div>
</div>

<div class="row_fluid">
    <div class="row_section">
        <div class="section">
            <ul class="unstyled sections_list" data-url="<?=URL::site($fed_id?I18n::$lang.'/'.$fed_id.'/section/ajax_list/':I18n::$lang.'/confederation/section/ajax_list','http')?>">
                <!-- Список секции выводятся в ajax_list -->
            </ul>
        </div>
    </div>
    <div class="aside aside2">
        <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute()?>
        </div>
        <?=View::factory('social_networks')?>
    </div>

</div>
