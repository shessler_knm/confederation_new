<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <title><?=__('Конфедерация спортивных единоборств и силовых видов спорта')?></title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="<?=URL::site('images/favicon.png')?>" type="image/x-icon">

        <?=$page->get_head(true)?>
    </head>

    <body class="main_page pattern_bg <?=$full_size?'full_size':'' ?>">
    <? if (Helper::is_production()): ?>
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P75DZW"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push(
                {'gtm.start': new Date().getTime(),event:'gtm.js'}
            );var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-P75DZW');</script>
        <!-- End Google Tag Manager -->
    <? endif; ?>

    <div class="shadow_top">
        <div class="main_flag_bg">
            <div class="container">
                <div class="container-fluid">
                    <div class="white_link row_fluid">
                        <ul class="unstyled inline lang">
                            <li <?=I18n::lang() == 'kz' ? 'class="active"' : ''?>><a href="<?=Helper::currentURI()?>" <?=strstr($array_langs['kz'],Request::initial()->uri())?'rel=nofollow':null?> class="link"><span class="ls">Қаз</span></a></li>
                            <li <?=I18n::lang() == 'ru' ? 'class="active"' : ''?>><a href="<?=URL::site('ru/'.Helper::currentURI())?>" <?=strstr($array_langs['ru'],Request::initial()->uri())&&I18n::$lang!='kz'?'rel=nofollow':null?> class="link"><span class="ls">Рус</span></a></li>
                            <li <?=I18n::lang() == 'en' ? 'class="active"' : ''?>><a href="<?=URL::site('en/'.Helper::currentURI())?>" <?=strstr($array_langs['en'],Request::initial()->uri())&&I18n::$lang!='kz'?'rel=nofollow':null?> class="link"><span class="ls">Eng</span></a></li>
                        </ul>

                        <div class="widget_social">
                            <a href="<?=Arr::get($social_links,'facebook','#')?>" title="facebook" target="_blank" class="icon_social i_fb2">facebook</a>
                            <a href="<?=Arr::get($social_links,'twitter','#')?>" title="twitter" target="_blank" class="icon_social i_tw2">twitter</a>
                            <a href="<?=Arr::get($social_links,'vk','#')?>" title="vk" target="_blank" class="icon_social i_vk2">vk</a>
                        </div>

                        <?/*if (!Auth::instance()->logged_in()) { */?><!--

                        <ul class="unstyled inline auth">
                            <li><a href="<?/*=URL::site('user/login')*/?>" class="link"><span class="ls"><?/*=__('Вход')*/?></span></a></li>
                            <li>/</li>
                            <li><a href="<?/*=URL::site('user/reg')*/?>" class="link"><span class="ls"><?/*=__('Регистрация')*/?></span></a></li>
                        </ul>
                        --><? /*}*/?>

                        <div class="logo">
                            <a href="<?=URL::site('confederation')?>"><img src="<?=URL::site('images/big_logo.png')?>"></a>
                            <a href="<?=URL::site('confederation')?>" class="link"><span class="ls"><?=__('Конфедерация спортивных единоборств и силовых видов спорта')?></span></a>
                        </div>


                        <ul class="unstyled inline clear nav_column list_federation">
                            <?php
                            $item = 1;
                            ?>
                            <?foreach ($federations as $row2): ?>
                            <li class="column pull-left item_<?=$row2->sef?>" style="width: 160px;">
                                <a href="<? echo URL::site($row2->sef)?>" class="link">
                                            <span class="img">
                                                <?=$row2->photo_s->html_img(126,126,array('alt'=>$row2->title))?>
                                                <i class="icon_flag"></i>
                                            </span>
                                    <span class="ls"><?=$row2->title?></span>
                                </a>
                            </li>
                            <?php
                            $item++;
                            ?>
                                <? if($item==4){ ?>
                                    <li class="column pull-left item_qazaqkures" style="width: 160px;">
                                            <a href="#" class="link">
                                            <span class="img">
                                            <img src="<?=URL::site('images/logo_conf/logo6.png')?>" alt="<?=__('Федерация қазақ күресі')?>" width="126" height="126">
                                        <i class="icon_flag"></i>
                                        </span>
                                        <span class="ls"><?=__('Федерация қазақ күресі')?></span>
                                        </a>
                                    </li>
                                <? } ?>
                            <? endforeach?>
                        </ul>

                        <div class="slogan">
                            <strong><?=__('Конфедерация – слияние сил во имя побед Казахстана').'!'?></strong>
                        </div>

                        <div class="copyright">© <?=date('Y').' '.__('Конфедерация спортивных единоборств и силовых видов спорта')?></div>

                        <div class="logo_knm">
                            <a href="http://kaznetmedia.kz/">kaznetmedia</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="shadow_bottom"></div>

    <?=$page->get_scripts() ?>
    <? if (Helper::is_production()): ?>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter22160996 = new Ya.Metrika({id:22160996,
                        webvisor:true,
                        clickmap:true,
                        trackLinks:true});
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="//mc.yandex.ru/watch/22160996" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    <? endif; ?>
    </body>
</html>