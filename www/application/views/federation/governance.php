<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <h1><?= __('Руководство федерации') ?></h1>
            <ul class="unstyled coach_list">
                <? foreach ($model as $row): ?>
                    <li class="media">
                        <? if ($row->photo): ?>
                            <div class="img-circle pull-left">
                                <?= $row->photo_s->html_img(400, null, array('alt' => $row->lastname . ' ' . $row->firstname . ' ' . $row->middlename)) ?>
                                <div class="mask200"></div>
                            </div>
                        <? endif ?>
                        <? if (!$row->photo): ?>
                            <div class="img-circle pull-left">
                                <img src="<?= URL::site('images/zagl.jpg') ?>">

                                <div class="mask200"></div>
                            </div>
                        <? endif ?>
                        <div class="media-body">
                            <h4 class="media-heading">
                                <?= $row->lastname . ' ' . $row->firstname . ' ' . $row->middlename ?>
                            </h4>
                            <em class="muted"><?= $row->post ?></em>

                            <div class="media_text"></div>
                        </div>
                    </li>
                <? endforeach ?>
            </ul>
            <?= $pagination ?>
        </div>
    </div>
    <div class="aside">
        <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div>
        <?= View::factory('social_networks') ?>
    </div>
</div>