<div class="row_fluid contacts_section clearfix bg-white">

    <h1><?=__('Контакты') ?></h1>
    <div class="media">
        <? if ((strstr(Request::current()->url(), 'box'))): ?>
        <a href="<?=URL::site(($fed_id) ? $fed_id : 'confederation')?>" class="pull-left">
            <img src="<?=URL::site('images/logo_conf/contact_logo_box.png')?>" alt="" width="170px">
        </a>
        <? endif?>
        <? if ((strstr(Request::current()->url(), 'taekwondo'))): ?>
        <a href="<?=URL::site(($fed_id) ? $fed_id : 'confederation')?>" class="pull-left">
            <img src="<?=URL::site('images/logo_partner/LogoWTF.png')?>" alt="" width="170px">
        </a>
        <? endif?>
        <? if ((strstr(Request::current()->url(), 'weightlifting'))): ?>
        <a href="<?=URL::site(($fed_id) ? $fed_id : 'confederation')?>" class="pull-left">
            <img src="<?=URL::site('images/logo_conf/contact_logo_atlet.png')?>" alt="" width="170px">
        </a>
        <? endif?>
        <? if ((strstr(Request::current()->url(), 'wrestling'))): ?>
        <a href="<?=URL::site(($fed_id) ? $fed_id : 'confederation')?>" class="pull-left">
            <img src="<?=URL::site('images/logo_conf/contact_logo_borba.png')?>" alt="" width="170px">
        </a>
        <? endif?>
        <? if ((strstr(Request::current()->url(), 'judo'))): ?>
        <a href="<?=URL::site(($fed_id) ? $fed_id : 'confederation')?>" class="pull-left">
            <img src="<?=URL::site('images/logo_conf/contact_logo_djudo.png')?>" alt="" width="170px">
        </a>
        <? endif?>


        <div class="media-body">
            <div class="media_text">
                <?=$model->contacts?>
            </div>
        </div>
    </div>
    <div class="contacts_map">
        <div id="map" class="size_map">
            <? if ((strstr(Request::current()->url(), 'box'))): ?>
                <div id='coor' data-lng="51.142076" data-lat="71.411681" data-color="twirl#darkblueIcon"></div>
            <? endif?>
            <? if ((strstr(Request::current()->url(), 'taekwondo'))): ?>
                <div id='coor' data-lng="51.142076" data-lat="71.411681" data-color="twirl#violetIcon"></div>
            <? endif?>
            <? if ((strstr(Request::current()->url(), 'weightlifting'))): ?>
                <div id='coor' data-lng="43.236828" data-lat="76.956658" data-color="twirl#orangeIcon"></div>
            <? endif?>
            <? if ((strstr(Request::current()->url(), 'wrestling'))): ?>
                <div id='coor' data-lng="51.130485" data-lat="71.425737" data-color="twirl#greenIcon"></div>
            <? endif?>
            <? if ((strstr(Request::current()->url(), 'judo'))): ?>
                <div id='coor' data-lng="51.142076" data-lat="71.411681" data-color="twirl#redIcon"></div>
            <? endif?>
        </div>

        <div class="text-right link_print">
            <a href="#" onclick="print();return false"><i class="i_site i_print"></i><span class="link"><span class="ls"><?=__('Печать')?></span></span></a>
            <div class="link_dropdown">


                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="i_site i_link"></i> <span class="link"><span class="ls"><?=__('Ссылка')?></span></span></a>
                <ul class="unstyled pull-right dropdown-menu" role="menu" aria-labelledby="dLabel">
                    <li>
                        <?=__('Понравился материал? Поделитесь им с друзьями в соцсетях!')?>
                        <div class="social_block" id="social_block">
                            Yandex share?
                        </div>
                    </li>
                </ul>
            </div>


        </div>

    </div>

</div>
