<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <div class="section">

            <div class="news_section" style="margin-top: 20px;">
                <?php if ($fed_id == 'box'):?>
<!--                    <object width="640px" height="360px">-->
<!--                        <param name="movie" value="http://kivvi.kz/static/player2/player.swf?config=http://kivvi.kz/static/player2/live.txt&amp;page=http://kivvi.kz/broadcast/AlmatyregionFed/&amp;url=rtmp://89.218.86.33%2Flive%2FAlmatyregionFed%3fhash%3dAlmatyregionFed08459e7e1597f775957cf93f013ae08a9a896e62">-->
<!--                        <param name="allowFullScreen" value="true">-->
<!--                        <param name="bgcolor" value="#000000">-->
<!--                        <param name="allowScriptAccess" value="always">-->
<!--                        <param name="flashvars" value="title=%D0%9A%D1%83%D0%B1%D0%BE%D0%BA+%D0%9A%D0%A4%D0%91-2015+%D0%BF%D0%BE+%D0%B1%D0%BE%D0%BA%D1%81%D1%83+%D1%81%D1%80%D0%B5%D0%B4%D0%B8+%D0%BC%D1%83%D0%B6%D1%87%D0%B8%D0%BD&amp;play=auto">-->
<!--                        <embed src="http://kivvi.kz/static/player2/player.swf?config=http://kivvi.kz/static/player2/live.txt&amp;page=http://kivvi.kz/broadcast/AlmatyregionFed/&amp;url=rtmp://89.218.86.33%2Flive%2FAlmatyregionFed%3fhash%3dAlmatyregionFed08459e7e1597f775957cf93f013ae08a9a896e62" type="application/x-shockwave-flash" width="640px" height="360px" allowfullscreen="true" allowscriptaccess="always" flashvars="title=%D0%9A%D1%83%D0%B1%D0%BE%D0%BA+%D0%9A%D0%A4%D0%91-2015+%D0%BF%D0%BE+%D0%B1%D0%BE%D0%BA%D1%81%D1%83+%D1%81%D1%80%D0%B5%D0%B4%D0%B8+%D0%BC%D1%83%D0%B6%D1%87%D0%B8%D0%BD&amp;play=auto" bgcolor="#000000">-->
<!--                    </object>-->
<!--                    <video id="watch_player_AlmatyregionFed_html5_api" class="vjs-tech" preload="true" src="http://89.218.86.33/live/AlmatyregionFed.m3u8"></video>-->
<!--                    <object width="640px" height="360px">-->
<!--                        <param name="movie" value="http://kivvi.kz/static/player2/player.swf?config=http://kivvi.kz/static/player2/live.txt&amp;page=http://kivvi.kz/broadcast/serega_1979/&amp;url=rtmp://89.218.86.33%2Flive%2Fserega_1979%3fhash%3dserega_1979626feb034988f40bbc391b0088f1c2104089aa4d">-->
<!--                        <param name="allowFullScreen" value="true">-->
<!--                        <param name="bgcolor" value="#000000">-->
<!--                        <param name="allowScriptAccess" value="always">-->
<!--                        <param name="flashvars" value="title=%D0%9A%D1%83%D0%B1%D0%BE%D0%BA+%D0%9A%D0%A4%D0%91-2015+%D0%90%D1%81%D1%82%D0%B0%D0%BD%D0%B0-%D0%92%D0%9A%D0%9E&amp;play=auto">-->
<!--                        <embed src="http://kivvi.kz/static/player2/player.swf?config=http://kivvi.kz/static/player2/live.txt&amp;page=http://kivvi.kz/broadcast/serega_1979/&amp;url=rtmp://89.218.86.33%2Flive%2Fserega_1979%3fhash%3dserega_1979626feb034988f40bbc391b0088f1c2104089aa4d" type="application/x-shockwave-flash" width="640px" height="360px" allowfullscreen="true" allowscriptaccess="always" flashvars="title=%D0%9A%D1%83%D0%B1%D0%BE%D0%BA+%D0%9A%D0%A4%D0%91-2015+%D0%90%D1%81%D1%82%D0%B0%D0%BD%D0%B0-%D0%92%D0%9A%D0%9E&amp;play=auto" bgcolor="#000000">-->
<!--                    </object>-->
                <?php endif?>

                <div class="h3"><span class="link"><span class="ls"><?=__('Новости')?></span></span></div>
                <?=Request::factory($fed_id.'/news/request/federation_preview')->execute()?>
                <div class="next_news_list">
                    <a href="<?=URL::site($fed_id.'/news/')?>" class="link_next"><?=__('Еще новости')?></a>
                </div>
            </div>

            <div class="slider_section slider_section_inline">
                <?=Request::factory($fed_id.'/news/request/widget')->execute()?>
            </div>

            <div class="hr_bottom coach_section">
                <div class="h3"><a href="<?=URL::site($fed_id.'/coach')?>" class="link"><span class="ls"><?=__('Тренерский состав')?></span></a></div>
                <?=Request::factory($fed_id.'/coach/request/preview')->query('query',true)->execute()?>
            </div>
        </div>
    </div>
    <div class="aside">
        <div class="h3"><a href="<?=URL::site($fed_id.'/event/'.date('Y').'/all/all/all')?>" class="link"><span class="ls"><?=__('Календарь')?></span></a></div>
        <?=Request::factory($fed_id.'/event/request/preview')->query('query',true)->execute()?>
        <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute()?>
        </div>
        <?=Request::factory($fed_id.'/result/request/index')->query('query',true)->execute()?>
        <?=View::factory('social_networks')?>
    </div>
</div>



