<div class="main-col-left news-col">
    <div class="news-wrap">
        <? foreach ($model as $row): ?>
            <div class="news-item">
                <img src="<?=URL::site($row->photo_s->dir . '/'. $row->photo_s->name . '.' . $row->photo_s->type);?>" alt="<?= Text::limit_chars(strip_tags($row->title), 75) ?>" class="news-img">
                <div class="news-date"><?=Date::textdate($row->date, 'd m, H:s');?></div>
                <div class="news-head-wrap">
                    <a href="<?=URL::site('rio2016/news/view/'.$row->{'sef_'.I18n::$lang})?>" class="news-head"><?= Text::limit_chars(strip_tags($row->title), 75) ?></a>
                </div>
            </div>
            <!-- /item -->
        <? endforeach; ?>
    </div>
</div>