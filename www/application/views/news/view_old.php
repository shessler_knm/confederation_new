<div class="row_fluid">
    <div class="row_section">
        <div class="section">
            <div class="page_section">
                <h1><?=$model->title?></h1>
                <em class="muted"><?=Date::textdate($model->date,'d m')?></em>
                <div class="media">
                    <?if ($model->photo):?>
                        <div class="figure"><?=$model->photo_s->html_cropped_img(500,null,array('alt'=>$model->title)) ?></div>
                    <?endif?>
                    <div class="media_text">
                        <?=$model->text?>
                    </div>
                    <?=$model->author?>
                    <?=$model->photograph?__('Фото').' '.$model->photograph:null?>
                </div>

            </div>
            <div class="info_page_section">
<!--                <em class="muted">--><?//= Date::textdate($model->date, 'd m') ?><!--</em>-->
<!--                <em class="muted">--><?//= Helper::word_cases(Comments::get_count('Article', $model->id), __('комментариев'), __('комментарий'), __('комментария')) ?><!--</em>-->
                <em class="muted"><?= Helper::word_cases($model->views, __('просмотров'), __('просмотр'), __('просмотра')) ?></em>

            </div>
            <?=__('Понравился материал? Поделитесь им с друзьями в соцсетях!')?>
            <div class="social_block" id="social_block">
                Yandex share?
            </div>

            <?=Comments::get_list($model->table_name(),$model->id)?>
        </div>
    </div>

    <div class="aside">
        <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute()?>
        </div>
        <?=View::factory('social_networks')?>
    </div>
</div>