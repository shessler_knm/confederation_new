<ul class="unstyled event_list news_list_inline">
    <?php
    $i = 0;
    ?>
    <? foreach ($model as $row): ?>
        <?php
        if ($i % 2 == 0) {
            echo "<li class='row-fluid'>";
        }
        ?>

        <div class="span6 item<?= $i % 2 + 1 ?> media">
            <div class="figure">
                <a href="<?= $row->view_url() ?>">
                    <?php
                        try {
                            echo $row->photo_s->html_cropped_img(500,null,array('alt'=>$row->title));
                        } catch (Exception $e) {

                        }
                    ?>
                </a>
            </div>
            <div class="media-body">
                <small><em><span class="date"><?= Date::textdate($row->date, 'd m, H:s') ?></span></em></small>
                <div class="h5"><a href="<?= $row->view_url() ?>" class="link"><span
                            class="ls"><?= Text::limit_chars(strip_tags($row->title), 75) ?></span></a></div>

                <div class="media_text"><?= $row->get_announcement() ?></div>
                <small><em>
<!--                        --><?// if (!($fed_id)): ?>
<!--                            --><?// foreach ($federations[$row->id] as $federation): ?>
<!--                                <a href="--><?//= URL::site($federation->sef . $fed_id) ?><!--" class="link">-->
<!--                                    <span class="ls">--><?//= strip_tags($federation->title) ?><!--</span></a>--><?//=$counter<count($federations[$row->id])?',':null?>
<!--                                --><?//$counter++?>
<!--                            --><?// endforeach ?>
<!--                            --><?//$counter=1?>
<!--                        --><?// endif ?>
                        <!--<span class="date"><?/*= Date::textdate($row->date, 'd m') */?></span>-->
<!--                        <span class="ico_eye">--><?//= $row->views ?><!--</span>-->
                        <span class="ico_comment"><?= Comments::get_count('News', $row->id) ?></span>
                        <span><?=$row->author?__('Автор').': '.$row->author:null?></span>
                    </em></small>
            </div>
            <div class="span6 hr"></div>
        </div>
        <?php
        if ($i % 2 == 1) {
            echo "</li>";
        }
        $i++;
        ?>
    <? endforeach ?>
    <?php
    if ($i % 2 == 0) {
        echo "</li>";
    }
    ?>
</ul>