<h1><?= __('Новости') ?></h1>


<?= Request::factory($fed_id . '/news/request/widget')->execute() ?>

<div class="row_fluid">
    <div class="row_section">
        <div class="section">
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade in active" id="year">
                    <ul class="unstyled event_list news_list_inline">
                        <?php
                        $i = 0;
                        ?>
                        <? foreach ($news as $row): ?>
                            <?php
                            if ($i % 2 == 0) {
                                echo "<li class='row-fluid'>";
                            }
                            ?>

                            <div class="span6 item<?= $i % 2 + 1 ?> media">
                                <div class="figure">
                                    <a href="<?= URL::site('confederation/news/'.$row->sef) ?>"><?= $row->photo_s->html_cropped_img(500,null,array('alt'=>$row->title)) ?></a>
                                </div>
                                <small><em>
                                    <span class="date"><?= Date::textdate($row->date, 'd m') ?></span>
                                    </em>
                                </small>
                                 <div class="h5"><a href="<?= URL::site('confederation/news/'.$row->sef) ?>" class="link"><span
                                            class="ls"><?= Text::limit_chars(strip_tags($row->title), 165) ?></span></a>
                                </div>

                                <div class="media_text"><?= $row->get_announcement() ?></div>
                                <small><em>
                                    <!--<span class="date"><?/*= Date::textdate($row->date, 'd m') */?></span>-->
<!--                                    <span class="ico_eye">--><?//= $row->views ?><!--</span>-->
                                    <span class="ico_comment"><?= Comments::get_count('News', $row->id) ?></span>
                                        <span><?=$row->author?__('Автор').': '.$row->author:null?></span>
                                    </em></small>
                                <div class="span6 hr"></div>
                            </div>

                            <?php
                            if ($i % 2 == 1) {
                                echo "</li>";
                            }
                            $i++;
                            ?>
                        <? endforeach ?>
                        <?php
                        if ($i % 2 == 0) {
                            echo "</li>";
                        }
                        ?>

                    </ul>
                    <?= $pagination ?>
                </div>
            </div>
        </div>
    </div>
    <div class="aside">
        <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div>
        <h3><?= __('Главное за сутки') ?></h3>
        <?= Request::factory($fed_id . '/news/request/main_preview')->query('query', true)->execute() ?>

        <?= View::factory('social_networks') ?>

    </div>
</div>



