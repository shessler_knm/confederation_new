<section class="l-section-wrap">
    <div class="l-section main-inner-section clear-wrap">
        <div class="main-col-left">
            <ul class="breadcrumbs">
                <li><a href="<?=URL::site('rio2016');?>"><?=__('Главная');?></a></li>
                <li><?=$model->title?></li>
            </ul>
            <div class="typo-main">
                <h1><?=$model->title?></h1>
                <div class="content-img-wrap">
                    <?if ($model->photo):?>
                        <div class="figure"><img src="<?=URL::site($model->photo_s->dir . '/'. $model->photo_s->name . '.' . $model->photo_s->type)?>"> <?/*=$model->photo_s->html_cropped_img(500,null,array('alt'=>$model->title))*/ ?></div>
                    <?endif?>
                </div>
                <p><?=$model->text?></p>
            </div>
            <!-- /typo -->

            <div class="share-wrap clear-wrap">
                <script type="text/javascript" src="http://yastatic.net/share/share.js" charset="utf-8"></script><div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="button" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir"></div>
            </div>
            <!-- /share wrap -->
            <?=Comments::get_list_rio($model->table_name(),$model->id)?>
            <!-- /comments wrap -->
        </div>
        <!-- /col -->
            <?=View::factory('rio/right_col');?>
        <!-- /col -->
    </div>
</section>