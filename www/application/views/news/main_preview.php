<ul class="unstyled result_list">
    <? foreach ($model as $row): ?>
        <li class="hr_top media">
            <div class="media_text">
                <a href="<?=$row->view_url()?>" class="link"><span class="ls"><?= $row->title ?></span></a>
            </div>
            <small><em><?=Date::textdate($row->date, 'd m')?></em></small>
        </li>
    <? endforeach ?>
</ul>

