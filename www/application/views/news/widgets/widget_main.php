<div class="bx-news">
    <? foreach ($model as $row): ?>
        <div class="bx-slide">
            <a href="<?=URL::site(($fed_id)?$fed_id.'/news/'.$row->{'sef_'.I18n::$lang}:'confederation/news/'.$row->{'sef_'.I18n::$lang})?>">
                <img src="<?=$row->photo_slider_s->url_cropped_image(null,null,'http')?>" alt="<?=Text::limit_words(strip_tags($row->title), 8)?>">
        <span class="head_news-title">
            <span><?=Text::limit_words(strip_tags($row->title), 8)?></span>
        </span>
            </a>
        </div>
    <? endforeach?>
</div>
