<div class="pattern_bg news_slider">
    <div id="myCarousel" class="row-fluid show-grid carousel slide">
        <div class="carousel-inner span8">
            <? foreach ($model as $row): ?>
            <div class="<?=($counter == 1) ? 'active' : NULL;?> item" data-index="<?=$counter?>">
                <a href="<?=URL::site(($fed_id)?$fed_id.'/news/'.$row->{'sef_'.I18n::$lang}:'confederation/news/'.$row->{'sef_'.I18n::$lang})?>">
                    <span class="figure" style="background-image: url('<?=$row->photo_slider_s->url_cropped_image(null,null,'http')?>')"></span>
<!--                    <span class="figure">--><?//=$row->photo_loaded($row->photo_slider_s)?><!--</span>-->
<!--                    <span class="title"><span><?/*=Text::limit_words(strip_tags($row->text), 25)*/?></span></span>-->
                </a>
            </div>
            <? $counter++ ?>
            <? endforeach?>
            <?$counter = 0;?>
        </div>
        <ul class="carousel-nav unstyled span4 white_link">
            <? foreach ($model as $row1): ?>
            <li><a href="<?=URL::site(($fed_id)?$fed_id.'/news/'.$row1->{'sef_'.I18n::$lang}:'confederation/news/'.$row1->{'sef_'.I18n::$lang})?>"
                   data-slide-to="<?=$counter?>"><strong><span class="link"><span class="ls"><?=Text::limit_words(strip_tags($row1->title), 8)?></span></span></strong></a>
            </li>
            <? $counter++ ?>
            <? endforeach?>
        </ul>
    </div>
</div>

