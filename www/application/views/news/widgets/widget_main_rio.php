<div class="main-col-left">
    <div class="slider1 owl-carousel">
        <? foreach ($model as $row): ?>
            <div>
                <a href="<?=URL::site(($fed_id)?$fed_id.'/news/'.$row->{'sef_'.I18n::$lang}:'confederation/news/'.$row->{'sef_'.I18n::$lang})?>">
                    <img src="<?=$row->photo_slider_s->url_cropped_image(null,null,'http')?>" alt="<?=Text::limit_words(strip_tags($row->title), 8)?>">
                </a>
            </div>
        <? endforeach?>
    </div>
</div>