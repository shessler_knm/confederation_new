<html>
<head>
    <title><?=Request::current()->query('title')?></title>
    <script type="text/javascript" src="<?=Url::site('js/lib/jquery-1.9.0.min.js')?>"></script>
</head>
<body>

<form enctype="multipart/form-data" method="post">
    <div>
        <label for="file"><?=Request::current()->query('title')?></label>
        <input type="file" name="file" id="file">
        <?=Form::hidden('field', $field)?>
        <?=Form::hidden('type', $model->type)?>
        <?=$type=='Img'
        ?Form::hidden('url', Photo::Instance()->getThumbs($model->url(false),100,false) )
        :Form::hidden('url', $model->file() )?>
        <?=Form::hidden('id', $model->id)?>
        <?=Form::hidden('prev_id', '')?>
        <div class="error"><?=Arr::get($errors,'file','')?></div>
    </div>
    <input type="submit" id="save" name="save" value="<?=__('Загрузить')?>"/>
</form>
<script type="text/javascript">
    $(function(){
        var field=$('input[name="field"]').val();
        var type=$('input[name="type"]').val();
        var url=$('input[name="url"]').val();
        //   console.log(url);
        var id=$('input[name="id"]').val();
        if (id!='')
        {
            opener.set<?=$type?>(field,url,id,type);
            window.close();
        } else {
            $('input[name="prev_id"]').val(opener.getStorageId(field))
        };
    });
</script>
</body>
</html>