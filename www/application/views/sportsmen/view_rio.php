<section class="l-section-wrap">
    <div class="l-section main-inner-section clear-wrap">
        <div class="main-col-left typo-main">
            <ul class="breadcrumbs">
                <li><a href="<?=URL::site('rio2016');?>"><?=__('Главная');?></a></li>
                <li><a href="<?=URL::site('/')?>/rio2016/<?=$federation->sef;?>/sportsmen/list"><?= $federation->title . '. ' . __('Олимпийская сборная');?></a></li>
                <li><?=$model->get_full_name();?></li>
            </ul>
            <h1><?=$model->get_full_name();?></h1>

            <div class="clear-wrap">
                <div class="olimpians-view-photo">
                    <? if ($model->photo): ?>
                        <img src="<?=URL::site($model->photo_s->dir . '/'. $model->photo_s->name . '.' . $model->photo_s->type);?>" alt="<?= $model->get_full_name() ?>" class="img-response">
                    <? else: ?>
                        <img src="<?= URL::site('images/zagl.jpg') ?>" alt="<?= $model->get_full_name() ?>" class="img-response">
                    <?endif; ?>
                </div>
                <div class="olimpians-view-info">

                    <div class="olimpian-params-wrap">
                        <div class="olimpian-param">
                            <div class="olimpian-param-left"><?=__('Дата рождения')?>:</div>
                            <div class="olimpian-param-right"><?=$model->birthday?></div>
                        </div>
                        <div class="olimpian-param">
                            <div class="olimpian-param-left"><?= __('Весовая категория') ?>:</div>
                            <div class="olimpian-param-right">
                                <?=$model->weight?> <?= __('кг') ?>
                            </div>
                        </div>
                        <div class="olimpian-param">
                            <div class="olimpian-param-left"><?= __('Тренер') ?>:</div>
                            <div class="olimpian-param-right"><?=$model->coach->get_full_name()?></div>
                        </div>
                    </div>

                    <h3 class="olimpian-info-head"><?=__('Биография')?></h3>
                    <div class="olimpian-info-text">
                        <?=$model->biography?>
                    </div>
                </div>
                <!-- /olimpians-view-info -->
            </div>

        </div>
        <!-- /col -->
        <?=View::factory('rio/right_col');?>
        <!-- /col -->
    </div>
</section>