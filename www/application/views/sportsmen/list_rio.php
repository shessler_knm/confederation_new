<section class="l-section-wrap">
    <div class="l-section main-inner-section clear-wrap">
        <div class="main-col-left typo-main">
            <ul class="breadcrumbs">
                <li><a href="<?=URL::site('rio2016');?>"><?=__('Главная');?></a></li>
                <li><?= $federation->title . '. ' . __('Олимпийская сборная');?></li>
            </ul>
            <h1><?= $federation->title . '. ' . __('Олимпийская сборная');?></h1>

            <ul class="category-triggers">
                <? foreach ($model as $row): ?>
                    <li <?= $row->{'sef_' . I18n::$lang} == $sef ? 'class="active"' : '' ?>>
                        <a href="<?= URL::site('/').'/rio2016/' . $fed_id . '/sportsmen/list/' . $row->{'sef_' . I18n::$lang} ?>">
                            <?= $row->title ?>
                        </a>
                    </li>
                <? endforeach ?>
            </ul>

            <? foreach ($players as $player): ?>
                <div class="olimpians-person">
                    <div class="op-photo">
                        <? if ($player->photo): ?>
                            <img src="<?=URL::site($player->photo_s->dir . '/'. $player->photo_s->name . '.' . $player->photo_s->type);?>" alt="<?= $player->get_full_name() ?>" class="img-response">
                        <? else: ?>
                            <img src="<?= URL::site('images/zagl.jpg') ?>" alt="<?= $player->get_full_name() ?>" class="img-response">
                        <?endif; ?>
                    </div>
                    <div class="op-name">
                        <a href="<?= URL::site('/rio2016').'/'.$fed_id . '/sportsmen/view/' . $player->{'sef_' . I18n::$lang} ?>"><?= $player->get_full_name() ?></a>
                    </div>
                    <div class="op-info">
                        <div class="op-info-left"><?= __('Дата рождения') ?>:</div>
                        <div class="op-info-right"><?= Date::textdate($player->birthday, 'd m Y') ?></div>
                    </div>
                    <div class="op-info">
                        <div class="op-info-left"><?= __('Весовая категория') ?>:</div>
                        <div class="op-info-right">
                            <?switch ($player->category) {
                                case 0:
                                    $v = 'не указано';
                                    break;
                                case 1:
                                    $v = 'до';
                                    break;
                                case 2:
                                    $v = '';
                                    break;
                                case 3:
                                    $v = 'свыше';
                                    break;
                            }?>
                            <?= $v . " " . $player->weight ?> <?= __('кг') ?>
                        </div>
                    </div>
                    <div class="op-info">
                        <div class="op-info-left"><?= __('Тренер') ?>:</div>
                        <div class="op-info-right"><?= $player->coach->get_full_name() ?></div>
                    </div>
                </div>
                <!-- /person -->
            <? endforeach; ?>

        </div>
        <!-- /col -->
        <?=View::factory('rio/right_col');?>
        <!-- /col -->
    </div>
</section>