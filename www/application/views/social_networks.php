<div class="social_widget">
    <h2><?= __("Мы в соцсетях") ?></h2>
    <ul class="unstyled inline" id="social_tab">
        <li class="active"><a href="#social_ig"><i class="ico-socials ig">&nbsp;</i></a></li>
        <li><a href="#social_fb"><i class="ico-socials fb">&nbsp;</i></a></li>
        <li><a href="#social_tw"><i class="ico-socials tw">&nbsp;</i></a></li>
        <li><a href="#social_vk"><i class="ico-socials vk">&nbsp;</i></a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="social_ig">
            <iframe src='/inwidget/index.php'<?php /*URL::site('/inwidget/index.php')*/?>' scrolling='no' frameborder='no' style='border:none;width:260px;height:330px;overflow:hidden;'></iframe>
        </div>
        <div class="tab-pane" id="social_fb">
            <div id="fb-root"></div>
            <iframe name="f154590908" width="300px" height="360px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:page Facebook Social Plugin"
                    src="http://www.facebook.com/v2.3/plugins/page.php?app_id=&amp;channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter%2F1ldYU13brY_.js%3Fversion%3D41%23cb%3Df179644e8%26domain%3Dconfederation.kz%26origin%3Dhttp%253A%252F%252Fconfederation.kz%252Ff2d80cc6a8%26relation%3Dparent.parent&amp;container_width=260&amp;height=360&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2FConfederatSport&amp;locale=ru_RU&amp;sdk=joey&amp;show_facepile=true&amp;show_posts=true&amp;width=300"
                    style="border: none; visibility: visible; width: 300px; height: 360px;" class=""></iframe>
        </div>

        <div class="tab-pane" id="social_tw">
            <?if (I18n::$lang=='ru'||I18n::$lang=='en'):?>
            <a class="twitter-timeline" href="https://twitter.com/ConfederSport"
               data-widget-id="345402143378898944"><?= __('Твиты пользователя') ?> @ConfederSport</a>
            <script>!function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                    if (!d.getElementById(id)) {
                        js = d.createElement(s);
                        js.id = id;
                        js.src = p + "://platform.twitter.com/widgets.js";
                        fjs.parentNode.insertBefore(js, fjs);
                    }
                }(document, "script", "twitter-wjs");</script>
            <?endif?>
            <?if(I18n::$lang=='kz'):?>
            <a class="twitter-timeline" href="https://twitter.com/ConfederSportKZ"
               data-widget-id="456367275163934720"><?=__('Твиты пользователя')?> @ConfederSportKZ</a>
            <script>!function(d,s,id){
                    var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';
                    if(!d.getElementById(id)){
                        js=d.createElement(s);
                        js.id=id;js.src=p+"://platform.twitter.com/widgets.js";
                        fjs.parentNode.insertBefore(js,fjs);}
                }(document,"script","twitter-wjs");</script>
            <?endif?>

        </div>

        <div class="tab-pane" id="social_vk">
            <div id="vk_groups"
                 style="height: 842px; width: 220px; background-image: none; background-position: initial initial; background-repeat: initial initial;">
                <?/*if (I18n::$lang=='ru'||I18n::$lang=='en'):*/?>
                <!--<iframe name="fXD6eab5" frameborder="0"
                        src="http://vk.com/widget_community.php?app=0&amp;width=220px&amp;_ver=1&amp;gid=52891963&amp;mode=0&amp;color1=FFFFFF&amp;color2=2B587A&amp;color3=5B7FA6&amp;height=400&amp;url=http%3A%2F%2Fsport.dev%2Fconfederation&amp;1405338c06d"
                        width="220" height="200" scrolling="no" id="vkwidget1"
                        style="overflow: hidden; height: 842px;">
                </iframe>-->
                    <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>

                 <!-- VK Widget -->
                <div id="vk_groups"></div>
                <script type="text/javascript">
                    VK.Widgets.Group("vk_groups", {mode: 2, width: "260", height: "400"}, 52891963);
                </script>
                <?/*endif*/?><!--
                <?/*if(I18n::$lang=='kz'):*/?>
                    <script type="text/javascript" src="//vk.com/js/api/openapi.js?111"></script>

                    <!-- VK Widget
                    <script type="text/javascript">
                        VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "400", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 67413254);
                    </script>
                --><?/*endif*/?>
            </div>
        </div>
    </div>
</div>

<div class="social_widget hide">
    <h2><?= __("Мы в соцсетях") ?></h2>
    <ul class="unstyled inline">
        <li><a href="#social_ig"><i class="ico-socials ig">&nbsp;</i></a></li>
        <li><a href="#social_fb"><i class="ico-socials fb">&nbsp;</i></a></li>
        <li class="active"><a href="#social_tw"><i class="ico-socials tw">&nbsp;</i></a></li>
        <li><a href="#social_vk"><i class="ico-socials vk">&nbsp;</i></a></li>
    </ul>
</div>
