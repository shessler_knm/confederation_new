<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <h1><?= __('Сборные команды') ?></h1>

            <div class="teams_section">
                <? if ($no_team): ?>
                    <h3><?= __('Команды не найдены') ?></h3>
                <? else: ?>

                    <ul class="unstyled inline nav_tabs">
                        <? foreach ($model as $row): ?>
                            <li <?= $row->{'sef_' . I18n::$lang} == $sef ? 'class="active"' : '' ?>><a
                                    href="<?= URL::site($fed_id . '/team/' . $row->{'sef_' . I18n::$lang}) ?>"
                                    class="link_dotted"><?= $row->title ?></a></li>
                        <? endforeach ?>
                    </ul>

                    <ul class="unstyled inline teams_list">
                        <? foreach ($players as $player): ?>
                            <li class="column">
                                <div class="inner">
                                    <? if ($player->photo): ?>
                                        <div class="img-circle">
                                            <?= $player->photo_s->html_cropped_img(200, null, array('alt' => $player->get_full_name())) ?>
                                            <a href="<?= URL::site($fed_id . '/team/person/' . $player->{'sef_' . I18n::$lang}) ?>"
                                               class="mask175"></a>
                                        </div>
                                    <? endif ?>
                                    <? if (!$player->photo): ?>
                                        <div class="img-circle">
                                            <img src="<?= URL::site('images/zagl.jpg') ?>"
                                                 alt="<?= $player->get_full_name() ?>">
                                            <a href="<?= URL::site($fed_id . '/team/person/' . $player->{'sef_' . I18n::$lang}) ?>"
                                               class="mask175"></a>
                                        </div>
                                    <? endif ?>
                                    <strong><a
                                            href="<?= URL::site($fed_id . '/team/person/' . $player->{'sef_' . I18n::$lang}) ?>"><?= $player->get_full_name() ?></a></strong>
                                    <dl class="dl-horizontal">
                                        <dt class="muted"><?= __('Дата рождения') ?>:</dt>
                                        <dd><?= Date::textdate($player->birthday, 'd m Y') ?></dd>
                                        <dt class="muted"><?= __('Весовая категория') ?>:</dt>
                                        <?switch ($player->category) {
                                            case 0:
                                                $v = 'не указано';
                                                break;
                                            case 1:
                                                $v = 'до';
                                                break;
                                            case 2:
                                                $v = '';
                                                break;
                                            case 3:
                                                $v = 'свыше';
                                                break;
                                        }?>
                                        <dd><?= $v . " " . $player->weight ?> <?= __('кг') ?></dd>
                                        <dt class="muted"><?= __('Тренер') ?>:</dt>
                                        <dd><?= $player->coach->get_full_name() ?></dd>
                                    </dl>
                                </div>
                            </li>
                        <? endforeach ?>
                    </ul>
                <?endif ?>
            </div>
        </div>
    </div>
    <div class="aside">
        <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div>
        <?= View::factory('social_networks') ?>
    </div>
</div>
<?= $pagination ?>
