<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <div class="page_section">
                <div class="media teams_list" style="padding-top: 25px;">
                    <?if ($model->photo):?>
                        <div class="img-circle mask200 pull-left">
                            <?=$model->photo_s->html_cropped_img(400,null,array('alt'=>$model->get_full_name()))?>
                            <!-- <div class="mask200"></div> -->
                        </div>
                    <?endif?>
                    <?if(!$model->photo):?>
                        <div class="img-circle mask200 pull-left">
                            <img src="<?=URL::site('images/zagl.jpg')?>">
                            <!-- <div class="mask200"></div> -->
                        </div>
                    <?endif?>
                    <div class="media-body">
                        <h4><?=$model->get_full_name()?></h4>
                        <dl class="dl-horizontal">
                            <dt class="muted"><?=__('Дата рождения')?>:</dt>
                            <dd><?=$model->birthday?></dd>
                            <dt class="muted"><?=__('Место рождения')?>:</dt>
                            <dd><?=$model->birthplace?></dd>
                            <dt class="muted"><?=__('Весовая категория')?>:</dt>
                            <dd><?=$model->weight?> <?=__('кг')?></dd>
                            <dt class="muted"><?=__('Тренер')?>:</dt>
                            <dd><?=$model->coach->get_full_name()?></dd>
                            <dt class="muted"><?=__('Биография')?>:</dt>
                            <dd><?=$model->biography?></dd>
                        </dl>
                    </div>
                </div>
            </div>
            <?=__('Понравился материал? Поделитесь им с друзьями в соцсетях!')?>
            <div class="social_block" id="social_block">
                Yandex share?
            </div>

        </div>
    </div>

    <div class="aside">
        <!-- <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute()?>
        </div> -->
        <?=View::factory('social_networks')?>
    </div>
</div>