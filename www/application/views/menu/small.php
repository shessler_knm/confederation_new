<div class="data_filter">
    <? foreach ($order as $key): ?>
        <? if (Arr::get($elements[$key], 'root')): ?>
            <a href="<?= $elements[$key]['link'] ?>" class="filter_articles <?php if ($elements[$key]['link'] == (I18n::$lang != 'kz' ? '/' . I18n::$lang : null) . Helper::currentURI()){ echo "active";} ?>">
                <span class="link"><?= $elements[$key]['title'] ?></span>
            </a>
        <? endif; ?>
    <? endforeach; ?>
</div>
