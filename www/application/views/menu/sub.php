<? $count_child = (Arr::get($element, 'childrens')) ? count($element['childrens']) : 0; ?>
<? if ($count_child): ?>
    <li class="dropdown-submenu">
        <a <?= Arr::get($element, 'blank') ?> tabindex="-1" href="<?= $element['link'] ?>"><?= $element['title'] ?></a>
        <ul class="dropdown-menu">
            <?= Controller_Menu::children($element['childrens'], $elements) ?>
        </ul>
    </li>
<? else: ?>
<li>
    <? if ($element['link'] == (I18n::$lang != 'kz' ? '/' . I18n::$lang : null) . Helper::currentURI()): ?>
        <a tabindex="-1" <?= Arr::get($element, 'blank') ?> href="<?= $element['link'] ?>"
           rel="nofollow"><?= $element['title'] ?></a>
    <? else: ?>
        <a tabindex="-1" <?= Arr::get($element, 'blank') ?> href="<?= $element['link'] ?>"><?= $element['title'] ?></a>
    <? endif?>
</li>
<? endif; ?>
