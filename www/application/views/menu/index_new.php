<div class="mobile"><span class="btn-mobile">Меню</span></div>
<ul class="unstyled inline js-menu menu">
    <? foreach ($order as $key): ?>
        <? if (Arr::get($elements[$key], 'root')): ?>
            <? $count_child = (Arr::get($elements[$key], 'childrens')) ? count($elements[$key]['childrens']) : 0; ?>
            <li>
                <? if ($count_child): ?>

                        <a class="link <?= Arr::get($elements[$key], 'active') ?>"
                           href="#">
                            <span><?= $elements[$key]['title'] ?></span>
                        </a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                            <?= Controller_Menu::children($elements[$key]['childrens'], $elements) ?>
                        </ul>

                <? else: ?>
                    <? if ($elements[$key]['link'] == (I18n::$lang != 'kz' ? '/' . I18n::$lang : null) . Helper::currentURI()): ?>
                        <a class="link <?= Arr::get($elements[$key], 'active') ?>" <?= Arr::get($elements[$key], 'target') ?>
                           href="<?= $elements[$key]['link'] ?>" rel="nofollow"><span><?= $elements[$key]['title'] ?></span></a>
                    <? else: ?>
                        <? if (!($user && $user->socpage && array_search($elements[$key]['restriction'], $restrictions))):?>
                            <a class="link <?= Arr::get($elements[$key], 'active') ?>" <?= Arr::get($elements[$key], 'target') ?>
                               href="<?= $elements[$key]['link'] ?>"><span><?= $elements[$key]['title'] ?></span></a>
                        <?endif ?>
                    <? endif ?>
                <? endif; ?>
            </li>
        <? endif; ?>
    <? endforeach; ?>
</ul>