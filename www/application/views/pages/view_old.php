<div class="row_fluid">
    <div class="row_section">
        <div class="section">
            <div class="page_section">
                <h1><?= $model->title ?></h1>
                <? if ($model->photo == true): ?>
                    <?= $model->photo_s->html_img(null,null,array('alt'=>$model->title)) ?>
                <? endif ?>
                <div class="text-left">
                    <!--                                        <a href="" title=""-->
                    <!--                                           data-index="" class="open_fancybox">-->
                    <?= $model->text ?>
                    <!--                                        </a>-->
                </div>
            </div>
            <?=__('Понравился материал? Поделитесь им с друзьями в соцсетях!')?>
            <div class="social_block" id="social_block"></div>
        </div>
    </div>
    <div class="aside">
        <div class="banner">
            <img src="<?= URL::site('images/big_logo.png') ?>" alt="">
        </div>
        <?= View::factory('social_networks') ?>
    </div>
</div>