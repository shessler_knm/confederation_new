<?php
$request =  Request::initial();
$url = strtolower($request->controller().'/'.$request->param('id'));
?>
<div class="row_fluid clearfix bg-white <?= ($url == 'page/partners') ? 'clmn_right partners' : '' ?>">
    <div class="row_section">
        <?php
            echo View::factory('breadcrumbs')->set('_breadcrumbs', $_breadcrumbs)->render();
        ?>

        <div class="section">
            <h1><?= $model->title ?></h1>
            <div class="page_section">

                <? if ($model->photo == true): ?>
                    <?= $model->photo_s->html_img(null,null,array('alt'=>$model->title)) ?>
                <? endif ?>

                <?php
                switch ($url) {
                    case('page/partners') :
                        require_once Kohana::find_file('views', 'partner/list', 'php');
                        break;
                    default:
                        echo $model->text;
                }
                ?>

            </div>

            <div class="social_block" >
                <?=__('Понравился материал? Поделитесь им с друзьями в соцсетях!')?>
                <div id="social_block">

                </div>
            </div>
        </div>
    </div>
    <?php
    switch ($url) {
        case('page/partners') :
            echo "";
            break;
        default:
            ?>
                <div class="aside">
                    <? if (strstr(Request::initial()->uri(),'grafik-kvalifikatsionnih-turnirov-na-olimpiiskie-igri-2016-goda') !== false): ?>
                        <a href="http://confederation.kz/ru/confederation/page/sistema-otbora-dlya-uchastiya-v-xxxi-letnih-olimpiiskih-igrah-v-rio-de-zhaneiro-utverzhdennaya-mezhdunarodnim-olimpiiskim-komitetom">
                            <img src="<?=URL::site('images/selection.jpg')?>" style="margin: 0 0 20px 40px;">
                        </a>
                    <? else: ?>
                        <?= View::factory('social_networks') ?>
                    <? endif; ?>
                </div>
            <?php
    }
    ?>

</div>