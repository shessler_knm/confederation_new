<div class="row_fluid clearfix bg-white">
<div class="breadcrumbs">
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('Личный тренер')?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach/top/100')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('ТОП участников') ?></span></a>
    </div>
</div>
    <div class="row_section">
        <div class="section">
            <h1><?= __('ТОП 100 участников') ?></h1>

            <form class="form_search_user_top" method="post">
                <div class="puli-left placeholder-group">
                    <label for="i_top" class="placeholder_label"><?= __('Имя пользователя') ?></label>
                    <input type="text" name="name" value="<?= $result ? $post : '' ?>" id="i_top"
                           class="input_top input_placeholder">
                </div>

                <input type="submit" value="<?= __('Найти') ?>" class="btn color">
            </form>
            <? if (!$empty): ?>
                <ul class="unstyled user_top_list">
                    <? foreach ($model as $row): ?>
                        <li class="media">

                            <? if ($row->user->photo_s->loaded()): ?>
                                <div class="pull-left">
                                    <div class="img-circle">
                                        <?= $row->user->photo_s->html_img(150) ?>
                                        <div class="mask84"></div>
                                    </div>
                                    <span class="icon_star"><?= $row->place ?></span>
                                </div>
                            <? endif ?>

                            <div class="media-body">

                                <div class="media-heading media-heading-top">
                                    <div class="pull-left"><a
                                            href="<?= URL::site('privatecoach/profile/' . $row->user->id) ?>"
                                            class="link top_user_name">
                                            <strong class="ls"><?= $row->user->get_full_name() ?></strong>
                                        </a></div>
                                    <? if ($user): ?>
                                        <? if ($row->user_id != $user->id): ?>
                                            <? if (!$subscribe->find_subs($user->id, $row->user_id)->loaded()): ?>
                                                <span class="pull-right">
                                            <a href="#" class="link subscribe" data-id="<?= $row->user_id ?>"
                                               data-url="<?= URL::site('privatecoach/subscribe/add', 'http') ?>"
                                               data-change="0"><span
                                                    class="ls"><?= __('Подписаться') ?></span></a></span>
                                            <? else: ?>
                                                <span class="pull-right">
                                            <a href="#" class="link subscribe" data-id="<?= $row->user_id ?>"
                                               data-url="<?= URL::site('privatecoach/subscribe/delete', 'http') ?>"
                                               data-change="1"><span
                                                    class="ls"><?= __('Отписаться') ?></span></a></span>
                                            <?endif ?>
                                        <? endif ?>
                                        <?else:?>
                                        <span class="pull-right">
                                            <a class="link subscribe" data-target="#myModal" data-toggle="modal"><span
                                                    class="ls"><?= __('Подписаться') ?></span></a></span>
                                    <? endif ?>
                                </div>
                                <div class="media-heading heading_title_program">
                                    <a href="<?= URL::site('privatecoach/program/view/' . $programs[$row->user->id]->parent->id) ?>"class="link">
                                        <span class="ls"><?= $programs[$row->user->id]->title ?></span>
                                    </a>
                                </div>

                                <!--                                <div class="user_goal"><em>-->
                                <?//= $row->user->mission_type->title ?><!--</em></div>-->
                                <!--                                <div class="media_text">-->
                                <?//= $row->user->annotation ?><!--</div>-->
                                <?= Request::factory(I18n::$lang.'/privatecoach/profile/photo_preview/' . $row->user_id)->execute() ?>
                            </div>
                        </li>
                    <? endforeach ?>
                </ul>
                <?= $pagination ?>
            <? else: ?>
                <?= $empty ?>
            <?endif ?>
        </div>
    </div>

    <div class="aside">
        <!-- <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div> -->

        <?= View::factory('social_networks') ?>

    </div>
</div>
<?= Form::input('sub', __('Подписаться'), array('style' => 'display:none', 'id' => 'sub')); ?>
<?= Form::input('unsub', __('Отписаться'), array('style' => 'display:none', 'id' => 'unsub')); ?>
