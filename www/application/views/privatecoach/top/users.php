
<div class="row_fluid clearfix bg-white">
<div class="breadcrumbs">
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('Личный тренер')?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach/top/users')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('Участники') ?></span></a>
    </div>
</div>
    <div class="row_section">
        <div class="section">
            <h1><?= __('Участники') ?></h1>

            <div class="form_search_user_top" method="post">
                <div class="puli-left placeholder-group">
                    <label for="i_top" class="placeholder_label"><?= __('Имя пользователя') ?></label>
                    <input type="text" name="name" value="<?= $result ? $name : '' ?>" id="i_top"
                           class="input_top input_placeholder">
                </div>

                <a href="#" class="btn color submit"><?= __('Найти') ?></a>
            </div>
            <? if (!$empty): ?>
                <ul class="unstyled user_top_list"  data-url="<?= URL::site('privatecoach/top/ajax_users') ?>">

                </ul>
            <? else: ?>
                <?= $empty ?>
            <?endif ?>
        </div>
    </div>

    <div class="aside">
        <!-- <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div> -->

        <?= View::factory('social_networks') ?>

    </div>
</div>
<?= Form::input('sub', __('Подписаться'), array('style' => 'display:none', 'id' => 'sub')); ?>
<?= Form::input('unsub', __('Отписаться'), array('style' => 'display:none', 'id' => 'unsub')); ?>