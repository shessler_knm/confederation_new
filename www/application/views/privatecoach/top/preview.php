<ul class="unstyled inline figure_list nav_column coach_list">
    <? foreach ($model as $row): ?>
        <li class="column pull-left">

            <?/* if ($row->photo_s->loaded()): */?>
                <div class="img-circle">
                    <a href="<?= URL::site('privatecoach/profile/' . $row->id) ?>"><?= $row->photo_s->html_img(150) ?></a>
                    <div class="mask84"></div>
                </div>
            <?/* endif */?>

            <div class="media-body">
                <!--<strong><?/*= '#' . $place */?></strong>-->

                <div class="media_text">
                    <a href="<?= URL::site('privatecoach/profile/' . $row->id) ?>"
                       class="link"><span class="ls"><?= $row->get_full_name() ?></span></a>
                </div>

<!--                <em class="muted">59 лайков</em>-->
                <? if ($user): ?>
                    <? if ($row->id != $user->id): ?>
                        <? if (!$subscribe->find_subs($user->id, $row->id)->loaded()): ?>
                            <a href="#" class="btn color subscribe" data-id="<?= $row->id ?>"
                               data-url="<?= URL::site('privatecoach/subscribe/add', 'http') ?>"
                               data-change="0"><?= __('Подписаться') ?></a>
                        <? else: ?>
                            <a href="#" class="btn color subscribe grey_btn" data-id="<?= $row->id ?>"
                               data-url="<?= URL::site('privatecoach/subscribe/delete', 'http') ?>"
                               data-change="1"><?= __('Вы подписаны') ?></a>
                        <?endif ?>
                    <? endif ?>
                <? endif ?>

            </div>
        </li>
        <? $place++ ?>
    <? endforeach ?>
</ul>
<?= Form::input('sub', __('Подписаться'), array('style' => 'display:none', 'id' => 'sub')); ?>
<?= Form::input('unsub', __('Отписаться'), array('style' => 'display:none', 'id' => 'unsub')); ?>
