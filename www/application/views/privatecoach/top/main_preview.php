<div class="header_section">
    <em><?= __('Наши лидеры') ?></em>
</div>
<ul class="unstyled inline leaders-list">
    <? foreach ($model as $row): ?>
        <li>
            <div class="img-circle">
                <?= $row->photo_s->html_img(100) ?>
                <div class="mask84"></div>
            </div>
            <a href="#">Заман Заманухин</a>
        </li>
    <? endforeach ?>
</ul>