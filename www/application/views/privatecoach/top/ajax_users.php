<? foreach ($model as $row): ?>
    <li class="media">

        <? if ($row->photo_s->loaded()): ?>
            <div class="pull-left">
                <div class="img-circle">
                    <?= $row->photo_s->html_img(150) ?>
                    <div class="mask84"></div>
                </div>
            </div>
        <? endif ?>

        <div class="media-body">

            <div class="media-heading media-heading-top">
                <div class="pull-left"><a
                        href="<?= URL::site('privatecoach/profile/' . $row->id) ?>"
                        class="link top_user_name">
                        <strong class="ls"><?= $row->get_full_name() ?></strong>
                    </a></div>

                <? if ($user): ?>
                    <? if ($row->id != $user->id): ?>
                        <? if (!$subscribe->find_subs($user->id, $row->id)->loaded()): ?>
                            <span class="pull-right">
                                            <a href="#" class="link subscribe" data-id="<?= $row->id ?>"
                                               data-url="<?= URL::site('privatecoach/subscribe/add', 'http') ?>"
                                               data-change="0"><span
                                                    class="ls"><?= __('Подписаться') ?></span></a></span>
                        <? else: ?>
                            <span class="pull-right">
                                            <a href="#" class="link subscribe" data-id="<?= $row->id ?>"
                                               data-url="<?= URL::site('privatecoach/subscribe/delete', 'http') ?>"
                                               data-change="1"><span
                                                    class="ls"><?= __('Отписаться') ?></span></a></span>
                        <?endif ?>
                    <? endif ?>
                <? else: ?>
                    <span class="pull-right">
                                            <a class="link subscribe" data-target="#myModal" data-toggle="modal"><span
                                                    class="ls"><?= __('Подписаться') ?></span></a></span>
                <? endif ?>
            </div>

            <div class="media-heading heading_title_program">
                <a href="<?= URL::site('privatecoach/program/view/' . $programs[$row->id]->parent->id) ?>"
                   class="link">
                    <span class="ls"><?= $programs[$row->id]->title ?></span>
                </a>
            </div>

            <!--                                <div class="user_goal"><em>-->
            <?//= $row->user->mission_type->title ?><!--</em></div>-->
            <!--                                <div class="media_text">-->
            <?//= $row->user->annotation ?><!--</div>-->
            <?= Request::factory(I18n::$lang . '/privatecoach/profile/photo_preview/' . $row->id)->execute() ?>
        </div>
    </li>
<? endforeach ?>
<? if ($amount > 0): ?>
    <div class="hr_top pagination_ajax text-center">
        <a href="#" id="more" data-counter="1"
           data-url="<?= URL::site('privatecoach/top/ajax_users') ?>" class="link"><span
                class="ls"><?= __('Показать еще') ?></span></a>
    </div>
<? endif ?>
