<div class="row_fluid page_diet clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <h1><?= __('Добавление ингредиента') ?></h1>
            <? if ($post): ?>
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?= __('Ингредиент отправлен на модерацию') ?>
                </div>
            <? endif ?>
            <div class="pull-right upload_img">
                <div class="preview_img">
                    <? if ($storage->loaded()): ?>
                        <img src="<?= $storage->url() ?>"> <!--Алмас, убери отсюда style="height:220px" хз откуда еще он у тебя тут ставится-->
                    <? else: ?>
                        <span><?= __('Нет фотографии') ?></span>
                    <? endif ?>
                </div>
                <form id="ing_form" name="uploadForm" action="<?= URL::site('privatecoach/diet/load_photo') ?>"
                      method="post"
                      enctype="multipart/form-data">
                    <input type="submit" id="upload" value="Upload File to Server" style="display: none">
                    <input type="file" name="image" class="input_upload">
                </form>
            </div>
            <form class="form_create" name="create_dish" method="post">
                <div class="media-body hr_bottom">
                    <div class="control-group">
                        <label class="control-label"><?= __('Название') ?></label>

                        <div class="controls">
                            <input type="text" name="title" class="input_title" value="<?= Arr::get($post, 'title') ?>">
                        </div>
                        <p class="error"><?= Arr::get($error, 'title') ?></p>
                    </div>

                    <div class="control-group">
                        <label class="control-label"><?= __('Описание') ?></label>

                        <div class="controls">
                            <textarea name="description"><?= Arr::get($post, 'description') ?></textarea>
                        </div>
                        <p class="error"><?= Arr::get($error, 'description') ?></p>
                    </div>
                </div>
                <div class="media-body">
                    <div class="control-group">
                        <label class="control-label"><?= __('Пищевая ценность') ?> (<?= __('на 100гр') ?>)</label>

                        <div class="controls">
                            <div class="controls">
                                <label><?= __('Калории') ?></label>
                                <input name="calories" type="text" class="input_mini" value="<?= Arr::get($post, 'calories') ?>">
                                <span><?= __('ккал') ?></span>
                                <p class="error"><?= Arr::get($error, 'calories') ?></p>
                            </div>
                            <div class="controls">
                                <label><?= __('Белки') ?></label>
                                <input name="protein" type="text" class="input_mini" value="<?= Arr::get($post, 'protein') ?>">
                                <span><?= __('гр') ?></span>
                                <p class="error"><?= Arr::get($error, 'nutrients') ?></p>
                            </div>
                            <div class="controls">
                                <label><?= __('Углеводы') ?></label>
                                <input name="carbohydrate" type="text" class="input_mini" value="<?= Arr::get($post, 'carbohydrate') ?>">
                                <span><?= __('гр') ?></span>
                                <p class="error"><?= Arr::get($error, 'nutrients') ?></p>
                            </div>
                            <div class="controls">
                                <label><?= __('Жиры') ?></label>
                                <input name="fat" type="text" class="input_mini" value="<?= Arr::get($post, 'fat') ?>">
                                <span><?= __('гр') ?></span>
                                <p class="error"><?= Arr::get($error, 'nutrients') ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <?= Form::input('image_file', null, array('style' => 'display:none')) ?>
                <div class="control_btn">
                    <input type="submit" value="<?= __('Добавить ингредиент') ?>" class="btn color">
                </div>
            </form>
        </div>
    </div>
    <div class="aside">
        <?= View::factory('social_networks') ?>
    </div>
</div>