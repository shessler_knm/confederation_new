<? foreach ($data as $id => $list): ?>
    <table class="table_diet ingestion" data-id="<?= $id ?>">
        <thead>
        <tr>
            <th class="green col1 bootstrap-timepicker dish_timepicker">
                <a href="#" class="i_white_del delete_ingestion"></a>
                               <span> <a href="#" class="add-on" data-format="hh:mm" type="text">
                                       <?= Date::formatted_time($times[$id], 'H:i') ?>
                                   </a></span>
                <input class="input_date_picker" style="display: none" data-format="hh:mm:ss"
                       type="text">
            </th>
            <th class="col2"></th>
            <th><?= __('Калории') ?></th>
            <th><?= __('Белки') ?></th>
            <th><?= __('Углеводы') ?></th>
            <th><?= __('Жиры') ?></th>
        </tr>
        </thead>

        <tbody>
        <!-- столбцы-->
        <? foreach ($list as $row): ?>
                <tr class="ingestion_item">

                    <td class="col1">
                        <a href="#" class="i_blue_del delete_dish" data-id="<?= Arr::get($row, 'id') ?>"></a>

                        <div class="img"
                             style="background-image: url('<?= URL::site('images/ingredients.jpg') ?>');"></div>
                    </td>
                    <td>
                        <a href="#" class="link_dotted"><?= Arr::get($row, 'title') ?></a>

                        <div class="controls">
                            <input class="dish_weight" value="<?= Arr::get($row, 'weight') ?>" type="text"
                                   data-id="<?= Arr::get($row, 'id') ?>">
                            <span><?= __('гр') ?></span>
                        </div>
                    </td>
                    <td class="calories"><?= Arr::get($row, 'calories') ?> </td>
                    <td class="protein"> <?= Arr::get($row, 'protein') ?> </td>
                    <td class="carbohydrate"><?= Arr::get($row, 'carbohydrate') ?> </td>
                    <td class="fat">  <?= Arr::get($row, 'fat') ?> </td>

                </tr>
        <? endforeach ?>

        </tbody>

        <!--        --><? //= $time ?>
    </table>
    <div class="link_control">
        <a href="#" class="link_dotted add_new_product"><span
                class="i_blue_add"></span><?= __('Добавить продукт') ?>
        </a>
    </div>
<? endforeach ?>



<div class="link_control top_hr">
    <a href="#" class="link_dotted add_ingestion" data-url="<?= URL::site('privatecoach/diet/add_ingestion') ?>"><span
            class="i_green_add"></span><?= __('Добавить прием пищи') ?></a>
</div>