<div class="row_fluid page_diet clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <h1><?= __('Добавление рецепта') ?></h1>
            <? if ($post): ?>
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?= __('Ваш рецепт отправлен на модерацию') ?>
                </div>
            <? endif ?>

            <div class="pull-right upload_img">

                <div class="preview_img">
                    <? if ($storage->loaded()): ?>
                        <img src="<?= $storage->url() ?>">
                    <? else: ?>
                        <span><?= __('Добавить фотографию') ?></span>
                    <? endif ?>
                </div>

                <form id="ing_form" name="uploadForm" action="<?= URL::site('privatecoach/diet/load_photo') ?>"
                      method="post"
                      enctype="multipart/form-data">
                    <input type="submit" id="upload" value="Upload File to Server" style="display: none">
                    <input type="file" name="image" class="input_upload">
                </form>
                <!-- Если не отправить изображение, то загружается ошибка, но ее почему-то не видно! -->
                <p class="error"><?= Arr::get($error, 'image_file') ?></p>
            </div>

            <form class="form_create" name="create_dish" method="post">
                <div class="media-body hr_bottom">
                    <div class="control-group">
                        <label class="control-label"><?= __('Название') ?></label>

                        <div class="controls">
                            <input type="text" name="title" class="input_title" value="<?= Arr::get($post, 'title') ?>">
                        </div>
                        <p class="error"><?= Arr::get($error, 'title') ?></p>
                    </div>

                    <div class="control-group">
                        <label class="control-label"><?= __('Описание') ?></label>

                        <div class="controls">
                            <textarea name="description"><?= Arr::get($post, 'description') ?></textarea>
                        </div>
                        <p class="error"><?= Arr::get($error, 'description') ?></p>
                    </div>
                    <div class="control-group">
                        <?= Form::select('category', $categories, Arr::get($post, 'category'), array('class' => 'custom_select')) ?>
                    </div>
                </div>

                <div class="media-body">
                    <div class="control-group">
                        <label class="control-label"><?= __('Ингредиент') ?></label>

                        <div class="control_autocomplit">
                            <div class="wrap_autocomplit">

                                <input class="typeahead" data-provide="typeahead" type="text" autocomplete="off"
                                       data-toggle=popover
                                       data-source="<?= URL::site('privatecoach/ingredient/source') ?>"
                                       data-content="<?= __('Такого ингредиента нет в нашей базе') ?>.">


                            </div>
                            <input class="add_data btn color" value="<?= __('Добавить') ?>">

                            <p class="error"><?= Arr::get($error, 'ings') ?></p>

                            <p class="error"><?= Arr::get($error, 'weight') ?></p>

                        </div>
                        <div id="ingredient_result" class="controls"
                             data-url="<?= URL::site('privatecoach/ingredient/add_element') ?>">
                            <? if ($dish_ingredient): ?>
                                <? foreach ($dish_ingredient as $row): ?>
                                    <div class="new_element controls">
                                        <label><a href="#" class="delete_ing icon i_blue_del" title="Удалить">
                                                &nbsp;</a><?= $row->title ?></label>
                                        <input type="hidden" name="ing_id_<?= $row->id ?>" value="<?= $row->title ?>">
                                        <input type="text" name="weight_<?= $row->id ?>" class="input_mini">
                                        <span><?= __('гр') ?></span>
                                    </div>

                                <? endforeach ?>
                            <? endif ?>
                        </div>

                        <div class="controls added_elements">

                        </div>
                    </div>
                </div>
                <?= Form::input('image_file', null, array('style' => 'display:none')) ?>
                <div class="control_btn">
                    <input type="submit" value="<?=__('Добавить рецепт')?>" class="btn color">
                </div>
            </form>

        </div>
    </div>
    <div class="aside">
        <?= View::factory('social_networks') ?>
    </div>
</div>