<tr class="ingestion_item">
    <td class="col1">
        <a href="#" class="i_blue_del delete_dish" data-id="<?= $dish_ration->id ?>"></a>

        <div class="img"
             style="background-image: url('<?= URL::site('images/ingredients.jpg') ?>');"></div>
    </td>
    <td>
        <a href="#" class="link_dotted"><?= $dish_ration->dish->title ?></a>

        <div class="controls">
            <input class="dish_weight" value="<?= $dish_ration->weight ?>" type="text"
                   data-id="<?= $dish_ration->id ?>">
            <span><?= __('гр') ?></span>
        </div>
    </td>
    <? foreach ($data as $key => $row): ?>
        <td class="<?= $key ?>"><?= $row ?></td>
    <? endforeach ?>
</tr>