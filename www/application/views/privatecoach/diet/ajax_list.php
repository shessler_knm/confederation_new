<? if ($count): ?>
    <div class="heading_search_menu">
        <div class="h3"><?= __('Результаты поиска') ?></div>
        <!-- Вот такой вот костыль с падежами. Русский язык явно не создан для программирования-->
        <? if ($count > 1): ?>
            <div
                class="result_search"><?= __('Найдено :count', array(':count' => Helper::word_cases($count, 'продуктов', 'продукт', 'продукта'))) ?></div>
        <? else: ?>
            <div
                class="result_search"><?= __('Найден :count', array(':count' => Helper::word_cases($count, 'продуктов', 'продукт', 'продукта'))) ?></div>
        <?endif ?>
    </div>


    <ul class="unstyled inline nav_tabs" id="dishes_tabs">
        <li class="active"><a href="#all" class="link_dotted"><?= __('Все') ?></a></li>
        <? foreach ($categories as $category): ?>
            <li><a href="#<?= $category->slug ?>" class="link_dotted"><?= $category->title ?></a></li>
        <? endforeach ?>
        <li><a href="#ingredients" class="link_dotted"><?= __('Ингредиенты') ?></a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="all">
            <ul class="unstyled inline clear nav_column diet_menu_list">
                <? foreach ($model as $row): ?>
                    <li class="column pull-left program_back_img">
                        <div class="inner">
                            <a href="#myDish" data-category="<?= get_class($row) ?>" data-id="<?= $row->id ?>"
                               data-toggle="modal" class="modal_anchor link">
                                <? if ($row->photo): ?>
                                    <span class="img" style="background-image: url('<?= $row->photo_s->url() ?>')"></span>
                                <?else:?>
                                    <span class="img" style="background-image: url('/images/default_diet.jpg')"></span>
                                <? endif ?>

                                <span class="ls">
                                <?= $row->title ?>
                            </span>
                            </a>

                            <? /*= Text::limit_chars(strip_tags($row->description), 220) */ ?>
                        </div>
                    </li>
                <? endforeach ?>
            </ul>
        </div>
    </div>

    <? foreach ($categories as $category): ?>
        <div class="tab-content">
            <div class="tab-pane" id="<?= $category->slug ?>">
                <ul class="unstyled inline clear nav_column diet_menu_list">
                    <? if ($data[$category->id]): ?>
                        <? foreach ($data[$category->id] as $row): ?>
                            <li class="column pull-left program_back_img">
                                <div class="inner">
                                    <a href="#myDish" data-category="Dish" data-id="<?= $row->id ?>" data-toggle="modal"
                                       class="modal_anchor link">
                                        <? if ($row->photo): ?>
                                            <span class="img"
                                                  style="background-image: url('<?= $row->photo_s->url() ?>')">

                                        </span>
                                        <?else:?>
                                            <span class="img" style="background-image: url('/images/default_diet.jpg')"></span>
                                        <? endif ?>

                                        <span class="ls">
                                        <?= $row->title ?>
                                    </span>
                                    </a>
                                    <? /*= Text::limit_chars(strip_tags($row->description), 220) */ ?>
                                </div>
                            </li>
                        <? endforeach ?>
                    <? endif ?>
                </ul>
            </div>
        </div>
    <? endforeach ?>

    <div class="tab-content">
        <div class="tab-pane" id="ingredients">
            <ul class="unstyled inline clear nav_column diet_menu_list">
                <? foreach ($ingredient as $row): ?>
                    <li class="column pull-left program_back_img">
                        <div class="inner">
                            <a href="#myDish" data-category="Ingredient" data-id="<?= $row->id ?>" data-toggle="modal"
                               class="modal_anchor link">
                                <? if ($row->photo): ?>
                                    <span class="img" style="background-image: url('<?= $row->photo_s->url() ?>')">
                                    <? /*= $row->photo_s->html_img(200, null, array('alt' => $row->title)) */ ?>
                                </span>
                                <?else:?>
                                    <span class="img" style="background-image: url('/images/default_diet.jpg')"></span>
                                <? endif ?>

                                <span class="ls">
                                <?= $row->title ?>
                            </span>
                            </a>
                            <? /*= Text::limit_chars(strip_tags($row->description), 220) */ ?>
                        </div>
                    </li>
                <? endforeach ?>
            </ul>
        </div>
    </div>

    <?= $pagination ?>
<? else: ?>
    <div class="heading_search_menu">
        <div class="h3"><?= __('Результаты поиска') ?></div>
    </div>
    <?= __('По данному запросу ничего не найдено') ?>
<? endif ?>

<!--<div class="hr_top pagination_ajax text-center">
    <a href="#" class="link"><span class="ls"><?/*= __('Показать еще') */?></span></a>
</div>-->

<div class="modal fade modal_dishes" id="myDish"
     data-url="<?= URL::site(I18n::$lang . '/privatecoach/diet/ajax_dish/') ?>" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <div class="modal-body" id="modal_dish">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->