<div class="popup_dish">


    <h3><?= $model->title ?></h3>
    <!-- спан не отображается=( сделайте с ним что-нибудь-->
    <div class="media hr_top">
    <div class="photo">
    <? if ($model->photo_s->loaded()): ?>
<!--            <span class="img" style="background-image: url('--><?//= $model->photo_s->url_image(200,200) ?><!--')"></span>-->
        <?= $model->photo_s->html_img(200, 200) ?>
    <? else: ?>
        <span class="img" style="background-image: url('/images/default_diet.jpg')"></span>
    <? endif ?>
    </div>
    <div class="media-body">
    <b><?= __('Пищевая ценность (в 100гр):') ?></b>
    <ul>
        <li><?= __('Калории') ?></li>
        <li><?= __('Белки') ?></li>
        <li><?= __('Углеводы') ?></li>
        <li><?= __('Жиры') ?></li>
    </ul>
    <ul>
        <li><?= $model->calories . ' ' . __('ккал') ?></li>
        <li><?= $model->protein . ' ' . __('гр') ?></li>
        <li><?= $model->carbohydrate . ' ' . __('гр') ?></li>
        <li><?= $model->fat . ' ' . __('гр') ?></li>
    </ul>
    <b><?= __('Описание:') ?></b>
    <div class="text_dish"><?= $model->description ?></div>
    </div>
    </div>
    <!-- end media -->
    <? if ($user): ?>
        <form name="new_my_dish">
            <ul class="my_dish-list">
            <input name="id" value="<?= $model->id ?>" style="display: none">
            <li>
                <b><?= __('Дата') ?></b>
                <span class="form_add_datepicker">
                <a href="#" class="add-on link_dotted"
                   data-lang="<?= I18n::$lang == 'kz' ? 'kk' : I18n::$lang ?>">
                    <?= date('d.m.Y') ?>
                </a>
                </span>
                <input name="date" value="<?= date('d.m.Y') ?>" style="display: none">
            </li>
            <li>
                <b><?= __('Время') ?></b>
                <span class="datetime">
                        <a href="#" class="add-on link_dotted" data-format="hh:mm" type="text">
                            <?= date('H:i') ?>
                        </a>
                    </span>
                <input name="time" value="<?= date('H:i') ?>" style="display: none">
            </li>
            <li>
                <b><?= __('Масса') ?></b>
                <?= Form::input('weight', null, array('type' => 'text', 'class' => 'input-mini')) ?>
                <?= __('гр') ?>
            </li>
            </ul>
        </form>
        <div>
        <?= HTML::anchor('#', __('Добавить в мой рацион'), array('class' => 'form_submit btn color', 'data-url' => URL::site('privatecoach/diet/add_to_my_diet'))) ?>
        </div>
    <? endif ?>
</div>