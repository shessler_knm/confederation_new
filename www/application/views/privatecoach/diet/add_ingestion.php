<table class="table_diet ingestion" data-id="<?= $ration->id ?>">
    <thead>
    <tr>
        <th class="green col1 bootstrap-timepicker dish_timepicker">
            <a href="#" class="i_white_del delete_ingestion"></a>
                               <span> <a href="#" class="add-on" data-format="hh:mm" type="text">
                                       00:00
                                   </a></span>
            <input class="input_date_picker" style="display: none" data-format="hh:mm:ss"
                   type="text">
        </th>
        <th class="col2"></th>
        <th><?= __('Калории') ?></th>
        <th><?= __('Белки') ?></th>
        <th><?= __('Углеводы') ?></th>
        <th><?= __('Жиры') ?></th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<div class="link_control">
    <a href="#" class="link_dotted add_new_product"><span
            class="i_blue_add"></span><?= __('Добавить продукт') ?>
    </a>
</div>