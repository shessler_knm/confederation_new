<div class="row_fluid page_diet clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <h1><?= __('Мой рацион') ?></h1>

            <!--<div class="date_my_diet">
                <p><?/*=__('Ваш рацион еще не составлен')*/?>.</p>
                <a href="#" class="link_date link_dotted" id="changeDate" data-lang="<?/*= I18n::$lang == 'kz' ? 'kk' : I18n::$lang */?>">
                    <span class="ls"><?/*=__('Выбрать дату начала')*/?></span>
                </a>
            </div>-->

            <div class="create_my_diet ">
                <div class="date_my_diet" data-changetime-url="<?= URL::site('privatecoach/diet/change_time') ?>"
                     data-changeweight-url="<?= URL::site('privatecoach/diet/change_weight') ?>">
                    <a href="#" class="nav_date prev"><span>&lsaquo;</span></a>
                    <a href="#" class="link_dotted" id="changeDate"
                       data-lang="<?= I18n::$lang == 'kz' ? 'kk' : I18n::$lang ?>">
                        <span class="ls"><?= __('Выбрать дату начала') ?></span>
                    </a>
                    <a href="#" class="nav_date next"><span>&rsaquo;</span></a>
                </div>
                <div class="dish_list" data-edited="<?= $edited ?>"
                     data-url-remove="<?= URL::site('privatecoach/diet/remove_data') ?>"
                     data-url="<?= URL::site('privatecoach/diet/my_diet_ajax') ?>">
                </div>
                <!--       <table class="table_diet">
                    <thead>
                    <tr>
                        <th class="green col1 bootstrap-timepicker dish_timepicker">
                            <a href="#" class="i_white_del"></a>
                               <span> <a href="#" class="add-on" data-format="hh:mm" type="text">
                                    00:00
                                </a></span>
                                <input class="input_date_picker" style="display: none" data-format="hh:mm:ss"
                                       type="text">
                        </th>
                        <th class="col2"></th>
                        <th><?//=__('Калории')?></th>
                        <th><?//=__('Белки')?></th>
                        <th><?//=__('Углеводы')?></th>
                        <th><?//=__('Жиры')?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="col1">
                            <a href="#" class="i_blue_del"></a>

                            <div class="img"
                                 style="background-image: url('<?//= URL::site('images/ingredients.jpg') ?>');"></div>
                        </td>
                        <td>
                            <a href="#" class="link_dotted">Суп гороховый</a>

                            <div class="controls">
                                <input type="text">
                                <span>гр</span>
                            </div>
                        </td>
                        <td>67 ккал</td>
                        <td>5 гр</td>
                        <td>40 гр</td>
                        <td>30 гр</td>
                    </tr>
                    </tbody>
                </table>
                <div class="link_control">
                    <a href="#" class="link_dotted"><span class="i_blue_add"></span>Добавить продукт</a>
                </div>

                <div class="link_control top_hr">
                    <a href="#" class="link_dotted"><span class="i_green_add"></span>Добавить прием пищи</a>
                </div>-->

                <table class="table_result" data-url="<?=URL::site('privatecoach/diet/get_norms')?>">
                    <thead>
                    <tr>
                        <th class="col1">
                            <strong><?= __('Итого') ?></strong>
                        </th>
                        <th id="cal_amount"><span>0 </span><?= __('ккал') ?></th>
                        <th id="prt_amount"><span>0 </span><?= __('гр') ?></th>
                        <th id="crb_amount"><span>0 </span><?= __('гр') ?></th>
                        <th id="fat_amount"><span>0 </span><?= __('гр') ?></th>
                    </tr>
                    </thead>
                    <tbody class="hidden">
                    <tr>
                        <td class="col1">
                            <strong><?= __('Дневная норма') ?></strong>

                            <div class="select_day dropdown" data-bmr="<?= $bmr ?>" data-weight="<?= $user->weight ?>"
                                 data-mission="<?= $mission ?>" data-height="<?= $user->height ?>"
                                 data-age="<?= $user->get_age() ?>" data-sex="<?= $user->sex ?>">

                                <a class="dropdown-toggle norm_levels" data-toggle="dropdown"
                                   href="#"><span><?= Arr::get($norms, 'level', __('Уровень физической нагрузки')) ?></span><b
                                        class="caret"></b></a>

                                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                    <li><a href="#" class="load_level"
                                           data-level="0"> <?= __('Минимальный уровень') ?></a></li>
                                    <li><a href="#" class="load_level" data-level="1"> <?= __('Низкий уровень') ?></a>
                                    </li>
                                    <li><a href="#" class="load_level" data-level="2"> <?= __('Средний уровень') ?></a>
                                    </li>
                                    <li><a href="#" class="load_level" data-level="3"> <?= __('Высокий уровень') ?></a>
                                    </li>
                                    <li><a href="#" class="load_level"
                                           data-level="4"> <?= __('Очень высокий уровень') ?></a></li>
                                </ul>

                            </div>
                        </td>
                        <td class="norm_cal"><span><?=Arr::get($norms,'calories',0)?></span> <?= __('ккал') ?></td>
                        <td class="norm_prt"><span><?=Arr::get($norms,'protein',0)?></span> <?= __('гр') ?></td>
                        <td class="norm_crb"><span><?=Arr::get($norms,'carbohydrate',0)?></span> <?= __('гр') ?></td>
                        <td class="norm_fat"><span><?=Arr::get($norms,'fat',0)?></span> <?= __('гр') ?></td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <form name="norm" method="post" id="user_norm" data-url="<?= URL::site('privatecoach/diet/user_norms') ?>">
        <?= Form::hidden('date', date('Y-m-d'), array('id' => 'norm_date')) ?>
        <?= Form::hidden('calories', null, array('id' => 'norm_calories')) ?>
        <?= Form::hidden('protein', null, array('id' => 'norm_protein')) ?>
        <?= Form::hidden('carbohydrate', null, array('id' => 'norm_carbohydrate')) ?>
        <?= Form::hidden('fat', null, array('id' => 'norm_fat')) ?>
    </form>
    <? if (Helper::is_production()): ?>
        <div class="aside">
            <div class="banner">
                <? /*= Request::factory('confederation/partner/request/banner')->execute() */ ?>
            </div>
            <?= View::factory('social_networks') ?>
        </div>
    <? endif ?>
</div>

<div class="modal fade modal_dishes" id="myDish"
     tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal_inner">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h2><?= __('Добавление продукта в мой рацион') ?></h2>


        <form method="post" id="new_product" name="new_product"
              data-url="<?= URL::site(I18n::$lang . '/privatecoach/diet/add_dish') ?>">
            <div class="control_autocomplit">
                <div class="wrap_autocomplit">

                    <input id="product" placeholder="<?= __('Поиск продукта по названию') ?>" class="typeahead"
                           data-provide="typeahead" type="text" autocomplete="off"
                           data-toggle=popover
                           data-source="<?= URL::site('privatecoach/diet/source') ?>"
                           data-content="<?= __('Такого блюда нет в нашей базе') ?>.">


                </div>

                <?/*= __('Масса') */?><!--<input class="product_weight" name="weight">-->

                <div class="product hidden">
                    <?= __('Пищевая ценность (в 100г)') ?>
                    <ul>
                        <li>
                            <?= __('Каллории') ?>
                            <span id="product_calories"></span>
                            <?= __('ккал') ?>
                        </li>
                        <li>
                            <?= __('Белки') ?>
                            <span id="product_protein"></span>
                            <?= __('гр') ?>
                        </li>
                        <li>
                            <?= __('Углеводы') ?>
                            <span id="product_carbohydrate"></span>
                            <?= __('гр') ?>
                        </li>
                        <li>
                            <?= __('Жиры') ?>
                            <span id="product_fat"></span>
                            <?= __('гр') ?>
                        </li>
                    </ul>
                </div>
                <input id="ration_id" name="id" class="hidden" value="">
                <input id="product_id" name="dish" class="hidden">
                <a class="add_my_product btn color"><?= __('Добавить в мой рацион') ?></a>

                <p class="error"><?= Arr::get($error, 'ings') ?></p>

                <p class="error"><?= Arr::get($error, 'weight') ?></p>

            </div>

        </form>
    </div>
</div>

<!--<div class="modal fade modal_data"-->
<!--     tabindex="-1" role="dialog"-->
<!--     aria-labelledby="myModalLabel" aria-hidden="true">-->
<!--    <h2>--><?//= __('Данные пользователя') ?><!--</h2>-->
<!---->
<!--    <form method="post" id="user_data" name="user_data"-->
<!--          data-url="--><?//= URL::site(I18n::$lang . '/privatecoach/user/save_user_data') ?><!--">-->
<!---->
<!--        --><? //= Form::input('weight', $user->weight) ?>
<!--        --><? //= Form::input('height', $user->height) ?>
<!--        <input class="save_user_data btn color" value="--><?//= __('Сохранить') ?><!--">-->
<!---->
<!--        <p class="error">--><?//= Arr::get($error, 'ings') ?><!--</p>-->
<!---->
<!--        <p class="error">--><?//= Arr::get($error, 'weight') ?><!--</p>-->
<!---->
<!---->
<!--    </form>-->
<!--</div>-->