<div class="row_fluid page_diet clearfix bg-white">
<div class="breadcrumbs">
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?= URL::site(I18n::$lang . '/privatecoach') ?>" itemprop="url" class="link"><span itemprop="title"
        class="ls"><?= __('Личный тренер') ?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?= URL::site(I18n::$lang . '/privatecoach/diet/index') ?>" itemprop="url" class="link"><span
        itemprop="title" class="ls"><?= __('Рацион питания') ?></span></a>
    </div>
</div>
    <div class="row_section">
        <div class="section">
            <h1><?= __('Рацион питания') ?></h1>
            <form id="diet_filter">
                <div class="search_block100">
                    <div class="input_block">
                        <?= Form::input('search', array_key_exists('q', $query) ? $query['q'] : __('Поиск продукта'), array('class' => 'filter_search', 'id' => 'search', 'data-default' => __('Поиск продукта'))) ?>
                    </div>
                    <a href="#" id="submit" class='btn color'
                        data-url=<?= URL::site(I18n::$lang . '/privatecoach/diet/ajax_list') ?>>
                        <?= __('Найти') ?>
                    </a>
                </div>
                <div class="filter_search_group">
                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseOne"
                        class="accordion-toggle collapsed link_dotted">
                        <?= __('Расширенный поиск') ?>
                        <span class="caret">&nbsp;</span>
                    </a>
                    <div id="collapseOne" class="collapse">
                        <div class="inner">
                            <div class="advanced_search">
                                <div class="advanced_inner">
                                    <label for="amount"><?= __('Калории') . ' (' . __('ккал') . ')' ?></label>
                                    <div class="wrap_scroll">
                                        <div id="calories_range"></div>
                                    </div>
                                    <div class="value_min">
                                        <input type="text" name="min_cal" id="min_cal" value="0">
                                        <strong><?= __('ккал') ?></strong>
                                    </div>
                                    <div class="value_max">
                                        <input type="text" name="max_cal" id="max_cal" value="900">
                                        <strong><?= __('ккал') ?></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="advanced_search">
                                <div class="advanced_inner">
                                    <label for="amount"><?= __('Белки') . ' (' . __('гр') . ')' ?></label>
                                    <div class="wrap_scroll">
                                        <div id="protein_range"></div>
                                    </div>
                                    <div class="value_min">
                                        <input type="text" name="min_prot" id="min_prot" value="0">
                                        <strong><?= __('гр') ?></strong>
                                    </div>
                                    <div class="value_max">
                                        <input type="text" name="max_prot" id="max_prot" value="50">
                                        <strong><?= __('гр') ?></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="advanced_search">
                                <div class="advanced_inner">
                                    <label for="amount"><?= __('Углеводы') . ' (' . __('гр') . ')' ?></label>
                                    <div class="wrap_scroll">
                                        <div id="carbohydrate_range"></div>
                                    </div>
                                    <div class="value_min">
                                        <input type="text" name="min_carb" id="min_carb" value="0">
                                        <strong><?= __('гр') ?></strong>
                                    </div>
                                    <div class="value_max">
                                        <input type="text" name="max_carb" id="max_carb" value="100">
                                        <strong><?= __('гр') ?></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="advanced_search">
                                <div class="advanced_inner">
                                    <label for="amount"><?= __('Жиры') . ' (' . __('гр') . ')' ?></label>
                                    <div class="wrap_scroll">
                                        <div id="fat_range"></div>
                                    </div>
                                    <div class="value_min">
                                        <input type="text" name="min_fat" id="min_fat" value="0">
                                        <strong><?= __('гр') ?></strong>
                                    </div>
                                    <div class="value_max">
                                        <input type="text" name="max_fat" id="max_fat" value="100">
                                        <strong><?= __('гр') ?></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <input type="submit" class="btn grey_btn" value="<?=__('Сбросить')?>">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <ul class="unstyled inline clear nav_column diet_menu_list fast_search" data-url="<?= URL::site('privatecoach/diet/fast_search') ?>">
                <li class="column pull-left">
                    <div class="inner">
                        <a href="#" class="fast_search_block" data-category="first">
                            <img src="/images/first.jpg">
                            <strong class="title"><?=__('Первое')?></strong>
                        </a>
                    </div>
                </li>
                <li class="column pull-left program_back_img">
                    <div class="inner">
                        <a href="#" class="fast_search_block" data-category="second">
                            <img src="/images/second.jpg">
                            <strong class="title"><?=__('Второе')?></strong>
                        </a>
                    </div>
                </li>
                <li class="column pull-left program_back_img">
                    <div class="inner">
                        <a href="#" class="fast_search_block" data-category="other">
                            <img src="/images/other.jpg">
                            <strong class="title"><?=__('Другие блюда')?></strong>
                        </a>
                    </div>
                </li>
                <li class="column pull-left program_back_img">
                    <div class="inner">
                        <a href="#" class="fast_search_block" data-category="ingredients">
                            <img src="/images/ingredients.jpg">
                            <strong class="title"><?=__('Ингредиенты')?></strong>
                        </a>
                    </div>
                </li>
            </ul>
            <a href="<?= URL::site('privatecoach/diet/create_recipe') ?>" class="link link_blue_add"><span class="icon i_blue_add"></span><span class="ls"><?= __('Добавить свой рецепт') ?></span></a>
            <a href="<?= URL::site('privatecoach/diet/create_ingredient') ?>" class="link link_blue_add"><span class="icon i_blue_add"></span><span class="ls"><?= __('Добавить ингредиент') ?></span></a>
            <div class="section_list_menu">
                <div class="dish_list event_star_list">
                </div>
            </div>
        </div>
    </div>
    <div class="aside">
        <!--<div class="banner">-->
        <? /*= Request::factory('confederation/partner/request/banner')->execute() */ ?>
        <!--</div>-->
        <?= View::factory('social_networks') ?>
    </div>
</div>
<!--<div id="advance_options" style="display: none" data-no-rating-hint="--><?//= __('Оценка отстуствует') ?><!--"></div>-->
<? // if (!$user): ?>
<!--    --><? //= View::factory('privatecoach/user/promt')->bind('ulogin', $ulogin)->render() ?>
<? // endif ?>