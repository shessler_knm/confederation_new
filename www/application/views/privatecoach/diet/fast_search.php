<? if ($count): ?>
    <div class="heading_search_menu">
        <div class="h3"><?= $category_name ?></div>
    </div>

    <ul class="unstyled inline clear nav_column diet_menu_list">
        <? foreach ($collection as $row): ?>
            <li class="column pull-left program_back_img">
                <div class="inner">
                    <a href="#myDish" data-category="<?= get_class($row) ?>" data-id="<?= $row->id ?>"
                       data-toggle="modal" class="modal_anchor link">
                        <? if ($row->photo): ?>
                            <span class="img" style="background-image: url('<?= $row->photo_s->url_cropped_image() ?>')"></span>
                        <? else: ?>
                            <span class="img" style="background-image: url('/images/default_diet.jpg')"></span>
                        <? endif ?>

                        <span class="ls">
                                <?= $row->title ?>
                            </span>
                    </a>
                    <? /*= Text::limit_chars(strip_tags($row->description), 220) */ ?>
                </div>
            </li>
        <? endforeach ?>
    </ul>
    <?= $pagination ?>
<? else: ?>
    <div class="heading_search_menu">
        <div class="h3"><?= $category_name ?></div>
    </div>
    <?= __('По данному запросу ничего не найдено') ?>
<? endif ?>

<!--<div class="hr_top pagination_ajax text-center">
    <a href="#" class="link"><span class="ls"><?/*= __('Показать еще') */?></span></a>
</div>-->

<div class="modal fade modal_dishes" id="myDish"
     data-url="<?= URL::site(I18n::$lang . '/privatecoach/diet/ajax_dish') ?>" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <div class="modal-body" id="modal_dish">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->