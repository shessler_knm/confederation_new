<script type="text/javascript">
    P_ID =<?=$id?>;
    U_PRO = false;
    U_LEVEL = false;
</script>
<div class="add_program_section" ng-app="privatecoach" ng-controller="mainCtrl">
    <div class="header_add_program">
        <input type="text" class="title_program" ng-model="model.title_<?= I18n::$lang ?>"
               style="height: 40px; width: 500px">
        <ul class="unstyled inline pull-right">
            <li>
                <?= __('Длительность') ?>
                <input ng-model="model.duration" ng-options="i for i in durations"/>
                нед.
            </li>
        </ul>
    </div>
    <div class="wrap_table">
        <table class="table table_add_program">
            <tbody ng-repeat="day in model.days">
            <tr class="thead">
                <td class="col1">
                            <span class="select_wrap">
                                <div class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{ day.title }}</a>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                        <li ng-repeat="d in weekDays | orderBy:sortWeek:reverse"><a href="#"
                                                                                                    ng-click="changeDay(d,day,$event)">{{
                                                d.title }}</a></li>
                                    </ul>
                                </div>
                            </span>
                </td>
                <td colspan="2">
                    <div class="text-right">
                        <a href="#" class="link" ng-click="copyDay(day,$event)"><span class="ls">Копировать</span></a>
                        <a href="#" class="link" ng-click="removeDay($index,day,$event)"><span class="ls">Удалить</span></a>
                    </div>
                </td>
            </tr>
            <tr ng-repeat="exercise in day.exercises">
                <td class="col1">{{ exercise.title }}</td>
                <td>
                    <ul class="unstyled inline number_times_list">
                        <li ng-repeat="effort in exercise.efforts">
                            <div class="number_select">
                                <span class="num">{{ effort.repeat }}x</span>
                                <a href="#" class="btn_up" ng-click="upRepeat(effort,$event)"></a>
                                <a href="#" class="btn_dwn" ng-click="downRepeat(effort,$event)"></a>
                            </div>
                        </li>
                    </ul>
                </td>
                <td class="controls-row">
                    <a href="#" class="i_add" title="Добавить" ng-click="addEffort(exercise,$event)">Добавить</a>
                    <a href="#" class="i_minus" title="Убрать" ng-click="removeEffort(exercise,$event)">Убрать</a>
                    <a href="#" class="i_del" title="Удалить" ng-click="removeExercise(day,exercise,$event)">Удалить</a>
                </td>
            </tr>
            <tr>
                <td><a href="#" ng-click="addExercise(day,$event)">+ Добавить упражнение</a></td>
            </tr>
            </tbody>


        </table>

        <input type="button" value="Добавить день" class="btn color" ng-click="addDay()">
        <input type="button" value="Сохранить" class="btn color" ng-click="save()">
    </div>
    <div id="exerciseModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="exerciseModal"
         aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancel()">×</button>
            <h3 id="myModalLabel">Выберите упражнение</h3>
        </div>
        <div class="modal-body" ng-controller="exerciseCtrl">
            <form class="form-search">
                <input type="text" class="input-medium search-query" placeholder="Название упражнения" ng-model="query">
            </form>
            <ul ng-repeat="item in items | filter:query">
                <li><a href="#" ng-click="select(item,$event)">{{ item.name }}</a></li>
            </ul>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true" ng-click="cancel()">Отмена</button>
        </div>
    </div>


</div>
