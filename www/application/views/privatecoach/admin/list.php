<? //foreach ($model as $row)?>
<? // $row->{'title_'.I18n::$lang}?>
<? // $row->mission?>
<? // $row->{'text_'.I18n::$lang}?>
<? // $row->created?>

<h1><?= __('Редактирование программ тренировок') ?></h1>
<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <ul class="unstyled news_list interview_list">
                <? foreach ($model as $row): ?>
                    <li class="media hr_top">
                        <div class="pull-left">
                            <a href="<?= URL::site('privatecoach/gym/view/' . $row->id) ?>"><?= $row->photo_s->html_cropped_img(350) ?></a>
                        </div>
                        <div class="media-body">
                            <div class="media-heading">
                                <div class="h4"><a href="<?= URL::site('privatecoach/program/view/' . $row->id) ?>"
                                                   class="link"><span
                                            class="ls"><?= $row->title ?></span></a></div>
                                <em>
                                    <?= $row->mission->title ?>
                                </em>

                                <div class="media_text"><?= Text::limit_words(strip_tags($row->text), 70) ?></div>
                                <small><em>
                                        <span class="date"><?= Date::textdate($row->created, 'd m') ?></span>
                                        <span> <?= Helper::word_cases($row->views, __('подписчиков'), __('подписчик'), __('подписчика')) ?></span>
                                        <span> <?= Helper::word_cases(Comments::get_count('Program', $row->id), __('комментариев'), __('комментарий'), __('комментария')) ?></span>
                                    </em></small>
                                    <a href="<?= URL::site('privatecoach/admin/program/edit/'.$row->id) ?>"><?= __('Редактировать') ?></a>
                            </div>
                        </div>
                    </li>
                <? endforeach ?>
                <?= $pagination ?>
            </ul>
        </div>
    </div>
</div>
