<div class="row_fluid contacts_section clearfix bg-white">
<div class="breadcrumbs">
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?= URL::site(I18n::$lang . '/privatecoach') ?>" itemprop="url" class="link"><span itemprop="title"
        class="ls"><?= __('Личный тренер') ?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?= URL::site(I18n::$lang . '/privatecoach/feed/list/' . $id) ?>" itemprop="url" class="link"><span
        itemprop="title" class="ls"><?= __('Лента событий') ?></span></a>
    </div>
</div>
<div class="row_section">
<h1><?= __('Лента событий') ?></h1>
<? if ($feeds->count()): ?>
<ul class="unstyled event_privatecoach_list" data-url="<?= URL::site('privatecoach/feed/ajax_list') ?>">
    <? foreach ($feeds as $feed): ?>
    <li>
        <div class="pull-left img-circle">
            <a href="<?=URL::site(I18n::$lang.'/privatecoach/profile/'.$feed->user_id)?>" class="mask55">
                <?= Feed::feed_user_photo($feed->user_id) ?>
            </a>
        </div>
        <div class="media-body tape">
            <div class="media-heading">
                <?= Feed::feed_text($feed->user_id, $user, $feed->type, $feed->type == "update_album" && array_key_exists($feed->value_1, $array) ? $array[$feed->value_1]['number'] : null, $feed->value_1) ?>
            </div>
            <? if ($feed->type == "new_photo"): ?>
            <a href="<?= URL::site('privatecoach/profile/' . $feed->user_id) ?>">
            <?= Feed::feed_photo($feed->value_1) ?></a>
            <? elseif ($feed->type == "update_album"): ?>
            <? foreach ($array[$feed->value_1]['photo'] as $row): ?>
            <!--                            <a href="--><?//= URL::site('privatecoach/profile/album/' . $feed->user_id) ?><!--">-->
            <?= Feed::feed_photo($row) ?>
            <!--                            </a>-->
            <? endforeach ?>
            <? endif ?>
            <div class="muted dropdown">
                <small><em class="date"><?= Date::textdate($feed->date, 'd m') ?></em></small>
                <? if ($feed->user_id == $user->id): ?>
                <a class="link dropdown-toogle" data-toggle="dropdown"
                    href="#">
                    <small class="ls"><em><?= __('Поделиться') ?></em></small>
                </a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                    <li>
                        <div class="pluso"
                            data-title="<?= strip_tags(Feed::feed_text($feed->user_id, $user, $feed->type, $array[$feed->value_1]['number'], $feed->value_1)) ?>"
                            data-url="<?= URL::site(I18n::$lang.'/privatecoach', 'http') ?>"
                            data-background="#ebebeb"
                            data-options="small,square,line,horizontal,nocounter,theme=08"
                        data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir"></div>
                    </li>
                </ul>
                <? endif ?>
                <? if ($user->id == $id): ?>
                <a class="link dropdown-toogle delete_event" href="#"
                    data-url="<?= URL::site('privatecoach/feed/delete_event') ?>"
                    data-id="<?= $feed->id ?>">
                    <small class="ls"><em>
                    <?= __('Удалить') ?>
                    </em></small>
                </a>
                <? endif ?>
            </div>
        </div>
    </li>
    <? endforeach ?>
    <? if ($amount > 10): ?>
    <div class="pagination_ajax text-center">
        <a href="#" class="link more" data-counter="1" data-over="<?= $amount ?>" data-id="<?= $id ?>"><span
        class="ls"><?= __('Показать еще') ?></span></a>
    </div>
    <? endif ?>
    <!--        <div class="data-over hidden">--><?//= __('Больше нет записей') ?><!--</div>-->
</ul>
<? else: ?>
<?= __('Лента пока пуста') ?>
<? endif ?>
<div id="confirm" style="display: none"><?= __('Вы действительно хотите удалить запись?') ?></div>

</div>

</div>