<? if ($feeds): ?>
    <? foreach ($feeds as $feed): ?>
        <li>
            <? if ($feed->type=="update_album" || $feed->type=="close_mission"):?>
                <div class="pull-left img-circle">
                    <a href="#" class="mask55">
                        <?= Feed::feed_user_photo($feed->user_id) ?>
                    </a>
                </div>
            <?elseif($feed->type=="new_sub"):?>
                <div class="pull-left img-circle">
                    <a href="#" class="mask55">
                        <?= Feed::feed_user_photo($feed->value_1) ?>
                    </a>
                </div>
            <?endif ?>

            <div class="media-body">
                <div class="media-heading">
                    <?= Feed::feed_text($feed->user_id, $user, $feed->type, $feed->type == "update_album" && array_key_exists($feed->value_1,$user_albums)? $user_albums[$feed->value_1]['number'] : null, $feed->value_1) ?>
                </div>
                <? if ($feed->type == "new_photo"): ?>
                    <a href="<?= URL::site('privatecoach/profile/' . $feed->user_id) ?>">
                        <?= Feed::feed_photo($feed->value_1) ?></a>
                <? elseif ($feed->type == "update_album"): ?>
                    <? foreach ($user_albums[$feed->value_1]['photo'] as $row): ?>
                        <!--                            <a href="--><?//= URL::site('privatecoach/profile/album/' . $feed->user_id) ?><!--">-->
                        <?= Feed::feed_photo($row) ?>
                        <!--                            </a>-->
                    <? endforeach ?>
                <? endif ?>
                <div class="muted dropdown">
                    <small><em class="date"><?= Date::textdate($feed->date, 'd m') ?></em></small>
                    <? if ($feed->user_id == $user->id && $feed->type!='new_sub'): ?>
                        <a class="link dropdown-toogle" data-toggle="dropdown"
                           href="#">
                            <small class="ls"><em><?= __('Поделиться') ?></em></small>
                        </a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                            <li>
                                <div class="pluso" data-title="<?= strip_tags(Feed::feed_text($feed->user_id, $user, $feed->type, $array[$feed->value_1]['number'], $feed->value_1)) ?>"
                                     data-url="<?=URL::site('privatecoach/profile/118','http')?>"
                                     data-background="#ebebeb"
                                     data-options="small,square,line,horizontal,nocounter,theme=08"
                                     data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir"></div>
                            </li>
                        </ul>
                    <? endif ?>
                    <?if ($user->id == $id):?>
                    <a class="link dropdown-toogle delete_event" href="#"
                       data-url="<?= URL::site('privatecoach/feed/delete_event') ?>"
                       data-id="<?= $feed->id ?>">
                        <small class="ls"><em>
                                <?= __('Удалить') ?>
                            </em></small>
                    </a>
                    <?endif?>
                </div>

            </div>
        </li>
    <? endforeach ?>
    <div id="over" data-over="<?= $over ?>"></div>
<? endif ?>