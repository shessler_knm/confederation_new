<div class="row_fluid clearfix bg-white">
<div class="breadcrumbs">
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('Личный тренер')?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach/page/'.$model->sef)?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=$model->title ?></span></a>
    </div>
</div>
    <div class="row_section">
        <div class="section">
            <div class="page_section">
                <h1><?= $model->title ?></h1>
                <? if ($model->photo == true): ?>
                    <?= $model->photo_s->html_img(null,null,array('alt'=>$model->title)) ?>
                <? endif ?>
                <div class="text-left">
                    <?= $model->text ?>
                </div>
                <div class="bottom_info">
                    <div class="social_block" id="social_block"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="aside">
        <!-- <div class="banner">
            <img src="<?= URL::site('images/big_logo.png') ?>" alt="">
        </div> -->
        <?= View::factory('social_networks') ?>
    </div>
</div>