<div class="row_fluid clearfix bg-white">

<div class="breadcrumbs">
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('Личный тренер')?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach/article/list')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('Статьи') ?></span></a>
    </div>
</div>

    <h1><?= __('Статьи') ?></h1>
    <div class="row_section">
        <div class="section">

            <div class="filter_list_section">
                <div class="title_filter"><?=__('Тема статьи')?></div>
                <div class="hr">
                    <div class="data_filter">
                        <? foreach ($filters as $filter):?>
                        <a href="#" class="filter_articles" data-category="<?=$filter->id?>">
                            <span class="link"><span class="ls"><?=$filter->title?></span></span>
                        </a>
                        <? endforeach?>
                    </div>
                </div>
                <div class="title_filter pull-left">
                <?=__('Сортировать')?>
                <a href="#" id="ratingFilter"><span class="ls"><?=__('По рейтингу')?></span></a>
                </div>
                <!-- <div class="data_filter pull-left"> -->

<!--                    <a href="#" id="dateFilter"><span class="link"><span class="ls">--><?//=__('По дате')?><!--</span></span></a>-->
                <!-- </div> -->
                <div class="control_filter pull-right">
                    <a href="#" id="clearFilters" data-clear="<?=URL::site('')?>" class="link"><span class="ls"><?=__('Очистить фильтр')?></span></a>

                </div>
            </div>

            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade in active" id="year">
                    <ul class="unstyled event_list count event_star_list" data-counter="1" data-count="<?=$model->count()?>" data-url="<?=URL::site('privatecoach/article/ajax_list')?>">
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <div class="aside">
        <!-- <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div> -->

        <?= View::factory('social_networks') ?>

    </div>
</div>



