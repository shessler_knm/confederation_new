<?php
$i = 0;
?>
<? foreach ($model as $row): ?>
    <?php
    if ($i % 2 == 0) {
        echo "<li class='row-fluid'>";
    }
    ?>

    <div class="span6 item<?= $i % 2 + 1 ?> media event_back_img">
        <? if ($row->photo):?>
        <div class="figure">
            <a href="<?= $row->view_url() ?>">
            <?= $row->photo_s->html_img(null, 190) ?>
            </a>
        </div>
        <?endif?>
        <? if ($user): ?>
            <div class="star view" data-id="<?= $row->id ?>" data-read-only=1
                 data-score="<?= $row->score ?>"></div>
        <? else: ?>
            <div class="star view" data-id="<?= $row->id ?>" data-read-only=1 data-target="#myModal" data-toggle="modal"
                 data-score="<?= $row->score ?>"></div>
        <?endif ?>
        <div class="h5"><a href="<?= $row->view_url() ?>" class="link"><span
                    class="ls"><?= Text::limit_chars(strip_tags($row->title), 165) ?></span></a>
        </div>
        <div class="media_text"><?= Text::limit_chars(strip_tags($row->text), 200) ?></div>
        <small><em>
                <span class="date"><?= Date::textdate($row->date, 'd m') ?></span>
                <? foreach ($categories[$row->id] as $category): ?>
                    <span class="data"><?= $category ?></span>
                <? endforeach ?>
                <span class="ico_eye"><?= $row->views ?></span>
                <span class="ico_comment"><?= Comments::get_count('Article', $row->id) ?></span>
            </em></small>

        <span id="ya_share<?= $counter ?>"
              data-yashareL10n="<?= I18n::$lang == 'kz' ? 'kk' : I18n::$lang ?>"
              data-yashareType="none"
              data-yashareTitle="<?= $row->title ?>"
              data-yashareDescription="<?= strip_tags(Text::limit_words($row->text, 50)) ?>"
              data-yashareLink="<?= URL::site('privatecoach/article/view/' . $row->id) ?>"
              data-yashareImage="<?= $row->photo_s->url() ?>">
                                      </span>
        <? $counter++ ?>
        <!-- <div class="span6 hr"></div> -->
    </div>

    <?php
    if ($i % 2 == 1) {
        echo "</li>";
    }
    $i++;
    ?>
<? endforeach ?>

<?php
if ($i % 2 == 0) {
    echo "</li>";
}
?>
<div id="over" data-over="<?= $check ?>"></div>
<? if ($amount > 0 && $more_id==1): ?>
    <div class="pagination_ajax text-center">
        <a href="#" class="link" id="more"
           data-url="<?= URL::site('privatecoach/article/ajax_list') ?>"><span class="ls"><?=__('Показать еще')?></span></a>
    </div>
<? endif ?>
