<div class="row_fluid clearfix bg-white">

<div class="breadcrumbs">
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('Личный тренер')?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach/article/list')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('Статьи') ?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach/article/view/'.$model->id)?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=$model->title ?></span></a>
    </div>
</div>

    <div class="row_section">
        <div class="section">
            <div class="page_section">
                <h1><?= $model->title ?></h1>

                <div class="media">
                    <div class="figure_page pull-left"><?= $model->photo_s->html_img(300, null) ?></div>
                    <div class="media_text">
                        <?= $model->text ?>
                    </div>
                </div>

                <div class="bottom_info">
                    <? foreach ($categories as $category): ?>
                        <em class="muted"><?= $category ?></em>
                    <? endforeach ?>

                    <div class="social_block pull-right" id="social_block">
                        Yandex share?
                    </div>
                </div>
            </div>
            <div class="info_page_section">
                <em class="muted"><?= Date::textdate($model->date, 'd m') ?></em>
                <em class="muted"><?= Helper::word_cases(Comments::get_count('Article', $model->id), __('комментариев'), __('комментарий'), __('комментария')) ?></em>
                <em class="muted"><?= Helper::word_cases($model->views, __('просмотров'), __('просмотр'), __('просмотра')) ?></em>

                <div class="pull-right">
                    <em class="muted"><?= Helper::word_cases($voters, __('проголосовавших'), __('проголосовавший'), __('проголосовавших')) ?></em>
                    <? if ($user): ?>
                        <div class="star view" data-id="<?= $model->id ?>" data-score="<?= $score ?>"></div>
                    <? else: ?>
                        <div class="star view" data-id="<?= $model->id ?>" data-score="<?= $score ?>" data-read-only=1
                             data-target="#myModal" data-toggle="modal"></div>
                    <?endif ?>
                </div>
            </div>


            <h3 class="h3"><?= __('Похожие статьи') ?></h3>
            <ul class="unstyled event_list no_top_h5">
                <? foreach ($alike as $row): ?>
                    <li class="media">
                        <div class="pull-left">
                            <a href="<?= $row->view_url() ?>"><?= $row->photo_s->html_img(150, null) ?></a>
                        </div>
                        <div class="h5"><a href="<?= $row->view_url() ?>" class="link"><span
                                    class="ls"><?= Text::limit_chars(strip_tags($row->title), 165) ?></span></a></div>
                        <div class="media_text"><?= Text::limit_chars(strip_tags($row->text), 155) ?></div>
                        <div class="media_bottom">
                            <small><em class="muted"><?= Date::textdate($row->date, 'd m') ?></em></small>
                            <small>
                                <? foreach ($row->categories->find_all() as $category): ?>
                                    <em class="muted"><?= $category->title ?></em>
                                <? endforeach ?>
                            </small>
                            <div class="pull-right">
                                <? if ($user): ?>
                                    <div class="star view" data-id="<?= $row->id ?>" data-read-only=1
                                         data-score="<?= $array_of_score[$row->id] ?>"></div>
                                <? else: ?>
                                    <div class="star view" data-id="<?= $row->id ?>" data-read-only=1
                                         data-target="#myModal" data-toggle="modal"
                                         data-score="<?= $array_of_score[$row->id] ?>"></div>
                                <?endif ?>
                            </div>
                        </div>
                    </li>
                <? endforeach ?>
            </ul>
            <?= Comments::get_list($model->table_name(), $model->id) ?>
        </div>
    </div>

    <div class="aside">
        <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div>
        <?= View::factory('social_networks') ?>
    </div>
</div>

<div id="advance_options" style="display: none" data-url="<?= URL::site('privatecoach/article/score') ?>"
     data-no-rating-hint="<?= __('Оценка отстуствует') ?>" data-read-only="false"></div>
