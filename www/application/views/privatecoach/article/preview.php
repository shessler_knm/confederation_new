<ul class="unstyled event_list event_star_list">
    <?php
    $i = 0;
    ?>
    <? foreach ($model as $row): ?>
    <?php
    if ($i % 2 == 0) {
        echo "<li class='row-fluid'>";
    }
    ?>

    <div class="span6 item<?=$i % 2 + 1?> media event_back_img">
        <div class="figure">
            <a href="<?=$row->view_url()?>"><?= $row->photo_s->html_img(null, 190) ?></a>
        </div>
        <div class="star view" data-id="1" data-read-only="1" data-score="3" title="" style="width: 150px;">
           <!-- Вставить то что нужно, просто тупо дивку скопировала-->
        </div>
        <div class="media-body">
            <div class="h5"><a href="<?=$row->view_url()?>" class="link"><span
                    class="ls"><?=Text::limit_chars(strip_tags($row->title), 75)?></span></a></div>

            <div class="media_text"><?=Text::limit_chars(strip_tags($row->text), 155)?></div>
            <small><em>
                <span class="date"><?=Date::textdate($row->date, 'd m ')?></span>
                    <? foreach ($row->categories->find_all() as $row1): ?>
                        <span class="data"><?=$row1->title?></span>
                    <?endforeach?>
                <span class="ico_eye"><?= $row->views ?></span>
                <span class="ico_comment"><?= Comments::get_count('Article', $row->id) ?></span>
            </em></small>
        </div>
        <!-- <div class="span6 hr"></div> -->
    </div>
    <?php
    if ($i % 2 == 1) {
        echo "</li>";
    }
    $i++;
    ?>
    <? endforeach ?>
    <?php
    if ($i % 2 == 0) {
        echo "</li>";
    }
    ?>
</ul>