<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <?if ($check):?>
        <ul class="unstyled">
            <? foreach ($model as $row): ?>
                <li>
                    <?= $row->photo_s->html_img(400, null) ?>
                    <? if ($row->user_id == $user->id): ?>
                        <a href="#" class="delete_photo"
                           data-url="<?= URL::site('privatecoach/user/delete_photo/') ?>"
                           data-id="<?=$row->id?>">
                            <?= __('Удалить фотографию') ?>
                        </a>
                    <? endif ?>
                </li>
            <? endforeach ?>
        </ul>
        <?else:?>
            <?= __('В альбоме отсутствуют фотографии или они были удалены') ?>
        <?endif?>
    </div>
</div>
<div id="alert_message" style="display: none"><?=__('Вы действительно хотите удалить фотографию?')?></div>
