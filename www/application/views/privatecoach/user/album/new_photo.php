<!--<li><em>--><?//= Date::textdate($model->date, 'd m, Y') ?><!--</em>-->
<!--    --><?//= $model->photo_s->html_img(200, null) ?>
<!--    <b>--><?//= __('Вес') ?><!--:</b> --><?//= $model->weight ?><!----><?//= __('кг') ?>
<!--    <b>--><?//= __('Подкожный жир') ?><!--:</b> --><?//= $model->fat ?><!--%-->
<!--    <a href="#" class="btn color remove_result"-->
<!--       data-id="--><?//= $model->id ?><!--">--><?//= __('Удалить') ?><!--</a></li>-->

<li class="column" style="width: 700px;">
    <div class="date_result"><em><?= Date::textdate($model->date, 'd m, Y') ?></em></div>
    <div class="inner">
        <div class="img-circle figure">
            <?= $model->photo_s->html_img() ?>
        </div>
        <div class="info_result"><strong><?= __('Вес') ?> :</strong> <?= $model->weight.' '.__('кг') ?></div>
        <div class="info_result"><strong><?= __('Подкожный жир') ?> :</strong><?= $model->fat?$model->fat.' %':__('Не указан') ?>
        <div class="info_result"><strong><?= __('Рост') ?> :</strong><?= $model->height?$model->height.' '.__('см'):__('Не указан') ?>
        </div>
        <div class="control_result">
            <a href="#" class="btn color remove_result" data-id="21"><?= __('Удалить') ?></a>
        </div>
    </div>
</li>