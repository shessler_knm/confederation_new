<h1><?=__('Галерея результатов')?></h1>
<div class="count_programm"><em><?= __('Всего :program', array(':program' => Helper::word_cases($programs->count(),'программ','программа','программы'))) ?></em></div>
<div class="page_program gallery_page_program" data-content="<?= URL::site('privatecoach/program/ajax_gallery')?>" data-id="<?=$program_id?>">
    <div class="program_section">
        <div class="program_slider">
            <div class="program_inner">
                <div class="program_item_wrap" style="margin-left: 5px">
                    <? foreach ($programs as $program): ?>
                        <div class="program_item program_results"
                             data-id = "<?=$program->id?>"
                            >
                            <div class="pull-left">
                                <? if ($program->parent->photo): ?>
                                    <div class="img-circle">
                                        <?= $program->parent->photo_s->html_img(84) ?>
                                        <div class="mask84"></div>
                                    </div>
                                <? endif ?>
                            </div>
                            <div class="media-body">
                                <span class="num_programm">№<?= $i ?>.</span>
                                <?= $program->parent->title ?>
                                <small>
                                    <em><?= Helper::word_cases($program->count_user_logs($program->user), 'результатов', 'результат', 'результата') ?></em>
                                </small>
                            </div>
                        </div>
                        <? $i++ ?>
                    <? endforeach ?>
                </div>

                <a class="nav_slider prev">&larr;</a>
                <a class="nav_slider next">&rarr;</a>
            </div>
        </div>
    </div>
</div>
<div class="program_logs">

</div>