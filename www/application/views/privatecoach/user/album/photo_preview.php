<!--После-->
<? if ($array): ?>
    <div class="photo_result">


        <!--До-->
        <? if (array_key_exists('left', $array)): ?>
            <div class="pull-left data_result data_before">
                <div class="img-circle img_result">
                    <a href="<?= URL::site('privatecoach/user/gallery/'.$id) ?>" class="link">
                        <?= $array['left']['photo']->html_img(null, 400) ?>
                    </a>
                </div>
                <ul class="unstyled data_list">
                    <li class="date_result"><?= Date::textdate($array['left']['date'], 'd m Y') ?></li>
                    <li><?= __('Вес') ?><span class="muted">, <?= __('кг') ?></span> <span
                            class="item_data"><?= $array['left']['weight'] ?></span></li>
                    <? if ($array['left']['fat']): ?>
                        <li><?= __('Подкожный жир') ?><span class="muted">, %</span> <span
                                class="item_data">   <?= $array['left']['fat'] ?></span></li>
                    <? endif ?>
                </ul>
            </div>
        <? endif ?>


        <? if (array_key_exists('right', $array)): ?>
            <div class="pull-right data_result data_after">
                <!--            --><? // if ($model->count() > 1): ?>
                <div class="img-circle img_result">
                    <a href="<?= URL::site('privatecoach/user/gallery/'.$id) ?>" class="link">
                        <?= $array['right']['photo']->html_img(null, 400) ?>
                    </a>
                </div>
                <ul class="unstyled data_list">
                    <li class="date_result"><?= Date::textdate($array['right']['date'], 'd m Y') ?></li>
                    <li><?= __('Вес') ?><span class="muted">, <?= __('кг') ?></span> <span
                            class="item_data"><?= $array['right']['weight'] ?></span></li>
                    <? if ($array['right']['fat']): ?>
                        <li><?= __('Подкожный жир') ?><span class="muted">, %</span> <span
                                class="item_data"><?= $array['right']['fat'] ?></span></li>
                    <? endif ?>
                </ul>
            </div>
        <? endif ?>

        <!--Предыдущие три фотографии-->
        <? if ($preview && array_key_exists('center', $array)): ?>
            <div class="between">
                <div class="prev_photo">
                    <a href="<?= URL::site('privatecoach/user/gallery/'.$id) ?>" class="link">
                <span class="ls">
                    <?= __('Всего :photo', array(':photo' => Helper::word_cases($model->count(),'фотографий','фотография','фотографии'))) ?>
                </span>
                    </a>
                    <ul class="unstyled inline">
                        <? foreach ($array['center'] as $img): ?>
                            <li><?= $img->html_img(null, 50) ?></li>
                        <? endforeach ?>
                    </ul>
                </div>
            </div>
        <? endif ?>
    </div>
<? endif ?>


