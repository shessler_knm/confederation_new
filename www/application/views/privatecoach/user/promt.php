<div class="modal fade modal_entry" id="myModal" data-url="<?=URL::site('privatecoach/main/session')?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <div class="modal-body">
                <div class="h3"><?=__('Вход и').' '.HTML::anchor(URL::site(i18n::$lang.'/user/reg'),'<span class="ls">'.__('регистрация').'</span>',array('class'=>'link'))?></div>
                <form id="promt_form" action="<?=URL::site('privatecoach/user/login')?>" method="post" accept-charset="utf-8" class="form-horizontal form_ulogin form_usual">
                    <div class="control-group">
                        <label><?= __('Email') ?>:</label>
                        <div class="controls">
                            <?= Form::input('username', '', array("class" => "")); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <label><?= __('Пароль') ?>:</label>
                        <div class="controls">
                            <?= Form::input('password', '', array("class" => "", "type" => "password")); ?>
                            <p class="error"><?= arr::get($errors, 'password') ?></p>
                        </div>
                    </div>
                    <div class="control-group control_group_btn">
                        <div class="controls">
                            <?= HTML::anchor(URL::site('user/passremind'), __('Забыли пароль?'), array("class" => "link pull-right link_forgotpass")) ?>
                            <?= Form::submit('submit', __('Войти'), array('class' => 'btn btn_circle btn-primary pull-left')) ?>
                        </div>
                    </div>
                    <div class="control-group control_entry_social">
                        <div><?= __('Авторизация через соц.сети') ?>:</div>
                        <?= $ulogin ?>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->