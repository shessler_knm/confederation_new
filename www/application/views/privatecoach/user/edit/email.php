<div class="row_fluid profile_edit_section profile_edit_section1 clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <form action="<?=URL::site('privatecoach/user/email')?>" method="post" class="form-horizontal form_usual_privatecoach">
                <div class="control-group">
                    <label class="control-label"><?= __('Текущий адрес')?></label>
                    <div class="controls"><?=$model->email?></div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?= __('Новый адрес')?></label>
                    <div class="controls"><?= Form::input('new_address') ?></div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?= __('Пароль')?></label>
                    <div class="controls"><?= Form::input('password',null,array('type'=>'password')) ?></div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <?=$success?__('Адрес электронной почты успешно изменен'):null?>
                        <span class="error"> <?=$error?$error[0]:null?></span>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="hr_top btn18">
                            <?= Form::input('refresh', __('Сохранить'), array('type' => 'submit','class'=>'btn color')) ?>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>