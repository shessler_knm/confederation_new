<div class="row_fluid profile_edit_section profile_edit_section1 clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <form action="" method="post" enctype="multipart/form-data" encoding="multipart/form-data"
                  class="form-horizontal form_usual_privatecoach">
                <div class="control-group">
                    <label class="control-label"><?= __('Имя') ?></label>

                    <div class="controls">
                        <?= Form::input('firstname', $user->firstname) ?>
                        <span class="error"><?= __(Arr::get($validate_error, 'firstname')) ?></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?= __('Фамилия') ?></label>

                    <div class="controls">
                        <?= Form::input('lastname', $user->lastname) ?>
                        <span class="error"><?= __(Arr::get($validate_error, 'lastname')) ?></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?= __('Текущий вес') ?></label>

                    <div class="controls">
                        <?= Form::input('weight', $user->weight, array('class' => 'input-mini')) ?>
                        <?= __('кг') ?>
<!--                        <span class="error">--><?//= __(Arr::get($validate_error, 'weight')) ?><!--</span>-->
                    </div>
                </div>
                 <div class="control-group">
                    <label class="control-label"><?= __('Текущий рост') ?></label>

                    <div class="controls">
                        <?= Form::input('height', $user->height, array('class' => 'input-mini')) ?>
                        <?= __('см') ?>
<!--                        <span class="error">--><?//= __(Arr::get($validate_error, 'height')) ?><!--</span>-->
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?= __('Пол') ?></label>

                    <div class="controls custom_checkbox">
                        <label class="pull-left">
                            <?= Form::radio('sex', 1, $user->sex == 1 ? true : false, array('class' => 'styled_radio_button')) ?><?= __('мужской') ?>
                        </label>
                        <label class="pull-left">
                            <?= Form::radio('sex', 2, $user->sex == 2 ? true : false, array('class' => 'styled_radio_button')) ?><?= __('женский') ?>
                        </label>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?= __('Дата рождения') ?></label>

                    <div class="controls">
                        <?= Form::input('birthdate', $user->birthdate, array('style' => 'display:none', 'id' => 'advanced_form')) ?>


                        <div id="bday" data-url="<?=URL::site('privatecoach/user/days')?>" class="select_day s_d">
                            <?= Form::select('bday', $date['days'], Date::formatted_time($user->birthdate,'d'),array('class'=>'custom_select')) ?>
                        </div>
                        <div class="select_day s_m">
                            <?= Form::select('bmonth', $date['months'], Date::formatted_time($user->birthdate,'m'), array('id' => 'bmonth','class'=>'custom_select')) ?>
                        </div>
                        <div class="select_day s_y">
                            <?= Form::select('byear', $date['years'], Date::formatted_time($user->birthdate,'Y'), array('id' => 'byear','class'=>'custom_select')) ?>
                        </div>


                        <!--                        <a href="#" id="datepicker">-->
                        <?//=$model->birthdate!='0000-00-00'?Date::formatted_time($model->birthdate,'d.m.Y'):'01.01.1970' ?><!--</a>-->
                        <!--                        <div id="birthdate">-->
                        <?//=$model->birthdate!='0000-00-00'?Date::formatted_time($model->birthdate,'d.m.Y'):'01.01.1970' ?><!--</div>-->
                        <!--                        <a href="#" id="datepicker" data-lang="-->
                        <?//=I18n::$lang=='kz'?'kk':I18n::$lang?><!--" data-date-format="yyyy-mm-dd" data-date="-->
                        <?//=$model->birthdate?><!--">-->
                        <?//=$model->birthdate!='0000-00-00'?Date::textdate($model->birthdate,'d m, Y'):'01.01.1970' ?><!--</a>-->
                        <span class="error"><?= __(Arr::get($validate_error, 'birthdate')) ?></span>
                        <label class="custom-label inline">
                            <?= Form::checkbox('hiding', $user->hiding, null, array('class' => 'styled_radio_button')) ?>
                            <?= __('Не отображать в анкете') ?>
                        </label>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?= __('О себе') ?></label>

                    <div class="controls">
                        <?= Form::textarea('annotation', $user->annotation, array('maxlength' => 500, 'id' => 'annotation')) ?>
                        <span class="error"><?= __(Arr::get($validate_error, 'annotation')) ?></span>
                        <div id="text_counter"></div>
                    </div>

                </div>
                <div class="control-group btn18">
                   <div class="controls">
                    <?= Form::input('refresh', __('Сохранить'), array('type' => 'submit', 'class' => 'btn btn_circle color')) ?>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="aside text-center m0">
        <div style="display:none" id="alert" data-text="<?= __('Вы должны выбрать валидное изображение') ?>!"></div>
        <iframe style="display: none;" id="superframe" name="superframe"></iframe>
        <!-- Область предварительного просмотра -->
        <form id="frm" name="uploadForm" action="<?= URL::site('privatecoach/user/avatar') ?>" method="post"
              enctype="multipart/form-data"
              encoding="multipart/form-data" target="superframe" class="upload_ava_form">
            <div class="avatar_user">
                <div class="img-circle">
                    <img id="uploadPreview">
                    <? if ($user->photo_s->loaded()): ?>
                        <div id="currentImage">
                            <?= $user->photo_s->html_img(300, 300) ?><br>
                        </div>
                    <? else: ?>
                        <div id="cap">
                            <img src="">
                        </div>
                    <? endif ?>
                    <div class="mask200"></div>
                </div>
                <? if ($top_user->loaded()): ?>
                    <span class="icon_star"><?= $top_user->place ?></span>
                <? endif ?>
            </div>
            <input id="uploadImage" style="display: none" type="file" name="uploadFile">
        </form>
        <a href="#" id="changeAvatar" class="link"><span class="ls"><?= __('Сменить аватар') ?></span></a>
    </div>
</div>

