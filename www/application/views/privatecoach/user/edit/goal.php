<div class="row_fluid profile_edit_section profile_edit_goal_section clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <form action="<?= URL::site('privatecoach/user/goal') ?>" method="post" class="form-horizontal form_usual_privatecoach">
                <div class="control-group">
                    <label class="control-label"><?= __('Уровень подготовки') ?></label>

                    <div class="controls">
                        <?= Form::select('training', $training, $user->training, array('class' => 'custom_select')) ?>
                        <span class="error"><?= __(Arr::get($validate_error, 'training')) ?></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?= __('Цель тренеровок') ?>:</label>

                    <div
                        class="controls"><?= Form::select('mission', $mission_array, $mission->type ? $mission->type : NULL, array('class' => 'custom_select')) ?></div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?= __('Целевой вес') ?></label>

                    <div class="controls">
                        <?= Form::input('target_weight', $mission->weight, array('class' => 'input-mini')) ?>
                        <?= __('кг') ?>
                        <span class="error"><?= __(Arr::get($validate_error, 'target_weight')) ?></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?= __('Целевой % подкожного жира') ?></label>

                    <div class="controls">
                        <?= Form::input('target_fat', $mission->fat, array('class' => 'input-mini')) ?>
                        %
                        <span class="error"><?= __(Arr::get($validate_error, 'target_fat')) ?></span>
                    </div>
                </div>
                <span class="error"><?= $error ?>.</span></br>
                <span class="error"><?= $hint ?></span>
                <div class="hr_top btn18">
                    <?= Form::input('refresh', __('Сохранить'), array('type' => 'submit', 'class' => 'btn color')) ?>
                    <? if (!$program->loaded()): ?>
                    <?= Form::input('program_selection', __('Подобрать программу тренировок'), array('type' => 'submit', 'class' => 'btn color')) ?>
                    <? endif ?>
                </div>
            </form>
        </div>
    </div>
</div>