<? if ($active_program->loaded()): ?>
    <div class="row_fluid profile_edit_section profile_edit_calc_section clearfix bg-white">
        <div class="row_section">

            <div class="title_calc">
                <span class="h3"><a href="#" class="link"><span
                            class="ls"><?= $active_program->title ?></span></a></span>
                    <span class="date_calc">
                        <?= Date::textdate($active_program->start_date, 'd m') ?>
                        -
                        <?= Date::textdate($dt->format('Y-m-d'), 'd m') ?>
                    </span>
            </div>

            <div class="h5"><?= __('Результат по сравнению с датой начала программы') ?>:</div>
            <? if ($mission->type == 'MUSCLE'): ?>
                <div class="result_calc"><span><?= __('Увеличение веса') ?></span>
                    <strong>+<?= $difference['weight'] ?> <?= __('кг') ?></strong></div>
            <? else: ?>
                <div class="result_calc"><span><?= __('Уменьшение веса') ?></span>
                    <strong>-<?= $difference['weight'] ?> <?= __('кг') ?></strong></div>
                <div class="result_calc hr_bottom"><span><?= __('Уменьшение подкожного жира') ?></span>
                    <strong>-<?= $difference['fat'] ?> %</strong></div>
            <?endif ?>


            <div class="photo_result" data-remove="<?= URL::site('privatecoach/user/remove_result') ?>"
                 data-refresh="<?= URL::site('privatecoach/user/refresh_result') ?>"
                 data-error="<?= __('Произошла ошибка') ?>">
                <h3><?= __('Результаты') ?></h3>
                <ul class="unstyled photo_list_result">
                    <? foreach ($progress as $result): ?>
                        <li class="column">
                            <div class="date_result"><em><?= Date::textdate($result->date, 'd m, Y') ?></em></div>
                            <div class="inner">
                                <div class="img-circle figure">
                                    <?= $result->photo_s->html_img() ?>
                                </div>
                                <div class="info_result">
                                    <strong><?= __('Вес') ?>:</strong>
                                    <?= $result->weight.' '.__('кг') ?>
                                </div>
                                <div class="info_result">
                                    <strong><?= __('Подкожный жир') ?>:</strong>
                                    <?= $result->fat ? $result->fat . ' %' : __('Не указан') ?>
                                </div>
                                <div class="info_result">
                                    <strong><?= __('Рост') ?>:</strong>
                                    <?= $result->height ? $result->height . ' ' . __('см') : __('Не указан') ?>
                                </div>
                                    <div class="control_result">
                                        <a href="#" class="btn color remove_result"
                                           data-id="<?= $result->id ?>"><?= __('Удалить') ?></a>
                                    </div>
                                </div>
                        </li>
                    <? endforeach ?>
                    <?= Form::hidden('ajax', null, array('id' => 'before_data')) ?>
                    <li class="column add_result_column">
                        <div class="date_result">&nbsp;</div>
                        <div class="inner">
                            <a href="#" id="add_result" title="<?= __('Добавить еще') ?>">
                                <span class="link"><span class="ls"><?= __('Добавить еще') ?></span></span>
                            </a>
                        </div>
                    </li>
                    <li class="clear">&nbsp;</li>
                </ul>
                <!--                <div class="control_list_result">-->
                <!--                    <a href="#" data-url="-->
                <?//= URL::site('privatecoach/user/save_result') ?><!--" class='btn color'-->
                <!--                       id='save_results'>--><?//= __('Сохранить') ?><!--</a>-->
                <!--                </div>-->

            </div>
        </div>
    </div>


    <div id="popup">
        <div class="inner">
            <h3><?= __('Добавить данные') ?></h3>

            <form method="post" class="form-horizontal" id="userImage" name="uploadForm"
                  data-result="<?= URL::site('privatecoach/user/add_result?album=' . $album_id) ?>"
                  enctype="multipart/form-data" encoding="multipart/form-data">
                <!--                --><? // if (Helper::is_production()): ?>
                <div class="control-group">
                    <a href="<?= URL::site(I18n::$lang . '/confederation/page/-instruktsiya-po-izmereniu-podkozhnogo-zhira') ?>"><?= __('Инструкция') ?></a>
                </div>
                <!--                --><? // endif ?>
                <div class="control-group">
                    <div class="control-label"><?= __('Вес') ?></div>

                    <div class="controls">
                        <?= Form::input('weight', $model->weight, array('class' => 'input-mini')) ?><?= __('кг') ?>
                    </div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?= __('Рост') ?></div>

                    <div class="controls">
                        <?= Form::input('height', $model->height, array('class' => 'input-mini')) ?><?= __('см') ?>
                    </div>
                </div>

                <div class="accordion" id="accordion2">
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle link_date link_dotted" data-toggle="collapse"
                               data-parent="#accordion2" href="#collapseOne">
                                <span class="ls"><?= __('Подкожный жир') ?></span>
                            </a>
                        </div>
                        <div id="collapseOne" class="accordion-body collapse">
                            <div class="accordion-inner">
                                <div class="control-group">
                                    <div class="control-label"><a href="#" class="hint link_dotted"
                                                                  data-img='<?= URL::site('images/min_logo.png', 'http') ?>'
                                                                  data-placement="bottom"
                                                                  data-content="<?= $data_hint[0] ?>"><span
                                                class="ls"><?= __('Складки на груди') ?></span></a></div>

                                    <div class="controls">
                                        <?= form::input('chest', Arr::get($_POST, 'chest', $model->chest), array('class' => 'input-mini')) ?><?= __('мм') ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="control-label"><a href="#" class="hint link_dotted"
                                                                  data-img='<?= URL::site('images/banner.png', 'http') ?>'
                                                                  data-placement="bottom"
                                                                  data-content="<?= $data_hint[1] ?>"><span
                                                class="ls"><?= __('Складки на животе') ?></span></a></div>

                                    <div class="controls">
                                        <?= form::input('stomach', Arr::get($_POST, 'stomach', $model->stomach), array('class' => 'input-mini')) ?><?= __('мм') ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="control-label"><a href="#" class="hint link_dotted"
                                                                  data-img='<?= URL::site('images/min_logo.png', 'http') ?>'
                                                                  data-placement="bottom"
                                                                  data-content="<?= $data_hint[2] ?>"><span
                                                class="ls"><?= __('Складки на бёдрах') ?></span></a></div>

                                    <div class="controls">
                                        <?= form::input('haunch', Arr::get($_POST, 'haunch', $model->haunch), array('class' => 'input-mini')) ?><?= __('мм') ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="control-label"><a href="#" class="hint link_dotted"
                                                                  data-img='<?= URL::site('images/min_logo.png', 'http') ?>'
                                                                  data-placement="bottom"
                                                                  data-content="<?= $data_hint[3] ?>"><span
                                                class="ls"><?= __('Складки на трицепсе') ?></span></a></div>

                                    <div class="controls">
                                        <?= form::input('triceps', Arr::get($_POST, 'triceps', $model->triceps), array('class' => 'input-mini')) ?><?= __('мм') ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="control-label"><a href="#" class="hint link_dotted"
                                                                  data-img='<?= URL::site('images/min_logo.png', 'http') ?>'
                                                                  data-placement="bottom"
                                                                  data-content="<?= $data_hint[4] ?>"><span
                                                class="ls"><?= __('Складки под лопаткой') ?></span></a></div>

                                    <div class="controls">
                                        <?= form::input('blade', Arr::get($_POST, 'blade', $model->blade), array('class' => 'input-mini')) ?><?= __('мм') ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="control-label"><a href="#" class="hint link_dotted"
                                                                  data-img='<?= URL::site('images/min_logo.png', 'http') ?>'
                                                                  data-placement="bottom"
                                                                  data-content="<?= $data_hint[5] ?>"><span
                                                class="ls"><?= __('Складки над подвздошной костью') ?></span></a></div>

                                    <div class="controls">
                                        <?= form::input('ilium', Arr::get($_POST, 'ilium', $model->ilium), array('class' => 'input-mini')) ?><?= __('мм') ?>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="control-label"><a href="#" class="hint link_dotted"
                                                                  data-img='<?= URL::site('images/min_logo.png', 'http') ?>'
                                                                  data-placement="bottom"
                                                                  data-content="<?= $data_hint[6] ?>"><span
                                                class="ls"><?= __('Складка подмышкой') ?></span></a></div>

                                    <div class="controls">
                                        <?= form::input('armpit', Arr::get($_POST, 'armpit', $model->armpit), array('class' => 'input-mini')) ?><?= __('мм') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?= Form::hidden('fat', null) ?>
                <?= Form::hidden('file', null) ?>
                <!--Яна, если ищешь картинки, которые грузятся примерно куда-то сюда, то тебе нужны два файла: ba.album.js(93 строка) и edit_program.js(84 строка)-->
                <div class="hr_top">
                    <a href="#" class="link_dotted" id="add_photo"><span
                            class="ls"><?= __('Выбрать фотографию') ?></span></a>
                    <a href="#" style="display: none" id="edit_photo"></a>
                    <a href="#" class="link_dotted link_date pull-right"
                       data-lang="<?= I18n::$lang == 'kz' ? 'kk' : I18n::$lang ?>" id="changeDate">
                        <span class="ls"><?= __('Выбрать дату') ?></span>
                    </a>
                </div>
                <div class="progress hidden">
                    <div class="bar"></div>
                    <div class="percent">0%</div>
                </div>

                <div id="status"></div>

                <?= Form::hidden('date', date('Y-m-d'), array('id' => 'advanced_form', 'data-year' => Date::formatted_time($active_program->start_date, 'Y'), 'data-month' => Date::formatted_time($active_program->start_date, 'm'), 'data-day' => Date::formatted_time($active_program->start_date, 'd'))) ?>
                <div class="text-center">
                    <!--                    <input id="submit" style="display: none" type="submit" name="uploadFile">-->
                    <?= Form::button('add', __('Добавить'), array('class' => 'btn color', 'id' => 'submit_result', 'disabled' => 'true')) ?>
                </div>
                <!--                <input type="file" id="myfile" name="myfile" style="display: none"><br>-->

            </form>
            <form method="post" name="Image" id="album_picture"
                  action="<?= URL::site('privatecoach/user/load_photo') ?>" enctype="multipart/form-data"
                  encoding="multipart/form-data">
                <input type="file" id="myfile" name="myfile" style="display: none"><br>
                <input type="submit" id="upload" value="Upload File to Server" style="display: none">
            </form>
            <?= Form::hidden('age', $age) ?>
            <?= Form::hidden('sex', $user->sex) ?>
        </div>
    </div>

    <div id="backgroundPopup"></div>
<? else: ?>
    <?= __('Для заполнения альбома, выберите программу тренировок') ?>
<?endif ?>
