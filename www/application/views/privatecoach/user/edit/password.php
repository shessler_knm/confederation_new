<div class="row_fluid profile_edit_section profile_edit_pass_section clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <form action="<?= URL::site('privatecoach/user/password') ?>" method="post"
                  class="form-horizontal form_usual_privatecoach">
                <div class="control-group">
                    <label class="control-label"><?= __('Текущий пароль') ?></label>
                    <div class="controls"><?= Form::input('current_password', null, array('type' => 'password')) ?>
                        <span class="error"><?= Arr::get($error, 'current_password') ?></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?= __('Новый пароль') ?></label>
                    <div class="controls"><?= Form::input('new_password', null, array('type' => 'password')) ?>
                        <span class="error"> <?= Arr::get($error, 'new_password') ?></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?= __('Повторите пароль') ?></label>
                    <div class="controls"><?= Form::input('repeat_password', null, array('type' => 'password')) ?>
                        <span class="error"><?= Arr::get($error, 'repeat_password') ?></span>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <?= $success ? __('Пароль успешно изменен') : null ?>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="hr_top btn18">
                            <?= Form::input('refresh', __('Сохранить'), array('type' => 'submit','class'=>'btn color')) ?>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>