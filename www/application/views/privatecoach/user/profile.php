<div class="row_fluid profile_user clearfix bg-white" data-own-profile="<?= $own_profile ?>">

    <!--Хлебные крошки. Микроразметку не трогать (itemscope itemtype)-->
<div class="breadcrumbs">
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?= URL::site(I18n::$lang . '/privatecoach') ?>" itemprop="url" class="link"><span itemprop="title"
        class="ls"><?= __('Личный тренер') ?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?= URL::site(I18n::$lang . '/privatecoach/profile/' . $current_user->id) ?>" itemprop="url"
            class="link"><span itemprop="title" class="ls"><?= $current_user->get_full_name() ?></span></a>
    </div>
</div>

        <div class="row_section">
            <div class="section">
                <h1><?= $current_user->get_full_name() ?></h1>
                <? if ($current_user->annotation): ?>
                <div class="user_info"><?= $current_user->annotation ?></div>
                <? endif ?>
                <? if ($log_count != null): ?>
                <div class="h3"><?= __('До и после') ?></div>
                <?= Request::factory(I18n::$lang . '/privatecoach/profile/photo_preview/' . $user_id . '?preview=true')->execute() ?>
                <? endif ?>
                <!--График-->
                <? if ($log_count >= 10): ?>
                <div class="h3"><a class="link"><span class="ls appChart"><?= __('Достижения') ?></span></a></div>
                <div class="h3"><a class="link"><span class="ls appChart"> </span></a></div>
                <div id="panel" data-user_id="<?= $user_id ?>"></div>
                <? endif ?>
                <!-- Программа-->
                <? if ($program->loaded()): ?>
                <div class="h3">
                    <a href="<?= URL::site('privatecoach/program/schedule') ?>" class="link">
                        <span class="ls program"><?= __('Программа тренировок') ?></span>
                    </a>
                </div>
                <div class="info_program">
                    <a href="<?= URL::site('privatecoach/program/view/' . $program->parent->id) ?>" class="link"><span
                    class="ls"><?= $program->parent->title ?></span></a>,
                    <?= Date::formatted_time($program->start_date, 'd.m.Y') ?>
                    - <?= Date::formatted_time($program->end_date($week_days), 'd.m.Y') ?>.
                    <?= __('Цель') ?>: <?= $program->mission->title ?>.
                </div>
                <div class="page_program" ng-app="privatecoach" ng-controller="scheduleCtrl">
                    <div lenta-calendar year="dtLenta.year" month="dtLenta.month" day="dtLenta.day"
                        programs="programs"
                        logs="logs" readonly="readonly">
                    </div>
                </div>
                <? if ($user): ?>
                <? if ($user->id == $current_user->id): ?>
                <div class="link_my_program">
                    <a href="<?= URL::site(i18n::$lang . '/privatecoach/user/album') ?>" class="link"><span
                    class="ls"><?= __('Добавить результат') ?></span></a>
                    <a href="<?= URL::site(i18n::$lang . '/privatecoach/program/edit/'.$program->id) ?>"
                        class="link"><span
                        class="ls"><?= __('Изменить') ?></span></a>
                        <a href="<?= URL::site(I18n::$lang . '/privatecoach/program/delete') ?>" class="link"><span
                        class="ls"><?= __('Удалить') ?></span></a>
                        <a href="<?= URL::site(i18n::$lang . '/privatecoach/program/pdf/' . $program->id) ?>"
                            target="_blank"><span class="ico_print">&nbsp;</span><span class="link"><span
                            class="ls">
                        <?= __('Распечатать') ?></span></span></a>
                    </div>
                    <? endif ?>
                    <? endif ?>
                    <? endif ?>
                    <? if ($user): ?>
                    <div class="hr_bottom">
                        <!-- Лента событий-->
                        <div class="h3">
                            <a href="<?= URL::site('privatecoach/feed/list/' . $current_user->id) ?>" class="link">
                                <span class="ls"><?= __('Лента событий') ?></span>
                            </a>
                        </div>
                        <?= Request::factory(I18n::$lang . '/privatecoach/feed/preview/' . $user_id)->execute() ?>
                    </div>
                    <? endif ?>
                </div>
            </div>
            <div class="aside">
                <div class="avatar_user">
                    <div class="img-circle">
                        <div class="mask200"></div>
                        <?= $current_user->photo_s->loaded() ? $current_user->photo_s->html_img(400, 400) : '' ?>
                    </div>
                    <? if ($top_user->loaded()): ?>
                    <span class="icon_star"><?= $top_user->place ?></span>
                    <? endif ?>
                </div>
                <!--Атритбуты пользователя-->
                <ul class="unstyled data_list">
                    <li><?= __('Пол') ?> <span class="item_data"><? if ($current_user->sex == 1): ?>
                        <?= __('муж.') ?>
                        <? elseif ($current_user->sex == 2): ?>
                        <?= __('жен.') ?>
                        <?endif ?>
                    </span>
                </li>
                <? if (!$current_user->hiding): ?>
                <li>
                    <?= __('Возраст') ?>
                    <span class="muted">, <?= __('лет') ?></span>
                    <span class="item_data"><?= $current_user->get_age($current_user->birthdate) ?></span>
                </li>
                <? endif ?>
                <li>
                    <?= __('Рост') ?>
                    <span class="muted">, <?= __('см') ?></span>
                    <span class="item_data"><?= $user->height ?></span>
                </li>
                <li>
                    <?= __('Вес') ?>
                    <span class="muted">, <?= __('кг') ?></span>
                    <span class="item_data"><?= $user->weight ?></span>
                </li>
                <!--            <li>-->
                <!--                --><? //= __('Рост') ?>
                <!--                <span class="muted">, --><?//= __('см') ?><!--</span>-->
                <!--                <span class="item_data">--><?//= $user->height ?><!--</span>-->
                <!--            </li>-->
                <? if ($user->fat): ?>
                <li>
                    <?= __('Подкожный жир') ?>
                    <span class="muted">, %</span>
                    <span class="item_data"><?= $user->fat ?></span>
                </li>
                <? endif ?>
            </ul>
            <div class="my_intent">
                <strong>
                <?= __('Цель') ?>:
                <? if ($user_mission->status != 1): ?>
                <?= $user_mission->type == 'MUSCLE' ? __('набор мышечной массы') : __('похудение') ?>
                </strong>
            </div>
            <ul class="unstyled data_list">
                <li>
                    <?= __('Вес') ?>
                    <span class="muted">, <?= __('кг') ?></span>
                    <span class="item_data"><?= $user_mission->weight ?></span>
                </li>
                <li>
                    <?= __('Подкожный жир') ?>
                    <span class="muted">, %</span>
                    <span class="item_data"><?= $user_mission->fat ?></span>
                </li>
            </ul>
            <? else: ?>
            <? if ($user): ?>)
            <? if ($user->id == $current_user->id): ?>
            <?= __('Цель тренировок нуждается в') ?>
            <a href="<?= URL::site('privatecoach/user/goal') ?>"><?= __('редактировании') ?></a>
            <? else: ?>
            <?= $current_user->get_full_name() ?> <?= __('пока не создал цель') ?>
            <?endif ?>
            <? endif ?>
            <?
            endif ?>
            <!------------------------------------------------------------------------------------------------------>
            <!-- Кнопка "подписаться" или "отписаться" -->
            <div class="section_profile_btn">
                <? if ($user): ?>
                <? if ($current_user->id != $user->id): ?>
                <? if (!$subscribe->find_subs($user->id, $current_user->id)->loaded()): ?>
                <a href="#" class="subscribe btn color" data-id="<?= $current_user->id ?>"
                    data-url="<?= URL::site('privatecoach/subscribe/add', 'http') ?>"
                data-change="0"><?= __('Подписаться') ?></a>
                <? else: ?>
                <a href="#" class="subscribe btn color" data-id="<?= $current_user->id ?>"
                    data-url="<?= URL::site('privatecoach/subscribe/delete', 'http') ?>"
                data-change="1"><?= __('Отписаться') ?></a>
                <?endif ?>
                <? else: ?>
                <a href="<?= URL::site('privatecoach/user/form') ?>"
                class="btn color"><?= __('Изменить данные') ?></a>
                <? endif ?>
                <? else: ?>
                <a class="promt subscribe btn color" data-action="subscribe" data-id="<?= $current_user->id ?>"
                data-target="#myModal" data-toggle="modal"><?= __('Подписаться') ?></a>
                <? endif ?>
            </div>
            <!--Счетчик просмотров--> <!-- Вьюшка: view_counter -->
            <?= View::factory('view_counter')->set('counter', $current_user->views) ?>
            <!-- Подписки--> <!-- Вьюшка: subscribe/subs -->
            <?= Request::factory(I18n::$lang . '/privatecoach/subscribe/subs/' . $user_id)->execute() ?>
            <!-- Подписчики--> <!-- Вьюшка: subscribe/subers -->
            <?= Request::factory(I18n::$lang . '/privatecoach/subscribe/subers/' . $user_id)->execute() ?>
            <!------------------------------------------------------------------------------------------------------>
            <!-- Корм для JS-->
            <?= Form::input('sub', __('Подписаться'), array('style' => 'display:none', 'id' => 'sub')); ?>
            <!-- Корм для JS-->
            <?= Form::input('unsub', __('Отписаться'), array('style' => 'display:none', 'id' => 'unsub')); ?>
            <!------------------------------------------------------------------------------------------------------>
        </div>
    </div>
    <input style="display:none" id="program_user_id" value="<?= $program->id ?>">
    <div id="appChart" data-weight="<?= __('вес') ?>, <?= __('кг') ?>" data-fat="<?= __('жир') ?>, %"
    style="display: none"></div>
    <div id="own_program" data-own-profile="<?= $own_profile ?>" style="display: none"></div>
    <div id="hint" data-hint="<?= __('Нажмите, чтобы посмотреть описание') ?>" style="display: none"></div>
    <?= View::factory('privatecoach/program/exercise_promt')->render() ?>
    <? if (!$user): ?>
    <?= View::factory('privatecoach/user/promt')->bind('ulogin', $ulogin)->render() ?>
    <? endif ?>