<div class="hr_top">

    <h4>
        <? if ($counter): ?>
            <?= Helper::word_cases($counter, 'подписок', 'подписка', 'подписки') ?>
        <? else: ?>
            <?= __('Нет подписок') ?>
        <?endif ?>
    </h4>

    <ul class="unstyled">
        <? foreach ($subs as $row): ?>
            <li><a href="<?= URL::site('privatecoach/profile/' . $row->target_id) ?>" class="link"><span
                        class="ls"><?= $row->get_target_name() ?></span></a></li>
        <? endforeach ?>
        <? if ($more): ?>
            <a href="<?= URL::site('privatecoach/subscribe/list/'.$id) ?>"
               class="link_dotted"><?= __('еще') ?> <?= $counter - 10 ?></a>
        <?endif ?>
    </ul>

</div>