<li class="media">
    <? if ($model->photo_s->loaded()): ?>
        <div class="pull-left img-circle">
            <?= $model->photo_s->html_img()?>
            <div class="mask84"></div>
        </div>
    <? endif ?>

    <div class="media-body">
        <div class="media-heading">
            <a href="<?=URL::site('privatecoach/profile/'.$model->id)?>" class="link top_user_name"><strong class="ls"><?= $model->get_full_name() ?></strong></a>
        </div>
    </div>
</li>