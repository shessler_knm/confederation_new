
<div class="row_fluid clearfix bg-white">
<div class="breadcrumbs">
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?= URL::site(I18n::$lang . '/privatecoach') ?>" itemprop="url" class="link"><span itemprop="title"
        class="ls"><?= __('Личный тренер') ?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?= URL::site(I18n::$lang . '/privatecoach/subscribe/list') ?>" itemprop="url" class="link"><span
        itemprop="title" class="ls"><?= __('Подписки/Подписчики') ?></span></a>
    </div>
</div>
    <div class="row_section">
        <div class="section">
            <h1><?= __('Список участников') ?></h1>
            <ul class="unstyled inline nav_tabs" id="Subscribes">
                <li class="active"><a href="#followers" class="link_dotted"><?= __('Подписки') ?></a></li>
                <li><a href="#following" class="link_dotted"><?= __('Подписчики') ?></a></li>
                <li><a href="#requests" class="link_dotted"><?= __('Заявки') ?>(<?= $request_count ? '+' : null ?><span
                class="requests_counter"><?= $requests_count ?></span>)</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="followers">
                    <ul class="unstyled user_subscribe_list">
                        <? foreach ($subs as $row): ?>
                        <li class="media">
                            <? if ($row->photo_s->loaded()): ?>
                            <div class="pull-left img-circle">
                                <?= $row->photo_s->html_img() ?>
                                <div class="mask84"></div>
                            </div>
                            <? endif ?>
                            <div class="media-body">
                                <div class="media-heading">
                                    <a href="<?= URL::site('privatecoach/profile/' . $row->id) ?>"
                                        class="link top_user_name"><strong
                                    class="ls"><?= $row->get_full_name() ?></strong></a>
                                </div>
                                <a href="#" class="sub_link sub_delete"
                                    data-url="<?= URL::site('privatecoach/subscribe/delete') ?>"
                                data-id="<?= $row->id ?>"><?= __('Отписаться') ?></a>
                            </div>
                        </li>
                        <? endforeach ?>
                    </ul>
                </div>
                <div class="tab-pane" id="following">
                    <ul class="unstyled user_subscribe_list">
                        <? foreach ($followers as $row): ?>
                        <li class="media">
                            <? if ($row->photo_s->loaded()): ?>
                            <div class="pull-left img-circle">
                                <?= $row->photo_s->html_img() ?>
                                <div class="mask84"></div>
                            </div>
                            <? endif ?>
                            <div class="media-body">
                                <div class="media-heading">
                                    <a href="<?= URL::site('privatecoach/profile/' . $row->id) ?>"
                                        class="link top_user_name"><strong
                                    class="ls"><?= $row->get_full_name() ?></strong></a>
                                </div>
                            </div>
                        </li>
                        <? endforeach ?>
                    </ul>
                </div>
                <div class="tab-pane" id="requests">
                    <ul class="unstyled user_subscribe_list">
                        <? foreach ($requests as $row): ?>
                        <li class="media">
                            <? if ($row->photo_s->loaded()): ?>
                            <div class="pull-left img-circle">
                                <?= $row->photo_s->html_img() ?>
                                <div class="mask84"></div>
                            </div>
                            <? endif ?>
                            <div class="media-body">
                                <div class="media-heading">
                                    <a href="<?= URL::site('privatecoach/profile/' . $row->id) ?>"
                                        class="link top_user_name"><strong
                                    class="ls"><?= $row->get_full_name() ?></strong></a>
                                </div>
                                <a href="#" class="sub_link sub_accept"
                                    data-url="<?= URL::site('privatecoach/subscribe/accept') ?>"
                                data-id="<?= $row->id ?>"><?= __('Добавить в подписчики') ?></a>
                            </div>
                        </li>
                        <? endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="aside">
        <?= View::factory('social_networks') ?>
    </div>
</div>