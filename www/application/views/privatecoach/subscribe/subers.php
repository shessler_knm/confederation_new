<div class="hr_top">

    <h4>
        <? if ($counter): ?>
            <?= Helper::word_cases($counter, 'подписчиков', 'подписчик', 'подписчика') ?>
        <? else: ?>
            <?= __('Нет подписчиков') ?>
        <?endif ?></h4>

    <ul class="unstyled">
        <? foreach ($followers as $row): ?>
            <li><a href="<?= URL::site('privatecoach/profile/' . $row->user_id) ?>" class="link"><span
                        class="ls"><?= $row->get_subscriber_name() ?></span></a></li>
        <? endforeach ?>
        <? if ($more): ?>
            <a href="<?= URL::site('privatecoach/subscribe/list/'.$id) ?>"
               class="link_dotted"><?= __('еще') ?> <?= $counter - 10 ?></a>
        <? endif ?>
    </ul>
</div>
