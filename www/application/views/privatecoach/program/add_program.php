<!--    <h1>--><?//=__('Редактирование')?><!--</h1>-->

<div id="myTabContent" class="tab-content">

    <ul class="unstyled inline nav_tabs">
        <!--            <li class="active"><a href="#create_programm" data-toggle="tab" class="link_dotted">-->
        <?//=__('Редактирование программу')?><!--</a></li>-->
        <li class="active"><a href="#professional_programm" data-toggle="tab"
                              class="link_dotted"><?= __('Профессиональные программы') ?></a></li>
        <li><a href="#user_programm" data-toggle="tab" class="link_dotted"><?= __('Программы пользователей') ?></a></li>
    </ul>
    <div class="tab-pane fade in active" id="professional_programm">
        <ul class="unstyled">
            <? foreach ($prof_programs as $prof_program): ?>
                <?= $prof_program->title ?>
                <?= Text::limit_chars($prof_program->text, 333) ?>
                <a href="<?=URL::site('privatecoach/program/edit/'.$prof_program)?>" class="program_copy" data-copy-url="<?= URL::site('privatecoach/program/copy') ?>"
                   data-url="<?= '/api/program/' . $prof_program->id ?>"><?= __('Копировать программу') ?></a>
            <? endforeach ?>
        </ul>
    </div>
    <div class="tab-pane fade" id="user_programm">
        <ul class="unstyled">
            <? foreach ($user_programs as $user_program): ?>
                <li><?= $user_program->user->get_full_name() ?></li>
                <li><?= $user_program->title ?></li>
                <li><?= Text::limit_chars($user_program->text, 333) ?></li>
                <a href="<?=URL::site('privatecoach/program/edit/'.$user_program)?>" class="program_copy" data-copy-url="<?= URL::site('privatecoach/program/copy') ?>"
                   data-url="<?= '/api/program/' . $user_program->id ?>"><?= __('Копировать программу') ?></a>
            <? endforeach ?>
        </ul>

    </div>
</div>


<div id="exerciseModal" class="popup_add_exercise modal hide fade" tabindex="-1" role="dialog"
     aria-labelledby="exerciseModal" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancel()">×</button>
        <h3 id="myModalLabel">Выберите упражнение</h3>
    </div>
    <div class="modal-body" ng-controller="exerciseCtrl">
        <form class="form-search">
            <input type="text" class="input-large search-query" placeholder="Название упражнения" ng-model="query">
        </form>
        <ul ng-repeat="item in items | filter:query">
            <li><a href="#" ng-click="select(item,$event)">{{ item.title }}</a></li>
        </ul>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true" ng-click="cancel()">Отмена</button>
    </div>
</div>


