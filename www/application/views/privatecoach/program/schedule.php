
<div class="row_fluid clearfix bg-white">
<div class="breadcrumbs">
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?= URL::site(I18n::$lang . '/privatecoach') ?>" itemprop="url" class="link"><span itemprop="title"
        class="ls"><?= __('Личный тренер') ?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?= URL::site(I18n::$lang . '/privatecoach/program/my_programs') ?>" itemprop="url" class="link"><span
        itemprop="title" class="ls"><?= __('Мои программы') ?></span></a>
    </div>
</div>
    <div class="row_section">
        <div class="section">
            <div class="page_program page_my_program" ng-app="privatecoach" ng-controller="scheduleCtrl">
                <h1><?= __('Мои программы') ?></h1>
                <div class="title_program_index">
                    <? if ($current_user_program->loaded()): ?>
                    <span class="h3"><a
                        href="<?= URL::site('privatecoach/program/view/' . $current_user_program->parent->id) ?>"
                        class="link"><span
                        class="ls"><?= $current_user_program->title ?></span></a></span>
                        <span class="item">
                            <?= Date::textdate($current_user_program->start_date, 'd m') ?>
                            -
                            <?= Date::textdate($date->format('Y-m-d'), 'd m') ?>
                        </span>
                        <? else: ?>
                        <?= __('В данный момент у Вас нет активной программы.') ?>
                        <a href="<?= URL::site(I18n::$lang . '/privatecoach/program/list') ?>"><?= __('Выбрать программу') ?></a>
                        <? endif ?>
                    </div>
                    <? if ($current_user_program->loaded()): ?>
                    <div lenta-calendar year="dtLenta.year" month="dtLenta.month" day="dtLenta.day" programs="programs"
                    logs="logs"></div>
                    <? endif ?>
                    <div class="link_my_program">
                        <? if ($current_user_program->loaded()): ?>
                        <a href="<?= URL::site(I18n::$lang . '/privatecoach/user/album') ?>" class="link"><span
                        class="ls"><?= __('Добавить результат') ?></span></a>
                        <a href="<?= URL::site(I18n::$lang . '/privatecoach/program/edit/' . $current_user_program->id) ?>"
                            class="link"><span class="ls"><?= __('Изменить') ?></span></a>
                            <a href="<?= URL::site(I18n::$lang . '/privatecoach/program/delete') ?>" class="link"><span
                            class="ls"><?= __('Удалить') ?></span></a>
                            <a href="<?= URL::site(i18n::$lang . '/privatecoach/program/pdf/' . $current_user_program->id) ?>"><span
                            class="ico_print">&nbsp;</span><span class="link"><span
                        class="ls"><?= __('Распечатать') ?></span></span></a>
                        <? endif ?>
                    </div>
                    <? if ($count): ?>
                    <h3><?= __('Список прошлых программ') ?></h3>
                    <ul class="unstyled">
                        <? foreach ($last_programs as $program): ?>
                        <li class="media hr_top">
                            <div class="pull-left">
                                <a href="<?= URL::site('privatecoach/program/view/' . $program->parent->id) ?>"><?= $program->parent->photo_s->html_cropped_img(200) ?></a>
                            </div>
                            <div class="media-body">
                                <div class="media-heading">
                                    <div class="h4">
                                        <a href="<?= URL::site('privatecoach/program/view/' . $program->parent->id) ?>"
                                            class="link">
                                            <span class="ls">
                                                <?= $program->title ?>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="media-p5"><em><?= $program->mission->title ?></em></div>
                                <div class="media-p5">
                                    <?= Date::textdate($program->start_date, 'd m, Y') ?>
                                    - <?= Date::textdate($data[$program->id], 'd m, Y') ?>
                                </div>
                                <div class="media-p10">
                                    <a href="<?= URL::site('privatecoach/user/gallery/?q=' . $program->id) ?>"
                                        class="link_dotted"><span class="ls"><?= __('Мои результаты') ?></span></a>
                                    </div>
                                    <div class="media-p">
                                        <? if (!$program->loaded()): ?>
                                        <a href="#"
                                            data-url="<?= URL::site('privatecoach/program/copy/' . $program->parent->id) ?>"
                                        class="add_program btn color"><?= __('Повторить') ?></a>
                                        <? endif ?>
                                        <div class="star view pull-right" data-read-only=1
                                        data-score=<?= array_key_exists($program->id, $data) ? $data[$program->id] : 0 ?>></div>
                                    </div>
                                </div>
                            </li>
                            <? endforeach ?>
                        </ul>
                        <? endif ?>
                    </div>
                </div>
            </div>
            <div class="aside">
                <?= View::factory('social_networks') ?>
            </div>
        </div>
        <div id="own_program" data-own-profile="1"></div>
        <?= View::factory('privatecoach/program/exercise_promt')->render() ?>
        <div id="hint" data-hint="<?= __('Нажмите, чтобы посмотреть описание') ?>" style="display: none"></div>