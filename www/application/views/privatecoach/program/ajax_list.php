<? if ($count): ?>

    <? foreach ($model as $row): ?>
        <li class="media program_back_img hr_top">
            <? if ($row->photo):?>
            <div class="pull-left">
                <a href="<?= URL::site('privatecoach/program/view/' . $row->id) ?>" style="background-image: url('<?= $row->photo_s->url()?>')">
                    <?/*= $row->photo_s->html_img(200, null, array('alt' => $row->title)) */?>
                </a>
            </div>
            <?endif?>
            <div class="media-body">
                <div class="media-heading">
                    <div class="h4">
                        <a href="<?= URL::site('privatecoach/program/view/' . $row->id) ?>" class="link">
                    <span class="ls">
                        <?= $row->title ?>
                    </span>
                        </a>
                    </div>
                </div>
                <div class="media-heading">
                    <em><?= $row->mission->title ?></em>
                </div>
                <div class="medium_text">
                    <?= Text::limit_chars(strip_tags($row->text), 220) ?>
                </div>
                <small><em>
                        <span class="date"><?= Date::textdate($row->created, 'd m') ?></span>
                        <!--                                        <span> -->
                        <?//= Helper::word_cases($row->views, __('подписчиков'), __('подписчик'), __('подписчика')) ?><!--</span>-->
                    <span
                        class="data"> <?= Helper::word_cases(Comments::get_count('Program', $row->id), __('комментариев'), __('комментарий'), __('комментария')) ?></span>
<!--                        <span class="data"> 6 просмотров</span>-->
                    </em></small>

                <div class="clearfix">
                    <? if ($user): ?>
                        <? if (!$current_program->loaded()): ?>
                            <a href="#" data-url="<?= URL::site('privatecoach/program/copy/' . $row->id) ?>"
                               class="add_program btn color"
                                ><?= __('Добавить себе') ?></a>
                        <? endif ?>
                    <? else: ?>
                        <a data-target="#myModal" data-toggle="modal"
                           class="btn color"
                            ><?= __('Добавить себе') ?></a>
                    <?endif ?>

                    <div class="star view" data-read-only=1
                         data-score=<?= array_key_exists($row->id, $array) ? $array[$row->id] : 0 ?>></div>
                </div>
            </div>
        </li>
    <? endforeach ?>

<!--    <a href="#back">Наверх</a>-->
    <?= $pagination ?>
<? else: ?>
    <?= __('По данному запросу ничего не найдено') ?>
<?endif ?>

    <!--<div class="hr_top pagination_ajax text-center">
    <a href="#" class="link"><span class="ls"><?/*= __('Показать еще') */?></span></a>
</div>-->