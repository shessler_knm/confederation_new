<ul class="unstyled programm_list news_list interview_list">
<? foreach ($model as $row): ?>
    <li class="hr_top media">
        <div class="pull-left">
            <a href="<?= URL::site('privatecoach/program/view/' . $row->id) ?>">
                <?= $row->photo_s->html_img(200, null, array('alt' => $row->title)) ?>
            </a>
        </div>
        <div class="media-body">
            <div class="media-heading">
                <div class="h5">
                    <a href="<?= URL::site('privatecoach/program/view/' . $row->id) ?>" class="link">
                    <span class="ls">
                        <?= $row->title ?>
                    </span>
                    </a>
                </div>
            </div>
            <div class="media-heading">
                <em class="muted"><?=$row->mission->title?></em>
            </div>
            <div class="medium_text">
                <?= Text::limit_chars(strip_tags($row->text),300) ?>
            </div>
            <small><em>
                    <span class="date"><?= Date::textdate($row->created, 'd m') ?></span>
                    <!--                                        <span> -->
                    <?//= Helper::word_cases($row->views, __('подписчиков'), __('подписчик'), __('подписчика')) ?><!--</span>-->
                    <span
                        class="data"> <?= Helper::word_cases(Comments::get_count('Program', $row->id), __('комментариев'), __('комментарий'), __('комментария')) ?></span>
<!--                    <span class="data"> --><?//= Helper::word_cases($row->views, __('просмотров'), __('просмотр'), __('просмотра')) ?><!--</span>-->
                </em></small>

            <div class="clearfix">
                <div class="star view" data-read-only=1
                     data-score=<?= array_key_exists($row->id, $scores) ? $scores[$row->id] : 0 ?>></div>
            </div>
        </div>
    </li>
<? endforeach ?>
</ul>
<!--<div class="hr_top pagination_ajax text-center">
    <a href="#" class="link"><span class="ls"><?/*= __('Показать еще') */?></span></a>
</div>-->
