<div class="program_log" data-index="1" data-max-slides="<?= $max_slides ?>">
    <div class="h1">
        <?= $model->parent->title ?>
    </div>

    <div class="pull-left program_log_info">
        <? if ($first_log): ?>
            <label><?= __('Дата') ?></label>
            <div class="item date">
                <?= Date::textdate($first_log->date, 'd m Y') ?>
            </div>
            <label><?= __('Вес') ?></label>
            <div class="item weight">
                <?= $first_log->weight ?>
            </div>
            <? if ($first_log->fat): ?>
                <label><?= __('Подкожный жир') ?></label>
                <div class="item fat">
                    <?= $first_log->fat ?>
                </div>
            <? endif ?>
        <? endif ?>
    </div>

    <div class="pull-right program_log_slider">
        <div class="pull-left photo_item">
            <? if ($first_log): ?>
                <div class="photo" style="background-image: url('<?= $first_log->photo_s->url() ?>')"></div>
            <? endif ?>
            <? if ($logs->count() > 1): ?>
                <a class="photo_slide photo_slide_prev" data-slide="prev">&larr;</a>
                <a class="photo_slide photo_slide_next" data-slide="next">&rarr;</a>
            <? endif ?>
        </div>
        <div class="media-body">
            <div class="inner">
                <? foreach ($logs as $log): ?>
                    <a href="<?= $log->photo_s->url() ?>" class="program_photo"
                       data-date="<?= Date::textdate($log->date, 'd m Y') ?>"
                       data-weight="<?= $log->weight . ' ' . __('кг') ?>"
                       data-fat="<?= $log->fat . ' %' ?>"
                       data-index="<?= $index ?>" style="background-image: url('<?= $log->photo_s->url() ?>')">
                        <? /*= $log->photo_s->html_img(100) */ ?>
                    </a>
                    <? $index++ ?>
                <? endforeach ?>
            </div>
        </div>
    </div>


</div>




