
<div class="row_fluid clearfix bg-white">
<div class="breadcrumbs">
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('Личный тренер')?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach/program/list')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('Программы тренировок') ?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach/program/view/'.$program->id)?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=$program->title ?></span></a>
    </div>
</div>
    <div class="row_section">
        <div class="section">
            <div class="page_section">
                <h1><?= $program->title ?></h1>

                <div class="goal_program_info">
                    <strong><?= __('Цель программы') ?>: </strong>
                    <span class="muted"><?= $program->mission->title ?></span>
                </div>

                <div class="media">
                    <div class="figure_page pull-left"><?= $program->photo_s->html_img(300, null) ?></div>
                    <div class="media_text">
                        <?= $program->text ?>
                    </div>
                    <div class="social_block pull-right" id="social_block">
                        Yandex share?
                    </div>
                </div>
            </div>

            <div class="info_page_section">
                <em class="muted"><?= Date::textdate($program->created, 'd m Y') ?></em>
                <em class="muted"><?= Helper::word_cases(Comments::get_count('Program', $program->id), __('комментариев'), __('комментарий'), __('комментария')) ?></em>
                <em class="muted"><?= Helper::word_cases($count, __('подписчиков'), __('подписчик'), __('подписчика')) ?></em>

                <div class="pull-right">
                    <em class="muted"><?= Helper::word_cases($score['count'], __('проголосовавших'), __('проголосовавший'), __('проголосовавших')) ?></em>
                    <? if ($user): ?>
                        <div class="star view" data-id="<?= $program->id ?>" data-score="<?= $score['score'] ?>"></div>
                    <? else: ?>
                        <div class="star view" data-id="<?= $program->id ?>" data-score="<?= $score['score'] ?>"
                             data-read-only=1 data-target="#myModal" data-toggle="modal"></div>
                    <?endif ?>
                </div>
            </div>

            <div class="page_program page_program_index page_info_program" id="program_slider">
                <div class="program_section">
                    <h4><?= __('Подробности программы') ?></h4>

                    <div class="program_slider">
                        <div class="program_inner">
                            <div class="program_item_wrap">
                                <!--                                --><? // foreach ($days as $day): ?>
                                <!--                                    --><? //= __('День') . ' ' . $i ?>
                                <!--                                    <td>-->
                                <!--                                        <ul class="unstyled">-->
                                <!--                                            --><? // foreach ($day as $exercise): ?>
                                <!--                                                <li>-->
                                <?//= $exercise ?><!--</li>-->
                                <!--                                            --><? // endforeach ?>
                                <!--                                        </ul>-->
                                <!--                                    </td>-->
                                <!--                                    --><? // $i++ ?>
                                <!--                                --><? // endforeach ?>
                                <? foreach ($days as $day): ?>

                                    <div class="program_item">
                                        <div class="list_checkbox">
                                            <div class="date"><?= __('День') . ' ' . $i ?></div>
                                            <div class="inner">
                                                <? foreach ($day as $exercise): ?>
                                                    <div data-target="#myExercise" data-toggle="modal" title="<?=__('Нажмите, чтобы посмотреть описание')?>" data-id="<?=array_search($exercise,$day)?>" class="title exTitle"><?= $exercise ?></div>
                                                <? endforeach ?>
                                            </div>
                                        </div>
                                    </div>

                                    <? $i++ ?>
                                <? endforeach ?>
                            </div>
                        </div>
                        <a class="nav_slider prev">←</a><a class="nav_slider next">→</a>
                    </div>
                </div>
            </div>
            <? if ($user): ?>
                <? if ($current_user_program->activity != 1): ?>
                    <!--                --><? // if (!$current_program->loaded()): ?>
                    <!--                    <a href="#" data-url="--><?//= URL::site('privatecoach/program/copy/' . $row->id) ?><!--"-->
                    <!--                       class="add_program btn color"-->
                    <!--                        >--><?//= __('Добавить себе') ?><!--</a>-->
                    <!--                --><? // endif ?>
                    <div class="btn_page_program">
                        <a href="#" id="program_copy"
                           data-url="<?= URL::site('privatecoach/program/copy/' . $program->id) ?>"
                           class="add_program btn color"><?= __('Добавить себе') ?></a>
                    </div>
                <? endif ?>
            <? else: ?>
                <a data-target="#myModal" data-toggle="modal"
                   class="btn color"
                    ><?= __('Добавить себе') ?></a>
            <? endif ?>

            <?/* foreach ($days as $day): */?><!--
            <?/*=__('День').' '.$i*/?>
            <td>
                <ul class="unstyled">
                    <?/* foreach ($day as $exercise): */?>
                    <li><?/*= $exercise */?></li>
                    <?/* endforeach */?>
                </ul>
            </td>
            <?/*$i++*/?>
            --><? //endforeach ?>

            <div class="comment_no_border">
                <?= Comments::get_list($program->object_name(), $program->id) ?>
            </div>


        </div>
    </div>

    <div class="aside">
        <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div>
        <?= View::factory('social_networks') ?>
    </div>
</div>
<?if ($user):?>
<div id="advance_options" style="display: none" data-url="<?= URL::site('privatecoach/program/score') ?>"
     data-no-rating-hint="<?= __('Оценка отстуствует') ?>" data-read-only="false"></div>
<?endif?>
<?= View::factory('privatecoach/program/exercise_promt')->render() ?>
<div id="hint" data-hint="<?= __('Нажмите, чтобы посмотреть описание') ?>" style="display: none"></div>