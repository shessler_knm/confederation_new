<div style="padding-bottom: 25px;
    margin-bottom: 30px;
    border-bottom: 3px solid #d0ab67;">

    <div style="width: 85px; float: right; text-align: center;">
        <img src="http://confederation.kz/images/pgf_logo.png" alt="" style="width: 83px;">
        <div style="text-align: center;
        margin-top: 5px;
        font-size: 8px;
        line-height: 10px;
        font-family: Arial,sans-serif;
        font-weight: bold;
        color: #0656a2;">confederation.kz</div>
    </div>

    <h1 style="margin: 0 100px 0 0;
    font-weight: bold;
    font-family: Arial,sans-serif;
    font-size: 32px;
    line-height: 45px;"><?= $program->title ?></h1>


    <div style="font-size: 16px; font-family: Arial,sans-serif; line-height: 20px; margin-right: 100px;">
        <? foreach ($muscle_groups as $muscle): ?>
        <?= $muscle->title ?>
        <? endforeach ?>
    </div>
</div>

<div>
    <? foreach ($days as $key => $day): ?>
    <div style="width: 50%;
    float: left;
    vertical-align: top;
    font-family: Arial,sans-serif;">
        <div style="margin-bottom: 20px;
    font-weight: bold;
    font-size: 16px;line-height: 20px;">
            <?= __('День') . ' ' . $key ?>
        </div>
        <div style="padding-bottom: 10px;">
            <? foreach ($day as $exercise): ?>
            <div style="display:block;padding: 0 20px 20px 0;">
                <img src="http://confederation.kz/images/pgf_square.png" alt="" style="margin-right: 5px;">
                <span style="font-family: Arial,sans-serif; font-size: 13px; line-height: 18px;"><?= $exercise ?></span>
            </div>
            <? endforeach ?>
        </div>
    </div>
    <? endforeach ?>
</div>


