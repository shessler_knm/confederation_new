
<div class="row_fluid clearfix bg-white">
<div class="breadcrumbs">
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('Личный тренер')?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach/program/list')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('Программы тренировок') ?></span></a>
    </div>
</div>
    <a name="back"></a>
    <h1><?= __('Программы тренировок') ?></h1>

    <form id="program_filter">
        <div class="filter_list_section filter_programm_section">
            <div class="clearfix">
                <div class="pull-left w225 w30">
                    <div class="title_filter"><?= __('Цели тренировок') ?></div>
                    <div class="filter_select mr10">
                        <?= Form::select('goal', $array['goals'], array_key_exists('mission',$query)?$query['mission']:null, array('class' => 'custom_select', 'id' => 'goals')) ?>
                    </div>
                </div>
                <div class="pull-left w200 w20">
                    <div class="filter_select mr20">
                        <div class="title_filter">&nbsp;</div>
                        <?= Form::select('muscle', $categories, array_key_exists('muscle',$query)?$query['muscle']:null, array('class' => 'custom_select muscles', 'id' => 'muscles')) ?>
                        <!--                    <select class="custom_select">-->
                        <!--                        <option value="1">Грудные мышцы</option>-->
                        <!--                        <option value="2">Попные мышцы</option>-->
                        <!--                    </select>-->
                        <? //=Form::select('body',$array['body'],$array['body'][$body->sef],array('class'=>'hidden'))?>
                    </div>
                </div>

                <div class="pull-left w200 w30">
                    <div class="title_filter"><?= __('Место тренировок') ?></div>
                    <div class="filter_select mr20">
                        <?= Form::select('place', $array['place'], array_key_exists('place',$query)?$query['place']:null, array('class' => 'custom_select', 'id' => 'place')) ?>
                    </div>
                </div>

                <div class="pull-left w200 w20">
                    <div class="title_filter"><?= __('Количество занятий в неделю') ?></div>
                    <div class="filter_select">
                        <?= Form::select('sessions', $array['sessions'], array_key_exists('sessions',$query)?$query['sessions']:null, array('class' => 'custom_select', 'id' => 'sessions')) ?>
                        <? //= Form::select('sex', $array['sex'], null, array('class' => 'custom_select', 'id' => 'sex')) ?>
                    </div>
                </div>
            </div>

            <div class="clearfix">
                <div class="pull-left filter_search_text">
                    <?= Form::input('search', array_key_exists('q',$query)?$query['q']:__('Поиск по тексту'), array('class' => 'filter_search', 'id' => 'search', 'data-default' => __('Поиск по тексту'))) ?>
                </div>

                <div class="control_filter pull-right">
                    <a href="#" id="clearFilters" data-clear="<?= URL::site('') ?>" class="link">
                        <span class="ls"><?= __('Очистить фильтр') ?></span>
                    </a>
                    <a href="#" id="submit" class='btn color'
                       data-url=<?= URL::site(I18n::$lang.'/privatecoach/program/ajax_list') ?>>
                        <?= __('Подобрать программу') ?>
                    </a>
                </div>
            </div>

        </div>
    </form>


    <div class="row_section">
        <div class="section">
            <ul class="unstyled programm_list event_star_list news_list interview_list">

            </ul>
        </div>
    </div>
    <div class="aside">
        <!-- <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div> -->
        <?= View::factory('social_networks') ?>
    </div>
</div>
<div id="advance_options" style="display: none" data-no-rating-hint="<?= __('Оценка отстуствует') ?>"></div>
