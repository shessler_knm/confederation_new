<script type="text/javascript">
    P_ID =<?=$model->id?>;
    U_PRO = true;
    U_LEVEL = <?=$level?>;
</script>
<div class="row_fluid clearfix bg-white">
<div class="edit_my_program_section">
    <h1><?= __('Редактирование программы') ?></h1>

    <div class="info_text"><?=__('Здесь, если необходимо, вы можете внести изменения в программу.')?></div>
    <div class="add_program_section" ng-app="privatecoach" ng-controller="mainCtrl"
         data-training="<?= $user->training ?>">
        <div class="header_add_program">
            <div class="title_program"><span>{{model.title_<?= I18n::$lang ?>}}</span> </div>
            <ul class="unstyled inline pull-right">
                <li>
                    <a href="#" class="link_dotted link_date start_date" ng-click="selectStartDate($event)"
                       data-date-format="dd.mm.yyyy" data-lang="<?=I18n::$lang=='kz'?'kk':I18n::$lang?>">
                       <?= __('Начало') ?>:
                        <span class="ls">{{model.start_date }}</span>
                    </a>
                </li>
                <li>
                    <?= __('Окончание') ?>:
                    {{model.end_date }}
                </li>
            </ul>
        </div>

        <div class="wrap_table">


            <table class="table table_add_program">


                <tbody ng-repeat="day in model.days">
                <tr class="thead">
                    <td class="col1">
                        <div class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{ day.title }} <span
                                    class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                <li ng-repeat="d in weekDays | orderBy:sortWeek:reverse"><a href="#"
                                                                                            ng-click="changeDay(d,day,$event)">{{
                                        d.title }}</a></li>
                            </ul>
                        </div>
                    </td>
                    <td>
                        <?=__('Подходы и повторения')?>
                    </td>
                </tr>

                <tr ng-repeat="exercise in day.exercises">
                    <td class="col1" data-target="#myExercise" data-toggle="modal" title="<?=__('Нажмите, чтобы посмотреть описание')?>" data-id="{{ exercise.id}}">{{ exercise.title }}</td>
                    <td>
                        <ul class="unstyled inline number_times_list">
                            <!--        <li>
                                        <div class="number_select  number_left">
                                            <span class="num ng-binding">2</span>
                                            <a href="#" class="btn_up" ng-click="upRepeat(effort,$event)"></a>
                                            <a href="#" class="btn_dwn" ng-click="downRepeat(effort,$event)"></a>
                                        </div>
                                        <span class="divider">x</span>
                                        <div class="number_select number_right">
                                            <span class="num ng-binding">10</span>
                                            <a href="#" class="btn_up" ng-click="upRepeat(effort,$event)"></a>
                                            <a href="#" class="btn_dwn" ng-click="downRepeat(effort,$event)"></a>
                                        </div>
                                    </li>-->

<!--                            --><?// switch ($level): ?>
                                <li ng-repeat="effort in exercise.efforts">
                                    <div class="number_select">
                                        <span class="num">{{ Math.floor(effort.repeat*user_level)}}x</span>
                                        <a href="#" class="btn_up" ng-click="upRepeat(effort,$event)"></a>
                                        <a href="#" class="btn_dwn" ng-click="downRepeat(effort,$event)"></a>
                                    </div>
                                </li>

                        </ul>
                        <div class="controls-row">
                            <a href="#" class="i_add" title="Добавить"
                               ng-click="addEffort(exercise,$event)"><?= __('Добавить') ?></a>
                            <a href="#" class="i_minus" title="Убрать"
                               ng-click="removeEffort(exercise,$event)"><?= __('Убрать') ?></a>
                        </div>
                    </td>
                </tr>
                </tbody>


            </table>
            <? if ($model->activity == 0): ?>
                <div data-add="<?= URL::site('privatecoach/program/add_data') ?>"
                     data-error="<?= __('Произошла ошибка') ?>"
                     data-data="<?= URL::site('privatecoach/program/data/' . $id) ?>" class="gray_block">
                    <div class="add_data empty_data">
                <span class="h3">
                    <?= __('Мои данные') ?>
                </span>
                        <span><?= __('Вы пока не предоставили никаких данных о себе') ?>.</span>

                        <div
                            class="control_data"><?= Form::button('add_data', __('Внести данные'), array('class' => 'btn color', 'id' => 'add_data')) ?></div>
                    </div>
                </div>
            <? endif ?>
            <div class="info_text"><?=__('Нажимая кнопку «Сохранить», Вы подтверждаете, что не имеете никаких противопоказаний для занятий спортом. Пользователь несет полную ответственность за какие-либо травмы, полученные, в ходе занятий с использованием данного приложения')?>.</div>
            <div class="btn_control_program">
                <!--        <input type="button" value="Добавить день" class="btn color" ng-click="addDay()">-->
                <input type="button" id="saveProgram" value="<?= __('Сохранить программу') ?>" class="btn color"
                       data-url="<?= URL::site('privatecoach/program/my_programs') ?>" ng-click="save()">
                <? if ($query): ?>
                    <a href="<?= URL::site('privatecoach/program/list?id=' . $id . '&mission=' . $query['mission'] . '&muscle=' . $query['muscle'] . '&place=' . $query['place'] . '&session=' . $query['session'] . '&q=' . $query['q']) ?>"
                       class="btn color"><?= __('Отменить') ?></a>
                <? else: ?>
                    <a href="<?= URL::site('privatecoach/program/list?id=' . $id) ?>"
                       class="btn color"><?= __('Отменить') ?></a>
                <?endif ?>
                <!--            <input type="button" value="-->
                <?//=__('Отменить')?><!--" class="btn color" ng-click="delete()">-->
            </div>
        </div>
    </div>
</div>

<div id="popup">
    <div class="inner">
        <h3><?= __('Добавить данные') ?></h3>

        <form method="post" class="form-horizontal" id="userImage" name="uploadForm"
              action="<?= URL::site('privatecoach/user/load_photo/') ?>"
              data-result="<?= URL::site('privatecoach/user/add_result/' . $id . '?album=' . $album_id) ?>"
              enctype="multipart/form-data" encoding="multipart/form-data">
            <!--    <form id="userImage" action="-->
            <?//=URL::site('privatecoach/user/add_result/'.$id.'?activity=true') ?><!--" method="post" enctype="multipart/form-data" encoding="multipart/form-data">-->

            <div class="control-group">
                <div class="control-label"><?= __('Вес') ?></div>

                <div class="controls">
                    <?= Form::input('weight', null, array('class' => 'input-mini')) ?><?= __('кг') ?>
                </div>
            </div>
            <div class="control-group">
                <div class="control-label"><?= __('Рост') ?></div>

                <div class="controls">
                    <?= Form::input('height', $user->height, array('class' => 'input-mini')) ?><?= __('см') ?>
                </div>
            </div>
            <p class="error"> <?= __(arr::get($errors, 'weight')) ?></p>

            <div class="accordion" id="accordion2">
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                            <?=__('Подкожный жир')?>
                        </a>
                    </div>
                    <div id="collapseOne" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <div class="control-group">
                                <div class="control-label"><a href="#" class="hint link_dotted"
                                                              data-img='<?= URL::site('images/min_logo.png', 'http') ?>'
                                                              data-placement="bottom"
                                                              data-content="<?= $data_hint[0] ?>"><span
                                            class="ls"><?= __('Складки на груди') ?></span></a></div>

                                <div class="controls">
                                    <?= form::input('chest', Arr::get($_POST, 'chest', null), array('class' => 'input-mini')) ?><?= __('мм') ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="control-label"><a href="#" class="hint link_dotted"
                                                              data-img='<?= URL::site('images/banner.png', 'http') ?>'
                                                              data-placement="bottom"
                                                              data-content="<?= $data_hint[1] ?>"><span
                                            class="ls"><?= __('Складки на животе') ?></span></a></div>

                                <div class="controls">
                                    <?= form::input('stomach', Arr::get($_POST, 'stomach', null), array('class' => 'input-mini')) ?><?= __('мм') ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="control-label"><a href="#" class="hint link_dotted"
                                                              data-img='<?= URL::site('images/min_logo.png', 'http') ?>'
                                                              data-placement="bottom"
                                                              data-content="<?= $data_hint[2] ?>"><span
                                            class="ls"><?= __('Складки на бёдрах') ?></span></a></div>

                                <div class="controls">
                                    <?= form::input('haunch', Arr::get($_POST, 'haunch', null), array('class' => 'input-mini')) ?><?= __('мм') ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="control-label"><a href="#" class="hint link_dotted"
                                                              data-img='<?= URL::site('images/min_logo.png', 'http') ?>'
                                                              data-placement="bottom"
                                                              data-content="<?= $data_hint[3] ?>"><span
                                            class="ls"><?= __('Складки на трицепсе') ?></span></a></div>

                                <div class="controls">
                                    <?= form::input('triceps', Arr::get($_POST, 'triceps', null), array('class' => 'input-mini')) ?><?= __('мм') ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="control-label"><a href="#" class="hint link_dotted"
                                                              data-img='<?= URL::site('images/min_logo.png', 'http') ?>'
                                                              data-placement="bottom"
                                                              data-content="<?= $data_hint[4] ?>"><span
                                            class="ls"><?= __('Складки под лопаткой') ?></span></a></div>

                                <div class="controls">
                                    <?= form::input('blade', Arr::get($_POST, 'blade', null), array('class' => 'input-mini')) ?><?= __('мм') ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="control-label"><a href="#" class="hint link_dotted"
                                                              data-img='<?= URL::site('images/min_logo.png', 'http') ?>'
                                                              data-placement="bottom"
                                                              data-content="<?= $data_hint[5] ?>"><span
                                            class="ls"><?= __('Складки над подвздошной костью') ?></span></a></div>

                                <div class="controls">
                                    <?= form::input('ilium', Arr::get($_POST, 'ilium', null), array('class' => 'input-mini')) ?><?= __('мм') ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="control-label"><a href="#" class="hint link_dotted"
                                                              data-img='<?= URL::site('images/min_logo.png', 'http') ?>'
                                                              data-placement="bottom"
                                                              data-content="<?= $data_hint[6] ?>"><span
                                            class="ls"><?= __('Складка подмышкой') ?></span></a></div>

                                <div class="controls">
                                    <?= form::input('armpit', Arr::get($_POST, 'armpit', null), array('class' => 'input-mini')) ?><?= __('мм') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?= Form::hidden('fat', null) ?>
            <?= Form::hidden('file', null) ?>
            <div class="hr_top">
                <a href="#" class="link_dotted" id="add_photo"><span
                        class="ls"><?= __('Выбрать фотографию') ?></span></a>
            </div>
            <div class="progress hidden">
                <div class="bar"></div>
                <div class="percent">0%</div>
            </div>

            <div id="status"></div>

            <?= Form::hidden('date', date('Y-m-d'), array('id' => 'advanced_form',)) ?>
            <div class="text-center">
                <input id="submit" style="display: none" type="submit" name="uploadFile">
                <?= Form::input('add', __('Добавить'), array('class' => 'btn color', 'id' => 'submit_result', 'disabled' => 'false')) ?>
            </div>
            <!--        <input type="file" name="myfile"><br>-->
            <!--        <input type="submit" value="Upload File to Server">-->

                <input type="file" id="myfile" name="myfile" class="hidden" style="height:0px"><br>
            <input type="submit" id="upload" value="Upload File to Server" style="display: none;">
        </form>


        <div id="status"></div>

        <?= Form::hidden('age', $age) ?>
        <?= Form::hidden('sex', $user->sex) ?>
    </div>
</div>

<div id="backgroundPopup"></div>
<!--<div class="progress">-->
<!--    <div class="bar"></div >-->
<!--    <div class="percent">0%</div >-->
<!--</div>-->
<?= View::factory('privatecoach/program/exercise_promt')->render() ?>
</div>