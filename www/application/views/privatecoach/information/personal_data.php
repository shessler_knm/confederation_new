<div class="row_fluid profile_edit_section clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <h1><?=__('Заполнение профиля')?></h1>

            <form action="" method="post" enctype="multipart/form-data" encoding="multipart/form-data"
                  class="form-horizontal form_usual_privatecoach">
                <div class="control-group">
                    <p><em><?=__('Пожалуйста, заполните данные профиля для подбора программы тренировок')?>.</em></p>
                </div>
                <div class="control-group">
                    <label class="control-label"><?= __('Имя') ?>:</label>

                    <div class="controls">
                        <?= Form::input('firstname', $user->firstname) ?>
                        <span class="error"><?=__(Arr::get($validate_error, 'firstname'))?></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?= __('Фамилия') ?>:</label>

                    <div class="controls">
                        <?= Form::input('lastname', $user->lastname) ?>
                        <span class="error"> <?=__(Arr::get($validate_error, 'lastname'))?></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?= __('Пол') ?>:</label>

                    <div class="controls custom_checkbox">
                        <label class="pull-left">
                            <?= Form::radio('sex', 1, true, array('class'=>'styled_radio_button')) ?><?= __('мужской') ?>
                        </label>
                        <label class="pull-left">
                            <?= Form::radio('sex', 2, false, array('class'=>'styled_radio_button')) ?><?= __('женский') ?>
                        </label>
                    </div>
                </div>

                <div class="control-group">
                    <div class="control-label"><?= __('Рост') ?></div>

                    <div class="controls">
                        <?= Form::input('height', $user->height, array('class' => 'input-mini')) ?><?= __('см') ?>
                        <span class="error"><?=__(Arr::get($validate_error, 'height'))?></span>
                    </div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?= __('Вес') ?></div>

                    <div class="controls">
                        <?= Form::input('weight', $user->weight, array('class' => 'input-mini')) ?><?= __('кг') ?>
                        <span class="error"><?=__(Arr::get($validate_error, 'weight'))?></span>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"><?= __('Дата рождения') ?>:</label>

                    <div class="controls">
                        <?= Form::input('birthdate', $user->birthdate, array('style' => 'display:none','id'=>'advanced_form')) ?>

                        <div id="bday" data-url="<?=URL::site('privatecoach/user/days')?>" class="select_day s_d">
                            <?= Form::select('bday', $date['days'], Date::formatted_time($user->birthdate,'d'),array('class'=>'custom_select')) ?>
                        </div>
                        <div class="select_day s_m">
                            <?= Form::select('bmonth', $date['months'], Date::formatted_time($user->birthdate,'m'), array('id' => 'bmonth','class'=>'custom_select')) ?>
                        </div>
                        <div class="select_day s_y">
                            <?= Form::select('byear', $date['years'], Date::formatted_time($user->birthdate,'Y'), array('id' => 'byear','class'=>'custom_select')) ?>
                        </div>
<!--                        <a href="#" id="datepicker">--><?//=$user->birthdate!='0000-00-00'?Date::formatted_time($user->birthdate,'d.m.Y'):'01.01.1970' ?><!--</a>-->
<!--                        <div id="birthdate">--><?//=$user->birthdate!='0000-00-00'?Date::formatted_time($user->birthdate,'d.m.Y'):'01.01.1970' ?><!--</div>-->
<!--                        <a href="#" id="datepicker" data-date-format="yyyy-mm-dd" data-lang="--><?//=I18n::$lang=='kz'?'kk':I18n::$lang?><!--" data-date="--><?//=$user->birthdate?><!--">--><?//=$user->birthdate!='0000-00-00'?Date::textdate($user->birthdate,'d m, Y'):'01.01.1970' ?><!--</a>-->
                        <span class="error"><?=__(Arr::get($validate_error, 'birthdate'))?></span>
<!--                        --><?//= Form::checkbox('checkbox',null,$user->hiding)?>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?= __('Цель тренеровок')?>:</label>
                    <div class="controls"><?= Form::select('type', $mission_array, NULL, array('class' => 'custom_select')) ?></div>
                </div>
                <div class="control-group">
                    <label class="control-label"><?= __('Необходимая масса')?></label>
                    <div class="controls">
                        <?= Form::input('weight', $mission_targets->weight, array('class' => 'input-mini')) ?>
                        <?= __('кг')?>
                        <span class="error"><?=__(Arr::get($validate_error, 'weight'))?></span>
                    </div>
                </div>
                <div class="control-group control-double-row">
                    <label class="control-label"><?= __('Необходимый процент подкожного жира')?></label>
                    <div class="controls">
                        <?= Form::input('fat', $mission_targets->fat, array('class' => 'input-mini')) ?>
                        %
<!--                        <span class="error">--><?//=__(Arr::get($validate_error, 'fat'))?><!--</span>-->
                    </div>
                </div>
                <div class="control-group">
                    <a href="<?= URL::site(I18n::$lang . '/confederation/page/-instruktsiya-po-izmereniu-podkozhnogo-zhira') ?>"><?= __('Инструкция') ?></a>
                </div>
                <div class="control-group">
                    <label class="control-label"><?= __('Уровень подготовки')?></label>
                    <div class="controls">
                        <?= Form::select('training', $training, null, array('class' => 'custom_select')) ?>
                        <span class="error"><?=__(Arr::get($validate_error, 'training'))?></span>
                    </div>
                </div>
                <div class="hr_top text-center btn18">
                    <?= form::input('submit', __('Подобрать программу').' →', array('type' => 'submit','class'=>'btn btn_circle btn_big color')) ?>
                </div>
                <?=Form::input('photo',null,array('id'=>'user_photo','style'=>'display:none'))?>
            </form>
        </div>
    </div>
    <div class="aside text-center">
        <div style="display:none" id="alert" data-text="<?= __('Вы должны выбрать валидное изображение') ?>!"></div>
        <iframe style="display: none;" id="superframe" name="superframe"></iframe>
        <!-- Область предварительного просмотра-->
        <form id="frm" name="uploadForm" action="<?= URL::site('privatecoach/user/avatar') ?>" method="post"
              enctype="multipart/form-data"
              encoding="multipart/form-data" target="superframe" class="upload_ava_form">
            <div class="img-circle">
                <img id="uploadPreview">
                <? if ($user->photo_s->loaded()): ?>
                    <div id="currentImage">
                        <?= $user->photo_s->html_img(300, 300) ?><br>
                    </div>
                <? else: ?>
                    <div id="cap">
                        <img src="">
                    </div>
                <? endif ?>
                <div class="mask200"></div>
            </div>
            <input id="uploadImage" style="visibility: hidden" type="file" name="uploadFile">
            <a href="#" id="changeAvatar" class="link"><span class="ls"><?= __('Сменить аватар') ?></span></a><br>
            <span class="error"><?=__(Arr::get($validate_error, 'photo'))?></span>
        </form>
    </div>
</div>


