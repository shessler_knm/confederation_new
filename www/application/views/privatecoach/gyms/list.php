
<div class="row_fluid clearfix bg-white">
<div class="breadcrumbs">
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('Личный тренер')?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach/gym/list')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('Тренажерные залы') ?></span></a>
    </div>
</div>
    <div class="row_section">
        <div class="section">
            <h1><?= __('Тренажерные залы') ?></h1>

            <div class="filter_list_section">
                <div class="title_filter"><?=__('Город')?></div>
                <div class="hr">
                    <div class="filter_select">
                        <?= Form::select('city_select', $cityList, $select_id, array('id' => 'select_id','class' => 'custom_select')) ?>
                    </div>
                </div>
                <div class="title_filter"><?=__('Сортировать')?></div>
                <div class="data_filter pull-left">
                    <a href="#" id="ratingFilter" data-url="<?=URL::site('privatecoach/gym/rating')?>"><span class="link"><span class="ls"><?=__('По рейтингу')?></span></span></a>
                    <a href="#" id="alphabetFilter"><span class="link"><span class="ls"><?=__('По алфавиту')?></span></span></a>
                </div>
                <div class="control_filter pull-right">
                    <a href="#" id="clearFilters" data-clear="<?=URL::site('')?>" class="link"><span class="ls"><?=__('Очистить фильтр')?></span></a>
                </div>
            </div>

            <ul class="unstyled gyms_list" data-url="<?= URL::site(I18n::$lang.'/privatecoach/gym/ajax_list') ?>">
            </ul>

        </div>
    </div>
<!--    <div class="aside">-->
<!--        <div class="banner">-->
<!--            --><?//= Request::factory('confederation/partner/request/banner')->execute()?>
<!--        </div>-->
<!--        --><?//=View::factory('social_networks')?>
<!--    </div>-->
</div>
<div id="advance_options" style="display: none" data-no-rating-hint="<?=__('Оценка отстуствует')?>" data-read-only=1></div>