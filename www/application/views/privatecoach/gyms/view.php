
<div class="row_fluid clearfix bg-white">
<div class="breadcrumbs">
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('Личный тренер')?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach/gym/list')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('Тренажерные залы') ?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach/gym/view/'.$model->id)?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=$model->title ?></span></a>
    </div>
</div>
    <div class="row_section">
        <div class="section">
            <div class="page_section">
                <h1><?=$model->title?></h1>
                <div class="figure_sections">
                    <?=$model->photo_s->html_img()?>
                </div>
                <div class="text-left"><?=$model->description?></div>
            </div>
            <div class="info_page_section">
                <em class="muted"><?= Helper::word_cases(Comments::get_count('Gym', $model->id), __('комментариев'), __('комментарий'), __('комментария')) ?></em>
                <em class="muted"><?= Helper::word_cases($model->views,__('просмотров'), __('просмотр'), __('просмотра'))?></em>

                <div class="social_block pull-right" id="social_block">
                    Yandex share?
                </div>
            </div>

            <div class="comment_no_border"><?=Comments::get_list($model->table_name(),$model->id)?></div>
        </div>

    </div>

    <div class="aside aside_sections aside_info_gym">

        <div class="hr_bottom">
            <div id="total_score" data-read-only="1" data-score="<?=$model->score?>"></div>
            <em class="muted"><?= Helper::word_cases($scores, __('проголосовавших'), __('проголосовавший'), __('проголосовавших')) ?></em>
        </div>

        <div class="hr_bottom">
            <div class="item">
                <?=$model->city->title?>
                <?=$model->address?>
            </div>
            <div class="item">
                <?=$model->telephone?>
            </div>
            <?if ($model->email):?>
                <div class="item">
                    <a href="mailto:<?=$model->email?>" class="link"><span class="ls"><?=$model->email?></span></a>
                </div>
            <?endif?>
        </div>
        <?if ($user):?>
        <div class="hr_bottom">
            <dl>
                <dt><?=__('Стоимость')?>:</dt>
                <dd><div class="star view" data-type="cost" data-id="<?=$model->id?>" data-score=<?=$score->cost_score?>></div></dd>
            </dl>
            <dl>
                <dt><?=__('Оснащение(тренажеры)')?>:</dt>
                <dd><div class="star view" data-type="trainers" data-id="<?=$model->id?>" data-score=<?=$score->trainers_score?>></div></dd>
            </dl>
            <dl>
                <dt><?=__('Оснащение(свободные веса)')?>:</dt>
                <dd><div class="star view" data-id="<?=$model->id?>" data-type="free_weights" data-score=<?=$score->free_weights_score?>></div></dd>
            </dl>
            <dl>
                <dt><?=__('Компетентность тренеров')?>:</dt>
                <dd><div class="star view" data-id="<?=$model->id?>" data-type="coach" data-score=<?=$score->coach_score?>></div></dd>
            </dl>
            <dl>
                <dt><?=__('Месторасположение')?>:</dt>
                <dd><div class="star view" data-id="<?=$model->id?>" data-type="location" data-score=<?=$score->location_score?>></div></dd>
            </dl>
            <div id="advance_options" style="display: none" data-url="<?=URL::site('privatecoach/gym/score')?>" data-no-rating-hint="<?=__('Оценка отстуствует')?>" data-read-only=0></div>
        </div>
        <?else:?>
            <div class="hr_bottom">
                <dl>
                    <dt><?=__('Стоимость')?>:</dt>
                    <dd><div class="star view" data-type="cost" data-target="#myModal" data-toggle="modal" data-id="<?=$model->id?>" data-score=0></div></dd>
                </dl>
                <dl>
                    <dt><?=__('Оснащение(тренажеры)')?>:</dt>
                    <dd><div class="star view" data-type="trainers" data-target="#myModal" data-toggle="modal" data-id="<?=$model->id?>" data-score=0>></div></dd>
                </dl>
                <dl>
                    <dt><?=__('Оснащение(свободные веса)')?>:</dt>
                    <dd><div class="star view" data-id="<?=$model->id?>" data-target="#myModal" data-toggle="modal" data-type="free_weights" data-score=0>></div></dd>
                </dl>
                <dl>
                    <dt><?=__('Компетентность тренеров')?>:</dt>
                    <dd><div class="star view" data-id="<?=$model->id?>" data-target="#myModal" data-toggle="modal" data-type="coach" data-score=0></div></dd>
                </dl>
                <dl>
                    <dt><?=__('Месторасположение')?>:</dt>
                    <dd><div class="star view" data-id="<?=$model->id?>" data-target="#myModal" data-toggle="modal" data-type="location" data-score=0></div></dd>
                </dl>
            </div>
            <div id="advance_options" style="display: none" data-url="<?=URL::site('privatecoach/gym/score')?>" data-no-rating-hint="<?=__('Оценка отстуствует')?>" data-read-only=1></div>
        <?endif?>

            <div class="map_gym" id="map" style="height: 300px"></div>

        <?= View::factory('social_networks') ?>
<!--        <div class="hr_top widget_social">-->
<!--            --><?//=__('Мероприятия в соцсетях')?>
<!--            <a href="https://www.facebook.com/ConfederatSport" title="facebook" class="icon_social i_fb">facebook</a>-->
<!--            <a href="https://twitter.com/ConfederSport" title="twitter" class="icon_social i_tw">twitter</a>-->
<!--            <a href="http://vk.com/confederationsport" title="vk" class="icon_social i_vk">vk</a>-->
<!--            <a href="https://www.youtube.com/channel/UCrcVvkXdSk2HsJmNGSF812g" title="youtube" class="icon_social i_yt">youtube</a>-->
<!--        </div>-->
    </div>
</div>
<div id="point" data-lng="<?=$model->lng?>" data-lat="<?=$model->lat?>"></div>


