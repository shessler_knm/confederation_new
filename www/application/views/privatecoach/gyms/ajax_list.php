<? foreach ($model as $row): ?>
    <li class="media">
        <div class="pull-left">
            <a href="<?= URL::site('privatecoach/gym/view/' . $row->id) ?>"><?= $row->photo_s->html_img(100, 100) ?></a>
        </div>
        <div class="media-body">
            <div class="media-heading">
                <div class="h5"><a href="<?= URL::site('privatecoach/gym/view/' . $row->id) ?>" class="link"><span
                            class="ls"><?= $row->title ?></span></a></div>
            </div>
            <div class="pull-right star view" data-score=<?=$row->score?>></div>

            <div class="media_text">
                <div><?= $row->city->title ?>, <?= $row->{'address_'.I18n::$lang} ?></div>
                <div><?= $row->telephone ?></div>
                <div><?=__('e-mail')?>: <?= $row->email ?></div>
<!--                <div><a href="--><?//=$row->site?><!--">--><?//=$row->site?><!--</a></div>-->
                <small><em>
                    <span class="ico_eye"><?= Helper::word_cases($row->views,'просмотров', 'просмотр', 'просмотра')?></span>
                    <span class="ico_comment"><?= Helper::word_cases(Comments::get_count('Gym', $row->id), 'комментариев', 'комментарий', 'комментария') ?></span>
                </em></small>
            </div>
        </div>
    </li>
<? endforeach ?>
    <? if ($amount >= 5): ?>
    <div class="hr_top pagination_ajax text-center">
        <a href="#" id="more" data-counter="1"
           data-url="<?= URL::site('privatecoach/gym/ajax_list') ?>" class="link"><span class="ls"><?=__('Показать еще') ?></span></a>
    </div>
<? endif ?>
