<?='<?xml version="1.0"?>'?>
<ymaps:ymaps xmlns:ymaps="http://maps.yandex.ru/ymaps/1.x" xmlns:repr="http://maps.yandex.ru/representation/1.x" xmlns:gml="http://www.opengis.net/gml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maps.yandex.ru/schemas/ymaps/1.x/ymaps.xsd">
    <ymaps:GeoObjectCollection>
        <gml:featureMembers>
                <ymaps:GeoObject>
                    <gml:name><?=$point->title?></gml:name>
                    <gml:description><?=strip_tags($point->description)?></gml:description>
                    <gml:Point>
                        <gml:pos><?=$point->lng?> <?=$point->lat?></gml:pos>
                    </gml:Point>
                </ymaps:GeoObject>
        </gml:featureMembers>
    </ymaps:GeoObjectCollection>
</ymaps:ymaps>