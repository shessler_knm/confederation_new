<div class="row_fluid clearfix bg-white">
<div class="breadcrumbs">
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?= URL::site(I18n::$lang . '/privatecoach') ?>" itemprop="url" class="link"><span itemprop="title"
        class="ls"><?= __('Личный тренер') ?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?= URL::site(I18n::$lang . '/privatecoach/exercise/list') ?>" itemprop="url" class="link"><span
        itemprop="title" class="ls"><?= __('Упражнения') ?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?= URL::site(I18n::$lang . '/privatecoach/exercise/view/' . $model->id) ?>" itemprop="url"
            class="link"><span itemprop="title" class="ls"><?= $model->title ?></span></a>
        </div>
    </div>
        <div class="row_section">
            <div class="section">
                <div class="page_section">
                    <h1><?= $model->title ?></h1>
                    <div class="media">
                        <!--                    <div class="figure_page pull-left">-->
                        <?//= $model->photo_s->html_img(300, null) ?><!--</div>-->
                        <div class="media_text">
                            <?= $model->text ?>
                        </div>
                    </div>
                    <div class="media_section page_section">
                        <? $i = 1 ?>
                        <div class="media_block">
                            <a href="<?= $firstImage->storage_id_s->url_image(480, 360) ?>" title=""
                                data-index="" class="open_fancybox">
                                <img class="big_preview_image" src="<?= $firstImage->storage_id_s->url_image(480, 360) ?>"/>
                            </a>
                        </div>
                        <ul class="unstyled inline media_list">
                            <? $i = 0 ?>
                            <? foreach ($images as $image): ?>
                            <li>
                                <a class="select_item" href="<?= $image->storage_id_s->url_image(480, 360) ?>"
                                    data-index="<?= $i ?>">
                                    <img src="<?= $image->storage_id_s->url_image(240, 180) ?>"/>
                                </a>
                            </li>
                            <? $i++ ?>
                            <? endforeach ?>
                        </ul>
                        <? if ($model->content): ?>
                        <div class="media media_video">
                            <div class="video_inner">
                                <iframe width="100%" height="315"
                                src="http://www.youtube.com/embed/<?= $model->content ?>" frameborder="0"
                                allowfullscreen></iframe>
                            </div>
                        </div>
                        <? endif ?>
                    </div>
                    <!--                <div class="bottom_info">-->
                    <!--                    --><? // foreach ($categories as $category): ?>
                    <!--                        <em class="muted">--><?//= $category ?><!--</em>-->
                    <!--                    --><? // endforeach ?>
                    <!---->
                    <!--                    <div class="social_block pull-right" id="social_block">-->
                    <!--                        Yandex share?-->
                    <!--                    </div>-->
                    <!--                </div>-->
                </div>
                <div class="info_page_section">
                    <!--                <em class="muted">--><?//= Date::textdate($model->date, 'd m') ?><!--</em>-->
                    <em class="muted"><?= Helper::word_cases(Comments::get_count('exercises', $model->id), __('комментариев'), __('комментарий'), __('комментария')) ?></em>
                    <em class="muted"><?= Helper::word_cases($model->views, __('просмотров'), __('просмотр'), __('просмотра')) ?></em>
                    <div class="pull-right">
                        <em class="muted"><?= Helper::word_cases($voters, __('проголосовавших'), __('проголосовавший'), __('проголосовавших')) ?></em>
                        <? if ($user): ?>
                        <div class="star view" data-id="<?= $model->id ?>" data-score="<?= $score ?>"></div>
                        <? else: ?>
                        <div class="star view" data-id="<?= $model->id ?>" data-score="<?= $score ?>" data-read-only=1
                        data-target="#myModal" data-toggle="modal"></div>
                        <? endif ?>
                    </div>
                </div>
                <?= Comments::get_list($model->table_name(), $model->id) ?>
            </div>
        </div>
        <div class="aside">
            <div class="banner">
                <?= Request::factory('confederation/partner/request/banner')->execute() ?>
            </div>
            <?= View::factory('social_networks') ?>
        </div>
    </div>
    <div id="advance_options" style="display: none" data-url="<?= URL::site('privatecoach/exercise/score') ?>"
    data-no-rating-hint="<?= __('Оценка отстуствует') ?>" data-read-only="false"></div>