<? if ($count): ?>

    <? foreach ($data as $row): ?>
        <li class="media program_back_img">
            <? if (isset($row['images_url'])): ?>
                <div class="pull-left">
                    <? foreach ($row['images_url'] as $image): ?>
                        <? if ($image): ?>
                            <a href="<?= URL::site('privatecoach/exercise/view/' . Arr::get($row, 'id')) ?>"
                               style="background-image: url('<?= $image ?>')">
                                <? /*= $row->photo_s->html_img(200, null, array('alt' => $row->title)) */ ?>
                            </a>
                            <? break; ?>
                        <? endif ?>
                    <? endforeach ?>
                </div>
            <? endif ?>
            <div class="media-body">
                <div class="media-heading">
                    <div class="h4">
                        <a href="<?= URL::site('privatecoach/exercise/view/' . Arr::get($row, 'id')) ?>" class="link">
                    <span class="ls">
                        <?= Arr::get($row, 'name') ?>
                    </span>
                        </a>
                    </div>
                </div>
                <div class="medium_text">
                    <?= Text::limit_chars(strip_tags(Arr::get($row, 'text')), 220) ?>
                </div>
                <small><em>
                    <span
                        class="data"> <?= Helper::word_cases(Comments::get_count('Exercise', Arr::get($row, 'id')), __('комментариев'), __('комментарий'), __('комментария')) ?></span>
                        <!--                        <span class="data"> 6 просмотров</span>-->
                    </em></small>

                <div class="clearfix">
                    <div class="star view" data-read-only=1
                         data-score=<?= array_key_exists(Arr::get($row, 'id'), $array) ? $array[Arr::get($row, 'id')] : 0 ?>></div>
                </div>
            </div>
        </li>
    <? endforeach ?>

    <!--    <a href="#back">Наверх</a>-->
    <?= $pagination ?>
<? else: ?>
    <?= __('По данному запросу ничего не найдено') ?>
<?endif ?>

<!--<div class="hr_top pagination_ajax text-center">
    <a href="#" class="link"><span class="ls"><?/*= __('Показать еще') */?></span></a>
</div>-->