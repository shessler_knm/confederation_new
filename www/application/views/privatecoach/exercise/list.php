<div class="row_fluid clearfix bg-white">
<div class="breadcrumbs">
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('Личный тренер')?></span></a>
    </div>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="bread_item">
        <a href="<?=URL::site(I18n::$lang.'/privatecoach/exercise/list')?>" itemprop="url" class="link"><span itemprop="title" class="ls"><?=__('Упражнения') ?></span></a>
    </div>
</div>
    <a name="back"></a>
    <h1><?= __('Упражнения') ?></h1>

    <form id="exercise_filter">
        <div class="filter_list_section filter_programm_section">
            <div class="clearfix">
                <div class="pull-left w200 w20">
                    <div class="filter_select mr20">
                        <div class="title_filter">&nbsp;</div>
                        <?= Form::select('muscle', $categories, array_key_exists('muscle',$query)?$query['muscle']:null, array('class' => 'custom_select muscles', 'id' => 'muscles')) ?>
                    </div>
                </div>
            </div>
            <div class="clearfix">
                <div class="pull-left filter_search_text">
                    <?= Form::input('search', array_key_exists('q',$query)?$query['q']:__('Поиск по тексту'), array('class' => 'filter_search', 'id' => 'search', 'data-default' => __('Поиск по тексту'))) ?>
                </div>
                <div class="control_filter pull-right">
                    <a href="#" id="clearFilters" data-clear="<?= URL::site('') ?>" class="link">
                        <span class="ls"><?= __('Очистить фильтр') ?></span>
                    </a>
                    <a href="#" id="submit" class='btn color'
                       data-url=<?= URL::site(I18n::$lang.'/privatecoach/exercise/ajax_list') ?>>
                        <?= __('Подобрать программу') ?>
                    </a>
                </div>
            </div>
        </div>
    </form>


    <div class="row_section">
        <div class="section">
            <ul class="unstyled exercise_list event_star_list news_list interview_list">

            </ul>
        </div>
    </div>
    <div class="aside">
        <!-- <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div> -->
        <?= View::factory('social_networks') ?>
    </div>
</div>
<div id="advance_options" style="display: none" data-no-rating-hint="<?= __('Оценка отстуствует') ?>"></div>
