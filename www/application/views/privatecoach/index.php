<? if ($user): ?>
    <? if ($top > 0): ?>
        <div class="top_week top_week_index">
            <div class="top_week_title">
                <h3><?= __('ТОП 5 недели') ?></h3>
                <a href="<?= URL::site('privatecoach/top/100') ?>" class="link"><span
                        class="ls"><?= __('ТОП 100 участников') ?></span></a>
            </div>
            <?= Request::factory(I18n::$lang.'/privatecoach/top/preview')->execute() ?>
        </div>
    <? endif ?>
    <? if ($program->loaded()): ?>
        <div class="title_program_index">
            <span class="h3"><a href="<?= URL::site('privatecoach/program/schedule') ?>" class="link"><span
                        class="ls"><?= $program->title ?></span></a></span>
            <span class="item"><?= $program->mission->title ?></span>
        <span class="item">
            <?= Date::textdate($program->start_date, 'd m') ?>
            -
            <?= Date::textdate($end_date, 'd m') ?>
        </span>
        </div>
        <div class="page_program page_program_index" ng-app="privatecoach" ng-controller="scheduleCtrl">
            <div lenta-calendar year="dtLenta.year" month="dtLenta.month" day="dtLenta.day" programs="programs"
                 logs="logs"></div>
        </div>
        <div class="link_my_program">
            <a href="<?=URL::site(i18n::$lang.'/privatecoach/user/album')?>" class="link"><span class="ls"><?= __('Добавить результат') ?></span></a>
<!--            <a href="#" class="link"><span class="ls">--><?//=__('Мои результаты')?><!--</span></a>-->
            <a href="<?= URL::site('privatecoach/program/edit/'. $program->id) ?>" class="link"><span
                    class="ls"><?= __('Изменить') ?></span></a>
        </div>
    <? endif ?>
<? endif ?>
<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <? if ($user): ?>
                <div class="event_privatecoach_section">
                    <div class="h3"><a href="<?= URL::site('privatecoach/feed/list') ?>" class="link"><span
                                class="ls"><?= __('Лента событий') ?></span></a></div>
                    <?= Request::factory(I18n::$lang.'/privatecoach/feed/main_preview')->execute() ?>
                </div>
            <? endif ?>
            <div class="news_section">
                <div class="h3"><a href="<?= URL::site('privatecoach/article/list') ?>" class="link"><span
                            class="ls"><?= __('Статьи') ?></span></a></div>
                <?= Request::factory(I18n::$lang.'/privatecoach/article/preview')->execute() ?>
            </div>

            <? if (!$user): ?>
                <!-- Программы тренировок-->
                <div class="programm_no_reg_user">
                    <div class="h3"><a href="<?= URL::site('privatecoach/program/list') ?>" class="link"><span
                                class="ls"><?= __('Программы тренировок') ?></span></a></div>
                    <?= Request::factory(I18n::$lang.'/privatecoach/program/main_preview')->execute() ?>
                </div>
                <? if ($top > 0): ?>
                    <div class="top_week top_week_index top_week_no_reg_user">
                        <div class="top_week_title">
                            <h3><?= __('ТОП 5 недели') ?></h3>
                            <a href="<?= URL::site('privatecoach/top/100') ?>" class="link"><span
                                    class="ls"><?= __('ТОП 100 участников') ?></span></a>
                        </div>
                        <?= Request::factory(I18n::$lang.'/privatecoach/top/preview')->execute() ?>
                    </div>
                <? endif ?>
            <? endif ?>

        </div>
    </div>
    <div class="aside">
        <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div>
        <?= View::factory('social_networks') ?>
    </div>
</div>
<div id="own_program" data-own-profile="1"></div>
<?= View::factory('privatecoach/program/exercise_promt')->render() ?>
<div id="hint" data-hint="<?= __('Нажмите, чтобы посмотреть описание') ?>" style="display: none"></div>