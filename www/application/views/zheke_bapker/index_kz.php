<!DOCTYPE html>
<html>
<head>
    <title>Жеке Бапкер</title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">

    <meta property="og:image" content="media/img/conf_logo.png">
    <meta itemprop="image" content="media/img/conf_logo.png">
    <link rel="image_src" href="../../../zheke_bapker_kz/media/img/conf_logo.png">

    <link rel="shortcut icon" href="../../../zheke_bapker_kz/favicon.png" type="image/x-icon">
    <link rel="icon" href="/favicon.ico">
    <link rel="canonical" href="#" />

    <script type="text/javascript" src="../../../zheke_bapker_kz/media/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="../../../zheke_bapker_kz/media/js/jquery.nicescroll.js"></script>
    <script type="text/javascript" src="../../../zheke_bapker_kz/media/js/jquery.viewportchecker.js"></script>
    <script type="text/javascript" src="../../../zheke_bapker_kz/media/js/index.js"></script>

    <link rel="stylesheet" href="../../../zheke_bapker_kz/media/css/font-opensans.css"/>
    <link rel="stylesheet" href="../../../zheke_bapker_kz/media/css/animations.css"/>
    <link rel="stylesheet" href="../../../zheke_bapker_kz/media/css/style.css" type="text/css">
    <link rel="stylesheet" href="../../../zheke_bapker_kz/media/css/responsive.css" type="text/css">


</head>
<body class="kz_vers">

<!-- page1 with content-->
<section class="parallax page1">
    <a href="http://confederation.kz/" class="conf_logo" target="_blank">
        <img alt="Спорттық жекпе-жек және күш қолданылатын спорт түрлері Конфедерациясы" src="../../../zheke_bapker_kz/media/img/conf_logo.png">
        <span class="">Спорттық жекпе-жек және күш қолданылатын спорт түрлері Конфедерациясы
        </span>
    </a>

    <ul class="langs">
        <li class="active"><a href="/zheke-bapker">Каз</a></li>
        <li><a href="<?=URL::site('/ru/zheke-bapker')?>">Рус</a></li>
        <li><a href="<?=URL::site('/en/zheke-bapker')?>">Eng</a></li>
    </ul>

    <!--  info slider  -->
    <div class="info_box_page1">
        <!-- слайд будет показывать дефолтно слайд с <input type="radio" checked> -->
        <!--
        слайдер на  CSS
        каждый слайд завязан на отработку radio.
        у каждого слайда свой класс для label - для иконки, ее расположения и анимации, и сопуствующего текста.
       - - - -
        <label>
        <IMG> - в телефоне меняется эта картинка
        <div class="slider_text_box">

         размер изображения: 217х383
         в стилях размер картинки зашит.

         сейчас стоят тестовые изображения:
         которые поменять на требуемые
        - - - -
        -->


        <!--unit firt - с дефолтной заставкой - лого ЛТ
        каз версия  <img src="media/img/page1_sl_main_kz.png
        рус версия  <img src="media/img/page1_sl_main_ru.png
        англ версия  <img src="media/img/page1_sl_main_en.png
        -->
        <div class="slide">
            <input type="radio" name="slider_switch" id="slide1" checked>
            <label class="start" for="slide1"></label>
            <img src="../../../zheke_bapker_kz/media/img/page1_sl_main_kz.png" alt="Жеке бапкер" class="slider_img">
            <div class="slider_text_box">
                <h1 class="fadeInDowntit">Жеке бапкер</h1>
                <div class="slider_text fadeInDowntext">
                    Сізді спортпен шұғылданып, үнемі бабыңызда жүруге шақыратын  «Жеке бапкер» айдары өз жетістігіңізді достарыңызбен бөлісуге, салауатты өмір салты мен дұрыс тамақтану турасында мол ақпарат алуға, пайдалы жаттығу бағдарламасын дайындауға көмектеседі
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->


        <!--unit -->
        <div class="slide">
            <input type="radio" name="slider_switch" id="slide2">
            <label class="training" for="slide2">
                <img src="../../../zheke_bapker_kz/media/img/page1_icon_training.png" alt="" class="slideLeft">
            </label>
            <img src="../../../zheke_bapker_kz/media/img/page1_kz_slider_1treni.png" alt="Жаттығулар" class="slider_img">
            <div class="slider_text_box">
                <h2 class="fadeInDowntit">Жаттығулар</h2>
                <div class="slider_text fadeInDowntext">
                     Бұлшық еттер тобын шынықтыруға арналған алуан түрлі жаттығулар жинағы мен алға қойған мақсатыңызға жетуге көмектесетін түрлі жаттығулар кестесі
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->

        <!--unit -->
        <div class="slide">
            <input type="radio" name="slide_switch" id="slide3"/>
            <label class="pedometer" for="slide3">
                <img src="../../../zheke_bapker_kz/media/img/page1_icon_pedometer.png" alt="" class="slideLeft2">
            </label>
            <img src="../../../zheke_bapker_kz/media/img/page1_kz_slider_2shagi.png" alt="Адым, қадам өлшеуіш" class="slider_img">
            <div class="slider_text_box">
                <h2 class="fadeInDowntit">Адым, қадам өлшеуіш </h2>
                <div class="slider_text fadeInDowntext">
                    Жүріп өткен қашықтықты, жылдамдық пен жаяу немесе велосипедпен жүргендегі шығындалған энергияны өлшейтін интерактивті құрал
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->

        <!--unit -->
        <div class="slide">
            <input type="radio" name="slide_switch" id="slide4" />
            <label class="diary" for="slide4">
                <img src="../../../zheke_bapker_kz/media/img/page1_icon_diary.png" alt="" class="slideLeft3">
            </label>
            <img src="../../../zheke_bapker_kz/media/img/page1_kz_slider_5diary.png" alt="Күнделік" class="slider_img diary_img">
            <div class="slider_text_box">
                <h2 class="fadeInDowntit">Күнделік </h2>
                <div class="slider_text fadeInDowntext">
                   Спортпен шұғылданған кездегі есеп және қажетті калориялар статистикасы
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->

        <!--unit -->
        <div class="slide">
            <input type="radio" name="slide_switch" id="slide5"/>
            <label class="food" for="slide5">
                <img src="../../../zheke_bapker_kz/media/img/page1_icon_food.png" alt="" class="slideLeft">
            </label>
            <img src="../../../zheke_bapker_kz/media/img/page1_kz_slider_3pitan.png" alt="Тағам" class="slider_img food_img">
            <div class="slider_text_box">
                <h2 class="fadeInDowntit">Тағам  </h2>
                <div class="slider_text fadeInDowntext">
                    Дұрыс тамақтануға арналған дайын ас мәзірлері мен күнделікті мәзірге арналған, калория көлемін анықтайтын құрам
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->

        <!--unit -->
        <div class="slide">
            <input type="radio" name="slide_switch" id="slide6" />
            <label class="pulse" for="slide6">
                <img src="../../../zheke_bapker_kz/media/img/page1_icon_pulse.png" alt="" class="slideLeft2">
            </label>
            <img src="../../../zheke_bapker_kz/media/img/page1_kz_slider_4puls.png" alt="Тамырдың соғуын өлшеуiш" class="slider_img pulse_img">
            <div class="slider_text_box">
                <h2 class="fadeInDowntit">Тамырдың соғуын өлшеуiш</h2>
                <div class="slider_text fadeInDowntext">
                    Түрлі жағдайлар кезінде қазіргі тамырдың соғуын өлшеу
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->

        <!--unit -->
        <div class="slide">
            <input type="radio" name="slide_switch" id="slide7"/>
            <label class="playlist" for="slide7">
                <img src="../../../zheke_bapker_kz/media/img/page1_icon_playlist.png" alt="" class="slideLeft3">
            </label>
            <img src="../../../zheke_bapker_kz/media/img/page1_kz_slider_6play.png" alt="Плейлист" class="slider_img playlist_img">
            <div class="slider_text_box">
                <h2 class="fadeInDowntit">Плейлист </h2>
                <div class="slider_text fadeInDowntext">
                    Жаттығу кезінде көмектесетін сіздің жеке плейлистіңіз
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->

    </div>
    <!--  /info slider  -->


</section><!-- /page1-->


<div class="wrapper">
    <div class="parallax page1_simulator"></div>


    <section class="intro_box">
        <div class="intro_content">
              <span class="intro_text">
                  ТЕГІН ЖҮКТЕП АЛУ
              </span>

                <span class="intro_links">
                    <span>Смартфонға арналған қосымша (iOS және Android)</span>
                     <a href="https://itunes.apple.com/us/app/licnyj-trener/id938673136?mt=8" target="_blank"><img src="../../../zheke_bapker_kz/media/img/btn_apple_kaz.png" alt=""></a>
                  <a href="https://play.google.com/store/apps/details?id=kz.simplecode.confederation"target="_blank"><img src="../../../zheke_bapker_kz/media/img/btn_google_kaz.png" alt=""></a>
                </span>
        </div><!-- /intro_content -->
    </section><!-- /inter-->



    <section class="parallax page2">
    <div class="page_2_bg"></div>


        <!-- слайд будет показывать дефолтно слайд с <input type="radio" checked> -->
        <!--
        слайдер на  CSS
        каждый слайд завязан на отработку radio.
        у каждого слайда свой класс для label - для иконки, ее расположения и анимации, и сопуствующего текста.
       - - - -
        <label>
        <IMG> - в телефоне меняется эта картинка
        <div class="slider_text_box">

         размер изображения: 246х436
         в стилях размер картинки зашит.

         сейчас стоят тестовые изображения:
           page2_sl_1.png,
           page2_sl_2.png,
           которые поменять на требуемые
        - - - -
        -->


        <div class="info_box_2">

            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_1"  checked>
                <label class="training" for="slide2_1"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_kz_slider_1treni.png" alt="Жаттығулар" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Жаттығулар</h2>
                    <div class="slider_text fadeInRight">
                        Бұлшық еттер тобын шынықтыруға арналған алуан түрлі жаттығулар жинағы мен алға қойған мақсатыңызға жетуге көмектесетін түрлі жаттығулар кестесі
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->

            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_2">
                <label class="pedometer" for="slide2_2"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_kz_slider_2shagi.png" alt="Адым, қадам өлшеуіш " class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Адым, қадам өлшеуіш </h2>
                    <div class="slider_text fadeInRight">
                        Жүріп өткен қашықтықты, жылдамдық пен жаяу немесе велосипедпен жүргендегі шығындалған энергияны өлшейтін интерактивті құрал
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->

            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_3">
                <label class="food" for="slide2_3"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_kz_slider_3pitan.png" alt="Тағам" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Тағам </h2>
                    <div class="slider_text fadeInRight">
                        Дұрыс тамақтануға арналған дайын ас мәзірлері мен күнделікті мәзірге арналған, калория көлемін анықтайтын құрам
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->


            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_4">
                <label class="pulse" for="slide2_4"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_kz_slider_4puls.png" alt="Тамырдың соғуын өлшеуiш" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Тамырдың соғуын өлшеуiш</h2>
                    <div class="slider_text fadeInRight">
                       Түрлі жағдайлар кезінде қазіргі тамырдың соғуын өлшеу
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->


            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_5">
                <label class="diary" for="slide2_5"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_kz_slider_5diary.png" alt="Күнделік" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Күнделік</h2>
                    <div class="slider_text fadeInRight">
                        Спортпен шұғылданған кездегі есеп және қажетті калориялар статистикасы
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->


            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_6">
                <label class="playlist" for="slide2_6"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_kz_slider_6play.png" alt="Плейлист" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Плейлист</h2>
                    <div class="slider_text fadeInRight">
                       Жаттығу кезінде көмектесетін сіздің жеке плейлистіңіз
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->


            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_7">
                <label class="articles" for="slide2_7"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_kz_slider_7articles.png" alt="Мақалалар" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Мақалалар</h2>
                    <div class="slider_text fadeInRight">
                        Салауатты өмір салты, фитнес, тамақтану мен тағам қоспалары туралы үнемі жаңарып тұратын мақалалар жинағы
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->

            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_8">
                <label class="ribbon " for="slide2_8"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_kz_slider_8lenta.png" alt="Жаңалықтар таспасы" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Жаңалықтар таспасы</h2>
                    <div class="slider_text fadeInRight">
                       Достарыңыздың жетістіктері көрінетін тізбе арқылы өз әлеуетіңізді салыстыра аласыз
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->

            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_9">
                <label class="users" for="slide2_9"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_kz_slider_9users.png" alt="Пайдаланушылар" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Пайдаланушылар </h2>
                    <div class="slider_text fadeInRight">
                       Достар қатарына қосуға болатын қолданушылар парағын іздеу және көру
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->



            <div class="download_box">
              <h3>СМАРТФОНҒА ТЕГІН ЖҮКТЕП АЛУ</h3>

                <a href="https://itunes.apple.com/us/app/licnyj-trener/id938673136?mt=8" target="_blank"><img src="../../../zheke_bapker_kz/media/img/btn_apple_kaz.png" alt=""></a>
                <a href="https://play.google.com/store/apps/details?id=kz.simplecode.confederation"target="_blank"><img src="../../../zheke_bapker_kz/media/img/btn_google_kaz.png" alt=""></a>
            </div>

        </div>







        <footer class="footer">
            <div class="footer_content">
              <span class="footer_text">
                  ТИТУЛДЫ ДЕМЕУШІЛЕР
              </span>

                <span class="footer_links">
                    <a href="http://sk.kz/"><img src="../../../zheke_bapker_kz/media/img/sk_logo.png" alt=""></a>
                    <a href="http://www.kmg.kz/"><img src="../../../zheke_bapker_kz/media/img/kmg_logo.png" alt=""></a>
                </span>

                <a href="http://kaznetmedia.kz/" class="knm">kaznetmedia. воплощаем идеи</a>
            </div><!-- /intro_content -->
        </footer>
    </section><!-- /page2-->

</div><!-- /wrapper-->
</body>
</html>