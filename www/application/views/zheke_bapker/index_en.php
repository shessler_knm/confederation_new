<!DOCTYPE html>
<!-- saved from url=(0036)http://zheke-bapker.kz/index_en.html -->
<html style="overflow: hidden;"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Private Coach</title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">

    <meta property="og:image" content="media/img/conf_logo.png">
    <meta itemprop="image" content="media/img/conf_logo.png">
    <link rel="image_src" href="../../../zheke_bapker_kz/media/img/conf_logo.png">

    <link rel="shortcut icon" href="http://zheke-bapker.kz/favicon.ico">
    <link rel="shortcut icon" href="http://zheke-bapker.kz/favicon.png" type="image/x-icon">
    <link rel="canonical" href="http://zheke-bapker.kz/index_en.html#">

    <script type="text/javascript" src="../../../zheke_bapker_kz/media/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="../../../zheke_bapker_kz/media/js/jquery.nicescroll.js"></script>
    <script type="text/javascript" src="../../../zheke_bapker_kz/media/js/jquery.viewportchecker.js"></script>
    <script type="text/javascript" src="../../../zheke_bapker_kz/media/js/index.js"></script>

    <link rel="stylesheet" href="../../../zheke_bapker_kz/media/css/font-opensans.css">
    <link rel="stylesheet" href="../../../zheke_bapker_kz/media/css/animations.css">
    <link rel="stylesheet" href="../../../zheke_bapker_kz/media/css/style.css" type="text/css">
    <link rel="stylesheet" href="../../../zheke_bapker_kz/media/css/responsive.css" type="text/css">


</head>
<body>

<!-- page1 with content-->
<section class="parallax page1">
    <a href="http://confederation.kz/" class="conf_logo" target="_blank">
        <img alt="Combat and strength sports Confederation" src="../../../zheke_bapker_kz/media/img/conf_logo.png">
        <span class="">Combat and strength sports Confederation
        </span>
    </a>

    <ul class="langs">
        <li><a href="/zheke-bapker">Каз</a></li>
        <li><a href="<?=URL::site('/ru/zheke-bapker')?>">Рус</a></li>
        <li class="active"><a href="<?=URL::site('/en/zheke-bapker')?>">Eng</a></li>
    </ul>

    <!--  info slider  -->
    <div class="info_box_page1">
        <!-- слайд будет показывать дефолтно слайд с <input type="radio" checked> -->
        <!--
        слайдер на  CSS
        каждый слайд завязан на отработку radio.
        у каждого слайда свой класс для label - для иконки, ее расположения и анимации, и сопуствующего текста.
       - - - -
        <label>
        <IMG> - в телефоне меняется эта картинка
        <div class="slider_text_box">

         размер изображения: 217х383
         в стилях размер картинки зашит.

         сейчас стоят тестовые изображения:
         которые поменять на требуемые
        - - - -
        -->


        <!--unit firt - с дефолтной заставкой - лого ЛТ
        каз версия  <img src="media/img/page1_sl_main_kz.png
        рус версия  <img src="media/img/page1_sl_main_ru.png
        англ версия  <img src="media/img/page1_sl_main_en.png
        -->
        <div class="slide">
            <input type="radio" name="slider_switch" id="slide1" checked="">
            <label class="start" for="slide1"></label>
            <img src="../../../zheke_bapker_kz/media/img/page1_sl_main_en.png" alt="" class="slider_img">
            <div class="slider_text_box">
                <h1 class="fadeInDowntit">Private coach</h1>
                <div class="slider_text fadeInDowntext">
                   For your sports training and conditioning, helps you to follow your progress and share it with your friends, you can learn more about healthy lifestyle and nutrition, draw up your own effective training program and many others
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->


        <!--unit -->
        <div class="slide">
            <input type="radio" name="slider_switch" id="slide2">
            <label class="training" for="slide2">
                <img src="../../../zheke_bapker_kz/media/img/page1_icon_training.png" alt="" class="slideLeft">
            </label>
            <img src="media/imgpage1_en_slider_1treni.png" alt="Training programs" class="slider_img">
            <div class="slider_text_box">
                <h2 class="fadeInDowntit">Training programs</h2>
                <div class="slider_text fadeInDowntext">
                    The set of various programmes for training of different muscle groups with an opportunity for developing programmes with defined class schedule to achieve the goals set
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->

        <!--unit -->
        <div class="slide">
            <input type="radio" name="slide_switch" id="slide3">
            <label class="pedometer" for="slide3">
                <img src="../../../zheke_bapker_kz/media/img/page1_icon_pedometer.png" alt="" class="slideLeft2">
            </label>
            <img src="../../../zheke_bapker_kz/media/img/page1_en_slider_2shagi.png" alt="Pedometer" class="slider_img pedometer_img">
            <div class="slider_text_box">
                <h2 class="fadeInDowntit">Pedometer </h2>
                <div class="slider_text fadeInDowntext">
                    Interactive pedometer for calculating the distance traveled, speed and extended energy on foot or on bicycle
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->

        <!--unit -->
        <div class="slide">
            <input type="radio" name="slide_switch" id="slide4">
            <label class="diary" for="slide4">
                <img src="../../../zheke_bapker_kz/media/img/page1_icon_diary.png" alt="" class="slideLeft3">
            </label>
            <img src="../../../zheke_bapker_kz/media/img/page1_en_slider_5diary.png" alt="Diary" class="slider_img diary_img">
            <div class="slider_text_box">
                <h2 class="fadeInDowntit">Diary </h2>
                <div class="slider_text fadeInDowntext">
                    A summary of all your sports activities, statistics of calories consumed
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->

        <!--unit -->
        <div class="slide">
            <input type="radio" name="slide_switch" id="slide5">
            <label class="food" for="slide5">
                <img src="../../../zheke_bapker_kz/media/img/page1_icon_food.png" alt="" class="slideLeft">
            </label>
            <img src="../../../zheke_bapker_kz/media/img/page1_en_slider_3pitan.png" alt="Diet" class="slider_img food_img">
            <div class="slider_text_box">
                <h2 class="fadeInDowntit">Diet </h2>
                <div class="slider_text fadeInDowntext">
                  Menu for healthy food with the list of ingredients for composing the daily ration with calories calculation
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->

        <!--unit -->
        <div class="slide">
            <input type="radio" name="slide_switch" id="slide6">
            <label class="pulse" for="slide6">
                <img src="../../../zheke_bapker_kz/media/img/page1_icon_pulse.png" alt="" class="slideLeft2">
            </label>
            <img src="../../../zheke_bapker_kz/media/img/page1_en_slider_4puls.png" alt="Pulse diagnostics" class="slider_img pulse_img">
            <div class="slider_text_box">
                <h2 class="fadeInDowntit">Pulse diagnostics</h2>
                <div class="slider_text fadeInDowntext">
                    Heart rate measurement for tracking the present state in various conditions
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->

        <!--unit -->
        <div class="slide">
            <input type="radio" name="slide_switch" id="slide7">
            <label class="playlist" for="slide7">
                <img src="../../../zheke_bapker_kz/media/img/page1_icon_playlist.png" alt="" class="slideLeft3">
            </label>
            <img src="../../../zheke_bapker_kz/media/img/page1_en_slider_6play.png" alt="Playlist" class="slider_img playlist_img">
            <div class="slider_text_box">
                <h2 class="fadeInDowntit">Playlist </h2>
                <div class="slider_text fadeInDowntext">
                    Your individual playlist with the music with tone maintaining during training
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->

    </div>
    <!--  /info slider  -->


</section><!-- /page1-->


<div class="wrapper">
    <div class="parallax page1_simulator"></div>


    <section class="intro_box">
        <div class="intro_content">
              <span class="intro_text">
                  DOWNLOAD FREE
              </span>

                <span class="intro_links">
                    <span>Application for iOS and Android-based devices</span>
                    <a href="https://itunes.apple.com/us/app/licnyj-trener/id938673136?mt=8" target="_blank"><img src="../../../zheke_bapker_kz/media/img/btn_apple_eng2.png" alt=""></a>
                    <a href="https://play.google.com/store/apps/details?id=kz.simplecode.confederation"target="_blank"><img src="../../../zheke_bapker_kz/media/img/btn_google_eng2.png" alt=""></a>
                </span>
        </div><!-- /intro_content -->
    </section><!-- /inter-->



    <section class="parallax page2">
    <div class="page_2_bg"></div>


        <!-- слайд будет показывать дефолтно слайд с <input type="radio" checked> -->
        <!--
        слайдер на  CSS
        каждый слайд завязан на отработку radio.
        у каждого слайда свой класс для label - для иконки, ее расположения и анимации, и сопуствующего текста.
       - - - -
        <label>
        <IMG> - в телефоне меняется эта картинка
        <div class="slider_text_box">

         размер изображения: 246х436
         в стилях размер картинки зашит.

         сейчас стоят тестовые изображения:
           page2_sl_1.png,
           page2_sl_2.png,
           которые поменять на требуемые
        - - - -
        -->


        <div class="info_box_2">

            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_1" checked="">
                <label class="training" for="slide2_1"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_en_slider_1treni.png" alt="Training programs" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Training programs</h2>
                    <div class="slider_text fadeInRight">
                        The set of various programmes for training of different muscle groups with an opportunity for developing programmes with defined class schedule to achieve the goals set
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->

            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_2">
                <label class="pedometer" for="slide2_2"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_en_slider_2shagi.png" alt="Pedometer" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Pedometer</h2>
                    <div class="slider_text fadeInRight">
                        Interactive pedometer for calculating the distance traveled, speed and extended energy on foot or on bicycle
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->

            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_3">
                <label class="food" for="slide2_3"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_en_slider_3pitan.png" alt="Diet" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Diet</h2>
                    <div class="slider_text fadeInRight">
                        Menu for healthy food with the list of ingredients for composing the daily ration with calories calculation
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->


            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_4">
                <label class="pulse" for="slide2_4"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_en_slider_4puls.png" alt="Pulse diagnostics" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Pulse diagnostics</h2>
                    <div class="slider_text fadeInRight">
                        Heart rate measurement for tracking the present state in various conditions
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->


            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_5">
                <label class="diary" for="slide2_5"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_en_slider_5diary.png" alt="Diary" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Diary</h2>
                    <div class="slider_text fadeInRight">
                        A summary of all your sports activities, statistics of calories consumed
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->


            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_6">
                <label class="playlist" for="slide2_6"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_en_slider_6play.png" alt="Playlist" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Playlist</h2>
                    <div class="slider_text fadeInRight">
                        Your individual playlist with the music with tone maintaining during training
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->


            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_7">
                <label class="articles" for="slide2_7"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_en_slider_7articles.png" alt="Articles" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Articles</h2>
                    <div class="slider_text fadeInRight">
                        Constantly enlarged collection of articles on healthy life, fitness, nutrition and food additives
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->

            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_8">
                <label class="ribbon " for="slide2_8"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_en_slider_8lenta.png" alt="News feed" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">News feed</h2>
                    <div class="slider_text fadeInRight">
                       Tape of your friends achievements, which can be compared with your success
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->

            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_9">
                <label class="users" for="slide2_9"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_en_slider_9users.png" alt="Users" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Users </h2>
                    <div class="slider_text fadeInRight">
                       Search and preview of profiles of users, who could be added to friends list
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->



            <div class="download_box">
              <h3>DOWNLOAD FREE TO YOUR DEVICE</h3>

                 <a href="https://itunes.apple.com/us/app/licnyj-trener/id938673136?mt=8" target="_blank"><img src="../../../zheke_bapker_kz/media/img/btn_apple_eng2.png" alt=""></a>
                 <a href="https://play.google.com/store/apps/details?id=kz.simplecode.confederation" target="_blank"><img src="../../../zheke_bapker_kz/media/img/btn_google_eng2.png" alt=""></a>
            </div>

        </div>







        <footer class="footer">
            <div class="footer_content">
              <span class="footer_text">
                  TITLE SPONSORS
              </span>

                <span class="footer_links">
                    <a href="http://sk.kz/"><img src="../../../zheke_bapker_kz/media/img/sk_logo.png" alt=""></a>
                    <a href="http://www.kmg.kz/"><img src="../../../zheke_bapker_kz/media/img/kmg_logo.png" alt=""></a>
                </span>

                <a href="http://kaznetmedia.kz/" class="knm">kaznetmedia. воплощаем идеи</a>
            </div><!-- /intro_content -->
        </footer>
    </section><!-- /page2-->

</div><!-- /wrapper-->

<div id="ascrail2000" class="nicescroll-rails nicescroll-rails-vr" style="width: 16px; z-index: 1000; cursor: default; position: fixed; top: 0px; height: 100%; right: 0px; opacity: 0;"><div class="nicescroll-cursors" style="position: relative; top: 0px; float: right; width: 14px; height: 353px; border: 1px solid rgb(168, 168, 168); border-radius: 5px; background-color: rgb(188, 188, 188); background-clip: padding-box;"></div></div><div id="ascrail2000-hr" class="nicescroll-rails nicescroll-rails-hr" style="height: 16px; z-index: 1000; position: fixed; left: 0px; width: 100%; bottom: 0px; cursor: default; display: none; opacity: 0;"><div class="nicescroll-cursors" style="position: absolute; top: 0px; height: 14px; width: 1600px; border: 1px solid rgb(168, 168, 168); border-radius: 5px; background-color: rgb(188, 188, 188); background-clip: padding-box;"></div></div></body></html>