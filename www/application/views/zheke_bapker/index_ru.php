<!DOCTYPE html>
<html>
<head>
    <title>Личный тренер</title>
    <meta charset="UTF-8">
    <meta name="description" content="Ваш «Личный тренер» для занятий спортом и поддержания физической формы поможет вам следить за своим прогрессом и делиться им со своими друзьями, узнавать много нового о здоровом образе жизни и полезном питании, составлять собственную эффективную программу тренировок и многое другое">
    <meta name="keywords" content="">
    <meta property="og:title" content="Мобильное приложение 'Личный тренер">
    <meta property="og:description" content="Ваш «Личный тренер» для занятий спортом и поддержания физической формы поможет вам следить за своим прогрессом и делиться им со своими друзьями, узнавать много нового о здоровом образе жизни и полезном питании, составлять собственную эффективную программу тренировок и многое другое">
	

    <meta property="og:image" content="media/img/conf_logo.png">
    <meta itemprop="image" content="media/img/conf_logo.png">
    <link rel="image_src" href="../../../zheke_bapker_kz/media/img/conf_logo.png">

    <link rel="shortcut icon" href="../../../zheke_bapker_kz/favicon.png" type="image/x-icon">
    <link rel="icon" href="/favicon.ico">
    <link rel="canonical" href="#" />

    <script type="text/javascript" src="../../../zheke_bapker_kz/media/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="../../../zheke_bapker_kz/media/js/jquery.nicescroll.js"></script>
    <script type="text/javascript" src="../../../zheke_bapker_kz/media/js/jquery.viewportchecker.js"></script>
    <script type="text/javascript" src="../../../zheke_bapker_kz/media/js/index.js"></script>

    <link rel="stylesheet" href="../../../zheke_bapker_kz/media/css/font-opensans.css"/>
    <link rel="stylesheet" href="../../../zheke_bapker_kz/media/css/animations.css"/>
    <link rel="stylesheet" href="../../../zheke_bapker_kz/media/css/style.css" type="text/css">
    <link rel="stylesheet" href="../../../zheke_bapker_kz/media/css/responsive.css" type="text/css">


</head>
<body>

<!-- page1 with content-->
<section class="parallax page1">
    <a href="http://confederation.kz/" class="conf_logo" target="_blank">
        <img alt=" Конфедерация спортивных единоборств и силовых видов спорта" src="../../../zheke_bapker_kz/media/img/conf_logo.png">
        <span class="">Конфедерация спортивных единоборств<br>
              и силовых видов спорта
        </span>
    </a>


    <ul class="langs">
        <li><a href="/zheke-bapker">Каз</a></li>
        <li class="active"><a href="<?=URL::site('/ru/zheke-bapker')?>">Рус</a></li>
        <li><a href="<?=URL::site('/en/zheke-bapker')?>">Eng</a></li>
    </ul>


    <!--  info slider  -->
    <div class="info_box_page1">
        <!-- слайд будет показывать дефолтно слайд с <input type="radio" checked> -->
        <!--
        слайдер на  CSS
        каждый слайд завязан на отработку radio.
        у каждого слайда свой класс для label - для иконки, ее расположения и анимации, и сопуствующего текста.
       - - - -
        <label>
        <IMG> - в телефоне меняется эта картинка
        <div class="slider_text_box">

         размер изображения: 217х383
         в стилях размер картинки зашит.

         сейчас стоят тестовые изображения:
         которые поменять на требуемые
        - - - -
        -->


        <!--unit firt - с дефолтной заставкой - лого ЛТ
        каз версия  <img src="media/img/page1_sl_main_kz.png
        рус версия  <img src="media/img/page1_sl_main_ru.png
        англ версия  <img src="media/img/page1_sl_main_en.png
        -->
        <div class="slide">
            <input type="radio" name="slider_switch" id="slide1" checked>
            <label class="start" for="slide1"></label>
            <img src="../../../zheke_bapker_kz/media/img/page1_sl_main_ru.png" alt="" class="slider_img">
            <div class="slider_text_box">
                <h1 class="fadeInDowntit">Личный тренер</h1>
                <div class="slider_text fadeInDowntext">
                    Ваш «Личный тренер» для занятий спортом и поддержания физической формы поможет вам следить за своим прогрессом и делиться им со своими друзьями, узнавать много нового о здоровом образе жизни и полезном питании, составлять собственную эффективную программу тренировок и многое другое
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->


        <!--unit -->
        <div class="slide">
            <input type="radio" name="slider_switch" id="slide2">
            <label class="training" for="slide2">
                <img src="../../../zheke_bapker_kz/media/img/page1_icon_training.png" alt="" class="slideLeft">
            </label>
            <img src="../../../zheke_bapker_kz/media/img/page1_slider_1treni.png" alt="Тренировки" class="slider_img">
            <div class="slider_text_box">
                <h2 class="fadeInDowntit">Тренировки</h2>
                <div class="slider_text fadeInDowntext">
                    Набор разнообразных программ
                    для тренировок на различные группы мышц
                    и возможностью создания собственных программ с заданным расписанием занятий
                    для достижения поставленных целей
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->

        <!--unit -->
        <div class="slide">
            <input type="radio" name="slide_switch" id="slide3"/>
            <label class="pedometer" for="slide3">
                <img src="../../../zheke_bapker_kz/media/img/page1_icon_pedometer.png" alt="" class="slideLeft2">
            </label>
            <img src="../../../zheke_bapker_kz/media/img/page1_slider_2shagi.png" alt="Шагомер" class="slider_img">
            <div class="slider_text_box">
                <h2 class="fadeInDowntit">Шагомер </h2>
                <div class="slider_text fadeInDowntext">
                    Интерактивный шагомер для подсчёта пройденного расстояния, скорости и израсходованной энергии пешком или на велосипеде
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->

        <!--unit -->
        <div class="slide">
            <input type="radio" name="slide_switch" id="slide4" />
            <label class="diary" for="slide4">
                <img src="../../../zheke_bapker_kz/media/img/page1_icon_diary.png" alt="" class="slideLeft3">
            </label>
            <img src="../../../zheke_bapker_kz/media/img/page1_slider_5diary.png" alt="Дневник" class="slider_img">
            <div class="slider_text_box">
                <h2 class="fadeInDowntit">Дневник </h2>
                <div class="slider_text fadeInDowntext">
                    Сводка всех ваших действий по занятию спортом, а также статистика потребленных калорий
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->

        <!--unit -->
        <div class="slide">
            <input type="radio" name="slide_switch" id="slide5"/>
            <label class="food" for="slide5">
                <img src="../../../zheke_bapker_kz/media/img/page1_icon_food.png" alt="" class="slideLeft">
            </label>
            <img src="../../../zheke_bapker_kz/media/img/page1_slider_3pitan.png" alt="Питание" class="slider_img food_img">
            <div class="slider_text_box">
                <h2 class="fadeInDowntit">Питание </h2>
                <div class="slider_text fadeInDowntext">
                    Готовое меню блюд для здорового питания с перечнем ингредиентов и возможностью составления ежедневного рациона с подсчётом калорий
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->

        <!--unit -->
        <div class="slide">
            <input type="radio" name="slide_switch" id="slide6" />
            <label class="pulse" for="slide6">
                <img src="../../../zheke_bapker_kz/media/img/page1_icon_pulse.png" alt="" class="slideLeft2">
            </label>
            <img src="../../../zheke_bapker_kz/media/img/page1_slider_4puls.png" alt="Диагностика пульса" class="slider_img pulse_img">
            <div class="slider_text_box">
                <h2 class="fadeInDowntit">Диагностика пульса</h2>
                <div class="slider_text fadeInDowntext">
                     Измерение вашего пульса для отслеживания текущего состояния в различных условиях
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->

        <!--unit -->
        <div class="slide">
            <input type="radio" name="slide_switch" id="slide7"/>
            <label class="playlist" for="slide7">
                <img src="../../../zheke_bapker_kz/media/img/page1_icon_playlist.png" alt="" class="slideLeft3">
            </label>
            <img src="../../../zheke_bapker_kz/media/img/page1_slider_6play.png" alt="Плейлист" class="slider_img playlist_img">
            <div class="slider_text_box">
                <h2 class="fadeInDowntit">Плейлист </h2>
                <div class="slider_text fadeInDowntext">
                    Ваш индивидуальный плейлист с музыкой для поддержания тонуса во время тренировок
                </div><!-- /slider_text-->
            </div><!-- /slider_text_box -->
        </div><!-- /slide-->
        <!-- /unit -->

    </div>
    <!--  /info slider  -->


</section><!-- /page1-->


<div class="wrapper">
    <div class="parallax page1_simulator"></div>


    <section class="intro_box">
        <div class="intro_content">
              <span class="intro_text">
                  Скачать бесплатно
              </span>

                <span class="intro_links">
                    <span>Приложение для смартфонов (iOS и Android)</span>
                   <a href="https://itunes.apple.com/us/app/licnyj-trener/id938673136?mt=8" target="_blank"><img src="../../../zheke_bapker_kz/media/img/btn_apple_ru.png" alt=""></a>
                    <a href="https://play.google.com/store/apps/details?id=kz.simplecode.confederation"target="_blank"><img src="../../../zheke_bapker_kz/media/img/btn_google_ru.png" alt=""></a>
                </span>
        </div><!-- /intro_content -->
    </section><!-- /inter-->



    <section class="parallax page2">
    <div class="page_2_bg"></div>


        <!-- слайд будет показывать дефолтно слайд с <input type="radio" checked> -->
        <!--
        слайдер на  CSS
        каждый слайд завязан на отработку radio.
        у каждого слайда свой класс для label - для иконки, ее расположения и анимации, и сопуствующего текста.
       - - - -
        <label>
        <IMG> - в телефоне меняется эта картинка
        <div class="slider_text_box">

         размер изображения: 246х436
         в стилях размер картинки зашит.

         сейчас стоят тестовые изображения:
           page2_sl_1.png,
           page2_sl_2.png,
           которые поменять на требуемые
        - - - -
        -->


        <div class="info_box_2">

            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_1"  checked>
                <label class="training" for="slide2_1"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_slider_1treni.png" alt="Тренировки" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Тренировки</h2>
                    <div class="slider_text fadeInRight">
                        Набор разнообразных программ
                        для тренировок на различные группы мышц
                        и возможностью создания собственных программ с заданным расписанием занятий
                        для достижения поставленных целей
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->

            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_2">
                <label class="pedometer" for="slide2_2"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_slider_2shagi.png" alt="Шагомер" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Шагомер</h2>
                    <div class="slider_text fadeInRight">
                        Интерактивный шагомер для подсчёта пройденного расстояния, скорости и израсходованной энергии пешком или на велосипеде </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->

            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_3">
                <label class="food" for="slide2_3"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_slider_3pitan.png" alt="Питание" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Питание</h2>
                    <div class="slider_text fadeInRight">
                        Готовое меню блюд для здорового питания с перечнем ингредиентов и возможностью составления ежедневного рациона с подсчётом калорий
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->


            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_4">
                <label class="pulse" for="slide2_4"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_slider_4puls.png" alt="Диагностика пульса" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Диагностика пульса</h2>
                    <div class="slider_text fadeInRight">
                        Измерение вашего пульса для отслеживания текущего состояния в различных условиях
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->


            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_5">
                <label class="diary" for="slide2_5"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_slider_5diary.png" alt="Дневник" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Дневник</h2>
                    <div class="slider_text fadeInRight">
                        Сводка всех ваших действий по занятию спортом, а также статистика потребленных калорий
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->


            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_6">
                <label class="playlist" for="slide2_6"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_slider_6play.png" alt="Плейлист" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Плейлист</h2>
                    <div class="slider_text fadeInRight">
                        Ваш индивидуальный плейлист с музыкой для поддержания тонуса во время тренировок
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->


            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_7">
                <label class="articles" for="slide2_7"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_slider_7Articles.png" alt="Статьи" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Статьи</h2>
                    <div class="slider_text fadeInRight">
                        Постоянно пополняемый сборник статей о здоровом образе жизни, фитнесе, питании и пищевых добавках
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->

            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_8">
                <label class="ribbon " for="slide2_8"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_slider_8lenta.png" alt="Лента событий" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Лента событий</h2>
                    <div class="slider_text fadeInRight">
                        Лента достижений ваших друзей, по которой вы сможете сравнивать свои успехи
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->

            <!--unit -->
            <div class="slide2">
                <input type="radio" name="slider2_switch" id="slide2_9">
                <label class="users" for="slide2_9"></label>
                <img src="../../../zheke_bapker_kz/media/img/page2_slider_9users.png" alt="Пользователи" class="slider_img">
                <div class="slider_text_box">
                    <h2 class="fadeInRight">Пользователи </h2>
                    <div class="slider_text fadeInRight">
                        Поиск и просмотр профилей пользователей, которых вы можете добавить в друзья
                    </div><!-- /slider_text-->
                </div><!-- /slider_text_box -->
            </div><!-- /slide-->
            <!-- /unit -->



            <div class="download_box">
              <h3>Скачать бесплатно
                  на свой смартфон</h3>

                 <a href="https://itunes.apple.com/us/app/licnyj-trener/id938673136?mt=8" target="_blank"><img src="../../../zheke_bapker_kz/media/img/btn_apple_ru.png" alt=""></a>
                 <a href="https://play.google.com/store/apps/details?id=kz.simplecode.confederation"target="_blank"><img src="../../../zheke_bapker_kz/media/img/btn_google_ru.png" alt=""></a>
            </div>

        </div>







        <footer class="footer">
            <div class="footer_content">
              <span class="footer_text">
                  Титульные спонсоры
              </span>

                <span class="footer_links">
                    <a href="http://sk.kz/"><img src="../../../zheke_bapker_kz/media/img/sk_logo.png" alt=""></a>
                    <a href="http://www.kmg.kz/"><img src="../../../zheke_bapker_kz/media/img/kmg_logo.png" alt=""></a>
                </span>

                <a href="http://kaznetmedia.kz/" class="knm">kaznetmedia. воплощаем идеи</a>
            </div><!-- /intro_content -->
        </footer>
    </section><!-- /page2-->

</div><!-- /wrapper-->
</body>
</html>