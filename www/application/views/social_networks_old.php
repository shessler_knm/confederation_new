<div class="social_widget">
    <h2><?= __("Мы в соцсетях") ?></h2>
    <ul class="unstyled inline" id="social_tab">
        <li><a href="#social_fb" class="link_dotted">Facebook</a></li>
        <li class="active"><a href="#social_tw" class="link_dotted">Twitter</a></li>
        <li><a href="#social_vk" class="link_dotted"><?= __('Вконтакте') ?></a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane" id="social_fb">
            <?if (I18n::$lang=='ru'||I18n::$lang=='en'):?>
            <iframe
                src="https://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FConfederatSport%3Fhc_location%3Dstream&amp;width=240&amp;height=360&amp;show_faces=true&amp;colorscheme=light&amp;stream=false&amp;show_border=false&amp;header=false"
                scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:250px; height:360px;"
                allowTransparency="true">
            </iframe>
            <?endif?>
            <?if (I18n::$lang=='kz'):?>
                <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FConfederSportKZ&amp;width=240&amp;height=360&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false"
                scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:250px; height:360px;"
                allowTransparency="true">
            </iframe>
            <?endif?>
        </div>

        <div class="tab-pane active" id="social_tw">
            <?if (I18n::$lang=='ru'||I18n::$lang=='en'):?>
            <a class="twitter-timeline" href="https://twitter.com/ConfederSport"
               data-widget-id="345402143378898944"><?= __('Твиты пользователя') ?> @ConfederSport</a>
            <script>!function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                    if (!d.getElementById(id)) {
                        js = d.createElement(s);
                        js.id = id;
                        js.src = p + "://platform.twitter.com/widgets.js";
                        fjs.parentNode.insertBefore(js, fjs);
                    }
                }(document, "script", "twitter-wjs");</script>
            <?endif?>
            <?if(I18n::$lang=='kz'):?>
            <a class="twitter-timeline" href="https://twitter.com/ConfederSportKZ"
               data-widget-id="456367275163934720"><?=__('Твиты пользователя')?> @ConfederSportKZ</a>
            <script>!function(d,s,id){
                    var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';
                    if(!d.getElementById(id)){
                        js=d.createElement(s);
                        js.id=id;js.src=p+"://platform.twitter.com/widgets.js";
                        fjs.parentNode.insertBefore(js,fjs);}
                }(document,"script","twitter-wjs");</script>
            <?endif?>

        </div>

        <div class="tab-pane" id="social_vk">
            <div id="vk_groups"
                 style="height: 842px; width: 220px; background-image: none; background-position: initial initial; background-repeat: initial initial;">
                <?if (I18n::$lang=='ru'||I18n::$lang=='en'):?>
                <iframe name="fXD6eab5" frameborder="0"
                        src="http://vk.com/widget_community.php?app=0&amp;width=220px&amp;_ver=1&amp;gid=52891963&amp;mode=0&amp;color1=FFFFFF&amp;color2=2B587A&amp;color3=5B7FA6&amp;height=400&amp;url=http%3A%2F%2Fsport.dev%2Fconfederation&amp;1405338c06d"
                        width="220" height="200" scrolling="no" id="vkwidget1"
                        style="overflow: hidden; height: 842px;">
                </iframe>
                <?endif?>
                <?if(I18n::$lang=='kz'):?>
                    <script type="text/javascript" src="//vk.com/js/api/openapi.js?111"></script>

                    <!-- VK Widget -->
                    <script type="text/javascript">
                        VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "400", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 67413254);
                    </script>
                <?endif?>
            </div>
        </div>
    </div>
