<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <div class="page_section">
                <h1><?=$model->title?></h1>
                <em class="muted"><?=Date::textdate($model->date,'d m')?></em>
                <div class="media">
                    <div class="pull-left">
                        <?/*=$model->photo_s->html_img(240, 240,array('alt'=>$model->title)) */?>
                        <?php if (file_exists(URL::site($model->photo_s->dir . '/'. $model->photo_s->name . '.' . $model->photo_s->type))): ?>
                            <img src="<?=URL::site($model->photo_s->dir . '/'. $model->photo_s->name . '.' . $model->photo_s->type);?>">
                        <?php endif; ?>
                    </div>
                    <div class="media_text">
                        <?=$model->text?>
                    </div>
                </div>
            </div>
            <?=__('Понравился материал? Поделитесь им с друзьями в соцсетях!')?>
            <div class="social_block" id="social_block">
                Yandex share?
            </div>

            <?=Comments::get_list($model->table_name(),$model->id)?>
        </div>
    </div>

    <div class="aside">
        <!-- <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute()?>
        </div> -->
        <?=View::factory('social_networks')?>
    </div>
</div>