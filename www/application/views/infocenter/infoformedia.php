<div class="row_fluid">
    <div class="row_section">
        <div class="section">
            <div class="page_section">
                <h1><?=$model->title?></h1>

                <div class="text-left"><?=$model->text?></div>
            </div>
            <?=__('Понравился материал? Поделитесь им с друзьями в соцсетях!')?>
            <div class="social_block" id="social_block"> </div>

        </div>
    </div>
    <div class="aside">
        <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute()?>
        </div>
        <?=View::factory('social_networks')?>
    </div>
</div>

