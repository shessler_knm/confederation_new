<div class="row_fluid">
    <div class="row_section">
        <div class="section">
            <h1><?=__('СМИ о нас')?></h1>

            <ul class="unstyled event_list">
                <?php
                $i = 0;
                ?>
                <? foreach ($model as $row): ?>
                    <?php
                    if ($i % 2 == 0) {
                        echo "<li class='row-fluid'>";
                    }
                    ?>

                    <div class="span6 item<?=$i % 2 + 1?> media">
                        <div class="figure">
                            <a href="<?=URL::site('confederation/infocenter/aboutus/'.$row->{'sef_'.I18n::$lang})?>"><?= $row->photo_s->html_img(400, null,array('alt'=>$row->title)) ?></a>
                        </div>
                        <div class="media-body">
                            <div class="h5"><a href="<?=URL::site('confederation/infocenter/aboutus/'.$row->{'sef_'.I18n::$lang})?>" class="link"><span
                                class="ls"><?=$row->title?></span></a></div>

                            <div class="media_text"><?=Text::limit_words(strip_tags($row->text), 155)?></div>
                            <small><em><?=Date::textdate($row->date, 'd m')?>
                                <?if ($row->author==true):?>
                                        <?=$row->author?>
                                <?endif?>
                                <?if ($row->source==true):?>
                                        <?=$row->source?>
                                <?endif?>
                            </em></small>
                            <div class="span6 hr"></div>
                        </div>
                    </div>
                    <?php
                    if ($i % 2 == 1) {
                        echo "</li>";
                    }
                    $i++;
                    ?>
                <?endforeach?>
                <?php
                if ($i % 2 == 0) {
                    echo "</li>";
                }
                ?>

            </ul>
            <?=$pagination?>
        </div>
    </div>
    <div class="aside">
        <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute()?>
        </div>
        <?=View::factory('social_networks')?>
    </div>
</div>
