<div class="interview_section">
     <div class="h3"><a href="<?= URL::site('confederation/infocenter/interview/') ?>" class="link"><span
                class="ls"><?= __("Интервью") ?></span></a></div>
    <? foreach ($model as $row): ?>
        <div class="media">
            <? if ($row->photo == true): ?>
                <div class="pull-left img-circle mask148">
                    <a href="<?= URL::site('confederation/infocenter/interview/' . $row->{'sef_' . I18n::$lang}) ?>"><img src="<?=URL::site($row->photo_s->dir . '/'. $row->photo_s->name . '.' . $row->photo_s->type);?>"><?/*= $row->photo_s->html_img(400, null,array('alt'=>$row->title))*/ ?></a>
                </div>
            <? endif ?>
            <? if (!$row->photo): ?>
                <div class="pull-left img-circle">
                    <img src="<?= URL::site('images/zagl.jpg') ?>" alt="">
                    <a href="<?= URL::site('confederation/infocenter/interview/' . $row->{'sef_' . I18n::$lang}) ?>"
                       class="mask148"></a>
                </div>
            <? endif ?>
            <div class="media-body">
                <div class="h4"><a href="<?= URL::site('confederation/infocenter/interview/' . $row->{'sef_' . I18n::$lang}) ?>"
                       class="link"><?= $row->title; ?></a></div>

                <!-- <div class="media_text"><?= $row->get_announcement() ?></div> -->
                <small><em>
                        <span class="date"><?= Date::textdate($row->date, 'd m') ?></span>
<!--                        --><?// foreach ($federations[$row->id] as $federation): ?>
<!--                        <a href="--><?//= URL::site($federation->sef . $fed_id) ?><!--" class="link">-->
<!--                            <span class="ls">--><?//= strip_tags($federation->title) ?><!--</span>-->
<!--                            </a>--><?//= $counter < count($federations[$row->id]) ? ',' : null ?>
<!--                            --><?// $counter++ ?>
<!--                        --><?// endforeach ?>
<!--                        --><?// $counter = 1 ?>
                    </em></small>
            </div>
        </div>
    <? endforeach ?>
</div>

