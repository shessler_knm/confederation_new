<div class="hr_bottom interview_section">
     <div class="h3"><a href="<?= URL::site('confederation/infocenter/interview/') ?>" class="link"><span
                class="ls"><?= __("Интервью") ?></span></a></div>
    <? foreach ($model as $row): ?>
        <div class="media">
            <? if ($row->photo == true): ?>
                <div class="pull-left img-circle">
                    <?= $row->photo_s->html_img(400, null,array('alt'=>$row->title)) ?>
                    <a href="<?= URL::site('confederation/infocenter/interview/' . $row->{'sef_' . I18n::$lang}) ?>"
                       class="mask175"></a>
                </div>
            <? endif ?>
            <? if (!$row->photo): ?>
                <div class="pull-left img-circle">
                    <img src="<?= URL::site('images/zagl.jpg') ?>" alt="">
                    <a href="<?= URL::site('confederation/infocenter/interview/' . $row->{'sef_' . I18n::$lang}) ?>"
                       class="mask175"></a>
                </div>
            <? endif ?>
            <div class="media-body">
                <div class="h4"><a href="<?= URL::site('confederation/infocenter/interview/' . $row->{'sef_' . I18n::$lang}) ?>"
                       class="link"><em
                            class="ls"><?= $row->title; ?></em></a></div>

                <div class="media_text"><?= $row->get_announcement() ?></div>
                <small><em>
                        <span class="date"><?= Date::textdate($row->date, 'd m') ?></span>
<!--                        --><?// foreach ($federations[$row->id] as $federation): ?>
<!--                        <a href="--><?//= URL::site($federation->sef . $fed_id) ?><!--" class="link">-->
<!--                            <span class="ls">--><?//= strip_tags($federation->title) ?><!--</span>-->
<!--                            </a>--><?//= $counter < count($federations[$row->id]) ? ',' : null ?>
<!--                            --><?// $counter++ ?>
<!--                        --><?// endforeach ?>
<!--                        --><?// $counter = 1 ?>
                    </em></small>
            </div>
        </div>
    <? endforeach ?>
</div>

