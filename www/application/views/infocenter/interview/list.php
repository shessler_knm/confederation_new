<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <?php
        echo View::factory('breadcrumbs')->set('_breadcrumbs', $_breadcrumbs)->render();
        ?>
        <div class="section">
            <h1><?= __('Интервью') ?></h1>

            <form action="" method="get" class="row-fluid form-inline form_sport">
                <label><?= __('Вид спорта') ?>:</label>
                <?= Form::select('sport_type', $sport_types, $select_id, array('class' => 'auto_submit custom_select')); ?>
            </form>
            <ul class="unstyled news_list interview_list">
                <? foreach ($model as $row): ?>
                    <li class="media">
                        <? if ($row->photo): ?>
                            <div class="pull-right img-circle mask140">
                                <a href="<?= URL::site('confederation/infocenter/interview/' . $row->{'sef_' . I18n::$lang}) ?>">
                                <img src="<?=URL::site($row->photo_s->dir . '/'. $row->photo_s->name . '.' . $row->photo_s->type);?>"><?/*=$row->photo_s->html_img(400, null,array('alt'=>$row->title))*/ ?>
                                </a>
                            </div>
                        <? endif ?>
                        <? if (!$row->photo): ?>
                            <div class="pull-right img-circle mask140">

                                <a href="<?= URL::site('confederation/infocenter/interview/' . $row->{'sef_' . I18n::$lang}) ?>">
                                    <img src="<?= URL::site('images/zagl.jpg') ?>" alt="">
                                </a>
                            </div>
                        <? endif ?>

                        <div class="media-body">
                            <div class="h4"><a href="<?= URL::site('confederation/infocenter/interview/' . $row->{'sef_' . I18n::$lang}) ?>"
                                   class="link">
                            <span class="ls">
<!--                                --><? //=$row->lastname?>
                                <!--                                --><?//=$row->firstname?><!--:-->
                                <?= __($row->title); ?>
                            </span>
                                </a></div>

                            <div class="media_text"><?= $row->get_announcement() ?></div>
                            <small><em>
                                    <span class="date"><?= Date::textdate($row->date, 'd m') ?></span>
<!--                                    --><?// foreach ($federations[$row->id] as $federation): ?>
<!--                                    <a href="--><?//= URL::site($federation->sef . $fed_id) ?><!--" class="link">-->
<!--                                        <span class="ls">--><?//= strip_tags($federation->title) ?><!--</span>-->
<!--                                        </a>--><?//= $counter < count($federations[$row->id]) ? ',' : null ?>
<!--                                        --><?// $counter++ ?>
<!--                                    --><?// endforeach ?>
<!--                                    --><?// $counter = 1 ?>
                                    <span class="ico_eye"><?= $row->views ?></span>
                                    <span class="ico_comment"><?= Comments::get_count('News', $row->id) ?></span>
                                </em></small>
                        </div>
                    </li>
                <? endforeach ?>
                <?= $pagination?>
            </ul>
        </div>
    </div>

    <div class="aside">
        <!-- <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div> -->
        <?= View::factory('social_networks') ?>
    </div>
</div>