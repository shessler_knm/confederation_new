<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <div class="page_section">
                <h1><?= $model->title;?></h1>
                <em class="muted"><?=Date::textdate($model->date,'d m, H:i')?></em>
                <div class="media">
                    <?if ($model->photo==true):?>
                        <div class="pull-left img-circle mask200">
                            <?/*=$model->photo_s->html_img(400,null,array('alt'=>$model->title))*/?>
                            <img src="<?=URL::site($model->photo_s->dir . '/'. $model->photo_s->name . '.' . $model->photo_s->type);?>">
                            <!-- <div class="mask200"></div> -->
                        </div>
                    <?endif?>
                    <?if (!$model->photo):?>
                        <div class="pull-left img-circle mask200">
                            <img src="<?=URL::site('images/zagl.jpg')?>">
                            <!-- <div class="mask200"></div> -->
                        </div>
                    <?endif?>
                    <div class="media-text">
                        <?=$model->text?>
                    </div>
                    <div class="media">
                        <div class="text-right">
                            <em>
                                <?if ($model->author==true):?>
                                    <?=$model->author?>
                                <?endif?>
                            </em>
                        </div>
                        <div class="text-right">
                            <em class="muted">
                                <?if ($model->photograph==true):?>
                                    <?=$model->photograph?>
                                <?endif?>
                            </em>
                        </div>
                    </div>
                </div>
            </div>
            <?=__('Понравился материал? Поделитесь им с друзьями в соцсетях!')?>
            <div  class="social_block" id="social_block">
                Yandex share?
            </div>

            <?=Comments::get_list($model->table_name(),$model->id)?>

        </div>
    </div>

    <div class="aside">
        <!-- <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute()?>
        </div> -->
        <?=View::factory('social_networks')?>
    </div>
</div>
