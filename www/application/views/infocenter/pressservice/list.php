<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <?php
        echo View::factory('breadcrumbs')->set('_breadcrumbs', $_breadcrumbs)->render();
        ?>
        <div class="section">
            <h1><?=__('Информация для СМИ')?></h1>
                <ul class="unstyled event_list">
                        <?php
                        $i=0;
                        ?>
                            <? foreach ($model as $row): ?>
                                <?php
                                if ($i % 2 == 0){
                                    echo "<li class='row-fluid'>";
                                }
                                ?>

                            <div class="span6 item<?=$i%2+1?> media">


                                    <div class="figure">
                                        <a href="<?=URL::site('confederation/infocenter/pressservice/'.$row->{'sef_'.I18n::$lang})?>">
                                            <?/*= $row->photo_s->html_img(240, 240,array('alt'=>$row->title)) */?>
                                            <img src="<?=URL::site($row->photo_s->dir . '/'. $row->photo_s->name . '.' . $row->photo_s->type);?>">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <div class="h5"><a href="<?=URL::site('confederation/infocenter/pressservice/'.$row->{'sef_'.I18n::$lang})?>" class="link"><span class="ls"><?=$row->title?></span></a></div>
                                        <div class="media_text"><?=Text::limit_chars(strip_tags($row->text), 155)?></div>
                                        <small><em><?=Date::textdate($row->date, 'd m')?></em></small>
                                        <div class="span6 hr"></div>
                                    </div>

                            </div>
                                <?php
                                if ($i % 2==1){
                                    echo "</li>";
                                }
                                $i++;
                                ?>
                            <?endforeach?>

                            <?php
                            if ($i % 2 == 0){
                                echo "</li>";
                            }
                            ?>

                </ul>
            <?=$pagination?>
        </div>
    </div>
    <div class="aside">
        <!-- <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute()?>
        </div> -->
        <?=View::factory('social_networks')?>

    </div>

</div>



