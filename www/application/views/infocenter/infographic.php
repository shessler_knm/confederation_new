<div class="row_fluid clearfix bg-white">
    <?php
    echo View::factory('breadcrumbs')->set('_breadcrumbs', $_breadcrumbs)->render();
    ?>
    <div class="row_section">

        <ul class="unstyled infographic_list">
            <? foreach ($model as $row):?>
            <li class="item_media <?=$row->orientation==1?'media_w1':'media_w2'?>">
                <? if (I18n::lang() == 'ru'): ?>
                    <a href="<?=$row->photoru_s->url()?>" title="<?=$row->title?></br><?=Date::textdate($row->date,'d m, H:i')?>" class="infographic" rel="group">
                        <?=$row->photoru_s->html_img($row->weight,$row->height,array('alt'=>$row->title))?>
                        <span class="title_item_media">
                            <span>
                                <?=$row->title?>
                                <em class="muted"><?=Date::textdate($row->date,'d m')?></em>
                            </span>
                        </span>
                    </a>
                <? endif ?>
                <? if (I18n::lang() == 'en'): ?>
                    <a href="<?=$row->photoen_s->url()?>" title="<?=$row->title?></br><?=Date::textdate($row->date,'d m, H:i')?>" class="infographic" rel="group">
                        <?=$row->photoen_s->html_img($row->weight,$row->height,array('alt'=>$row->title))?>
                        <span class="title_item_media">
                            <span>
                                <?=$row->title?>
                                <em class="muted"><?=Date::textdate($row->date,'d m')?></em>
                            </span>
                        </span>
                    </a>
                <? endif ?>
                <? if (I18n::lang() == 'kz'): ?>
                    <a href="<?=$row->photokz_s->url()?>" title="<?=$row->title?></br><?=Date::textdate($row->date,'d m, H:i')?>" class="infographic" rel="group">
                        <?=$row->photokz_s->html_img($row->weight,$row->height,array('alt'=>$row->title))?>
                        <span class="title_item_media">
                            <span>
                                <?=$row->title?>
                                <em class="muted"><?=Date::textdate($row->date,'d m')?></em>
                            </span>
                        </span>
                    </a>
                <? endif ?>


            </li>
            <?endforeach?>
        </ul>

    </div>

</div>
