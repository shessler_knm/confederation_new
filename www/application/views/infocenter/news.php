<h1><?= __('Новости') ?></h1>

<?= Request::factory('confederation/news/request/widget')->execute() ?>

<div class="row_fluid">
    <div class="row_section">
        <div class="section">

            <ul class="unstyled event_list news_list_inline">
                <?php
                $i = 0;
                ?>
                <? foreach ($model as $row): ?>
                    <?php
                    if ($i % 2 == 0) {
                        echo "<li class='row-fluid'>";
                    }
                    ?>
                    <div class="span6 item<?= $i % 2 + 1 ?> media">
                        <div class="figure">
                            <a href="<?= URL::site('confederation/news/' . $row->{'sef_' . I18n::$lang}) ?>"><?= $row->photo_s->html_img(400, null,array('alt'=>$row->title)) ?></a>
                        </div>
                        <div class="media-body">
                            <small><em>
                                <span class="date"><?= Date::textdate($row->date, 'd m') ?></span>
                                </em>
                            </small>
                             <div class="h5">
                                <a href="<?= URL::site('confederation/news/' . $row->{'sef_' . I18n::$lang}) ?>"
                                   class="link"><span
                                        class="ls"><?= Text::limit_chars(strip_tags($row->title), 165) ?></span></a>
                            </div>

                            <div class="media_text"><?= $row->get_announcement() ?></div>
                            <small><em>
                                    <!--<span class="date"><?/*= Date::textdate($row->date, 'd m') */?></span>-->
<!--                                    --><?// foreach ($federations[$row->id] as $federation): ?>
<!--                                    <a href="--><?//= URL::site($federation->sef) ?><!--" class="link"><span-->
<!--                                            class="ls">--><?//= strip_tags($federation->title) ?><!--</span>-->
<!--                                        </a>--><?//= $counter < count($federations[$row->id]) ? ',' : null ?>
<!--                                        --><?// $counter++ ?>
<!--                                    --><?// endforeach ?>
<!--                                    --><?//$counter=1?>

                                    <span class="ico_comment"><?= Comments::get_count('News', $row->id) ?></span>
                                    <span><?=$row->author?__('Автор').': '.$row->author:null?></span>
                                </em></small>
                        </div>
                        <div class="span6 hr"></div>
                    </div>
                    <?php
                    if ($i % 2 == 1) {
                        echo "</li>";
                    }
                    $i++;
                    ?>
                    <?php
                    if ($i % 2 == 0) {
                        echo "</li>";
                    }
                    ?>
                <? endforeach ?>

            </ul>
            <?= $pagination ?>

        </div>
    </div>
    <div class="aside">
        <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div>
        <?= View::factory('social_networks') ?>

    </div>
</div>