<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?= Url::site('images/favicon.png') ?>" type="image/x-icon">
    <? if (isset($total_pages)) : ?>
        <? if (strstr(Request::current()->url(), 'event')): ?>
            <?= Request::factory('link')->query('url', Request::current()->url())->query('total_pages', $total_pages)->query('event', true)->query('event_page', Request::initial()->query('page') ? Request::initial()->query('page') : false)->execute() ?>
        <? else: ?>
            <?= Request::factory('link')->query('url', Request::current()->url())->query('total_pages', $total_pages)->execute() ?>
        <?endif ?>
    <? else: ?>
        <?= Helper::canonical($fed_id) ?>
    <? endif ?>

    <link rel="stylesheet" href="<?= URL::site("font/pt_sans.css") ?>">
    <!--[if IE]>-->
    <link rel="stylesheet" href="<?= URL::site('font/pt_sans_regular_ie.css') ?>"/>
    <!--[endif]-->
    <!--[if IE]>-->
    <link rel="stylesheet" href="<?= URL::site('font/pt_sans_bold_ie.css') ?>"/>
    <!--[endif]-->
    <!--[if IE]>-->
    <link rel="stylesheet" href="<?= URL::site('font/pt_sans_italic_ie.css') ?>"/>
    <!--[endif]-->
    <!--[if IE]>-->
    <link rel="stylesheet" href="<?= URL::site('font/pt_sans_bold_italic_ie.css') ?>"/>
    <!--[endif]-->
    <?= $page->get_head(true) ?>
    <script>
        var BASE_URL = '<?=url::base();?>';
        var LANG = '<?=I18n::$lang?>';
        USER_ID = "<?=isset($user) && $user instanceof ORM?$user->id:'undefined'?>";
        CUR_USER_ID = "<?=isset($current_user) && $current_user instanceof ORM?$current_user->id:'undefined'?>";
        CSRF_TOKEN = "<?=Session::instance()->get('csrf-token')?>";
    </script>
</head>


<body class="main_template <?= $fed_id ?> <?= $full_size ? 'full_size' : '' ?>">
<? if (Helper::is_production()): ?>
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P75DZW"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push(
            {'gtm.start': new Date().getTime(),event:'gtm.js'}
        );var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-P75DZW');</script>
    <!-- End Google Tag Manager -->
<? endif ?>
<div id="wrapper">

<div id="top">
    <div class="container">

        <!--        max-width: 570px------------------->

        <div class="min_block dropdown_block lang_menu">
            <a class="dropdown_toggle" data-toggle="dropdown" href="#" data-backdrop="0">
                <span>
                    <?= I18n::lang() == 'kz' ? 'Қаз' : '' ?>
                    <?= I18n::lang() == 'ru' ? 'Рус' : '' ?>
                    <?= I18n::lang() == 'en' ? 'Eng' : '' ?>
                </span>
            </a>

            <ul class="dropdown_menu unstyled white_link">
                <li <?= I18n::lang() == 'kz' ? 'class="active"' : '' ?>>
                    <a href="<?= $array_langs['kz'] ?>" <?= strstr($array_langs['kz'], Request::initial()->uri()) ? 'rel=nofollow' : null ?>
                       class="link"><span class="ls">Қаз</span></a>
                </li>
                <li <?= I18n::lang() == 'ru' ? 'class="active"' : '' ?>>
                    <a href="<?= $array_langs['ru'] ?>" <?= strstr($array_langs['ru'], Request::initial()->uri()) && I18n::$lang != 'kz' ? 'rel=nofollow' : null ?>
                       class="link"><span class="ls">Рус</span></a>
                </li>
                <li <?= I18n::lang() == 'en' ? 'class="active"' : '' ?>>
                    <a href="<?= $array_langs['en'] ?>" <?= strstr($array_langs['en'], Request::initial()->uri()) && I18n::$lang != 'kz' ? 'rel=nofollow' : null ?>
                       class="link"><span class="ls">Eng</span></a>
                </li>
            </ul>
        </div>

        <div class="min_block dropdown_block top_menu">
            <a class="dropdown_toggle" data-toggle="dropdown" href="#" data-backdrop="0"><span><i class="icon">Menu</i></span></a>

            <ul class="dropdown_menu unstyled white_link" role="menu" aria-labelledby="dLabel">
                <? if (!(strstr(Request::current()->url(), 'confederation'))): ?>
                    <li><a href="<? echo URL::site('confederation') ?>" class="link"><span
                                class="ls"><?= __('Конфедерация') ?></span></a></li>
                <? endif ?>
                <? foreach ($federations as $row2): ?>
                    <li><a href="<? echo URL::site($row2->sef) ?>" class="link"><span
                                class="ls"><?= $row2->title ?></span></a></li>
                <? endforeach ?>
            </ul>
        </div>

        <div class="min_block dropdown_block search_menu">
            <a class="dropdown_toggle" data-toggle="dropdown" href="#" data-backdrop="0"><span><i
                        class="icon">Search</i></span></a>

            <ul class="dropdown_menu unstyled inline white_link">
                <li>
                    <form action="<?= URL::site($fed_id ? $fed_id . '/search/list' : 'confederation/search/list') ?>"
                          class="form-horizontal">
                        <input name="q" type="search" value="<?= __('Поиск по сайту') ?>" class="input_search">
                        <input type="button" class="btn color" value="<?= __('Найти') ?>">
                    </form>
                </li>
            </ul>
        </div>

        <!--    END    max-width: 570px-------------->


        <ul class="max_block unstyled inline white_link">
            <li>
                <div class="inner"><a href="<? echo URL::site('confederation') ?>" class="link"><span
                            class="ls"><?= __('Конфедерация') ?></span></a></div>
            </li>
            <? foreach ($federations as $row2): ?>
                <li>
                    <div class="inner"><a href="<? echo URL::site($row2->sef) ?>" class="link"><span
                                class="ls"><?= $row2->title ?></span></a></div>
                </li>
            <? endforeach ?>
        </ul>

    </div>
</div>

<div id="header" class="pattern_bg">
    <div class="header_flag">
        <div class="container">

            <div class="pull-left title">
                <div class="middle">
                    <? if (!(strstr(Request::current()->url(), 'confederation')) && !(strstr(Request::current()->url(), 'privatecoach')) && !(strstr(Request::current()->url(), 'user')) && !(strstr(Request::current()->url(), 'partner/list'))): ?>
                        <? if (strstr(URL::site($fed_id), Request::initial()->uri())): ?>
                            <a href="<?= URL::site($fed_id) ?>"
                               rel="nofollow"><?= $federation->photo_s->html_img(126, 126, array('alt' => $federation->title)) ?></a>
                            <div class="h1"><a href="<?= URL::site($fed_id) ?>"
                                               rel="nofollow"><?= $federation->title ?></a></div>
                        <? else: ?>
                            <a href="<?= URL::site($fed_id) ?>"><?= $federation->photo_s->html_img(126, 126, array('alt' => $federation->title)) ?></a>
                            <div class="h1"><a href="<?= URL::site($fed_id) ?>"><?= $federation->title ?></a></div>
                        <?endif ?>
                    <? else: ?>
                        <? if (strstr(URL::site('confederation'), Request::initial()->uri())): ?>
                            <a href="<?= URL::site('confederation') ?>" rel="nofollow"><img
                                    src="<?= URL::site('images/min_logo.png') ?>"></a>
                            <div class="h1"><a href="<?= URL::site('confederation') ?>"
                                               rel="nofollow"><?= __('Конфедерация спортивных единоборств <br> и силовых видов спорта') ?></a>
                            </div>
                        <? else: ?>
                            <a href="<?= URL::site('confederation') ?>"><img
                                    src="<?= URL::site('images/min_logo.png') ?>"></a>
                            <div class="h1"><a
                                    href="<?= URL::site('confederation') ?>"><?= __('Конфедерация спортивных единоборств <br> и силовых видов спорта') ?></a>
                            </div>
                        <?endif ?>
                    <? endif ?>
                </div>
            </div>

            <div class="media-body lang max_block">
                <? if (strstr(Request::current()->uri(), 'privatecoach')): ?>
                    <ul class="unstyled inline white_link">
                        <li <?= I18n::lang() == 'kz' ? 'class="active"' : '' ?>>
                            <a href="<?= $array_langs['kz'] ?>" <?= strstr($array_langs['kz'], Request::initial()->uri()) ? 'rel=nofollow' : null ?>
                               class="link"><span class="ls">Қаз</span></a>
                        </li>
                        <li <?= I18n::lang() == 'ru' ? 'class="active"' : '' ?>>
                            <a href="<?= $array_langs['ru'] ?>" <?= strstr($array_langs['ru'], Request::initial()->uri()) && I18n::$lang != 'kz' ? 'rel=nofollow' : null ?>
                               class="link"><span class="ls">Рус</span></a>
                        </li>
                        <li <?= I18n::lang() == 'en' ? 'class="active"' : '' ?>>
                            <a href="<?= $array_langs['en'] ?>" <?= strstr($array_langs['en'], Request::initial()->uri()) && I18n::$lang != 'kz' ? 'rel=nofollow' : null ?>
                               class="link"><span class="ls">Eng</span></a>
                        </li>
                    </ul>
                <? else: ?>
                    <ul class="unstyled inline white_link">
                        <li <?= I18n::lang() == 'kz' ? 'class="active"' : '' ?>>
                            <a href="<?= $array_langs['kz'] ?>" <?= strstr($array_langs['kz'], Request::initial()->uri()) ? 'rel=nofollow' : null ?>
                               class="link"><span class="ls">Қаз</span></a>
                        </li>
                        <li <?= I18n::lang() == 'ru' ? 'class="active"' : '' ?>>
                            <a href="<?= $array_langs['ru'] ?>" <?= strstr($array_langs['ru'], Request::initial()->uri()) && I18n::$lang != 'kz' ? 'rel=nofollow' : null ?>
                               class="link"><span class="ls">Рус</span></a>
                        </li>
                        <li <?= I18n::lang() == 'en' ? 'class="active"' : '' ?>>
                            <a href="<?= $array_langs['en'] ?>" <?= strstr($array_langs['en'], Request::initial()->uri()) && I18n::$lang != 'kz' ? 'rel=nofollow' : null ?>
                               class="link"><span class="ls">Eng</span></a>
                        </li>
                    </ul>
                <?endif ?>
                <form action="<?= URL::site($fed_id ? $fed_id . '/search/list' : 'confederation/search/list') ?>">
                    <input name="q" type="search" value="<?= __('Поиск по сайту') ?>" class="input_search">
                </form>
            </div>

            <div class="lang mid_block">
                <ul class="unstyled inline white_link">
                    <li class="dropdown search_dropdown">
                        <form
                            action="<?= URL::site($fed_id ? $fed_id . '/search/list' : 'confederation/search/list') ?>">
                            <input name="q" type="search" value=" " class="input_search">
                        </form>
                    </li>

                    <li class="dropdown lang_dropdown">
                        <a class="dropdown_toggle link" data-toggle="dropdown" href="#">
                            <span class="ls">
                                <?= I18n::lang() == 'kz' ? 'Қаз' : '' ?>
                                <?= I18n::lang() == 'ru' ? 'Рус' : '' ?>
                                <?= I18n::lang() == 'en' ? 'Eng' : '' ?>
                            </span>
                            <b class="caret"></b>
                        </a>

                        <ul class="dropdown_menu unstyled">
                            <li <?= I18n::lang() == 'kz' ? 'class="active"' : '' ?>>
                                <a href="<?= $array_langs['kz'] ?>" <?= strstr($array_langs['kz'], Request::initial()->uri()) ? 'rel=nofollow' : null ?>
                                   class="link"><span
                                        class="ls">Қаз</span></a>
                            </li>
                            <li <?= I18n::lang() == 'ru' ? 'class="active"' : '' ?>>
                                <a href="<?= $array_langs['ru'] ?>" <?= strstr($array_langs['ru'], Request::initial()->uri()) && I18n::$lang != 'kz' ? 'rel=nofollow' : null ?>
                                   class="link"><span
                                        class="ls">Рус</span></a>
                            </li>
                            <li <?= I18n::lang() == 'en' ? 'class="active"' : '' ?>>
                                <a href="<?= $array_langs['en'] ?>" <?= strstr($array_langs['en'], Request::initial()->uri()) && I18n::$lang != 'kz' ? 'rel=nofollow' : null ?>
                                   class="link"><span
                                        class="ls">Eng</span></a>
                            </li>
                        </ul>
                    </li>
                </ul>


            </div>

        </div>
    </div>
</div>


<!--Этот блок показывать токлько на страницах конфедерации-->
<? if (strstr(Request::current()->url(), 'confederation')): ?>
    <div class="baner-big">
        <div class="baner-img">
            <a href="http://worldjudo2015.kz/ru/"><img src="<?= URL::site('images/judo1440' . I18n::$lang . '.png') ?>" alt="<?= __('Чемпионат мира по дзюдо 2015'); ?>"></a>
        </div>
    </div>
    <!--<div id="panel_coach">
        <div class="container">
            <a href="<?/*= URL::site(I18n::$lang . '/privatecoach') */?>">
                <img src="<?/*= URL::site('images/coach_' . I18n::$lang . '.png') */?>">

                <em><?/*=
                    __('Ставь цели, добивайся, делись') . '<br>' .
                    __('результатами, меняйся вместе с нами') . '!'*/?></em>
            </a>
        </div>
    </div>-->
<? endif ?>


<!--Этот блок показыыать только на в разделах Личного тренера-->
<? // if (!Helper::is_production()):?>
<? if (!$user && (strstr(Request::current()->url(), 'privatecoach'))): ?>
    <div id="change_yourself_section">
        <div class="container">
            <div class="col-1">
                <h3 class="h3">
                    <a href="<?= URL::site('privatecoach') ?>" class="link"><span
                            class="ls"><?= __('Личный тренер') ?></span></a>
                </h3>

                <p><em><?=
                        __('Ставь цели, добивайся, делись ') .
                        __('результатами, меняйся вместе с нами') . '.'?></em></p>

                <!--<p><strong><em><?/*= __('Нас уже 15 842 — присоединяйся') . '!' */?></em></strong></p>-->
            </div>
            <div class="col-2">
                <div class="inner">
                    <?= Request::factory(I18n::$lang . '/privatecoach/top/main_preview')->execute() ?>

                </div>
            </div>
            <div class="col-3">
                <? if (!$user): ?>
                <? if ($fed_id): ?>
                <form action="<?= URL::site($fed_id . '/user/login') ?>" method="post"
                      accept-charset="utf-8" class="form">
                    <? elseif (strstr(Request::current()->url(), 'confederation')): ?>
                    <form action="<?= URL::site('confederation/user/login') ?>"
                          method="post" accept-charset="utf-8" class="form">
                        <? else: ?>
                        <form action="<?= URL::site('privatecoach/user/login') ?>"
                              method="post" accept-charset="utf-8" class="form">
                            <? endif ?>
                            <div class="control-group placeholder-group">
                                <label class="placeholder_label" for="i_login">
                                    <?= __('Логин или e-mail') ?></label>
                                <?= Form::input('username', '', array("id" => "i_login", "class" => "input_text input_placeholder")); ?>
                            </div>
                            <div class="control-group placeholder-group">
                                <label class="placeholder_label" for="i_pass">
                                    <?= __('Пароль') ?></label>
                                <?= Form::input('password', '', array("id" => "i_pass", "class" => "input_text input_placeholder", "type" => "password")); ?>
                                <div class="error"><?= arr::get($errors, 'password') ?></div>
                            </div>

                            <div class="control-group">
                                <?= $ulogin ?>
                            </div>
                            <div class="control-group">
                                <?= Form::submit('submit', __('Войти'), array('class' => 'btn color')) ?>
                            </div>


                            <div class="clear"><a href="
<?= URL::site(I18n::$lang . '/user/reg') ?>" class="link"><span
                                        class="ls">
<?= __('или зарегистрироваться') ?></span></a></div>
                        </form>

                        <? else: ?>
                            <ul class="unstyled privatecoach_nav">
                                <li>
                                    <a class="icon_user"
                                       href="
<?= URL::site(I18n::$lang . '/privatecoach/profile/' . $user->id) ?>">
                                        <span class="link"><span class="ls">
<?= __('Профиль') ?></span></span>
                                    </a>
                                </li>
                                <li>
                                    <a class="icon_program" href="
<?= URL::site(I18n::$lang . '/privatecoach/program/my_programs') ?>">
                                        <span class="link"><span
                                                class="ls">
<?= __('Программа тренировок') ?></span></span>
                                    </a>
                                </li>
                                <li>
                                    <a class="icon_fav" href="
<?= URL::site(I18n::$lang . '/privatecoach/subscribe/list') ?>">
                                        <span class="link"><span class="ls">
<?= __('Подписки') ?></span></span>
                                    </a>
                                </li>
                                <li>
                                    <a class="icon_exit"
                                       href="<?= URL::site(I18n::$lang . '/privatecoach/user/logout') ?>">
                                        <span class="link"><span class="ls">
<?= __('Выйти') ?></span></span>
                                    </a>
                                </li>
                            </ul>
                        <?
                        endif
                        ?>
            </div>

        </div>
    </div>
<? endif ?>
<? //  endif ?>


<? if ($fed_id): ?>
    <div id="nav_main">
        <div class="container">
            <?= Request::factory(Route::get('federation_menu')->uri(array('fed_id' => $fed_id, 'menu' => 'menu', 'controller' => 'Menu', 'action' => 'build', 'id' => 'menu_federation')))->execute(); ?>
        </div>
    </div>
<? elseif (strstr(Request::current()->url(), 'confederation')): ?>
    <div id="nav_main">
        <div class="container">
            <?= Request::factory(Route::get('confederation_menu')->uri(array('controller' => 'Menu', 'action' => 'build', 'id' => 'menu_confederation')))->execute(); ?>
        </div>
    </div>
<?
elseif (strstr(Request::current()->url(), 'privatecoach')): ?>
    <? if ($user): ?>
        <div id="privatecoach_header">
            <div class="container">
                <div class="h1 privatecoach_tit"><a
                        href="<?= URL::site('privatecoach') ?>"><?= __('Личный тренер') ?></a>
                    <? if ($user->get_users_count(true) >= 100): ?>
                        <em><?= __('Нас уже :count', array(':count' => $user->get_users_count(true))) ?></em>
                    <? endif ?>
                </div>

                <div class="pull-right privatecoach_nav">
                    <!--                --><? //= Request::factory(Route::get('confederation_menu')->uri(array('controller' => 'Menu', 'action' => 'build', 'id' => 'profile_menu')))->execute(); ?>
                    <ul class="unstyled inline">
                        <li>
                            <a class="icon_user"
                               href="<?= URL::site(I18n::$lang . '/privatecoach/profile/' . $user->id) ?>">
                                <span class="link"><span class="ls"><?= __('Профиль') ?></span></span>
                            </a>
                        </li>
                        <li>
                            <a class="icon_program"
                               href="<?= URL::site(I18n::$lang . '/privatecoach/program/my_programs') ?>">
                                <span class="link"><span class="ls"><?= __('Мои программы') ?></span></span>
                            </a>
                        </li>
                        <li>
                            <a class="icon_fav" href="<?= URL::site(I18n::$lang . '/privatecoach/subscribe/list') ?>">
                                <span class="link"><span class="ls"><?= __('Подписки') ?></span></span>
                            </a>
                        </li>
                        <li>
                            <a class="icon_exit exit_link"
                               href="<?= URL::site(I18n::$lang . '/privatecoach/user/logout') ?>">
                                <span class="link "><span class="ls"><?= __('Выйти') ?></span></span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div id="nav_main" class="privatecoach_nav_main">
                    <?= Request::factory(Route::get('privatecoach_menu')->uri(array('controller' => 'Menu', 'action' => 'build', 'id' => 'change_yourself')))->execute(); ?>
                </div>
                <? if (strstr(Request::current()->url(), 'privatecoach/user') && (!strstr(Request::current()->url(), 'user/gallery'))): ?>
                    <h1 class="privatecoach_tit_h1"><?= __('Изменить данные') ?></h1>

                    <div id="nav_main">
                        <?= Request::factory(Route::get('privatecoach_menu')->uri(array('controller' => 'Menu', 'action' => 'build', 'id' => 'profile')))->execute(); ?>
                    </div>
                <? endif ?>
            </div>
        </div>
    <? else: ?>
        <div class="container">
            <div id="nav_main" class="privatecoach_nav_main">
                <?= Request::factory(Route::get('privatecoach_menu')->uri(array('controller' => 'Menu', 'action' => 'build', 'id' => 'change_yourself')))->execute(); ?>
            </div>
        </div>
    <?endif ?>
<?endif ?>



<div id="content">
    <div class="container">
        <?= $page->content() ?>
    </div>
</div>

<div id="footer">
<? if (!(strstr(Request::current()->url(), '/partner/list'))): ?>
    <div id="partner_footer">
        <div class="container">
            <? if ((strstr(Request::current()->url(), 'confederation'))): ?>
                <?= Request::factory('confederation/partner/request/preview')->execute() ?>
            <? endif ?>
            <? if ((strstr(Request::current()->url(), $fed_id))): ?>
                <?= Request::factory($fed_id . '/partner/request/preview')->execute() ?>
            <? endif ?>
            <? if ((strstr(Request::current()->url(), 'privatecoach')) && !(strstr(Request::current()->url(), 'user'))): ?>
                <?= Request::factory('confederation/partner/request/preview')->execute() ?>
            <? endif ?>
            <? if ((strstr(Request::current()->url(), 'user'))): ?>
                <?= Request::factory('confederation/partner/request/preview')->execute() ?>
            <? endif ?>
        </div>
    </div>
<? endif ?>
<div id="nav_footer" class="pattern_bg">
    <div class="container">
        <ul class="unstyled inline white_link nav_column footer_list_link">
            <li class="column pull-left column1">
                <ul class="unstyled">

                    <li class="h6"><span
                            class="white ls"><?= __('Контакты') ?></span></li>
                    <li>
                        <? if (URL::site('confederation/page/contact') == (I18n::$lang != 'kz' ? '/' . I18n::$lang : null) . Helper::currentURI()): ?>
                            <a href="<?= URL::site('confederation/page/contact') ?>" rel="nofollow" class="link"><span
                                    class="ls"><?= __('Конфедерация') ?></span></a>
                        <? else: ?>
                            <a href="<?= URL::site('confederation/page/contact') ?>" class="link"><span
                                    class="ls"><?= __('Конфедерация') ?></span></a>
                        <?endif ?>
                    </li>
                    <li>
                        <? if (URL::site('confederation/page/all-contacts') == (I18n::$lang != 'kz' ? '/' . I18n::$lang : null) . Helper::currentURI()): ?>
                            <a href="<?= URL::site('confederation/page/all-contacts') ?>" rel="nofollow"
                               class="link"><span
                                    class="ls"><?= __('Федерации') ?></span></a>
                        <? else: ?>
                            <a href="<?= URL::site('confederation/page/all-contacts') ?>" class="link"><span
                                    class="ls"><?= __('Федерации') ?></span></a>
                        <?endif ?>
                    </li>
                </ul>
            </li>
            <li class="column pull-left column2">
                <ul class="unstyled">
                    <li class="h6"><span
                            class="white ls"><?= __('Инфоцентр') ?></span></li>

                    <li>
                        <? if (URL::site('confederation/news') == (I18n::$lang != 'kz' ? '/' . I18n::$lang : null) . Helper::currentURI()): ?>
                            <a href="<?= URL::site('confederation/news') ?>" rel="nofollow"
                               class="link"><span
                                    class="ls"><?= __('Новости') ?></span></a>
                        <? else: ?>
                            <a href="<?= URL::site('confederation/news') ?>" class="link"><span
                                    class="ls"><?= __('Новости') ?></span></a>
                        <?endif ?>
                    </li>
                    <li>
                        <? if (URL::site('confederation/infocenter/gallery') == (I18n::$lang != 'kz' ? '/' . I18n::$lang : null) . Helper::currentURI()): ?>
                            <a href="<?= URL::site('confederation/infocenter/gallery') ?>" rel="nofollow"
                               class="link"><span
                                    class="ls"><?= __('Фото и видео') ?></span></a>
                        <? else: ?>
                            <a href="<?= URL::site('confederation/infocenter/gallery') ?>" class="link"><span
                                    class="ls"><?= __('Фото и видео') ?></span></a>
                        <?endif ?>
                    </li>
                    <li>
                        <? if (URL::site('confederation/infocenter/pressservice') == (I18n::$lang != 'kz' ? '/' . I18n::$lang : null) . Helper::currentURI()): ?>
                            <a href="<?= URL::site('confederation/infocenter/pressservice') ?>" rel="nofollow"
                               class="link"><span
                                    class="ls"><?= __('Информация для СМИ') ?></span></a>
                        <? else: ?>
                            <a href="<?= URL::site('confederation/infocenter/pressservice') ?>" class="link"><span
                                    class="ls"><?= __('Информация для СМИ') ?></span></a>
                        <?endif ?>
                    </li>
                    <li>
                        <? if (URL::site('confederation/partner') == (I18n::$lang != 'kz' ? '/' . I18n::$lang : null) . Helper::currentURI()): ?>
                            <a href="<?= URL::site('confederation/page/partners') ?>" rel="nofollow" class="link"><span
                                    class="ls"><?= __('Партнеры') ?></span></a>
                        <? else: ?>
                            <a href="<?= URL::site('confederation/page/partners') ?>" class="link"><span
                                    class="ls"><?= __('Партнеры') ?></span></a>
                        <?endif ?>
                    </li>

                </ul>
            </li>
            <li class="column pull-left column3">
                <ul class="unstyled">
                    <li class="h6">
                        <? if (URL::site('confederation/event') == (I18n::$lang != 'kz' ? '/' . I18n::$lang : null) . Helper::currentURI()): ?>
                            <a href="<?= URL::site('confederation/event') ?>" rel="nofollow" class="link"><span
                                    class="ls"><?= __('Соревнования') ?></span></a>
                        <? else: ?>
                            <a href="<?= URL::site('confederation/event') ?>" class="link"><span
                                    class="ls"><?= __('Соревнования') ?></span></a>
                        <?endif ?>
                    </li>
                    <li>
                        <? if (URL::site('box/event/' . date('Y') . '/' . date('n') . '/all/22') == (I18n::$lang != 'kz' ? '/' . I18n::$lang : null) . Helper::currentURI()): ?>
                            <a href="<?= URL::site('box/event/' . date('Y') . '/' . date('n') . '/all/22') ?>" rel="nofollow"
                               class="link"><span
                                    class="ls"><?= __('Бокс') ?></span></a>
                        <? else: ?>
                            <a href="<?= URL::site('box/event/' . date('Y') . '/' . date('n') . '/all/22') ?>" class="link"><span
                                    class="ls"><?= __('Бокс') ?></span></a>
                        <?endif ?>
                    </li>
                    <li>
                        <? if (URL::site('wrestling/event/' . date('Y') . '/' . date('n') . '/all/26') == (I18n::$lang != 'kz' ? '/' . I18n::$lang : null) . Helper::currentURI()): ?>
                            <a href="<?= URL::site('wrestling/event/' . date('Y') . '/' . date('n') . '/all/26') ?>" rel="nofollow"
                               class="link"><span class="ls"><?= __('Борьба') ?></span></a>
                        <? else: ?>
                            <a href="<?= URL::site('wrestling/event/' . date('Y') . '/' . date('n') . '/all/26') ?>" class="link"><span
                                    class="ls"><?= __('Борьба') ?></span></a>
                        <?endif ?>
                    </li>
                    <li>
                        <? if (URL::site('judo/event/' . date('Y') . '/' . date('n') . '/all/25') == (I18n::$lang != 'kz' ? '/' . I18n::$lang : null) . Helper::currentURI()): ?>
                            <a href="<?= URL::site('judo/event/' . date('Y') . '/' . date('n') . '/all/25') ?>" rel="nofollow" class="link"><span
                                    class="ls"><?= __('Дзюдо') ?></span></a>
                        <? else: ?>
                            <a href="<?= URL::site('judo/event/' . date('Y') . '/' . date('n') . '/all/25') ?>" class="link"><span
                                    class="ls"><?= __('Дзюдо') ?></span></a>
                        <?endif ?>
                    </li>
                    <li>
                        <? if (URL::site('taekwondo/event/' . date('Y') . '/' . date('n') . '/all/24') == (I18n::$lang != 'kz' ? '/' . I18n::$lang : null) . Helper::currentURI()): ?>
                            <a href="<?= URL::site('taekwondo/event/' . date('Y') . '/' . date('n') . '/all/24') ?>" rel="nofollow"
                               class="link"><span class="ls"><?= __('Таеквондо') ?></span></a>
                        <? else: ?>
                            <a href="<?= URL::site('taekwondo/event/' . date('Y') . '/' . date('n') . '/all/24') ?>" class="link"><span
                                    class="ls"><?= __('Таеквондо') ?></span></a>
                        <?endif ?>
                    </li>
                    <li>
                        <? if (URL::site('weightlifting/event/' . date('Y') . '/' . date('n') . '/all/23') == (I18n::$lang != 'kz' ? '/' . I18n::$lang : null) . Helper::currentURI()): ?>
                            <a href="<?= URL::site('weightlifting/event/' . date('Y') . '/' . date('n') . '/all/23') ?>" rel="nofollow"
                               class="link"><span class="ls"><?= __('Тяжелая атлетика') ?></span></a>
                        <? else: ?>
                            <a href="<?= URL::site('weightlifting/event/' . date('Y') . '/' . date('n') . '/all/23') ?>" class="link"><span
                                    class="ls"><?= __('Тяжелая атлетика') ?></span></a>
                        <?endif ?>
                    </li>
                </ul>
            </li>
            <li class="column pull-left column4">
                <ul class="unstyled">
                    <li class="h6"><span
                            class="white ls"><?= __('Сайты федераций') ?></span></li>
                    <li><a href="http://www.kfb.kz" class="link"><span
                                class="ls"><?= __('Казахстанская федерация бокса') ?></span></a></li>
                    <li><a href="http://www.wrestling.kz "" class="link"><span
                            class="ls"><?= __('Федерация греко-римской, вольной и женской борьбы') ?></span></a>
                    </li>
                    <li><a href="http://fdk.kz" class="link"><span
                                class="ls"><?= __('Федерация дзюдо') ?></span></a></li>
                    <li><a href="http://wfrk.kz" class="link"><span
                                class="ls"><?= __('Федерация тяжёлой атлетики') ?></span></a></li>
                    <li><a href="http://kaztkd.kz/" class="link"><span
                                class="ls"><?= __('Федерация таеквондо (WTF) Республики Казахстан') ?></span></a>
                    </li>
                </ul>
            </li>
            <!--                <li class="column pull-left">-->
            <!--                    <ul class="unstyled">-->
            <!--                        <li class="h6"><a href="-->
            <?//= URL::site('privatecoach') ?><!--" class="link"><span-->
            <!--                                        class="ls">-->
            <?//= __('Личный тренер') ?><!--</span></a></li>-->
            <!--                    </ul>-->
            <!--                </li>-->
        </ul>
    </div>
</div>


<div id="bottom_info">
    <div class="container">
        <div class="row_fluid">
                <span class="pull-left copyright">
                    © <?= date('Y') . ' ' . __('Конфедерация спортивных единоборств и силовых видов спорта') ?>
                </span>
                <span class="pull-right">
                    <a href="mailto:info@confederation.kz" class="link"><span
                            class="ls" style="font-weight: bold;"><?= __('Напишите нам') ?></span></a>
                    <a href="<?= URL::site($fed_id ? $fed_id . '/page/ad' : 'confederation/page/ad') ?>"
                       class="link"><span class="ls"><?= __('Рекламодателю') ?></span></a>
                    <? if ($full_size): ?>
                        <a href="?site_version=mobile" class="link size_link" style="display: none"><span
                                class="ls"><?= __('Мобильная версия') ?></span></a>
                    <? else: ?>
                        <a href="?site_version=full" class="link size_link" style="display: none"><span
                                class="ls"><?= __('Полная версия') ?></span></a>

                    <? endif; ?>
                    <a href="http://kaznetmedia.kz" class="logo_knm">kaznetmedia</a>
                       <a href="http://kaznetmedia.kz/" target="_blank"
                          title="kaznetmedia">
                           <img src="<?= URL::site('images/logo_partner/logo_award.png') ?>"
                                alt="kaznetmedia" style="height: 60px; margin-left:25px">
                       </a>
                </span>
            <input id="date-picker" style="display: none" value="<?= I18n::$lang ?>">
        </div>
    </div>
</div>
</div>
<!-- #footer -->

</div>

<?php
/*if (!Helper::is_production() || isset($_REQUEST['showprofiler'])) {
    echo View::factory('profiler/stats');
}
*/
?>

<?= $page->get_scripts() ?>
<? if (Helper::is_production()): ?>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter22160996 = new Ya.Metrika({id: 22160996,
                        webvisor: true,
                        clickmap: true,
                        trackLinks: true});
                } catch (e) {
                }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="//mc.yandex.ru/watch/22160996" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
<? endif; ?>
</body>
</html>