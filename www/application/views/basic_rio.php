<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Rio 2016</title>
    <meta name="description" content="МЛ">
    <meta name="viewport" content="width=1250, initial-scale=0.2">

    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="favicon.png" type="image/png">

    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?=URL::site('css/style_rio.css');?>">

    <script src="<?=URL::site('js/jquery-1.10.1.min.js');?>"></script>
    <script src="<?=URL::site('js/modernizr-2.6.2-respond-1.1.0.min.js');?>"></script>

    <!--[if lt IE 9]>
    <script src="<?=URL::site('js/fallback/selectivizr-min.js');?>"></script>
    <script src="<?=URL::site('js/fallback/jquery.backgroundSize.js');?>"></script>
    <![endif]-->
    <!--[if lte IE 9]><script src="<?=URL::site('js/fallback/jquery.placeholder.min.js');?>"></script><![endif]-->
    <script src="<?=URL::site('js/common.js');?>"></script>
    <script src="<?=URL::site('js/plugins/owl.carousel2-min.js');?>"></script>
    <script src="<?=URL::site('js/plugins/jquery.plugin.min.js');?>"></script>
    <script src="<?=URL::site('js/plugins/jquery.countdown.min.js');?>"></script>
    <script src="<?=URL::site('js/index_rio.js');?>"></script>
    <script src="<?=URL::site('js/yashare.js');?>"></script>

</head>
<body>
<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<div class="wrapper">

<section class="l-section-wrap header-wrap">
    <div class="header-back left" style="background-image: url('<?=URL::site('images/rio2016/header-back-left.png');?>');"></div>
    <div class="header-back right" style="background-image: url('<?=URL::site('images/rio2016/header-back-right.png');?>');"></div>
    <div class="l-section header">
        <a href="<?=URL::site('rio2016')?>" class="logo"><img src="<?=URL::site('images/rio2016/logo.png');?>" alt=""></a>
        <a href="<?=URL::site('rio2016')?>" class="logo-rio"><img src="<?=URL::site('images/rio2016/logo-rio.png');?>" alt=""></a>
    </div>
</section>

<section class="l-section-wrap">
    <div class="l-centered timer-section">
        <div class="timer-caption">
            <span class="dark-text"><?=__('Быть первым!');?></span> <span class="light-text"><?=__('Быть лучшим!');?></span>
        </div>
        <div id="timer" class="timer"></div>
        <div class="timer-text">
            <span class="counter-text"><?=__('До олимпийских игр в Рио-де-Жанейро');?></span>
        </div>
    </div>
</section>
    <?= $page->content() ?>


    <section class="l-section-wrap sport-section-wrap" style="background-image: url('<?=URL::site('images/rio2016/sport-section-back.jpg');?>');">
        <div class="l-section sport-section">
            <div class="sport-logos-wrap">
                <div class="sport-logo">
                    <div class="sport-img-wrap">
                        <img src="<?=URL::site('images/rio2016/sport1.png');?>" alt="" class="sport-img">
                    </div>
                    <div class="sport-img-hover-wrap">
                        <img src="<?=URL::site('images/rio2016/sport1-hover.png');?>" alt="" class="sport-img-hover">
                    </div>
                    <div class="sport-name"><?=__('Бокс');?></div>
                    <ul class="sport-nav">
                        <?/*Почему-то если вставить весь урл в URL::site режется язык*/?>
                        <li><a href="<?=URL::site('/');?>/rio2016/box/sportsmen/list"><?=__('Олимпийская сборная');?></a></li>
                        <li><a href="<?=URL::site('/')?>/rio2016/box/coach/list""><?=__('Тренерский состав');?></a></li>
                        <li><a href="<?=URL::site('rio2016/box/event/list');?>"><?=__('Бокс в РИО');?></a></li>
                    </ul>
                </div>
                <div class="sport-logo">
                    <div class="sport-img-wrap">
                        <img src="<?=URL::site('images/rio2016/sport2.png');?>" alt="" class="sport-img">
                    </div>
                    <div class="sport-img-hover-wrap">
                        <img src="<?=URL::site('images/rio2016/sport2-hover.png');?>" alt="" class="sport-img-hover">
                    </div>
                    <div class="sport-name"><?=__('Борьба');?></div>
                    <ul class="sport-nav">
                        <li><a href="<?=URL::site('/')?>/rio2016/wrestling/sportsmen/list"><?=__('Олимпийская сборная');?></a></li>
                        <li><a href="<?=URL::site('/')?>/rio2016/wrestling/coach/list""><?=__('Тренерский состав');?></a></li>
                        <li><a href="<?=URL::site('rio2016/wrestling/event/list');?>"><?=__('Борьба в РИО');?></a></li>
                    </ul>
                </div>
                <div class="sport-logo">
                    <div class="sport-img-wrap">
                        <img src="<?=URL::site('images/rio2016/sport3.png');?>" alt="" class="sport-img">
                    </div>
                    <div class="sport-img-hover-wrap">
                        <img src="<?=URL::site('images/rio2016/sport3-hover.png');?>" alt="" class="sport-img-hover">
                    </div>
                    <div class="sport-name"><?=__('Дзюдо');?></div>
                    <ul class="sport-nav">
                        <li><a href="<?=URL::site('/')?>/rio2016/judo/sportsmen/list"><?=__('Олимпийская сборная');?></a></li>
                        <li><a href="<?=URL::site('/')?>/rio2016/judo/coach/list""><?=__('Тренерский состав');?></a></li>
                        <li><a href="<?=URL::site('rio2016/judo/event/list');?>"><?=__('Дзюдо в РИО');?></a></li>
                    </ul>
                </div>
                <div class="sport-logo">
                    <div class="sport-img-wrap">
                        <img src="<?=URL::site('images/rio2016/sport4.png');?>" alt="" class="sport-img">
                    </div>
                    <div class="sport-img-hover-wrap">
                        <img src="<?=URL::site('images/rio2016/sport4-hover.png');?>" alt="" class="sport-img-hover">
                    </div>
                    <div class="sport-name"><?=__('Таеквондо');?></div>
                    <ul class="sport-nav">
                        <li><a href="<?=URL::site('/')?>/rio2016/taekwondo/sportsmen/list"><?=__('Олимпийская сборная');?></a></li>
                        <li><a href="<?=URL::site('/')?>/rio2016/taekwondo/coach/list"><?=__('Тренерский состав');?></a></li>
                        <li><a href="<?=URL::site('rio2016/taekwondo/event/list');?>"><?=__('Таеквондо в РИО');?></a></li>
                    </ul>
                </div>
                <div class="sport-logo">
                    <div class="sport-img-wrap">
                        <img src="<?=URL::site('images/rio2016/sport5.png');?>" alt="" class="sport-img">
                    </div>
                    <div class="sport-img-hover-wrap">
                        <img src="<?=URL::site('images/rio2016/sport5-hover.png');?>" alt="" class="sport-img-hover">
                    </div>
                    <div class="sport-name"><?=__('Тяжелая атлетика');?></div>
                    <ul class="sport-nav">
                        <li><a href="<?=URL::site('/')?>/rio2016/weightlifting/sportsmen/list"><?=__('Олимпийская сборная');?></a></li>
                        <li><a href="<?=URL::site('/')?>/rio2016/weightlifting/coach/list""><?=__('Тренерский состав');?></a></li>
                        <li><a href="<?=URL::site('rio2016/weightlifting/event/list');?>"><?=__('Тяжелая атлетика в РИО');?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

<section class="l-section-wrap footer-sectoin-wrap">
    <div class="l-section footer-section">
        <div class="footer-head"><?=__('Титульные спонсоры');?></div>
        <div class="sponsor-logos">
            <a href="#" class="sponsor-logo"><img src="<?=URL::site('images/rio2016/sponsor1.png');?>" alt=""></a>
            <a href="#" class="sponsor-logo"><img src="<?=URL::site('images/rio2016/sponsor2.png');?>" alt=""></a>
        </div>
    </div>
</section>

</div>
<!-- /wrapper -->

</body>
</html>
