<div class="control-group <?= $error ? 'error' : '' ?>">
    <?= Form::label($field, __($label), array('class' => 'control-label')) ?>
    <?php
    $attr = Arr::get($params, 'attr', array());
    $attr = $attr + array(
            'id' => $field,
            'class' => 'span8',
            'multiple' => 'multiple',
            'type' => 'file'
        );
    ?>
    <div class="controls">
        <?= Form::input("files[]", null, $attr) ?>
        <span class="help-inline"><?= $error ?></span>
    </div>
</div>