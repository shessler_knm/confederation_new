<div class="main-col-right">
    <div class="slider2-wrap">
        <div class="slider2 owl-carousel">
            <? foreach($quotes as $key => $quote): ?>
                <?php $image = $quote->photo_s;?>
                <div class="item" data-text="<?=$key?>"><img src="<?=$image->dir.$image->name.'.'.$image->type;?>" alt="<?=$quote->title;?>"></div>
            <? endforeach; ?>
        </div>
    </div>
    <div class="slider2-texts">
        <?foreach($quotes as $key => $quote): ?>
            <div class="js-slider2-text text<?=$key?>" <? if ($key != 0) { ?>style="display: none;"<? } ?>>
                <div class="slider2-quote"><?=$quote->quote;?></div>
                <div class="slider2-name"><?=$quote->title;?></div>
            </div>
        <? endforeach; ?>
    </div>
</div>