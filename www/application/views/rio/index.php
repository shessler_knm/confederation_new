<section class="l-section-wrap slider-section-wrap">
    <div class="slider-back-wrap">
        <div class="slider-back-inner" style="background-image: url('<?=URL::site('images/rio2016/slider-back.jpg');?>');">
        </div>
    </div>
    <div class="l-centered slider-section clear-wrap">
        <?= Request::factory('confederation/news/request/widget_rio')->query('query', true)->execute() ?>
        <!-- /col -->
        <?= Request::factory('confederation/event/request/preview_rio')->query('query', true)->execute() ?>
    </div>
</section>


<section class="l-section-wrap">
    <div class="l-section news-section clear-wrap">
        <?= Request::factory('confederation/news/request/confederation_preview_rio')->execute() ?>
        <!-- /col -->
        <div class="main-col-right slider2-col">
            <div class="slider2-wrap">
                <div class="slider2 owl-carousel">
                    <? foreach($quotes as $key => $quote): ?>
                        <?php $image = $quote->photo_s;?>
                        <div class="item" data-text="<?=$key?>"><img src="<?=$image->dir.$image->name.'.'.$image->type;?>" alt="<?=$quote->title;?>"></div>
                    <? endforeach; ?>
                </div>
            </div>

            <div class="slider2-texts">
                <?foreach($quotes as $key => $quote): ?>
                    <div class="js-slider2-text text<?=$key?>" <? if ($key != 0) { ?>style="display: none;"<? } ?>>
                        <div class="slider2-quote"><?=$quote->quote;?></div>
                        <div class="slider2-name"><?=$quote->title;?></div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
        <!-- /col -->
    </div>
</section>