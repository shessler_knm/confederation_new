<div class="partner_section">
    <h3><?= __('Титульные спонсоры') ?></h3>
    <ul class="unstyled inline partner_list hr_partner big_partner">
        <? if (I18n::lang() == 'ru'): ?>
            <li class="column col60">
                <a href="http://sk.kz/" target="_blank" title="Информационно-аналитический портал АО «Самрук-Казына»">
                    <img src="<?= URL::site('images/logo_partner/samruk_rus.png') ?>"
                         alt="Информационно-аналитический портал АО «Самрук-Казына»" style="height: 85px">
                </a>
            </li>
        <? endif ?>
        <? if (I18n::lang() == 'en'): ?>
            <li class="column col60">
                <a href="http://sk.kz/" target="_blank" title="Информационно-аналитический портал АО «Самрук-Казына»">
                    <img src="<?= URL::site('images/logo_partner/samruk_eng.png') ?>"
                         alt="Информационно-аналитический портал АО «Самрук-Казына»" style="height: 85px">
                </a>
            </li>
        <? endif ?>
        <? if (I18n::lang() == 'kz'): ?>
            <li class="column col60">
                <a href="http://sk.kz/" target="_blank" title="Информационно-аналитический портал АО «Самрук-Казына»">
                    <img src="<?= URL::site('images/logo_partner/samruk_kaz.png') ?>"
                         alt="Информационно-аналитический портал АО «Самрук-Казына»" style="height: 85px">
                </a>
            </li>
        <? endif ?>
        <li class="column">
            <a href="http://www.kmg.kz/" target="_blank" title="АО НК «КазМунайГаз»">
                <img src="<?= URL::site('images/logo_partner/logo19.png') ?>" alt="АО НК «КазМунайГаз»" style="height: 85px">
            </a>
        </li>
    </ul>
</div>

<div class="partner_section">
    <h3><?= __('Сотрудничество') ?></h3>
    <ul class="unstyled inline nav_column partner_list column_partner">
        <li class="column pull-left center">
            <a href="http://www.olympic.kz/" target="_blank"
               title="Национальный Олимпийский комитет Республики Казахстан">
                <img src="<?= URL::site('images/logo_partner/logo16.png') ?>"
                     alt="Национальный Олимпийский комитет Республики Казахстан"  style="height: 65px">
            </a>
        </li>
        <li class="column pull-left center">
            <a href="http://sport.gov.kz/" target="_blank"
               title="Қазақстан Республикасы Cпорт және Дене Шынықтыру істері агенттігі.">
                <img src="<?= URL::site('images/logo_partner/agenstvonew.png') ?>"
                     alt="Қазақстан Республикасы Cпорт және Дене Шынықтыру істері агенттігі." style="height: 85px">
            </a>
        </li>
        <li class="column pull-left center">
            <a class="no_link" href="http://www.ocasia.org" target="_blank">
                <img src="<?= URL::site('images/logo_partner/olympic-council.png') ?>" alt="Олимпийский Совет Азии" style="height: 70px">
            </a>
        </li>
        <li class="column pull-left center">
            <a href="http://www.sportaccord.com/en/" target="_blank"
               title="Sportaccord">
                <img src="<?= URL::site('images/logo_partner/sa-logo.png') ?>" alt="Sportaccord" style="height: 60px">
            </a>
        </li>

        <li class="column pull-left center">
            <a class="no_link" href="http://www.aiba.org" target="_blank">
                <img src="<?= URL::site('images/logo_partner/aiba.png') ?>"
                     alt="Международная ассоциация любительского бокса (AIBA)" style="height: 70px">
            </a>
        </li>
        <li class="column pull-left center">
            <a class="no_link" href="http://www.ijf.org" target="_blank"><img
                    src="<?= URL::site('images/logo_partner/ijf.png') ?>" alt="Международная федерация дзюдо (IJF)"
                    style="height: 85px">
            </a>
        </li>
        <li class="column pull-left center">
            <a class="no_link" href="http://www.iwf.net" target="_blank"><img
                    src="<?= URL::site('images/logo_partner/new_iwf_logo.png') ?>"
                    alt="Международная федерация тяжелой атлетики (IWF)" style="height: 85px">
            </a>
        </li>
        <li class="column pull-left center">
            <a class="no_link" href="http://www.worldtaekwondofederation.net" target="_blank">
                <img src="<?= URL::site('images/logo_partner/logo.wtf.png') ?>"
                     alt="Всемирная Федерация таеквондо (WTF)" style="height: 70px">
            </a>
        </li>
        <li class="column pull-left center">
            <a class="no_link" href="http://fila-official.com" target="_blank">
                <img src="<?= URL::site('images/logo_partner/uww.png') ?>"
                     alt="United World Wrestling"  style="height: 70px">
            </a>
        </li>
        <li class="column pull-left center">
            <a class="no_link" href="http://www.bolashak.gov.kz/index.php/ru/" target="_blank">
                <img src="<?= URL::site('images/logo_partner/bolashak.png') ?>"
                    alt="Акционерное общество «Центр международных программ «Болашак»"  style="height: 80px">
            </a>
        </li>
    </ul>
</div>

<div class="partner_section">
    <h3><?= __('Спонсоры') ?></h3>
    <ul class="unstyled inline nav_column partner_list column_partner">

        <? if (I18n::lang() == 'kz'): ?>
            <li class="column pull-left center">
                <a href="http://www.huawei.com/kz/" target="_blank">
                    <img src="<?= URL::site('images/logo_partner/huawei.png') ?>" alt="" style="height:80px;">
                </a>
            </li>
        <? endif ?>

        <? if (I18n::lang() == 'en'): ?>
            <li class="column pull-left center">
                <a href="http://www.huawei.com/en/" target="_blank">
                    <img src="<?= URL::site('images/logo_partner/huawei.png') ?>" alt="" style="height:80px;">
                </a>
            </li>
        <? endif ?>

        <? if (I18n::lang() == 'ru'): ?>
            <li class="column pull-left center">
                <a href="http://www.huawei.com/ru/" target="_blank">
                    <img src="<?= URL::site('images/logo_partner/huawei.png') ?>" alt="" style="height:80px;">
                </a>
            </li>
        <? endif ?>

        <li class="column pull-left center">
            <a href="http://www.sat.kz/" target="_blank">
                <img src="<?= URL::site('images/logo_partner/satco.png') ?>" alt="" style="height:75px;">
            </a>
        </li>

    </ul>
</div>


<div class="partner_section">
    <h3><?= __('Партнёры') ?></h3>
    <ul class="unstyled inline nav_column partner_list">
        <li class="column pull-left last_column">
            <a href="http://kazsporttv.kz" target="_blank"
               title="Kazsport">
                <img src="<?= URL::site('images/logo_partner/Kazsport_logo.png') ?>"
                     alt="Kazsport" style="height: 70px">
            </a>
        </li>

        <li class="column pull-left">
            <a href="http://kaznetmedia.kz/" target="_blank"
               title="kaznetmedia">
                <img src="<?= URL::site('images/knm_logo.png') ?>"
                     alt="kaznetmedia" style="height: 60px">
            </a>
        </li>

        <li class="column pull-left">
            <a href="http://www.logycom.kz/" target="_blank">
                <img src="<?= URL::site('images/logycom.png') ?>"
                     alt="LogyCom" style="height: 60px">
            </a>
        </li>

        <li class="column pull-left center">
            <a href="http://www.telecom.kz" target="_blank"
               title="telecom">
                <img src="<?= URL::site('images/logo_partner/ktc.png') ?>" alt="telecom" style="height: 40px">
            </a>
        </li>

        <li class="column pull-left">
            <a href="http://www.ramadaplazaastana.kz/" target="_blank">
                <img src="<?= URL::site('storage/d2/d22b1ab1eb826286fbd5e5e8a100a28e.png') ?>"
                     alt="ramadaplaza" style="height: 60px">
            </a>
        </li>

        <li class="column pull-left">
            <a href="http://www.tengrihotel.kz/" target="_blank">
                <img src="<?= URL::site('storage/10/10ec978a28c05a5bc59259142fc195e4.png') ?>"
                     alt="tengri hotel" style="height: 60px">
            </a>
        </li>

        <li class="column pull-left">
            <a href="http://www.grandsapphirehotel.com/ru/?sid=85s9ihghkaeqmti538ldtga1b7" target="_blank">
                <img src="<?= URL::site('storage/92/9213d55b5de308e94c90995eac1b9e1a.png') ?>"
                     alt="grand sapphire hotel" style="height: 60px">
            </a>
        </li>

        <li class="column pull-left">
            <a href="http://dumanhotel.awardspace.com/" target="_blank">
                <img src="<?= URL::site('storage/1d/1de6cd469fb19fc2225d1b905d03a50c.png') ?>"
                     alt="duman hotel" style="height: 60px">
            </a>
        </li>

        <li class="column pull-left">
            <a href="http://www.group.kz/hotel/hotel.html" target="_blank">
                <img src="<?= URL::site('storage/bf/bfaa6ac6b24b50c38ae0f1095f703dff.png') ?>"
                     alt="Отрар hotel" style="height: 60px">
            </a>
        </li>

    </ul>
</div>