<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <div class="page_section">
                <div class="text-left">

<!-- EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN -->
<div class="page_partner_list">
    <h2>Title Sponsors</h2>
    <ul class="unstyled inline partner_list big_partner">
        <li class="column col60"><a class="no_link" href="http://sk.kz/" target="_blank"><img src="http://confederation.kz/storage/1e/1e88ea8285673d66344d826e2c726f73.png" alt="Samruk-Kazyna"  style="height:100px;"></a><div class="title center">JSC “Sovereign wealth fund <br> “Samruk-Kazyna”</div></li>
        <li class="column"><a class="no_link" href="http://www.kmg.kz/" target="_blank"><img src="http://confederation.kz/storage/6a/6a3d5ba1e26e5782218e0fba8792bd1c.png" alt="KazMunayGas"  style="height:100px;"></a><div class="title center">JSC "National company <br> "KazMunayGaz"</div></li>
    </ul>
    <h2></h2>
    <h2>Cooperation</h2>
    <ul class="unstyled inline partner_list column_partner">
        <li class="column center"><a class="no_link" href="http://www.olympic.kz/" target="_blank"><img src="http://confederation.kz/storage/2c/2c612480908ab1e3cc6937066183cc7f.jpeg" alt="National Olympic Committee of the Republic of Kazakhstan" style="height:90px;"></a><div class="title center">National Olympic Committee <br> of the Republic of Kazakhstan</div></li>
        <li class="column center"><a class="no_link" href="http://sport.gov.kz/ru/" target="_blank"><img src="http://confederation.kz/storage/8f/8f69ebf2faa9c790e9779b748d526c5b.jpeg" alt="Sports and Physical Training Agency of the Republic of Kazakhstan" style="height:90px;"></a><div class="title center">Sports and Physical Training <br> Agency of the Republic <br> of Kazakhstan</div></li>
        <li class="column center"><a class="no_link" href="http://www.ocasia.org/" target="_blank"><img src="http://confederation.kz/storage/dc/dc807c0fe45d37e5d28fb46624e130d5.png" alt="Olympic Council of Asia" style="height:90px;"></a><div class="title center">Olympic Council of Asia</div></li>
        <li class="column center"><a class="no_link" href="http://www.sportaccord.com/en/" target="_blank"><img src="http://confederation.kz/storage/bb/bbc59f5c7ecb40c69065d16538145e65.png" alt="International SportAccord Convention" style="height:90px;"></a><div class="title center">International SportAccord <br> Convention</div></li>
        <li class="column center"><a class="no_link" href="http://www.aiba.org/" target="_blank"><img src="http://confederation.kz/storage/be/bec9f4ac18359b16ac6b6bbb48c32a9e.png" alt="International Boxing Association (AIBA)" style="height:90px;"></a><div class="title center">International Boxing <br> Association (AIBA)</div></li>
        <li class="column center"><a class="no_link" href="http://www.ijf.org/" target="_blank"><img src="http://confederation.kz/storage/cd/cdd4e350c0578051397bc923e0290292.png" alt="International Judo Federation (IJF)" style="height:90px;"></a><div class="title center">International Judo <br> Federation (IJF)</div></li>
        <li class="column center"><a class="no_link" href="http://www.iwf.net/" target="_blank"><img src="http://confederation.kz/storage/e4/e4186371c3d525cd6975fe07272d30cc.png" alt="International Weightlifting Federation (IWF)" style="height:90px;"></a><div class="title center">International Weightlifting <br> Federation (IWF)</div></li>
        <li class="column center"><a class="no_link" href="http://www.worldtaekwondofederation.net/" target="_blank"><img src="http://confederation.kz/storage/e8/e83e641a981087ad1b2e732f52e406a5.png" alt="World Taekwondo Federation (WTF)" style="height:90px;"></a><div class="title center">World Taekwondo <br> Federation (WTF)</div></li>
        <li class="column center"><a class="no_link" href="http://fila-official.com/" target="_blank"><img src="http://confederation.kz/storage/58/58536cdb2597cc786eafe6aa4b0638eb.png" alt="INTERNATIONAL FEDERATION OF ASSOCIATED WRESTLING STYLES (FILA)" style="height:90px;"></a><div class="title center">The International <br> Federation of Associated <br> Wrestling Styles (FILA) </div></li>
        <li class="column center"><a class="no_link" href="http://www.bolashak.gov.kz/index.php/ru/" target="_blank"><img src="http://confederation.kz/storage/0b/0b0bec325c27279ae8c79403d7b186a8.png" alt="JSC" style="height:90px;"></a><div class="title center">JSC “Center for International <br> programs Bolashak” </div></li>
    </ul>
    <h2></h2>
    <h2>Sponsors</h2>
    <ul class="unstyled inline partner_list">
        <li class="column col60"><a class="no_link" href="http://lukoil-overseas.ru/" target="_blank"><img src="http://confederation.kz/storage/17/1795b8d17e7f10f7c91c47ae5db500bc.png" alt="Lukoil" style="height:45px;"></a><div class="title center">Филиал <br> «ЛУКОЙЛ Оверсиз Сервис Б.В.» <br> в г. Астана</div></li>
        <li class="column"><a class="no_link" href="http://www.ncoc.kz/ru/default.aspx" target="_blank"><img src="http://confederation.kz/storage/5c/5c636fe83c0dd8a57b295351eb3ec28e.png" alt="NCOC" style="height:70px;"></a><div class="title center">North Caspian Operating <br> Company B.V.</div></li>
    </ul>
    <h2></h2>
    <h2>Partners</h2>
    <ul class="unstyled inline partner_list">
        <li class="column"><a class="no_link" href="http://kazsporttv.kz/ru" target="_blank"><img src="http://confederation.kz/storage/25/25b8ab86d6621f8191d604cf28fdbee0.png" alt="" style="width:170px;"></a><div class="title center">TV channel «KAZsport»</div></li>
    </ul>
</div>
<!-- EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN  EN -->


<!-- KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ -->
<div class="page_partner_list">
    <h2>Титулдық демеушілер</h2>
    <ul class="unstyled inline partner_list big_partner">
        <li class="column col60"><a class="no_link" href="http://sk.kz/" target="_blank"><img src="http://confederation.kz/storage/1e/1e9ba7cea852adaec926b23c4c8f45be.png" alt="Самұрық-Қазына" style="height:100px;"></a><div class="title center">«Самұрық Қазына» әл-ауқат <br> қоры» акционерлік қоғамы</div></li>
        <li class="column"><a class="no_link" href="http://www.kmg.kz/" target="_blank"><img src="http://confederation.kz/storage/6a/6a3d5ba1e26e5782218e0fba8792bd1c.png" alt="ҚазМұнайГаз" style="height:100px;"></a><div class="title center">«ҚазМұнайГаз» Ұлттық <br> компаниясы» акционерлік <br> қоғамы</div></li>
    </ul>
    <h2></h2>
    <h2>Ынтымақтастық</h2>
    <ul class="unstyled inline partner_list column_partner">
        <li class="column center"><a class="no_link" href="http://www.olympic.kz/" target="_blank"><img src="http://confederation.kz/storage/2c/2c612480908ab1e3cc6937066183cc7f.jpeg" alt="Қазақстан Республикасының Ұлттық Олимпиадалық комитетi" style="height:90px;"></a><div class="title center">Қазақстан <br> Республикасының Ұлттық <br> Олимпиадалық Комитеті</div></li>
        <li class="column center"><a class="no_link" href="http://sport.gov.kz/ru/" target="_blank"><img src="http://confederation.kz/storage/8f/8f69ebf2faa9c790e9779b748d526c5b.jpeg" alt="Қазақстан Республикасының Спорт және дене шынықтыру істері агенттігі" style="height:90px;"></a><div class="title center">Қазақстан <br> Республикасының Спорт <br> және дене шынықтыру <br> істері агенттігі</div></li>
        <li class="column center"><a class="no_link" href="http://www.ocasia.org/" target="_blank"><img src="http://confederation.kz/storage/dc/dc807c0fe45d37e5d28fb46624e130d5.png" alt="Азия Олимпиадалық Кеңесі" style="height:90px;"></a><div class="title center">Азия Олимпиадалық <br> Кеңесі</div></li>
        <li class="column center"><a class="no_link" href="http://www.sportaccord.com/en/" target="_blank"><img src="http://confederation.kz/storage/bb/bbc59f5c7ecb40c69065d16538145e65.png" alt="«Спортаккорд» Халықаралық спорт конвенциясы" style="height:90px;"></a><div class="title center">«Спортаккорд» <br> Халықаралық спорт <br> конвенциясы</div></li>
        <li class="column center"><a class="no_link" href="http://www.aiba.org/" target="_blank"><img src="http://confederation.kz/storage/be/bec9f4ac18359b16ac6b6bbb48c32a9e.png" alt="Халықаралық әуесқой бокс федерациясы (AIBA)" style="height:90px;"></a><div class="title center">Халықаралық әуесқой бокс <br> федерациясы (АIBA)</div></li>
        <li class="column center"><a class="no_link" href="http://www.ijf.org/" target="_blank"><img src="http://confederation.kz/storage/cd/cdd4e350c0578051397bc923e0290292.png" alt="Халықаралық дзюдо федерациясы (IJF)" style="height:90px;"></a><div class="title center">Халықаралық дзюдо <br> федерациясы (IJF)</div></li>
        <li class="column center"><a class="no_link" href="http://www.iwf.net/" target="_blank"><img src="http://confederation.kz/storage/e4/e4186371c3d525cd6975fe07272d30cc.png" alt="Халықаралық ауыр атлетика федерациясы (IWF)" style="height:90px;"></a><div class="title center">Халықаралық ауыр  <br> атлетика федерациясы <br> (IWF)</div></li>
        <li class="column center"><a class="no_link" href="http://www.worldtaekwondofederation.net/" target="_blank"><img src="http://confederation.kz/storage/e8/e83e641a981087ad1b2e732f52e406a5.png" alt="Дүниежүзілік таеквондо федерациясы (WTF)" style="height:90px;"></a><div class="title center">Дүниежүзілік таэквондо <br> федерациясы (WTF)</div></li>
        <li class="column center"><a class="no_link" href="http://fila-official.com/" target="_blank"><img src="http://confederation.kz/storage/58/58536cdb2597cc786eafe6aa4b0638eb.png" alt="Күрестің бірлескен стильдері халықаралық федерациясы (FILA)" style="height:90px;"></a><div class="title center">Күрестің бірлескен <br> стильдері халықаралық <br> федерациясы (FILA) <br> </div></li>
        <li class="column center"><a class="no_link" href="http://www.bolashak.gov.kz/index.php/ru/" target="_blank"><img src="http://confederation.kz/storage/0b/0b0bec325c27279ae8c79403d7b186a8.png" alt="«Халықаралық бағдарламалар орталығы» АҚ" style="height:90px;"></a><div class="title center">«Халықаралық <br> бағдарламалар орталығы» <br> АҚ</div></li>
    </ul>
    <h2></h2>
    <h2>Демеуші</h2>
    <ul class="unstyled inline partner_list">
        <li class="column col60"><a class="no_link" href="http://lukoil-overseas.ru/" target="_blank"><img src="http://confederation.kz/storage/17/1795b8d17e7f10f7c91c47ae5db500bc.png" alt="Lukoil" style="height:45px;"></a><div class="title center">Филиал <br> «ЛУКОЙЛ Оверсиз Сервис Б.В.» <br> в г. Астана</div></li>
        <li class="column"><a class="no_link" href="http://www.ncoc.kz/ru/default.aspx" target="_blank"><img src="http://confederation.kz/storage/5c/5c636fe83c0dd8a57b295351eb3ec28e.png" alt="NCOC" style="height:70px;"></a><div class="title center">North Caspian Operating <br> Company B.V.</div></li>
    </ul>
    <h2></h2>
    <h2>Серіктестер</h2>
    <ul class="unstyled inline partner_list">
        <li class="column"><a class="no_link" href="http://kazsporttv.kz/ru" target="_blank"><img src="http://confederation.kz/storage/25/25b8ab86d6621f8191d604cf28fdbee0.png" alt="KAZsport" style="width:170px;"></a><div class="title center">«KAZsport» телеарнасы</div></li>
    </ul>
</div>
<!-- KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ  KZ -->


<!-- RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU -->
<div class="page_partner_list">
    <h2>Титульные спонсоры</h2>
    <ul class="unstyled inline partner_list big_partner">
        <li class="column col60"><a class="no_link" href="http://sk.kz/" target="_blank"><img src="http://confederation.kz/storage/1e/1e88ea8285673d66344d826e2c726f73.png" alt="СамурыкКазына" style="height:100px;"></a><div class="title center">Акционерное общество «Фонд <br> национального благосостояния <br> «Самрук-Казына»</div></li>
        <li class="column"><a class="no_link" href="http://www.kmg.kz/" target="_blank"><img src="http://confederation.kz/storage/6a/6a3d5ba1e26e5782218e0fba8792bd1c.png" alt="КазМунайГаз" style="height:100px;"></a><div class="title center">Акционерное общество <br> «Национальная компания <br> «КазМунайГаз»</div></li>
    </ul>
    <h2></h2>
    <h2>Сотрудничество</h2>
    <ul class="unstyled inline partner_list column_partner">
        <li class="column center"><a class="no_link" href="http://www.olympic.kz" target="_blank"><img src="http://confederation.kz/storage/2c/2c612480908ab1e3cc6937066183cc7f.jpeg" alt="Национальный Олимпийский комитет Республики Казахстан" style="height:90px;"></a><div class="title">Национальный <br> Олимпийский Комитет <br> Республики Казахстан</div></li>
        <li class="column center"><a class="no_link" href="http://sport.gov.kz/ru/" target="_blank"><img src="http://confederation.kz/storage/8f/8f69ebf2faa9c790e9779b748d526c5b.jpeg" alt="Агентство Республики Казахстан по делам спорта и физической культуры" style="height:90px;"></a><div class="title">Агентство Республики <br> Казахстан по делам спорта <br> и физической культуры</div></li>
        <li class="column center"><a class="no_link" href="http://www.ocasia.org" target="_blank"><img src="http://confederation.kz/storage/dc/dc807c0fe45d37e5d28fb46624e130d5.png" alt="Олимпийский Совет Азии" style="height:90px;"></a><div class="title">Олимпийский Совет Азии</div></li>
        <li class="column center"><a class="no_link" href="http://www.sportaccord.com/en/" target="_blank"><img src="http://confederation.kz/storage/bb/bbc59f5c7ecb40c69065d16538145e65.png" alt="Международная конвенция «Спортаккорд»" style="height:90px;"></a><div class="title">Международная спортивная <br> конвенция «Спортаккорд»</div></li>
        <li class="column center"><a class="no_link" href="http://www.aiba.org" target="_blank"><img src="http://confederation.kz/storage/be/bec9f4ac18359b16ac6b6bbb48c32a9e.png" alt="Международная ассоциация любительского бокса (AIBA)" style="height:90px;"></a><div class="title">Международная ассоциация <br> любительского бокса (АIBA)</div></li>
        <li class="column center"><a class="no_link" href="http://www.ijf.org" target="_blank"><img src="http://confederation.kz/storage/cd/cdd4e350c0578051397bc923e0290292.png" alt="Международная федерация дзюдо (IJF)" style="height:90px;"></a><div class="title">Международная федерация <br> дзюдо (IJF)</div></li>
        <li class="column center"><a class="no_link" href="http://www.iwf.net" target="_blank"><img src="http://confederation.kz/storage/e4/e4186371c3d525cd6975fe07272d30cc.png" alt="Международная федерация тяжелой атлетики (IWF)" style="height:90px;"></a><div class="title">Международная федерация <br> тяжелой атлетики (IWF)</div></li>
        <li class="column center"><a class="no_link" href="http://www.worldtaekwondofederation.net" target="_blank"><img src="http://confederation.kz/storage/e8/e83e641a981087ad1b2e732f52e406a5.png" alt="Всемирная Федерация таеквондо (WTF)" style="height:90px;"></a><div class="title">Всемирная Федерация <br> таеквондо (WTF)</div></li>
        <li class="column center"><a class="no_link" href="http://fila-official.com" target="_blank"><img src="http://confederation.kz/storage/58/58536cdb2597cc786eafe6aa4b0638eb.png" alt="Международная федерация объединенных стилей борьбы (FILA)" style="height:90px;"></a><div class="title">Международная федерация <br> объединенных стилей <br> борьбы (FILA)</div></li>
        <li class="column center"><a class="no_link" href="http://www.bolashak.gov.kz/index.php/ru/" target="_blank"><img src="http://confederation.kz/storage/0b/0b0bec325c27279ae8c79403d7b186a8.png" alt="Акционерное общество «Центр международных программ «Болашак»" style="height:90px;"></a><div class="title">Акционерное общество <br> «Центр международных <br> программ «Болашак»</div></li>
    </ul>
    <h2></h2>
    <h2>Спонсоры</h2>
    <ul class="unstyled inline partner_list">
        <li class="column col60"><a class="no_link" href="http://lukoil-overseas.ru/" target="_blank"><img src="http://confederation.kz/storage/17/1795b8d17e7f10f7c91c47ae5db500bc.png" alt="Lukoil" style="height:45px;"></a><div class="title center">Филиал <br> «ЛУКОЙЛ Оверсиз Сервис Б.В.» <br> в г. Астана</div></li>
        <li class="column"><a class="no_link" href="http://www.ncoc.kz/ru/default.aspx" target="_blank"><img src="http://confederation.kz/storage/5c/5c636fe83c0dd8a57b295351eb3ec28e.png" alt="NCOC" style="height:70px;"></a><div class="title center">North Caspian Operating <br> Company B.V.</div></li>
    </ul>
    <h2></h2>
    <h2>Партнеры</h2>
    <ul class="unstyled inline partner_list">
        <li class="column"><a class="no_link" href="http://kazsporttv.kz/ru" target="_blank"><img src="http://confederation.kz/storage/25/25b8ab86d6621f8191d604cf28fdbee0.png" alt="" style="width:170px;"></a><div class="title center">Телеканал «KAZsport»</div></li>
    </ul>
    <h2></h2>
</div>
<!-- RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU  RU -->

                </div>
            </div>
        </div>
    </div>
    <div class="aside">
        <div class="banner">
            <img src="<?= URL::site('images/big_logo.png') ?>" alt="">
        </div>
        <?= View::factory('social_networks') ?>
    </div>
</div>