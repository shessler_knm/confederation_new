<div class="row_fluid clearfix bg-white clmn_right partners">
    <div class="row_section">
        <?php
        echo View::factory('breadcrumbs')->set('_breadcrumbs', $_breadcrumbs)->render();
        ?>
        <h1><?= __('Партнеры')?></h1>
        <div class="section">
            <div class="page_section">
                <div class="aside">
                    <? foreach($categories as $category): ?>
                        <div class="aside-block">
                            <div class="h5"><?=$category->{'title_'.I18n::$lang}?></div>
                            <? foreach ($category->partners->order_by('priority', 'asc')->find_all() as $part): ?>
                                <a class="titul" onclick="get_partner(<?=$part->id?>)"><?= $part->{'title_'.I18n::$lang};?></a>
                            <? endforeach; ?>
                        </div>
                    <? endforeach; ?>
                </div>

                <div class="page_partner_list">
                    <img src="<?= URL::site('images/bx_loader.gif')?>">
                </div>
            </div>

            <div class="social_block" >
                <?=__('Понравился материал? Поделитесь им с друзьями в соцсетях!')?>
                <div id="social_block">

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Этот блок показывать только в партнерах -->

<?php if (count($videos)): ?>
<div id="video-sponsore">
    <h2>Видео от спонсора</h2>
    <div class="spr-slider">
        <?php foreach($videos as $video): ?>
        <?php
            $videoId = explode('v=', $video->{'link_'.I18n::$lang});
        ?>
        <div class="slide">
            <a data-target="#modal" data-toggle="modal" onclick="get_video('<?=$videoId[1];?>','<?=$video->{'title_'.I18n::$lang}?>')" style="cursor: pointer;">
                <span class="video-preview">
                    <img src="http://img.youtube.com/vi/<?=$videoId[1];?>/0.jpg">
                </span>
                <?=$video->{'title_'.I18n::$lang}?>
            </a>
        </div>
        <?php endforeach; ?>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true"style="min-width: 680px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close-modal" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body" style="min-height: 490px;">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close-modal" data-dismiss="modal"><?=__('Закрыть')?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<script type="text/javascript" src="<?=URL::site('js/lib/jquery-1.9.0.min.js')?>"></script>
<script type="text/javascript">
    function get_partner(id) {
        $.ajax({
            dataType: 'json',
            type: 'POST',
            data: {id: id},
            url: '<?=URL::site('/page/get_partner')?>',
            success: function (data) {
                var partner = data.success;
                var html = '<p>' + partner.category + '</p>'+
                    '<p>'+
                        '<img src="' + partner.photo + '" alt="' + partner.title + '" style="height:100px;">'+
                    '</p>'+
                    '<p><b>Веб-сайт: </b><a href="http://' + partner.site + '">' + partner.site + '</a></p>'+
                    '<p>'+
                        partner.text +
                    '</p>';
                $('.page_partner_list').html(html);
            }
        });
    }

    function get_video(videoId, title) {
        $('#myModalLabel').html(title);
        $('.modal-body').html(
            '<iframe width="640" height="480" src="https://www.youtube.com/embed/' + videoId + '" frameborder="0" allowfullscreen></iframe>'
        );
    }
    $(document).ready(function() {
        var el = $('.aside').children().children('a:first');
        el.click();
        $('.close-modal').on('click', function() {
            $('.modal-body').html('');
        });
    });
</script>