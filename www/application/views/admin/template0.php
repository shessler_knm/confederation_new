<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript">
        BASE_URL = "<?=URL::base()?>";
    </script>
    <?=$page->get_head()?>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="navbar">
            <div class="navbar-inner">
                <?if ($user && $user->loaded()): ?>
                <a class="brand" href="<?=$user->edit_url()?>"><?=$user->username?></a>
                <? endif;?>
                <ul class="nav">
                    <li>
                        <a href="<?=URL::site()?>">
                            На сайт                           
                        </a>
                    </li>
                    <?php
                        if (class_exists('Model_Notification')){
                            ?>
                    <li <?=Request::$current->controller() == 'notification' ? 'class="active"' : ''?>>
                        <a href="<?=URL::site('admin/notification/list')?>">
                            Уведомления
                            <?php
                            $notify = Orm::factory('admin_notification')->count_all();
                            if ($notify > 0) {
                                echo '<span class="badge badge-success">'.$notify.'</span>';
                            }
                            ?>
                        </a>
                    </li>
                            <?
                        }
                    ?>
                    <!--                    <li><a href="#">Link</a></li>-->
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="span12">
            <ul class="breadcrumb">
                <?php
                $last = array_pop($crumbs);
                ?>
                <?foreach ($crumbs as $el): ?>
                <li><a href="<?=URL::site($el['uri'])?>"><?=$el['title']?></a> <span class="divider">/</span></li>
                <? endforeach;?>
                <li class="active"><?=$last['title']?></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="span2">
            <?=View::factory("components/menu_admin", array("items" => $menu))?>
        </div>
        <div class="span10">
            <?php
            foreach ($page->read_messages(Cms_Page::PAGE_MESSAGE_NOTICE) as $msg) {
                ?>
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?=$msg?>
                </div>
                <?
            }
            ?>
            <?php
            foreach ($page->read_messages(Cms_Page::PAGE_MESSAGE_WARNING) as $msg) {
                ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?=$msg?>
                </div>
                <?
            }
            ?>
            <?php
            foreach ($page->read_messages(Cms_Page::PAGE_MESSAGE_ERROR) as $msg) {
                ?>
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?=$msg?>
                </div>
                <?
            }
            ?>
            <?=$page->content()?>
        </div>
    </div>

    <div class="navbar" style="margin: 90px 0 0 0; line-height: 42px;">
        <div class="navbar-inner">Разработано в <a href="http://kaznetmedia.kz/">kaznetmedia</a></div>
    </div>

</div>
<?=View::factory('profiler/stats')?>
</body>
</html>