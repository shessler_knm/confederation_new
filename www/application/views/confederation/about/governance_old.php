<div class="row_fluid">
    <div class="row_section">
        <div class="section">
            <h1><?=__('Исполнительный комитет')?></h1>
            <ul class="unstyled coach_list">
                <? foreach ($model as $row):?>
                    <li class="hr_bottom media">
                        <?if ($row->photo==true):?>
                            <div class="img-circle pull-left">
                                <?=$row->photo_s->html_img(null,null,array('alt'=>$row->lastname.' '.$row->firstname.' '.$row->middlename))?>
                                <div class="mask200"></div>
                            </div>
                        <?endif?>
                        <?if (!$row->photo):?>
                            <div class="img-circle pull-left">
                                <img src="<?=URL::site('images/zagl.jpg')?>">
                                <div class="mask200"></div>
                            </div>
                        <?endif?>
                        <div class="media-body">
                            <div class="h4 media-heading">
                                <?=$row->lastname?>
                                <?=$row->firstname?>
                                <?=$row->middlename?>
                            </div>
                            <em class="muted"><?=$row->post?></em>
                            <div class="media_text">   </div>
                        </div>
                    </li>
                <?endforeach?>
            </ul>
        </div>
    </div>
    <div class="aside">
        <!-- <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute()?>
        </div> -->
        <?=View::factory('social_networks')?>
    </div>
</div>



