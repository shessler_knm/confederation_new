<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <?php
        echo View::factory('breadcrumbs')->set('_breadcrumbs', $_breadcrumbs)->render();
        ?>
        <div class="section">
            <h1><?=__('Исполнительный комитет')?></h1>
            <ul class="unstyled coach_list">
                <? foreach ($model as $row):?>
                    <li class="media">
                        <?if ($row->photo==true):?>
                                <div class="img-circle mask140 pull-left">
                                    <?=$row->photo_s->html_img(null,null,array('alt'=>$row->lastname.' '.$row->firstname.' '.$row->middlename))?>
                                </div>
                        <?endif?>
                        <?if (!$row->photo):?>
                            <div class="img-circle pull-left">
                                <div class="mask140">
                                    <img src="<?=URL::site('images/zagl.jpg')?>">
                                </div>
                            </div>
                        <?endif?>
                        <div class="media-body">
                            <div class="h4 media-heading">
                                <?=$row->lastname?>
                                <?=$row->firstname?>
                                <?=$row->middlename?>
                            </div>
                            <span><?=$row->post?></span>
                            <div class="media_text">   </div>
                        </div>
                    </li>
                <?endforeach?>
            </ul>
        </div>
    </div>
    <div class="aside">
        <!-- <div class="banner"> -->
            <!-- // <?= Request::factory('confederation/partner/request/banner')->execute()?> -->
        <!-- </div> -->
        <?=View::factory('social_networks')?>
    </div>
</div>



