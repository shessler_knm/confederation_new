<div class="row_fluid contacts_section clearfix bg-white">

<h1><?=$model->title ?></h1>
    <div class="media">
        <a href="<?=URL::site(($fed_id) ? $fed_id : 'confederation')?>">
            <img src="<?=URL::site('images/logo_conf/contact_logo_conf.png')?>" alt="" width="170px">
        </a>
    </div>
    <div class="media">
        <!-- <div class="media-body"> -->
<!--            <h4 class="media-heading">--><?//=$model->title?><!--</h4>-->
            <div class="media_text">
                <?=$model->text?>
            </div>
        <!-- </div> -->
    </div>

    <div class="contacts_map">
        <div id="map" class="size_map">
            <?if (strstr(Request::current()->url(), 'confederation')):?>
                <div id='coor' data-lng="51.142076" data-lat="71.411681" data-color="twirl#blueIcon"></div>
            <?endif?>
        </div>

        <div class="text-right link_print">
            <a href="#" onclick="print();return false"><i class="i_site i_print"></i><span class="link"><span class="ls"><?=__('Печать')?></span></span></a>

            <div class="link_dropdown">


                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="i_site i_link"></i> <span class="link"><span class="ls"><?=__('Ссылка')?></span></span></a>
                <ul class="unstyled pull-right dropdown-menu" role="menu" aria-labelledby="dLabel">
                    <li>
                        <?=__('Понравился материал? Поделитесь им с друзьями в соцсетях!')?>
                        <div class="social_block" id="social_block">
                            Yandex share?
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>