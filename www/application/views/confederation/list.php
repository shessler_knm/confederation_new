<div class="row_fluid clearfix bottom-20">
    <div class="row_section white">
        <!-- <div class="section"> -->

            <?= Request::factory('confederation/translation/request/preview')->execute() ?>

            <div class="unit-row">

                <!-- Новый блок в структуре -->
                    <div class="unit-head_news" id="nslider">


                        <div class="h3">
                            <span class="link">
                                <span class="ls"><?= __('Новости') ?></span>
                            </span>
<!--                            <object width="630px" height="419px">-->
<!--                                <param name="movie" value="http://kivvi.kz/static/player2/player.swf?config=http://kivvi.kz/static/player2/live.txt&amp;page=http://kivvi.kz/broadcast/serega_1979/&amp;url=rtmp://89.218.86.33%2Flive%2Fserega_1979%3fhash%3dserega_1979626feb034988f40bbc391b0088f1c2104089aa4d">-->
<!--                                <param name="allowFullScreen" value="true">-->
<!--                                <param name="bgcolor" value="#000000">-->
<!--                                <param name="allowScriptAccess" value="always">-->
<!--                                <param name="flashvars" value="title=%D0%9A%D1%83%D0%B1%D0%BE%D0%BA+%D0%9A%D0%A4%D0%91-2015+%D0%90%D1%81%D1%82%D0%B0%D0%BD%D0%B0-%D0%92%D0%9A%D0%9E&amp;play=auto">-->
<!--                                <embed src="http://kivvi.kz/static/player2/player.swf?config=http://kivvi.kz/static/player2/live.txt&amp;page=http://kivvi.kz/broadcast/serega_1979/&amp;url=rtmp://89.218.86.33%2Flive%2Fserega_1979%3fhash%3dserega_1979626feb034988f40bbc391b0088f1c2104089aa4d" type="application/x-shockwave-flash" width="640px" height="360px" allowfullscreen="true" allowscriptaccess="always" flashvars="title=%D0%9A%D1%83%D0%B1%D0%BE%D0%BA+%D0%9A%D0%A4%D0%91-2015+%D0%90%D1%81%D1%82%D0%B0%D0%BD%D0%B0-%D0%92%D0%9A%D0%9E&amp;play=auto" bgcolor="#000000">-->
<!--                            </object>-->
                        <?php if (Auth::instance()->logged_in()): ?>
                            <!--<script type="text/javascript">
                                delivery = function() {
                                    $.ajax({
                                        dataType: 'json',
                                        type: 'POST',
                                        url: '/user/delivery',
                                        success: function (data) {
                                            $("#delivery_link").html(data.success);
                                        }
                                    });
                                };
                            </script>
                            <a class="more-news" onclick="delivery();" id="delivery_link">
                                <?php
/*                                if($user->delivery != 1) {
                                    echo __('Подписаться на новости');
                                }
                                else {
                                    echo __('Отписаться от рассылки');
                                }
                                */?>
                            </a>-->
                        <?php endif; ?>
                        </div>
                        <?= Request::factory('confederation/news/request/widget')->execute() ?>
                    </div>
                    <!-- Конец блока -->

                <div class="unit-calendar">

                    <?= Request::factory('confederation/event/request/preview')->query('query', true)->execute() ?>
                </div>
            </div>

            <div class="unit-row">
            <!-- Список новостей выводить по три новости -->
            <div class="unit-last_news">
            <?= Request::factory('confederation/news/request/confederation_preview')->execute() ?>
            </div>
            <div class="unit-banner">
            <!-- Блок или банер мобильное приложение -->
                <div class="promo-video">
                    <iframe width="100%" height="235" src="https://www.youtube.com/embed/luVFHIfVkvI" frameborder="0" allowfullscreen></iframe>
                </div>
                <a href="<?=URL::site('/privatecoach');?>">
                    <div class="banner">
                        <img src="<?=URL::site('/images/banner-new.png');?>">
                        <?= Request::factory('confederation/partner/request/banner')->execute() ?>
                    </div>
                </a>
                <!-- <div class="unit-mobilepro">
                    <a href="https://itunes.apple.com/us/app/licnyj-trener/id938673136?mt=8"><img src="<?=URL::site('/images/icon-ios.png'); ?>"></a>
                    <a href="https://play.google.com/store/apps/details?id=kz.simplecode.confederation"><img src="<?=URL::site('/images/icon-android.png');?>"></a>
                </div> -->
            </div>
            </div>

        <!-- </div> -->
    </div>
</div>
<div class="clear"></div>
<div class="row_fluid">
<?= Request::factory('confederation/interview/request/preview')->execute() ?>

<!-- Слайдер рядом с интервью  -->
<div class="unit-cite">
<div class="bx-slider">
    <?php
        if (count($quotes)) {
        foreach($quotes as $quote):
    ?>
    <div class="bx-slide">
        <div class="figure">
            <?php $image = $quote->photo_s;?>
            <img style="width:200px; height:225px;" src="<?=$image->dir.$image->name.'.'.$image->type;?>" alt="<?=$quote->title;?>">
        </div>
        <div class="media-body">
            <div class="media-text">
                <a href="#">
                <h3 class="bx-title"><?=$quote->title;?></h3>
                <?=$quote->quote;?>
                </a>
            </div>
        </div>
    </div>
    <?php
        endforeach;
        }
    ?>
</div>
</div>
<!-- Конец слайдера -->

</div>
<div class="clear"></div>
<div class="row_fluid end">
    <div class="unit-media clearfix">
        <?= Request::factory('confederation/infocenter/request/gallery_preview')->execute() ?>
    </div>
</div>
