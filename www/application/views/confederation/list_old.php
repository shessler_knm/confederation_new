<div class="row_fluid">
    <div class="row_section">
        <div class="section">

            <?= Request::factory('confederation/translation/request/preview')->execute() ?>
            <div class="news_section">
                <div class="h3"><span class="link"><span class="ls"><?= __('Новости') ?></span></span></div>
                <div class="slider_section slider_section_inline">
                    <?= Request::factory('confederation/news/request/widget')->execute() ?>
                </div>
                <?= Request::factory('confederation/news/request/confederation_preview')->execute() ?>

                <div class="next_news_list">
                    <a href="<?= URL::site('confederation/news') ?>" class="link_next"><?= __('Еще новости') ?></a>
                </div>
            </div>



            <?= Request::factory('confederation/interview/request/preview')->execute() ?>
            <?= Request::factory('confederation/infocenter/request/gallery_preview')->execute() ?>
        </div>
    </div>

    <div class="aside">
        <div class="h3"><a href="<?= URL::site('confederation/event/') ?>" class="link"><span
                    class="ls"><?= __('Календарь') ?></span></a></div>
        <?= Request::factory('confederation/event/request/preview')->query('query', true)->execute() ?>
        <!--        <div class="banner">-->
        <!--            <a href="http://confederation.kz/forum/"><img src="-->
        <? //= URL::site('images/banner.png') ?><!--"></a>-->
        <!--        </div>-->
        <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div>
        <?= View::factory('social_networks') ?>
    </div>
</div>
























