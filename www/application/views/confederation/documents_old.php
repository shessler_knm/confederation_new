<div class="row_fluid">
    <div class="row_section">
        <div class="section">
            <h1><?= __('Документы') ?></h1>

            <form action="<?= URL::site('confederation/document') ?>" method="post"
                  class="row-fluid form-inline form_sport">
                <label><?= __('Вид спорта') ?>:</label>
                <?= Form::select('sport_type', $sport_types, $select_id, array('class' => 'auto_submit custom_select')); ?>
            </form>
            <ul class="unstyled result_list">
                <? foreach ($model as $row): ?>
                    <li class="hr_bottom media">
                        <div class="media_text">
                            <? if (I18n::lang() == 'ru'): ?>
                                <a href="<?= $row->photoru_s->download_file_url() ?>"><?= $row->title ?></a>
                            <? endif ?>
                            <? if (I18n::lang() == 'en'): ?>
                                <a href="<?= $row->photoen_s->download_file_url() ?>"><?= $row->title ?></a>
                            <? endif ?>
                            <? if (I18n::lang() == 'kz'): ?>
                                <a href="<?= $row->photokz_s->download_file_url() ?>"><?= $row->title ?></a>
                            <? endif ?>
                        </div>
                        <em class="muted"><?= Date::textdate($row->date, 'd m') ?></em>
                    <span class="widget">
                        <? if (I18n::lang() == 'ru'): ?>
                            <span
                                class="icon_ext <?= pathinfo($row->photoru_s->original_name, PATHINFO_EXTENSION) ?>"></span>
                            <em class="muted">
                                <?= pathinfo($row->photoru_s->original_name, PATHINFO_EXTENSION) ?>,
                                <?= $row->photoru_s->human_filesize($row->photoru_s->size) ?>
                            </em>
                        <? endif ?>
                        <? if (I18n::lang() == 'en'): ?>
                            <span
                                class="icon_ext <?= pathinfo($row->photoen_s->original_name, PATHINFO_EXTENSION) ?>"></span>
                            <em class="muted">
                                <?= pathinfo($row->photoen_s->original_name, PATHINFO_EXTENSION) ?>,
                                <?= $row->photoen_s->human_filesize($row->photoen_s->size) ?>
                            </em>
                        <? endif ?>
                        <? if (I18n::lang() == 'kz'): ?>
                            <span
                                class="icon_ext <?= pathinfo($row->photokz_s->original_name, PATHINFO_EXTENSION) ?>"></span>

                            <em class="muted">
                                <?= pathinfo($row->photokz_s->original_name, PATHINFO_EXTENSION) ?>,
                                <?= $row->photokz_s->human_filesize($row->photokz_s->size) ?>
                            </em>
                        <? endif ?>
                    </span>
                    </li>
                <? endforeach ?>
            </ul>
            <?= $pagination ?>
        </div>
    </div>
    <div class="aside">
        <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div>
        <?= View::factory('social_networks') ?>
    </div>
</div>