<div class="row_fluid coach_page clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <h1>
                <?=$model->lastname?>  <?=$model->firstname?>  <?=$model->middlename?>
            </h1>
            <div class="media">
                <?if ($model->photo==true):?>
                    <div class="img-circle pull-left">
                        <?=$model->photo_s->html_cropped_img(400,null)?>
                    </div>
                <?endif?>
                <?if (!$model->photo):?>
                    <div class="img-circle pull-left">
                        <img src="<?=URL::site('images/zagl.jpg')?>">
                    </div>
                <?endif?>

                <div class="media-body coach_page_info">
                    <p>
                        <em class="muted"><?= __('Должность') ?></em>
                        <em><?=$model->post?></em>
                    </p>
                    <p>
                        <em class="muted"><?= __('Команда') ?></em>
                        <em><?=$model->team->title?></em>
                    </p>
                </div>

                <div class="media_text">
                    <?=$model->biography?>
                </div>
            </div>

            <div class="social_block" id="social_block">
                Yandex share?
            </div>

        </div>
    </div>
    <div class="aside">

        <!-- <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute()?>
        </div> -->

    </div>
</div>


