<section class="l-section-wrap">
    <div class="l-section main-inner-section clear-wrap">
        <div class="main-col-left typo-main">
            <ul class="breadcrumbs">
                <li><a href="<?=URL::site('rio2016');?>"><?=__('Главная');?></a></li>
                <li><?=$federation->title . '. ' . __('Тренерский состав');?></li>
            </ul>
            <h1><?=__('Тренерский состав');?></h1>
            <? if ($fed_id == 'wrestling'): ?>
            <ul class="category-triggers">
                <? foreach ($model as $row): ?>
                    <li <?= $row->{'sef_' . I18n::$lang} == $sef ? 'class="active"' : '' ?>>
                        <a href="<?= URL::site('/').'/rio2016/' . $fed_id . '/coach/list/' . $row->{'sef_' . I18n::$lang} ?>">
                            <?= $row->title ?>
                        </a>
                    </li>
                <? endforeach ?>
            </ul>
            <? endif;?>

            <? foreach ($coaches as $row): ?>
            <div class="person-item">
                <div class="person-photo">
                <? if ($row->photo): ?>
                    <img src="<?=URL::site($row->photo_s->dir . '/'. $row->photo_s->name . '.' . $row->photo_s->type);?>" alt="<?=$row->get_full_name()?>" class="img-response">
                <? else: ?>
                    <img src="<?= URL::site('images/zagl.jpg') ?>" alt="<?=$row->get_full_name()?>" class="img-response">
                <?endif;?>
                </div>
                <div class="person-info">
                    <div class="person-name-wrap">
                        <? if ($row->{'biography_' . I18n::$lang}): ?>
                            <a href="<?= URL::site('rio2016') . '/' . $fed_id . '/coach/view/' . $row->{'sef_' . I18n::$lang} ?>" class="person-name"><?=$row->get_full_name()?></a>
                        <? else: ?>
                            <?=$row->get_full_name()?>
                        <?endif;?>
                    </div>
                    <div class="person-descr"><?= $row->post ?></div>
                </div>
            </div>
            <!-- /item -->
            <? endforeach; ?>
        </div>
        <!-- /col -->
        <?=View::factory('rio/right_col');?>
        <!-- /col -->
    </div>
</section>