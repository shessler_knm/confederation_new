<div class="row_fluid clearfix bg-white">
    <div class="row_section">
        <div class="section">
            <h1><?= __('Тренерский состав') ?></h1>
            <? if ($fed_id == 'wrestling'): ?>
                <ul class="unstyled inline nav_tabs">
                    <? foreach ($model as $team): ?>
                        <li <?= $team->{'sef_' . I18n::$lang} == $sef ? 'class="active"' : '' ?>><a
                                href="<?= URL::site($fed_id . '/coach/' . $team->{'sef_' . I18n::$lang}) ?>"
                                class="link_dotted"><?= $team->title ?></a></li>
                    <? endforeach ?>
                </ul>
            <? endif ?>
            <ul class="unstyled coach_list">
                <? foreach ($coaches as $row): ?>
                    <li class="media">
                        <? if ($row->photo): ?>
                            <div class="img-circle pull-left">
                                <?= $row->photo_s->html_cropped_img(200, null,array('alt'=>$row->get_full_name())) ?>
                                <? if ($row->{'biography_' . I18n::$lang}): ?>
                                    <a href="<?= URL::site($fed_id . '/coach/person/' . $row->{'sef_' . I18n::$lang}) ?>"
                                       class="mask200"></a>
                                <? endif ?>
                            </div>
                        <? endif ?>
                        <? if (!$row->photo): ?>
                            <div class="img-circle pull-left">
                                <img src="<?= URL::site('images/zagl.jpg') ?>" alt="<?=$row->get_full_name()?>">
                                <? if ($row->{'biography_' . I18n::$lang}): ?>
                                    <a href="<?= URL::site($fed_id . '/coach/person/' . $row->{'sef_' . I18n::$lang}) ?>"
                                       class="mask200"></a>
                                <? endif ?>
                            </div>
                        <? endif ?>
                        <div class="media-body">
                            <div class="h4 media-heading">
                                <? if ($row->{'biography_' . I18n::$lang}): ?>
                                    <a href="<?= URL::site($fed_id . '/coach/person/' . $row->{'sef_' . I18n::$lang}) ?>"
                                       class="link">
                                    <span
                                        class="ls"><?= $row->lastname ?> <?= $row->firstname ?> <?= $row->middlename ?></span>
                                    </a>
                                <? else: ?>
                                    <span
                                        class="ls"><?= $row->lastname ?> <?= $row->firstname ?> <?= $row->middlename ?></span>
                                <? endif ?>
                            </div>
                            <em class="muted"><?= $row->post ?></em>

                            <div class="media_text">
                                <? if ($row->{'biography_'.I18n::$lang}): ?>
                                <?= Text::limit_chars(strip_tags($row->biography), 500) ?>
                            </div>
                            <a href="<?= URL::site($fed_id . '/coach/person/' . $row->{'sef_' . I18n::$lang}) ?>"
                               class="link_dotted more_link">
                                <em><?= __('Читать дальше') ?></em>
                            </a>
                            <? endif ?>

                        </div>

                    </li>
                <? endforeach ?>
            </ul>
            <?= $pagination ?>
            <?= __('Понравился материал? Поделитесь им с друзьями в соцсетях!') ?>
            <div class="social_block" id="social_block">
                Yandex share?
            </div>
        </div>
    </div>
    <div class="aside">

       <!--  <div class="banner">
            <?= Request::factory('confederation/partner/request/banner')->execute() ?>
        </div> -->
        <?= View::factory('social_networks') ?>
    </div>
</div>


