<ul class="unstyled inline figure_list nav_column coach_list">
    <? foreach ($model as $row): ?>
        <li class="column pull-left">
            <? if ($row->photo): ?>
                <div class="img-circle">
                    <?= $row->photo_s->html_cropped_img(200, null) ?>
                    <? if ($row->{'biography_' . I18n::$lang}): ?>
                        <a href="<?= URL::site($fed_id . '/coach/person/' . $row->{'sef_' . I18n::$lang}) ?>"
                           class="mask84"></a>
                    <? endif ?>
                </div>
            <? endif ?>
            <? if (!$row->photo): ?>
                <div class="img-circle">
                    <img src="<?= URL::site('images/zagl.jpg') ?>" alt="">
                    <? if ($row->{'biography_' . I18n::$lang}): ?>
                        <a href="<?= URL::site($fed_id . '/coach/person/' . $row->{'sef_' . I18n::$lang}) ?>"
                           class="mask84"></a>
                    <? endif ?>
                </div>
            <? endif ?>
            <span>
            <? if ($row->{'biography_' . I18n::$lang}): ?>
                <a href="<?= URL::site($fed_id . '/coach/person/' . $row->{'sef_' . I18n::$lang}) ?>" class="link">
                <span class="ls">
                    <?= $row->firstname ?>
                    <?= $row->lastname ?>
                </span>
                </a>
            <? else: ?>
                    <span class="ls">
                    <?= $row->firstname ?>
                    <?= $row->lastname ?>
                </span>
            <? endif ?>
        </span>
        </li>
    <? endforeach ?>
</ul>
