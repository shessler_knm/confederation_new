<div class="comments_section">
    <div class="clearfix">
        <h3 class="pull-left h3">
            <? if (substr($counter, -1) >= 5 && substr($counter, -1) <= 9 || substr($counter, -1) == 0) : ?>
                <?= ($counter) . ' ' . __('комментариев') ?>
            <? elseif (($counter > '11' && ($counter < 14))): ?>
                <?= ($counter) . ' ' . __('комментариев') ?>
            <? elseif (substr($counter, -1) == 1): ?>
                <?= ($counter) . ' ' . __('комментарий') ?>
            <? elseif (substr($counter, -1) >= 2 && substr($counter, -1) <= 4): ?>
                <?= ($counter) . ' ' . __('комментария') ?>
            <?endif?>
        </h3>
        <? if ($user): ?>
            <a href="#" class="add_comment muted pull-right"><?=__('Добавить комментарий')?></a>
        <? else: ?>
            <div class="pull-right ulogin_comment">
                <?=__('Хотите оставить комментарий? Для этого вам необходимо авторизоваться через одну из соцсетей')?>:
                <?=$ulogin?>
            </div>
        <?endif?>
    </div>

    <?=View::factory('comments/form', array('target' => $target, 'id' => $id))?>

    <div class="comments toplevel">
        <?php
        foreach ($model as $row) {
            echo View::factory('comments/comment', array('model' => $row, 'level' => 1, 'token' => $token));
        }
        ?>
    </div>
</div>