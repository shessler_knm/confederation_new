<div class="news-comments-wrap">
    <h3 class="nc-head" id="comments"><?=__('Комментарии');?> (<?=$counter?>)</h3>
    <? if($user): ?>
    <div class="nc-name-wrap"><span class="nc-name"><?=$user->get_full_name();?></span></div>
    <form action="<?=Url::site('comment/post_rio/'.$id).Url::query(array('target' => $target))?>" class="nc-form" method="post">
        <textarea name="text" id="" cols="30" rows="10" class="std-textarea nc-textarea" placeholder="<?=__('Ваш комментарий');?>..."></textarea>
        <input type="hidden" name="parent_id" value="0"/>
        <input type="hidden" name="table_name" value="<?=$target?>">
        <input type="hidden" name="save" value="1">
        <input type="submit" class="std-sub nc-submit">
    </form>
    <? else: ?>
        <div><?=__('Хотите оставить комментарий? Для этого вам необходимо авторизоваться')?>:</div>
    <?endif;?>
    <script>
        $(document).ready(function() {
            $('a').on('click', function() {
                var id = this.getAttribute('data-id');
                if (id) {
                    $(this).hide();
                    $(this).parent().append(
                        '<form action="<?=Url::site('comment/post_rio/'.$id).Url::query(array('target' => $target))?>" class="nc-form" method="post">' +
                            '<textarea name="text" id="" cols="30" rows="10" class="std-textarea nc-textarea" placeholder="<?=__('Ваш комментарий');?>..."></textarea>' +
                            '<input type="hidden" name="parent_id" value="' + id + '"/>' +
                            '<input type="hidden" name="table_name" value="<?=$target?>">' +
                            '<input type="hidden" name="save" value="1">' +
                            '<input type="submit" class="std-sub nc-submit">'
                    );
                }
            });
        });
    </script>
    <?php
    foreach ($model as $row) {
        echo View::factory('comments/comment_rio', array('model' => $row, 'level' => 1, 'token' => $token));
    }
    ?>
</div>