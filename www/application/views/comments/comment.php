<div class="media level<?=$level?>">
    <a name="comment_<?=$model->id?>"></a>

    <div class="media-body">
        <div class="media-heading">
            <!--<a href="<?/*=URL::site('user/view/' . $model->user->id)*/?>"></a>-->
            <strong><?=$model->user->get_full_name()?></strong>
            <span class="muted">
                <?if($model->parent_id): ?>
                    <span class="parent">&rarr; <?=$model->parent->user->get_full_name()?></span>
                <?endif;?>

                <em><?=Date::textdate($model->date,'d m') ?>, <?=Date::reformat($model->date, 'H:i') ?></em>
            </span>
        </div>
        <? if ($model->status != 2): ?>
        <div class="txt" style="text-decoration: none;color: #2B2B2B;">
            <?=$model->text?>
        </div>
        <span class="muted">
            <a href="#" class="link ans" data-id="<?=$model->id?>" data-level="<?=$level?>"><span class="ls"><?=__('Ответить')?></span></a>
            <? if ($user && $user->id == $model->user_id): ?>
                <a href="#" class="delete_comment link" data-id="<?=$model->id?>" data-url="<?=URL::site('comment/remove')?>">
                    <span class="ls"><?=__('Удалить')?></span>
                </a>
            <? endif ?>
        </span>
        <? else: ?>
        <div class="txt">
            <?=__('Комментарий удален')?>
        </div>
        <?endif ?>

        </div>

<?php
$children = $model->children();
?>
<? if ($children->reset(false)->count_all() > 0): ?>

<?php

    foreach ($children->find_all() as $row) {
        echo View::factory('comments/comment', array('model' => $row, 'level' => $level + 1, 'token' => $token));
    }
    ?>

<? endif; ?>

</div>

