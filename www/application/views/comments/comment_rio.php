<div class="nc-item">
    <div class="nc-name-wrap">
        <span class="nc-name"><?=$model->user->get_full_name()?></span>
        <div class="nc-date"><?=Date::textdate($model->date,'d m') ?>, <?=Date::reformat($model->date, 'H:i') ?></div>
    </div>
    <div class="nc-text"><?=$model->text?></div>
    <a class="nc-answer" data-id="<?=$model->id?>" data-level="<?=$level?>"><?=_('Ответить');?></a>

    <?php
    $children = $model->children();
    ?>
    <? if ($children->reset(false)->count_all() > 0): ?>

        <?php

        foreach ($children->find_all() as $row) {
            echo View::factory('comments/comment_rio', array('model' => $row, 'level' => $level + 1, 'token' => $token));
        }
        ?>

    <? endif; ?>
</div>