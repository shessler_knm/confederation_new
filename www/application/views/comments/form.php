<? if ($user): ?>
    <form action="<?=Url::site('comment/post/'.$id).Url::query(array('target' => $target))?>"
        class="comment_form form-horizontal" style="display: none">
        <div class="control-group">
            <div class="control-label">
                <?=__('Ваше имя')?>:
            </div>
            <div class="controls">
                <?=$user->get_full_name()?>
            </div>
        </div>


        <div class="control-group">
            <div class="control-label">
                <?=__('Комментарий')?>:
            </div>
            <div class="controls">
                <textarea rows="3" cols="" name="text"></textarea>
                <div class="muted"><?=__('Оставляя комментарий, вы автоматически соглашаетесь с правилами комментирования')?></div>
            </div>
        </div>
        <input type="hidden" name="parent_id" value="0"/>
        <input type="hidden" name="table_name" value="<?=$target?>">
        <input type="hidden" name="save" value="1">

        <div class="control-group">
            <div class="controls">
                <input type="reset" name="reset" value="<?=__('Отмена')?>" class="btn color disabled">
                <input type="submit" value="<?=__('Отправить')?>" class="btn color">
            </div>
        </div>
    </form>
<? else: ?>
<form style="display: none" class="comment_form">
    <div class="form_line">
        <p><?=__('Вы не авторизованы').'!'?></p>
        <?='<a href='.URL::site('user/login').'>'.__('Войти').'</a> | <a href='.URL::site('user/reg').'>'.__('Регистрация').'</a>';?>
    </div>
</form>

<?endif; ?>