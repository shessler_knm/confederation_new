                <div class="media white_link">
                    <div class="pull-left e404"><strong>404</strong></div>
                    <div class="media-body">
                        <h1><?=__('Страница не найдена')?></h1>
                        <div class="media_text">
                            <?=__('Извините, перевод данной страницы отсутствует')?>.
                            <br>
                            <br>
                            <?=__('Вы можете <a href=:last_uri class="link"><span class="ls">вернуться назад</span></a> или перейти на <a href=:site class="link"><span class="ls">главную страницу</span></a>',array(':last_uri'=>$last_uri,':site'=>URL::site()))?>
                        </div>
                    </div>
                </div>



