<div class="media white_link">
    <div class="pull-left e404"><strong>404</strong></div>
    <div class="media-body">
        <h1><?=__('Страница не найдена')?></h1>
        <div class="media_text">
            <?=__('К сожалению, страницы, на которую вы хотели попасть, нет на нашем сайте')?>.
            <br>
            <?=__('Возможно, вы ввели неправильный адрес или она была удалена')?>.
            <br>
            <br>
            <?=__('Вы можете')?> <a href="<?=URL::site($last_uri)?>" class="link"><span class="ls"><?=__('вернуться назад')?></span></a> <?=__('или перейти на')?> <a href="<?=URL::site()?>" class="link"><span class="ls"><?=__('главную страницу')?></span></a>
        </div>
    </div>
</div>



