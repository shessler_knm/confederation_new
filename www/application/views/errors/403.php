<div class="media white_link">
    <div class="pull-left e404"><strong>403</strong></div>
    <div class="media-body">
        <h1><?=__('Отказано в доступе')?></h1>
        <div class="media_text">
            <?=__('К сожалению, у Вас нет доступа к этой странице').'.'?>
            <br>
            <?=__('Возможно, вы ввели неправильный адрес или вам нужно войти на сайт').'.'?>
            <br>
            <br>
            <?=__('Вы можете <a href="#" class="link"><span class="ls">вернуться назад</span></a> или перейти на <a href="#" class="link"><span class="ls">главную страницу</span></a>.')?>
        </div>
    </div>
</div>



