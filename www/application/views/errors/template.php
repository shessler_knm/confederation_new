<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?=__('Конфедерация спортивных единоборств и силовых видов спорта')?></title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?=URL::site('images/favicon.png')?>" type="image/x-icon">
    <link type="text/css" href="<?=URL::site('js/lib/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
    <link type="text/css" href="<?=URL::site('js/lib/bootstrap/css/bootstrap-responsive.min.css')?>" rel="stylesheet">
    <link type="text/css" href="<?=URL::site('css/style.css')?>" rel="stylesheet">
</head>
<body class="main_page pattern_bg error404">
<div class="shadow_top">
    <div class="wrap_err">
        <div class="container">
            <div class="container-fluid">
                <div class="white_link row_fluid">
                    <ul class="unstyled inline lang">
                        <?=Request::factory('exception/request/switch_lang')->execute()?>
                    </ul>
                </div>
                <?=$content?>
            </div>
        </div>
    </div>
</div>

<div class="footer">
    <div class="copyright container">
        <div class="container-fluid">
            © <?=__('Конфедерация спортивных единоборств и силовых видов спорта')?>, <?=date('Y')?>
        </div>
    </div>
</div>

<div class="shadow_bottom"></div>
<? if (Helper::is_production()): ?>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-42967352-1', 'confederation.kz');
    ga('send', 'pageview');
</script>
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 984403356;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/984403356/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>
<? endif; ?>
</body>
</html>