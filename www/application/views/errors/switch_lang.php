<?if ($model === null):?>
    <li <?= I18n::lang() == 'kz' ? 'class="active"' : '' ?>>
        <a href="<?= Helper::currentURI() ?>" <?=strstr($array_langs['kz'],Request::initial()->uri())?'rel=nofollow':null?> class="link">
            <span class="ls">Қаз</span>
        </a>
    </li>

    <li <?= I18n::lang() == 'ru' ? 'class="active"' : '' ?>>
        <a href="<?=URL::site('ru' . Helper::currentURI()) ?>" <?=strstr($array_langs['ru'],Request::initial()->uri())&&I18n::$lang!='kz'?'rel=nofollow':null?> class="link">
            <span class="ls">Рус</span>
        </a>
    </li>

    <li <?= I18n::lang() == 'en' ? 'class="active"' : '' ?>>
        <a href="<?= URL::site('en' . Helper::currentURI()) ?>" <?=strstr($array_langs['en'],Request::initial()->uri())&&I18n::$lang!='kz'?'rel=nofollow':null?> class="link">
            <span class="ls">Eng</span>
        </a>
    </li>
<?else:?>
    <li <?= I18n::lang() == 'kz' ? 'class="active"' : '' ?>>
        <a href="<?= $model->sef_kz ? $url . $model->sef_kz : Helper::currentURI() ?>" <?=strstr($array_langs['kz'],Request::initial()->uri())?'rel=nofollow':null?> class="link">
            <span class="ls">Қаз</span>
        </a>
    </li>

    <li <?= I18n::lang() == 'ru' ? 'class="active"' : '' ?>>
        <a href="<?= $model->sef_ru ? URL::site('ru' . $url . $model->sef_ru) : URL::site('ru' . Helper::currentURI()) ?>" <?=strstr($array_langs['ru'],Request::initial()->uri())&&I18n::$lang!='kz'?'rel=nofollow':null?> class="link">
            <span class="ls">Рус</span>
        </a>
    </li>

    <li <?= I18n::lang() == 'en' ? 'class="active"' : '' ?>>
        <a href="<?= $model->sef_en ? URL::site('en' . $url . $model->sef_en) : URL::site('en' . Helper::currentURI()) ?>" <?=strstr($array_langs['en'],Request::initial()->uri())&&I18n::$lang!='kz'?'rel=nofollow':null?> class="link">
            <span class="ls">Eng</span>
        </a>
    </li>
<?endif?>