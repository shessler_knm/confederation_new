<ul class="breadcrumbs">
    <li class="bread_item"><a href="<?=URL::site('/');?>" class="link"><?=__('Главная');?></a></li>
    <?php foreach($_breadcrumbs as $crumb): ?>
        <li class="bread_item"><a href="<?=$crumb['url'];?>" class="link"><?=$crumb['title'];?></a></li>
    <?php endforeach; ?>
</ul>