<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(


    'driver'       => 'Ulogin',
    'hash_method'  => 'sha256',
    'hash_key'     => 'as-0df98309wuqrpokjadspofasjfd908',
    'lifetime'     => 1209600,
    'session_type' => Session::$default,
    'session_key'  => 'auth_user',

);
