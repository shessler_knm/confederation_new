<?php defined('SYSPATH') or die('No direct script access.');

return array(
    'development' => array(
        'apns_cert' => "apns-dev.pem",
        'passphrase' => "kaznetmedia",
        'connect_timeout' => 60,
        'server_url' => "ssl://gateway.sandbox.push.apple.com:2195",
    ),
    'production' => array(
        'apns_cert' => "apns-pro.pem",
        'passphrase' => "kaznetmedia",
        'connect_timeout' => 60,
        'server_url' => "ssl://gateway.push.apple.com:2195",
    ),
);
