<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
	'default' => array
	(
		'type'       => 'PDO_MySQL',
		'connection' => array(
            /*'dsn'      => 'mysql:host=localhost;dbname=cnfd',
            'hostname' => 'localhost',
            'username' => 'cnfd',
            'password' => 'cnfd',
            'database' => 'cnfd',
            'persistent' => FALSE,*/

            'dsn'      => Helper::set_if_production('mysql:host=89.218.81.178;dbname=cnfd_confederation','mysql:host=localhost;dbname=sport'),
            'hostname' => Helper::set_if_production('89.218.81.178', 'localhost'),
            'username' => Helper::set_if_production('cnfd', 'root'),
            'password' => Helper::set_if_production('53IEYK2QFtvzA6z4irzR', ''),
            'database' => Helper::set_if_production('cnfd_confederation', 'sport'),
            'persistent' => FALSE,

            /*'dsn'      => 'mysql:host=127.0.0.1;dbname=cnfd',
            'hostname' => '127.0.0.1',
            'username' => 'root',
            'password' => '',
            'database' => 'cnfd',
            'persistent' => FALSE,
*/
            /*'hostname' => 'localhost',
           'username' => 'root',
           'password' => '',
           'database' => 'sport',
           'persistent' => FALSE,*/
		),
		'table_prefix' => '',
		'charset'      => 'utf8',
		'caching'      => FALSE,
	),

);
