<?php
    return array(
        'modules'=>array(
            'database'=>MODPATH.'database',
            'orm'=>MODPATH.'orm',
            'auth'=>MODPATH.'auth',
//            'storage'=>MODPATH.'storage',
            'recaptcha'     => MODPATH.'recaptcha',     // Re-captcha
            'ulogin'     => MODPATH.'ulogin',     // Re-captcha
            'image'      => MODPATH.'image',
            "slider" => MODPATH.'slider', // слайдер
            "category"=>MODPATH.'category',
            'page'=>MODPATH.'page',
            'email'       => MODPATH.'email',
            'pagination'=>MODPATH.'pagination',
            'search'=>MODPATH.'search',
            'phpflicker'=>MODPATH.'phpflickr',
            'mpdf'=>MODPATH.'mpdf',
            'minion'=>MODPATH.'minion'
        ),
        'behavior'=>'init', //Определяет поведение модуля, bootstrap инициализация модулей и среды в бутсрапе
                                 //init - инициализация среды и модулей в модуле cms
        'environment'=>array(
            Kohana::PRODUCTION=>array(
                'base_url'=>'/',
                'errors'=>false,
                'profile'=>false,
            ),
            Kohana::DEVELOPMENT=>array(
                'base_url'=>'/'
            )
        )
    );
