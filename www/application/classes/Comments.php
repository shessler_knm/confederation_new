<?php
class Comments
{
    public static function get_list($target,$id,$token=''){
        /** @var Request $req  */
        $req=Request::factory('comment/list/'.$id);
        $req->query('target',$target)->query('token',$token);
        return $req->execute();
    }

    public static function get_list_rio($target,$id,$token=''){
        /** @var Request $req  */
        $req=Request::factory('comment/list_rio/'.$id);
        $req->query('target',$target)->query('token',$token);
        return $req->execute();
    }

    public static function get_count($target,$id){
        $count = ORM::factory('Comment')
            ->where('table_name','=',$target)
            ->where('status','=',1)
            ->where('target_id','=',$id)
            ->count_all();
        return $count;
    }

}