<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Confederation extends Site
{
    public function scripts(){
        return parent::scripts()+array(
            'index'=>'js/index.js',

        );
    }

    public function action_index()
    {
        $this->page->title(__('Конфедерация спортивных единоборств и силовых видов спорта'));
        $quotes = ORM::factory('Quote')->where('rio', '=', 0)->find_all();
        $this->page->content(View::factory('confederation/list')->bind('quotes', $quotes));
    }
}