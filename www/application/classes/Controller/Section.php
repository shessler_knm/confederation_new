<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Section extends Site
{
    public function before()
    {
        parent::before();
        if ($this->fed_id) {
            Register::instance()->activemenu = 32;
        } else {
            Register::instance()->activemenu = 20;
        }
    }


    public function scripts()
    {
        return parent::scripts() + array(
            'comments' => 'js/comments.js',
            "yandexmap" => "http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU",
            'map' => 'js/yandexmap.js',
        );
    }

    public function init_langs($model = NULL)
    {
        $langs = array('ru' => '/ru/', 'en' => '/en/', 'kz' => '/');
        if ($model === NULL) {
            parent::init_langs($model);
        } else {
            if ($this->fed_id) {
                foreach ($langs as $lang => $val) {
                    if (empty($model->{'sef_' . $lang})) {
                        $this->array_langs[$lang] = $val . $this->fed_id . '/section/' . $model->{'sef_' . I18n::$lang};
                    } else {
                        $this->array_langs[$lang] = $val . $this->fed_id . '/section/' . $model->{'sef_' . $lang};
                    }
                }
            } else {
                foreach ($langs as $lang => $val) {
                    if (empty($model->{'sef_' . $lang})) {
                        $this->array_langs[$lang] = $val . 'confederation/section/' . $model->{'sef_' . I18n::$lang};
                    } else {
                        $this->array_langs[$lang] = $val . 'confederation/section/' . $model->{'sef_' . $lang};
                    }
                }
            }
        }
    }

    public function action_xmlgeoobject()
    {
        $this->render = FALSE;

        $id = $this->request->query('id');
        if ($id) {
            $points = ORM::factory('Section')->where('city_id', '=', $id)->find_all();
        } else {
            $points = ORM::factory('Section')->find_all();
        }
        $this->response->headers('Content-type', 'text/xml');
        $this->response->body(View::factory('sections/xmlgeoobject')
            ->bind('points', $points)
            ->bind('id', $id));
    }

    public function action_index()
    {
        switch ($this->fed_id) {
            case 'box':
                $this->page->title(__('Секции бокса в Астане, Алматы и крупных городах Казахстана - Казахстанская федерация бокса'));
                $this->page->meta(__('Секции бокса в Астане, Алматы и прочих крупных городах и населенных пунктах Казахстана. Адреса, контактные данные'), 'description');
                break;
            case 'judo':
                $this->page->title(__('Секции дзюдо в Алматы, Астане и крупных городах Казахстана - Федерация дзюдо Казахстана'));
                $this->page->meta(__('Секции дзюдо в Алматы, Астане и прочих крупных городах и населенных пунктах Казахстана. Адреса, контактные данные'), 'description');
                break;
            case 'wrestling':
                $this->page->title(__('Секции борьбы в Астане, Алматы и других крупных городах Казахстана - Федерация греко-римской, вольной и женской борьбы Республики Казахстан'));
                $this->page->meta(__('Секции борьбы в Астане, Алматы и прочих крупных городах и населенных пунктах Казахстана. Адреса, контактные данные'), 'description');
                break;
            case 'taekwondo':
                $this->page->title(__('Секции таеквондо в Астане, Алматы и других крупных городах Казахстана - Федерация таеквондо (WTF) Республики Казахстан'));
                $this->page->meta(__('Секции таеквондо в Астане, Алматы и прочих крупных городах и населенных пунктах Казахстана. Адреса, контактные данные'), 'description');
                break;
            case 'weightlifting':
                $this->page->title(__('Секции тяжелой атлетики в Астане, Алматы и других крупных городах Казахстана - Федерация тяжелой атлетики Казахстана'));
                $this->page->meta(__('Секции тяжелой атлетики в Астане, Алматы и прочих крупных городах и населенных пунктах Казахстана. Адреса, контактные данные'), 'description');
                break;
            default:
                $this->page->title(__('Секции'));
//                $this->page->meta(__(''), 'description');
        }
        $category = ORM::factory('Category');
        $country = $category->where('slug', '=', 'kazahstan')->find();
        $sports = $category->get_sef_sports()->order_by('title_' . I18n::$lang, 'ASC')->find_all();
        $cities = ORM::factory('City')->where('country_id', '=', $country->id)->order_by('title_' . I18n::$lang, 'ASC')->find_all();
        $cityList = $cities->as_array('id', 'title');
        $cityList = array(0 => __('Все города')) + $cityList;
        $sportList = $sports->as_array('id', 'title');
        $sportList = array(0 => __('Все виды спорта')) + $sportList;
        $lng = array();
        $lat = array();
//        if ($this->fed_id) {
//            $model = $this->federation->sections->where('title_'.I18n::$lang,'!=','')->find_all()->as_array();
//        } else {
//            $model = ORM::factory('Section')->where('title_'.I18n::$lang,'!=','')->find_all()->as_array();
//        }
        foreach ($cities as $place) {
            $lat[$place->id] = $place->lat;
        }
        foreach ($cities as $place) {
            $lng[$place->id] = $place->lng;
        }
        $this->page->content(View::factory('sections/list')
                ->set('selected', 0)
                ->bind('cityList', $cityList)
                ->bind('sportList', $sportList)
                ->bind('lngList', $lng)
                ->bind('latList', $lat)
        );
        $this->add_crumb(__('Секции'), '');

    }

    function action_ajax_list()
    {
        $this->render = false;
        if ($this->fed_id) {
            $model = $this->federation->sections->where('title_' . I18n::$lang, '!=', '');
        } else {
            $model = ORM::factory('Section')->where('title_' . I18n::$lang, '!=', '');
        }
        $model = $this->get_sections($model);
        $pagination = $this->pagination($model);
        $page = $this->request->param('page');
        $total_pages = $pagination->total_pages;
        if ($total_pages) {
            $this->check_pages($page, $total_pages);
        }
        $model = $model->find_all();
        $count = $model->count();
        $this->response->body(View::factory('sections/ajax_list')
            ->bind('count', $count)
            ->bind('model', $model)
            ->bind('pagination', $pagination));
    }

    public function get_sections($section)
    {
        $city_id = $this->request->param('id');
        $sport_id = $this->request->param('id2');
        $q = $this->request->query('section_object');
        if ($city_id > 0) {
            $section->where('city_id', '=', $city_id);
        }
        if ($sport_id > 0) {
            $section->join('sections_categories', 'left')->on('sections_categories.section_id', '=', 'section.id');
            $section->where('sections_categories.category_id', '=', $sport_id);
        }
        if ($q) {
            $section->where_open()
                ->where('title_ru', 'LIKE', "%$q%")
                ->or_where('title_kz', 'LIKE', "%$q%")
                ->or_where('title_en', 'LIKE', "%$q%")
                ->or_where('address', 'LIKE', "%$q%")
                ->where_close();
        }
        return $section;
    }


    public function action_view()
    {
        $sef = $this->request->param('sef');
        $model = ORM::factory('Section')->where('sef_' . i18n::lang(), '=', $sef)->find();
        if (!$model->loaded()) {
            foreach ($this->array_langs as $lang => $url) {
                $model = ORM::factory('Section')->where('sef_' . $lang, '=', $sef)->find();
                if ($model->loaded()) {
                    throw new HTTP_Exception_Translate();
                }
            }
            throw new HTTP_Exception_404('Страница не найдена');
        }
        $this->init_langs($model);
        $this->page->title($model->title.' - '.__('Секции').' - '.$this->federation->loaded()?$this->federation->{'title_'.I18n::$lang}:__('Конфедерация спортивных единоборств и силовых видов спорта'));
        $this->page->meta(Text::limit_chars($model->description,200), 'description');
        $this->page->content(View::factory('sections/view')
            ->bind('model', $model));
    }

    public function action_translit_message()
    {
        $this->render = false;
        $langs = array('ru' => '/ru/', 'en' => '/en/', 'kz' => '/');
        $this->write_lst_uri = false;
        $this->response->status(404);
        $this->session OR $this->session = Session::instance();
        $last_uri = '/' . ltrim($this->session->get('last_uri', URL::base()), '/');
        $model = ORM::factory('Section');
        preg_match('"([^/]*$)"', $last_uri, $match);
        foreach ($langs as $lang => $val) {
            if ($model->where('sef_' . $lang, '=', $match[0])->find()->loaded()) {
                break;
            }
        }
        $url = $this->fed_id ? $this->fed_id . '/section' : 'confederation/section';
        Helper::currentURI();
        $view = View::factory('errors/translite')
            ->bind('url', $url)
            ->bind('model', $model)
            ->bind('model_name', $model_name)
            ->bind('last_uri', $last_uri);
        $this->response->body($view);
    }

}