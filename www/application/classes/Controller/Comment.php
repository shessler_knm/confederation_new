<?php
class Controller_Comment extends Site
{
    public function action_list()
    {

        $this->render = false;
        $this->page->content(
            View::factory('comments/list')
                ->bind('model', $model)
                ->bind('target', $table_name)
                ->bind('id', $target_id)
                ->bind('token', $token)
                ->bind('counter',$counter)
        );
        $token = $this->request->query('token');
        $table_name = $this->request->query('target');
        $target_id = $this->request->param('id');
        $counter = Comments::get_count($table_name,$target_id);
        $collection = ORM::factory('Comment')
            ->where('status', '!=', 0)
            ->where('target_id', '=', $target_id)
            ->where('parent_id', '=', 0)
            ->where('table_name', '=', $table_name)
            ->order_by('date');
        $model = $collection->find_all();
        $this->response->body($this->page->content());
    }

    public function action_list_rio()
    {

        $this->render = false;
        $this->page->content(
            View::factory('comments/list_rio')
                ->bind('model', $model)
                ->bind('target', $table_name)
                ->bind('id', $target_id)
                ->bind('token', $token)
                ->bind('counter',$counter)
        );
        $token = $this->request->query('token');
        $table_name = $this->request->query('target');
        $target_id = $this->request->param('id');
        $counter = Comments::get_count($table_name,$target_id);
        $collection = ORM::factory('Comment')
            ->where('status', '!=', 0)
            ->where('target_id', '=', $target_id)
            ->where('parent_id', '=', 0)
            ->where('table_name', '=', $table_name)
            ->order_by('date');
        $model = $collection->find_all();
        $this->response->body($this->page->content());
    }

    public function action_post()
    {
        $this->render = false;
        if ($this->request->post('save') && $this->user) {
            $data = Arr::extract($this->request->post(), array('text', 'table_name', 'parent_id'));
            $id = $this->request->param('id');
            $model = ORM::factory('Comment');
            $model->date = date('Y-m-d H:i:s');
            $model->user_ip = Helper::get_ip_address();
            $model->status = 1;
            $model->values($data);
            $model->target_id = $id;
            $model->user_id = $this->user->id;
            try {
                $model->save();
            } catch (ORM_Validation_Exception $e) {
                throw new HTTP_Exception_500();
            }
            $this->response->body(View::factory('comments/comment', array('model' => $model, 'level' => 1)));
        }
    }

    public function action_post_rio()
    {
        $this->render = false;
        if ($this->request->post('save') && $this->user) {
            $data = Arr::extract($this->request->post(), array('text', 'table_name', 'parent_id'));
            $id = $this->request->param('id');
            $model = ORM::factory('Comment');
            $model->date = date('Y-m-d H:i:s');
            $model->user_ip = Helper::get_ip_address();
            $model->status = 1;
            $model->values($data);
            $model->target_id = $id;
            $model->user_id = $this->user->id;
            try {
                $model->save();
            } catch (ORM_Validation_Exception $e) {
                throw new HTTP_Exception_500();
            }
            $this->redirect($this->request->referrer().'#comments');
        }
    }

    public function action_remove()
    {
        $this->render = false;
        if ($this->user) {
            if ($this->request->post('id')) {
                $model = ORM::factory('Comment', $this->request->post('id'));
                if ($model->loaded() && $model->user_id == $this->user->id) {
                    $model->status = 2;
                    $model->save();
//                    Search::delete($model);
                    $this->response->body(__('Коментарий удален'));
                }
            }
        }
    }

}
