<?php
/**
 * Created by PhpStorm.
 * User: Almas
 * Date: 05.11.13
 * Time: 15:45
 */

class Controller_Link extends Site
{

    public function action_index()
    {
        $this->render = false;
        $current_uri = $this->request->query('url');
        $total_pages = $this->request->query('total_pages');
        $links = array();
        $page = preg_match('#page-(.*)#', $current_uri, $page_number);
        $is_event = $this->request->query('event');
        $event_page = $this->request->query('event_page');
        if ($is_event) {
            $next = $current_uri . '?page=' . ($event_page + 1);
            $prev = $current_uri . '?page=' . ($event_page - 1);
           if ($total_pages > 1 && $event_page <= $total_pages) {
                if ($event_page == 2 && $total_pages != 2) {
                    $links['canonical'] = $current_uri;
                    $links['prev'] = $current_uri;
                    $links['next'] = $next;
                } elseif ($event_page == $total_pages) {
                    $links['canonical'] = $current_uri;
                    $links['prev'] = $prev;
                } elseif ($event_page > 1 && $event_page != $total_pages) {
                    $links['canonical'] = $current_uri;
                    $links['prev'] = $prev;
                    $links['next'] = $next;
                }
            }
        } elseif ($page) {
            $next = preg_replace('#/page-.*#', '/page-' . ($page_number[1] + 1), $current_uri);
            $canonical = preg_replace('#/page-.*#', '', $current_uri);
            $prev = preg_replace('#/page-.*#', '/page-' . ($page_number[1] - 1), $current_uri);
            if ($page_number[1] == 1) {
                $links['next'] = $next;
            } elseif
            ($page_number[1] == 2 && $total_pages != 2
            ) {
                $links['canonical'] = $canonical;
                $links['prev'] = $canonical;
                $links['next'] = $next;
            } elseif ($page_number[1] == 2 && $total_pages == 2) {
                $links['canonical'] = $canonical;
                $links['prev'] = $canonical;
            } elseif ($page_number[1] == $total_pages) {
                $links['canonical'] = $canonical;
                $links['prev'] = $prev;
            } else {
                $links['canonical'] = $canonical;
                $links['prev'] = $prev;
                $links['next'] = $next;
            }
        } elseif ($total_pages > 1) {
            $links['next'] = $current_uri . '/page-2';
        }
        $this->response->body(View::factory('links')
                ->bind('links', $links)
        );

    }
}