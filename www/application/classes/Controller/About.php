<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_About extends Site
{
    public function  action_index()
    {
        //О федерации
        $this->page->content(View::factory('federation/about')
            ->bind('model',$model));
        $id=$this->request->param('fed_id');
        $model = $this->federation;
        if(!$model->loaded() )
            throw new HTTP_Exception_404('Страница не найдена');

    }
}