<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Team extends Site
{

    public function before()
    {
        parent::before();
        if ($this->fed_id) {
            Register::instance()->activemenu = 29;
        } else {
            Register::instance()->activemenu = 29;
        }
    }

    public function init_langs($model = NULL, $person = NULL)
    {
        $langs = array('ru' => '/ru/', 'en' => '/en/', 'kz' => '/');
        if ($model === NULL) {
            parent::init_langs($model);
        } else {
            if ($person) {
                foreach ($langs as $lang => $val) {
                    if (empty($model->{'sef_' . $lang}) || $model->{'sef_' . $lang} == '-') {
                        $this->array_langs[$lang] = $val . $this->fed_id . '/team/person/' . $model->{'sef_' . I18n::$lang};
                    } else {
                        $this->array_langs[$lang] = $val . $this->fed_id . '/team/person/' . $model->{'sef_' . $lang};
                    }
                }
            } else {
                foreach ($langs as $lang => $val) {
                    if (empty($model->{'sef_' . $lang})) {
                        $this->array_langs[$lang] = $val . $this->fed_id . '/team/' . $model->{'sef_' . I18n::$lang};
                    } else {
                        $this->array_langs[$lang] = $val . $this->fed_id . '/team/' . $model->{'sef_' . $lang};
                    }
                }
            }

        }
    }

    public function action_index()
    {
        $sef = $this->request->param('sef');
        $federation = $this->federation;
        $teams = $federation->teams;
        $first_team = $teams->reset(false)->order_by('id', 'ASC')->where('is_published', '!=', '2');
        $teams = $teams->where('main', '=', '2')->find_all();
        $team = ORM::factory('Team')->where('sef_' . i18n::lang(), '=', $sef)->find();
        $this->init_langs($team);
        if ($teams->as_array()) {
            $first_team = $first_team->find();
            if ($sef && $first_team->federation_id == $federation->id) {
                $players = ORM::factory('Player')->where('is_published', '=', '1')->where('team_id', '=', $team->id)->where('firstname_'.I18n::$lang,'!=','')->where('lastname_'.I18n::$lang,'!=','')->order_by('weight', 'ASC')->order_by('category', 'ASC');
            } else {
                $this->redirect($this->fed_id . '/team/' . $first_team->{'sef_' . I18n::$lang}, 301);
            }
            $this->page->title($team->title . ' - ' . __('Сборные команды') . ' - ' . $federation->{'title_' . I18n::$lang});
            $this->page->meta(Text::limit_words($team->text, 150), 'description');
            $pagination = $this->pagination($players, 'player_pagination.default');
            $page = $this->request->param('page');
            $total_pages = $pagination->total_pages;
            $this->check_pages($page, $total_pages);
            View::bind_global('total_pages', $total_pages);
            $players = $players->find_all();
        } else {
            $no_team = true;
        }
        if (!$team->loaded()) {
            foreach ($this->array_langs as $lang => $url) {
                $model = ORM::factory('Team')->where('sef_' . $lang, '=', $sef)->find();
                if ($model->loaded()) {
                    throw new HTTP_Exception_Translate();
                }
            }
            throw new HTTP_Exception_404('Страница не найдена');
        }
        $this->page->content(View::factory('team/list')
                ->bind('model', $teams)
                ->bind('sef', $sef)
                ->bind('no_team', $no_team)
                ->bind('players', $players)
                ->bind('pagination', $pagination)
        );
    }

    public function action_person()
    {
        //контроллер одной команды
        $sef = $this->request->param('sef');
        $model = ORM::factory('Player')->where('sef_' . i18n::lang(), '=', $sef)->find();
        if (!$model->loaded()) {
            foreach ($this->array_langs as $lang => $url) {
                $model = ORM::factory('Player')->where('sef_' . $lang, '=', $sef)->find();
                if ($model->loaded()) {
                    throw new HTTP_Exception_Translate();
                }
            }
            throw new HTTP_Exception_404('Страница не найдена');
        }
        $this->init_langs($model, true);
        $this->page->title($model->get_full_name() . ' - ' . __('Сборные команды') . ' - ' . $this->federation->{'title_' . I18n::$lang});
        $this->page->meta(Text::limit_words($model->biography, 150), 'description');
        $this->page->content(View::factory('team/players/view')
            ->bind('model', $model));
    }

    public function action_translit_message()
    {
        $this->render = false;
        $langs = array('ru' => '/ru/', 'en' => '/en/', 'kz' => '/');
        $this->write_lst_uri = false;
        $this->response->status(404);
        $this->session OR $this->session = Session::instance();
        $last_uri = '/' . ltrim($this->session->get('last_uri', URL::base()), '/');
        $model = ORM::factory('Player');
        preg_match('"([^/]*$)"', $last_uri, $match);
        foreach ($langs as $lang => $val) {
            if ($model->where('sef_' . $lang, '=', $match[0])->find()->loaded()) {
                break;
            }
        }
        preg_match('"([^/]+)(?=/[^/]*$)"', $last_uri, $match);
        if ($match[0] == 'person') {
            $url = $this->fed_id . '/team/person';
        } else {
            $url = $this->fed_id . '/team';
        }
        Helper::currentURI();
        $view = View::factory('errors/translite')
            ->bind('url', $url)
            ->bind('model', $model)
            ->bind('model_name', $model_name)
            ->bind('last_uri', $last_uri);
        $this->response->body($view);
    }

}