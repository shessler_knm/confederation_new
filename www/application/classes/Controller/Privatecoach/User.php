<?php

class Controller_Privatecoach_User extends Privatecoach_Site
{
    public function before()
    {
        if ($this->request->action() != 'login' && $this->request->action() != 'logout') {
            $this->need_auth = true;
        }
        parent::before();
    }

    public function action_avatar()
    {
        $validate = Validation::factory($_FILES);
        $validate->rules('uploadFile', array(
            array("Upload::not_empty"),
            array("Upload::valid"),
            array("Upload::type", array(":value", array("png", "jpg", "gif", "jpeg"))),
            array("Upload::size", array(":value", "2M")),
            array('Upload::image')
        ));
        if ($validate->check()) {
            $file = Cms_Storage::instance()->upload($_FILES, 'uploadFile');
            if ($file instanceof ORM && $file->loaded()) {
                $uploaded = "";
            }
            $model = ORM::factory('User', array('id' => $this->user->id));
            $model->photo = $file->id;
//            Feed::new_photo($this->user, $file->id);
            $model->save();
//            $this->response->body($file>url());
        }
    }

    public function action_form()
    {
        $this->page->script('bootstrap-datepicker.js', 'js/lib/datepicker/js/bootstrap-datepicker.js');
        $this->page->script('bootstrap-datepicker.ru.js', 'js/lib/datepicker/js/locales/bootstrap-datepicker.ru.js');
        $this->page->script('bootstrap-datepicker.kk.js', 'js/lib/datepicker/js/locales/bootstrap-datepicker.kk.js');
        $this->page->script('loadimage', 'js/loadimage.js');
        $this->page->style('datepicker.css', 'js/lib/datepicker/css/datepicker.css');
        $view = View::factory('privatecoach/user/edit/form')
            ->bind('sex', $sex)
            ->bind('date', $date)
            ->bind('model', $model)
            ->bind('params', $params)
            ->bind('validate_error', $validate_error)
            ->bind('checkbox', $checkbox)
            ->bind('top_user', $top_user)
            ->bind('progress', $progress);
        $sex = array();
        $checkbox = $this->user->hiding;
        $sex[1] = __('Муж.');
        $sex[2] = __('Жен.');
        for ($i = intval(date('Y')) - 99; $i <= intval(date('Y')); $i++) {
            $date['years'][$i] = $i;
        }
        $date['months'][1] = __('Январь');
        $date['months'][2] = __('Февраль');
        $date['months'][3] = __('Март');
        $date['months'][4] = __('Апрель');
        $date['months'][5] = __('Май');
        $date['months'][6] = __('Июнь');
        $date['months'][7] = __('Июль');
        $date['months'][8] = __('Август');
        $date['months'][9] = __('Сентябрь');
        $date['months'][10] = __('Октябрь');
        $date['months'][11] = __('Ноябрь');
        $date['months'][12] = __('Декабрь');

        $months[1] = 'January';
        $months[2] = 'February';
        $months[3] = 'March';
        $months[4] = 'April';
        $months[5] = 'May';
        $months[6] = 'June';
        $months[7] = 'July';
        $months[8] = 'August';
        $months[9] = 'September';
        $months[10] = 'October';
        $months[11] = 'November';
        $months[12] = 'December';


        if ($this->request->post('refresh')) {
            $values = $this->request->post();
            $profile_validate = Validation::factory($_POST);
            $profile_validate->rules('firstname', array(
                array('not_empty'),
            ));
            $profile_validate->rules('lastname', array(
                array('not_empty'),
            ));
            $profile_validate->rules('birthdate', array(
                array('not_empty'),
                array('date')
            ));
            if ($profile_validate->check()) {
                $this->user->values($values, array('firstname', 'lastname', 'annotation', 'sex', 'birthdate', 'height', 'weight'));
                $this->user->hiding = array_key_exists('hiding', $values) ? 1 : 0;
                $this->user->save();
            } else {
                $validate_error = $profile_validate->errors(0);
            }
        }
        $qdate = strtotime($months[intval(Date::formatted_time($this->user->birthdate, 'm'))] . ' ' . Date::formatted_time($this->user->birthdate, 'Y'));
        if ($this->user->birthdate) {
            for ($i = 1; $i <= intval(date('t', $qdate)); $i++) {
                $date['days'][$i] = $i;
            };
        }
        $params = ORM::factory('User_Log')->where('user_id', '=', $this->user->id)->order_by('date', 'DESC')->order_by('id', 'DESC')->find();
        $top_user = ORM::factory('Top')->where('user_id', '=', $this->user->id)->find();
        $this->page->content($view);
    }


    public function action_login()
    {
        $ulogin = ULogin::widget_ulogin();
        $id = Session::instance()->get('id');
        $action = Session::instance()->get('action');
        if (!$this->user) {
            if (Arr::get($_POST, 'submit')) {
                $username = $this->request->post('username');
                $password = $this->request->post('password');
                $remember = (bool)$this->request->post('remember');
                if (Auth::instance()->login($username, $password, $remember)) {
                    if (!$this->user) {
                        $this->user = Auth::instance()->get_user();
                    }
                    if ($action) {
                        $this->{$action}($id);
                    }
                    $this->redirect($this->last_uri);
                } else {
                    $this->errors['password'] = __('Неверная пара логин/пароль');
                }
            }
            if (ULogin::check_auth()) {
                if (!$this->user) {
                    $this->user = Auth::instance()->get_user();
                }
                if ($action) {
                    $this->{$action}($id);
                }
                $this->redirect($this->last_uri);
            }
        } else {
            $this->redirect(URL::site('privatecoach/profile'));
        }
        $this->page->content(View::factory('user/login')
            ->bind('error', $errors)
            ->bind('ulogin', $ulogin));
    }

    public function action_logout()
    {
        if ($this->user) {
            Auth::instance()->logout();
            Session::instance()->destroy();
            Cookie::delete('authautologin');
        }
        $this->redirect(URL::site(((I18n::$lang != 'ru') ? I18n::$lang : null) . '/privatecoach'));
    }

    public function action_goal()
    {
        $view = View::factory('privatecoach/user/edit/goal')
            ->bind('error', $error)
            ->bind('hint', $hint)
            ->bind('training', $training)
//            ->bind('time_error', $time_error)
            ->bind('validate_error', $validate_error)
            ->bind('mission', $current_mission)
            ->bind('mission_array', $mission_array)
            ->bind('program', $program)
//            ->bind('success', $success)
            ->bind('model', $logs);
        $training = array();
        $training['beginner'] = __('Начинающий');
        $training['amateur'] = __('Любитель');
        $training['advanced'] = __('Продвинутый');
        $training['prof'] = __('Профессионал');
//        $control_time = 900;
        $mission = ORM::factory('Mission_Log');
        $new_mission = $mission;
        $current_mission = $mission->where('user_id', '=', $this->user->id)->where('status', '=', null)->find();
        $logs = ORM::factory('User_Log')->where('user_id', '=', $this->user->id)->order_by('date', 'DESC')->order_by('id', 'DESC')->find();
        $user = ORM::factory('User', array('id' => $this->user->id));
        $program = $user->programs->where('activity', '=', 1)->find();
//        if ($current_mission->loaded()) {
//            if (((strtotime(date('Y-m-d H:i:s')) - strtotime($current_mission->date_begin)) > $control_time) && $current_mission->status != 1) {
//                $time_error = true;
//            }
//        }
        if ($this->request->post()) {

            $values = $this->request->post();
            $profile_validate = Validation::factory($_POST);
            $profile_validate->rules('target_weight', array(
                array('not_empty'),
            ));
            $profile_validate->rules('target_fat', array(
                array('not_empty'),
            ));
            $profile_validate->rules('training', array(
                array('not_empty'),
            ));

            if ($profile_validate->check()) {

                //Запись новой цели тренировок. Запись происходит, если предыдущая цель достигнута, и юзер ввел валидные данные.

                if ($this->condition($logs->fat, $logs->weight, strip_tags($values['target_fat']), strip_tags($values['target_weight']), $values['mission'])) {
                    $error = __('Ваши текущие данные уже соответсвуют поставленной цели');
                    $hint = __('Ваша масса:') . ' ' . $logs->weight . __('кг') . '. ' . __('Ваш уровень подкожного жира:') . ' ' . $logs->fat . '%.';
                } else {
                    $this->user->training = $values['training'];
                    $this->user->save();
                    $new_mission->date_begin = date('Y-m-d H:i:s');
                    $new_mission->user_id = $this->user->id;
                    $new_mission->weight = $values['target_weight'];
                    $new_mission->fat = $values['target_fat'];
                    $new_mission->type = $values['mission'];
                    $new_mission->save();
                    $current_mission = $new_mission;
//                        $success = __('Данные сохранены. Через 15 минут значение изменить будет нельзя до тех пор, пока не будет достигнута текущая цель');
                }

//                elseif (!$time_error) {
//
//                    if ($this->condition($logs->fat, $logs->weight, strip_tags($values['target_fat']), strip_tags($values['target_weight']), $values['mission'])) {
//                        $error = __('Ваши текущие данные уже соответсвуют поставленной цели. Пожалуйста, выберите другую цель. Ваша масса:') . ' ' . $logs->weight . __('кг') . '. ' . __('Ваш уровень подкожного жира:') . ' ' . $logs->fat . '%.';
//                    } else {
//                        //Редактирование недостигнутой цели. Редактирование происходит, если юзер ввел валидные данные в течение 15 минут после последнего редактирования.
//                        $this->user->training = $values['training'];
//                        $this->user->save();
//                        $current_mission->date_begin = date('Y-m-d H:i:s');
//                        $current_mission->user_id = $this->user->id;
//                        $current_mission->weight = $values['target_weight'];
//                        $current_mission->fat = $values['target_fat'];
//                        $current_mission->type = $values['mission'];
//                        $current_mission->save();
//                        $success = __('Данные сохранены. Через 15 минут значение изменить будет нельзя до тех пор, пока не будет достигнута текущая цель');
//                    }
//
//                }
                if (array_key_exists('program_selection', $values)) {
                    $this->redirect(i18n::$lang . '/privatecoach/program/list/?mission=' . $values['mission']);
                }
            } else {
                $validate_error = $profile_validate->errors(0);
            }
            $user->mission_id = ORM::factory('Mission')->where('sef', '=', $values['mission'])->find()->id;
            $user->save();

        }
        $mission_array = ORM::factory('Mission_Log')->types->find_all()->as_array('sef', 'title');
        $this->page->content($view);
    }

    public function action_load_photo()
    {
        $this->render = false;
        if ($_FILES) {
            $file_validate = Validation::factory($_FILES);
            $file_validate->rules('myfile', array(
                array("Upload::not_empty"),
                array("Upload::valid"),
                array("Upload::type", array(":value", array("png", "jpg", "gif", "jpeg"))),
                array("Upload::size", array(":value", "2M")),
                array('Upload::image')
            ));
            if ($file_validate->check()) {
                $file = Cms_Storage::instance()->upload($_FILES, 'myfile');
                $data['file'] = $file->id;
                $data['src'] = $file->url();
                $this->response->body(json_encode($data));
            } else {
                $this->response->body('error');
            }
        }
    }

    public function action_add_result()
    {
        $this->render = false;
        $activity = $this->request->query('activity') ? 1 : 0;
        $album_id = $this->request->query('album');
        $album = ORM::factory('User_Album', $album_id);
        $id = $this->request->param('id');
        if ($id) {
            $program = ORM::factory('Program', $id);
        } else {
            $program = ORM::factory('Program')->where('user_id', '=', $this->user->id)->where('activity', '=', 1)->find();
        }
        $age = $this->user->get_age($this->user->birthdate);
        if ($age < 0 || $age > 120) {
            $age_error = true;
        }
        if ($this->request->post()) {
            $values = $this->request->post();
            $validate = Validation::factory($_POST);
            $validate->rules('weight', array(
                array('not_empty'),
            ));
            $validate->rules('date', array(
                array('not_empty'),
            ));
//            $validate->rules('chest', array(
//                array('not_empty'),
//            ));
//            $validate->rules('stomach', array(
//                array('not_empty'),
//            ));
//            $validate->rules('haunch', array(
//                array('not_empty'),
//            ));
//            $validate->rules('triceps', array(
//                array('not_empty'),
//            ));
//            $validate->rules('blade', array(
//                array('not_empty'),
//            ));
//            $validate->rules('ilium', array(
//                array('not_empty'),
//            ));
//            $validate->rules('armpit', array(
//                array('not_empty'),
//            ));
            $validate->rules('file', array(
                array('not_empty'),
            ));
            if ($validate->check()) {
                if ($values) {
                    $model = ORM::factory('User_Log');
                    $old_fat = ORM::factory('User_Log')->where('user_id', '=', $this->user->id)->order_by('date', 'DESC')->order_by('id', 'DESC')->find()->fat;
                    $model->user_id = $this->user->id;
                    $model->fat = Arr::get($values, 'fat');
                    $model->weight = $values['weight'];
                    $model->height = Arr::get($values, 'height');
                    $model->date = Date::formatted_time($values['date'], 'Y-m-d');
                    $model->chest = Arr::get($values, 'chest');
                    $model->stomach = Arr::get($values, 'stomach');
                    $model->haunch = Arr::get($values, 'haunch');
                    $model->triceps = Arr::get($values, 'triceps');
                    $model->blade = Arr::get($values, 'blade');
                    $model->ilium = Arr::get($values, 'ilium');
                    $model->armpit = Arr::get($values, 'armpit');
                    $model->program_id = $program->id;
                    $model->status = $activity;
                    $model->photo = $values['file'];
                    $model->album_id = $album_id;
                    $model->status = 1;
                    $model->save();
                    $album->status = 1;
                    $album->save();
                    $this->user->fat = Arr::get($values, 'fat');
                    $this->user->weight = Arr::get($values, 'weight');
                    $this->user->height = Arr::get($values, 'height');
                    $this->user->save();

                    Feed::update_album($this->user->id, $album_id);
                    if (($old_fat - $model->fat) >= 1) {
                        Feed::new_fat($this->user, ($old_fat - round($model->fat, 2)));
                    }
                    //Проверка на заверщение миссии
                    $mission = ORM::factory('Mission_Log')->where('user_id', '=', $this->user->id)->where('status', '=', null)->find();
                    $logs = ORM::factory('User_Log')->where('user_id', '=', $this->user->id)->order_by('date', 'DESC')->order_by('id', 'DESC')->find();
                    if (($logs->fat < $mission->fat && ($mission->type == 'SLIMMING' && $logs->weight < $mission->weight)) || ($mission->type == 'MUSCLE' && $logs->weight > $mission->weight)) {
                        $close_mission_user = ORM::factory('Top')->where('user_id', '=', $this->user)->find();
                        $all_users = ORM::factory('Top')->where('user_id', '!=', $this->user)->find_all();
                        $this->mission_completed($mission, $close_mission_user, $all_users);
                    }
                }
            } else {
                $errors = $validate->errors(0);
                $this->response->body(json_encode($errors));
            }
        }
    }

    public function action_gallery()
    {
        $this->page->script('program_gallery', 'js/program_gallery.js');
        $view = View::factory('privatecoach/user/album/gallery')
            ->set('i', 1)
            ->bind('id', $id)
            ->bind('program_id', $program_id)
            ->bind('programs', $programs);
        $id = $this->request->param('id');
        $program_id = $this->request->query('q');
        if (!$id) {
            $this->redirect(i18n::$lang . '/privatecoach/user/gallery/' . $this->user->id . '?q=' . $program_id);
        }
        $programs = ORM::factory('Program')->where('user_id', '=', $id)->where('activity', '!=', 0)->find_all();
        $this->page->content($view);
    }


    public function action_album()
    {
        $this->page->style('datepicker.css', 'js/lib/datepicker/css/datepicker.css');
        $this->page->script('bootstrap-datepicker.js', 'js/lib/datepicker/js/bootstrap-datepicker.js');
        $this->page->script('bootstrap-datepicker.ru.js', 'js/lib/datepicker/js/locales/bootstrap-datepicker.ru.js');
        $this->page->script('bootstrap-datepicker.kk.js', 'js/lib/datepicker/js/locales/bootstrap-datepicker.kk.js');
        $this->page->script('popover', 'js/popover.js');
        $this->page->script('jquery.form.js', 'js/jquery.form.js');
        $this->page->script('profile', 'js/ba.album.js');
        $view = View::factory('privatecoach/user/edit/calc')
            ->bind('age', $age)
            ->bind('difference', $difference)
            ->bind('sex', $sex)
            ->bind('data_hint', $data_hint)
            ->bind('model', $model)
            ->bind('age_error', $age_error)
            ->bind('active_program', $active_program)
            ->bind('progress', $progress)
            ->bind('mission', $mission)
            ->bind('album_id', $album_id)
            ->bind('dt', $dt)
            ->bind('values', $values);
        $age = $this->user->get_age($this->user->birthdate);
        $active_program = ORM::factory('Program')->where('user_id', '=', $this->user->id)->where('activity', '=', 1)->find();
        $dt = new DateTime($active_program->start_date);
        $dt->add(new DateInterval('P' . ($active_program->duration * 7) . 'D'));
        $progress = ORM::factory('User_Log')->where('user_id', '=', $this->user->id)->where('program_id', '=', $active_program->id)->order_by('id', 'ASC')->find_all();
//        $inactive_log = ORM::factory('User_Log')->where('status', '=', 0)->find_all();
//        foreach ($inactive_log as $row) {
//            $row->delete();
//        }
        $inactive_albums = ORM::factory('User_Album')->where('status', '=', 0)->find_all();
        foreach ($inactive_albums as $row) {
            $row->delete();
        }
        $album = ORM::factory('User_Album');
        $album->user_id = $this->user->id;
        $album->created = date('Y-m-d');
        $album->save();
        $album_id = $album->id;
        $sex = $this->user->sex;
        $mission = ORM::factory('Mission_Log')->where('user_id', '=', $this->user)->order_by('id', 'DESC')->find();
        $model = ORM::factory('User_Log')->where('user_id', '=', $this->user->id)->order_by('date', 'DESC')->order_by('id', 'DESC')->find();
        if ($progress->count() >= 2) {
            foreach ($progress as $first_result) {
                $first_weight = $first_result->weight;
                $first_fat = $first_result->fat;
                break;
            }
            $last_weight = $model->weight;
            $last_fat = $model->fat;
            if ($mission->type == 'MUSCLE') {
                $difference['weight'] = $last_weight - $first_weight;
                $difference['fat'] = $last_fat - $first_fat;
            } else {
                $difference['weight'] = $first_weight - $last_weight;
                $difference['fat'] = $first_fat - $last_fat;
            }
        }

        $data_hint = array();
        $data_hint[] = __('Диагональная складка посередине между соском и верхней частью грудной мышцы у подмышки');
        $data_hint[] = __('Вертикальная складка 2,5 см вправо от пупка');
        $data_hint[] = __('Вертикальная складка посередине между коленной чащечкой и паховым сгибом (впадина, где верхняя часть ноги подсоединяется к ягодицам).');
        $data_hint[] = __('Вертикальная складка посередине между верхом плеча и локтём.');
        $data_hint[] = __('Диагональная (под углом 45 градусов) складка на верхней части спины чуть пониже лопатки.');
        $data_hint[] = __('Диагональная складка над верхней передней частью подвздошного гребешка (место выше верхней передней выступающей области бедренной кости)');
        $data_hint[] = __('Вертикальная складка непосредственно ниже от центра подмышки на уровне соска.');
        $this->page->content($view);
    }

    public function action_email()
    {
        $view = View::factory('privatecoach/user/edit/email')
            ->bind('error', $error)
            ->bind('success', $success)
            ->bind('model', $model);
        $post = $this->request->post();
        $error = array();
        $model = $this->user;
        if ($post) {
            if (Auth::instance()->check_password($post['password'])) {
                try {
                    $model->email = $post['new_address'];
                    $model->save();
                    $success = true;
                } catch (ORM_Validation_Exception $e) {
                    $array = $e->errors('validation');
                    $error[] = $array['email'];
                }
            } else {
                $error[] = __('Неверный пароль');
            }
        }
        $this->page->content($view);
    }

    public function action_password()
    {
        $view = View::factory('privatecoach/user/edit/password')
            ->bind('error', $error)
            ->bind('success', $success)
            ->bind('model', $model);
        $post = $this->request->post();
        $error = array();
        $model = $this->user;
        $valid = Validation::factory($post)
            ->rules('new_password', array(
                array('not_empty'),
                array('min_length', array(':value', 8)),
            ))
            ->rules('repeat_password', array(
                array('matches', array(':validation', ':field', 'new_password'))
            ));
        if ($post) {
            if ($valid->check()) {
                if (Auth::instance()->check_password($this->request->post('current_password'))) {
                    try {
                        $model->password = $this->request->post('new_password');
                        $model->save();
                        $success = true;
                    } catch (ORM_Validation_Exception $e) {
                        $array = $e->errors('validation');
                        $error[] = $array['password'];
                    }
                } else {
                    $error['current_password'] = __('Неверный пароль');
                }
            } else {
                $error = $valid->errors(0);
                $error[0] = current($error);
            }
        }
        $this->page->content($view);
    }


    public function action_remove_result()
    {
        $this->render = false;
        $id = (int)$this->request->query('id');
        $model = ORM::factory('User_Log', $id);
        if ($model->loaded() && $model->user_id == $this->user->id) {
            $model->delete();
            $this->response->body('success');
        } else {
            $this->response->body('error');
        }
    }


    public function mission_completed($current_mission, $close_mission_user, $all_users)
    {
        $current_mission->date_end = date('Y-m-d H:i:s');
        $current_mission->status = 1;
        if ($current_mission->save()) {
            if (!$close_mission_user->loaded()) {
                $close_mission_user->user_id = $this->user->id;
            }
            $close_mission_user->place = 1;
            $close_mission_user->save();
            foreach ($all_users as $user) {
                if ($user->place < $close_mission_user->place || $close_mission_user->place == 1) {
                    $user->place += 1;
                    if ($user->place > 5) {
                        $user->delete();
                    } else {
                        $user->save();
                    }
                }
            }
//            Feed::close_mission($this->user);
        };
    }

    public function action_counter()
    {
        $this->render = false;
        $id = $this->request->query('id');
        $counter_view = ORM::factory('User', array('id' => $id));
        $this->view_counter($counter_view->id);
        $counter = $counter_view->views;
        $this->response->body(View::factory('view_counter')
            ->bind('counter', $counter));
    }

    public function action_refresh_result()
    {
        $this->render = false;
        $active_program = ORM::factory('Program')->where('user_id', '=', $this->user->id)->where('activity', '=', 1)->find();
        $model = ORM::factory('User_Log')->where('user_id', '=', $this->user->id)->where('program_id', '=', $active_program->id)->order_by('id', 'DESC')->find();
        if ($model->loaded()) {
            $this->response->body(View::factory('privatecoach/user/album/new_photo')->bind('model', $model));
        } else {
            $this->response->body('error');
        }
    }

    public function action_save_result()
    {
        $this->render = false;
        $model = ORM::factory('User_Log')->where('user_id', '=', $this->user->id)->where('status', '=', 0)->find_all();
        $album = ORM::factory('User_Album');
        $album->user_id = $this->user->id;
        $album->created = date('Y-m-d');
        $album->save();
        foreach ($model as $row) {
            $row->status = 1;
            $row->album_id = $album->id;
            $row->save();
//            $photos[$row->id] = $row->photo;

        }
        Feed::update_album($this->user, $album->id);
        $this->response->body('success');

    }


    protected function condition($fat, $weight, $target_fat, $target_weight, $mission)
    {
        if ($mission == 'SLIMMING' && ($weight <= $target_weight || $fat <= $target_fat) || ($mission == 'MUSCLE' && $weight >= $target_weight)) {
            return true;
        }
    }

    protected function subscribe($id)
    {
        if (!$this->user->has('subscribes', $id)) {
            $model = ORM::factory('Subscribe');
            $model->user_id = $this->user->id;
            $model->target_id = $id;
            $model->save();
            Feed::new_sub($this->user, $id);
        }
        return;
    }

    public function add_program()
    {

    }
}