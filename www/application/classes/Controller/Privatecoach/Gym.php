<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Privatecoach_Gym extends Privatecoach_Site
{
    public function before()
    {
        $this->pc_need_auth = false;
        parent::before();
        Register::instance()->activemenu = 54;
    }

    public function scripts()
    {
        return parent::scripts() + array(
            'gym' => 'js/gym.js',
            'comments' => 'js/comments.js',
        );
    }

    public function action_list()
    {
        $this->page->title(__('Тренажерные залы'));
        $country = ORM::factory('Category')->where('slug', '=', 'kazahstan')->find();
        $cities = ORM::factory('City')->where('country_id', '=', $country->id)->where('is_published','=',1)->order_by('priority','ASC')->order_by('title_'.I18n::$lang,'ASC')->find_all()->as_array('id', 'title');
        $cityList[0] = __('Все города');
        foreach ($cities as $key=>$city){
            $cityList[$key]=$city;
        }

//        ksort($cityList);
        $array = array();
        $model = ORM::factory('Gym');
        $gyms = $model->limit(5)->find_all();
        $amount = $model->find_all()->count();
        $gym_score_model = ORM::factory('Gym_Score');
        foreach ($gyms as $row) {
            $cost = $gym_score_model->where('target_id', '=', $row->id)->find_all()->as_array('id', 'cost_score');
            $trainer = $gym_score_model->where('target_id', '=', $row->id)->find_all()->as_array('id', 'trainers_score');
            $free_weights = $gym_score_model->where('target_id', '=', $row->id)->find_all()->as_array('id', 'free_weights_score');
            $coach = $gym_score_model->where('target_id', '=', $row->id)->find_all()->as_array('id', 'coach_score');
            $location = $gym_score_model->where('target_id', '=', $row->id)->find_all()->as_array('id', 'location_score');
            if ($cost && $trainer && $free_weights && $coach && $location) {
                $total_score = (0.4 * array_sum($cost)) / count($cost)
                    + (0.15 * array_sum($trainer)) / count($trainer)
                    + (0.15 * array_sum($free_weights)) / count($free_weights)
                    + (0.15 * array_sum($coach)) / count($coach)
                    + (0.15 * array_sum($location)) / count($location);
                $array[$row->id] = $total_score;
            }
        }
        $this->page->content(View::factory('privatecoach/gyms/list')
            ->bind('amount', $amount)
            ->bind('array', $array)
            ->bind('model', $gyms)
            ->set('select_id', 1)
            ->bind('cityList', $cityList));
    }

    public function action_view()
    {
        $this->page->script('yandexmap','http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU');
        $this->page->script('map','js/gymmap.js');
        $id = $this->request->param('id');
        $model = ORM::factory('Gym', $id);
        if (!$model->loaded())
            throw new HTTP_Exception_404('Страница не загружена');
        if ($this->user) {
            $score = $model->gym_score->where('user_id', '=', $this->user->id)->find();
        }
        $scores = ORM::factory('Gym_Score')->where('target_id', '=', $id)->find_all()->count();
        $this->view_counter($model);
        $this->page->title($model->title);
        $this->page->meta($model->description, 'description');
        $this->page->content(View::factory('privatecoach/gyms/view')
                ->bind('model', $model)
                ->bind('scores', $scores)
                ->bind('score', $score)
        );
    }

    public function action_ajax_list()
    {
        $this->render = false;
        $city = $this->request->query('city');
        $ratSort = $this->request->query('score');
        $alphabetSort = $this->request->query('alphabet');
        $counter = $this->request->query('counter') ? $this->request->query('counter') : 1;

        $check = 1;
        $data = array();
        $selected = $this->request->query('selected');

        $model = ORM::factory('Gym');
        if ($city) {
            $model = $model->where('city_id', '=', $city);
        }
        if ($ratSort) {
            $model = $model->order_by('score', 'DESC');
        }
        if ($alphabetSort) {
            $model = $model->order_by('title_' . I18n::$lang, 'ASC');
        }
        $model = $model->limit(5)->offset(($counter - 1) * 5)->find_all();
        $amount = $model->count();
        $this->response->body(View::factory('privatecoach/gyms/ajax_list')
                ->bind('selected', $selected)
                ->bind('check', $check)
                ->bind('amount', $amount)
                ->bind('model', $model)
        );
    }

    public function action_score()
    {
        $this->render = false;
        $score = $this->request->query('score');
        $type = $this->request->query('type');
        $target = $this->request->query('target');
        $gym = ORM::factory('Gym', $target);
        $model = ORM::factory('Gym_Score')->where('user_id', '=', $this->user->id)->where('target_id', '=', $target)->find();
        $model->{$type . '_score'} = $score;
        if (!$model->loaded()) {
            $model->user_id = $this->user->id;
            $model->target_id = $target;
        }
        if ($model->cost_score != 0 && $model->trainers_score != 0 && $model->free_weights_score != 0 && $model->coach_score != 0 & $model->location_score != 0) {
            $model->total_score = 0.4 * $model->cost_score + 0.15 * $model->trainers_score + 0.15 * $model->free_weights_score + 0.15 * $model->coach_score + 0.15 * $model->location_score;
        }
        $model->save();
        $array = $gym->gym_score->find_all()->as_array('id', 'total_score');
        $gym->score = array_sum($array) / count($array);
        $gym->save();
    }

    public function action_filter()
    {
        $this->render = false;
        $sort = (int)$this->request->query('sort');
        $view = View::factory('privatecoach/gyms/rating')
            ->bind('amount', $amount)
            ->bind('model', $model);
        if ($sort) {
            $model = ORM::factory('Gym')->order_by('score', 'DESC')->find_all();
        } else {
            $model = ORM::factory('Gym')->find_all();
        }
        $amount = $model->count();
//        if ($model->count() == $amount) {
//            $check = false;
//        }
        $this->response->body($view);
    }

    public function action_xmlgeoobject()
    {
        $this->render = FALSE;
        $id = $this->request->query('id');
        $points = ORM::factory('Gym', array('id' => $id));
        $this->response->headers('Content-type', 'text/xml');
        $this->response->body(View::factory('privatecoach/gyms/xmlgeoobject')
            ->bind('point', $points)
            ->bind('id', $id));
    }

}