<?php

class Controller_Privatecoach_Profile extends Privatecoach_Site
{

    public function before()
    {
//        $this->need_auth = true;
        parent::before();
    }

    public function action_index()
    {
        $this->page->title(__('Профиль'));
        $view = View::factory('privatecoach/user/profile')
            ->bind('subscribe', $subscribe)
            ->bind('top_user', $top_user)
            ->bind('current_user', $current_user)
            ->bind('user_mission', $user_mission)
            ->bind('user_id', $id)
//            ->bind('user_log', $user_log)
            ->bind('chartData', $chartData)
            ->bind('current_user_program', $current_user_program)
            ->bind('own_profile', $own_profile)
            ->bind('log_count', $log_count)
            ->bind('week_days', $week_days)
            ->bind('ulogin', $ulogin)
            ->bind('program', $program);
        $id = $this->request->param('id');
        $ulogin = ULogin::widget_ulogin('Promt');
        if ($this->user) {
            if ($this->user->id == $id) {
                $this->page->script('jquery.form', 'js/jquery.form.js');
            }
        }
        $this->page->script('angular', 'js/lib/angular.min.js');
        $this->page->script('bootstrap-datepicker.js', 'js/lib/datepicker/js/bootstrap-datepicker.js');
        $this->page->script('resource', 'js/lib/angular/angular-resource.min.js');
        $this->page->script('sanitize', 'js/lib/angular/angular-sanitize.js');
        $this->page->script('privatecoach', 'js/privatecoach.js');
        $this->page->script('appChart', 'js/appChart.js');
        $this->page->script('jsgl', 'js/js2D/jsgl.min.js');
        $this->page->script_dir('js/privatecoach');

        if ($id) {
            $this->check_personal_data($id);
            $current_user = ORM::factory('User', array('id' => $id));
            if (!$current_user->loaded()) {
                throw new HTTP_Exception_404('Страница не найдена');
            }
        } else {
            if ($this->user) {
                $this->redirect(URL::site(I18n::$lang . '/privatecoach/profile/' . $this->user->id));
            } else {
                $this->redirect(URL::site(I18n::$lang . '/privatecoach'));
            }
        }
        $subscribe = ORM::factory('Subscribe');
//        $user_log = ORM::factory('User_Log')->where('user_id', '=', $id)->order_by('date', 'DESC')->order_by('id', 'DESC')->find();
        $current_program = ORM::factory('Program')->where('user_id', '=', $id)->where('activity', '=', 1)->find();
        $logs = ORM::factory('User', $id)->logs->where('program_id', '=', $current_program)->order_by('date', 'DESC')->limit(10)->find_all();
        $log_count = $logs->count();
        $user_mission = ORM::factory('Mission_Log')->where('user_id', '=', $id)->order_by('date_begin', 'DESC')->find();
        if (!$current_user->loaded()) {
//            throw new HTTP_Exception_404();
        }
        View::bind_global('current_user', $current_user);

        $this->view_counter($current_user);
        $program = ORM::factory('Program')->where('user_id', '=', $id)->where('activity', '=', 1)->order_by('id', 'DESC')->find();
        $exercises = $program->program_exercises->find_all()->as_array();
        foreach ($exercises as $row) {
            $week_days[$row->week_day] = $row->week_day;
        }
        $top_user = ORM::factory('Top')->where('user_id', '=', $id)->find();
        if ($this->user) {
            $own_profile = $this->user->id == $id;
        } else {
            $own_profile = false;
        }
        $this->page->content($view);
    }

    public function action_chartData()
    {
        $this->render = false;
        $id = $this->request->param('id');
        $user = ORM::factory('User', array('id' => $id));

        if (!$user->loaded()) throw new HTTP_Exception_404();
        $current_program = ORM::factory('Program')->where('user_id', '=', $id)->where('activity', '=', 1)->find();
        $logs = $user->logs->where('program_id', '=', $current_program)->order_by('date', 'DESC')->limit(10)->find_all()->as_array();
//        if (count($logs) == 10) {
        $data = array();
        $i = 0;
        foreach ($logs as $log) {
            $data[$i] = array(
                'week' => $i,
                'weight' => $log->weight ? $log->weight : null,
                'fat' => $log->fat,
                'date' => $log->date
            );
            $i++;
        }
        $this->response->body(json_encode($data));
    }

//    }

    public function action_photo_preview()
    {
        $this->render = false;
        $id = $this->request->param('id');
        $i = 0;
        $preview = $this->request->query('preview') ? $this->request->query('preview') : false;
        $active_program = ORM::factory('Program')->where('user_id', '=', $id)->where('activity', '=', 1)->find();
        $model = ORM::factory('User_Log')
            ->where('program_id', '=', $active_program->id)
            ->order_by('date', 'DESC')
            ->order_by('id', 'DESC')
            ->find_all();
        $count = $model->count();
        if ($preview) {
            if ($count == 1) {
                foreach ($model as $row) {
                    $array['left']['photo'] = $row->photo_s;
                    $array['left']['weight'] = $row->weight;
                    $array['left']['fat'] = $row->fat;
                    $array['left']['date'] = $row->date;
                }
            } else {
                foreach ($model as $row) {
                    $i++;
                    if ($i == 1) {
                        $array['right']['photo'] = $row->photo_s;
                        $array['right']['weight'] = $row->weight;
                        $array['right']['fat'] = $row->fat;
                        $array['right']['date'] = $row->date;
                    } elseif ($i == $model->count()) {
                        $array['left']['photo'] = $row->photo_s;
                        $array['left']['weight'] = $row->weight;
                        $array['left']['fat'] = $row->fat;
                        $array['left']['date'] = $row->date;
                    } else {
                        if (array_key_exists('center', $array)) {
                            if (count($array['center']) < 3) {
                                $array['center'][] = $row->photo_s;
                            }
                        } else {
                            $array['center'][] = $row->photo_s;
                        }
                    }

                }
            }
        } else {
            if ($count == 1) {
                foreach ($model as $row) {
                    $array['left']['photo'] = $row->photo_s;
                    $array['left']['weight'] = $row->weight;
                    $array['left']['fat'] = $row->fat;
                    $array['left']['date'] = $row->date;
                }
            } else {
                foreach ($model as $row) {
                    $i++;
                    switch ($i) {
                        case 1:
                            $array['right']['photo'] = $row->photo_s;
                            $array['right']['weight'] = $row->weight;
                            $array['right']['fat'] = $row->fat;
                            $array['right']['date'] = $row->date;
                            break;
                        case $model->count():
                            $array['left']['photo'] = $row->photo_s;
                            $array['left']['weight'] = $row->weight;
                            $array['left']['fat'] = $row->fat;
                            $array['left']['date'] = $row->date;
                            break;
                    }
                }
            }
        }
        $this->response->body(View::factory('privatecoach/user/album/photo_preview')
            ->bind('array', $array)
            ->bind('id', $id)
            ->bind('preview', $preview)
            ->bind('model', $model));
    }

    public function action_album()
    {
        $this->page->title(__('Альбом "До и после"'));
        $id = $this->request->param('id');
        $model = ORM::factory('Progress')
            ->where('user_id', '=', $id)
            ->order_by('date', 'DESC')
            ->order_by('id', 'DESC')
            ->find_all();
        if ($model->count() > 0) {
            $check = true;
        }
        $this->page->content(View::factory('privatecoach/user/album/album')
                ->bind('check', $check)
                ->bind('model', $model)
        );
    }


    protected function condition($fat, $weight, $target_fat, $target_weight, $mission)
    {
        if ($mission == 'SLIIMING' && ($weight <= $target_weight || $fat <= $target_fat) || ($mission == 'MUSCLE' && $weight >= $target_weight)) {
            return true;
        }
    }

//    public function mission_completed($current_mission)
//    {
//        Feed::close_mission($this->user);
//        $current_mission->date_end = date('Y-m-d H:i:s');
//        $current_mission->status = 1;
//        $current_mission->save();
//    }
}