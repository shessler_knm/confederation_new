<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Privatecoach_Information extends Privatecoach_Site
{
    public function before()
    {
        $this->need_auth = true;
        parent::before();
    }

    public function action_personal_data()
    {
//        if ($this->user->pers_data == 1) {
//            $this->redirect(I18n::$lang . '/privatecoach/profile/' . $this->user->id);
//        }
        $this->page->script('loadimage', 'js/loadimage.js');
        $this->page->style('datepicker.css', 'js/lib/datepicker/css/datepicker.css');
        $this->page->script('bootstrap-datepicker.js', 'js/lib/datepicker/js/bootstrap-datepicker.js');
        $this->page->script('bootstrap-datepicker.ru.js', 'js/lib/datepicker/js/locales/bootstrap-datepicker.ru.js');
        $this->page->script('bootstrap-datepicker.kk.js', 'js/lib/datepicker/js/locales/bootstrap-datepicker.kk.js');
        $view = View::factory('privatecoach/information/personal_data')
            ->bind('error', $error)
            ->bind('training', $training)
            ->bind('sex', $sex)
            ->bind('date', $date)
//            ->bind('model', $model)
            ->bind('validate_error', $validate_error)
            ->bind('mission_targets', $mission_targets)
            ->bind('mission_array', $mission_array)
            ->bind('mission', $current_mission);
        $sex = array();
        $sex[1] = __('Муж.');
        $sex[2] = __('Жен.');
        $training = array();
        $training['beginner'] = __('Начинающий');
        $training['advanced'] = __('Продвинутый');
        for ($i = intval(date('Y')) - 99; $i <= intval(date('Y')); $i++) {
            $date['years'][$i] = $i;
        }
        for ($i = 1; $i <= intval(date('t')); $i++) {
            $date['days'][$i] = $i;
        };
        $date['months'][1] = __('Январь');
        $date['months'][2] = __('Февраль');
        $date['months'][3] = __('Март');
        $date['months'][4] = __('Апрель');
        $date['months'][5] = __('Май');
        $date['months'][6] = __('Июнь');
        $date['months'][7] = __('Июль');
        $date['months'][8] = __('Август');
        $date['months'][9] = __('Сентябрь');
        $date['months'][10] = __('Октябрь');
        $date['months'][11] = __('Ноябрь');
        $date['months'][12] = __('Декабрь');
        $mission = ORM::factory('Mission_Log');
        $new_mission = $mission->where('user_id', '=', $this->user->id)->find();
        if ($this->request->post()) {
            $values = $this->request->post();
            $profile_validate = Validation::factory($_POST);
            $profile_validate->rules('firstname', array(
                array('not_empty'),
            ));
            $profile_validate->rules('lastname', array(
                array('not_empty'),
            ));
//            $profile_validate->rules('height', array(
//                array('not_empty'),
//            ));
            $profile_validate->rules('birthdate', array(
                array('not_empty'),
                array('date')
            ));
//            $profile_validate->rules('weight', array(
//                array('not_empty'),
//            ));
            $profile_validate->rules('training', array(
                array('not_empty'),
            ));
            $profile_validate->rules('photo', array(
                array('not_empty'),
            ));

//                if ($this->condition($logs->fat, $logs->weight, strip_tags($values['target_fat']), strip_tags($values['target_weight']), $values['mission'])) {
//                    $error = __('Ваши текущие данные уже соответсвуют поставленной цели. Пожалуйста, выберите другую цель. Ваша масса:') . ' ' . $logs->weight . __('кг') . '. ' . __('Ваш уровень подкожного жира:') . ' ' . $logs->fat . '%.';
//                }
            $this->user->values($values, array('firstname', 'lastname', 'sex', 'birthdate', 'height', 'weight'));
            $this->user->height = $values['height'] ? $values['height'] : null;
            $this->user->training = $values['training'];
            $this->user->save();
            $new_mission->date_begin = date('Y-m-d H:i:s');
            $new_mission->user_id = $this->user->id;
            if ($values['weight']) {
                $new_mission->weight = $values['weight'];
            }
            if ($values['fat']) {
                $new_mission->fat = $values['fat'];
            }
            $new_mission->type = $values['type'];
            $new_mission->save();
            $current_mission = $new_mission;
            if ($profile_validate->check()) {
                $this->user->pers_data = 1;
                $this->user->save();
                $this->redirect(URL::site('privatecoach/program/list/?mission=' . $new_mission->type));
            } else {
                $validate_error = $profile_validate->errors(0);
                if (array_key_exists('photo', $validate_error)) {
                    $validate_error['photo'] = 'Добавьте фотографию профиля';
                }
            }
        }
        $mission_targets = $this->user->mission_logs->order_by('id', 'DESC')->find();
        $mission_array = ORM::factory('Mission_Log')->types->find_all()->as_array('sef', 'title');
        $this->page->content($view);

    }

    protected function condition($fat, $weight, $target_fat, $target_weight, $mission)
    {
        if ($mission == 'SLIMMING' && ($weight <= $target_weight || $fat <= $target_fat) || ($mission == 'MUSCLE' && $weight >= $target_weight)) {
            return true;
        }
    }
}