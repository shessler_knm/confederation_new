<?php
class Controller_Privatecoach_Top extends Privatecoach_Site
{
    public function before()
    {
        $this->pc_need_auth = false;
        parent::before();
    }

    public function action_preview()
    {
        $this->render = false;
        $subscribe = ORM::factory('Subscribe');
        $top = ORM::factory('Top')->order_by('place', 'ASC')->limit(5)->find_all()->as_array(null, 'user_id');
        if ($top) {
            $model = ORM::factory('User')->where('id', 'in', $top)->find_all()->as_array();
            krsort($model);
        }
        $this->response->body(View::factory('privatecoach/top/preview')
            ->set('place', 1)
            ->bind('subscribe', $subscribe)
            ->bind('model', $model));
    }

    public function action_users()
    {
        $this->page->title(__('Участники'));
        $this->page->script('users','js/users.js');
        $post = $this->request->post('name');
        $subscribe = ORM::factory('Subscribe');
        $model = ORM::factory('User');
        if ($post) {
            $just_users = ORM::factory('User')->where_open()
                ->where('firstname', 'LIKE', "%$post%")
                ->or_where('lastname', 'LIKE', "%$post%")
                ->or_where('middlename', 'LIKE', "%$post%")
                ->where_close()
                ->find_all()
                ->as_array('id', 'id');
            $result = true;
            if (!count($just_users)) {
                $just_users[] = 0;
            }
            $model = $model->where('id', 'in', $just_users);
        }
        $id = $this->user?$this->user->id:null;
        $model = $model->where('pers_data', '=', 1)->where('id','!=',$id);
        $model = $model->find_all();
        if ($model->count() == 0) {
            $empty = __('Поиск не дал результатов');
        }
        foreach ($model as $user) {
            $programs[$user->id] = $user->programs->where('activity', '=', 1)->find();
        }
        $this->page->content(View::factory('privatecoach/top/users')
                ->bind('model', $model)
                ->bind('programs', $programs)
                ->bind('result', $result)
                ->bind('subscribe', $subscribe)
                ->bind('empty', $empty)
                ->bind('name', $post)
        );
    }

    public function action_ajax_users()
    {
        $this->render = false;
        $q = $this->request->query('q');
        $check = 1;
        $data = array();
        $model = ORM::factory('User')->where('pers_data','=','1');
        if ($q) {
            $just_users = ORM::factory('User')->where_open()
                ->where('firstname', 'LIKE', "%$q%")
                ->or_where('lastname', 'LIKE', "%$q%")
                ->or_where('middlename', 'LIKE', "%$q%")
                ->where_close()
                ->find_all()
                ->as_array('id', 'id');
            $result = true;
            if (!count($just_users)) {
                $just_users[] = 0;
            }
            $model = $model->where('id', 'in', $just_users);
        }
        $counter = $this->request->query('counter') ? $this->request->query('counter') : 1;
        $subscribe = ORM::factory('Subscribe');
        $model = $model->find_all();
        foreach ($model as $row) {
            if ($check > (($counter - 1) * 10) && $check <= $counter * 10) {
                $data[] = $row;
            }
            $check++;
            $programs[$row->id] = $row->programs->where('activity', '=', 1)->find();
        }
        $amount = $model->count() - ($counter * 10);
        if ($data) {
            $model = $data;
        }
        $this->response->body(View::factory('privatecoach/top/ajax_users')
                ->bind('selected', $selected)
                ->bind('check', $check)
                ->bind('amount', $amount)
                ->bind('programs', $programs)
                ->bind('subscribe', $subscribe)
                ->bind('model', $model)
        );
    }

    public function action_photo_preview()
    {
        $this->render = false;
        $this->need_auth = false;
        $this->pc_need_auth = false;
        $id = $this->request->param('id');
        $i = 1;
        $preview = $this->request->query('preview') ? $this->request->query('preview') : false;
        $model = ORM::factory('User_Log')
            ->where('user_id', '=', $id)
            ->order_by('date', 'DESC')
            ->order_by('id', 'DESC')
            ->find_all();
        $count = $model->count();
        if ($preview) {
            if ($count == 1) {
                foreach ($model as $row) {
                    $array['left']['photo'] = $row->photo_s;
                    $array['left']['weight'] = $row->weight;
                    $array['left']['fat'] = $row->fat;
                    $array['left']['date'] = $row->date;
                }
            } else {
                foreach ($model as $row) {
                    switch ($i) {
                        case 1:
                            $array['right']['photo'] = $row->photo_s;
                            $array['right']['weight'] = $row->weight;
                            $array['right']['fat'] = $row->fat;
                            $array['right']['date'] = $row->date;
                            break;
                        case 2:
                            $array['left']['photo'] = $row->photo_s;
                            $array['left']['weight'] = $row->weight;
                            $array['left']['fat'] = $row->fat;
                            $array['left']['date'] = $row->date;
                            break;
                        case 3:
                        case 4:
                        case 5:
                            $array['center'][] = $row->photo_s;
                            break;
                    }
                    $i++;
                }
            }
        } else {
            if ($count == 1) {
                foreach ($model as $row) {
                    $array['left']['photo'] = $row->photo_s;
                    $array['left']['weight'] = $row->weight;
                    $array['left']['fat'] = $row->fat;
                    $array['left']['date'] = $row->date;
                }
            } else {
                foreach ($model as $row) {
                    switch ($i) {
                        case 1:
                            $array['right']['photo'] = $row->photo_s;
                            $array['right']['weight'] = $row->weight;
                            $array['right']['fat'] = $row->fat;
                            $array['right']['date'] = $row->date;
                            break;
                        case 2:
                            $array['left']['photo'] = $row->photo_s;
                            $array['left']['weight'] = $row->weight;
                            $array['left']['fat'] = $row->fat;
                            $array['left']['date'] = $row->date;
                            break;
                    }
                }
            }
        }
        $this->response->body(View::factory('privatecoach/user/album/photo_preview')
            ->bind('array', $array)
            ->bind('id', $id)
            ->bind('preview', $preview)
            ->bind('model', $model));
    }

    public function action_100()
    {
        $this->page->title(__('ТОП 100'));
        $post = htmlspecialchars(strip_tags($this->request->post('name')));
        $subscribe = ORM::factory('Subscribe');
        $top_users = ORM::factory('Top');
        if ($post) {
            $just_users = ORM::factory('User')->where_open()
                ->where('firstname', 'LIKE', "%$post%")
                ->or_where('lastname', 'LIKE', "%$post%")
                ->or_where('middlename', 'LIKE', "%$post%")
                ->where_close()
                ->find_all()
                ->as_array('id', 'id');
            $result = true;
            if (!count($just_users)) {
                $just_users[] = 0;
            }
            $top_users = $top_users->where('user_id', 'in', $just_users);
        }
        $users = $top_users->order_by('place', 'ASC');
        $pagination = $this->pagination($users, 'top_pagination.default');
        $users = $users->find_all();
        foreach ($users as $user) {
            $programs[$user->user->id] = $user->user->programs->where('activity', '=', 1)->find();
        }
        if ($users->count() == 0) {
            $empty = __('Поиск не дал результатов');
        }
        $this->page->content(View::factory('privatecoach/top/list')
                ->bind('programs', $programs)
                ->bind('pagination', $pagination)
                ->bind('post', $post)
                ->bind('result', $result)
                ->bind('empty', $empty)
                ->bind('model', $users)
                ->bind('subscribe', $subscribe)
        );

    }

    public function action_main_preview(){
        $this->render = false;
        $top = ORM::factory('Top')->order_by('place', 'ASC')->limit(3)->find_all()->as_array(null, 'user_id');
        if ($top) {
            $model = ORM::factory('User')->where('id', 'in', $top)->find_all()->as_array();
            krsort($model);
            $this->response->body(View::factory('privatecoach/top/main_preview')
                    ->set('place', 1)
                    ->bind('model', $model)
            );
        }

    }
}
