<?php

/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 05.08.13
 * Time: 11:28
 * To change this template use File | Settings | File Templates.
 */
class Controller_Privatecoach_Program extends Privatecoach_Site
{

    public function before()
    {
        parent::before();
        if ($this->request->action() != 'my_programs') {
            Register::instance()->activemenu = 52;
        }
    }

    public function scripts()
    {
        return parent::scripts() + array(
            'comments' => 'js/comments.js',
        );
    }

    public function action_ajax_exercise()
    {
        $this->render = false;
        $view = View::factory('privatecoach/program/ajax_exercise')
            ->bind('muscles', $muscles)
            ->bind('exercise', $exercise);
        $id = $this->request->param('id');
        $exercise = ORM::factory('Exercise', $id);
        $muscles = $exercise->categories->find_all();
        $this->response->body($view);
    }

    public function action_main_preview()
    {
//        $this->pc_need_auth = true;
        $this->render = false;
        $scores = array();
        $model = ORM::factory('Program')->where('type', '=', 'free')->limit(3)->find_all();
        foreach ($model as $row) {
            $score = $row->program_scores->find_all()->as_array('id', 'score');
            if ($score) {
                $scores[$row->id] = array_sum($score) / count($score);
            }
        }
        $this->response->body(
            View::factory('privatecoach/program/main_preview')
                ->bind('model', $model)
                ->bind('scores', $scores)
        );

    }

    public function action_edit()
    {
        $this->page->style('datepicker.css', 'js/lib/datepicker/css/datepicker.css');
        $this->page->script('angular', 'js/lib/angular.min.js');
        $this->page->script('bootstrap-datepicker.js', 'js/lib/datepicker/js/bootstrap-datepicker.js');
        $this->page->script('bootstrap-datepicker.ru.js', 'js/lib/datepicker/js/locales/bootstrap-datepicker.ru.js');
        $this->page->script('bootstrap-datepicker.kk.js', 'js/lib/datepicker/js/locales/bootstrap-datepicker.kk.js');
        $this->page->script('resource', 'js/lib/angular/angular-resource.min.js');
        $this->page->script('sanitize', 'js/lib/angular/angular-sanitize.js');
        $this->page->script('privatecoach', 'js/privatecoach.js');
        $this->page->script('popover', 'js/popover.js');
        $this->page->script('jquery.form.js', 'js/jquery.form.js');
        $this->page->script('edit_program', 'js/edit_program.js');
        $this->page->script_dir('js/privatecoach');
        $this->page->content(
            View::factory('privatecoach/program/edit_program')
                ->bind('album_id', $album_id)
                ->bind('query', $query)
                ->bind('age', $age)
                ->bind('id', $id)
                ->bind('sex', $sex)
                ->bind('level', $level)
                ->bind('data_hint', $data_hint)
                ->bind('model', $model)
        );
        $levels = array(
            'beginner' => 0.4,
            'amateur' => 0.6,
            'advanced' => 0.8,
            'professional' => 1
        );
        $level = $levels[mb_strtolower($this->user->training)];
        $id = $this->request->param('id');
        $model = ORM::factory('Program', $id);
        if (!$model->loaded()) {
            throw new HTTP_Exception_404();
        }
        $query = $this->request->query();
        $inactive_albums = ORM::factory('User_Album')->where('status', '=', 0)->find_all();
        foreach ($inactive_albums as $row) {
            $row->delete();
        }
        $album = ORM::factory('User_Album');
        $album->user_id = $this->user->id;
        $album->created = date('Y-m-d');
        $album->save();
        $album_id = $album->id;
        $age = $this->user->get_age();
        if (!$model->loaded() /* || $model->activity == 1*/) {
            $this->redirect('privatecoach/profile/' . $this->user->id);
        }
        $data_hint = array();
        $data_hint[] = __('Диагональная складка посередине между соском и верхней частью грудной мышцы у подмышки');
        $data_hint[] = __('Вертикальная складка 2,5 см вправо от пупка');
        $data_hint[] = __('Вертикальная складка посередине между коленной чащечкой и паховым сгибом (впадина, где верхняя часть ноги подсоединяется к ягодицам).');
        $data_hint[] = __('Вертикальная складка посередине между верхом плеча и локтём.');
        $data_hint[] = __('Диагональная (под углом 45 градусов) складка на верхней части спины чуть пониже лопатки.');
        $data_hint[] = __('Диагональная складка над верхней передней частью подвздошного гребешка (место выше верхней передней выступающей области бедренной кости)');
        $data_hint[] = __('Вертикальная складка непосредственно ниже от центра подмышки на уровне соска.');
    }

    public function action_choose()
    {

//        $this->page->style('datepicker.css', 'js/lib/datepicker/css/datepicker.css');
//        $this->page->script('angular', 'js/lib/angular.min.js');
//        $this->page->script('bootstrap-datepicker.js', 'js/lib/datepicker/js/bootstrap-datepicker.js');
//        $this->page->script('resource', 'js/lib/angular/angular-resource.min.js');
//        $this->page->script('sanitize', 'js/lib/angular/angular-sanitize.js');
//        $this->page->script('privatecoach', 'js/privatecoach.js');
//        $this->page->script_dir('js/privatecoach');
        $this->page->content(
            View::factory('privatecoach/program/add_program')
                ->bind('prof_programs', $prof_programs)
                ->bind('user_programs', $user_programs)
        );
        $program = ORM::factory('Program');
        $prof_programs = $program->where('type', '=', 'free')->order_by('id', 'DESC')->find_all();
        $user_programs = $program->where('type', '!=', 'free')->order_by('id', 'DESC')->find_all();
    }

    public function action_my_programs()
    {
        $this->page->script('angular', 'js/lib/angular.min.js');
//        $this->page->script('bootstrap-datepicker.js','js/lib/datepicker/js/bootstrap-datepicker.js');
        $this->page->script('resource', 'js/lib/angular/angular-resource.min.js');
        $this->page->script('sanitize', 'js/lib/angular/angular-sanitize.js');
        $this->page->script('privatecoach', 'js/privatecoach.js');
        $this->page->script_dir('js/privatecoach');
        $this->page->script('program_selection', 'js/program_selection.js');
        $this->page->title(__('Мои программы'));
        $data = array();
        $score = array();
        $programs = array();
        $current_user_program = ORM::factory('Program')->where('user_id', '=', $this->user->id)->where('activity', '=', 1)->find();
        $date = new DateTime($current_user_program->start_date);
        $date->add(new DateInterval('P' . ($current_user_program->duration * 7) . 'D'));
        $last_programs = ORM::factory('Program')->where('user_id', '=', $this->user->id)->where('activity', '=', 2)->find_all();
        foreach ($last_programs as $program) {
            $dt = new DateTime($program->start_date);
            $dt->add(new DateInterval('P' . ($program->duration * 7) . 'D'));
            if ($dt->format('Y-m-d') <= date('Y-m-d')) {
                $programs[] = $program->id;
                $data[$program->id] = $dt->format('Y-m-d');
                $rating = $program->program_scores->find_all()->as_array('id', 'score');
                $score[$program->id] = $rating ? array_sum($rating) / count($rating) : 0;
            }
        }
        if ($programs) {
            $last_programs = ORM::factory('Program')
                ->where('id', 'IN', $programs)
                ->order_by('id', 'DESC')
                ->limit(3)
                ->find_all();
            $count = true;
        } else {
            $count = false;
        }

        $this->page->content(View::factory('privatecoach/program/schedule')
                ->bind('data', $data)
                ->bind('date', $date)
                ->bind('current_user_program', $current_user_program)
                ->bind('last_programs', $last_programs)
                ->bind('count', $count)
        );
    }

    public function action_list()
    {
        $this->page->title(__('Программы тренировок'));
        $this->page->script('program_selection', 'js/program_selection.js');
        $query = $this->request->query();
        if ($query) {
            if (array_key_exists('id', $query)) {
                $model = ORM::factory('Program', $query['id']);
                if ($model->loaded()) {
                    $model->delete();
                }
            }
        }

        $categories = ORM::factory('Category')->where('sef', '=', 'muscle_group')->find();
        $merge = array('0' => '');
        $categories = $categories->children->find_all()->as_array('id', 'title_' . I18n::$lang);
        $categories[0] = '';
        ksort($categories);
        $array['place'] = array('', 'home' => __('Дома'), 'gym' => __('Тренажерный зал'));
        $array['goals'] = array_merge($merge, ORM::factory('Mission')->find_all()->as_array('sef', 'title_' . I18n::$lang));
//        $array['body']=ORM::factory('Mission')->get_sef_body()->find_all();
        $array['sessions'] = array(
            '0' => '',
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            '7' => __('Каждый день')
        );
        $array['sex'] = array(
            '0' => '',
            '1' => __('Муж'),
            '2' => __('Жен'),
        );
        $this->page->content(View::factory('privatecoach/program/list')
//                ->bind('goal', $goal)
//                ->bind('model', $model)
//                ->bind('current_user_program', $current_user_program)
                ->bind('array', $array)
                ->bind('query', $query)
                ->bind('categories', $categories)
//                ->bind('pagination', $pagination)
        );

    }

    public function action_view()
    {
        $this->page->script('angular', 'js/lib/angular.min.js');
        $this->page->script('resource', 'js/lib/angular/angular-resource.min.js');
        $this->page->script('sanitize', 'js/lib/angular/angular-sanitize.js');
        $this->page->script('privatecoach', 'js/privatecoach.js');
        $this->page->script_dir('js/privatecoach');
        $this->page->script('program_selection', 'js/program_selection.js');
        $array = array();
        $id = $this->request->param('id');
        $program = ORM::factory('Program', $id);
        if (!$program->loaded() || $program->parent->loaded()) {
            throw new HTTP_Exception_404();
        }
        $program_scores = $program->program_scores->find_all()->as_array('id', 'score');
        if ($program_scores) {
            $score['score'] = array_sum($program_scores) / count($program_scores);
            $score['count'] = count($program_scores);
        } else {
            $score['score'] = 0;
            $score['count'] = 0;
        }
        $count = ORM::factory('Program')->where('parent_id', '=', $program->id)->where('activity', '=', 1)->find_all()->count();
        if ($this->user) {
            $current_user_program = ORM::factory('Program')->where('user_id', '=', $this->user->id)->where('activity', '=', 1)->find();
        }
        $exercises = $program->program_exercises->order_by('week_day', 'ASC')->find_all();
        foreach ($exercises as $exercise) {
            $sets = preg_replace('#,#', '/', $exercise->sets);
            $array[$exercise->week_day][$exercise->id] = $exercise->exercise->title . ', ' . $sets;
        }
        $this->page->title($program->title);
        $this->page->meta($program->text, 'description');
        $this->page->content(View::factory('privatecoach/program/view')
                ->set('i', 1)
                ->bind('current_user_program', $current_user_program)
                ->bind('days', $array)
                ->bind('program', $program)
                ->bind('count', $count)
                ->bind('score', $score)
        );
    }

    public function action_ajax_list()
    {
        $this->render = false;
        $view = View::factory('privatecoach/program/ajax_list')
            ->bind('current_program', $current_program)
            ->bind('model', $model)
            ->bind('array', $array)
            ->bind('count', $count)
            ->bind('pagination', $pagination);
        $values = $this->request->post();
        $q = $this->request->post('search');
        $array = array();
        $muscle_filter = array();
        $page = $this->request->param('page');
        $model = ORM::factory('Program')->where('type', '=', 'free');
        if ($this->user) {
            $current_program = ORM::factory('Program')->where('user_id', '=', $this->user->id)->where('activity', '=', 1)->find();
        }
        $programs = ORM::factory('Program')->where('type', '=', 'free')->find_all();
        foreach ($programs as $program) {
            $exercises = $program->program_exercises->order_by('week_day', 'ASC')->find_all()->as_array('week_day', 'program_id');
            $session[count($exercises)][] = $program->id;
        }
        if (array_key_exists('sessions', $values)) {
            $session[$values['sessions']][] = 0;
        }
        if ($values) {
            $mission = ORM::factory('Mission')->where('sef', '=', $values['goal'])->find();
            if ($mission->loaded()) {
                $model->where('mission_id', '=', $mission->id);
                if ($mission->sef == 'SLIMMING' && $this->user && $this->user->loaded()) {
                    $model->where('sex', '=', $this->user->sex);
                }
            }
            if ($values['place']) {
                $model->where('place', '=', $values['place']);
            }
            if ($values['sessions'] && $values['sessions'] != 0) {
                $model->where('id', 'in', $session[$values['sessions']]);
            }

            if ($q && $q != __('Поиск по тексту')) {
                $model->where_open()
                    ->where('text_ru', 'LIKE', "%$q%")
                    ->or_where('text_kz', 'LIKE', "%$q%")
                    ->or_where('text_en', 'LIKE', "%$q%")
                    ->or_where('title_kz', 'LIKE', "%$q%")
                    ->or_where('title_ru', 'LIKE', "%$q%")
                    ->or_where('title_en', 'LIKE', "%$q%")
                    ->where_close();
            }
            if ($values['muscle']) {
                $muscle_program = ORM::factory('Program')->where('type', '=', 'free');
                $muscle_program = $muscle_program->join('program_categories', 'left')->on('program_categories.program_id', '=', 'program.id');
                $muscle_program = $muscle_program->where('program_categories.category_id', '=', $values['muscle'])->find_all();
                foreach ($muscle_program as $filter) {
                    $muscle_filter[] = $filter->id;
                }
                $muscle_filter[] = 0;
                $model = $model->where('id', 'in', $muscle_filter);
            }
//            if ($values['sex']) {
//                $model->where('sex', '=', $values['sex']);
//            } else {
            switch ($this->user->sex) {
                case 1:
                    $filter_sex = 2;
                    break;
                case 2:
                    $filter_sex = 1;
                    break;
                default:
                    $filter_sex = null;
            }
            $model->where('sex', '!=', $filter_sex);
//            }

        }
        $pagination = $this->pagination($model, 'program_pagination.default');
        $model = $model->where('title_' . I18n::$lang, '!=', '')->where('text_' . I18n::$lang, '!=', '')->find_all();
        $count = $model->count();
        foreach ($model as $row) {
            $score = $row->program_scores->find_all()->as_array('id', 'score');
            if ($score) {
                $array[$row->id] = array_sum($score) / count($score);
            }
        }
        $this->response->body($view);

    }

    public function action_copy()
    {
        $this->render = false;
        $query = $this->request->query();
//        $parent_id = $this->request->post('parent_id');
        $parent_id = $this->request->param('id');
        $data = $this->program_as_array($parent_id);
        if ($this->user->pers_data) {
            if ($data) {
                $sets = array();
                $week_days = array();
                $program = ORM::factory('Program');
                $program->user_id = $this->user->id;
                $program->{'title_ru'} = $data['title_ru'];
                $program->{'title_en'} = $data['title_en'];
                $program->{'title_kz'} = $data['title_kz'];
//            $program->{'text_' . I18n::$lang} = $data['text'];
                $program->activity = 0;
                $program->type = 'copy';
                $program->sex = $this->user->sex;
                $program->mission = $data['mission'];
                $program->parent_id = $parent_id;
                $program->created = date('Y-m-d');
                $program->save();
                foreach ($data['days'] as $days) {
                    $week_days[] = $days['day'];
                    foreach ($days['exercises'] as $exercises) {
                        $pr_ex = ORM::factory('Program_Exercise');
                        $pr_ex->week_day = $days['day'];
                        $pr_ex->exercise_id = $exercises['id'];
                        $pr_ex->program_id = $program->id;
                        $pr_ex->index_number = $exercises['index'];
                        foreach ($exercises['efforts'] as $efforts) {
                            $sets[] = $efforts['repeat'];
                        }
                        $pr_ex->sets = implode(',', $sets);
                        $pr_ex->save();
                        unset($sets);
                    }
                }
//            if (count($week_days) == 0) {
//                //туповато, но а че делать если в программу не добавили дней с упраженинеиями?
//                $this->redirect('privatecoach/program/list');
//            }
                $program->start_date = date('Y-m-d');
                $program->duration = $data['duration'];
                $program->save();
                if ($query) {
                    $this->response->body(URL::site((I18n::$lang != 'kz' ? I18n::$lang : null) . '/privatecoach/program/edit/' . $program->id . '?mission=' . $query['mission'] . '&muscle=' . $query['muscle'] . '&place=' . $query['place'] . '&session=' . $query['session'] . '&q=' . $query['q']));
                } else {
                    $this->response->body(URL::site((I18n::$lang != 'kz' ? I18n::$lang : null) . '/privatecoach/program/edit/' . $program->id));
                }
            }
        } else {
            $this->response->body(URL::site((I18n::$lang != 'kz' ? I18n::$lang : null) . '/privatecoach/information/personal_data'));
        }
    }

    protected function program_as_array($id)
    {
        $model = ORM::factory('Program', $id);
        if (!$model->loaded()) {
            throw new HTTP_Exception_404();
        }
        $dt = new DateTime($model->start_date);
        $dt->add(new DateInterval('P' . ($model->duration * 7) . 'D'));

        $data = $model->as_array();

        $ext_data = array(
            'title_ru' => $model->title_ru,
            'title_kz' => $model->title_kz,
            'title_en' => $model->title_en,
            'text' => $model->text,
            'start_date' => Date::formatted_time($model->start_date, 'd.m.Y'),
            'end_date' => $dt->format('d.m.Y'),
            'duration' => $model->duration,
            'mission' => $model->mission,
            'days' => $model->days_as_array(),
            'activity' => $model->activity,
            'period' => array(
                'start' => Date::formatted_time($model->start_date, 'Ymd'),
                'end' => $dt->format('Ymd'),
            )
        );

        $data = array_merge($data, $ext_data);

        return $data;
    }

    public function action_admin()
    {
        if (array_search('admin', $this->user->roles->find_all()->as_array('id', 'name'))) {
            $model = ORM::factory('Program')
                ->where('type', '=', 'free');
            $pagination = $this->pagination($model);
            $model = $model->find_all();
            $this->page->content(View::factory('privatecoach/program/admin')
                    ->bind('model', $model)
                    ->bind('pagination', $pagination)
            );
        } else {
            $this->response->status(403);
            throw new HTTP_Exception_403("You don't have permission to access :uri", array(':uri' => $this->request->uri()));
        }
    }

    public function action_data()
    {
        $this->render = false;
        $id = $this->request->param('id');
        $model = ORM::factory('User_Log')->where('user_id', '=', $this->user->id)->where('program_id', '=', $id)->order_by('id', 'DESC')->find();
        $this->response->body(
            View::factory('privatecoach/program/data')
                ->bind('model', $model)
        );
    }

    public function action_delete()
    {
        $this->render = false;
        $model = ORM::factory('Program')->where('user_id', '=', $this->user->id)->where('activity', '=', 1)->find();
        $parent = $model->parent;
        if ($model->loaded()) {
            $model->activity = 2;
            $parent->subscribers--;
            $parent->save();
            $model->save();
        }
        $this->redirect(I18n::$lang . '/privatecoach/profile/' . $this->user->id);
    }


//    public function action_save_data(){
//        $this->redirect(URL::site('privatecoach/profile/'.$this->user->id),301);
//    }

    public function action_exec()
    {
//        exec("php ".DOCROOT."index.php --task=Program");
        Minion_Task::factory(array('Program'))->execute();
    }

    public function action_ajax_gallery()
    {
        $this->render = false;
        $view = View::factory('privatecoach/program/ajax_gallery')
            ->set('index', 1)
            ->bind('model', $model)
            ->bind('max_slides', $max_slides)
            ->bind('first_log', $first_log)
            ->bind('logs', $logs);
        $id = $this->request->param('id');
        $model = ORM::factory('Program', $id);
        if (!$model->loaded()) {
            throw new HTTP_Exception_404();
        }
        $logs = $model->user->logs->where('program_id', '=', $id)->find_all();
        foreach ($logs as $log) {
            $first_log = $log;
            break;
        }
        $max_slides = $logs->count();
        $this->response->body($view);
    }

    public function action_score()
    {
        $this->render = false;
        $score = $this->request->query('score');
        $target = $this->request->query('target');
        $program = ORM::factory('Program', $target);
        $model = ORM::factory('Program_Score')->where('user_id', '=', $this->user->id)->where('target_id', '=', $target)->find();
        if (!$model->loaded()) {
            $model->user_id = $this->user->id;
            $model->target_id = $target;
        }
        $model->score = $score;
        $model->save();
        $model = ORM::factory('Program_Score')->where('target_id', '=', $target)->find_all();
        foreach ($model as $row) {
            $scores[] = $row->score;
        }
        $total_score = array_sum($scores) / count($scores);
        $program->score = $total_score;
        $program->save();
    }

    public function action_pdf()
    {
        $this->render = false;
        $this->response->headers('Content-type', 'application/pdf');
        $id = $this->request->param('id');
        $program = ORM::factory('Program', $id);
        if (!$program->loaded()) {
            throw new HTTP_Exception_404();
        }
        $muscle_groups = $program->categories->find_all();
        $exercises = $program->program_exercises->order_by('week_day', 'ASC')->find_all();
        foreach ($exercises as $exercise) {
            $sets = preg_replace('#,#', '/', $exercise->sets);
            $days[$exercise->week_day][$exercise->exercise_id] = $exercise->exercise->title . ', ' . $sets;
        }

        $pdf = View_Mpdf_Core::factory('privatecoach/program/pdf_program')
            ->bind('muscle_groups', $muscle_groups)
            ->bind('days', $days)
            ->bind('program', $program);
        $this->response->body($pdf);
    }
}