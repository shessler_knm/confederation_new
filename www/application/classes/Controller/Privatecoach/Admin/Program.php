<?php
class Controller_Privatecoach_Admin_Program extends Site
{
    protected $need_auth = true;

    public function before()
    {
        parent::before();
        if (!array_search('admin', $this->user->roles->find_all()->as_array('id', 'name'))) {
            $this->response->status(403);
            throw new HTTP_Exception_403("You don't have permission to access :uri", array(':uri' => $this->request->uri()));
        }
    }

    public function action_edit()
    {
        $this->page->style('datepicker.css', 'js/lib/datepicker/css/datepicker.css');
        $this->page->script('angular', 'js/lib/angular.min.js');
        $this->page->script('bootstrap-datepicker.js', 'js/lib/datepicker/js/bootstrap-datepicker.js');
        $this->page->script('resource', 'js/lib/angular/angular-resource.min.js');
        $this->page->script('sanitize', 'js/lib/angular/angular-sanitize.js');
        $this->page->script('privatecoach', 'js/privatecoach.js');
        $this->page->script_dir('js/privatecoach');
        $this->page->content(
            View::factory('privatecoach/admin/edit')->bind('id', $id)
        );
        $id = $this->request->param('id');
        $model = ORM::factory('Program', $id);
        if (!$model->loaded()) {
            throw new HTTP_Exception_404();
        }

    }

}