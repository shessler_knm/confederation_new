<?php

/**
 * Created by PhpStorm.
 * User: id541rak
 * Date: 15.04.14
 * Time: 16:53
 */
class Controller_Privatecoach_Ingredient extends Privatecoach_Site
{

    public function action_source()
    {
        $this->render = false;
        $query = $this->request->query('query');
        $model = ORM::factory('Ingredient')
            ->having('title_ru', 'LIKE', "%$query%")
            ->find_all();
        $data = array();
        foreach ($model as $row) {
            $data[] = array('name' => $row->{'title_ru'}, 'id' => $row->id);
        }
        $this->response->body(json_encode($data));
    }

    public function action_results()
    {
        $this->render = false;
        $query = $this->request->query('q');
        $model = ORM::factory('Ingredient')->where('title_ru', 'Like', "%$query%")->find_all();
        $data = array();
        foreach ($model as $row) {
            $data[] = array('name' => $row->title);
        }
        $this->response->body(json_encode($data));
    }

    public function action_add_element()
    {
        $this->render = false;
        $query = $this->request->query('q');
        $model = ORM::factory('Ingredient', $query);
        if ($model->loaded()) {
            $this->response->body(View::factory('privatecoach/diet/add_element')->bind('model', $model));
        }
    }

//    public function action_import()
//    {
//        $this->render = false;
//        $filename = DOCROOT . 'ing.txt';
//        $handle = file($filename);
//        foreach ($handle as $row) {
//            $collection = explode(chr(9), $row);
//            $model = ORM::factory('Ingredient', $collection[0]);
//            if ($model->loaded()) {
//                $model->title_en = $collection[1];
//                $model->description_en = $collection[2];
//                $model->save();
//            }
//        }
//    }
} 