<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Almas
 * Date: 22.07.13
 * Time: 15:35
 * To change this template use File | Settings | File Templates.
 */
class Controller_Privatecoach_Subscribe extends Privatecoach_Site
{
    public function before()
    {
        $this->need_auth = true;
        parent::before();
    }

    public function scripts()
    {
        return parent::scripts() + array(
            'subscribe' => 'js/subscribe.js'
        );
    }

    public function action_list()
    {
        $id = $this->request->param('id');
        if ($id) {
            $user = ORM::factory('User', $id);
        } else {
            $user = $this->user;
        }
        $subs = $user->subscribers->where('status', '=', 1)->find_all();
        $followers = $user->following->where('status', '=', 1)->find_all();
        $requests = $user->following->where('status', '=', 0)->find_all();
        $requests_count = $requests->count();
        $this->page->content(View::factory('privatecoach/subscribe/list')
                ->bind('subs', $subs)
                ->bind('requests_count', $requests_count)
                ->bind('requests', $requests)
                ->bind('followers', $followers)
        );
    }

    public function action_accept()
    {
        $this->render = false;
        $id = $this->request->query('id');
        $model = ORM::factory('Subscribe')
            ->where('user_id', '=', $id)
            ->where('target_id', '=', $this->user->id)
            ->find();
        if ($model->loaded()) {
            $model->status = 1;
            $model->save();
            $subscriber = ORM::factory('User', $model->user_id);
            $this->response->body(View::factory('privatecoach/subscribe/new_subscriber')->bind('model', $subscriber));
        }
    }

    public function action_delete()
    {
        $this->render = false;
        $target = $this->request->query('id');
        $type = $this->request->query('type');
        if ($type) { //удаление подписчика
            $user = ORM::factory('User', array('id' => $target));
            if ($user->has('subscribes', $this->user->id)) {
                $user->remove('subscribes', $this->user->id);
                Feed::delete_event('new_sub', $this->user->id, $target);
            }
        } else { //удаление подписки
            if ($this->user->has('subscribes', $target)) {
                $this->user->remove('subscribes', $target);
                Feed::delete_event('new_sub', $target, $this->user->id);
            }
        }
        $this->response->body('success');
    }

    public function action_add()
    {
        $this->render = false;
        $q = $this->request->query('id');
        if (!$this->user->has('subscribes_second', $q)) {
            $model = ORM::factory('Subscribe');
            $model->user_id = $this->user->id;
            $model->target_id = $q;
            $model->save();
            Feed::new_sub($this->user, $q);
        }
        $this->response->body('success');
    }

    public function action_subs()
    {
        $this->render = false;
        $view = View::factory('privatecoach/subscribe/subs')
            ->bind('counter', $counter)
            ->bind('more', $more)
            ->bind('id', $id)
            ->bind('exists', $exists)
            ->bind('subs', $subscribers);

        $id = $this->request->param('id');
        $exists = true;
        $subscribers = ORM::factory('Subscribe')->where('user_id', '=', $id)->where('status', '=', 1);
        $counter = $subscribers->reset(false)->count_all();
        $subscribers = $subscribers->limit(10)->find_all();
        if ($counter > 10) {
            $more = true;
        } elseif ($counter == 0) {
            $exists = false;
        }
        $this->response->body($view);
    }

    public function action_subers()
    {
        $this->render = false;
        $view = View::factory('privatecoach/subscribe/subers')
            ->bind('counter', $counter)
            ->bind('id', $id)
            ->bind('followers', $followers)
            ->bind('exists', $exists)
            ->bind('more', $more);
        $id = $this->request->param('id');
        $exists = true;
        $followers = ORM::factory('Subscribe')->where('target_id', '=', $id)->where('status', '=', 1);
        $counter = $followers->reset(false)->count_all();
        $followers = $followers->limit(10)->find_all();
        if ($counter > 10) {
            $more = true;
        } elseif ($counter == 0) {
            $exists = false;
        }
        $this->response->body($view);
    }
}
