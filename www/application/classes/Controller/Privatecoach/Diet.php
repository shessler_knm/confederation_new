<?php

/**
 * Created by PhpStorm.
 * User: id541rak
 * Date: 04.04.14
 * Time: 9:46
 */
class Controller_Privatecoach_Diet extends Privatecoach_Site
{

    public function before()
    {
        if (preg_match('#.*my.*#', $this->request->action())) {
            $this->need_auth = true;
        }
        parent::before();
    }

    public function scripts()
    {
        return parent::scripts() + array(
            'diet' => "/js/diet.js",
            'jquery.form.js' => "/js/jquery.form.js",
        );
    }

    public function action_index()
    {
        $this->page->script('moment.js', 'js/moment.js');
        $this->page->script('b-datetimepicker.js', 'js/lib/bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js');
        $this->page->style('datepicker.css', 'js/lib/bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.css');
        $view = View::factory('privatecoach/diet/index')->bind('query', $query);
        $query = $this->request->query();
        if ($query) {
            if (array_key_exists('id', $query)) {
                $model = ORM::factory('Dish', $query['id']);
                if ($model->loaded()) {
                    $model->delete();
                }
            }
        }
        $this->page->content($view);
    }

    public function action_add_to_my_diet()
    {
        $this->render = false;
        $post = $this->request->post();
        if ($post) {
            $datetime = Date::formatted_time(Arr::get($post, 'date') . ' ' . Arr::get($post, 'time'), 'Y-m-d H:i:s');
            $ration = ORM::factory('Ration')->where('user_id', '=', $this->user->id)->where('datetime', '=', $datetime)->find();
            if (!$ration->loaded()) {
                $ration->datetime = $datetime;
                $ration->user_id = $this->user->id;
                $ration->save();
            }
            $dish_ration = ORM::factory('Dish_Ration');
            $dish_ration->ration_id = $ration->id;
            $dish_ration->weight = Arr::get($post, 'weight');
            $dish_ration->dish_id = Arr::get($post, 'id');
            $dish_ration->save();
        }
        $this->response->body('success');
    }

    public function action_ajax_list()
    {
        $this->render = false;
//        $this->page->script('bootstrap','js/lib/bootstrap/js/bootstrap.min.js');
        $view = View::factory('privatecoach/diet/ajax_list')
            ->bind('count', $count)
            ->bind('data', $data)
            ->bind('model', $collection)
            ->bind('ingredient', $ingredient)
            ->bind('categories', $categories)
            ->bind('pagination', $pagination);
        $values = $this->request->post();
        $q = Arr::get($values, 'search');
        $model = ORM::factory('Dish');
        $ingredient = ORM::factory('Ingredient');
        $categories = ORM::factory('Category')->get_sef_dishes()->find_all();
        if ($values) {
            if ($q && $q != __('Поиск продукта')) {
                $model->where_open()
                    ->where('description_ru', 'LIKE', "%$q%")
                    ->or_where('description_kz', 'LIKE', "%$q%")
                    ->or_where('description_en', 'LIKE', "%$q%")
                    ->or_where('title_kz', 'LIKE', "%$q%")
                    ->or_where('title_ru', 'LIKE', "%$q%")
                    ->or_where('title_en', 'LIKE', "%$q%")
                    ->where_close();
                $ingredient->where_open()
                    ->where('description_ru', 'LIKE', "%$q%")
                    ->or_where('description_kz', 'LIKE', "%$q%")
                    ->or_where('description_en', 'LIKE', "%$q%")
                    ->or_where('title_kz', 'LIKE', "%$q%")
                    ->or_where('title_ru', 'LIKE', "%$q%")
                    ->or_where('title_en', 'LIKE', "%$q%")
                    ->where_close();
            }
            $model->where('calories', '>=', Arr::get($values, 'min_cal', 0))
                ->where('calories', '<=', Arr::get($values, 'max_cal', 900))
                ->where('protein', '>=', Arr::get($values, 'min_prot', 0))
                ->where('protein', '<=', Arr::get($values, 'max_prot', 50))
                ->where('carbohydrate', '>=', Arr::get($values, 'min_carb', 0))
                ->where('carbohydrate', '<=', Arr::get($values, 'max_carb', 100))
                ->where('fat', '>=', Arr::get($values, 'min_fat', 0))
                ->where('fat', '<=', Arr::get($values, 'max_fat', 100)
                );
            $ingredient->where('calories', '>=', Arr::get($values, 'min_cal', 0))
                ->where('calories', '<=', Arr::get($values, 'max_cal', 900))
                ->where('protein', '>=', Arr::get($values, 'min_prot', 0))
                ->where('protein', '<=', Arr::get($values, 'max_prot', 50))
                ->where('carbohydrate', '>=', Arr::get($values, 'min_carb', 0))
                ->where('carbohydrate', '<=', Arr::get($values, 'max_carb', 100))
                ->where('fat', '>=', Arr::get($values, 'min_fat', 0))
                ->where('fat', '<=', Arr::get($values, 'max_fat', 100)
                );
        }
        $model = $model->where('is_published', '=', 1)->find_all()->as_array();
        $ingredient = $ingredient->where('is_published', '=', 1)->find_all()->as_array();
        $collection = array_merge($model, $ingredient);
        foreach ($model as $row) {
            $data[$row->category_id][] = $row;
        }
        $count = count($collection);
        $this->response->body($view);
    }

    public function action_my_diet()
    {
        $this->page->script('bootstrap-datepicker.js', 'js/lib/datepicker/js/bootstrap-datepicker.js');
        $this->page->script('bootstrap-datetimepicker.js', 'js/lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js');
        $this->page->script('bootstrap-datepicker.ru.js', 'js/lib/datepicker/js/locales/bootstrap-datepicker.ru.js');
        $this->page->script('bootstrap-datepicker.kk.js', 'js/lib/datepicker/js/locales/bootstrap-datepicker.kk.js');
        $this->page->style('datepicker.css', 'js/lib/datepicker/css/datepicker.css');
        $this->page->style('datetimepicker.css', 'js/lib/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css');
        $view = View::factory('privatecoach/diet/my_diet')
            ->bind('bmr', $bmr)
            ->bind('norms', $norms)
            ->bind('edited', $edited)
            ->bind('mission', $mission);

        $norms = array();
        $levels = array(
            __('Минимальный уровень'),
            __('Низкий уровень'),
            __('Средний уровень'),
            __('Высокий уровень'),
            __('Очень высокий уровень')
        );
        $norm_model = ORM::factory('User_Norm')->where('date', '=', date('Y-m-d'))->where('user_id', '=', $this->user->id)->find();
        if ($norm_model->loaded()) {
            $norms['calories'] = $norm_model->calories;
            $norms['protein'] = $norm_model->protein;
            $norms['carbohydrate'] = $norm_model->carbohydrate;
            $norms['fat'] = $norm_model->fat;
            $norms['level'] = Arr::get($levels, $norm_model->level, null);
        }
        $edited = ORM::factory('Ration')
            ->where('user_id', '=', $this->user->id)
            ->where(DB::expr("DATE_FORMAT(datetime,'%Y-%m-%d')"), '=', date('Y-m-d'))
            ->find()
            ->loaded();
        $mission = $this->user->mission_logs->order_by('date_begin', 'DESC')->order_by('id', 'DESC')->find()->type == 'MUSCLE' ? 1 : 2;
        if ($this->user->sex == 1) {
            $bmr = 88.36 + (13.4 * $this->user->weight) + (4.8 * $this->user->height) - (5.7 * $this->user->get_age());
        } else {
            $bmr = 447.6 + (9.2 * $this->user->weight) + (3.1 * $this->user->height) - (4.3 * $this->user->get_age());
        }
        $this->page->content($view);
    }

    public function action_get_norms()
    {
        $this->render = false;
        $date = $this->request->query('date');
        $norms = array();
        $levels = array(
            __('Минимальный уровень'),
            __('Низкий уровень'),
            __('Средний уровень'),
            __('Высокий уровень'),
            __('Очень высокий уровень')
        );
        $norm_model = ORM::factory('User_Norm')->where('user_id', '=', $this->user->id)->where('date', '=', $date)->find();
        if ($norm_model->loaded()) {
            $norms['calories'] = $norm_model->calories;
            $norms['protein'] = $norm_model->protein;
            $norms['carbohydrate'] = $norm_model->carbohydrate;
            $norms['fat'] = $norm_model->fat;
            $norms['level'] = Arr::get($levels, $norm_model->level, null);
        } else {
            $norms['calories'] = 0;
            $norms['protein'] = 0;
            $norms['carbohydrate'] = 0;
            $norms['fat'] = 0;
            $norms['level'] = __('Уровень физической нагрузки');
        }
        $this->response->body(json_encode($norms));
    }

    public function action_my_diet_ajax()
    {
        $this->render = false;
        $view = View::factory('privatecoach/diet/my_diet_ajax')
            ->bind('times', $times)
            ->bind('data', $data);

        $data = array();
        $times = array();

        $date = $this->request->query('date');
        $rations = ORM::factory('Ration')
            ->where('user_id', '=', $this->user->id)
            ->where(DB::expr("DATE_FORMAT(datetime,'%Y-%m-%d')"), '=', $date)
            ->find_all();
        foreach ($rations as $ration) {
            $dish_rations = $ration->dish_rations->find_all();
            $data[$ration->id] = array();
            $times[$ration->id] = Date::formatted_time($ration->datetime, 'H:i');
            foreach ($dish_rations as $row) {
                $dish = $row->dish;
                $data[$ration->id][] = array(
                    'id' => $row->id,
                    'title' => $dish->title,
                    'calories' => round(($dish->calories * $row->weight) / 100, 2),
                    'protein' => round(($dish->protein * $row->weight) / 100, 2),
                    'carbohydrate' => round(($dish->carbohydrate * $row->weight) / 100, 2),
                    'fat' => round(($dish->fat * $row->weight) / 100, 2),
                    'weight' => $row->weight,
                );
            }
        }
        $this->response->body($view);
    }

    public function action_add_ingestion()
    {
        $this->render = false;
        $view = View::factory('privatecoach/diet/add_ingestion')
            ->bind('ration', $ration);
        $date = $this->request->query('date');
        $ration = ORM::factory('Ration');
        $ration->datetime = $date . ' 00:00:00';
        $ration->user_id = $this->user->id;
        $ration->save();
        $this->response->body($view);
    }

    public function action_user_norms()
    {
        $this->render = false;
        $post = $this->request->post();
        $validate = Validation::factory($post);
        $validate
            ->rules('date', array(
                array('date'),
                array('not_empty'),
            ))
            ->rules('calories', array(
                array('not_empty'),
                array('numeric'),
            ))
            ->rules('protein', array(
                array('not_empty'),
                array('numeric'),
            ))
            ->rules('carbohydrate', array(
                array('not_empty'),
                array('numeric'),
            ))
            ->rules('fat', array(
                array('not_empty'),
                array('numeric'),
            ));
        if ($validate->check()) {
            $model = ORM::factory('User_Norm')->where('user_id', '=', $this->user->id)->where('date', '=', Arr::get($post, 'date'))->find();
            $model->user_id = $this->user->id;
            $model->values($post);
            $model->save();
        }
    }

    public function action_change_time()
    {
        $this->render = false;
        $id = $this->request->query('id');
        $time = $this->request->query('time');
        $ration = ORM::factory('Ration', $id);
        if (!$ration->loaded()) {
            $ration->user_id = $this->user->id;
        }
        $ration->datetime = Date::formatted_time($ration->datetime, 'Y-m-d') . ' ' . $time[0];
        $ration->save();

    }

    public function action_change_weight()
    {
        $this->render = false;
        $weight = $this->request->query('weight');
        $id = $this->request->query('id');
        $model = ORM::factory('Dish_Ration', $id);
        $model->weight = $weight;
        $model->save();
        $dish = $model->dish;
        $data = array(
            'calories' => ($dish->calories * $weight) / 100,
            'protein' => ($dish->protein * $weight) / 100,
            'carbohydrate' => ($dish->carbohydrate * $weight) / 100,
            'fat' => ($dish->fat * $weight) / 100
        );
        $this->response->body(json_encode($data));
    }

    public function action_source()
    {
        $this->render = false;
        $query = $this->request->query('query');
        $model = ORM::factory('Dish')
            ->having('title_' . I18n::$lang, 'LIKE', "%$query%")
            ->find_all();
        $data = array();
        foreach ($model as $row) {
            $data[] = array(
                'name' => $row->{'title_' . I18n::$lang},
                'id' => $row->id,
                'calories' => $row->calories,
                'protein' => $row->protein,
                'carbohydrate' => $row->carbohydrate,
                'fat' => $row->fat,
            );
        }
        $this->response->body(json_encode($data));
    }

    public function action_add_dish()
    {
        $this->render = false;
        $view = View::factory('privatecoach/diet/add_dish')
            ->bind('dish_ration', $dish_ration)
            ->bind('data', $data);
        $data = array();
        $post = $this->request->post();
        if ($post) {
            $dish_ration = ORM::factory('Dish_Ration');
            $ration = ORM::factory('Ration', Arr::get($post, 'id'));
            if (!$ration->loaded()) {
                $ration->user_id = $this->user->id;
                $ration->datetime = Date::formatted_time('now', 'Y-m-d H:i:s');
                $ration->save();
            }
            $dish_ration->dish_id = Arr::get($post, 'dish');
            $dish_ration->ration_id = $ration->id;
            $dish_ration->weight = 0;
            $dish_ration->save();
            if ($dish_ration->loaded()) {
                $data['calories'] = round(($dish_ration->dish->calories * $dish_ration->weight) / 100, 2);
                $data['protein'] = round(($dish_ration->dish->protein * $dish_ration->weight) / 100, 2);
                $data['carbohydrate'] = round(($dish_ration->dish->carbohydrate * $dish_ration->weight) / 100, 2);
                $data['fat'] = round(($dish_ration->dish->fat * $dish_ration->weight) / 100, 2);
            }
        }
        $this->response->body($view);
    }

    public function action_remove_data()
    {
        $this->render = false;
        $id = $this->request->query('id');
        $model = $this->request->query('model');
        $model = ORM::factory($model, $id);
        if ($model->loaded()) {
            $model->delete();
            $this->response->body('success');
        }
    }

//    public function action_remove_ration()
//    {
//        $this->render = false;
//        $id = $this->request->query('id');
//        $model = ORM::factory('Ration', $id);
//        if ($model->loaded()) {
//            $model->delete();
//            $this->response->body('success');
//        }
//    }

    public function action_load_photo()
    {
        $this->render = false;
        if ($_FILES) {
            $file_validate = Validation::factory($_FILES);
            $file_validate->rules('image', array(
                array("Upload::not_empty"),
                array("Upload::valid"),
                array("Upload::type", array(":value", array("png", "jpg", "gif", "jpeg"))),
                array("Upload::size", array(":value", "2M")),
                array('Upload::image')
            ));
            if ($file_validate->check()) {
                $file = Cms_Storage::instance()->upload($_FILES, 'image');
                $data['file'] = $file->id;
                $data['src'] = $file->url();
                $this->response->body(json_encode($data));
            } else {
                $this->response->body('error');
            }
        }
    }

    public function check_calories($value)
    {
        return $value <= 900;
    }

    public function action_fast_search()
    {
        $this->render = false;
        $query = $this->request->query('category');
        if ($query == 'ingredients') {
            $collection = ORM::factory('Ingredient')->where('is_published', '=', 1)->find_all();
            $category_name = __('Ингредиенты');
        } else {
            $category = ORM::factory('Category')->where('slug', '=', $query)->find();
            $category_name = $category->title;
            if ($category->loaded()) {
                $collection = ORM::factory('Dish')->where('category_id', '=', $category->id)->where('is_published', '=', 1)->find_all();
            }
        }
        $count = $collection->count();
        $this->response->body(View::factory('privatecoach/diet/fast_search')
                ->bind('category_name', $category_name)
                ->bind('count', $count)
                ->bind('collection', $collection)
        );
    }

    public function action_create_ingredient()
    {
        $post = $this->request->post();
        $storage = ORM::factory('Storage', Arr::get($post, 'image_file'));
        $validate = Validation::factory($post);
        $validate
            ->rules('title', array(
                array('not_empty'),
                array('min_length', array(':value', 2)),

            ))->rules('description', array(
                array('not_empty'),
                array('max_length', array(':value', 700)),

            ))->rules('image_file', array(
                array('not_empty'),
            ))
            ->rules('protein', array(
                array('not_empty'),
                array('numeric')
            ))
            ->rules('carbohydrate', array(
                array('not_empty'),
                array('numeric')
            ))
            ->rules('fat', array(
                array('not_empty'),
                array('numeric')
            ))
            ->rules('calories', array(
                array('not_empty'),
                array(array($this, 'check_calories'), array(':value')),
                array('numeric')
            ));
        $nutrients = Arr::get($post, 'protein') + Arr::get($post, 'carbohydrate') + Arr::get($post, 'fat');
        if ($post) {
            if ($validate->check()) {
                if ($nutrients <= 100) {
                    $model = ORM::factory('Ingredient');
                    $model->{'title_' . I18n::$lang} = Arr::get($post, 'title');
                    $model->{'description_' . I18n::$lang} = Arr::get($post, 'description');
                    $model->photo = Arr::get($post, 'image_file');
                    $model->protein = Arr::get($post, 'protein');
                    $model->carbohydrate = Arr::get($post, 'carbohydrate');
                    $model->fat = Arr::get($post, 'fat');
                    $model->calories = Arr::get($post, 'calories');
                    $model->save();
                } else {
                    $error['nutrients'] = __('Сумма нутриентов превышает 100г');
                }
            } else {
                $error = $validate->errors(0);
                if ($nutrients > 100) {
                    $error['nutrients'] = __('Сумма нутриентов превышает 100г');
                }
            }
        }
        $view = View::factory('privatecoach/diet/create_ingredient')
            ->bind('storage', $storage)
            ->bind('post', $post)
            ->bind('error', $error);
        $this->page->content($view);
    }

    public function action_create_recipe()
    {
        $post = $this->request->post();
        $ing_ids = array();
        $weights = array();
        $error = array();
        $dish_ingredient = null;
        $storage = ORM::factory('Storage', Arr::get($post, 'image_file'));
        foreach ($post as $key => $row) {
            if (strstr($key, 'ing_id')) {
                preg_match('#ing_id_(.*)#', $key, $match);
                if ($match) {
                    $ing_ids[] = $match[1];
                    unset($match);
                }
            } elseif (strstr($key, 'weight')) {
                preg_match('#weight_(.*)#', $key, $match);
                if ($match) {
                    $weights[$match[1]] = $row;
                    unset($match);
                }
            }
        }
        $validate = Validation::factory($post);
        $weight_validate = Validation::factory($weights);
        foreach ($weight_validate as $value) {
            $weight_validate->rules($value, array(
                array('not_empty'),
                array('numeric')
            ));
        }
        $validate
            ->rules('title', array(
                array('not_empty'),
                array('min_length', array(':value', 2)),

            ))->rules('description', array(
                array('not_empty'),
                array('max_length', array(':value', 700)),

            ))->rules('image_file', array(
                array('not_empty'),
            ));
        if ($post) {
            if ($validate->check() && $ing_ids && $weight_validate->check()) {
                $model = ORM::factory('Dish');
                $model->{'title_' . I18n::$lang} = Arr::get($post, 'title');
                $model->{'description_' . I18n::$lang} = Arr::get($post, 'description');
                $model->photo = Arr::get($post, 'image_file');
                $model->category_id = Arr::get($post, 'category');
                if ($model->save($validate)) {
                    $ingredients = ORM::factory('Ingredient')->where('id', 'in', $ing_ids)->find_all();
                    foreach ($ingredients as $ingredient) {
                        $dish_ingredient = ORM::factory('Dish_Ingredient');
                        $dish_ingredient->dish_id = $model->id;
                        $dish_ingredient->ingredient_id = $ingredient->id;
                        $dish_ingredient->weight = $weights[$ingredient->id];
                        $dish_ingredient->save();
                    }
                    $model->protein = $model->caloric_content(null, $model, 'protein');
                    $model->carbohydrate = $model->caloric_content(null, $model, 'carbohydrate');
                    $model->fat = $model->caloric_content(null, $model, 'fat');
                    $model->calories = $model->caloric_content(null, $model, 'calories');
                    $model->save();
                }
            } else {
                $error['weight'] = $weight_validate->errors(0);
                $error['form'] = $validate->errors(0);
                $error['ings'] = array('ings' => __('Отсутствуют ингредиенты'));
                $error = array_merge($error['weight'], $error['form'], $error['ings']);
            }
        }
        $categories = ORM::factory('Category')->get_sef_dishes()->find_all()->as_array('id', 'title');
        $view = View::factory('privatecoach/diet/create_recipe')
            ->bind('dish_ingredient', $dish_ingredient)
            ->bind('storage', $storage)
            ->bind('post', $post)
            ->bind('error', $error)
            ->bind('categories', $categories);
        $this->page->content($view);
    }

    public function action_ajax_dish()
    {
        $this->render = false;
        $id = $this->request->param('id');
        $query = $this->request->query('category');
        if (strstr($query, 'Model')) {
            preg_match('#Model_(.*)#', $query, $class_name);
            $query = $class_name[1];
        }
        $model = ORM::factory($query, $id);
        $view = View::factory('privatecoach/diet/ajax_dish')
            ->bind('model', $model);
        $this->response->body($view);
    }

    public function action_import()
    {
        $this->render = false;
        $filename = DOCROOT . 'dishes.txt';
        $handle = file($filename);
        foreach ($handle as $row) {
            $collection = explode(chr(9), $row);
            $model = ORM::factory('Dish', $collection[0]);
            if ($model->loaded()) {
                $model->title_en = trim($collection[1]);
                $model->description_en = trim($collection[2]);
                $model->save();
            }
        }
    }
} 