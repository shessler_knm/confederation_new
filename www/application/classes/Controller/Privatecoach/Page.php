<?php
class Controller_Privatecoach_Page extends Site
{
    public function scripts(){
        return parent::scripts()+array(
            'jquery.fancybox'=> "/js/fancybox/jquery.fancybox.js",
            'jquery.fancybox.pack'=>"/js/fancybox/jquery.fancybox.pack.js",
            'jquery.fancybox-buttons'=>"/js/fancybox/jquery.fancybox-buttons.js",
            'jquery.fancybox-thumbs'=>"/js/fancybox/jquery.fancybox-thumbs.js",
            'jquery.fancybox-media'=>"/js/fancybox/jquery.fancybox-media.js",
            'page_fancybox' => 'js/page_fancybox.js',
            "contacts" => "http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU",
            'map' => 'js/contacts.js',
        );
    }

    public function action_index(){
        $view = View::factory('privatecoach/page/view')
            ->bind('model',$model);
        $id = $this->request->param('id');
        $model=ORM::factory('Page',array('sef'=>$id));
        if (!$model->loaded()){
            throw new HTTP_Exception_404();
        }
        $this->page->title($model->title);
        $this->page->meta($model->text,'description');
        $this->page->content($view);
    }


}
