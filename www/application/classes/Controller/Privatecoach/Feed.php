<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Almas
 * Date: 17.07.13
 * Time: 10:50
 * To change this template use File | Settings | File Templates.
 */
class Controller_Privatecoach_Feed extends Privatecoach_Site
{

    public function scripts()
    {
        return parent::scripts() + array(
            'feed' => 'js/feed.js',
            'share' => 'http://share.pluso.ru/pluso-like.js',
        );
    }

    public function before()
    {
        $this->need_auth = true;
        parent::before();
    }

    public function action_main_preview()
    {
        $this->render = false;
        $feeds = ORM::factory('Feed');
        $array = array();
        $id = $this->user->id;
        $subscribers = ORM::factory('Subscribe')->where('user_id', '=', $id)->find_all()->as_array(null, 'target_id');
        $subscribers[] = $id;
        $albums = ORM::factory('User_Album')->where('user_id', 'in', $subscribers)->find_all();
        $keys = $albums->as_array(null, 'id');
        foreach ($albums as $album) {
            $array[$album->id] = array(
                'photo' => array(),
                'number' => 0,
            );
            foreach ($album->photos->find_all() as $photo) {
                $array[$album->id]['photo'][$photo->id] = $photo->photo_s->id;
                $array[$album->id]['number'] = count($array[$album->id]['photo']);
            }
        }
        $common_types = array('close_mission', 'new_achievement');
        $rules = $this->user->feed_rules->find_all()->as_array('id', 'feed_id');
        $rules[0] = 0;
        $keys[] = 0;
        $feeds = $feeds
            //События юзеров, на которых я подписан, не относящиеся к альбомам или к персональным типам событий
            ->and_where_open()
            ->where('user_id', 'in', $subscribers)
            ->and_where_open()

            ->and_where('type', 'in', $common_types)
            ->and_where('user_id', 'in', $subscribers)

            ->or_where('type', '=', 'new_sub')
            ->and_where('user_id', '=', $id)

            ->and_where_close()
            ->and_where_close()

            //События юзеров, на которых я подписан, не относящиеся к альбомам или к персональным типам событий
            ->or_where_open()
            ->or_where('user_id', 'in', $subscribers)
            ->and_where('type', '=', 'update_album')
            ->and_where('value_1', 'in', $keys)
            ->or_where_close()

            ->where('id', 'not in', $rules)
            ->order_by('id', 'DESC')
            ->limit(10)
            ->find_all();
        $check = $feeds->count() > 0 ? true : false;
        $this->response->body(View::factory('privatecoach/feed/user_preview')
                ->bind('array', $array)
                ->bind('check', $check)
                ->bind('feeds', $feeds)
                ->bind('id', $id)
        );
    }

    public function action_preview()
    {
        $this->render = false;
        $id = $this->request->param('id');
        $feeds = ORM::factory('Feed');
        $array = array();
        $rules = $this->user->feed_rules->find_all()->as_array('id', 'feed_id');
        $rules[0] = 0;
        $common_types = array('close_mission', 'new_achievement');
        if ($this->user->id == $id) {
            $subscribers = ORM::factory('Subscribe')->where('user_id', '=', $id)->find_all()->as_array(null, 'target_id');
            $subscribers[] = $id;
            $albums = ORM::factory('User_Album')->where('user_id', 'in', $subscribers)->find_all();
            $keys = $albums->as_array(null, 'id');
            $keys[] = 0;
            foreach ($albums as $album) {
                $array[$album->id] = array(
                    'photo' => array(),
                    'number' => 0,
                );
                foreach ($album->photos->find_all() as $photo) {
                    $array[$album->id]['photo'][$photo->id] = $photo->photo_s->id;
                    $array[$album->id]['number'] = count($array[$album->id]['photo']);
                }
            }
            $feeds = $feeds
                //События юзеров, на которых я подписан, не относящиеся к альбомам или к персональным типам событий
                ->and_where_open()
                ->where('user_id', 'in', $subscribers)
                ->and_where_open()

                ->and_where('type', 'in', $common_types)
                ->and_where('user_id', 'in', $subscribers)

                ->or_where('type', '=', 'new_sub')
                ->and_where('user_id', '=', $id)

                ->and_where_close()
                ->and_where_close()

                //События юзеров, на которых я подписан(+я сам), относящиеся к альбомам
                ->or_where_open()
                ->or_where('user_id', 'in', $subscribers)
                ->and_where('type', '=', 'update_album')
                ->and_where('value_1', 'in', $keys)
                ->or_where_close()

                ->where('id', 'not in', $rules)
                ->order_by('id', 'DESC')
                ->limit(10)
                ->find_all();

            $albums = ORM::factory('User_Album')->where('user_id', 'in', $subscribers)->find_all();
            foreach ($albums as $album) {
                $array[$album->id] = array(
                    'photo' => array(),
                    'number' => 0,
                );
                foreach ($album->photos->find_all() as $photo) {
                    $array[$album->id]['photo'][$photo->id] = $photo->photo_s->id;
                    $array[$album->id]['number'] = count($array[$album->id]['photo']);
                }
            }
        } else {
            $albums = ORM::factory('User_Album')->where('user_id', '=', $id)->find_all();
            $keys = $albums->as_array(null, 'id');
            $keys[] = 0;
            foreach ($albums as $album) {
                $array[$album->id] = array(
                    'photo' => array(),
                    'number' => 0,
                );
                foreach ($album->photos->find_all() as $photo) {
                    $array[$album->id]['photo'][$photo->id] = $photo->photo_s->id;
                    $array[$album->id]['number'] = count($array[$album->id]['photo']);
                }
            }
            $feeds = $feeds
                //События юзера. Пока только достижения
                ->and_where_open()
                ->where('user_id', '=', $id)
                ->and_where('type', 'in', $common_types)
                ->and_where_close()

                //События юзера. Альбомы
                ->or_where_open()
                ->or_where('user_id', '=', $id)
                ->and_where('type', '=', 'update_album')
                ->and_where('value_1', 'in', $keys)
                ->or_where_close()

                ->order_by('id', 'DESC');
            $amount = $feeds->reset(false)->find_all()->count();
            $feeds = $feeds->limit(10)->find_all();
        }
        $check = $feeds->count() > 0 ? true : false;
        $this->response->body(View::factory('privatecoach/feed/user_preview')
                ->bind('array', $array)
                ->bind('check', $check)
                ->bind('feeds', $feeds)
                ->bind('id', $id)
        );
    }

    public function action_list()
    {
        Register::instance()->activemenu = 51;
        $id = $this->request->param('id');
        if (!$id) {
            $id = $this->user->id;
        }
        $common_types = array('close_mission', 'new_achievement','new_fat');
        $model = ORM::factory('Feed');
        $full_albums = array();
        if ($this->user->id == $id) {
            $subscribers = ORM::factory('Subscribe')->where('user_id', '=', $id)->find_all()->as_array(null, 'target_id');
            $subscribers[] = $id;
            $rules = $this->user->feed_rules->find_all()->as_array('id', 'feed_id');
            $rules[0] = 0;
            $albums = ORM::factory('User_Album')->where('user_id', 'in', $subscribers)->find_all();
            $keys = $albums->as_array(null, 'id');
            $keys[] = 0;
            foreach ($albums as $album) {
                $array[$album->id] = array(
                    'photo' => array(),
                    'number' => 0,
                );
                foreach ($album->photos->find_all() as $photo) {
                    $array[$album->id]['photo'][$photo->id] = $photo->photo_s->id;
                    $array[$album->id]['number'] = count($array[$album->id]['photo']);
                }
                if (count($array[$album->id]['photo'])){
                    $full_albums[]=$album->id;
                }
            }
            $feeds = $model
                //События юзеров, на которых я подписан, не относящиеся к альбомам или к персональным типам событий
                ->and_where_open()
                ->where('user_id', 'in', $subscribers)
                ->and_where_open()

                ->and_where('type', 'in', $common_types)
                ->and_where('user_id', 'in', $subscribers)

                ->or_where('type', '=', 'new_sub')
                ->and_where('user_id', '=', $id)

                ->and_where_close()
                ->and_where_close()

                //События юзеров, на которых я подписан(+я сам), относящиеся к альбомам
                ->or_where_open()
                ->or_where('user_id', 'in', $subscribers)
                ->and_where('type', '=', 'update_album')
                ->and_where('value_1', 'in', $keys)
//                ->and_where('value_1', 'in', $full_albums)
                ->or_where_close()

                ->where('id', 'not in', $rules)
                ->order_by('id', 'DESC');

            $amount = $feeds->reset(false)->find_all()->count();
            $feeds = $feeds->limit(10)->find_all();
        } else {
            $albums = ORM::factory('User_Album')->where('user_id', '=', $id)->find_all();
            $keys = $albums->as_array(null, 'id');
            $keys[] = 0;
            foreach ($albums as $album) {
                $full_albums[] = $album->id;
                $array[$album->id] = array(
                    'photo' => array(),
                    'number' => 0,
                );
                foreach ($album->photos->find_all() as $photo) {
                    $array[$album->id]['photo'][$photo->id] = $photo->photo_s->id;
                    $array[$album->id]['number'] = count($array[$album->id]['photo']);
                }
            }
            $feeds = $model
                ->and_where_open()
                ->where('user_id', '=', $id)
                ->and_where('type', 'in', $common_types)
                ->and_where_close()

                //События юзера. Альбомы
                ->or_where_open()
                ->or_where('user_id', '=', $id)
                ->and_where('type', '=', 'update_album')
                ->and_where('value_1', 'in', $keys)
                ->or_where_close()

                ->order_by('id', 'DESC');
            $amount = $feeds->reset(false)->find_all()->count();
            $feeds = $feeds->limit(10)->find_all();
        }
        $this->page->content(View::factory('privatecoach/feed/list')
                ->bind('array', $array)
                ->bind('amount', $amount)
                ->bind('albums', $albums)
                ->bind('amount', $amount)
                ->bind('feeds', $feeds)
                ->bind('id', $id)
        );
    }

    public function action_ajax_list()
    {
        $this->render = false;
        $check = 1;
        $array = array();
        $user_albums = array();
        $id = $this->request->param('id');
        $counter = $this->request->query('i');
        $common_types = array('close_mission', 'new_achievement');
        $model = ORM::factory('Feed');
        $rules = $this->user->feed_rules->find_all()->as_array('id', 'feed_id');
        $rules[0] = 0;
        $full_albums = array();
        if ($this->user->id == $id) {
            $subscribers = ORM::factory('Subscribe')->where('user_id', '=', $id)->find_all()->as_array(null, 'target_id');
            $subscribers[] = $id;
            $albums = ORM::factory('User_Album')->where('user_id', 'in', $subscribers)->find_all();
            $keys = $albums->as_array(null, 'id');
            $keys[] = 0;
            foreach ($albums as $album) {
                $user_albums[$album->id] = array(
                    'photo' => array(),
                    'number' => 0,
                );
                foreach ($album->photos->find_all() as $photo) {
                    $user_albums[$album->id]['photo'][$photo->id] = $photo->photo_s->id;
                    $user_albums[$album->id]['number'] = count($user_albums[$album->id]['photo']);
                }
                if (count($user_albums[$album->id]['photo'])) {
                    $full_albums[] = $album->id;
                }
            }
            $feeds = $model
                //События юзеров, на которых я подписан, не относящиеся к альбомам или к персональным типам событий
                ->and_where_open()
                ->where('user_id', 'in', $subscribers)
                ->and_where_open()

                ->and_where('type', 'in', $common_types)
                ->and_where('user_id', 'in', $subscribers)

                ->or_where('type', '=', 'new_sub')
                ->and_where('user_id', '=', $id)

                ->and_where_close()
                ->and_where_close()

                //События юзеров, на которых я подписан(+я сам), относящиеся к альбомам
                ->or_where_open()
                ->or_where('user_id', 'in', $subscribers)
                ->and_where('type', '=', 'update_album')
                ->and_where('value_1', 'in', $keys)
//                ->and_where('id', 'in', $full_albums)
                ->or_where_close()

//                ->limit($counter * 10)
                ->where('id', 'not in', $rules)
                ->order_by('id', 'DESC')
                ->find_all();
            foreach ($feeds as $feed) {
                if ($check > (($counter - 1) * 10) && $check <= $counter * 10) {
                    $array[] = $feed;
                }
                $check++;
            }
            $amount = $feeds->count();
        } else {
            $albums = ORM::factory('User_Album')->where('user_id', '=', $id)->find_all();
            $keys = $albums->as_array(null, 'id');
            $keys[] = 0;
            foreach ($albums as $album) {
                $user_albums[$album->id] = array(
                    'photo' => array(),
                    'number' => 0,
                );
                foreach ($album->photos->find_all() as $photo) {
                    $user_albums[$album->id]['photo'][$photo->id] = $photo->photo_s->id;
                    $user_albums[$album->id]['number'] = count($user_albums[$album->id]['photo']);
                }
            }
            $feeds = $model
                //События юзера. Пока только достижения
                ->and_where_open()
                ->where('user_id', '=', $id)
                ->and_where('type', 'in', $common_types)
                ->and_where_close()

                //События юзера. Альбомы
                ->or_where_open()
                ->or_where('user_id', '=', $id)
                ->and_where('type', '=', 'update_album')
                ->and_where('value_1', 'in', $keys)
                ->or_where_close()

                ->order_by('id', 'DESC')
                ->find_all();
            foreach ($feeds as $feed) {
                if ($check > (($counter - 1) * 10) && $check <= $counter * 10) {
                    $array[] = $feed;
                }
                $check++;
            }
            $amount = $feeds->count();
        }
        if ($amount <= 10 * intval($counter)) {
            $over = true;
        }
        $feeds = $array;
        $this->response->body(View::factory('privatecoach/feed/ajax_list')
                ->bind('counter', $counter)
                ->bind('user_albums', $user_albums)
                ->bind('feeds', $feeds)
                ->bind('over', $over)
                ->bind('id', $id)
        );
    }

    public function action_view()
    {
        $id = $this->request->param('id');
        $feed = ORM::factory('Feed', $id);
        $photos = $feed->album->photos->find_all();
        $count = $photos->count();

        if (!$feed->loaded()) {
            throw new HTTP_Exception_404('Страница не найдена');
        }
        $this->page->content(View::factory('privatecoach/feed/view')
            ->bind('photos', $photos)
            ->bind('feed', $feed)
            ->bind('count', $count)
        );
    }

    public function action_delete_event()
    {
        $this->render = false;
        $id = $this->request->query('id');
        $model = ORM::factory('Feed', $id);
        if ($model->loaded()) {
            if ($model->user_id == $this->user->id) {
                $model->delete();
                $this->response->body('success');
            } else {
                $rule = ORM::factory('Feed_Rule');
                $rule->user_id = $this->user->id;
                $rule->feed_id = $model->id;
                $rule->rule = 'hide';
                $rule->save();
                $this->response->body('hide');
            }
        }
    }
}