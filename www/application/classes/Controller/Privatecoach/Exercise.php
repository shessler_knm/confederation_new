<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Privatecoach_Exercise extends Privatecoach_Site
{

    public function before()
    {
        parent::before();
        Register::instance()->activemenu = 71;
    }

    public function action_view()
    {
        $this->page->script('comments.js', 'js/comments.js');
        $this->page->script('gallery.js', 'js/gallery.js');
        $this->page->script('jquery.fancybox', "/js/fancybox/jquery.fancybox.js");
        $this->page->script('jquery.fancybox.pack', "/js/fancybox/jquery.fancybox.pack.js");
        $this->page->script('jquery.fancybox-buttons', "/js/fancybox/jquery.fancybox-buttons.js");
        $this->page->script('jquery.fancybox-thumbs', "/js/fancybox/jquery.fancybox-thumbs.js");
        $this->page->script('jquery.fancybox-media', "/js/fancybox/jquery.fancybox-media.js");
        $id = $this->request->param('id');
        $model = ORM::factory('Exercise', $id);
        $images = $model->images->find_all();
        $firstImage = $model->images->find();
        $categories = $model->categories->where('exercise_id', '=', $id)->find_all()->as_array('id', 'title');
        if (!$model->loaded()) {
            throw new HTTP_Exception_404();
        }
        $this->page->title($model->title);
        $this->page->meta($model->text, 'description');
        $exercise_score = ORM::factory('Exercise_Score');
        $scores = $exercise_score->where('target_id', '=', $id)->find_all()->as_array(null, 'score');
        $score = count($scores) > 0 ? array_sum($scores) / count($scores) : 0;
        $this->view_counter($model);
        $this->page->content(View::factory('privatecoach/exercise/view')
                ->bind('model', $model)
                ->bind('score', $score)
                ->bind('categories', $categories)
                ->bind('firstImage', $firstImage)
                ->bind('images', $images)
                ->set('voters', count($scores))
        );
    }

    public function action_score()
    {
        $this->render = false;
        $score = $this->request->query('score');
        $target = $this->request->query('target');
        $article = ORM::factory('Exercise', $target);
        $model = ORM::factory('Exercise_Score')->where('user_id', '=', $this->user->id)->where('target_id', '=', $target)->find();
        if (!$model->loaded()) {
            $model->user_id = $this->user->id;
            $model->target_id = $target;
        }
        $model->score = $score;
        $model->save();
        $model = ORM::factory('Exercise_Score')->where('target_id', '=', $target)->find_all();
        foreach ($model as $row) {
            $scores[] = $row->score;
        }
        $total_score = array_sum($scores) / count($scores);
        $article->score = $total_score;
        $article->save();
    }

    public function action_list()
    {
        $this->page->title(__('Упражнения'));
        $this->page->script('exercise', 'js/exercise.js');
        $query = $this->request->query();
        if ($query) {
            if (array_key_exists('id', $query)) {
                $model = ORM::factory('Program', $query['id']);
                if ($model->loaded()) {
                    $model->delete();
                }
            }
        }

        $categories = ORM::factory('Category')->where('sef', '=', 'muscle_group')->find();
        $categories = $categories->children->find_all()->as_array('id', 'title_' . I18n::$lang);
        $categories[0] = '';
        ksort($categories);
        $this->page->content(View::factory('privatecoach/exercise/list')
                ->bind('array', $array)
                ->bind('query', $query)
                ->bind('categories', $categories)
        );

    }

    public function action_ajax_list()
    {
        $this->render = false;
        $view = View::factory('privatecoach/exercise/ajax_list')
            ->bind('current_program', $current_program)
            ->bind('model', $model)
            ->bind('array', $array)
            ->bind('count', $count)
            ->bind('data', $data)
            ->bind('pagination', $pagination);
        $values = $this->request->post();
        $q = $this->request->post('search');
        $array = array();
        $muscle_filter = array();
        $page = $this->request->param('page');
        $model = ORM::factory('Exercise');
        if ($values) {
            if ($q && $q != __('Поиск по тексту')) {
                $model->where_open()
                    ->where('text_ru', 'LIKE', "%$q%")
                    ->or_where('text_kz', 'LIKE', "%$q%")
                    ->or_where('text_en', 'LIKE', "%$q%")
                    ->or_where('title_kz', 'LIKE', "%$q%")
                    ->or_where('title_ru', 'LIKE', "%$q%")
                    ->or_where('title_en', 'LIKE', "%$q%")
                    ->where_close();
            }
            if ($values['muscle']) {
                $exercises = ORM::factory('Exercise');
                $exercises = $exercises->join('exercises_categories', 'left')->on('exercises_categories.exercise_id', '=', 'exercise.id');
                $exercises = $exercises->where('exercises_categories.category_id', '=', $values['muscle'])->find_all();
                foreach ($exercises as $filter) {
                    $muscle_filter[] = $filter->id;
                }
                $muscle_filter[0] = 0;
                $model = $model->where('id', 'in', $muscle_filter);
            }
        }
        $pagination = $this->pagination($model, 'program_pagination.default');
        $model = $model->where('title_' . I18n::$lang, '!=', '')->where('text_' . I18n::$lang, '!=', '')->find_all();
        $count = $model->count();
        foreach ($model as $row) {
            $score = $row->exercise_scores->find_all()->as_array('id', 'score');
            if ($score) {
                $array[$row->id] = array_sum($score) / count($score);
            }
        }
        $data = array();
        foreach ($model as $row) {
            $data[$row->id] = $row->as_array_ext(array(), 'exercise_list');
        }
        $this->response->body($view);

    }
}