<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Almas
 * Date: 17.07.13
 * Time: 10:50
 * To change this template use File | Settings | File Templates.
 */
class Controller_Privatecoach_Main extends Privatecoach_Site
{
    public function before()
    {
        $this->pc_need_auth = false;
        parent::before();
    }

    public function action_index()
    {
        $this->page->title(__('Личный тренер'));
        $this->page->script('angular', 'js/lib/angular.min.js');
        $this->page->script('bootstrap-datepicker.js', 'js/lib/datepicker/js/bootstrap-datepicker.js');
        $this->page->script('resource', 'js/lib/angular/angular-resource.min.js');
        $this->page->script('sanitize', 'js/lib/angular/angular-sanitize.js');
        $this->page->script('privatecoach', 'js/privatecoach.js');
        $this->page->script('appChart', 'js/appChart.js');
        $this->page->script('jsgl', 'js/js2D/jsgl.min.js');
        $this->page->script_dir('js/privatecoach');
        $this->page->content(View::factory('privatecoach/index')
            ->bind('end_date', $end_date)
            ->bind('top', $top)
            ->bind('program', $program));
        if ($this->user) {
            $program = ORM::factory('Program')->where('user_id', '=', $this->user->id)->where('activity', '=', 1)->order_by('id', 'DESC')->find();
            $dt = new DateTime($program->start_date);
            $dt->add(new DateInterval('P' . ($program->duration * 7) . 'D'));
            $end_date = $dt->format('Y-m-d');
        }
        $top = ORM::factory('Top')->find_all()->count();

    }

    public function action_session()
    {
        $this->render = false;
        $id = $this->request->query('id');
        $action = $this->request->query('action');
        $value = $this->request->query('value');
        Session::instance()->set('id', $id);
        Session::instance()->set('action', $action);
        if ($value) {
            Session::instance()->set('value', $value);
        }
    }

}