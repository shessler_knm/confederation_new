<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Privatecoach_Article extends Privatecoach_Site
{

    public function before()
    {
        parent::before();
        Register::instance()->activemenu = 53;
    }

    public function scripts()
    {
        return parent::scripts() + array(
            'comments' => 'js/comments.js',

        );
    }

    public function action_view()
    {
        //вьюха одной новости

        $id = $this->request->param('id');
        $model = ORM::factory('Article', $id);
        $array = array();
        $array_of_score = array();
        if (!$model->loaded()) {
            throw new HTTP_Exception_404();
        }
        $categories = $model->categories->where('article_id', '=', $id)->find_all()->as_array('id', 'title');
        foreach ($categories as $key => $row) {
            $array[] = $key;
        }
        $array[] = 0;
        $this->page->title($model->title);
        $this->page->meta($model->text, 'description');
        $alike = ORM::factory('Article');
        $alike->join('article_categories', 'left')->on('article_categories.article_id', '=', 'article.id');
        $alike->where('article_categories.category_id', 'in', $array)->where('article_categories.article_id', '!=', $id);
        $alike->join('articles_scores', 'left')->on('articles_scores.target_id', '=', 'article.id');
        $alike->distinct(true)->order_by('articles_scores.score', 'DESC');
        $alike = $alike->limit(3)->find_all()->as_array();
        $article_score = ORM::factory('Article_Score');
        foreach ($alike as $row) {
            $sum = $article_score->where('target_id', '=', $row->id)->find_all()->as_array('id', 'score');
            $array_of_score[$row->id] = count($sum) > 0 ? array_sum($sum) / count($sum) : 0;
        }
        $scores = $article_score->where('target_id', '=', $id)->find_all()->as_array(null, 'score');
        $score = count($scores) > 0 ? array_sum($scores) / count($scores) : 0;
        $this->view_counter($model);
        $this->page->content(View::factory('privatecoach/article/view')
                ->set('voters', count($scores))
                ->bind('alike', $alike)
                ->bind('array_of_score', $array_of_score)
                ->bind('model', $model)
                ->bind('score', $score)
                ->bind('categories', $categories)
        );
    }

    public function action_list()
    {
        $this->page->title(__('Статьи'));
        $this->page->script('article', 'js/article.js');
        $model = ORM::factory('Article')->order_by('date', 'ASC')->limit(2);
        $filters = ORM::factory('Category')->get_sef_articles()->find_all();
        $model = $model->find_all();
        foreach ($model as $row) {
            $categories[$row->id] = $row->categories->find_all()->as_array('id', 'title_' . I18n::$lang);
        }
        $this->page->content(View::factory('privatecoach/article/list')
                ->bind('model', $model)
                ->set('counter', 1)
                ->bind('scores', $scores)
                ->bind('categories', $categories)
                ->bind('filters', $filters)
        );
    }

    public function action_ajax_list()
    {
        $this->render = false;
        $query_categories = $this->request->query('category');
        $ratSort = $this->request->query('score');
        $counter = $this->request->query('counter') ? $this->request->query('counter') : 1;
        $check = 1;
        $data = array();
        $selected = $this->request->query('selected');

        $model = ORM::factory('Article')->where('is_published', '=', 1);

        if ($query_categories && count($query_categories) > 0 && is_array($query_categories)) {
            $model->join('article_categories')
                ->on('article_categories.article_id', '=', DB::expr('article.id AND article_categories.category_id IN :cats', array(':cats' => $query_categories)))
                ->group_by('article.id');
        }
        if ($ratSort) {
            $model = $model->order_by('score', 'DESC');
        }

        $model = $model->order_by('id','DESC')->find_all();

        foreach ($model as $row) {
            if ($check > (($counter - 1) * 6) && $check <= $counter * 6) {
                $data[] = $row;
            }
            $check++;
            $categories[$row->id] = $row->categories->find_all()->as_array('id', 'title_' . I18n::$lang);
        }
        $amount = $model->count() - ($counter * 6);
        if ($amount <= 0) {
            $check = true;
        }
        if ($data) {
            $model = $data;
        }

        $this->response->body(View::factory('privatecoach/article/ajax_list')
                ->set('counter', 1)
                ->bind('more_id', $counter)
                ->bind('selected', $selected)
                ->bind('categories', $categories)
                ->bind('check', $check)
                ->bind('amount', $amount)
                ->bind('model', $model)
        );
    }

    public function action_preview()
    {
        $this->render = false;
        $model = ORM::factory('Article')->where('main', '!=', '1')->order_by('date', 'ASC')->limit(4)->find_all();
        $this->response->body(View::factory('privatecoach/article/preview')
                ->bind('model', $model)
        );

    }

    public function action_score()
    {
        $this->render = false;
        $score = $this->request->query('score');
        $target = $this->request->query('target');
        $article = ORM::factory('Article', $target);
        $model = ORM::factory('Article_Score')->where('user_id', '=', $this->user->id)->where('target_id', '=', $target)->find();
        if (!$model->loaded()) {
            $model->user_id = $this->user->id;
            $model->target_id = $target;
        }
        $model->score = $score;
        $model->save();
        $model = ORM::factory('Article_Score')->where('target_id', '=', $target)->find_all();
        foreach ($model as $row) {
            $scores[] = $row->score;
        }
        $total_score = array_sum($scores) / count($scores);
        $article->score = $total_score;
        $article->save();
    }

}