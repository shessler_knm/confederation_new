<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Almas
 * Date: 16.09.13
 * Time: 15:42
 * To change this template use File | Settings | File Templates.
 */

class Controller_Exception extends Site
{

    public function action_index()
    {
        $this->render = false;
        $response = Response::factory();
        $response->status(404);
        $this->session OR $this->session = Session::instance();
        $last_uri = $this->session->get('last_uri', URL::base());
        $fed_id = Request::$current->param('fed_id');
        $view = View::factory('errors/template');
        $view->content = View::factory('errors/404')
            ->bind('fed_id', $fed_id)
            ->bind('last_uri', $last_uri);
        $this->response->body($view);
    }

    public function action_translate()
    {
        $this->render = false;
        $last_uri = '/' . ltrim($this->session->get('last_uri', URL::base()), '/');
        $view = View::factory('errors/template');
        $view->content = View::factory('errors/translite')
            ->bind('url', $url)
            ->bind('model', $model)
            ->bind('model_name', $model_name)
            ->bind('last_uri', $last_uri);
        $this->response->body($view);
    }

    /*
     * Экшен отвечает за ссылки переключателей языка на странице ошибок
     */
    public function action_switch_lang()
    {
        $this->render = false;
        $view = View::factory('errors/switch_lang')
            ->bind('url', $url)
            ->bind('model', $model)
            ->bind('model_name', $model_name)
            ->bind('last_uri', $last_uri);
        $langs = array('ru' => '/ru/', 'en' => '/en/', 'kz' => '/');
        $currentURI = Helper::currentURI();
//        $last_uri = '/' . ltrim($this->session->get('last_uri', URL::base()), '/');
        //поиск названия модели
        preg_match('"([^/]+)(?=/[^/]*$)"', $currentURI, $match);
        $team = preg_match('"team"', $last_uri);
        $coach = preg_match('"coach"', $last_uri);
        $model_name = array_key_exists(0,$match)?$match[0]:null;
        switch ($model_name) {
            case 'gallery' :
                $model = ORM::factory('Photovideo');
                break;
            case 'pressservice':
            case 'interview':
            case 'aboutus':
            case 'news':
                $model = ORM::factory('News');
                break;
            case $team == 1:
                $model = ORM::factory('Player');
                break;
            case $coach == 1:
                $model = ORM::factory('Coach');
                break;
            case 'section':
                $model = ORM::factory('Section');
                break;
//            default :
//                $model = ORM::factory(ucfirst($model_name));
        }
        preg_match('"([^/]*$)"', $currentURI, $match);
        if ($model !== null) {
            foreach ($langs as $lang => $val) {
                if ($model->where('sef_' . $lang, '=', $match[0])->find()->loaded()) {
                    break;
                }
            }
            $url = preg_replace('"([^/]*$)"', '', Helper::currentURI());
        }
        $this->response->body($view);
    }
}