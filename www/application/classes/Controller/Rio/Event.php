<?php defined('SYSPATH') OR die('No direct access allowed.');


class Controller_Rio_Event extends Rio_Site{

    public function action_list()
    {
        $this->add_crumb(__('Календарь'), '');
        $year = date('Y');
        $page = $this->request->query('page');
        if ($page == 1) {
            $this->redirect(Request::initial()->uri(), 301);
        }
        $year_array = array();
        $differences = array();
        $this->page->title(__('Календарь') . ' - ' . $this->federation ? $this->federation->{'title_' . I18n::$lang} : __('Конфедерация спортивных единоборств и силовых видов спорта'));
        $cities = ORM::factory('City');
        $sports = ORM::factory('Category');
        $month = array();
        $city_array = array();
        $avail_months = array();
        $month_id = date('m');
        $city_id = 'all';
        $fed_id = $this->request->param('fed_id');
        if ($fed_id != 'wrestling') {
            $sport_id = ORM::factory('Category')->where('title_en', '=', $fed_id)->find()->id;
        } else {
            $sport_ids = ORM::factory('Category')->where('title_en', 'LIKE', '%'.$fed_id.'%')->find_all();
        }

        $sport_array['all'] = __('Все виды спорта');
        $city_array['all'] = __('Все города');
        foreach ($cities->reset()->find_all() as $city) {
            $city_array[$city->sef] = $city->title;
        }


        if ($this->fed_id) {
            $model = $this->federation
                ->events
                ->where('is_published', '!=', '2');
            $check_model = $this->federation
                ->events
                ->where('is_published', '!=', '2')->where(DB::expr("DATE_FORMAT(date_begin,'%Y')"), '=', $year)->find();
            $a_years = $this->federation->events->where('is_published', '!=', '2')->order_by('date_begin', 'ASC')->find_all();
        } else {
            $model = ORM::factory('Event')
                ->where('is_published', '!=', '2');
            $check_model = ORM::factory('Event')
                ->where('is_published', '!=', '2')->where(DB::expr("DATE_FORMAT(date_begin,'%Y')"), '=', $year)->find();
            $a_years = ORM::factory('Event')->where('is_published', '!=', '2')->order_by('date_begin', 'ASC')->find_all();
        }

        if (!$check_model->loaded()) {
            throw new HTTP_Exception_404();
        }

        foreach ($a_years as $a_year) {
            $a_year = Date::formatted_time($a_year->date_begin, 'Y');
            if (!in_array($a_year, $year_array)) {
                if ($a_year < $year) {
                    $differences[0] = $year - $a_year;
                } elseif ($a_year > $year && Arr::get($differences, 1, false) == false) {
                    $differences[1] = $a_year - $year;
                }
                $year_array[] = $a_year;
            }
        }

        $model->where(DB::expr("DATE_FORMAT(date_begin,'%Y')"), '=', $year);
        if (!($month_id == 'all')) {
            $model->where(DB::expr("DATE_FORMAT(date_begin,'%m')"), '=', intval($month_id));
        }
        if (!($city_id == 'all') && (array_key_exists($city_id, $city_array))) {
            $model->where('city_id', '=', $cities->reset(false)->where('sef', '=', $city_id)->find()->id);
        } else {
            $city_id = 'all';
        }
        if (!($sport_id == 'all')) {
            if ($sport_ids) {
                $model->join('events_categories', 'left')->on('events_categories.event_id', '=', 'event.id');
                $model->and_where_open();
                    $model->or_where_open();
                    foreach($sport_ids as $sport_id) {
                        $model->or_where('events_categories.category_id', '=', $sport_id);
                    }
                    $model->or_where_close();
                $model->and_where_close();
                $model->distinct(true);
            } else {
                $model->join('events_categories', 'left')->on('events_categories.event_id', '=', 'event.id');
                $model->where('events_categories.category_id', '=', $sport_id);
            }
        }
        $count = $model->reset(false)->count_all();
        $model = $model
            ->where('title_' . I18n::$lang, '!=', '')//            ->where('text_' . I18n::$lang, '!=', '')
        ;
        $pagination = $this->pagination($model, 'event_pagination.default');
        $page = $this->request->query('page');
        $total_pages = $pagination->total_pages;
        $this->check_pages($page, $total_pages);
        View::bind_global('total_pages', $total_pages);
        $collection = $model->order_by('date_begin', 'ASC')->find_all();
        if ($count > 0) {
            foreach ($collection as $row) {
                $m = Date::formatted_time($row->date_begin, 'm');
                $avail_months[$m][] = $row;
            }

        }
        foreach ($sports->get_sef_sports()->find_all() as $row) {
            $sport_array[$row->id] = $row->title;
        }
        $month['all'] = __('Весь год');
        $month[1] = __('Январь');
        $month[2] = __('Февраль');
        $month[3] = __('Март');
        $month[4] = __('Апрель');
        $month[5] = __('Май');
        $month[6] = __('Июнь');
        $month[7] = __('Июль');
        $month[8] = __('Август');
        $month[9] = __('Сентябрь');
        $month[10] = __('Октябрь');
        $month[11] = __('Ноябрь');
        $month[12] = __('Декабрь');

        $this->page->content(
            View::factory('events/list_rio')->bind('pagination', $pagination)
                ->bind('pagination', $pagination)
                ->bind('year', $year)
                ->bind('month', $month)
                ->bind('avail_months', $avail_months)
                ->bind('differences', $differences)
                ->bind('month_id', $month_id)
                ->bind('city_id', $city_id)
                ->bind('sport_id', $sport_id)
                ->bind('count', $count)
                ->bind('city', $cities)
                ->bind('city_array', $city_array)
                ->bind('sport_array', $sport_array)
                ->bind('year_array', $year_array)
                ->bind('model', $model)
        );
    }

}