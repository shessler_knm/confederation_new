<?php defined('SYSPATH') OR die('No direct access allowed.');


class Controller_Rio_News extends Rio_Site{

    public function action_view()
    {
        //вьюха одной новости
        $sef = $this->request->param('id');
        $model = ORM::factory('News')
            ->where('sef_' . i18n::lang(), '=', $sef)
            /*->and_where('rio', '=', 1)*/
            ->find();

        if (!$model->loaded()) {
            foreach ($this->array_langs as $lang => $url) {
                $model = ORM::factory('News')->where('sef_' . $lang, '=', $sef)->find();
                if ($model->loaded()) {
                    throw new HTTP_Exception_Translate();
                }
            }
            throw new HTTP_Exception_404('Страница не найдена');
        }

        $this->view_counter($model);
        $this->init_langs($model);
        $this->page->title($model->title . ' - ' . __('Новости') . ' - ' . $this->federation->{'title_' . I18n::$lang});
        $this->page->meta(Text::limit_chars($model->text, 200), 'description');
        $this->page->content(View::factory('news/view_rio')
            ->bind('model', $model));
    }

}