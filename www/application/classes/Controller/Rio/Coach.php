<?php defined('SYSPATH') OR die('No direct access allowed.');


class Controller_Rio_Coach extends Rio_Site{

    public function action_list()
    {
        $sef = $this->request->param('id');
        $this->page->title(__('Тренерский состав'));
        $federation = $this->federation;
        $teams = $federation->teams;
        $first_team = $teams->reset(false)->order_by('id', 'ASC')->where('is_published', '!=', '2');
        $teams = $teams->find_all();
        $team = ORM::factory('Team');
        if ($this->fed_id == 'wrestling') {
            $first_team = $first_team->find();
            $this->init_langs($first_team);
            $coaches = $team->where('sef_' . I18n::$lang, '=', $sef)->find()->coaches->where('is_published', '!=', '2');
            if (!$sef || !$first_team->federation_id == $federation->id) {
                $this->redirect('rio2016/' . $this->fed_id . '/coach/list/' . $first_team->{'sef_' . I18n::$lang}, 301);
            }
        } else {
            $coaches = ORM::factory('Coach')->where('federation_id', '=', $federation->id)->where('is_published', '=', 1);
        }
        $pagination = $this->pagination($coaches, 'player_pagination.default');
        $page = $this->request->param('page');
        $total_pages = $pagination->total_pages;
        $this->check_pages($page, $total_pages);

        View::bind_global('total_pages', $total_pages);
        $coaches = $coaches->find_all();
        $this->page->content(
            View::factory('coach/list_rio')
                ->bind('sef', $sef)
                ->bind('model', $teams)
                ->bind('coaches', $coaches)
                ->bind('federation', $federation)
                ->bind('pagination', $pagination)
        );
    }

    public function action_view()
    {
        $sef = $this->request->param('id');
        $model = ORM::factory('Coach')->where('sef_' . i18n::lang(), '=', $sef)->find();
        $this->init_langs($model, true);
        $this->page->title($model->get_full_name() . ' - ' . __('Тренеры') . ' - ' . $this->federation->{'title_' . I18n::$lang});
        $this->page->meta(Text::limit_words($model->biography, 150), 'description');
        if (!$model->loaded()) {
            foreach ($this->array_langs as $lang => $url) {
                $model = ORM::factory('Coach')->where('sef_' . $lang, '=', $sef)->find();
                if ($model->loaded()) {
                    throw new HTTP_Exception_Translate();
                }
            }
            throw new HTTP_Exception_404('Страница не найдена');
        }
        $this->page->content(
            View::factory('coach/view_rio')
                ->bind('model', $model)
                ->bind('federation', $this->federation)
        );
    }

}