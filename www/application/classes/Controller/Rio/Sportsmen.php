<?php defined('SYSPATH') OR die('No direct access allowed.');


class Controller_Rio_Sportsmen extends Rio_Site{

    public function action_view()
    {
        $sef = $this->request->param('id');
        $model = ORM::factory('Player')->where('sef_' . i18n::lang(), '=', $sef)->find();
        if (!$model->loaded()) {
            foreach ($this->array_langs as $lang => $url) {
                $model = ORM::factory('Player')->where('sef_' . $lang, '=', $sef)->find();
                if ($model->loaded()) {
                    throw new HTTP_Exception_Translate();
                }
            }
            throw new HTTP_Exception_404('Страница не найдена');
        }
        $this->init_langs($model, true);
        $this->page->title($model->get_full_name() . ' - ' . __('Сборные команды') . ' - ' . $this->federation->{'title_' . I18n::$lang});
        $this->page->meta(Text::limit_words($model->biography, 150), 'description');
        $this->page->content(View::factory('sportsmen/view_rio')->bind('model', $model));
    }

    public function action_list()
    {
        $sef = $this->request->param('id');
        $fed_id = $this->request->param('fed_id');
        $federation = ORM::factory('Federation')->where('sef', '=', $fed_id)->find();
        $teams = $federation->teams;
        $first_team = $teams->reset(false)->order_by('id', 'ASC')->where('is_published', '!=', '2')/*->where('rio', '=', 1)*/;
        $teams = $teams->where('main', '=', '2')/*->where('rio', '=', 1)*/->find_all();
        $team = ORM::factory('Team')->where('sef_' . i18n::lang(), '=', $sef)/*->where('rio', '=', 1)*/->find();
        $this->init_langs($team);
        if ($teams->as_array()) {
            $first_team = $first_team->find();
            if ($sef && $first_team->federation_id == $federation->id) {
                $players = ORM::factory('Player')->where('is_published', '=', '1')->where('team_id', '=', $team->id)->where('firstname_'.I18n::$lang,'!=','')->where('lastname_'.I18n::$lang,'!=','')->order_by('weight', 'ASC')->order_by('category', 'ASC');
            } else {
                $language = (I18n::$lang == 'kz') ? '/' : I18n::$lang . '/';
                $this->redirect($language . 'rio2016/'. $fed_id . '/sportsmen/list/' . $first_team->{'sef_' . I18n::$lang}, 301);
            }
            $this->page->title($team->title . ' - ' . __('Сборные команды') . ' - ' . $federation->{'title_' . I18n::$lang});
            $this->page->meta(Text::limit_words($team->text, 150), 'description');
            $pagination = $this->pagination($players, 'player_pagination.default');
            $page = $this->request->param('page');
            $total_pages = $pagination->total_pages;
            $this->check_pages($page, $total_pages);
            View::bind_global('total_pages', $total_pages);
            $players = $players->find_all();
        } else {
            $no_team = true;
        }
        if (!$team->loaded()) {
            foreach ($this->array_langs as $lang => $url) {
                $model = ORM::factory('Team')->where('sef_' . $lang, '=', $sef)->find();
                if ($model->loaded()) {
                    throw new HTTP_Exception_Translate();
                }
            }
            throw new HTTP_Exception_404('Страница не найдена');
        }
        $this->page->content(View::factory('sportsmen/list_rio')
            ->bind('model', $teams)
            ->bind('sef', $sef)
            ->bind('no_team', $no_team)
            ->bind('players', $players)
            ->bind('pagination', $pagination)
            ->bind('federation', $federation)
        );
    }

}