<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_News extends Site
{
    public function before()
    {
        parent::before();
        if ($this->fed_id) {
            Register::instance()->activemenu = 25;
        } else {
            Register::instance()->activemenu = 18;
        }
    }

    public function scripts()
    {
        return parent::scripts() + array(
            'comments' => 'js/comments.js',
        );
    }

    public function init_langs($model = NULL)
    {
        $langs = array('ru' => '/ru/', 'en' => '/en/', 'kz' => '/');
        if ($model === NULL) {
            parent::init_langs($model);
        } else {
            foreach ($langs as $lang => $val) {
                if (empty($model->{'sef_' . $lang})) {
                    if ($this->fed_id) {
                        $this->array_langs[$lang] = $val . $this->fed_id . '/news/' . $model->{'sef_' . I18n::$lang};
                    } else {
                        $this->array_langs[$lang] = $val . 'confederation/news/' . $model->{'sef_' . I18n::$lang};
                    }
                } else {
                    if ($this->fed_id) {
                        $this->array_langs[$lang] = $val . $this->fed_id . '/news/' . $model->{'sef_' . $lang};
                    } else {
                        $this->array_langs[$lang] = $val . 'confederation/news/' . $model->{'sef_' . $lang};
                    }
                }
            }
        }
    }

    public function action_view()
    {
        //вьюха одной новости
        $sef = $this->request->param('sef');

        $model = ORM::factory('News')->where('sef', '=', $sef)->find();
        if (!$model->loaded()) {
            $model = ORM::factory('News')->where('sef_' . i18n::lang(), '=', $sef)->find();
        }

        if (!$model->loaded()) {
            foreach ($this->array_langs as $lang => $url) {
                $model = ORM::factory('News')->where('sef_' . $lang, '=', $sef)->find();
                if ($model->loaded()) {
                    throw new HTTP_Exception_Translate();
                }
            }
            throw new HTTP_Exception_404('Страница не найдена');
        }
        if (!(strstr(Request::current()->url(), 'confederation'))) {
            if (!($model->has('federations', $this->federation)) || !$model->{'text_' . I18n::$lang}) {
                throw new HTTP_Exception_404('Страница не найдена');
            }
        }
//        } else {
//            if (!$model->loaded() || !$model->{'text_' . I18n::$lang}) {
//                throw new HTTP_Exception_404('Страница не найдена');
//            }
//            $this->redirect(URL::site((I18n::$lang!='kz'?I18n::$lang.'/':null).'confederation/infocenter/news/'.$model->{'sef_'.I18n::$lang}));
//        }
        $this->view_counter($model);
        $this->init_langs($model);
        $this->page->title($model->title . ' - ' . __('Новости') . ' - ' . $this->federation->{'title_' . I18n::$lang});
        $this->page->meta(Text::limit_chars($model->text, 200), 'description');
        $this->page->content(View::factory('news/view')
            ->bind('model', $model));
    }

    public function action_index()
    {
        switch ($this->fed_id) {
            case 'box':
                $this->page->title(__('Последние новости бокса Казахстана - Казахстанская федерация бокса'));
                $this->page->meta(__('Новости казахстанского бокса на сегодня: только свежая и актуальная информация каждый день!'), 'description');
                break;
            case 'judo':
                $this->page->title(__('Новости дзюдо - Федерация дзюдо Казахстана'));
                $this->page->meta(__('Новости казахстанского дзюдо: свежая и достоверная информация, цифры, факты, мнения'), 'description');
                break;
            case 'wrestling':
                $this->page->title(__('Новости вольной, греко-римской и женской борьбы - Федерация греко-римской, вольной и женской борьбы Республики Казахстан'));
                $this->page->meta(__('Вольная, греко-римская и женская борьба. Актуальные новости, достоверные факты, интервью и сводки с полей'), 'description');
                break;
            case 'taekwondo':
                $this->page->title(__('Новости таеквондо - Федерация таеквондо (WTF) Республики Казахстан'));
                $this->page->meta(__('Новости таеквондо в Казахстане: цифры, факты, результаты, интервью со спортсменами'), 'description');
                break;
            case 'weightlifting':
                $this->page->title(__('Новости тяжелой атлетики - Федерация тяжелой атлетики Казахстана'));
                $this->page->meta(__('Новости тяжелой атлетики. Только актуальная и свежая информация, а также интервью, цифры, факты!'), 'description');
                break;
        }
        if ($this->fed_id) {
            $cat = ORM::factory('Category')->where('slug', '=', 'news')->find();
            $news = $this->federation->news;
            $news->join('news_categories', 'left')->on('news_categories.news_id', '=', 'news.id');
            $news->where('news_categories.category_id', '=', $cat->id);
            $news = $news->where('main', '!=', '1')->where('title_' . I18n::$lang, '!=', '')->where('text_' . I18n::$lang, '!=', '')->where('is_published', '=', 1)->order_by('date', 'DESC');
        } else {
            $news = ORM::factory('News')
                ->where('main', '=', '2')
                ->where('is_published', '!=', '2')
                ->where('title_' . I18n::$lang, '!=', '')
                ->where('text_' . I18n::$lang, '!=', '')
                ->order_by('date', 'DESC');
            $cat = ORM::factory('Category')->where('slug', '=', 'news')->find();
            $news->join('news_categories', 'left')->on('news_categories.news_id', '=', 'news.id');
            $news->where('news_categories.category_id', '=', $cat->id);
        }
        //список новостей на отдельной
        $pagination = $this->pagination($news);
        $page = $this->request->param('page');
        $total_pages = $pagination->total_pages;
        $this->check_pages($page, $total_pages);
        View::bind_global('total_pages', $total_pages);
        $news = $news->find_all();
        $this->page->content(View::factory('news/list')
            ->bind('news', $news)
            ->bind('pagination', $pagination));
        $this->add_crumb(__('Новости'), '');
    }

    public function action_federation_preview()
    {
        $this->render = false;
        $cat = ORM::factory('Category')->where('slug', '=', 'news')->find();
        $model = $this->federation->news;
        $model->join('news_categories', 'left')->on('news_categories.news_id', '=', 'news.id');
        $model->where('news_categories.category_id', '=', $cat->id);
        $model = $model->where('is_published', '!=', '2')->where('main', '=', '2')->where('title_' . I18n::$lang, '!=', '')->where('text_' . I18n::$lang, '!=', '')->order_by('date', 'DESC')->limit(7)->find_all();
        $this->response->body(View::factory('news/preview')
            ->bind('model', $model));
    }

    public function action_confederation_preview()
    {
        $this->render = false;
        $cat = ORM::factory('Category')->where('slug', '=', 'news')->find();
        $news = ORM::factory('News')
            ->where('main', '=', '2')
            ->where('is_published', '!=', '2')
            ->where('rio', '=', 0)
            ->order_by('date', 'DESC')
            ->limit(5);
        $news->join('news_categories', 'left')->on('news_categories.news_id', '=', 'news.id');
        $news->where('news_categories.category_id', '=', $cat->id);
        $news = $news->where('title_' . I18n::$lang, '!=', '')->where('text_' . I18n::$lang, '!=', '')->find_all();
        $federations = array();
        foreach ($news as $row) {
            $federations[$row->id] = $row->federations->find_all()->as_array();
        }
        $this->response->body(View::factory('news/preview')
            ->set('counter', 1)
            ->bind('model', $news)
            ->bind('federations', $federations)
        );
    }

    public function action_confederation_preview_rio()
    {
        $this->render = false;
        $cat = ORM::factory('Category')->where('slug', '=', 'news')->find();
        $news = ORM::factory('News')
            ->where('main', '=', '2')
            ->where('is_published', '!=', '2')
            /*->where('rio', '=', 1)*/
            ->order_by('date', 'DESC')
            ->limit(5);
        $news->join('news_categories', 'left')->on('news_categories.news_id', '=', 'news.id');
        $news->where('news_categories.category_id', '=', $cat->id);
        $news = $news->where('title_' . I18n::$lang, '!=', '')->where('text_' . I18n::$lang, '!=', '')->find_all();
        $federations = array();
        foreach ($news as $row) {
            $federations[$row->id] = $row->federations->find_all()->as_array();
        }
        $this->response->body(View::factory('news/preview_rio')
                ->set('counter', 1)
                ->bind('model', $news)
                ->bind('federations', $federations)
        );
    }

    public function action_widget()
    {
        $this->render = false;
        if ($this->fed_id) {
            $model = $this->federation->news
                ->where('title_' . I18n::$lang, '!=', '')->where('text_' . I18n::$lang, '!=', '')
                ->where('slider', '=', '2')
                ->where('is_published', '!=', '2')
                ->where('rio', '=', 0)
                ->where_open()
                ->or_where('news.photo', '>', '0')
                ->or_where('photo_slider', '>', '0')
                ->where_close()
                ->order_by('date', 'DESC')
                ->limit(5)
                ->find_all();
        } else {
            $model = ORM::factory('News')
                ->where('title_' . I18n::$lang, '!=', '')->where('text_' . I18n::$lang, '!=', '')
                ->where('slider', '=', '2')
                ->where('is_published', '!=', '2')
                ->where('rio', '=', 0)
                ->where_open()
                ->or_where('photo', '>', '0')
                ->or_where('photo_slider', '>', '0')
                ->where_close()
                ->order_by('date', 'DESC')
                ->limit(5)
                ->find_all();
        }
        $content = View::factory('news/widgets/widget_main')
            ->bind('model', $model)
            ->bind('nav', $nav)
            ->set('counter', 1);
        $this->response->body($content);
    }

    public function action_widget_rio()
    {
        $this->render = false;
        if ($this->fed_id) {
            $model = $this->federation->news
                ->where('title_' . I18n::$lang, '!=', '')->where('text_' . I18n::$lang, '!=', '')
                ->where('slider', '=', '2')
                ->where('is_published', '!=', '2')
                /*->where('rio', '=', 1)*/
                ->where_open()
                ->or_where('news.photo', '>', '0')
                ->or_where('photo_slider', '>', '0')
                ->where_close()
                ->order_by('date', 'DESC')
                ->limit(5)
                ->find_all();
        } else {
            $model = ORM::factory('News')
                ->where('title_' . I18n::$lang, '!=', '')->where('text_' . I18n::$lang, '!=', '')
                ->where('slider', '=', '2')
                ->where('is_published', '!=', '2')
                /*->where('rio', '=', 1)*/
                ->where_open()
                ->or_where('photo', '>', '0')
                ->or_where('photo_slider', '>', '0')
                ->where_close()
                ->order_by('date', 'DESC')
                ->limit(5)
                ->find_all();
        }
        $content = View::factory('news/widgets/widget_main_rio')
            ->bind('model', $model)
            ->bind('nav', $nav)
            ->set('counter', 1);
        $this->response->body($content);
    }

    public function action_main_preview()
    {
        $this->render = false;
        $news = $this->federation->news->where('is_published', '!=', '2')->where('main_preview', '!=', '1')->where('title_' . I18n::$lang, '!=', '')->where('text_' . I18n::$lang, '!=', '')->order_by('date', 'DESC')->limit(5)->find_all();
        $this->response->body(View::factory('news/main_preview')
            ->bind('model', $news));
    }

    public function action_translit_message()
    {
        $this->render = false;
        $langs = array('ru' => '/ru/', 'en' => '/en/', 'kz' => '/');
        $this->write_lst_uri = false;
        $this->response->status(404);
        $this->session OR $this->session = Session::instance();
        $last_uri = '/' . ltrim($this->session->get('last_uri', URL::base()), '/');
        $model = ORM::factory('News');
        preg_match('"([^/]*$)"', $last_uri, $match);
        foreach ($langs as $lang => $val) {
            if ($model->where('sef_' . $lang, '=', $match[0])->find()->loaded()) {
                break;
            }
        }
        $url = $this->fed_id . '/news';
        Helper::currentURI();
        $view = View::factory('errors/translite')
            ->bind('url', $url)
            ->bind('model', $model)
            ->bind('model_name', $model_name)
            ->bind('last_uri', $last_uri);
        $this->response->body($view);
    }

    public function action_rss()
    {
        $this->render = false;
        $this->response->headers('content-type', 'application/rss+xml');
        $lang = I18n::$lang;
        $info = array(
            'title' => 'Новости',
            'description' => 'Свежие новости от сайта www.confederation.kz',
            'link' => 'http://www.confederation.kz/ru/news/rss',
            'pubDate' => time()
        );
        $news = ORM::factory('News')
            ->where('is_published', '=', '1')
            ->where('title_' . I18n::$lang, '!=', '')
            ->where('announcement_' . I18n::$lang, '!=', '')
            ->order_by('date', 'DESC')
            ->limit(50);
        $items = array();
        foreach ($news->find_all() as $row) {
            $url = URL::site('ru/confederation/infocenter/news/' . $row->{'sef_' . I18n::$lang});
//            $url='confederation/infocenter/news/'.$row->{'sef_'.I18n::$lang};
            $items[] = array(
                'title' => ORM::clear_html($row->{'title_' . $lang}),
                'description' => strip_tags($row->{'announcement_' . $lang}),
                'image' => $row->photo_s->url_image(NULL, NULL, 'http'),
                'content' => strip_tags($row->{'text_' . $lang}),
                'link' => $url,
                'pubDate' => $row->date,
            );
        }
        echo Feed::create($info, $items);
    }
}
