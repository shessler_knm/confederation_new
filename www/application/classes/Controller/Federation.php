<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Federation extends Site
{
    public function action_view()
    {
        if ($this->request->param('sef')) {
            $this->{'action_' . $this->request->param('sef')}();
            return;
        }
        $this->page->title($this->federation->title);
        $this->page->content(View::factory('federation/list'));
    }

    public function action_index()
    {
        if ($this->request->param('sef')) {
            $this->{'action_' . $this->request->param('sef')}();
            return;
        }
        switch ($this->fed_id) {
            case 'box':
                $this->page->title(__('Казахстанская федерация бокса'));
                $this->page->meta(__('Бокс в Казахстане: новости, фото и видео с соревнований, календарь предстоящих событий, а также свежие результаты и обновления составов сборных!'), 'description');
                break;
            case 'judo':
                $this->page->title(__('Федерация дзюдо Казахстана'));
                $this->page->meta(__('Дзюдо в Казахстане: новости, фото и видео с соревнований, календарь ожидаемых событий, а также свежие результаты и обновления составов сборных!'), 'description');
                break;
            case 'wrestling':
                $this->page->title(__('Федерация греко-римской, вольной и женской борьбы Республики Казахстан'));
                $this->page->meta(__('Греко-римская, вольная и женская борьба в Казахстане. Свежие и актуальные новости, события, фото- и видеоматериалы'), 'description');
                break;
            case 'taekwondo':
                $this->page->title(__('Федерация таеквондо (WTF) Республики Казахстан'));
                $this->page->meta(__('Секции таеквондо в Астане, Алматы и прочих крупных городах и населенных пунктах Казахстана. Адреса, контактные данные'), 'description');
                break;
            case 'weightlifting':
                $this->page->title(__('Федерация тяжелой атлетики Казахстана'));
                $this->page->meta(__('Тяжелая атлетика в Казахстане: новости, фото и видео с турниров и чемпионатов, календарь ожидаемых событий'), 'description');
                break;
        }
        $this->page->content(View::factory('federation/list'));
    }

    public function action_governance()
    {
        $model = $this->federation->governance->order_by('priority', 'ASC');
        $pagination = $this->pagination($model);
        $model = $model->find_all();
        $this->page->title(__('Руководоство федерации'));
        $this->page->content(View::factory('federation/governance')
            ->bind('pagination', $pagination)
            ->bind('model', $model));
    }

    public function  action_history()
    {
        //О федерации
        $model = $this->federation;
        if (!$model->loaded())
            throw new HTTP_Exception_404('Страница не найдена');
        $this->page->title(__('История федерации'));
        $this->page->content(View::factory('federation/history')
            ->bind('model', $model));
    }

    public function action_contact()
    {
        if ($this->fed_id) {
            $this->page->script('contacts', 'http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU');
            $this->page->script('map', 'js/contacts.js');
            $model = $this->federation;
            if (!$model->loaded()) {
                throw new HTTP_Exception_404('Страница не найдена');
            }
            $this->page->title(__('Контакты'));
            $this->page->content(View::factory('federation/contacts')
                ->bind('model', $model));
        } else {
            throw new HTTP_Exception_404('Страница не найдена');
        }
    }

    public function  action_structure()
    {
        //О федерации
        $this->page->script('page_fancybox', 'js/page_fancybox.js');
        $this->page->script('jquery.fancybox', "/js/fancybox/jquery.fancybox.js");
        $this->page->script('jquery.fancybox.pack', "/js/fancybox/jquery.fancybox.pack.js");
        $this->page->script('jquery.fancybox-buttons', "/js/fancybox/jquery.fancybox-buttons.js");
        $this->page->script('jquery.fancybox-thumbs', "/js/fancybox/jquery.fancybox-thumbs.js");
        $this->page->script('jquery.fancybox-media', "/js/fancybox/jquery.fancybox-media.js");
        $model = $this->federation;
        if (!$model->loaded())
            throw new HTTP_Exception_404('Страница не найдена');
        $this->page->title(__('Структура'));
        $this->page->content(View::factory('federation/structure')
            ->bind('model', $model));
    }

//    public function action_sef()
//    {
//        $this->render = false;
//        $lang = $this->request->param('lang');
//        set_time_limit(0);
//        $id = ucfirst($this->request->param('id'));
//        $model = ORM::factory($id)->find_all();
//        foreach ($model as $row) {
//            if ($id == 'Coach' || $id == 'Player') {
//                $row->sef_ru = ORM::full_name_filter_sef('', $row->get_full_name('ru'));
//                $row->sef_en = ORM::full_name_filter_sef('', $row->get_full_name('en'));
//                $row->sef_kz = ORM::full_name_filter_sef('', $row->get_full_name('kz'));
//            } elseif($id == 'Team') {
//                $row->sef_ru = ORM::filter_team_sef('', $row->title_ru,$row->id);
//                $row->sef_kz = ORM::filter_team_sef('', $row->title_kz,$row->id);
//                $row->sef_en = ORM::filter_team_sef('', $row->title_en,$row->id);
//            }else {
//                $row->sef_ru = ORM::filter_sef('', $row, 'title_ru');
//                $row->sef_kz = ORM::filter_sef('', $row, 'title_kz');
//                $row->sef_en = ORM::filter_sef('', $row, 'title_en');
//            }
//            $row->save();
//        }
//        $this->response->body('Все хорошо прошло, надеюсь!');
//    }
}