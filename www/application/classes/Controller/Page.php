<?php

class Controller_Page extends Site
{
    public function scripts()
    {
        return parent::scripts() + array(
            'jquery.fancybox' => "/js/fancybox/jquery.fancybox.js",
            'jquery.fancybox.pack' => "/js/fancybox/jquery.fancybox.pack.js",
            'jquery.fancybox-buttons' => "/js/fancybox/jquery.fancybox-buttons.js",
            'jquery.fancybox-thumbs' => "/js/fancybox/jquery.fancybox-thumbs.js",
            'jquery.fancybox-media' => "/js/fancybox/jquery.fancybox-media.js",
            "contacts" => "http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU",
            'map' => 'js/contacts.js',
        );
    }

    public function action_index()
    {
        $sef = $this->request->param('id');
        if (preg_match('#_fb#', $sef)) {
            $this->page->script('page_fancybox', 'js/page_fancybox.js');
        }
        if($sef == 'partners') {
            $this->add_crumb(__('Партнеры'), '');
            $this->page->title(__('Партнеры'));
            $model = ORM::factory('Partner')->find_all();
            $categories = ORM::factory('Category')->where('parent_id', '=', 321)->find_all();
            $videos = ORM::factory('Pvideo')->where('title_'.I18n::$lang, '<>' , '')->find_all();
            $this->page->content(View::factory('partner/list')
                ->bind('model', $model)
                ->bind('categories', $categories)
                ->bind('videos', $videos));
        } else {
            $model = ORM::factory('Page', array('sef' => $sef));
            if (!$model->loaded()) {
                throw new HTTP_Exception_404();
            }
            $this->add_crumb($model->title, '');
            $this->page->title($model->title);
            $this->page->meta(Text::limit_chars($model->text, 200), 'description');
            $this->page->content(View::factory($model->view_file ? $model->view_file : 'pages/view')
                ->bind('model', $model));
        }
    }

    public function action_get_partner()
    {
        if ($this->request->is_ajax()) {
            $id = (int)Arr::get($_POST, 'id', 0);
            $partner = ORM::factory('Partner', $id)/*->as_array()*/;
            if ($partner->loaded()) {
                $result = array();
                $result['title'] = $partner->{'title_'.I18n::$lang};
                $result['text'] = $partner->{'text_'.I18n::$lang};
                $result['site'] = $partner->site;
                $result['category'] = $partner->category->{'title_'.I18n::$lang};
                $image = $partner->photo_s;
                $result['photo']    = URL::site($image->dir.$image->name.'.'.$image->type);
                die(json_encode(array("success" =>  $result)));
            } else {
                die(json_encode(array("error" =>  'Not found')));
            }
        }
    }

}
