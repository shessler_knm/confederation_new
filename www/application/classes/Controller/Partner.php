<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Partner extends Site
{
    public function action_preview()
    {
        //список партнеров
        $this->render = false;
        $model = ORM::factory('Banner')->where('partner', '=', '2')->where('main', '=', '2')->limit(7)->order_by('priority', 'ASC')->find_all();
        $this->response->body(View::factory('partner/preview_new')->bind('model', $model));
    }

    public function action_index()
    {
        //судьи федерации
        $banner = ORM::factory('Banner')->where('partner', '=', '2');
        $partner_category_1 = $banner->where('category', '=', '1')->find_all()->as_array();
        $partner_category_2 = $banner->where('category', '=', '2')->find_all()->as_array();
        $partner_category_3 = $banner->where('category', '=', '3')->find_all()->as_array();
        $this->page->content(View::factory('partner/list')
            ->bind('partner_category_1', $partner_category_1)
            ->bind('partner_category_2', $partner_category_2)
            ->bind('partner_category_3', $partner_category_3));
    }

    public function action_banner()
    {
        //рекламный баннер
        $this->render = false;
        $model = ORM::factory('Banner')->where('partner', '=', '1')->where('main', '=', '2')->order_by(DB::expr("RAND()"))->find();
        $arr_url = parse_url($model->{'url_'.I18n::$lang}?$model->{'url_'.I18n::$lang}:$model->url_ru);
        if ($arr_url['host'] == 'confederation.kz') {
            $url = URL::site($arr_url['path']);
        } else {
            $url = $model->{'url_'.I18n::$lang}?$model->{'url_'.I18n::$lang}:$model->url_ru;
        }
        $this->response->body(View::factory('banner/preview')
                ->bind('url', $url)
                ->bind('model', $model)
        );
    }
}