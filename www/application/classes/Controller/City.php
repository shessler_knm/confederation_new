<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Almas
 * Date: 02.08.13
 * Time: 9:46
 * To change this template use File | Settings | File Templates.
 */

class Controller_City extends Site{

    public function action_source(){
        $this->render = false;
        $query = $this->request->query('q');
        I18n::$lang = $this->request->query('lang');
        $cities = ORM::factory('City')->where('title_'.I18n::$lang,'Like',"%$query%")->find_all();
        $data = array();
        $data['totalResultsCount']=$cities->count();
        foreach ($cities as $city) {
            $data['geonames'][] = array('name' => $city->title, 'sef'=>$city->sef, 'country'=>$city->country->title);
        }
        $this->response->body(json_encode($data));
    }

}