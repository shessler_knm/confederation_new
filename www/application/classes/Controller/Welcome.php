<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Site
{

    public $template = 'template';

    public function action_index()
    {
        $data_kz = array(
            'facebook'=>'https://www.facebook.com/ConfederSportKZ',
            'vk'=>'http://vk.com/confederationkz',
            'twitter'=>'https://twitter.com/ConfederSportKZ',
        );
        $data_ru = array(
            'facebook'=>'https://www.facebook.com/ConfederatSport',
            'vk'=>'http://vk.com/confederationkz',
            'twitter'=>'https://twitter.com/ConfederSport',
        );
        $data_en = array(
            'facebook'=>'https://www.facebook.com/ConfederatSport',
            'vk'=>'http://vk.com/confederationkz',
            'twitter'=>'https://twitter.com/ConfederSport',
        );
        $content = 'data_'.I18n::$lang;
        View::bind_global('social_links',$$content);
    }

    public function action_login_as()
    {
        $user=ORM::factory('User',$this->request->param('id'));
        Auth::instance()->force_login($user);
        $this->redirect('privatecoach');
    }

}
