<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Saidashev Marat
 * Date: 26.08.13
 * Time: 10:52
 * To change this template use File | Settings | File Templates.
 */

class Controller_Synchronize_City extends API_Synchronize {


    public function action_post_index() {
        if (!$this->request->post('items_available')) {
            parent::action_post_index();
        } else {
            $items_available = $this->request->post('items_available');
            $fed_id = $this->request->post('fed_id');
            $collection=null;
            $model = ORM::factory('City');

            if (is_array($items_available) && count($items_available) > 0) {
                if (count($items_available) > 1)
                    $collection=$model->where('id',DB::expr('NOT IN'),$items_available)->find_all();
                elseif (count($items_available) == 1)
                    $collection=$model->where('id','!=',$items_available[0])->find_all();
            } else {
                $collection = $model->find_all();
            }

            $collection = $this->create_collection($collection);
            $edits = $this->get_edits($model->object_name(),$fed_id);
            $collection += $edits;

            $data = array();
            if ($collection) {
                $data = $this->create_tasks($collection);
            }

            //Отправляем
            $this->response_collection($collection, $data);

            //Очищаем таблицу edits
            $this->del_edits($edits);
        }
    }

}