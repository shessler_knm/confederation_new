<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Saidashev Marat
 * Date: 26.08.13
 * Time: 10:52
 * To change this template use File | Settings | File Templates.
 */

class Controller_Synchronize_Video extends API_Synchronize {

    public function action_post_index() {
        if (!$this->request->post('items_available')) {
            parent::action_post_index();
        } else {
            $items_available = $this->request->post('items_available');
            $parent_id = $this->request->post('parent_id');
            $fed_id = $this->request->post('fed_id');
            $parent_model = $this->request->post('parent_model');
            $parent_hasmany = $this->request->post('parent_hasmany');
            $collection=null;
            $parent_model = ucfirst($parent_model); // На всякий случай.
            $model = ORM::factory($parent_model,$parent_id);

            if (is_array($items_available) && count($items_available) > 0) {
                if (count($items_available) > 1)
                    $collection=$model
                        ->$parent_hasmany
                        ->where(DB::expr('`'.$model->$parent_hasmany->object_name().'`.`id`'),DB::expr('NOT IN'),$items_available)
                        ->find_all();
                elseif (count($items_available) == 1)
                    $collection=$model
                        ->$parent_hasmany
                        ->where(DB::expr('`'.$model->$parent_hasmany->object_name().'`.`id`'),'!=',$items_available[0])
                        ->find_all();
            } else {
                $collection = $model->$parent_hasmany->find_all();
            }

            $collection = $this->create_collection($collection);
            $edits = $this->get_edits('video',$fed_id);
            $collection += $edits;
            $data = array();
            if ($collection) {
                $data = $this->create_tasks($collection);
            }

            //Отправляем
            $this->response_collection($collection, $data);

            //Очищаем таблицу edits
            $this->del_edits($edits);
        }
    }

}