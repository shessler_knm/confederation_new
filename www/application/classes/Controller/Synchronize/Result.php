<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Saidashev Marat
 * Date: 26.08.13
 * Time: 10:52
 * To change this template use File | Settings | File Templates.
 */

class Controller_Synchronize_Result extends API_Synchronize {


    public function action_post_index() {
        if (!$this->request->post('items_available')) {
            parent::action_post_index();
        } else {
            $items_available = $this->request->post('items_available');
            $parent_id = $this->request->post('parent_id');
            $fed_id = $this->request->post('fed_id');
            $parent_model = $this->request->post('parent_model');
            $parent_hasmany = $this->request->post('parent_hasmany');

            $collection=null;
            $parent_model = ucfirst($parent_model); // На всякий случай.

            $parent = ORM::factory($parent_model,$parent_id);

            // Грубо достаем рузультаты по событиям
            $events = array_keys($parent->events->find_all()->as_array('id'));

            $model = ORM::factory('Result')->where('event_id',DB::expr('IN'),$events);

            if (is_array($items_available) && count($items_available) > 0) {
                if (count($items_available) > 1)
                    $collection=$model->where('id',DB::expr('NOT IN'),$items_available)->find_all();
                elseif (count($items_available) == 1)
                    $collection=$model->where('id','!=',$items_available[0])->find_all();
            } else {
                $collection = $model->find_all();
            }

            $collection = $this->create_collection($collection);
            $edits = $this->get_edits($model->object_name(),$fed_id);
            $collection += $edits;

            $data = array();
            if ($collection) {
                $data = $this->create_tasks($collection);
            }

            //Отправляем
            $this->response_collection($collection, $data);

            //Очищаем таблицу edits
            $this->del_edits($edits);
        }
    }

}