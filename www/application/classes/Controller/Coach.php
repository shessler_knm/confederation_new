<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Coach extends Site
{

    public function before()
    {
        parent::before();
        if ($this->fed_id) {
            Register::instance()->activemenu = 30;
        } else {
            Register::instance()->activemenu = 30;
        }
    }

    public function init_langs($model = NULL, $person = NULL)
    {
        $langs = array('ru' => '/ru/', 'en' => '/en/', 'kz' => '/');
        if ($model === NULL) {
            parent::init_langs($model);
        } else {
            if ($person) {
                foreach ($langs as $lang => $val) {
                    if (empty($model->{'sef_' . $lang})) {
                        $this->array_langs[$lang] = $val . $this->fed_id . '/coach/person/' . $model->{'sef_' . I18n::$lang};
                    } else {
                        $this->array_langs[$lang] = $val . $this->request->param('fed_id') . '/coach/person/' . $model->{'sef_' . $lang};
                    }
                }
            } elseif ($this->fed_id == 'wrestling') {
                foreach ($langs as $lang => $val) {
                    if (empty($model->{'sef_' . $lang})) {
                        $this->array_langs[$lang] = $val . $this->fed_id . '/coach/' . $model->{'sef_' . I18n::$lang};
                    } else {
                        $this->array_langs[$lang] = $val . $this->request->param('fed_id') . '/coach/' . $model->{'sef_' . $lang};
                    }
                }
            }
        }
    }

    public function action_index()
    {
        $sef = $this->request->param('sef');
        $this->page->title(__('Тренерский состав'));
        $federation = $this->federation;
        $teams = $federation->teams;
        $first_team = $teams->reset(false)->order_by('id', 'ASC')->where('is_published', '!=', '2');
        $teams = $teams->find_all();
        $team = ORM::factory('Team');
        if ($this->fed_id == 'wrestling') {
            $first_team = $first_team->find();
            $this->init_langs($first_team);
            $coaches = $team->where('sef_' . I18n::$lang, '=', $sef)->find()->coaches->where('is_published', '!=', '2');
            if (!$sef || !$first_team->federation_id == $federation->id) {
                $this->redirect($this->fed_id . '/coach/' . $first_team->{'sef_' . I18n::$lang}, 301);
            }
        } else {
            $coaches = ORM::factory('Coach')->where('federation_id', '=', $federation->id)->where('is_published', '=', 1);
        }
        $pagination = $this->pagination($coaches, 'player_pagination.default');
        $page = $this->request->param('page');
        $total_pages = $pagination->total_pages;
        $this->check_pages($page, $total_pages);

        View::bind_global('total_pages', $total_pages);
        $coaches = $coaches->find_all();
        $this->page->content(View::factory('coach/list')
                ->bind('sef', $sef)
                ->bind('model', $teams)
                ->bind('coaches', $coaches)
                ->bind('pagination', $pagination)
        );
    }

    public function action_preview()
    {
        $this->render = false;
        $coach = $this->federation->coaches->order_by('lastname_ru', 'ASC')->where('is_published', '!=', '2');
        $coach = $coach->limit(5)->find_all();
        $this->response->body(View::factory('coach/preview')
            ->bind('model', $coach));
    }

    public function action_person()
    {
        //вьюха одного тренера
        $this->page->content(View::factory('coach/view')
            ->bind('model', $model));
        $sef = $this->request->param('sef');
        $model = ORM::factory('Coach')->where('sef_' . i18n::lang(), '=', $sef)->find();
        $this->init_langs($model, true);
        $this->page->title($model->get_full_name() . ' - ' . __('Тренеры') . ' - ' . $this->federation->{'title_' . I18n::$lang});
        $this->page->meta(Text::limit_words($model->biography, 150), 'description');
        if (!$model->loaded()) {
            foreach ($this->array_langs as $lang => $url) {
                $model = ORM::factory('Coach')->where('sef_' . $lang, '=', $sef)->find();
                if ($model->loaded()) {
                    throw new HTTP_Exception_Translate();
                }
            }
            throw new HTTP_Exception_404('Страница не найдена');
        }
    }

    public function action_translit_message()
    {
        $this->render = false;
        $langs = array('ru' => '/ru/', 'en' => '/en/', 'kz' => '/');
        $this->write_lst_uri = false;
        $this->response->status(404);
        $this->session OR $this->session = Session::instance();
        $last_uri = '/' . ltrim($this->session->get('last_uri', URL::base()), '/');
        $model = ORM::factory('Coach');
        preg_match('"([^/]*$)"', $last_uri, $match);
        foreach ($langs as $lang => $val) {
            if ($model->where('sef_' . $lang, '=', $match[0])->find()->loaded()) {
                break;
            }
        }
        preg_match('"([^/]+)(?=/[^/]*$)"', $last_uri, $match);
        if ($match[0] == 'person') {
            $url = $this->fed_id . '/coach/person';
        } else {
            $url = $this->fed_id . '/coach';
        }
        Helper::currentURI();
        $view = View::factory('errors/translite')
            ->bind('url', $url)
            ->bind('model', $model)
            ->bind('model_name', $model_name)
            ->bind('last_uri', $last_uri);
        $this->response->body($view);
    }

}