<?php

class Controller_User extends Site
{
    public function scripts()
    {
        return parent::scripts() + array(
            'storage' => '/js/lib/storage.js',
        );
    }

    public function before()
    {
        parent::before();
        if (in_array($this->request->action(), array('reg', 'login'))) {
            $this->page->script('ulogin', 'http://ulogin.ru/js/ulogin.js');
        }
        if (in_array($this->request->action(), array('reg'))) {
            $this->page->script('passfield', 'js/passfield.min.js');
            $this->page->style('passfield', 'css/passfield.min.css');
        }
    }

    public static function after_reg($username, $hash, $email)
    {

    }

    public function action_reg()
    {
        $this->page->content(
            View::factory('user/reg')
                ->bind('recaptcha', $recaptcha)
                ->bind('ulogin', $ulogin)
        );
        $this->page->title(__('Регистрация пользователя'));
        $recaptcha = Recaptcha::instance();
        $model = ORM::factory('User');
        $ulogin = ULogin::widget_ulogin();
        $valid = Validation::factory($this->request->post())
            ->rules('password', array(
                array('not_empty'),
                array('min_length', array(':value', 6)),
            ))
            ->rules('password_confirm', array(
                array('matches', array(':validation', ':field', 'password'))

            ));
//        $valid = $recaptcha->rules($valid);
//        $valid->label('recaptcha_response_field', 'Капча');
        $valid->label('password', 'Пароль');
        $valid->label('password_confirm', 'подтверждение пароля');


        if ($this->request->post('submit')) {
            try {
                $post = $this->request->post();
                $str1 = implode(',', $post);
                $str2 = (string)$post;
                $hash = md5(rand(9999, getrandmax()));
                $model->a_hash = $hash;
                $model->reg_date = date('Y-m-d H:i:s');
                $model->values($this->request->post());
//                $langs = ORM::factory('lang')->select_options('name','id');
//                $model->lang = (array_key_exists(I18n::lang(), $langs)) ? $langs[I18n::lang()] : current($langs);

                if ($model->save()) {
                    Model_User::after_reg($this->request->post('username'), $hash, $this->request->post('email'));
                    $session = Session::instance();
                    $auth_redirect = $session->get('auth_redirect', '');
                    $session->delete('auth_redirect');
//                    $this->redirect($auth_redirect,301);
                    $this->page->content(
                        View::factory('message_page', array(
                            'msg' => __('На указанный адрес электронной почты выслано письмо для подтверждения регистрации')
                        ))
                    );
                }
            } catch (ORM_Validation_Exception $e) {
                $this->errors = $e->errors('validation');
            }
        }
        if (uLogin::check_auth()) {
            if (Session::instance()->get('after_login')) {
//                $this->redirect(Session::instance()->get('after_login'));
                $session = Session::instance();
                $auth_redirect = $session->get('auth_redirect', '');
                $session->delete('auth_redirect');
                Request::initial()->redirect($auth_redirect, 301);
            } else {
                $this->redirect($this->last_uri, 301);
            }

        }
    }

    public function action_login()
    {
//        $this->render = false;
        if (Auth::instance()->logged_in()) {
            $this->redirect('confederation', 301);
//            Request::initial()->redirect('');
        } else {

            $ulogin = ULogin::widget_ulogin();
            if (Arr::get($_POST, 'submit')) {
                $username = $this->request->post('username');
                $password = $this->request->post('password');
                $remember = (bool)$this->request->post('remember');
                if (Auth::instance()->login($username, $password, $remember)) {
//                    if (Session::instance()->get('after_login')) {
//                        $this->redirect(Session::instance()->get('after_login'), 301);
//                    } else {
//                        $this->redirect($this->last_uri, 301);
//                    }
                    $this->redirect($this->last_uri);
                } else {
                    $this->errors['password'] = __('Неверная пара логин/пароль');
                }
            }
            if (ULogin::check_auth()) {
                if (Session::instance()->get('after_login')) {
                    $this->redirect(Session::instance()->get('after_login'), 301);
                } else {
                    $this->redirect($this->last_uri, 301);
                }
            }
        }
        $this->page->content(View::factory('user/login')
            ->bind('error', $errors)
            ->bind('ulogin', $ulogin));
    }

    public function action_activation()
    {
        $this->page->title(__('Активация'));
        $model = ORM::factory('User')
            ->where('id', '=', (int)$this->request->param('id'))
            ->where('a_hash', '=', $this->request->query('hash'))
            ->find();
        if ($model->a_hash && $model->loaded()) {
            $model->roles->where('role_id', '=', 1)->find();
            if (!$model->roles->loaded()) {
                $model->add('roles', ORM::factory('role', 1));
                $model->a_hash = '';
                $model->save();
            } else {
                $this->request->body(URL::base());
            }
        } else {
            $this->request->body(URL::base());
        }
        if ($model->loaded()) {
            $this->page->content(
                View::factory('message_page', array(
                    'msg' => __('Активация прошла успешно. Пройдите по данной :login, чтобы авторизоваться', array(':login' => HTML::anchor(URL::site(I18n::$lang . '/user/login'), __('ссылке'))))
                ))
            );
        } else {
            throw new HTTP_Exception_404();
        }
    }

    public function action_source()
    {
        $this->render = false;
        $query = $this->request->query('query');
        $users = ORM::factory('User')
            ->having('username', 'LIKE', "%$query%")
            ->find_all();
        $data = array();
        foreach ($users as $user) {
            $data[] = array('name' => $user->username, 'id' => $user->id);
        }
        $this->response->body(json_encode($data));
    }

    public function action_logout()
    {
        parent::action_logout();
    }

    public function action_passremind()
    {
        $this->page->content(
            View::factory('user/passremind')->bind('recaptcha', $recaptcha)
        );
        $recaptcha = Recaptcha::instance();
        $model = ORM::factory('User');

        if ($this->request->post('submit')) {
            $validation = Validation::factory($this->request->post())
                ->rules('email', array(
                    array('not_empty'),
                    array('email')
                ));
            $validation = $recaptcha->rules($validation);
            $validation->label('recaptcha_response_field', 'Капча');
            if ($validation->check()) {
                $model->where('email', '=', $this->request->post('email'))->find();
                if ($model->loaded()) {
                    $password = rand(9999, getrandmax());
                    $model->password = $password;
                    if ($model->save()) {
                        Model_User::_passremind($model->id, $password);
                        $this->page->content(
                            View::factory('message_page', array(
                                'msg' => __('На указанный адрес электронной почты выслано письмо с инструкцией по смене пароля.')
                            ))
                        );
                    }
                } else {
                    $this->errors['email'] = __('Пользователя с таким e-mail у нас не зарегистрировано!');
                }
            } else {
                $this->errors = $validation->errors('validation');
            }
        }
    }

    public function action_delivery() {
        if ($this->request->is_ajax()) {
            $user = Auth::instance()->get_user();
            $user->delivery();
            die(
                json_encode(
                    array(
                        'success' => ($user->delivery == 1) ? __("Отписаться от рассылки") : __("Подписаться на рассылку")
                    )
                )
            );
        }
    }

}
