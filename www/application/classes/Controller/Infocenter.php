<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Infocenter extends Site
{
    public function before()
    {
        parent::before();
        Register::instance()->activemenu = 11;
    }

    public function scripts()
    {
        return parent::scripts() + array(
            'comments' => 'js/comments.js',
            'gallery' => 'js/gallery.js'
        );
    }

    public function init_langs($model = NULL, $action = NULL)
    {
        $langs = array('ru' => '/ru/', 'en' => '/en/', 'kz' => '/');
        if ($model === NULL) {
            parent::init_langs($model);
        } else {
            foreach ($langs as $lang => $val) {
                if (empty($model->{'sef_' . $lang})) {
                    $this->array_langs[$lang] = $val . 'confederation/infocenter/' . $action . '/' . $model->{'sef_' . I18n::$lang};
                } else {
                    $this->array_langs[$lang] = $val . 'confederation/infocenter/' . $action . '/' . $model->{'sef_' . $lang};
                }
            }
        }
    }

    public function action_interview()
    {
        $sef = $this->request->param('sef');
        if ($sef) {
            $model = ORM::factory('News')->where('sef_' . i18n::lang(), '=', $sef)->find();
            if (!$model->loaded()) {
                foreach ($this->array_langs as $lang => $url) {
                    $model = ORM::factory('News')->where('sef_' . $lang, '=', $sef)->find();
                    if ($model->loaded()) {
                        throw new HTTP_Exception_Translate();
                    }
                }
                throw new HTTP_Exception_404('Страница не найдена');
            }
            if (!$model->{'text_' . I18n::$lang}) {
                throw new HTTP_Exception_404();
            }
            $this->init_langs($model);
            $this->view_counter($model);
            $this->page->title($model->title . ' - ' . __('Интервью') . ' - ' . __('Конфедерация спортивных единоборств и силовых видов спорта'));
            $this->page->meta(Text::limit_chars($model->text, 200), 'description');
            $this->page->content(View::factory('infocenter/interview/view')
                ->bind('model', $model));
        } else {
            $this->add_crumb(__('Интервью'), '');
            $this->page->title(__('Интервью') . ' - ' . __('Конфедерация спортивных единоборств и силовых видов спорта'));
            $this->page->content(View::factory('infocenter/interview/list')
                ->set('counter', 1)
                ->bind('pagination', $pagination)
                ->bind('federations', $federations)
                ->bind('model', $collection)
                ->bind('sport_types', $sport_types)
                ->bind('select_id', $select_id));
            $select_id = $this->request->query('sport_type');
            if ($select_id == 'all') {
                $this->redirect(URL::site(I18N::$lang . '/confederation/interview'), 301);
            }
            $select_id = $select_id ? $select_id : 'all';
            $model = ORM::factory('News');
            $category = ORM::factory('Category', array('slug' => 'interviews'));
            $model->join('news_categories', 'left')->on('news_categories.news_id', '=', 'news.id');
            $model->where('news_categories.category_id', '=', $category->id);
            $model->where('title_' . I18n::$lang, '!=', '')->where('text_' . I18n::$lang, '!=', '');
            $sport_types = array('all' => __('Любой')) + ORM::factory('Category')->get_sef_sports()->select_options('id', 'title_' . I18n::$lang);
            if (!array_key_exists($select_id, $sport_types) && $select_id) {
                throw new HTTP_Exception_404();
            }
            if ($select_id != 'all') {
                $model->join('news_sports', 'left')->on('news_sports.news_id', '=', 'news.id');
                $model->where('news_sports.category_id', '=', $select_id);
            }
            $pagination = $this->pagination($model);
            $total_pages = $pagination->total_pages;
            if ($this->check_pages($this->request->param('page'), $total_pages, $select_id != 'all' ? true : false)) {
                $this->redirect(URL::site('confederation/interview') . '?sport_type=' . $select_id, 301);
            }

            View::bind_global('total_pages', $total_pages);
            $collection = $model->order_by('date', 'DESC')->find_all();
            $federations = array();
            foreach ($collection as $row) {
                $federations[$row->id] = $row->federations->find_all()->as_array();
            }
        }
    }

    public function action_gallery()
    {
        $this->page->script('jquery.fancybox', "/js/fancybox/jquery.fancybox.js");
        $this->page->script('jquery.fancybox.pack', "/js/fancybox/jquery.fancybox.pack.js");
        $this->page->script('jquery.fancybox-buttons', "/js/fancybox/jquery.fancybox-buttons.js");
        $this->page->script('jquery.fancybox-thumbs', "/js/fancybox/jquery.fancybox-thumbs.js");
        $this->page->script('jquery.fancybox-media', "/js/fancybox/jquery.fancybox-media.js");
        $sef = $this->request->param('sef');
        $type = $this->request->param('type');
        if ($sef && !$type) {
            $model = ORM::factory('Photovideo')->where('sef_' . i18n::lang(), '=', $sef)->find();
            if (!$model->loaded()) {
                foreach ($this->array_langs as $lang => $url) {
                    $model = ORM::factory('Photovideo')->where('sef_' . $lang, '=', $sef)->find();
                    if ($model->loaded()) {
                        throw new HTTP_Exception_Translate();
                    }
                }
                throw new HTTP_Exception_404('Страница не найдена');
            }
            if ($model->type == 2) {
                if (!$model->{'content_' . I18n::$lang}) throw new HTTP_Exception_404('Страница не найдена');
                $this->view_counter($model);
                $this->init_langs($model, 'gallery');
                $this->page->title($model->title.' - '.__('Фото и Видео').' - '.__('Конфедерация спортивных единоборств и силовых видов спорта'));
                $this->page->meta(Text::limit_chars($model->description,200), 'description');
                $this->add_crumb(__('Видео'), URL::site('/infocenter/gallery'));
                $this->add_crumb($model->title, '');
                $this->page->content(View::factory('album/video')
                    ->bind('model', $model));
            } else {
                $this->page->title($model->title.' - '.__('Фото и Видео').' - '.__('Конфедерация спортивных единоборств и силовых видов спорта'));
                $this->page->meta(Text::limit_chars($model->description,200), 'description');
                $this->view_counter($model);
                $this->init_langs($model, 'gallery');
                $photoSelected = ORM::factory('Photo')->where('album_id', '=', $model->id)->find();
                $photos = ORM::factory('Photo')->where('album_id', '=', $model->id)->find_all();
                $this->page->content(View::factory('album/photo')
                        ->bind('photos', $photos)
                        ->bind('photoSelected', $photoSelected)
                        ->bind('model', $model)
                );
                $this->add_crumb(__('Фото'), URL::site('/infocenter/gallery'));
                $this->add_crumb($model->title, '');
            }
        } else {
            if($type && $type == 'video') {
                $this->page->title(__('Видео').' - '.__('Конфедерация спортивных единоборств и силовых видов спорта'));
            } elseif ($type && $type == 'photo') {
                $this->page->title(__('Фото').' - '.__('Конфедерация спортивных единоборств и силовых видов спорта'));
            } else {
                $this->page->title(__('Фото и Видео').' - '.__('Конфедерация спортивных единоборств и силовых видов спорта'));
            }
            $model = ORM::factory('Photovideo');
            $photovideo = $model
                ->where('title_' . I18n::$lang, '!=', '');
            if ($type == 'video') {
                $photovideo = $photovideo->where('type', '=', 2);
            } elseif ($type = 'photo') {
                $photovideo = $photovideo->where('type', '=', 1);
            }
//                ->where('type', '=', 1)
//                ->where('type', '=', 2)
            $photovideo = $photovideo->where('content_' . I18n::$lang, '!=', '')->order_by('date', 'DESC');
            $pagination = $this->pagination($photovideo);
            $page = $this->request->param('page');
            $total_pages = $pagination->total_pages;
            $model = $photovideo->find_all();
            $this->check_pages($page, $total_pages);

            View::bind_global('total_pages', $total_pages);



            $photos = array();
            $federations = array();
            foreach ($model as $album) {
                $federations[$album->id] = $album->federations->find_all();
                if ($album->type == 1) {
                    $jacket = ORM::factory('Photo')
                        ->where('album_id', '=', $album->id)
                        ->where('is_published', '=', '1')
                        ->where('jacket', '=', 1)
                        ->find();
                    if ($jacket->loaded()) {
                        $photos[$album->id] = $jacket;
                    } else {
                        $photos[$album->id] = ORM::factory('Photo')->where('album_id', '=', $album->id)->find();
                    }
                }
            }

            $this->add_crumb(__('Фото и Видео'), '');
            $this->page->content(View::factory('album/index')
                    ->set('counter', 0)
                    ->set('federation_counter', 0)
                    ->bind('model', $model)
                    ->bind('federations', $federations)
                    ->bind('photos', $photos)
                    ->bind('pagination', $pagination)
                    ->bind('type', $type)
            );
        }
    }

    public function  action_infoformedia()
    {
        $model = ORM::factory('Page', array('sef' => 'infocenter/infoformedia'));
        if (!($model->loaded()))
            throw new HTTP_Exception_404('Страница не найдена');
        $this->page->content(View::factory('infocenter/infoformedia')
            ->bind('model', $model));
    }

    public function action_infographic()
    {
        $this->page->script('jquery.fancybox', "/js/fancybox/jquery.fancybox.js");
        $this->page->script('jquery.fancybox.pack', "/js/fancybox/jquery.fancybox.pack.js");
        $this->page->script('jquery.fancybox-buttons', "/js/fancybox/jquery.fancybox-buttons.js");
        $this->page->script('jquery.fancybox-thumbs', "/js/fancybox/jquery.fancybox-thumbs.js");
        $this->page->script('jquery.fancybox-media', "/js/fancybox/jquery.fancybox-media.js");
        $this->page->script('infographic', "/js/infographic.js");
        $this->page->title(__('Инфографика'));
        $model = ORM::factory('Infographic');
        $pagination = $this->pagination($model);
        $page = $this->request->param('page');
        $total_pages = $pagination->total_pages;
        $this->check_pages($page, $total_pages);
        View::bind_global('total_pages', $total_pages);
        $model = $model->find_all();
        $this->page->content(View::factory('infocenter/infographic')
            ->bind('pagination', $pagination)
            ->bind('model', $model));
        $this->add_crumb(__('Инфографика'), '');
    }

    public function action_pressservice()
    {
        $sef = $this->request->param('sef');
        if ($sef) {
            $this->page->content(View::factory('infocenter/pressservice/view')
                ->bind('model', $model));
            $model = ORM::factory('News');
            $model->where('sef_' . i18n::lang(), '=', $sef)->find();
            if (!$model->loaded()) {
                foreach ($this->array_langs as $lang => $url) {
                    $model = ORM::factory('News')->where('sef_' . $lang, '=', $sef)->find();
                    if ($model->loaded()) {
                        throw new HTTP_Exception_Translate();
                    }
                }
                throw new HTTP_Exception_404('Страница не найдена');
            }
            if (!$model->{'text_' . I18n::$lang}) {
                throw new HTTP_Exception_404();
            }
            $this->init_langs($model, 'pressservice');
            $this->page->title($model->title.' - '.__('Пресс-служба').' - '.__('Конфедерация спортивных единоборств и силовых видов спорта'));
            $this->page->meta(Text::limit_chars($model->text,200), 'description');
        } else {
            $this->add_crumb(__('Пресс-служба'), '');
            $this->page->title(__('Пресс-служба').' - '.__('Конфедерация спортивных единоборств и силовых видов спорта'));
            $this->page->content(View::factory('infocenter/pressservice/list')
                ->bind('model', $q)
                ->bind('pagination', $pagination));
            $model = ORM::factory('News');
            $cat = ORM::factory('Category')->where('slug', '=', 'pressservice')->find();
            $model->join('news_categories', 'left')->on('news_categories.news_id', '=', 'news.id');
            $model->where('news_categories.category_id', '=', $cat->id)->where('title_' . I18n::$lang, '!=', '')->where('text_' . I18n::$lang, '!=', '');
            $pagination = $this->pagination($model);
            $page = $this->request->param('page');
            $total_pages = $pagination->total_pages;
            $this->check_pages($page, $total_pages);
            $q = $model->order_by('date', 'DESC')->find_all();
        }
    }

    public function action_aboutus()
    {
        $sef = $this->request->param('sef');
        if ($sef) {
            $this->page->content(View::factory('infocenter/aboutus/view')
                ->bind('model', $model));
            $model = ORM::factory('News')->where('sef_' . i18n::lang(), '=', $sef)->find();
            if (!$model->loaded()) {
                foreach ($this->array_langs as $lang => $url) {
                    $model = ORM::factory('News')->where('sef_' . $lang, '=', $sef)->find();
                    if ($model->loaded()) {
                        throw new HTTP_Exception_Translate();
                    }
                }
                throw new HTTP_Exception_404('Страница не найдена');
            }
            if (!$model->{'text_' . I18n::$lang}) {
                throw new HTTP_Exception_404();
            }
            $this->init_langs($model, 'aboutus');
            $this->page->title($model->title.' - '.__('СМИ о нас').' - '.__('Конфедерация спортивных единоборств и силовых видов спорта'));
            $this->page->meta(Text::limit_chars($model->text,200), 'description');
        } else {
            $this->add_crumb(__('СМИ о нас'), '');
            $this->page->title(__('СМИ о нас').' - '.__('Конфедерация спортивных единоборств и силовых видов спорта'));
            $this->page->content(View::factory('infocenter/aboutus/list')
                ->bind('model', $q)
                ->bind('pagination', $pagination));
            $model = ORM::factory('News');
            $cat = ORM::factory('Category')->where('slug', '=', 'mediaaboutus')->find();
            $model->join('news_categories', 'left')->on('news_categories.news_id', '=', 'news.id');
            $model->where('news_categories.category_id', '=', $cat->id)->where('title_' . I18n::$lang, '!=', '')->where('text_' . I18n::$lang, '!=', '');
            $pagination = $this->pagination($model);
            $page = $this->request->param('page');
            $total_pages = $pagination->total_pages;
            $this->check_pages($page, $total_pages);
            View::bind_global('total_pages', $total_pages);
            $q = $model->find_all();
        }
    }

    public function action_fact()
    {
        $sef = $this->request->param('sef');
        if ($sef) {
            $this->page->content(View::factory('infocenter/facts/view')
                ->bind('model', $model));
            $model = ORM::factory('Fact');
            $model->where('sef_' . i18n::lang(), '=', $sef)->find();
            if (!$model->loaded()) {
                foreach ($this->array_langs as $lang => $url) {
                    $model = ORM::factory('Fact')->where('sef_' . $lang, '=', $sef)->find();
                    if ($model->loaded()) {
                        throw new HTTP_Exception_Translate();
                    }
                }
                throw new HTTP_Exception_404('Страница не найдена');
            }
            if (!$model->{'text_' . I18n::$lang}) {
                throw new HTTP_Exception_404();
            }
            $this->init_langs($model, 'fact');
            $this->page->title($model->title.' - '.__('Интересные факты').' - '.__('Конфедерация спортивных единоборств и силовых видов спорта'));
            $this->page->meta(Text::limit_chars($model->text,200), 'description');
        } else {
            $this->page->title(__('Интересные факты').' - '.__('Конфедерация спортивных единоборств и силовых видов спорта'));
            $this->add_crumb(__('Интересные факты'), '');
            $this->page->content(View::factory('infocenter/facts/list')
                ->bind('model', $q)
                ->bind('pagination', $pagination));
            $model = ORM::factory('Fact')->where('title_' . I18n::$lang, '!=', '')->where('text_' . I18n::$lang, '!=', '');
            $pagination = $this->pagination($model);
            $page = $this->request->param('page');
            $total_pages = $pagination->total_pages;
            $this->check_pages($page, $total_pages);
            View::bind_global('total_pages', $total_pages);
            $q = $model->find_all();
        }
    }

    public function action_translit_message()
    {
        $this->render = false;
        $langs = array('ru' => '/ru/', 'en' => '/en/', 'kz' => '/');
        $this->write_lst_uri = false;
        $this->response->status(404);
        $this->session OR $this->session = Session::instance();
        $last_uri = '/' . ltrim($this->session->get('last_uri', URL::base()), '/');
        preg_match('"([^/]+)(?=/[^/]*$)"', $last_uri, $match);
        $model_name = $match[0];
        switch ($model_name) {
            case 'gallery' :
                $model = ORM::factory('Photovideo');
                break;
            case 'pressservice':
            case 'aboutus':
                $model = ORM::factory('News');
                break;
            default :
                $model = ORM::factory(ucfirst($model_name));
        }
        preg_match('"([^/]*$)"', $last_uri, $match);
        foreach ($langs as $lang => $val) {
            if ($model->where('sef_' . $lang, '=', $match[0])->find()->loaded()) {
                break;
            }
        }
        $url = 'confederation/infocenter/' . $model_name;
        Helper::currentURI();
        $view = View::factory('errors/translite')
            ->bind('url', $url)
            ->bind('model', $model)
            ->bind('model_name', $model_name)
            ->bind('last_uri', $last_uri);
        $this->response->body($view);
    }

    public function action_gallery_preview()
    {
        $this->render = false;
        $album = ORM::factory('Photovideo')->where('type', '=', '1')->where('title_' . I18n::$lang, '!=', '')->order_by('id', 'DESC')->limit(1)->find_all();
        $photo = array();
        foreach ($album as $row) {
            $jacket = ORM::factory('Photo')->where('album_id', '=', $row->id)->where('is_published', '=', '1')->where('jacket', '=', 1)->find();
            if ($jacket->loaded()) {
                $photo[$row->id] = array($jacket);
            } else {
                $photo[$row->id] = ORM::factory('Photo')->where('album_id', '=', $row->id)->order_by('id', 'ASC')->limit(1)->find_all();
            }
        }
        $video = ORM::factory('Photovideo')->where('type', '=', '2')->order_by('id', 'DESC')->limit(1)->find_all();
        $this->response->body(View::factory('album/preview')
                ->set('counter', 0)
                ->bind('album', $album)
                ->bind('photo', $photo)
                ->bind('video', $video)
        );
    }

    public function action_rss()
    {
        $this->render = false;
        $this->response->headers('content-type', 'application/rss+xml');
        $lang = I18n::$lang;
        $info = array(
            'title' => 'Интересные факты',
            'description' => 'Интересные факты от сайта www.confederation.kz',
            'link' => 'http://www.confederation.kz/ru/infocenter/rss',
            'pubDate' => time()
        );
        $facts = ORM::factory('Fact')
            ->where('is_published', '=', '1')
            ->where('title_' . I18n::$lang, '!=', '')
            ->where('text_' . I18n::$lang, '!=', '')
            ->order_by('date', 'DESC')
            ->limit(50);
        $items = array();
        foreach ($facts->find_all() as $row) {
            $url = URL::site('ru/confederation/infocenter/fact/' . $row->{'sef_' . I18n::$lang});
//            $url='confederation/infocenter/fact/'.$row->{'sef_'.I18n::$lang};
            $items[] = array(
                'title' => ORM::clear_html($row->{'title_' . $lang}),
                'description' => Text::limit_chars(strip_tags($row->{'text_' . $lang}), 155),
                'image' => $row->photo_s->url_image(NULL, NULL, 'http'),
                'content' => strip_tags($row->{'text_' . $lang}),
                'link' => $url,
                'pubDate' => $row->date,
            );
        }
        echo Feed::create($info, $items);
    }
}