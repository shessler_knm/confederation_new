<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Translation extends Site
{
    public function action_preview()
    {
        $this->render = false;
        $view = View::factory('translation/preview')->bind('model', $model);
        $model = ORM::factory('Translation')->where('is_published', '=', 1)->find_all();
        if ($model->count() > 0) {
            $this->response->body($view);
        }
    }
}