<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Event extends Site
{
    public function before()
    {
        parent::before();
        if ($this->fed_id) {
            Register::instance()->activemenu = 27;
        } else {
            Register::instance()->activemenu = 19;
        }
    }


    public function scripts()
    {
        return parent::scripts() + array(
            'eventroute' => 'js/eventroute.js'
        );
    }

    public function init_langs($model = NULL)
    {
        $langs = array('ru' => '/ru/', 'en' => '/en/', 'kz' => '/');
        if ($model === NULL) {
            parent::init_langs($model);
        } else {
            if ($this->fed_id) {
                foreach ($langs as $lang => $val) {
                    if (empty($model->{'sef_' . $lang})) {
                        $this->array_langs[$lang] = $val . $this->fed_id . '/event/' . $model->{'sef_' . I18n::$lang};
                    } else {
                        $this->array_langs[$lang] = $val . $this->fed_id . '/event/' . $model->{'sef_' . $lang};
                    }
                }
            } else {
                foreach ($langs as $lang => $val) {
                    if (empty($model->{'sef_' . $lang})) {
                        $this->array_langs[$lang] = $val . 'confederation/event/' . $model->{'sef_' . I18n::$lang};
                    } else {
                        $this->array_langs[$lang] = $val . 'confederation/event/' . $model->{'sef_' . $lang};
                    }
                }
            }
        }
    }

    public function action_xmlgeoobject()
    {
        $this->render = FALSE;
        $id = $this->request->query('id');
        $points = ORM::factory('Event', array('id' => $id));
        $this->response->headers('Content-type', 'text/xml');
        $this->response->body(View::factory('events/xmlgeoobject')
            ->bind('point', $points)
            ->bind('id', $id));
    }

    public function action_index()
    {

        $this->add_crumb(__('Календарь'), '');
        $this->page->content(View::factory('events/list')
            ->bind('pagination', $pagination)
            ->bind('year', $year)
            ->bind('month', $month)
            ->bind('avail_months', $avail_months)
            ->bind('differences', $differences)
            ->bind('month_id', $month_id)
            ->bind('city_id', $city_id)
            ->bind('sport_id', $sport_id)
            ->bind('count', $count)
            ->bind('city', $cities)
            ->bind('city_array', $city_array)
            ->bind('sport_array', $sport_array)
            ->bind('year_array', $year_array)
            ->bind('model', $model));
        $year = (int)($this->request->param('id'));
        $page = $this->request->query('page');
        if ($page == 1) {
            $this->redirect(Request::initial()->uri(), 301);
        }
        $year_array = array();
        $differences = array();
        $this->page->title(__('Календарь') . ' - ' . $this->federation ? $this->federation->{'title_' . I18n::$lang} : __('Конфедерация спортивных единоборств и силовых видов спорта'));
        $cities = ORM::factory('City');
        $sports = ORM::factory('Category');
        $month = array();
        $city_array = array();
        $avail_months = array();
        $month_id = $this->request->param('id2');
        $city_id = $this->request->param('city');
        $sport_id = $this->request->param('sport');
        if ($this->request->post('city')) {
            $city_title = $this->request->post('city');
            $city_id = ORM::factory('City')->where('title_ru', 'LIKE', $city_title . '%')->or_where('title_kz', 'LIKE', $city_title . "%")
                ->or_where('title_en', 'LIKE', $city_title . '%')->find()->sef;
            if (!$city_id) {
                $city_id = 'all';
            }

            if (!$year || !$month_id || !$city_id || !$sport_id) {
                if (!$year) {
                    $year = date('Y');
                }
                if (!$month_id) {
                    $month_id = date('n');
                }
                if (!$city_id) {
                    $city_id = "all";
                }
                if (!$sport_id) {
                    $sport_id = "all";
                }
            }

            $this->redirect(URL::site(Route::get(
                Route::name(Request::current()->route())
            )->uri(
                    array(
                        'id' => $year,
                        'id2' => $month_id,
                        'city' => $city_id,
                        'sport' => $sport_id,
                        'fed_id' => $this->fed_id,
                        'controller' => 'event',
                    )
                )), 301);
        }
        $sport_array['all'] = __('Все виды спорта');
        $city_array['all'] = __('Все города');
        foreach ($cities->reset()->find_all() as $city) {
            $city_array[$city->sef] = $city->title;
        }
        if (!$year || !$month_id || !$city_id || !$sport_id) {
            if (!$year) {
                $year = date('Y');
            }
            if (!$month_id) {
                $month_id = date('n');
            }
            if (!$city_id) {
                $city_id = "all";
            }
            if (!$sport_id) {
                $sport_id = "all";
            }
            $this->redirect(URL::site(Route::get(
                Route::name(Request::current()->route())
            )->uri(
                    array(
                        'id' => $year,
                        'id2' => $month_id,
                        'city' => $city_id,
                        'sport' => $sport_id,
                        'fed_id' => $this->fed_id,
                        'controller' => 'event',
                    )
                )), 301);
        }

        if ($this->fed_id) {
            $model = $this->federation
                ->events
                ->where('is_published', '!=', '2');
            $check_model = $this->federation
                ->events
                ->where('is_published', '!=', '2')->where(DB::expr("DATE_FORMAT(date_begin,'%Y')"), '=', $year)->find();
            $a_years = $this->federation->events->where('is_published', '!=', '2')->order_by('date_begin', 'ASC')->find_all();
        } else {
            $model = ORM::factory('Event')
                ->where('is_published', '!=', '2');
            $check_model = ORM::factory('Event')
                ->where('is_published', '!=', '2')->where(DB::expr("DATE_FORMAT(date_begin,'%Y')"), '=', $year)->find();
            $a_years = ORM::factory('Event')->where('is_published', '!=', '2')->order_by('date_begin', 'ASC')->find_all();
        }

        if (!$check_model->loaded()) {
            throw new HTTP_Exception_404();
        }

        foreach ($a_years as $a_year) {
            $a_year = Date::formatted_time($a_year->date_begin, 'Y');
            if (!in_array($a_year, $year_array)) {
                if ($a_year < $year) {
                    $differences[0] = $year - $a_year;
                } elseif ($a_year > $year && Arr::get($differences, 1, false) == false) {
                    $differences[1] = $a_year - $year;
                }
                $year_array[] = $a_year;
            }
        }

        $model->where(DB::expr("DATE_FORMAT(date_begin,'%Y')"), '=', $year);
        if (!($month_id == 'all')) {
            $model->where(DB::expr("DATE_FORMAT(date_begin,'%m')"), '=', intval($month_id));
        }
        if (!($city_id == 'all') && (array_key_exists($city_id, $city_array))) {
            $model->where('city_id', '=', $cities->reset(false)->where('sef', '=', $city_id)->find()->id);
        } else {
            $city_id = 'all';
        }
        if (!($sport_id == 'all')) {
            $model->join('events_categories', 'left')->on('events_categories.event_id', '=', 'event.id');
            $model->where('events_categories.category_id', '=', $sport_id);
        }
        $count = $model->reset(false)->count_all();
        $model = $model
            ->where('title_' . I18n::$lang, '!=', '')//            ->where('text_' . I18n::$lang, '!=', '')
        ;
        $pagination = $this->pagination($model, 'event_pagination.default');
        $page = $this->request->query('page');
        $total_pages = $pagination->total_pages;
        $this->check_pages($page, $total_pages);
        View::bind_global('total_pages', $total_pages);
        $collection = $model->order_by('date_begin', 'ASC')->find_all();
        if ($count > 0) {
            foreach ($collection as $row) {
                $m = Date::formatted_time($row->date_begin, 'm');
                $avail_months[$m][] = $row;
            }

        }
        foreach ($sports->get_sef_sports()->find_all() as $row) {
            $sport_array[$row->id] = $row->title;
        }
        $month['all'] = __('Весь год');
        $month[1] = __('Январь');
        $month[2] = __('Февраль');
        $month[3] = __('Март');
        $month[4] = __('Апрель');
        $month[5] = __('Май');
        $month[6] = __('Июнь');
        $month[7] = __('Июль');
        $month[8] = __('Август');
        $month[9] = __('Сентябрь');
        $month[10] = __('Октябрь');
        $month[11] = __('Ноябрь');
        $month[12] = __('Декабрь');


    }

    public function action_preview()
    {
        //список событий на главной
        if ($this->request->query('query')) {
            $this->render = false;
            if ($this->fed_id) {
                $model = $this->federation
                    ->events
                    ->where('is_published', '!=', '2')
                    ->where('date_end', '>=', date('Y.m.d H:i:s'))
                    ->where('title_' . I18n::$lang, '!=', '')
                    ->where('rio', '=', 0)
                    ->order_by('important', 'DESC')
                    ->order_by('date_begin', 'ASC')
                    ->limit(4)
                    ->find_all();
            } else {
                $model = ORM::factory('Event')
                    ->where('is_published', '!=', '2')
                    ->where('title_' . I18n::$lang, '!=', '')
                    ->where('date_end', '>=', date('Y.m.d H:i:s'))
                    ->where('rio', '=', 0)
                    ->order_by('important', 'DESC')
                    ->order_by('date_begin', 'ASC')
                    ->limit(4)
                    ->find_all();
                $federations = array();
                foreach ($model as $row) {
                    $federations[$row->id] = $row->federations->find_all();
                }
            }
            $this->response->body(View::factory('events/preview')
                    ->bind('model', $model)
                    ->bind('federations', $federations)
            );
        }
    }

    public function action_preview_rio()
    {
        //список событий на главной
        if ($this->request->query('query')) {
            $this->render = false;
            if ($this->fed_id) {
                $model = $this->federation
                    ->events
                    ->where('is_published', '!=', '2')
                    ->where('date_end', '>=', date('Y.m.d H:i:s'))
                    ->where('title_' . I18n::$lang, '!=', '')
                    /*->where('rio', '=', 1)*/
                    ->order_by('important', 'DESC')
                    ->order_by('date_begin', 'ASC')
                    ->limit(4)
                    ->find_all();
            } else {
                $model = ORM::factory('Event')
                    ->where('is_published', '!=', '2')
                    ->where('title_' . I18n::$lang, '!=', '')
                    ->where('date_end', '>=', date('Y.m.d H:i:s'))
                    /*->where('rio', '=', 1)*/
                    ->order_by('important', 'DESC')
                    ->order_by('date_begin', 'ASC')
                    ->limit(4)
                    ->find_all();
                $federations = array();
                foreach ($model as $row) {
                    $federations[$row->id] = $row->federations->find_all();
                }
            }
            $this->response->body(View::factory('events/preview_main_rio')
                    ->bind('model', $model)
                    ->bind('federations', $federations)
            );
        }
    }

    public function action_view()
    {

        $this->page->script('yandexmap', 'http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU');
        $this->page->script('map', 'js/eventmap.js');
        $sef = $this->request->param('sef');
        $model = ORM::factory('Event')->where('sef_' . I18n::$lang, '=', $sef)->find();
        if (!$model->loaded()) {
            foreach ($this->array_langs as $lang => $url) {
                $model = ORM::factory('Event')->where('sef_' . $lang, '=', $sef)->find();
                if ($model->loaded()) {
                    throw new HTTP_Exception_Translate();
                }
            }
            throw new HTTP_Exception_404('Страница не найдена');
        }
        $this->init_langs($model);
        $this->page->title($model->title . ' - ' . __('Календарь') . ' - ' . $this->federation ? $this->federation->{'title_' . I18n::$lang} : __('Конфедерация спортивных единоборств и силовых видов спорта'));
        $this->page->meta(Text::limit_chars($model->text, 200), 'description');
        $this->page->content(View::factory('events/view')
            ->bind('model', $model));
    }

    public function action_sponsor()
    {
        //спонсоры
        $this->render = false;
        $id = $this->request->query('id');
        $model = ORM::factory('Event', array('id' => $id))->sponsors->where('sponsor', '=', '1')->limit(5)->find_all();
        $this->response->body(View::factory('events/sponsor')->bind('model', $model));
    }

    public function action_organization()
    {
        //участвующие организации
        $this->render = false;
        $id = $this->request->query('id');
        $model = ORM::factory('Event', array('id' => $id))->sponsors->where('sponsor', '=', '2')->limit(5)->find_all();
        $this->response->body(View::factory('events/sponsor')->bind('model', $model));
    }

    public function action_translit_message()
    {
        $this->render = false;
        $langs = array('ru' => '/ru/', 'en' => '/en/', 'kz' => '/');
        $this->write_lst_uri = false;
        $this->response->status(404);
        $this->session OR $this->session = Session::instance();
        $last_uri = '/' . ltrim($this->session->get('last_uri', URL::base()), '/');
        $model = ORM::factory('Event');
        preg_match('"([^/]*$)"', $last_uri, $match);
        foreach ($langs as $lang => $val) {
            if ($model->where('sef_' . $lang, '=', $match[0])->find()->loaded()) {
                break;
            }
        }
        if ($this->fed_id) {
            $url = $this->fed_id . '/event';
        } else {
            $url = 'confederation/event';
        }
        Helper::currentURI();
        $view = View::factory('errors/translite')
            ->bind('url', $url)
            ->bind('model', $model)
            ->bind('model_name', $model_name)
            ->bind('last_uri', $last_uri);
        $this->response->body($view);
    }
}