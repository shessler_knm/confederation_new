<?php
class Controller_Governance extends Site
{

    public function action_index()
    {
        $this->page->title(__('Руководство конфедерации'));
        $model = ORM::factory('Governance')->order_by('priority','ASC');
        $pagination = $this->pagination($model);
        $page = $this->request->param('page');
        $total_pages = $pagination->total_pages;
        $this->check_pages($page,$total_pages);
        View::bind_global('total_pages', $total_pages);
        $model = $model->find_all();
        $this->page->content(View::factory('confederation/about/governance')
            ->bind('pagination',$pagination)
            ->bind('model',$model));
        $this->add_crumb(__('Исполнительный комитет'), '');
    }


}
