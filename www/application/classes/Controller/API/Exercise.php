<?php
/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 02.08.13
 * Time: 15:43
 * To change this template use File | Settings | File Templates.
 */

class Controller_API_Exercise extends Privatecoach_API
{
    public $perpage = 1000;

    public $perpage_limit=1000;


    public function response_collection($collection, $extra = array())
    {
        I18n::lang($this->request->param('lang'));
        $models = array();
        foreach ($collection as $model) {
            $models[] = $this->collection_extra_for_model($model) + $model->as_array_ext();
        }
        $this->response->body(json_encode($models + $extra));
    }

    public function action_get_index()
    {
        $model = $this->model();
        $collection = null;

        $id = $this->request->param('id');

        if ($id) {
            $model = $this->model_filter_get($model, $id);
            $model->where('id', '=', $id)->find();
            if (!$model->loaded()) {
                throw new HTTP_Exception_404;
            }
            $extra = $this->model_extra_get($model);
            $this->response_model($model, $extra);
        } else {
            $perpage = (int)$this->request->query('perpage');
            $perpage = $perpage ? $perpage : $this->perpage;
            if ($perpage > $this->perpage_limit) {
                $perpage = $this->perpage_limit;
            }
            $model = $this->collection_filter_get($model, $id);
            Event::instance()->dispatch('api.model.before.find_all', $model);
//            $pagination = Pagination::factory(array(
//                'total_items'=>$perpage,
//                'current_page' => array('source' => 'query_string', 'key' => 'page'),
//            ));
//            $pagination->apply($model);
            $pagination = Pagination::factory(array(
                    'items_per_page' => $perpage,
                    'total_items' => $model->reset(false)->count_all(),
                    'current_page' => array('source' => 'query_string', 'key' => 'page'),
                )
            );
            $pagination->apply($model);
            $this->pagination = $pagination->render();
//            $model->pagination($perpage);
            $collection = $model->find_all();
            $extra = $this->collection_extra_get($collection);
            $this->response_collection($collection, $extra);
//            $this->response->body(View::factory('profiler/stats'));
        }

    }
}