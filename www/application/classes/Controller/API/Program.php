<?php

/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 31.07.13
 * Time: 11:51
 * To change this template use File | Settings | File Templates.
 */
class Controller_API_Program extends Privatecoach_API
{
//    public $perpage=1000;

    public function before()
    {
        parent::before();

        Event::instance()->bind('api.model.save.after', array($this, 'after_save'));
        Event::instance()->bind('api.model.update.after', array($this, 'after_save'));
        Event::instance()->bind('api.model.update.before', array($this, 'before_save'));
        Event::instance()->bind('api.model.before.find_all', array($this, 'by_user'));
    }

    public function by_user($model)
    {
        if ($this->request->query('user_id')) {
            $id = $this->request->query('user_id');
        } else {
            $id = $this->user->id;
        }
        $model->where('user_id', '=', $id);
    }

    public function editable_keys($model)
    {
        $keys = array(
            "start_date",
            "duration",
            'activity',
            'subscribers',
        );
        return $keys;
    }

    public function before_save($model, $raw)
    {
        $model->title_ru = Arr::get($raw, 'title_ru');
        $model->title_kz = Arr::get($raw, 'title_kz');
        $model->title_en = Arr::get($raw, 'title_en');
        $model->text_ru = Arr::get($raw, 'text');
        if ($model->user_id && $model->user_id != $this->user->id) {
            throw new HTTP_Exception_403;
        }
        if ($model->type != 'free') {
            $model->user_id = $this->user->id;
        }
    }

    public function action_get_index()
    {
        $model = $this->model();
        $collection = null;

        $id = $this->request->param('id');

        if ($id) {
            $model = $this->model_filter_get($model, $id);
            $model->where('id', '=', $id)->find();
            if (!$model->loaded()) {
                throw new HTTP_Exception_404;
            }
            $extra = $this->model_extra_get($model);
            $this->response_model($model, $extra);
        } else {
            $model = $this->collection_filter_get($model, $id);
            Event::instance()->dispatch('api.model.before.find_all', $model);
            $collection = $model->where('activity', '=', 1)->find_all();
            $extra = $this->collection_extra_get($collection);
            $this->response_collection($collection, $extra);
        }
    }

    protected function model_extra_get($model)
    {
        I18n::lang($this->request->param('lang'));
//        $model->start_date = $model->start_date >= date('Y-m-d') ? $model->start_date : date('Y-m-d');
        $dt = new DateTime($model->start_date);
        $dt->add(new DateInterval('P' . ($model->duration ? ($model->duration * 7 - 1) : 0) . 'D'));
        $data = array(
            'id' => $model->id,
            'title_ru' => $model->title_ru,
            'title_kz' => $model->title_kz,
            'title_en' => $model->title_en,
            'text' => $model->text,
            'start_date' => Date::formatted_time($model->start_date, 'd.m.Y'),
            'end_date' => $dt->format('d.m.Y'),
            'duration' => $model->duration,
            'mission' => $model->mission,
            'days' => $model->days_as_array(),
            'activity' => $model->activity,
            'subscribers' => $model->subscribers,
            'parent_id' => $model->parent->id,
            'period' => array(
                'start' => Date::formatted_time($model->start_date, 'Ymd'),
                'end' => $dt->format('Ymd'),
            )
        );
        return $data;
    }

    public function after_save($model)
    {
        $raw_values = $this->body_to_array();
        $this->save_days($model, Arr::get($raw_values, 'days', array()));
        if ($model->activity == 1) {
            $collection = ORM::factory('Program')->where('user_id', '=', $model->user_id)->where('id', '!=', $model->id)->where('activity', '=', 1)->find_all();
            foreach ($collection as $m) {
                $m->activity = 0;
                $m->save();
            }
        }

    }

    protected function collection_extra_for_model($model)
    {
        return $this->model_extra_get($model);
    }

    public function response_model($model, $extra = array())
    {
        $data = $extra + $model->as_array_ext();
        $this->response->body(json_encode($data));
    }


    protected function save_days($model, $days)
    {
        if (!$model->loaded()) {
            return;
        }
        $model->remove('exercises');
        foreach ($days as $day) {
            if (!isset($day->exercises) || !is_array($day->exercises)) {
                continue; //Пропускаем день без упражнений
            };
            foreach ($day->exercises as $index => $exercise) {
                $efforts = array();
                foreach ($exercise->efforts as $effort) {
                    $efforts[] = $effort->repeat;
                }
                $efforts = implode(',', $efforts);
                $m = ORM::factory('Program_Exercise');
                $m->program_id = $model->id;
                $m->exercise_id = $exercise->id;
                $m->week_day = $day->value;
                $m->sets = $efforts;
                $m->index_number = $index;
                $m->save();

            }
        }
    }


}