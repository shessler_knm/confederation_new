<?php
/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 09.08.13
 * Time: 11:35
 * To change this template use File | Settings | File Templates.
 */

class Controller_API_ProgramLog extends Privatecoach_API
{
    public $model_name = 'Program_Log';

    public $perpage = 1000;
    public $perpage_limit = 1000;

    public function before()
    {
        parent::before();
        Event::instance()->bind('api.model.save.before', array($this, 'before_save'));
    }

    public function before_save($model, $raw)
    {
        if ($this->user) {
            if ($model->user_id && $model->user_id != $this->user->id) {
                throw new HTTP_Exception_403;
            }
            $model->user_id = $this->user->id;
        }
    }

    protected function collection_filter_get($model, $id)
    {
        $model = parent::collection_filter_get($model, $id);
        $start_date = $this->request->query('start_date');
        $end_date = $this->request->query('end_date');
        if (!$start_date || !$end_date) { //Берем текущую неделю
            $curWeek = date('N') - 1;
            $dt = new DateTime();
            $dt->sub(new DateInterval('P' . $curWeek . 'D'));
            $start_date = $dt->format('Y-m-d');
            $dt->add(new DateInterval('P6D'));
            $end_date = $dt->format('Y-m-d');
        }
        $model->where('date', 'BETWEEN', array($start_date, $end_date));
        if ($this->request->query('user_id')) {
            $user_id=$this->request->query('user_id');
        } else {
            $user_id = $this->user->id;
        }
        if ($this->request->query('user_id')) {
            $user_id = $this->request->query('user_id');
        }
        $model->where('user_id', '=', $user_id);
        return $model;
    }

    protected function collection_extra_for_model($model)
    {
        return array(
            'exercise_id' => $model->exercise_id,
            'program_id' => $model->program_id
        );
    }


}