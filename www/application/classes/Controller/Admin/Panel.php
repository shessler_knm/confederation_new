<?php

class Controller_Admin_Panel extends Admin
{

    public function action_index()
    {
        $this->page->content(
            View::factory('admin/panel')
                ->set('modules',CMS::modules_info())
        );
    }
    public function action_storage()
    {
        $this->render = false;
        $view = View::factory('storage/public_upload_form')
            ->bind('model', $model)
            ->set('field', $this->request->query('id'))
            ->set('type', $this->request->query('type'));
        $type = $this->request->query('type');
        $model = Storage::getInstance()->get($this->request->query('value'));
        if ($this->request->post('save')) {
            $valid = Validation::factory($_FILES)
                ->rule('file', 'Upload::not_empty')
                ->rule('file', 'Upload::valid')
                ->rule('file', 'Upload::type', $type == 'Img'
                ? array(':value', array('jpg', 'png', 'gif'))
                : array(':value', array('doc', 'docx', 'pdf', 'rar', 'zip', 'xls', 'xlsx', 'txt')))
                ->rule('file', 'Upload::size', array(':value', '50MB'))
                ->label('file', $this->request->query('title'));
//            print_r($type);

            if ($valid->check()) {
                $model = Storage::getInstance()->save($_FILES['file']);
                $prev_id = $this->request->post('prev_id');
                if ($prev_id && !empty($prev_id)) {
                    $prev_id = Storage::getInstance()->get($prev_id);
                    $prev_id->loaded() AND $prev_id->id != $model->id AND $prev_id->delete();
                }
            } else {
                $this->errors = $this->errors + $valid->errors('validation');
            }
        }
        $this->response->body($view);
    }
    public function action_delimg()
    {
        $this->render = false;
        $model = ORM::factory("Storage",$this->request->param('id'));
        $model->loaded() && $model->delete();
    }
    public function action_file_upload()
    {
//        $file = Arr::get($_FILES, 'file');
        $valid = Validation::factory($_FILES);
        $valid->rules('file', array(
            array('Upload::not_empty', array(':value')),
            array('Upload::valid', array(':value')),
            array('Upload::size', array(':value', '50M')),
            array('Upload::type', array(':value', array('jpg', 'jpeg', 'png', 'gif', 'doc', 'docx', 'xls', 'xlsx', 'txt', 'ppt', 'pptx', 'rar', 'zip', '7z', 'odt', 'ods', 'odp','pdf'))),
        ))->label('file', 'Р¤Р°Р№Р»');
        $this->upload($valid,$_FILES,'file',$tags=array());
    }

    public function action_image_upload()
    {
//        $file = Arr::get($_FILES, 'file');
        $valid = Validation::factory($_FILES);
        $valid->rules('file', array(
            array('Upload::not_empty', array(':value')),
            array('Upload::valid', array(':value')),
            array('Upload::size', array(':value', '50M')),
            array('Upload::type', array(':value', array('jpg', 'jpeg', 'png', 'gif'))),
        ))->label('file', 'Р¤Р°Р№Р»');

        $this->upload($valid,$_FILES,'file',$tags=array());
    }

    public function upload($valid, $files,$field,$tags=array())
    {
        set_time_limit(0);
        $this->render = false;
        $array = array(//            'filelink' => false
        );
        if ($valid->check()) {
            /** @var Model_Storage $s */
            $s = Cms_Storage::instance()->upload($files,$field,$tags);
            $array = array(
                'filename' => Helper::translit($s->original_name),
                'filelink' => $s->url(),
            );
        } else {
            $array = array(
//                'filelink' =>'#',
                'filename' => Arr::get($valid->errors('validation'), 'file')

            );
            echo json_encode($array);
            return;
        }

        $array['filelink'] = preg_replace('#\\\#','/',$array['filelink']);
        echo stripslashes(json_encode($array));
    }

}
