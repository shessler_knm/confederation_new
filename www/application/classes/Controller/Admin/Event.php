<?php
class Controller_Admin_Event extends Admin
{
    public $option = array(
        'model' => 'Event',
    );


    public function action_synchronize_model()
    {

        $tasks = array('task' => 'synchronize',
            'env' => Helper::set_if_production(Kohana::PRODUCTION, Kohana::DEVELOPMENT));


        $federations = ORM::factory('Federation')->find_all();
        foreach ($federations as $fed) {
            $model = $this->factory_model();
            if ($fed->url) {
                Task_Synchronize::factory($tasks + array(
                    'model' => $model->object_name(),
                    'parent_hasmany' => 'events',
                    'fed_id' => $fed->id
                ))->execute();
            }
        }

        Minion_Task::factory($tasks)->execute();

        $this->redirect($this->last_uri);
    }
/*Метод добавляет к сефу id, т.к. для событий вполне нормально повторяющиеся заголовки
    public function action_advanced_sef()
    {
        $langs = array('kz', 'ru', 'en');
        $model = ORM::factory('Event')->find_all();
        foreach ($model as $row) {
            foreach ($langs as $lang) {
                if ($row->{'sef_' . $lang}&&$row->{'title_' . $lang}) {
                    $row->{'sef_' . $lang} = $row->id . '_' . Helper::translit($row->{'title_' . $lang});
                }
            }
            $row->save();
        }
    }*/
}