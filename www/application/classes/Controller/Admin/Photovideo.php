<?php
class Controller_Admin_Photovideo extends Admin
{
    public $option = array('model' => 'Photovideo');

    /**
     * Метод для синхронизации с Flickr
     */
    public function action_synchfl()
    {
        exec("php ".DOCROOT."index.php --task=Synchflickr > /dev/null 2>&1 &");
        $this->page->message('Синхронизация запущена');
        $this->redirect($this->last_uri);
    }


}