<?php
class Controller_Admin_Result extends Admin
{
    public $option=array('model'=>'Result');

    public function scripts(){
       return parent::scripts()+array(
           'chained'=>'js/lib/jquery.chained.remote.js',
           'select'=>'js/select.js',

       );
    }

    public function action_getEvent(){
        $this->render = false;
        $return = array();
        $fed_id = Arr::get($_GET,'federation_id');
        if($fed_id){
            $res = ORM::factory('Federation',$fed_id)->events->find_all();
            foreach($res as $value){
                $return[$value->id] = $value->title_ru;
            }
        }
        $this->response->body(json_encode($return));
    }

    public function action_synchronize_model() {

        $tasks = array('task' => 'synchronize',
            'env' => Helper::set_if_production(Kohana::PRODUCTION,Kohana::DEVELOPMENT));


        $federations = ORM::factory('Federation')->find_all();
        foreach($federations as $fed) {
            $model = $this->factory_model();
            if ($fed->url) {
                Task_Synchronize::factory($tasks + array (
                    'model' => $model->object_name(),
                    'parent_hasmany' => false,
                    'parent_model' => 'Federation',
                    'fed_id' => $fed->id
                ))->execute();
            }
        }

        Minion_Task::factory($tasks)->execute();

        $this->redirect($this->last_uri);
    }
}