<?php
class Controller_Admin_City extends Admin
{
    public $option = array(
        'model' => 'City',
    );


    public function action_synchronize_model() {

        $tasks = array('task' => 'synchronize',
            'env' => Helper::set_if_production(Kohana::PRODUCTION,Kohana::DEVELOPMENT));



        $federations = ORM::factory('Federation')->find_all();
        foreach($federations as $fed) {
            $model = $this->factory_model();
            if ($fed->url) {
                Task_Synchronize::factory($tasks + array (
                    'model' => $model->object_name(),
                    'parent_model' => false,
                    'parent_hasmany' => false,
                    'parent_id' => false,
                    'fed_id' => $fed->id
                ))->execute();
            }
        }

        Minion_Task::factory($tasks)->execute();

        $this->redirect($this->last_uri);
    }

}