<?php

class Controller_Admin_Program extends Admin
{
    public $option = array('model' => 'Program');

    public function before()
    {
        parent::before();
        Event::instance()->bind('admin.list.init', array($this, 'init_list'));
    }

    public function init_list($collection){
        return $collection->where('type','=','free');
    }
}