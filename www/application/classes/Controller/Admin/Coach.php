<?php
class Controller_Admin_Coach extends Admin
{
    public $option=array('model'=>'Coach');

    public function scripts(){
        return parent::scripts()+array(
            'chained'=>'js/lib/jquery.chained.remote.js',
            'select'=>'js/select_coach_remote.js',

        );
    }


    public function action_getTeam(){
        $this->render = false;
        $return = array();
        $fed_id = Arr::get($_GET,'federation_id');
        if($fed_id){
            $res = ORM::factory('Federation',$fed_id)->teams->find_all();
            foreach($res as $value){
                $return[$value->id] = $value->title_ru;
            }
        }
        $this->response->body(json_encode($return));
    }
}