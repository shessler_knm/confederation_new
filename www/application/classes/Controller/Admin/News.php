<?php
class Controller_Admin_News extends Admin
{
    public $option = array(
        'model' => 'News',
    );

    public function extrl_validation($model)
    {
        $data = $this->request->post('model');
        return Validation::factory($data)
            ->rule('federations', 'Orm::must_selected');
    }

    public function action_synchronize_model()
    {

        $tasks = array('task' => 'synchronize',
            'env' => Helper::set_if_production(Kohana::PRODUCTION, Kohana::DEVELOPMENT));


        $federations = ORM::factory('Federation')->find_all();
        foreach ($federations as $fed) {
            $model = $this->factory_model();
            if ($fed->url) {
                Task_Synchronize::factory($tasks + array(
                    'model' => $model->object_name(),
                    'parent_hasmany' => $model->object_name(),
                    'parent_id' => $fed->id,
                    'fed_id' => $fed->id
                ))->execute();
            }
        }

        Minion_Task::factory($tasks)->execute();

        $this->redirect($this->last_uri);
    }

    public function action_fix_url()
    {
        $langs = array('kz', 'en', 'ru');
        $model = ORM::factory('News')->find_all();
        $i = 0;
        $count = array();
        foreach ($model as $row) {
            $m = 0;
            foreach ($langs as $lang) {
                if (strstr($row->{'text_' . $lang}, 'confederation/news')) {
                    $row->{'text_' . $lang} = preg_replace('#confederation/news#', 'confederation/infocenter/news', $row->{'text_' . $lang});
                    $i++;
                    $m++;
                    $count[$row->id]=$m;
                }
            }
            $row->save();
        }
        $content = '<p>Обработано ' . $i . ' ссылок</p>' . '<p>Обработано ' . count($count) . ' новостей</p>';
        $this->page->content($content);
    }
}