<?php

class Controller_Admin_Exercise extends Admin
{
    public $option = array('model' => 'Exercise');

    public function before()
    {
        parent::before();
        Event::instance()->bind("admin.edit.save.after", array($this, 'multiupload'));
    }

    public function multiupload($object)
    {
        $type = 'file';
        $file_array = Arr::get($_FILES, 'files');
        if (!$file_array) {
            $resp_arr = array(
                'err' => 1,
                'msg' => __('Файлы не выбраны, или имеют слишком большой размер суммарный размер'),
                'file',
                "Не известная ошибка");
            $this->response->body(json_encode($resp_arr));

        } else {
            $fields = array(
                'error',
                'name',
                'type',
                'tmp_name',
                'size'
            );
            $files = array();
            foreach ($file_array['name'] as $key => $name) {
                $item = array();
                foreach ($fields as $f) {
                    $item[$f] = $file_array[$f][$key];
                }
                $files['file_' . $key] = $item;
            }
            $resp_arr = array();
            foreach ($files as $index => $ff) {
                $validate = Validation::factory($files);
                if ($type == 'image') {
                    $validate->rules($index, array(
                        array("Upload::not_empty"),
                        array("Upload::valid"),
                        array("Upload::type", array(":value", array("png", "jpg", "gif", "jpeg"))),
                        array("Upload::size", array(":value", "2M"))

                    ));
                } else {
                    $validate->rules($index, array(
                        array("Upload::not_empty"),
                        array("Upload::valid"),
                        array("Upload::type", array(":value", array('doc', 'docx', 'pdf', 'rar', 'zip', 'xls', 'xlsx', 'txt', "png", "jpg", "gif", "jpeg"))),
                        array("Upload::size", array(":value", "2M"))

                    ));
                }
                if ($validate->check()) {
                    $file = Cms_Storage::instance()->upload($files, $index);
                    if ($file instanceof ORM && $file->loaded()) {
                        $arr = array(
                            'err' => 0,
                            'file' => $file->id,
                            'name' => $file->original_name,
                            'url' => $file->download_file_url(),
                            'type' => $file->type
                        );
                        $resp_arr[] = $arr;
                        $model = ORM::factory('Exercise_Storage');
                        $model->storage_id = $file->id;
                        $model->exercise_id = $object->id;
                        try {
                            $model->save();
                        } catch (ORM_Validation_Exception $e) {
                            continue;
                        }
                    }
                } else {
                    $arr = array(
                        'err' => 1,
                        'msg' => Arr::get($validate->errors('storage'), $index, "Не известная ошибка"),
                        'name' => $ff['name'],
                        'url' => '',
                        'type' => ''
                    );

                    $resp_arr[] = $arr;
                }
            }
        }


    }
}