<?php

class Controller_Admin_Section extends Admin
{
    public $option = array(
        'model' => 'Section',
    );
    protected function _after_save($model, $values = null)
    {
        $town=$this->request->post('town');

        if ($town) {
            $vals=explode(';',$town);
            $town = ORM::factory('City')->where('title_ru', 'IN', $vals)->find();
            if ($town->loaded()) {
                if ($model->city_id != $town->id) {
                    $model->city_id = $town->id;
                    $model->save();
                }
            }
        }
        parent::_after_save($model, $values);
    }
}