<?php defined('SYSPATH') or die('No direct script access.');
class Controller_Admin_Comment extends Admin_Embeded
{
    public $option = array('model' => 'Comment');

    protected function _list_init($collection)
    {
        if (!$this->request->query('search')) {
            $parent_id = $this->request->param('id', 0);
            $collection->where($collection->parent_field(), '=', $parent_id);
            $collection->{$collection->parent_field()} = $parent_id;
        }
        return $collection;
    }
}
