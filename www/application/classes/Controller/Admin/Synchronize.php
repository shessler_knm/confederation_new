<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Saidashev Marat
 * Date: 10.09.13
 * Time: 9:45
 * To change this template use File | Settings | File Templates.
 */

class Controller_Admin_Synchronize extends Admin {

    public function log($str) {
        Kohana::$log->add(Log::ERROR,$str);
    }

    public function action_index() {

        set_time_limit(0);

        $tasks = array('task' => 'synchronize',
                       'env' => Helper::set_if_production(Kohana::PRODUCTION,Kohana::DEVELOPMENT));

        //Запуск общей синхронизации данных
        $this->log('Начало синхронизации');
        $start = time();

        foreach (ORM::factory('Federation')->find_all() as $fed) {

            $this->log('Синхронизируем => '.$fed->url. ' start');


            if (!$fed->url) continue;

            /**
             * Синхронизируем категории все родительские
             * потом только старны (потому что остальное не нужно)
             *
            */

            $this->log('* Синхронизируем города');
            Minion_Task::factory($tasks + array(
                'model' => 'city',
                'parent_model' => false,
                'parent_hasmany' => false,
                'parent_id' => false,
                'fed_id' => $fed->id
            ))->execute();
            $this->log('* Синхронизируем города done --');

            $this->log('* Синхронизируем события');
            Minion_Task::factory($tasks + array(
                'model' => 'event',
                'parent_hasmany' => 'events',
                'fed_id' => $fed->id
            ))->execute();
            $this->log('* Синхронизируем события done --');

            $this->log('* Синхронизируем результаты');
            Minion_Task::factory($tasks + array(
                'model' => 'result',
                'parent_hasmany' => false,
                'parent_model' => 'Federation',
                'fed_id' => $fed->id
            ))->execute();
            $this->log('* Синхронизируем результаты done --');

            $this->log('* Синхронизируем новости');
            Minion_Task::factory($tasks + array(
                'model' => 'news',
                'parent_hasmany' => 'news',
                'parent_id' => $fed->id,
                'fed_id' => $fed->id
            ))->execute();
            $this->log('* Синхронизируем новости done --');

            $this->log('* Синхронизируем команды');
            Minion_Task::factory($tasks + array(

                'model' => 'team',
                'parent_hasmany' => 'teams',
                'parent_id' => $fed->id,
                'fed_id' => $fed->id
            ))->execute();
            $this->log('* Синхронизируем команды done --');

            $this->log('* Синхронизируем тренеров');
            Minion_Task::factory($tasks + array(
                'model' => 'coach',
                'parent_hasmany' => 'coaches',
                'parent_id' => $fed->id,
                'fed_id' => $fed->id
            ))->execute();
            $this->log('* Синхронизируем тренеров done --');

            $this->log('* Синхронизируем игроков');
            foreach(ORM::factory('Team')
                        ->where('synch_id','!=',NULL)
                        ->where('synch_site','=',$fed->url)
                        ->find_all() as $team) {
                Minion_Task::factory($tasks + array(
                    'model' => 'player',
                    'parent_model' => 'Team',
                    'parent_hasmany' => 'players',
                    'parent_field' => 'team_id',
                    'parent_id' => $team->id,
                    'fed_id' => $fed->id

                ))->execute();
            }
            $this->log('* Синхронизируем игроков done --');

            //Демон
            $this->synchronization();

            $this->log('Синхронизируем => '.$fed->url.' done;');
        }

        $this->log('Завершение синхронизации ! время синхр. = '.round(($start - time()) / Date::MINUTE).' минут');
    }


    public function synchronization()
    {
        $tasks = array('task' => 'synchronize',
            'env' => Helper::set_if_production(Kohana::PRODUCTION,Kohana::DEVELOPMENT));

        Minion_Task::factory($tasks)->execute();
    }
}