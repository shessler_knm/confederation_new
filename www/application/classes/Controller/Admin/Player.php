<?php

class Controller_Admin_Player extends Admin_Embeded
{
    public $option = array(
        'model' => 'Player',
    );

    public function scripts(){
        return parent::scripts()+array(
            'chained'=>'js/lib/jquery.chained.remote.js',
            'select'=>'js/select_team_remote.js',

        );
    }

    public function action_synchronize_model() {
        $team = ORM::factory('Team',$this->request->param('id'));
        $fed = $team->federation;
        $model = $this->factory_model();

        $tasks = array('task' => 'synchronize',
            'env' => Helper::set_if_production(Kohana::PRODUCTION,Kohana::DEVELOPMENT));

//        $model = ORM::factory()
//        $fed =
        if ($fed->url) {
            Task_Synchronize::factory($tasks + array (
                'model' => $model->object_name(),
                'parent_model' => 'team',
                'parent_hasmany' => 'players',
                'parent_field' => 'team_id',
                'parent_id' => $team->id,
                'fed_id' => $fed->id
            ))->execute();
        }


        $this->redirect($this->last_uri);
    }


    public function action_getTeam(){
        $this->render = false;
        $return = array();
        $fed_id = Arr::get($_GET,'federation_id');
        if($fed_id){
            $res = ORM::factory('Federation',$fed_id)->teams->find_all();
            foreach($res as $value){
                $return[$value->id] = $value->title_ru;
            }
        }
        $this->response->body(json_encode($return));
    }
    public function action_getCoach(){
        $this->render = false;
        $return = array();
        $fed_id = Arr::get($_GET,'federation_id');
        if($fed_id){
            $res = ORM::factory('Federation',$fed_id)->coaches->find_all();
            foreach($res as $value){
                $return[$value->id] = $value->get_full_name();
            }
        }
        $this->response->body(json_encode($return));
    }

}