<?php

class Controller_REST_Article extends API
{

    public function action_post_index()
    {
        $fields = array('id', 'title', 'photo', 'date', 'main', 'score', 'categories', 'is_published', 'comments_count', 'views_count');
        $collection = $this->model();
        $options = Arr::get($this->filters, "tagIds");
        if (!empty($options)) {
            $collection->join('article_categories')
                ->on('article_categories.article_id', '=',
                    DB::expr('article.id AND article_categories.category_id IN :cats', array(':cats' => $options)))
                ->group_by('article.id');
        }
        $collection = $collection->where('title_' . I18n::$lang, '!=', '')->where('text_' . I18n::$lang, '!=', '')->limit($this->perpage)->offset($this->offset)->order_by('score', $this->sorting);
        $this->response($collection, $fields);
    }

    public function action_get_view()
    {
        $id = $this->request->param('id');

        if ($id) {
            $model = $this->model($id);
            if (!$model->loaded()) {
                $this->response(array('error' => 'No such article'));
            } else {
                $this->response($model);
            }
        } else {
            $this->response(array('error' => 'Unknown id'));
        }
    }

    public function action_get_tags()
    {
        $data = array();
        $categories = ORM::factory('Category')->get_sef_articles()->order_by('id', 'ASC')->find_all();
        foreach ($categories as $category) {
            $data[] = array('tag_name' => $category->title, 'tag_id' => $category->id);
        }
        $this->response($data);
    }

    public function action_post_score()
    {
        $id = Arr::get($this->raw_values, 'article_id');
        $score = Arr::get($this->raw_values, 'score');
        if (is_numeric($score) && $score >= 0 && $score <= 5) {
            $model = $this->model($id);
            $article_scores = $model->article_scores->where('user_id', '=', $this->user->id)->find();
            $article_scores->target_id = $id;
            $article_scores->user_id = $this->user->id;
            $article_scores->score = $score;
            try {
                $article_scores->save();
                if ($article_scores->loaded()) {
                    $scores = $model->article_scores->where('target_id', '=', $article_scores->target_id)->find_all()->as_array('id', 'score');
                    $score = array_sum($scores) / count($scores);
                    $model->score = $score;
                    $model->save();
                    $this->response($score);
                }
            } catch (ORM_Validation_Exception $e) {
                $this->response(array('error' => $e->errors(0)));
            }
        } else {
            $this->response(array('error' => 'Invalid value'));
        }
    }
}