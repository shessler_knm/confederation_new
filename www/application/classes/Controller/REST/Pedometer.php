<?php

class Controller_REST_Pedometer extends API
{
    protected $need_auth = true;
    public $data = array(
        'total' => array(
            'time' => '00:00:00',
            'distance' => 0,
            'average_speed' => 0,
            'calories' => 0,
            'max_speed' => 0,
            'steps' => 0,
        ),
        'data' => array()
    );
    public $data_types = array(
        'total' => array(
            'bike' => array(
                'time' => '00:00:00',
                'distance' => 0,
                'average_speed' => 0,
                'calories' => 0,
                'max_speed' => 0,
                'steps' => 0,
            ),
            'running' => array(
                'time' => '00:00:00',
                'distance' => 0,
                'average_speed' => 0,
                'calories' => 0,
                'max_speed' => 0,
                'steps' => 0,
            ),
        ),
        'data' => array()
    );

    public function action_post_save_result()
    {
        $raw_values = $this->raw_values;
        $model = $this->model();
        $raw_values['datetime'] = $raw_values['date'];
        unset($raw_values['date']);
        $model->values($raw_values);
        $model->user_id = $this->user->id;
        $model->event = 'result';
        $model->save();
        if ($model->saved()) {
            Feed::feed_pedometer_result($this->user, $model->id);
        }
        $this->response('true');
    }

    public function action_post_index()
    {
        $model = $this->model();
        $collection = $model->where('user_id', '=', $this->user->id);
        $this->response($collection);
    }

    public function action_post_statistic()
    {
        $date = Arr::get($this->raw_values, 'date');
        $date = Date::formatted_time($date, 'Y-m');
        $model = $this->model();
        $type = Arr::get($this->raw_values, 'type', 'running');
        $collection = $model
            ->where('user_id', '=', $this->user->id)
            ->where('type', '=', $type)
            ->where('event', '=', 'result')
            ->where(DB::expr("DATE_FORMAT(datetime,'%Y-%m')"), '=', $date)
            ->order_by('datetime', 'ASC')
            ->find_all();
        $data = $this->total_results($collection);
        $this->response($data);
    }

    public function total_results($collection, $full_response = false)
    {

        if ($full_response) {
            $data = $this->data_types;
            foreach ($collection as $row) {
                $data['total'][$row->type]['time'] += $row->time;
                $data['total'][$row->type]['distance'] += $row->distance;
                $data['total'][$row->type]['average_speed'] += $row->average_speed;
                $data['total'][$row->type]['calories'] += $row->calories;
                $data['total'][$row->type]['max_speed'] = floatval($data['max_speed'] >= $row->max_speed ? $data['max_speed'] : $row->max_speed);
                $data['total'][$row->type]['steps'] += $row->steps;
                $data['data'][] = $row->as_array_ext();
            }
            if (count($collection)) {
                foreach ($data['total'] as $key => $type) {
                    $data['total'][$key]['average_speed'] = floatval(number_format($data['total'][$key]['average_speed'] / count($collection), 2));
                    $hours = floor($data['total'][$key]['time'] / 3600);
                    $minutes = floor(($data['total'][$key]['time'] / 3600 - $hours) * 60);
                    $seconds = round((($data['total'][$key]['time'] / 3600 - $hours) * 60 - $minutes) * 60);
                    $data['total'][$key]['time'] = Date::formatted_time($hours . ':' . $minutes . ':' . $seconds, 'H:i:s');
                }
            }
        } else {
            $data = $this->data;
            foreach ($collection as $row) {
                $data['total']['time'] += $row->time;
                $data['total']['distance'] += $row->distance;
                $data['total']['average_speed'] += $row->average_speed;
                $data['total']['calories'] += $row->calories;
                $data['total']['max_speed'] = floatval($data['max_speed'] >= $row->max_speed ? $data['max_speed'] : $row->max_speed);
                $data['total']['steps'] += $row->steps;
                $data['data'][] = $row->as_array_ext();
            }
            if (count($collection)) {
                $data['total']['average_speed'] = floatval(number_format($data['total']['average_speed'] / count($collection), 2));
                $hours = floor($data['total']['time'] / 3600);
                $minutes = floor(($data['total']['time'] / 3600 - $hours) * 60);
                $seconds = round((($data['total']['time'] / 3600 - $hours) * 60 - $minutes) * 60);
                $data['total']['time'] = Date::formatted_time($hours . ':' . $minutes . ':' . $seconds, 'H:i:s');
            }
        }
        return $data;
    }

    public function action_post_calendar()
    {
        $model = $this->model();
        $date = Arr::get($this->raw_values, 'date');
        $collection = $model;
        $collection = $collection
            ->where('user_id', '=', $this->user->id)
            ->where(DB::expr("DATE_FORMAT(datetime,'%Y-%m')"), '=', $date)
            ->order_by('datetime', 'ASC')
            ->find_all();
        $data = $this->total_results($collection, true);
        $this->response($data);
    }

    public function action_post_add()
    {
        $date = Arr::get($this->raw_values, 'date');
        $time = Arr::get($this->raw_values, 'time');
        $type = Arr::get($this->raw_values, 'type');
        $model = $this->model();
        $model->user_id = $this->user->id;
        $model->type = $type;
        $model->event = 'calendar';
        $model->datetime = Date::formatted_time($date . ' ' . $time, 'Y-m-d H:i');
        $model->save();
        $this->response('true');
    }
}