<?php

class Controller_REST_Feed extends API
{

    public function action_post_index()
    {
        $object = array();
        $collection = $this->model();
        $subscribes = $this->user->subscribers->where('status', '=', 1)->find_all()->as_array('id', 'id');
        $subscribes[0] = 0;
        $common_types = array('close_mission', 'new_weight', 'new_fat');
        $rules = $this->user->feed_rules->find_all()->as_array('id', 'feed_id');
        $rules[0] = 0;
        $albums = ORM::factory('User_Album')->where('user_id', 'in', $subscribes)->find_all();
        $keys = $albums->as_array(null, 'id');
        $keys[] = 0;
        $collection = $collection
            ->and_where_open()
            ->where('user_id', 'in', $subscribes)
            ->and_where_open()

            ->and_where('type', 'in', $common_types)
            ->and_where('user_id', 'in', $subscribes)

            ->or_where('type', '=', 'new_sub')
            ->and_where('user_id', '=', $this->user->id)

            ->and_where_close()
            ->and_where_close()

            //События юзеров, на которых я подписан(+я сам), относящиеся к альбомам
            ->or_where_open()
            ->or_where('user_id', 'in', $subscribes)
            ->and_where('type', '=', 'update_album')
            ->and_where('value_1', 'in', $keys)
            ->or_where_close()

            ->or_where_open()
            ->or_where('user_id', 'in', $subscribes)
            ->and_where('type', '=', 'training')
            ->or_where_close()

            ->or_where_open()
            ->or_where('user_id', 'in', $subscribes)
            ->and_where('type', '=', 'pedometer')
            ->or_where_close()

            ->where('id', 'not in', $rules)
            ->limit($this->per_page)
            ->offset($this->offset)
            ->order_by('id', 'DESC')
            ->find_all();
        $data = $this->construct_data($collection);
        foreach ($data as $row) {
            $object[] = $row;
        }
        $this->response($object);
    }

    public function construct_data($collection)
    {
        $i = 0;
        $data = array();
        $fields = array('id', 'date', 'type');
        foreach ($collection as $row) {
            switch ($row->type) {
                case 'update_album':
                    $album = $row->album;
                    $photos = $album->photos->find_all();
                    $data[$i] = $row->as_array_ext($fields);
                    foreach ($photos as $photo) {
                        $data[$i]['data']['photoUrls'][] = $photo->photo_s->url('http');
                    }
                    $data[$i]['user']['id'] = $row->user->id;
                    $data[$i]['user']['sex'] = $row->user->sex == 1 ? 'M' : 'F';
                    $data[$i]['user']['photo'] = $row->user->photo ? $row->user->photo_s->url('http') : null;
                    $data[$i]['user']['name'] = $row->user->get_full_name();
                    if (!count($photos)) {
                        unset($data[$i]);
                    }
                    break;
                case 'new_sub':
                    $data[$i] = $row->as_array_ext($fields);
                    $data[$i]['user']['id'] = $row->user->id;
                    $data[$i]['user']['sex'] = $row->user->sex == 1 ? 'M' : 'F';
                    $data[$i]['user']['photo'] = $row->user->photo ? $row->user->photo_s->url('http') : null;
                    $data[$i]['user']['name'] = $row->user->get_full_name();
                    break;
                case ('new_weight'):
                    $data[$i] = $row->as_array_ext($fields);
                    $data[$i]['data']['value'] = $row->value_1;
                    $data[$i]['user']['id'] = $row->user_id;
                    $data[$i]['user']['sex'] = $row->user->sex == 1 ? 'M' : 'F';
                    $data[$i]['user']['photo'] = $row->user->photo ? $row->user->photo_s->url('http') : null;
                    $data[$i]['user']['name'] = $row->user->get_full_name();
                    break;
                case ('new_fat'):
                    $data[$i] = $row->as_array_ext($fields);
                    $data[$i]['data']['value'] = $row->value_1;
                    $data[$i]['user']['id'] = $row->user_id;
                    $data[$i]['user']['sex'] = $row->user->sex == 1 ? 'M' : 'F';
                    $data[$i]['user']['photo'] = $row->user->photo ? $row->user->photo_s->url('http') : null;
                    $data[$i]['user']['name'] = $row->user->get_full_name();
                    break;
                case ('close_mission'):
                    $data[$i] = $row->as_array_ext($fields);
                    $data[$i]['data']['mission_text'] = Feed::award_text($this->user, $row->value_1);
                    $data[$i]['user']['id'] = $row->user_id;
                    $data[$i]['user']['sex'] = $row->user->sex == 1 ? 'M' : 'F';
                    $data[$i]['user']['photo'] = $row->user->photo ? $row->user->photo_s->url('http') : null;
                    $data[$i]['user']['name'] = $row->user->get_full_name();
                    break;
                case ('training'):
                    $training = ORM::factory('Training', $row->value_1);
                    if ($training->loaded()) {
                        //время, калории, ср, макс скорость и дистанцию
                        $data[$i] = $row->as_array_ext($fields);
                        $data[$i]['data']['spend_min'] = $training->spend_min;
                        $data[$i]['data']['calories'] = $training->calories_burned;
                        $data[$i]['data']['level'] = $training->level;;
                        $data[$i]['data']['weight'] = $training->weight;
                        $data[$i]['data']['fat'] = $training->fat;
                        $data[$i]['data']['photo'] = $training->photo ? $training->photo->url('http') : null;
                        $data[$i]['user']['id'] = $row->user_id;
                        $data[$i]['user']['sex'] = $row->user->sex == 1 ? 'M' : 'F';
                        $data[$i]['user']['photo'] = $row->user->photo ? $row->user->photo_s->url('http') : null;
                        $data[$i]['user']['name'] = $row->user->get_full_name();
                    }
                    break;
                case ('pedometer'):
                    $training = ORM::factory('Pedometer', $row->value_1);
                    if ($training->loaded()) {
                        //время, калории, ср, макс скорость и дистанцию
                        $data[$i] = $row->as_array_ext($fields);
                        $data[$i]['data']['time'] = $training->time;
                        $data[$i]['data']['calories'] = $training->calories;
                        $data[$i]['data']['distance'] = $training->distance;
                        $data[$i]['data']['average_speed'] = $training->average_speed;
                        $data[$i]['data']['max_speed'] = $training->max_speed;
                        $data[$i]['data']['type'] = $training->type;
                        $data[$i]['user']['id'] = $row->user_id;
                        $data[$i]['user']['sex'] = $row->user->sex == 1 ? 'M' : 'F';
                        $data[$i]['user']['photo'] = $row->user->photo ? $row->user->photo_s->url('http') : null;
                        $data[$i]['user']['name'] = $row->user->get_full_name();
                    }
                    break;
            }
            if (Arr::get($data, $i)) {
                $data[$i]['url'] = URL::site(I18n::$lang . '/privatecoach', 'http');

                $i++;
            }
            end($data);
        }
        return $data;
    }

    public function action_get_view()
    {
        $id = $this->request->param('id');
        $data = array();
        $fields = array('id', 'date', 'type');
        if ($id) {
            $model = $this->model($id);
            if ($model->loaded()) {
                switch ($model->type) {
                    case 'update_album':
                        $album = $model->album;
                        $photos = $album->photos->find_all();
                        $data = $model->as_array_ext($fields);
                        foreach ($photos as $photo) {
                            $data['data']['photoUrls'][] = $photo->photo_s->url('http');
                        }
                        $data['user']['id'] = $model->user->id;
                        $data['user']['sex'] = $model->user->sex == 1 ? 'M' : 'F';
                        $data['user']['photo'] = $model->user->photo ? $model->user->photo_s->url('http') : null;
                        $data['user']['name'] = $model->user->get_full_name();
                        break;
                    case 'new_sub':
                        $data = $model->as_array_ext($fields);
                        $data['user']['id'] = $model->user->id;
                        $data['user']['sex'] = $model->user->sex == 1 ? 'M' : 'F';
                        $data['user']['photo'] = $model->user->photo ? $model->user->photo_s->url('http') : null;
                        $data['user']['name'] = $model->user->get_full_name();
                        break;
                    case ('new_fat' || 'new_weight'):
                        $data = $model->as_array_ext($fields);
                        $data['data'][] = $model->value_1;
                        $data['user']['id'] = $model->user_id;
                        $data['user']['sex'] = $model->user->sex == 1 ? 'M' : 'F';
                        $data['user']['photo'] = $model->user->photo ? $model->user->photo_s->url('http') : null;
                        $data['user']['name'] = $model->user->get_full_name();
                        break;
                    case ('close_mission'):
                        $data = $model->as_array_ext($fields);
                        $data['data'][] = $model->value_1;
                        $data['user']['id'] = $model->user_id;
                        $data['user']['sex'] = $model->user->sex == 1 ? 'M' : 'F';
                        $data['user']['photo'] = $model->user->photo ? $model->user->photo_s->url('http') : null;
                        $data['user']['name'] = $model->user->get_full_name();

                        switch (true) {
                            case ($model->user->id == 1 && $model->user->sex == 1):
                                $data['user']['text'] = __('Бодибилдер Уровень 1');
                                break;
                            case ($model->id == 2 && $model->user->sex == 1):
                                $data['user']['text'] = __('Бодибилдер Уровень 2');
                                break;
                            case ($model->user->id == 3 && $model->user->sex == 1):
                                $data['user']['text'] = __('Бодибилдер Уровень 3');
                                break;
                            case ($model->user->id == 4 && $model->user->sex == 1):
                                $data['user']['text'] = __('Мистер Мира');
                                break;
                            case ($model->user->id == 5 && $model->user->sex == 1):
                                $data['user']['text'] = __('Мистер Вселенная');
                                break;
                            case ($model->user->id == 1 && $model->user->sex == 2):
                                $data['user']['text'] = __('Первый уровень стройности');
                                break;
                            case ($model->user->id == 2 && $model->user->sex == 2):
                                $data['user']['text'] = __('Второй уровень стройности');
                                break;
                            case ($model->user->id == 3 && $model->user->sex == 2):
                                $data['user']['text'] = __('Третий уровень стройности');
                                break;
                            case ($model->user->id == 4 && $model->user->sex == 2):
                                $data['user']['text'] = __('Мисс Мира');
                                break;
                            case ($model->user->id == 5 && $model->user->sex == 2):
                                $data['user']['text'] = __('Мисс Вселенная');
                                break;

                        }
                        break;
                }
                $this->response($data);
            } else {
                $this->response('no such feed');
            }
        } else {
            $this->response('unknown id');
        }


    }

    public function action_post_diary()
    {
        $object = array();
        $collection = $this->model();
        $common_types = array('close_mission', 'new_weight', 'new_fat', 'new_sub');
        $rules = $this->user->feed_rules->find_all()->as_array('id', 'feed_id');
        $rules[0] = 0;
        $albums = ORM::factory('User_Album')->where('user_id', '=', $this->user->id)->find_all();
        $keys = $albums->as_array(null, 'id');
        $keys[] = 0;
        $collection = $collection
            ->where('user_id', '=', $this->user->id)
            ->where('id', 'not in', $rules)
            ->limit($this->per_page)
            ->offset($this->offset)
            ->order_by('id', 'DESC')
            ->find_all();
        $data = $this->construct_data($collection);
        foreach ($data as $row) {
            $object[] = $row;
        }
        $this->response($object);
    }

    public function action_get_delete()
    {
        $id = $this->request->param('id');
        $model = $this->model($id);
        if ($model->loaded()) {
            $model->delete();
            $this->response('true');
        } else {
            $this->response(array('error' => 'No such feed'));
        }
    }
}