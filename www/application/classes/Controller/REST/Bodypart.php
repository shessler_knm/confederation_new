<?php
/**
 * Created by PhpStorm.
 * User: sgm
 * Date: 13.02.14
 * Time: 17:36
 */

class Controller_Rest_Bodypart extends API {

    public function action_get_index(){
        $this->per_page = 1000;
        $this->response->headers('Etag');
        $model = ORM::factory('Category')->where('slug','=','gruppi-mishshch')->find();
        $collection = $model->children;
        $this->response($collection);
    }

    public function action_head_index(){
        $this->check_cache(sha1($this->request->uri() .date('Y-m-d h:i')));
    }
} 