<?php

/**
 * Created by PhpStorm.
 * User: sgm
 * Date: 20.02.14
 * Time: 12:02
 */
class Controller_Rest_Comment extends API
{

    protected $need_auth = true;

    public function before()
    {
        parent::before();
    }

    public function action_post_new()
    {
        $comment= $this->model();
        $errors = array();
        $object = $this->request->param('id');
        $id = $this->request->param('id2');
        if ($object) {
            $model = ORM::factory(ucfirst($object), $id);
            if (!$model->loaded()) {
                $errors[] = "no such " . $object;
            }
        } else {
            $errors[] = "no object";
        }
        
        if (!$errors) {
            $values = $this->body_to_array();
            $values['table_name'] = $model->table_name();
            $values['target_id'] = $id;
            $values['status'] = 1;
            $values['user_ip'] = Helper::get_ip_address();
            $values['user_id'] = $this->user->id;
            $values['date'] = date('Y-m-d H:i:s');
            $comment->values($values);
            $comment->save();
            $this->response($comment, array(), 200, false);
        } else {
            $this->response(array('error' => implode(",", $errors)));
        }

    }

    public function action_post_list()
    {
        $object = $this->request->param('id');
        $model = ORM::factory(ucfirst($object));
        $table_name = $model->table_name();
        $target_id = $this->request->param('id2');
        $fields = array('id', 'table_name', 'text', 'date');
        $collection = ORM::factory('Comment')
            ->where('status', '=', 1)
            ->where('target_id', '=', $target_id)
            ->where('table_name', '=', $table_name)
            ->order_by('date', 'DESC');
//        $collection = $collection->find_all();
        $this->response($collection, $fields);
    }

    public function action_post_delete()
    {
        $id = (int)Arr::get($this->raw_values, 'id') ? (int)Arr::get($this->raw_values, 'id') : (int)$this->request->param('id');
        if (!$id) {
            $this->response(array('error' => 'no id'));
        } else {
            $model = $this->model($id);
            if (!$model->loaded()) {
                $this->response(array('error' => 'no such comment'));
            } else {
                if ($model->user_id != $this->user->id) {
                    $this->response(array('error' => 'access denied'));
                } else {
                    $model->delete();
                    $this->response(array('deleted'));
                }
            }
        }
    }
} 