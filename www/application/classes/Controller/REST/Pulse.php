<?php

/**
 * Created by PhpStorm.
 * User: id541rak
 * Date: 23.07.14
 * Time: 9:31
 */
class Controller_Rest_Pulse extends API
{

    public function action_get_update()
    {
        $pulses = ORM::factory('Pulse')->find_all();
        foreach ($pulses as $pulse) {
            $category = ORM::factory('Category', $pulse->type_id);
            if ($category->loaded()){
                $pulse_type = ORM::factory('Pulse_Type')->where('title_ru','=',$category->title_ru)->find();
                if ($pulse_type->loaded()){
                    $pulse->type_id = $pulse_type->id;
                    $pulse->save();
                }
            }
        }
    }

    public function action_post_add_category()
    {
        $category = ORM::factory('Pulse_Type');
        $category->{'title_' . I18n::$lang} = Arr::get($this->raw_values, 'name');
        $category->user_id = $this->user->id;
        try {
            $category->save();
        } catch (ORM_Validation_Exception $e) {
            $this->response(array('error' => $e->errors(0)));
        }
        $this->response('true');
    }

    public function action_post_category_list()
    {
        $category = ORM::factory('Pulse_Type')->where('user_id', '=', null)->or_where('user_id', '=', $this->user->id)->find_all();;
        $this->response($category);
    }

    public function action_get_category_list()
    {
        $category = ORM::factory('Pulse_Type')->where('user_id', '=', null)->or_where('user_id', '=', $this->user->id)->find_all();;
        $this->response($category);
    }

    public function action_post_delete_category()
    {
        $id = Arr::get($this->raw_values, 'id');
        $category = ORM::factory('Pulse_Type', $id);
        if ($category->loaded()) {
            $category->delete();
            $this->response('true');
        } else {
            $this->response(array('error' => 'Category not found'));
        }
    }

    public function action_post_save_result()
    {
        $model = $this->model();
        $category_name = Arr::get($this->raw_values, 'category_name');
        $category_id = Arr::get($this->raw_values, 'category_id');
        if ($category_name) {
            $category = ORM::factory('Pulse_Type');
            $category->{'title_' . I18n::$lang} = $category_name;
            $category->user_id = $this->user->id;
            try {
                $category->save();
            } catch (ORM_Validation_Exception $e) {
                $this->response(array('error' => $e->errors(0)));
            }
            $this->raw_values['type_id'] = $category->id;
        } else {
            $this->raw_values['type_id'] = $category_id;
        }
        if (!$this->raw_values['datetime']) {
            $this->raw_values['datetime'] = date('Y-m-d H:i');
        }
        $model->values($this->raw_values);
        $model->user_id = $this->user->id;
        try {
            $model->save();
            $this->response('true');
        } catch (Database_Exception $e) {
            $this->response(array('error' => 'Category not found'));
        }
    }

    public function action_get_results_by_category()
    {
        $fields = array('id', 'value');
        $category_id = $this->request->param('id');
        $model = $this->model()->where('type_id', '=', $category_id);
        $this->response($model, $fields);
    }

    public function action_post_statistics()
    {
        $option = Arr::get($this->raw_values, 'option');
        $type = Arr::get($option, 'statisticType');
        $model = $this->model()->where('user_id', '=', $this->user->id);
        $categories = ORM::factory('Pulse_Type')->find_all();
        if (strtolower($type) == "by_categories") {
            $values = array();
            $results = array();
            $collection = $model->limit(15)->find_all();
            foreach ($collection as $row) {
                $values[$row->type_id][] = $row->value;
            }
            if ($values) {
                foreach ($categories as $category) {
                    if (array_key_exists($category->id, $values)) {
                        $results[] = array(
                            'category_id' => $category->id,
                            'category_name' => $category->title,
                            'average_value' => round(array_sum($values[$category->id]) / count($values[$category->id])),
                            'values' => $values[$category->id]
                        );
                    }
                }
                $this->response($results);
            } else {
                $this->response(array('error' => 'No results'));
            }
        } else {
            $fields = array('value', 'type_id', 'datetime');
            $this->response($model, $fields);
        }


    }
} 