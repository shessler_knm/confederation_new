<?php

class Controller_REST_Advice extends API
{

    protected $_methods = array(
        'skip_training',
        'skip_exercise',
        'fast_training',
        'long_training',
        'no_running',
        'common'
    );

    protected $need_auth = true;


    public function action_get_index()
    {
        foreach ($this->_methods as $method) {
            if (method_exists($this, $method)) {
                $this->{$method}();
            }
        }
    }

    public function skip_training()
    {
        $week_exercises = array();
        $ex_count = array();
        $log_count = array();
        $week_days = array();
        $model = ORM::factory('Program')
            ->where('user_id', '=', $this->user->id)
            ->where('activity', '=', 1)
            ->find();
        if ($model->loaded()) {
            $program_exercises = ORM::factory('Program_Exercise')
                ->where('program_id', '=', $model->id)
                ->find_all();
            foreach ($program_exercises as $row) {
                $week_days[$row->week_day] = $row->week_day;
            }
            foreach ($program_exercises as $exercise) {
                $week_exercises[$exercise->week_day][$exercise->exercise_id] = $exercise->id;
            }
            $program_logs = ORM::factory('Program_Log')
                ->where('program_id', '=', $model->id)
                ->limit($program_exercises->count())
                ->find_all();

            $start_date = $model->start_date($week_days);

            if ($start_date < date('Y-m-d')) {
                foreach ($week_exercises as $week => $exs) {
                    $ex_count[$week] = count($exs);
                }
                foreach ($program_logs as $log) {
                    $week = Date::formatted_time($log->date, 'N');
                    $log_count[$week][] = $log->id;
                }
                foreach ($log_count as $week => $exs) {
                    $array[$week] = count($exs);
                }
                foreach ($week_days as $day) {
                    if (!array_key_exists($day, $log_count)) {

//                        $this->response(array('event'=>'skip_training_day'));
                        $this->send_advice("skip_training");
                    } elseif ($log_count[$day] != $ex_count[$day]) {
                        $this->send_advice("skip_exercise");
//                        $this->response(array('event'=>'skip_exercise'));
                    }
                }
            }
        }
    }

    public function send_advice($sef)
    {
        $advice = ORM::factory('Advice');
        if (intval($sef)) {
            $advice = $advice->where('id', '=', $sef)->find();
        } else {
            $advice = $advice->where('sef', '=', $sef)->find();
        }
        if ($advice->loaded()) {
            $this->response($advice, array("title", "text"));
        } else {
            $this->response(array());
        }
    }

    public function common()
    {
        $index = false;
        $advice_id = $this->user->advice_id;
        $collection = ORM::factory('Advice')->where('sef', '=', 'common')->where('title_' . I18n::$lang, '!=', '')->where('text_' . I18n::$lang, '!=', '')->find_all()->as_array('id', 'id');
        foreach ($collection as $row) {
            if ($index) {
                $id = $row;
                $this->user->advice_id = $row;
                $this->user->save();
                break;
            }
            if ($row == $advice_id) {
                $index = true;
            }
        }
        if (!$id) {
            reset($collection);
            $id = current($collection);
            $this->user->advice_id = $id;
            $this->user->save();

        }
        $this->send_advice($id);

    }
}