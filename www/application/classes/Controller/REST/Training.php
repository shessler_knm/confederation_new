<?php

class Controller_REST_Training extends API
{
    protected $need_auth = true;

    public function before()
    {
        require_once(Kohana::find_file('classes', 'Base64image'));
        parent::before();
    }

    public function action_post_save_result()
    {
        $raw_values = $this->raw_values;
        $program = $this->user->programs->where('activity', '=', 1)->find();
        $raw_values['program_id'] = $program->id;
        $raw_values['user_id'] = $this->user->id;
        $valid = Validation::factory($raw_values);
        $valid->rules('date', array(
                array('date'),
                array('not_empty')
            ))
            ->rules('spend_min', array(
                array('not_empty'),
                array('numeric')
            ))
            ->rules('level', array(
                array('not_empty'),
            ));
        if (Arr::get($this->raw_values, 'photo')) {
            $base64image = new Base64image(Arr::get($this->raw_values, 'photo'));
            $file = $base64image->createTmp();
            $image = Cms_Storage::instance()->add($file, date('ymdhis'), array(), 'jpeg');
            $raw_values['photo'] = $image->id;
        }
        if ($valid->check()) {
            $model = $this->model();
            $model->values($raw_values);
            $model->save();
            if ($model->saved()) {
                Feed::feed_training_result($this->user, $model->id);
            }
            $this->response('true');
        } else {
            $this->response(array('error' => $valid->errors(0)));
        }
    }
}