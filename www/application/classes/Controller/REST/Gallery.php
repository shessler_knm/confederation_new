<?php

/**
 * Created by PhpStorm.
 * User: sgm
 * Date: 18.03.14
 * Time: 15:24
 */
class Controller_REST_Gallery extends API
{

    public function action_post_index()
    {
        $data = array();

        $option = Arr::get($this->raw_values, 'option');
        $user_id = Arr::get($option, 'user');
        $programs = ORM::factory('Program')
            ->where('user_id', '=', $user_id)
            ->where('activity', '!=', 2)
            ->limit($this->per_page)
            ->offset($this->offset)
            ->order_by('id', 'DESC');

        foreach ($programs->find_all() as $program) {
            $row = $program->as_array_ext(array('id', 'title'), 'view');
            unset($row['text']);
            unset($row['photo']);
            unset($row['target']);
            unset($row['score']);
            unset($row['subscribers']);
            unset($row['exercises']);
            unset($row['comments_count']);
            $week_days = array();
            $exercises = $program->program_exercises->find_all()->as_array();
            foreach ($exercises as $exercise) {
                $week_days[$exercise->week_day] = $exercise->week_day;
            }
            $row['results'] = array();
            $exercises = $program->program_exercises->order_by('week_day', 'ASC')->find_all()->as_array();
            foreach ($exercises as $exercise) {
                $week_days[$exercise->week_day] = $exercise->week_day;
//                $program_exercises[$exercise->week_day][] = $exercise;
            }
            $row['start_date'] = $week_days ? $program->start_date($week_days) : null;
            $row['end_date'] = $week_days ? $program->end_date($week_days) : null;
            $row['active'] = $program->activity == 1;
            foreach ($program->user_logs->find_all() as $log) {
                $row['results'][] = $log->as_array_ext(array('fat', 'weight', 'date', 'photo'));
            }
            $data[] = $row;

        }

        $this->response($data);
    }

    public function action_post_more()
    {
        $data = array();
        $model = ORM::factory('Program')
            ->where('user_id', '=', Arr::get($this->raw_values, 'user'))
            ->where('activity', '=', '2')
            ->limit($this->per_page)
            ->offset($this->offset);
        foreach ($model->find_all() as $program) {
            $row = $program->as_array_ext(array('id', 'title'));
            unset($row['text']);
            unset($row['photo']);
            unset($row['target']);
            unset($row['score']);
            unset($row['subscribers']);
            unset($row['exercises']);
            unset($row['comments_count']);
            $row['start_date'] = Date::formatted_time($program->start_date, 'd.m.Y');
            $row['end_date'] = $program->end_date();
            $row['results'] = array();
            foreach ($program->user_logs->find_all() as $log) {
                $row['results'][] = $log->as_array_ext(array('fat', 'weight', 'date', 'photo'));
            }
            $data[] = $row;
        }
        $this->response($data);
    }

    public function action_get_result()
    {
        $model = ORM::factory('');
    }

} 