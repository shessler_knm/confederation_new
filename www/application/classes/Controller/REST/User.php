<?php

class Controller_REST_User extends API_User
{

    public function before()
    {
        parent::before();
        require_once(Kohana::find_file('classes', 'Base64image'));
        Event::instance()->bind("api.controller.action.passremind.post_save", array($this, 'passremind_post_save'));
        Event::instance()->bind("api.controller.action.passremind.success", array($this, 'passremind_success'));
        Event::instance()->bind("user.controller.action.reg", array($this, 'reg_success'));
        Event::instance()->bind("user.controller.action.passremind", array($this, 'restore_password'));
        Event::instance()->bind("user.controller.action.passreset", array($this, 'new_password'));

    }

    public function reg_success($model)
    {
        $config = Kohana::$config->load('email.options');
        $text = ORM::factory('Page', array('sef' => 'reg_success'))->text;
        $text = strtr($text, array(':code' => $model->activation_hash,));
        Mail::create($config['from'], $model->email, 'Подтверждение регистрации для приложения "Личный тренер"', $text);

    }

    public function restore_password($model)
    {
        $config = Kohana::$config->load('email.options');
        $text = __('Ваш новый пароль:') . ' ' . $model->password;
        Mail::create($config['from'], $model->email, 'Востановление пароля пользователя сайта Confederation.kz', $text);
    }

    public function new_password($model, $password)
    {
        $config = Kohana::$config->load('email.options');
        $newPassdw = md5(rand(9999, getrandmax()));
        $newPassdw = substr($newPassdw, 0, 8);
        $model->password = $newPassdw;
        if ($model->save()) {
            Event::instance()->dispatch('user.controller.action.new_password', $model, $newPassdw);
            $this->response('true');
        }
        $text = __('Ваш новый пароль:') . ' ' . $newPassdw;
        Mail::create($config['from'], $model->email, 'Новый пароль пользователя сайта Confederation.kz', $text);
    }

    public function passremind_post_save(Model $model = null)
    {
        if (!$model || !$model->loaded()) {
            return false;
        }
        $remind_mail_tpl = View::factory('mail/passremind', array('model' => $model))->render();
        $config = Kohana::$config->load('email.options');
        $subject = 'Password recovery'; // __('Восстановление пароля');
        $mail = ORM::factory('Mail');
        $mail->subject = $subject;
        $mail->text = $remind_mail_tpl;
        $mail->to = $model->email;
        $mail->from = $config['from'];
        $mail->save();
    }

    public function passremind_success(Model $model = null, $new_passwd = null)
    {
        if (!$model || !$model->loaded() || !$new_passwd) {
            return false;
        }
        $remind_mail_tpl = View::factory('mail/passremind-success', array('model' => $model, 'passwd' => $new_passwd))->render();
        $config = Kohana::$config->load('email.options');
        $subject = 'Password recovery success'; // __('Восстановление пароля');
        $mail = ORM::factory('Mail');
        $mail->subject = $subject;
        $mail->text = $remind_mail_tpl;
        $mail->to = $model->email;
        $mail->from = $config['from'];
        $mail->save();
    }

    public function action_post_register()
    {
        $raw_values = $this->body_to_array();
        $raw_values['username'] = $raw_values['email'];
        $model = ORM::factory('User', array('email' => Arr::get($raw_values, 'email')));
        if ($model->loaded() && !$model->has('roles', ORM::factory('Role', array('name' => 'login')))) {
            $this->response(array('error' => 'Email is not activated'));
        } else {
            $model = $this->model();
            $model->values($raw_values);
            $validation = Validation::factory($raw_values);
            $validation->rule('password', 'min_length', array(':value', 8));
            try {
                $model->save($validation);
            } catch (ORM_Validation_Exception $e) {
                foreach ($e->errors() as $label => $error) {
                    if ($label != '_external') {
                        switch ($error[0]) {
                            case 'unique':
                                $this->errors[$label] = "Email is already registered";
                                break;
                            case 'email':
                                $this->errors[$label] = "Email is invalid";
                                break;
                            case 'not_empty':
                                $this->errors[$label] = ucfirst($label) . " cannot be empty";
                                break;
                            default:
                                $this->errors[$label] = $error[0];
                        }
                    }


                }
                if (strlen($raw_values['password']) < 8) {
                    $this->errors['password'] = "Password must be at least 8 symbols";
                }
            }
            if ($model->loaded()) {
                $model->activation_hash = rand(1000, 9999);
                $model->save();
                Event::instance()->dispatch('user.controller.action.reg', $model);
                $this->response('true');
            } else {
                $this->response(array('error' => array_pop($this->errors)));
            }
        }
    }

    public function action_post_login()
    {
//        $this->user_detection();
        $fields = array('id', 'email', 'firstname', 'lastname', 'username', 'birthdate', 'height', 'training');
        if ($this->user && $this->user->loaded()) {
            $model = $this->user;
            $token = $this->user->tokens->last()->token;
            $extra = $this->model_extra_get($model) + array('session' => $token);
            $data = $extra + array('userProfile' => $model->as_array_ext($fields, 'login'));
            $this->response($data, $fields);
        } else {
            $raw_values = $this->body_to_array();
            $token = Arr::get($raw_values, 'token');

            if ($token) {
                $auth = ULogin::check_auth($token);
                if ($auth instanceof ORM) {
                    if (!$this->user) {
                        $this->user = $auth;
                    }
                    $extra = $this->model_extra_get($this->user) + array('session' => $this->user->tokens->last()->token);
                    $data = $extra + array('userProfile' => $auth->as_array_ext($fields, 'login'));
                    $this->response($data, $fields);
                } else {
                    $this->response($auth, $fields);
                }
            } else {
                $email = Arr::get($raw_values, 'email');
                $password = Arr::get($raw_values, 'password');
                $model = ORM::factory('User', array('email' => $email));
                if (Auth::instance()->login($email, $password, true)) {
                    $this->user = Auth::instance()->get_user();
                } else {
                    if ($model->loaded() && !$model->has('roles', ORM::factory('Role', array('name' => 'login')))) {
                        $this->errors['error'] = 'Email is not activated';
                    } elseif (!$this->user) {
                        $this->errors['error'] = 'Incorrect email or password';
                    }

                }
                if ($this->user) {
                    $token = $this->user->tokens->last()->token;
                    $extra = $this->model_extra_get($model) + array('session' => $token);
                    $data = $extra + array('userProfile' => $model->as_array_ext($fields, 'login'));
                    $this->response($data, $fields);
//                $this->response_model($model, $extra);
                } else {
                    $this->response(array('error' => array_pop($this->errors)));
                }
            }
        }
    }

    public function action_post_ureg()
    {

    }

    public function action_post_logout()
    {
        $this->user = Auth::instance()->logout(true);
        $this->response(array('logout' => true));

    }

    public function action_post_reset()
    {
        $model = $this->model();
        $raw_values = $this->body_to_array();
        $message = '';
        if (!array_key_exists('email', $raw_values)) {
            $raw_values['email'] = null;
            $this->errors['email'] = "Email cannot be empty";
        }
        $model->where('email', '=', $raw_values['email'])->find();
        if ($model->loaded()) {
            $password = rand(9999, getrandmax());
            $model->password = $password;
            if ($model->save()) {
                Model_User::_passremind($model->id, $password);
                $this->page->content(
                    View::factory('message_page', array(
                        'msg' => __('На указанный адрес электронной почты выслано письмо с инструкцией по смене пароля.')
                    ))
                );
            }
//            $hash = rand(1000, 9999);
//            $model->passremind_hash = $hash;
//            $model->save();
//            Event::instance()->dispatch('user.controller.action.passremind', $model);
//            $message = __('На указанный адрес электронной почты выслано письмо с инструкцией по смене пароля');
            $this->response('true');
        } else {
            $this->errors['error'] = __('Пользователя с таким e-mail у нас не зарегистрировано') . '!';
            $this->response($this->errors);
        }


    }

    /*
     * Производит, собственно, смену пароля пользователя и отправку ему уведомления об этом
     */
    public function action_post_resetuserpasswd()
    {
        $model = $this->model();
        $key = Arr::get($this->raw_values, 'key');
        if ($key && strlen($key) > 5) {
            $model->where('passremind_hash', '=', $key)->find();
            if ($model->loaded()) {
                $newPassdw = md5($model->email . rand(9999, getrandmax()));
                $newPassdw = substr($newPassdw, 0, 8);
                $model->password = $newPassdw;
                if ($model->save()) {
                    Event::instance()->dispatch('user.controller.action.new_password', $model, $newPassdw);
//                    Event::instance()->dispatch("api.controller.action.passremind.success",$model, $newPassdw);
//                    $message = __('Ваш новый пароль был отправлен на электронный адрес, указанный при регистрации. Никому не сообщайте Ваш пароль.') . "\n";
//                    $message .= "\n" . __('Сейчас Вы будете перенаправлены на страницу авторизации');
                    $this->response('true');
                }
            } else {
//                $this->errors['key'] = __('Invalid code');
                $this->response(array('error' => 'invalid code'));
            }
        };
//        $this->response(array('errors' => $this->errors, 'message' => $message));
    }

    /*
    * Смена пользователем пароля в случае если он забыл свой пароль.
    */
    public function action_post_setuserpasswd()
    {
        $model = $this->model();
        $key = $this->request->query('key');
        $raw_values = $this->body_to_array();
        $message = '';
        $newPassdw = null;
        if ($key && strlen($key) > 5) {
            $model->where('passremind_hash', '=', $key)->find();
            if ($model->loaded()) {
                if ($raw_values['password'] == $raw_values['confirm_password']) {
                    $newPassdw = $raw_values['password'];
                    $model->password = $newPassdw;
                } else {
                    $this->errors['key'] = __('Пароли не совпадают');
                    $model->passremind_hash = '';
                }
                $model->passremind_hash = '';
                if ($model->save() && $newPassdw) {
                    Event::instance()->dispatch("api.controller.action.passremind.success", $model, $newPassdw);
                    $message = __('Ваш новый пароль был отправлен на электронный адрес, указанный при регистрации. Никому не сообщайте Ваш пароль.') . "\n";
//                    $message .= "\n" . __('Сейчас Вы будете перенаправлены на страницу авторизации');
                } else {
                    $this->errors['passwd'] = __('Введенные пароли не совпадают');
                }
            } else {
                $this->errors['key'] = __('Код восстановления не зарегистрирован');
            }
        };
        $this->response(array('errors' => $this->errors, 'message' => $message));
    }

    public function action_get_no_user()
    {
        $this->response(array('error' => 'need auth'));
    }

    public function action_post_confirm()
    {
        $model = $this->model();
        $raw_values = $this->body_to_array();
        $model = $model->where('activation_hash', '=', Arr::get($raw_values, 'code'))->where('email', '=', Arr::get($raw_values, 'email'))->find();
        if ($model->loaded()) {
            $model->activation_hash = null;
            $model->save();
            $role = ORM::factory('Role', array('name' => 'login'));
            $model->add('roles', $role);
            Auth::instance()->force_login($model);
            $this->user = Auth::instance()->get_user();
            if (!$this->user) {
                $this->user = $model;
            }
            $token = $this->user->tokens->find();
            $extra = $this->model_extra_get($model) + array('session' => $token->token);
            $fields = array('id', 'firstname', 'lastname');
            $data = $extra + array('userProfile' => $model->as_array_ext($fields));
            $this->response($data);
        } else {
            $this->response(array('error' => 'wrong activation code'));
            $this->errors = $this->errors + array('code' => 'wrong activation code');
        }

//        $this->response->body(json_encode(array('activation'=>$success,'errors'=>$this->errors)));

    }

    public function action_get_data()
    {
        $fields = array('id', 'email', 'username', 'firstname', 'lastname', 'sex', 'birthdate', 'annotation', 'training', 'height');
        $id = $this->request->param('id');
        if ($id) {
            $model = $this->model($id);
            if ($model->loaded()) {
                $this->response($model, $fields);
            } else {
                $this->response(array('error' => 'User not found'), array(), 404);
            }
        } else {
            $this->response(array('error' => 'User not found'), array(), 404);
        }
    }

    public function action_post_info()
    {
        $option = $this->filters;
        $user_type = Arr::get($option, 'user_type', 'ALL');
        $user_search = Arr::get($option, 'user_search');
        $fields = array('id', 'firstname', 'lastname');
        $collection = $this->model()->where('pers_data', '=', 1);
        if ($user_type == 'TOP') {
            $top = ORM::factory('Top')->order_by('place', 'ASC')->find_all()->as_array('id', 'user_id');
            if ($top) {
                $collection = $collection->where('id', 'in', $top);
            }
        }
        $collection = $collection
            ->where_open()
            ->where('username', 'LIKE', "%$user_search%")
            ->or_where('email', 'LIKE', "%$user_search%")
            ->or_where('firstname', 'LIKE', "%$user_search%")
            ->or_where('lastname', 'LIKE', "%$user_search%")
            ->or_where('middlename', 'LIKE', "%$user_search%")
            ->where_close();
        if ($user_type == 'NEW') {
            $collection = $collection->order_by('id', 'DESC');
        }
        $this->response($collection, $fields);

    }

    public function action_post_profile()
    {
        $model = $this->user;
        $raw_values = $this->body_to_array();
        $option = Arr::get($raw_values, 'settings');
        $photo = Arr::get($raw_values, 'photo');
        if ($model && $model->loaded()) {
            if ($photo) {
                if ($this->parse_url_if_valid($photo)) {
//                    $file = file_get_contents($photo);
//                    $info = pathinfo($photo);
//                    $file_name = $info['basename'];
//                    $file_extension = $info['extension'];
//                    file_put_contents(DOCROOT . '/storage/tmp/' . $file_name . '.' . $file_extension, $file);
//                    $storage = new Cms_Storage();
                } else {
                    //Костыль проверки base64 изображения
//                    $img = imagecreatefromstring(base64_decode($photo));
//                    if ($img === false) {
//                        $this->response(array('error' => 'Invalid image'));
//                        return;
//                    }
                    $base64image = new Base64image(Arr::get($raw_values, 'photo'));
                    $file = $base64image->createTmp();
                    $image = Cms_Storage::instance()->add($file, date('ymdhis'), array(), 'jpeg');
                    $raw_values['photo'] = $image->id;
                    if ($model->photo_s->loaded()) {
                        $model->photo_s->delete();
                    }
                }
            }
            foreach ($this->raw_values as $row => $value) {
                if (!$value) {
                    unset($raw_values[$row]);
                }
            }
            if (Arr::get($raw_values, 'target') && Arr::get($raw_values, 'target_weight')) {
                $mission_log = ORM::factory('Mission_Log');
                $mission_log->user_id = $this->user->id;
                $mission_log->date_begin = date('Y-m-d');
                $mission_log->type = Arr::get($raw_values, 'target');
                $mission_log->weight = Arr::get($raw_values, 'target_weight', $mission_log->weight);
                $mission_log->fat = Arr::get($raw_values, 'target_fat', $mission_log->fat);
                $mission_log->save();
            }
            if (Arr::get($raw_values, 'sex')) {
                if (Arr::get($raw_values, 'sex') == 'M') {
                    $raw_values['sex'] = 1;
                } elseif ((Arr::get($raw_values, 'sex') == 'F')) {
                    $raw_values['sex'] = 2;
                }
            }
            if ($option) {
                unset($raw_values['settings']);
                $raw_values = array_merge($raw_values, $option);
            }
            $model->values($raw_values);
            $model->pers_data = 1;
            try {
                $model->save();
            } catch (ORM_Validation_Exception $e) {
                $this->response->body(json_encode(array('error' => $e)));
            }
            $this->response->body('true');
//            } else {
//                foreach ($profile_validate->errors() as $key => $error) {
//                    $this->errors[$key] = $key . ' cannot be empty';
//                }
//                $this->response->body(json_encode(array('error' => implode(",", $this->errors))));
//            }
        } else {
            $this->response(array('error' => 'User not found'));
        }
    }

    public function action_get_profile()
    {
        $fields = array('id', 'email', 'username', 'firstname', 'lastname', 'sex', 'birthdate', 'training', 'height');
        if ($this->user && $this->user->loaded()) {
            $this->response($this->user, $fields);
        } else {
            $this->response(array('error' => 'Invalid token'), array());
        }
    }

    public function action_post_result()
    {

        $raw_values = $this->body_to_array();
        if ($this->user) {
            $program = $this->user->programs->where('activity', '=', 1)->find();
            if (!Arr::get($this->raw_values, 'image')) {
                $this->response(array('error' => 'no image'));
            } elseif (!$program->loaded()) {
                $this->response(array('error' => 'no active program'));
            } else {
                $base64image = new Base64image(Arr::get($this->raw_values, 'image'));
                $image = $base64image->createTmp();
                $file = Cms_Storage::instance()->add($image);
                $album = $this->user->albums->where('status', '=', 0)->find();
                if (!$album->loaded()) {
                    $album->created = date('Y-m-d');
                    $album->user_id = $this->user->id;

                }
                $album->status = 1;
                $album->save();
                $old_fat = ORM::factory('User_Log')->where('user_id', '=', $this->user->id)->order_by('date', 'DESC')->order_by('id', 'DESC')->find()->fat;
                $raw_values['user_id'] = $this->user->id;
                $raw_values['program_id'] = $program->id;
                $raw_values['status'] = 1;
//            $raw_values['date'] = date('Y-m-d');
                $raw_values['photo'] = $file->id;
                $raw_values['album_id'] = $album->id;
                $model = $this->user->logs;
                $model->values($raw_values);
                try {
                    $model->save();
                    Feed::update_album($this->user->id, $album->id);
                    if (($old_fat - $model->fat) >= 1) {
                        Feed::new_fat($this->user, ($old_fat - round($model->fat, 2)));
                    }
                    //Проверка на заверщение миссии
                    $mission = ORM::factory('Mission_Log')->where('user_id', '=', $this->user->id)->where('status', '=', null)->find();
                    $logs = ORM::factory('User_Log')->where('user_id', '=', $this->user->id)->order_by('date', 'DESC')->order_by('id', 'DESC')->find();
                    if (($logs->fat < $mission->fat && ($mission->type == 'SLIMMING' && $logs->weight < $mission->weight)) || ($mission->type == 'MUSCLE' && $logs->weight > $mission->weight)) {
                        $close_mission_user = ORM::factory('Top')->where('user_id', '=', $this->user)->find();
                        $all_users = ORM::factory('Top')->where('user_id', '!=', $this->user)->find_all();
                        $this->mission_completed($mission, $close_mission_user, $all_users);
                    }
                    $this->response->body('true');
                } catch (ORM_Validation_Exception $e) {
                    $this->response(array('error' => $e));
                }
            }
        } else {
            $this->response(array('error' => 'Invalid token'));
        }

    }

    public function action_delete_result()
    {
        $id = Arr::get($this->raw_values, 'id');
        $model = ORM::factory('User_Log', $id);
        if ($model->loaded() && $model->user_id == $this->user->id) {
            $model->delete();
            $this->response->body('true');
        } else {
            $this->response->body('error');
        }
    }

    public function mission_completed($current_mission, $close_mission_user, $all_users)
    {
        $current_mission->date_end = date('Y-m-d H:i:s');
        $current_mission->status = 1;
        if ($current_mission->save()) {
            if (!$close_mission_user->loaded()) {
                $close_mission_user->user_id = $this->user->id;
            }
            $close_mission_user->place = 1;
            $close_mission_user->save();
            foreach ($all_users as $user) {
                if ($user->place < $close_mission_user->place || $close_mission_user->place == 1) {
                    $user->place += 1;
                    if ($user->place > 5) {
                        $user->delete();
                    } else {
                        $user->save();
                    }
                }
            }
//            Feed::close_mission($this->user);
        };
    }

    public function action_post_change_password()
    {
        $values = $this->raw_values;
        $model = $this->user;
        $error = array();
        if (Auth::instance()->check_password($values['current_password'])) {
            try {
                $model->password = $values['future_password'];
                $model->save();
            } catch (ORM_Validation_Exception $e) {
                $array = $e->errors('validation');
                $error[] = $array['password'];
            }
        } else {
            $error['error'] = 'Invalid password';
        }
        if ($error) {
            $this->response($error);
        } else {
            $this->response('true');
        }
    }

    public function action_post_change_email()
    {

        $values = $this->raw_values;
        $error = array();
        $model = $this->user;
        if (Auth::instance()->check_password($values['password'])) {
            try {
                $model->email = $values['email'];
                $model->save();
            } catch (ORM_Validation_Exception $e) {
                $array = $e->errors('validation');
                $error['error'] = $array['email'];
            }
        } else {
            $error['error'] = 'Invalid password';
        }
        if ($error) {
            $this->response($error);
        } else {
            $this->response('true');
        }
    }

    public function action_post_subscribes()
    {
        $fields = array('id', 'firstname', 'lastname');
        if ($this->user && $this->user->loaded()) {
            $model = $this->user->subscribers->where('status', '=', 1)->offset($this->offset)->limit($this->per_page)->find_all();
            $this->response($model, $fields);
        } else {
            $this->response(array('error' => 'Invalid token'));
        }
    }

    public function action_post_followers()
    {
        $fields = array('id', 'firstname', 'lastname');
        if ($this->user && $this->user->loaded()) {
            $model = $this->user->following->where('status', '=', 1)->offset($this->offset)->limit($this->per_page)->find_all();
            $this->response($model, $fields);
        } else {
            $this->response(array('error' => 'Invalid token'));
        }
    }

    public function action_post_change_data()
    {
        $model = $this->user;
        $raw_values = $this->raw_values;
        $profile_validate = Validation::factory($_POST);
        $profile_validate->rules('firstname', array(
            array('not_empty'),
        ));
        $profile_validate->rules('lastname', array(
            array('not_empty'),
        ));
        $profile_validate->rules('birthdate', array(
            array('not_empty'),
            array('date')
        ));
        if ($profile_validate->check()) {
            $model->values($raw_values, array('firstname', 'lastname', 'annotation', 'sex', 'birthdate'));
            $model->hiding = array_key_exists('hiding', $raw_values) ? 1 : 0;
            $model->save();
        } else {
            $error['error'] = $profile_validate->errors(0);
        }
        if ($error) {
            $this->response($error);
        } else {
            $this->response($model);
        }
//        $params = ORM::factory('User_Log')->where('user_id', '=', $this->user->id)->order_by('date', 'DESC')->order_by('id', 'DESC')->find();
//        $top_user = ORM::factory('Top')->where('user_id', '=', $this->user->id)->find();
    }

    public function action_post_calc()
    {
        $raw_values = $this->raw_values;
        $validate = Validation::factory($raw_values);
        $validate->rules('weight', array(
            array('not_empty')
        ));
        $validate->rules('stomach', array(
            array('not_empty')
        ));
        $validate->rules('chest', array(
            array('not_empty')
        ));
        $validate->rules('haunch', array(
            array('not_empty')
        ));
        $validate->rules('triceps', array(
            array('not_empty')
        ));
        $validate->rules('blade', array(
            array('not_empty')
        ));
        $validate->rules('ilium', array(
            array('not_empty')
        ));
        $validate->rules('armpit', array(
            array('not_empty')
        ));
        if ($validate->check()) {
            $y = 495;
            $z = 450;
            $bd = null;
            $m = $raw_values['chest'] + $raw_values['stomach'] + $raw_values['haunch'] + $raw_values['triceps']
                + $raw_values['blade'] + $raw_values['ilium'] + $raw_values['armpit'];
            $n = pow($m, 2);
            $sex = $this->user->sex;
            $age = $this->user->get_age();
            if ($sex == 1) {
                $bd = 1.1120 - (0.00043499 * $m) + (0.00000055 * $n) - (0.00028826 * $age);
            } else {
                $bd = 1.0970 - (0.00046971 * $m) + (0.00000056 * $n) - (0.00012828 * $age);
            }

            $perfat = ($y / $bd) - $z;

            $bfint = round($perfat);
            $remain = $perfat - $bfint;
            $suff = round($remain * 10);
            $finbf = $bfint + $suff / 10;

            if ($finbf < 0) {
                $error['error'] = 'Negative value - ' . $finbf;
                $this->response($error);
            } else if ($finbf > 85) {
                $error['error'] = 'Value is too big - ' . $finbf;
                $this->response($error);
            } else {
                $this->response($finbf);
            }
        } else {
            $this->response(array('errors' => $validate->errors(0)));
        }

    }

    public function action_post_options()
    {
        $raw_values = $this->raw_values;
        if ($this->user && $this->user->loaded()) {
            $this->user->values($raw_values);
            $this->user->save();
            $this->response('true');
        } else {
            $this->response(array('error' => 'Invalid token'));
        }
    }

    public function action_get_test()
    {
        $this->response(array('status' => 1));
    }
}