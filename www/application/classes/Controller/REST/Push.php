<?php

/**
 * Created by PhpStorm.
 * User: sgm
 * Date: 13.02.14
 * Time: 17:36
 */
class Controller_Rest_Push extends API
{
//    protected $need_auth = true;

    public function action_post_index()
    {
        //id пользователя
        //pushToken
        //client=iOS | Android
        $raw_values = $this->raw_values;
        $user = ORM::factory('User', Arr::get($raw_values, 'user_id'));
        if ($user->loaded()) {
            $model = ORM::factory('Device')->where('user_id', '=', Arr::get($raw_values, 'user_id'))
                ->where('push_token', '=', Arr::get($raw_values, 'push_token'))
                ->where('client', '=', Arr::get($raw_values, 'client'))
                ->find();
            if (!$model->loaded()) {
                try {
                    $model->values($raw_values);
                    $model->save();
                } catch (ORM_Validation_Exception $e) {
                    $this->response(array('error' => $e));
                }
            } else {
                $this->response(array('error' => 'Device is already registered'));
            }
            $this->response('true');
        } else {
            $this->response(array('error' => 'User not found'));
        }
    }

    public function action_get_send_message()
    {
        $push_token = array($this->request->query('push_token'));
        $data = array('message' => 'БУГА ГА');
        if ($this->request->query('client') == 'android') {
            $this->sendGoogleCloudMessage($push_token, $data);
        } elseif ($this->request->query('client') == 'iOS') {
            $this->sendAPNS($push_token, $data);
        } else {
            $this->response(array('error' => 'No client'));
        }
    }

    function sendAPNS($push_token, $data)
    {
        $this->send_ios_push($push_token, $data);
    }

    function sendGoogleCloudMessage($push_token, $data)
    {
        $config = Kohana::$config->load('gcm');
        $apiKey = Arr::get((array)$config, 'api_key');
        $url = Arr::get((array)$config, 'url');
        $post = array(
            'registration_ids' => $push_token,
            'data' => $data,
        );
        $request = new Request($url);
        $request->method(Request::POST);
        $request->headers('Authorization', 'key=' . $apiKey);
        $request->headers('Content-Type', 'application/json');
        $request->body(json_encode($post));
        $response = $request->execute();
        echo $response->body();
    }

    function replace_unicode_escape_sequence($match)
    {
        return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
    }

    function send_ios_push($push_tokens, $message)
    {
        $sound = 'default';
        $config = Kohana::$config->load('apn');
        $config = Arr::get((array)$config, 'development'/*Kohana::$environment*/);
        $payload = array();
        $payload["aps"] = array('alert' => $message["message"], 'sound' => $sound);
        $payload = json_encode($payload);
        $payload = preg_replace_callback('/\\\\u([0-9a-f]{4})/i', array($this, 'replace_unicode_escape_sequence'), $payload);

        $apns_url = Arr::get((array)$config,'server_url');
        $apns_cert = Arr::get((array)$config,'apns_cert');

        if (file_exists(Kohana::find_file("certificates", $apns_cert, false))) {
            echo("cert file exists\n");
        } else {
            echo("cert file not exists\n");;
        }
        $success = 0;
        $stream_context = stream_context_create();
        stream_context_set_option($stream_context, 'ssl', 'local_cert', Kohana::find_file("certificates", $apns_cert, false));
        stream_context_set_option($stream_context, 'ssl', 'passphrase', Arr::get($config,'passphrase'));

        $apns = stream_socket_client($apns_url, $error, $error_string, Arr::get($config,'connect_timeout'), STREAM_CLIENT_CONNECT, $stream_context);

        foreach ($push_tokens as $device) {
            $apns_message = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $device)) . chr(0) . chr(strlen($payload)) . $payload;
            if (fwrite($apns, $apns_message)) {
                $success++;
                echo("sent\n");
            } else {
                echo("failed \n");
            }
        }
        echo("fetch done\n");
        @socket_close($apns);
        fclose($apns);
        return $success;
    }

//
} 