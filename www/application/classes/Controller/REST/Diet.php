<?php

/**
 * Created by PhpStorm.
 * User: id541rak
 * Date: 19.05.14
 * Time: 9:44
 */
class Controller_REST_Diet extends API
{
    public $model_name = 'Dish';

//    protected $need_auth = true;

    public function action_post_new_ingredient()
    {
        $model = ORM::factory('Ingredient');
        $values = array();
        $lang_fields = array('title', 'description_text');
        if (Arr::get($this->raw_values, 'photo')) {
            $base64image = new Base64image(Arr::get($this->raw_values, 'photo'));
            $file = $base64image->createTmp();
            $image = Cms_Storage::instance()->add($file, date('ymdhis'), array(), 'jpeg');
            $this->raw_values['photo'] = $image->id;
        }
        foreach ($this->raw_values as $key => $value) {
            if (array_search($key, $lang_fields) !== false) {
                if ($key == 'description_text') {
                    $key = 'description_' . I18n::$lang;
                } else {
                    $key = $key . '_' . I18n::$lang;
                }

            }
            $values[$key] = $value;
        }
        $model->values($values);
        try {
            $model->save();
        } catch (ORM_Validation_Exception $e) {
            $this->response(array('error' => $e->errors(0)));
        }
        $this->response('true');
    }

    public function action_post_new_recipe()
    {
        $raw_values = $this->raw_values;
        $model = ORM::factory('Dish');
        $values = array();
        $lang_fields = array('title', 'description_text');
        if (Arr::get($raw_values, 'photo')) {
            $base64image = new Base64image(Arr::get($raw_values, 'photo'));
            $file = $base64image->createTmp();
            $image = Cms_Storage::instance()->add($file, date('ymdhis'), array(), 'jpeg');
            $this->raw_values['photo'] = $image->id;
        }
        foreach ($raw_values as $key => $value) {
            if (array_search($key, $lang_fields) !== false) {
                if ($key == 'description_text') {
                    $key = 'description_' . I18n::$lang;
                } else {
                    $key = $key . '_' . I18n::$lang;
                }
            }
            if ($key == 'category_id') {
                $value = ORM::factory('Category', array('slug' => $value))->id;
                $key = 'category_id';
            }
            $values[$key] = $value;
        }
        $model->values($values, array('title_' . I18n::$lang, 'description_' . I18n::$lang, 'photo', 'category_id'));
        try {
            $model->save();
            foreach ($raw_values['ingredients'] as $ingredient) {
                $dish_ingredient = $model->dish_ingredients;
                $dish_ingredient->dish_id = $model->id;
                $dish_ingredient->ingredient_id = $ingredient->ingredient_id;
                $dish_ingredient->weight = $ingredient->weigth;
                $dish_ingredient->save();
            }
        } catch (ORM_Validation_Exception $e) {
            $this->response(array('error' => $e->errors(0)));
        }
        $this->response('true');
    }

    public function action_post_index()
    {
        $fields = array('id',
            'title',
            'description',
            'photo',
            'protein',
            'carbohydrate',
            'fat',
            'calories',
            'dish_ingredients'
        );

        $raw_values = Arr::get($this->raw_values, 'option');
        $slug = Arr::get($raw_values, 'category');
        $q = Arr::get($raw_values, 'query');

        $model = ORM::factory('Dish')->where('is_published', '=', 1)
            ->where_open()
            ->where('description_ru', 'LIKE', "%$q%")
            ->or_where('description_kz', 'LIKE', "%$q%")
            ->or_where('description_en', 'LIKE', "%$q%")
            ->or_where('title_kz', 'LIKE', "%$q%")
            ->or_where('title_ru', 'LIKE', "%$q%")
            ->or_where('title_en', 'LIKE', "%$q%")
            ->where_close()
            ->where('description_' . I18n::$lang, '!=', '')
            ->where('title_' . I18n::$lang, '!=', '')
            ->where('calories', '>=', Arr::get($raw_values, 'min_calories', 0))
            ->where('calories', '<=', Arr::get($raw_values, 'max_calories', 900))
            ->where('protein', '>=', Arr::get($raw_values, 'min_proteins', 0))
            ->where('protein', '<=', Arr::get($raw_values, 'max_proteins', 50))
            ->where('carbohydrate', '>=', Arr::get($raw_values, 'min_carbs', 0))
            ->where('carbohydrate', '<=', Arr::get($raw_values, 'max_carbs', 100))
            ->where('fat', '>=', Arr::get($raw_values, 'min_fats', 0))
            ->where('fat', '<=', Arr::get($raw_values, 'max_fats', 100));

        if ($slug) {
            $category = ORM::factory('Category', array('slug' => $slug));
            if ($category->loaded()) {
                $model = $model->where('category_id', '=', $category->id);
            } else {
                $this->response(array('error' => 'Category is not valid'));
                return;
            }
        }

        $this->response($model, $fields);
    }

    public function action_post_change_time()
    {
        $datetime = Arr::get($this->raw_values, 'datetime');
        $new_time = Arr::get($this->raw_values, 'newtime');
        $model = ORM::factory('Ration')->where('datetime', '=', $datetime)->where('user_id', '=', $this->user->id)->find();
        if ($model->loaded()) {
            $model->datetime = $new_time;
            $model->save();
            $this->response('true');
        } else {
            $this->response('ration not found');
        }
    }

    public function action_post_ration_values()
    {
        $data = array('energy_values' => array(), 'norm_values' => array(), 'differences' => array());
        $energy_values = array('calories' => 0, 'protein' => 0, 'carbohydrate' => 0, 'fat' => 0);
        $energy_norms = array('calories' => 0, 'protein' => 0, 'carbohydrate' => 0, 'fat' => 0);
        $differences = array();
        $date = Arr::get($this->raw_values, 'date');
        $model = ORM::factory('Ration')->where('user_id', '=', $this->user->id)->where(DB::expr("DATE_FORMAT(datetime,'%Y-%m-%d')"), '=', $date)->find_all();
        foreach ($model as $row) {
            $dish_rations = $row->dish_rations->find_all();
            foreach ($dish_rations as $dish_ration) {
                $dish = $dish_ration->dish;
                $energy_values['calories'] += round(($dish->calories * $dish_ration->weight) / 100, 2);
                $energy_values['protein'] += round(($dish->protein * $dish_ration->weight) / 100, 2);
                $energy_values['carbohydrate'] += round(($dish->carbohydrate * $dish_ration->weight) / 100, 2);
                $energy_values['fat'] += round(($dish->fat * $dish_ration->weight) / 100, 2);
            }
        }
        $user_norm = $this->user->norms->where('date', '=', $date)->find();
        if ($user_norm->loaded()) {
            $energy_norms['calories'] = $user_norm->calories;
            $energy_norms['protein'] = $user_norm->protein;
            $energy_norms['carbohydrate'] = $user_norm->carbohydrate;
            $energy_norms['fat'] = $user_norm->fat;
        } else {
            //2 - средний уровень нагрузок. Выбирается по умолчанию, если уровень нагрузок еще не выбран
            $energy_norms = $this->calculate_user_norms(2);
        }

        $differences['calories'] = $energy_values['calories'] - $energy_norms['calories'];
        $differences['protein'] = $energy_values['protein'] - $energy_norms['protein'];
        $differences['carbohydrate'] = $energy_values['carbohydrate'] - $energy_norms['carbohydrate'];
        $differences['fat'] = $energy_values['fat'] - $energy_norms['fat'];

        $data['energy_values'] = $energy_values;
        $data['norm_values'] = $energy_norms;
        $data['differences'] = $differences;
        $this->response($data);
    }

    public function action_post_ingredients()
    {
        $option = Arr::get($this->raw_values, 'option');
        $q = Arr::get($option, 'query');
        $fields = array('id',
            'title',
            'description',
            'photo',
            'protein',
            'carbohydrate',
            'fat',
            'calories',
        );
        $model = ORM::factory('Ingredient')->where('is_published', '=', 1);
        if ($q) {
            $model->where_open()
                ->where('title_' . I18n::$lang, 'like', "%$q%")
                ->or_where('description_' . I18n::$lang, 'like', "%$q%")
                ->where_close();
        }
        $this->response($model, $fields);
    }

    public function action_get_view()
    {
        $id = $this->request->param('id');
        $model = ORM::factory($this->model_name, $id);
        if ($model->loaded()) {
            $this->response($model);
        } else {
            $this->response(array('error' => 'Invalid id'));
        }
    }

    public function action_post_my_diet()
    {
        $data = array('rations' => array(), 'energy_values' => array(), 'norm_values' => array(), 'differences' => array());
        $energy_values = array('calories' => 0, 'protein' => 0, 'carbohydrate' => 0, 'fat' => 0);
        $energy_norms = array('calories' => 0, 'protein' => 0, 'carbohydrate' => 0, 'fat' => 0);
        $differences = array();
        $date = Arr::get($this->raw_values, 'date');

        if ($date) {
            $date = Date::formatted_time($date, 'Y-m-d');
            $model = ORM::factory('Ration')
                ->where('user_id', '=', $this->user->id)
                ->where(DB::expr("DATE_FORMAT(datetime,'%Y-%m-%d')"), '=', $date)
                ->order_by('datetime', 'ASC')
                ->find_all();
            foreach ($model as $row) {
                $ration = array();
                $dish_rations = $row->dish_rations->find_all();
                foreach ($dish_rations as $dish_ration) {
                    $dish = $dish_ration->dish;
                    $ration[] = array(
                        'id' => $dish_ration->id,
                        'title' => $dish->title,
                        'calories' => round(($dish->calories * $dish_ration->weight) / 100, 2),
                        'protein' => round(($dish->protein * $dish_ration->weight) / 100, 2),
                        'carbohydrate' => round(($dish->carbohydrate * $dish_ration->weight) / 100, 2),
                        'fat' => round(($dish->fat * $dish_ration->weight) / 100, 2),
                        'weight' => $dish_ration->weight,
                    );
                    $energy_values['calories'] += round(($dish->calories * $dish_ration->weight) / 100, 2);
                    $energy_values['protein'] += round(($dish->protein * $dish_ration->weight) / 100, 2);
                    $energy_values['carbohydrate'] += round(($dish->carbohydrate * $dish_ration->weight) / 100, 2);
                    $energy_values['fat'] += round(($dish->fat * $dish_ration->weight) / 100, 2);
                }
                $data['rations'][] = array('time' => Date::formatted_time($row->datetime, 'H:i'), 'dishes' => $ration);
            }

            $user_norm = $this->user->norms->where('date', '=', $date)->find();
            if ($user_norm->loaded()) {
                $energy_norms['calories'] = $user_norm->calories;
                $energy_norms['protein'] = $user_norm->protein;
                $energy_norms['carbohydrate'] = $user_norm->carbohydrate;
                $energy_norms['fat'] = $user_norm->fat;
            } else {
                //2 - средний уровень нагрузок. Выбирается по умолчанию, если уровень нагрузок еще не выбран
                $energy_norms = $this->calculate_user_norms(2);
            }

            $differences['calories'] = $energy_values['calories'] - $energy_norms['calories'];
            $differences['protein'] = $energy_values['protein'] - $energy_norms['protein'];
            $differences['carbohydrate'] = $energy_values['carbohydrate'] - $energy_norms['carbohydrate'];
            $differences['fat'] = $energy_values['fat'] - $energy_norms['fat'];

            $data['energy_values'] = $energy_values;
            $data['norm_values'] = $energy_norms;
            $data['differences'] = $differences;
            $this->response($data);
        } else {
            $this->response(array('error' => 'Invalid time'));
        }
    }

    public function calculate_user_norms($level)
    {
        $norms = array();
        if ($this->user->sex == 1) {
            $bmr = 88.36 + (13.4 * $this->user->weight) + (4.8 * $this->user->height) - (5.7 * $this->user->get_age());
        } else {
            $bmr = 447.6 + (9.2 * $this->user->weight) + (3.1 * $this->user->height) - (4.3 * $this->user->get_age());
        }
        $cal_levels = array(1.2, 1.4, 1.6, 1.8, 2.0);
        $prt_levels = array(1, 1.25, 1.5, 1.75, 2);
        $k_cal = Arr::get($cal_levels, $level);
        $k_prt = Arr::get($prt_levels, $level);
        $cal_result = round($bmr * $k_cal);
        $prt_result = round($this->user->weight * $k_prt);
        $fat_result = round($cal_result / 27.5);
        $crb_result = round(($cal_result - 4.1 * $prt_result - 9 * $fat_result) / 3.5);
        $mission = $this->user->mission_logs->order_by('date_begin', 'DESC')->order_by('id', 'DESC')->find()->type;
        switch ($mission) {
            case 'MUSCLE':
                $cal_result = round($cal_result * 1.2);
                $prt_result = round($prt_result * 1.2);
                $fat_result = round($fat_result * 1.2);
                $crb_result = round($crb_result * 1.2);
                break;
            case 'SLIMMING':
                $cal_result = round($cal_result * 1.2);
                $prt_result = round($prt_result * 1.2);
                $fat_result = round($fat_result * 1.2);
                $crb_result = round($crb_result * 1.2);
                break;
        }
        $norms['calories'] = $cal_result;
        $norms['protein'] = $prt_result;
        $norms['carbohydrate'] = $crb_result;
        $norms['fat'] = $fat_result;
        return $norms;
    }

    public function action_post_user_norms()
    {
        $level = Arr::get($this->raw_values, 'level', 0);
        $norms = $this->calculate_user_norms($level);
        $norms['date'] = Arr::get($this->raw_values, 'date');
        $norms['level'] = Arr::get($this->raw_values, 'level');
        $model = ORM::factory('User_Norm')->where('date', '=', $norms['date'])->where('user_id', '=', $this->user->id)->find();
        $model->values($norms);
        $model->user_id = $this->user->id;
        $model->save();
        $this->response($norms);
    }

    public function action_post_delete()
    {
        $datetime = Arr::get($this->raw_values, 'datetime');
        $id = Arr::get($this->raw_values, 'id');
        if ($id) {
            $dish_ration = ORM::factory('Dish_Ration', $id);
            if ($dish_ration->loaded()) {
                $dish_ration->delete();
                $this->response('true');
            } else {
                $this->response(array('error' => 'dish not found'));
            }
        } else {
            $ration = ORM::factory('Ration')->where(DB::expr("DATE_FORMAT(datetime,'%Y-%m-%d %H:%i')"), '=', $datetime)->where('user_id', '=', $this->user->id)->find();
            if ($ration->loaded()) {
                $ration->delete();
                $this->response('true');
            } else {
                $this->response(array('error' => 'ration not found'));
            }
        }
    }

    public function action_post_statistics()
    {
        $data = array('days_and_values' => array(), 'days_and_norms' => array(), 'value_differences' => array());
        $values = array();
        $norms_array = array();
        $value_differences = array();
        $example = $this->request->param('id');
        $selected_month = Arr::get($this->raw_values, 'date');
        $selected_month = Date::formatted_time($selected_month, 'Y-m');
        $type = Arr::get($this->raw_values, 'type');
        $types = array('CALORIES', 'PROTEIN', 'CARBOHYDRATE', 'FAT');
        //заглушка
        if ($example) {
            $days = cal_days_in_month(CAL_GREGORIAN, date('m'), date('y'));
            for ($x = 0; $x <= $days; $x++) {
                $date = Date::formatted_time(date('y') . '-' . date('m') . '-' . $x, 'Y-m-d');
                switch ($type) {
                    case 'calories' || 'CALORIES':
                        $data['days_and_values'][$date] = rand(2000, 2200);
                        $data['days_and_norms'][$date] = rand(2000, 2200);
                        break;
                    case 'protein' || 'PROTEIN':
                        $data['days_and_values'][$date] = rand(100, 120);
                        $data['days_and_norms'][$date] = rand(100, 120);
                        break;
                    case 'carbohydrate' || 'CARBOHYDRATE':
                        $data['days_and_values'][$date] = rand(400, 500);
                        $data['days_and_norms'][$date] = rand(400, 500);
                        break;
                    case 'fat' || 'FAT':
                        $data['days_and_values'][$date] = rand(50, 100);
                        $data['days_and_norms'][$date] = rand(50, 100);
                        break;
                }
                $data['value_differences']['CALORIES'] = rand(-200, 200);
                $data['value_differences']['PROTEIN'] = rand(-10, 10);
                $data['value_differences']['CARBOHYDRATE'] = rand(-50, 50);
                $data['value_differences']['FAT'] = rand(-10, 10);
            }
        } else {
            if ($type) {
                $norms = $this->user->norms->where(DB::expr("DATE_FORMAT(date,'%Y-%m')"), '=', $selected_month)->find_all();
                foreach ($norms as $norm) {
                    $data['days_and_norms'][] = array('value' => (int)$norm->{mb_strtolower($type)}, 'date' => Date::formatted_time($norm->date, 'Y-m-d'));
                    $norms_array[Date::formatted_time($norm->date, 'Y-m-d')] = (int)$norm->{mb_strtolower($type)};
                }

                $rations = $this->user->rations->where(DB::expr("DATE_FORMAT(datetime,'%Y-%m')"), '=', $selected_month)->find_all();
                foreach ($rations as $ration) {
                    $dish_rations = $ration->dish_rations->find_all();
                    foreach ($dish_rations as $dish_ration) {
                        if (isset($dish_ration->dish->{mb_strtolower($type)})) {
                            $values[] = round(($dish_ration->weight * $dish_ration->dish->{mb_strtolower($type)}) / 100);
                        } else {
                            $this->response(array('error' => 'type is incorrect'));
                            return;
                        }
                    }
                    if (Date::formatted_time($ration->datetime, 'Y-m-d') == date('Y-m-d')) {
                        foreach ($types as $nutr_type) {
                            $today_total[$nutr_type] = 0;

                            foreach ($dish_rations as $dish_ration) {
                                $today_total[$nutr_type] += round(($dish_ration->weight * $dish_ration->dish->{mb_strtolower($nutr_type)}) / 100);
                            }

                            $model = $this->user->norms->where('date', '=', date('Y-m-d'))->find();

                            if ($model->loaded()) {
                                $today_norm = $model->{mb_strtolower($nutr_type)};
                            } else {
                                $today_norm = 0;
                            }

                            $value_differences[$nutr_type] = $today_total[$nutr_type] - $today_norm;
                        }
                    }
                    $data['days_and_values'][] = array(
                        'value' => (int)array_sum($values),
                        'date' => Date::formatted_time($ration->datetime, 'Y-m-d'),
                        'normValue' => isset($norms_array[Date::formatted_time($ration->datetime, 'Y-m-d')]) ? $norms_array[Date::formatted_time($ration->datetime, 'Y-m-d')] : 0
                    );
                    $values = array();
                }

                //Если нет данных
                if (!$value_differences) {
                    foreach ($types as $nutr_type) {
                        $value_differences[$nutr_type] = 0;
                    }
                }
                $data['value_differences'] = $value_differences;

                if ($data) {
                    $this->response($data);
                } else {
                    $this->response(array('error' => 'No data'));
                }
            } else {
                $this->response(array('error' => 'Invalid type'));
            }

        }
    }

    public function action_post_add_dish()
    {
        $raw_values = $this->raw_values;
        $date = Date::formatted_time(Arr::get($raw_values, 'date'), 'Y-m-d');
        $time = Date::formatted_time(Arr::get($raw_values, 'time'), 'H:i');
        $datetime = $date . ' ' . $time;
        $dish_id = Arr::get($raw_values, 'dish_id');
        $ration = ORM::factory('Ration')->where('datetime', '=', $datetime)->find();
        if (!$ration->loaded()) {
            $ration->user_id = $this->user->id;
            $ration->datetime = $datetime;
            $ration->save();
        }
        if ($dish_id) {
            $dish_ration = ORM::factory('Dish_Ration');
            $dish_ration->dish_id = Arr::get($raw_values, 'dish_id');
            $dish_ration->ration_id = $ration->id;
            $dish_ration->weight = Arr::get($raw_values, 'weight');
            try {
                $dish_ration->save();
            } catch (ORM_Validation_Exception $e) {
                $this->response(array('error' => $e->errors(0)));
            }
        }
        $this->response('true');


    }

    public function action_get_dish_ingredients()
    {
        $id = $this->request->param('id');
        $model = ORM::factory('Dish', $id);
        if ($model->loaded()) {
            $collection = $model->dish_ingredients;
            $this->response($collection);
        } else {
            $this->response(array('error' => 'No such dishes'));
        }
    }

    public function action_post_statisticsOld()
    {
        $data = array('days_and_values' => array(), 'days_and_norms' => array(), 'value_differences' => array());
        $values = array();
        $value_differences = array();
        $example = $this->request->param('id');
        $selected_month = Arr::get($this->raw_values, 'date');
        $selected_month = Date::formatted_time($selected_month, 'Y-m');
        $type = Arr::get($this->raw_values, 'type');
        $types = array('CALORIES', 'PROTEIN', 'CARBOHYDRATE', 'FAT');
        //заглушка
        if ($example) {
            $days = cal_days_in_month(CAL_GREGORIAN, date('m'), date('y'));
            for ($x = 0; $x <= $days; $x++) {
                $date = Date::formatted_time(date('y') . '-' . date('m') . '-' . $x, 'Y-m-d');
                switch ($type) {
                    case 'calories' || 'CALORIES':
                        $data['days_and_values'][$date] = rand(2000, 2200);
                        $data['days_and_norms'][$date] = rand(2000, 2200);
                        break;
                    case 'protein' || 'PROTEIN':
                        $data['days_and_values'][$date] = rand(100, 120);
                        $data['days_and_norms'][$date] = rand(100, 120);
                        break;
                    case 'carbohydrate' || 'CARBOHYDRATE':
                        $data['days_and_values'][$date] = rand(400, 500);
                        $data['days_and_norms'][$date] = rand(400, 500);
                        break;
                    case 'fat' || 'FAT':
                        $data['days_and_values'][$date] = rand(50, 100);
                        $data['days_and_norms'][$date] = rand(50, 100);
                        break;
                }
                $data['value_differences']['CALORIES'] = rand(-200, 200);
                $data['value_differences']['PROTEIN'] = rand(-10, 10);
                $data['value_differences']['CARBOHYDRATE'] = rand(-50, 50);
                $data['value_differences']['FAT'] = rand(-10, 10);
            }
        } else {
            if ($type) {
                $norms = $this->user->norms->where(DB::expr("DATE_FORMAT(date,'%Y-%m')"), '=', $selected_month)->find_all();
                foreach ($norms as $norm) {
//                    $data['days_and_norms'][Date::formatted_time($norm->date, 'Y-m-d')] = (int)$norm->{$type};
                    $data['days_and_norms'][Date::formatted_time($norm->date, 'Y-m-d')] = (int)$norm->{mb_strtolower($type)};
                }

                $rations = $this->user->rations->where(DB::expr("DATE_FORMAT(datetime,'%Y-%m')"), '=', $selected_month)->find_all();
                foreach ($rations as $ration) {
                    $dish_rations = $ration->dish_rations->find_all();
                    foreach ($dish_rations as $dish_ration) {
                        if (isset($dish_ration->dish->{mb_strtolower($type)})) {
                            $values[] = round(($dish_ration->weight * $dish_ration->dish->{mb_strtolower($type)}) / 100);
                        } else {
                            $this->response(array('error' => 'type is incorrect'));
                            return;
                        }
                    }
                    if (Date::formatted_time($ration->datetime, 'Y-m-d') == date('Y-m-d')) {
                        foreach ($types as $nutr_type) {
                            $today_total[$nutr_type] = 0;

                            foreach ($dish_rations as $dish_ration) {
                                $today_total[$nutr_type] += round(($dish_ration->weight * $dish_ration->dish->{mb_strtolower($nutr_type)}) / 100);
                            }

                            $model = $this->user->norms->where('date', '=', date('Y-m-d'))->find();

                            if ($model->loaded()) {
                                $today_norm = $model->{mb_strtolower($nutr_type)};
                            } else {
                                $today_norm = 0;
                            }

                            $value_differences[$nutr_type] = $today_total[$nutr_type] - $today_norm;
                        }
                    }
                    $data['days_and_values'][Date::formatted_time($ration->datetime, 'Y-m-d')] = (int)array_sum($values);
                    $values = array();
                }

                //Если нет данных
                if (!$value_differences) {
                    foreach ($types as $nutr_type) {
                        $value_differences[$nutr_type] = 0;
                    }
                }
                if (!$data['days_and_values']) {
                    $data['days_and_values'] = (object)$data['days_and_values'];
                }
                if (!$data['days_and_norms']) {
                    $data['days_and_norms'] = (object)$data['days_and_norms'];
                }
                $data['value_differences'] = $value_differences;

                if ($data) {
                    $this->response($data);
                } else {
                    $this->response(array('error' => 'No data'));
                }
            } else {
                $this->response(array('error' => 'Invalid type'));
            }

        }
    }

}