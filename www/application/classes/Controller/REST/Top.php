<?php

class Controller_REST_Top extends API
{

    public function action_get_index(){
        $collection = $this->model();
        $collection = $collection->find_all();
        $data = array();
        $fields = array('user_id','place');
        foreach ($collection as $row){
            $data[] = $row->as_array_ext($fields);
        }
        $this->response($data);
    }
}