<?php

/**
 * Created by PhpStorm.
 * User: sgm
 * Date: 11.02.14
 * Time: 11:25
 */
class Controller_REST_Program extends API
{
    protected $need_auth = true;

    public function before()
    {
        if ($this->request->action() == 'post_test') {
            $this->need_auth = false;
        }
        parent::before();
        require_once(Kohana::find_file('classes', 'Base64image'));
    }

    public function action_post_test()
    {
        $headers = $this->request->headers();
        $this->response(array('headers' => $headers, 'body' => $this->raw_values));
    }


    public function action_post_index()
    {
        if ($this->user) {
            $raw_values = $this->body_to_array();
            $collection = $this->model();
            $collection = $collection->where('type', '=', 'free');
            if (count($this->filters)) {
                foreach ($this->filters as $key => $value) {
                    switch ($key) {
                        default:
                            $collection->where($key, '=', $value);

                        case "target":
                            if ($value) {
                                $collection->where('mission_id', '=', ORM::factory('Mission', array('sef' => $value)));
                            }
                            break;

                        case "place":
                            if ($value) {
                                $value = strtolower($value);
                                $collection->where('place', '=', $value);
                            }
                            break;

                        case "part":
                            if ($value) {
                                $collection->join('program_categories', 'right')->on('program_categories.program_id', '=', 'program.id')
                                    ->join('categories', 'right')->on('program_categories.category_id', '=', 'categories.id')
                                    ->where('categories.id', '=', $value);
                            }
                            break;

                        case "classInWeek":
                            if ($value) {
                                $programs = ORM::factory('Program')->where('type', '=', 'free')->find_all();
                                $session = array();
                                foreach ($programs as $program) {
                                    $exercises = $program->program_exercises->find_all()->as_array('week_day', 'program_id');
                                    if (count($exercises)) {
                                        $session[count($exercises)][] = $program->id;
                                    }

                                }
                                if (count($session)) {
                                    $collection->where('program.id', 'in', $session[$value]);
                                }
                            }
                            break;

                    }
                }
            }
            switch ($this->user->sex) {
                case 1:
                    $filter_sex = 2;
                    break;
                case 2:
                    $filter_sex = 1;
                    break;
                default:
                    $filter_sex = null;
            }

            $data = array();
            $fields = array('id', 'title', 'photo', 'target', 'score', 'subscribers');
            $collection = $collection->where('sex', '!=', $filter_sex);
            $this->response($collection, $fields);
        } else {
            $this->response(array('error' => 'Invalid token'), array());
        }
    }

    public function action_get_current()
    {
        $model = $this->model();
        $model = $model->where('user_id', '=', $this->user->id)->where('activity', '=', 1)->find();
        if ($model->loaded()) {
            $this->response($model);
        } else {
            $this->response(array('error' => 'No program'));
        }
    }


    public function action_post_edit()
    {
        $id = (int)Arr::get($this->raw_values, 'program_id') ? (int)Arr::get($this->raw_values, 'program_id') : (int)$this->request->param('id');
        if (!$id) {
            $this->response(array('error' => 'no program_id'));
        } else {
            $model = $this->model($id);
            if (!$model->loaded()) {
                $this->response(array('error' => 'no such program'));
            } else {
                if ($model->user_id != $this->user->id) {
                    $this->response(array('error' => 'access denied'));
                } else {
                    $model->values($this->raw_values);
                    try {
                        $model->save();
                        $this->save_days($model, Arr::get($this->raw_values, 'days'));
                        $this->response($model);
                    } catch (ORM_Validation_Exception $e) {
                        $this->response(array('error' => implode(",", $e->errors())));
                    }
                }
            }
        }
    }

    public function action_post_statistics()
    {
        $fields = array('id', 'user_id', 'program_id', 'weight');
        $data = array('days_and_values' => array());
        $current_user_program = $this->user->programs->where('activity', '=', 1)->find();
//        $example = $this->request->param('id2');
//        if ($example) {
//            $days = cal_days_in_month(CAL_GREGORIAN, date('m'), date('y'));
//            for ($x = 0; $x <= $days; $x++) {
//                $date = Date::formatted_time(date('y') . '-' . date('m') . '-' . $x, 'Y-m-d');
//                $data['days_and_values'][$date] = rand(50, 55);
//            }
//            $this->response($data);
//        } else {
        if ($current_user_program->loaded()) {
            $selected_month = Arr::get($this->raw_values, 'date');
            $selected_month = Date::formatted_time($selected_month, 'Y-m');
            $selected_month_logs = $this->user->logs->where(DB::expr("DATE_FORMAT(date,'%Y-%m')"), '=', $selected_month)->order_by('id', 'DESC')->find_all();
            foreach ($selected_month_logs as $log) {
                $data['days_and_values'][] = array('value' => $log->weight, 'date' => Date::formatted_time($log->date, 'Y-m-d'));
            }
            if ($data['days_and_values']) {
//                $data['days_and_values'] = (object)$data['days_and_values'];
                $this->response($data, $fields);
            } else {
                $this->response(array('error' => 'No data'));
            }
        } else {
            $this->response(array('error' => 'No active program'));
        }
//        }
    }

    public function action_post_statisticsOld()
    {
        $fields = array('id', 'user_id', 'program_id', 'weight');
        $data = array('days_and_values' => array());
        $current_user_program = $this->user->programs->where('activity', '=', 1)->find();
        if ($current_user_program->loaded()) {
            $selected_month = Arr::get($this->raw_values, 'date');
            $selected_month = Date::formatted_time($selected_month, 'Y-m');
            $selected_month_logs = $this->user->logs->where(DB::expr("DATE_FORMAT(date,'%Y-%m')"), '=', $selected_month)->order_by('id', 'DESC')->find_all();
            foreach ($selected_month_logs as $log) {
                $data['days_and_values'][Date::formatted_time($log->date, 'Y-m-d')] = $log->weight;
            }
            if ($data['days_and_values']) {
                $data['days_and_values'] = (object)$data['days_and_values'];
                $this->response($data, $fields);
            } else {
                $this->response(array('error' => 'No data'));
            }
        } else {
            $this->response(array('error' => 'No active program'));
        }
//        }
    }


    public function action_get_current_data()
    {
        $first_data = null;
        $last_data = null;
        $current_user_program = $this->user->programs->where('activity', '=', 1)->find();
        if ($current_user_program->loaded()) {
            $current_program_logs = $this->user->logs->where('program_id', '=', $current_user_program->id)->find_all();
            if ($current_program_logs->count() > 1) {
                $this->response($this->calculate_statistic($current_program_logs));
            } else {
                $this->response(array('error' => 'little data'));
            }
        } else {
            $this->response(array('error' => 'No active program'));
        }
    }

    public function action_get_common_data()
    {

        $current_program_logs = $this->user->logs->find_all();
        if ($current_program_logs->count() > 1) {
            $response = $this->calculate_statistic($current_program_logs);
        } else {
            $response = array('error' => 'little data');
        }
        $this->response($response);
    }

    public function calculate_statistic($current_program_logs)
    {
        $first_data = null;
        $last_data = null;
        foreach ($current_program_logs as $log) {
            if ($first_data === null) {
                $first_data = $log->weight;
            } else {
                $last_data = $log->weight;
            }
        }
        $absolute_data = $last_data - $first_data;
        $relative_data = round((($last_data / $first_data) * 100) - 100);
        if ($absolute_data >= 0) {
            $absolute_data = '+' . $absolute_data;
            $relative_data = '+' . $relative_data . '%';
        } else {
            $absolute_data = strval($absolute_data);
            $relative_data = $relative_data . '%';
        }
        $response = array('absolute_data' => $absolute_data, 'relative_data' => $relative_data);
        return $response;
    }

    public function action_delete()
    {

    }

    public function action_post_new()
    {
        $id = (int)Arr::get($this->raw_values, 'program_id') ? (int)Arr::get($this->raw_values, 'program_id') : (int)$this->request->param('id');
        $date = Arr::get($this->raw_values, 'date');
        $days = Arr::get($this->raw_values, 'days', array());

        if ($id) {
            $model = $this->model($id);
            if ($model->loaded()) {
                $program = $this->model();
                $data = $model->as_array();
                unset($data['subscribers']);
                unset($data['score']);
                $program->values($data);
                $program->user_id = $this->user->id;
                $program->start_date = $date;
                $program->categories = $model->categories->find_all()->as_array('id', 'id');
                $program->parent_id = $model->id;
                $program->type = 'copy';
                $program->activity = 1;
                try {
                    $program->save();
                    $this->save_days($program, $days);
                    $collection = ORM::factory('Program')->where('user_id', '=', $program->user_id)->where('id', '!=', $program->id)->where('activity', '=', 1)->find_all();
                    foreach ($collection as $m) {
                        $m->activity = 2;
                        $m->save();
                    }
                    $this->response($program);
                } catch (ORM_Validation_Exception $e) {
                    $this->response(array('error' => implode(",", $e->errors())));
                }
            } else {
                $this->response(array('error' => 'no such program'));
            }
        } else {
            $this->response(array('error' => 'no program_id'));
        }
    }

    public function action_get_view()
    {
        $id = $this->request->param('id');
        $model = $this->model($id);
        if (!$model->loaded()) {
            $this->response(array('errors' => 'no such program'));
        } else {
            $this->response($model);
        }

    }

    public function action_get_exercises()
    {
        $id = $this->request->param('id');
        $model = $this->model($id);
        $data = array();
        $collection = $model->program_exercises->find_all();
        $fields = array('title', 'text');
        foreach ($collection as $raw) {
            $data[] = $raw->as_array_ext() + $raw->exercise->as_array_ext($fields);
        }
        $this->response($data);
    }

    protected function save_days($model, $days)
    {
        if (!$model->loaded()) {
            return;
        }
        $model->remove('exercises');
        foreach ($days as $day) {
            if (!isset($day['exercises']) || !is_array($day['exercises'])) {
                continue; //Пропускаем день без упражнений
            };
            foreach ($day['exercises'] as $index => $exercise) {
                $efforts = array();
                foreach ($exercise['efforts'] as $effort) {
                    $efforts[] = $effort;
                }
                $efforts = implode(',', $efforts);
                $m = ORM::factory('Program_Exercise');
                $m->program_id = $model->id;
                $m->exercise_id = $exercise['id'];
                $m->week_day = $day['value'];
                $m->sets = $efforts;
                $m->index_number = $index;
                $m->save();

            }
        }
    }

    public function action_training_time()
    {
        $model = ORM::factory('Training');
        $model->values($this->raw_values);
        $model->save();
        if ($model->saved()) {
            $training_time = Arr::get($this->raw_values, 'training_time');
            if ($training_time < 600) {
                $new_advice = ORM::factory('User_Advice')->where('user_id', '=', $this->user->id)->where('type', '=', 'fast_training')->find();
                if (!$new_advice->loaded()) {
                    $new_advice->user_id = $this->user->id;
                    $new_advice->type = 'fast_training';
                }
                $new_advice->status = 1;
                $new_advice->save();
            }
        }
    }

    public function action_post_check_day()
    {
        $current_user_program = $this->user->programs->where('activity', '=', 1)->find();
        $program_days = $current_user_program->all_days();
        $check_date = Arr::get($this->raw_values, 'date');
        if ($current_user_program->loaded()) {
            if ($check_date <= date('Y-m-d') && array_search($check_date, $program_days) !== false) {
                $model = ORM::factory('Training');
                $model->values($this->raw_values);
                $model->program_id = $current_user_program->id;
                try {
                    $model->save();
                    $this->response('true');
                } catch (ORM_Validation_Exception $e) {
                    $this->response(array('error' => $e->errors(0)));
                }
            } else {
                $this->response(array('error' => 'Date is invalid'));
            }
        } else {
            $this->response(array('error' => 'No active program'));
        }

    }

    public function action_get_my()
    {
        $collection = $this->model();
        $collection = $collection->where('user_id', '=', $this->user->id);
        $this->response($collection);
    }

    public function action_get_complete()
    {
        if (!$this->user) {
            $this->response(array('error' => 'invalid token'));
        } else {
            $model = $this->user->programs;
            $model = $model->where('activity', '=', 1)->find();
            if (!$model->loaded()) {
                $this->response(array('error' => 'no active program'));
            } else {
                $model->activity = 2;
                $model->save();
                $this->response->body('true');
            }
        }
    }

    public function action_post_score()
    {
        $raw_values = $this->raw_values;
        $model = $this->model($raw_values['program_id']);
        $score = Arr::get($raw_values, 'score');
        if (is_numeric($score) && $score >= 0 && $score <= 5) {
            $program_scores = $model->program_scores->where('user_id', '=', $this->user->id)->find();
            $program_scores->target_id = $raw_values['program_id'];
            $program_scores->user_id = $this->user->id;
            $program_scores->score = $raw_values['score'];
            try {
                $program_scores->save();
                if ($program_scores->loaded()) {
                    $scores = $model->program_scores->where('target_id', '=', $program_scores->target_id)->find_all()->as_array('id', 'score');
                    $score = array_sum($scores) / count($scores);
                    $model->score = $score;
                    $model->save();
                    $this->response($score);
                }
            } catch (ORM_Validation_Exception $e) {
                $this->response(array('error' => $e->errors(0)));
            }
        } else {
            $this->response(array('error' => 'Invalid value'));
        }

    }

    public function action_post_create_user_program()
    {
        if ($this->user && $this->user->loaded()) {
            $raw_values = $this->raw_values;
            $mission = Arr::get($raw_values, 'target');
//            if ($mission=='S')
            $mission = ORM::factory('Mission', array('sef' => $mission));
            $days = Arr::get($raw_values, 'days', array());
            if (Arr::get($this->raw_values, 'photo')) {
                $base64image = new Base64image(Arr::get($this->raw_values, 'photo'));
                $file = $base64image->createTmp();
                $image = Cms_Storage::instance()->add($file, date('ymdhis'), array(), 'jpeg');
                $raw_values['photo'] = $image->id;
            }
            $model = $this->model();
            $model->{'title_' . I18n::$lang} = Arr::get($raw_values, 'title');
            $model->{'text_' . I18n::$lang} = Arr::get($raw_values, 'description');
            $model->duration = Arr::get($raw_values, 'duration');
            $model->photo = Arr::get($raw_values, 'photo');
            $model->activity = 0;
            $model->created = date('Y-m-d');
            $model->type = 'user';
            $model->user_id = $this->user->id;
            $model->mission_id = $mission->id;
            $model->save();
            $this->save_days($model, $days);
            $this->response($model);
        } else {
            $this->response(array('error' => 'Invalid token'));
        }
    }

    public function action_post_user_programs()
    {
        $option = Arr::get($this->raw_values, 'option');
        $programType = Arr::get($option, 'programType');
        $model = $this->model();
        if ($programType == 'favourite' || $programType == 'FAVOURITE') {
            $favourites = $this->user->favourites->find_all()->as_array('id', 'program_id');
            if ($favourites) {
                $model = $model->where('id', 'in', $favourites);
            } else {
                $model = array();
            }
        } else {
            $model = $model->where('user_id', '=', $this->user->id)->where('type', '=', 'user');
        }
        $this->response($model);
    }

    public function action_post_add_to_favourite_program()
    {
        $id = Arr::get($this->raw_values, 'program_id');
        if ($id) {
            $model = ORM::factory('Favourite');
            $model->user_id = $this->user->id;
            $model->program_id = $id;
            $model->save();
            $this->response('true');
        } else {
            $this->response(array('error' => 'no id'));
        }
    }

    public function action_get_delete_user_program()
    {
        $id = $this->request->param('id');
        $model = $this->model($id);
        if ($model->loaded()) {
            if ($model->user_id == $this->user->id) {
                $model->delete();
                $this->response('true');
            } else {
                $this->response(array('error' => 'access denied'));
            }
        } else {
            $this->response(array('error' => 'program not found'));
        }
    }

    public function action_get_delete_favourite_program()
    {
        $id = $this->request->param('id');
        $model = ORM::factory('Favourite')->where('user_id', '=', $this->user->id)->where('program_id', '=', $id)->find();
        if ($model->loaded()) {
            $model->delete();
            $this->response('true');
        } else {
            $this->response(array('error' => 'program not found'));
        }
    }
}