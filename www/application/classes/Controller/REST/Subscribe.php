<?php

class Controller_REST_Subscribe extends API
{

    public function action_post_requests()
    {
        $collection = $this->model()->where('target_id', '=', $this->user->id)->where('status', '=', 0)->find_all()->as_array('id', 'user_id');
        $users = array();
        $fields = array('id', 'firstname', 'lastname');
        if ($collection) {
            $users = ORM::factory('User')->where('id', 'in', $collection);
        }
        $this->response($users, $fields);
    }

    public function action_post_confirm()
    {
        $id = Arr::get($this->raw_values, 'user_id');
        $response = Arr::get($this->raw_values, 'accept');
        $model = $this->model()->where('user_id', '=', $id)->where('target_id', '=', $this->user->id)->find();
        if ($model->loaded()) {
            if ($response) {
                $model->status = 1;
                $model->save();
            } else {
                $model->delete();
            }
            $this->response('true');
        } else {
            $this->response(array('error' => 'User not found'));
        }
    }

    public function action_get_add()
    {
        $id = $this->request->param('id');
        $model = $this->model();
        $model = $model->where('user_id', '=', $this->user->id)->where('target_id', '=', $id)->find();
        if (!$model->loaded()) {
            if ($id) {
                $model->user_id = $this->user->id;
                $model->target_id = $id;
                $model->save();
                $this->response->body('true');
            } else {
                $this->response->body(json_encode(array('error' => 'there is no id')));
            }
        } else {
            $this->response->body(json_encode(array('error' => 'subscription already exists')));
        }
    }

    public function action_post_delete()
    {
        $id = Arr::get($this->raw_values, 'id');
        $type = Arr::get($this->raw_values, 'type');
        $model = $this->model();
        if ($type == 'follower' || $type == 'FOLLOWER') {
            //удаляется подписчик
            $model = $model->where('target_id', '=', $this->user->id)->where('user_id', '=', $id)->find();
        } elseif ($type == 'subscribe' || $type == 'SUBSCRIBE') {
            //удаляется подписка
            $model = $model->where('user_id', '=', $this->user->id)->where('target_id', '=', $id)->find();
        }
        if ($model->loaded()) {
            $model->delete();
            $this->response->body('true');
        } else {
            $this->response->body(json_encode(array('error' => 'this subscription does not exist')));
        }
    }
}