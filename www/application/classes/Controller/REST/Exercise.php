<?php

class Controller_REST_Exercise extends API
{

    public function action_get_index()
    {
        $collection = $this->model();
        $collection = $collection->find_all();
        $data = array();
        $fields = array('id', 'title', 'text','video_url');
        foreach ($collection as $row) {
            $extra['categories'] = $row->categories->find_all()->as_array(null, 'id');
            $data[] = $row->as_array_ext($fields) + $extra;
        }
        $this->response($data);
    }

    public function action_head_index()
    {
        $this->check_cache(sha1($this->request->uri() . date('Y-m-d h:i')));
    }

    public function action_post_check()
    {
        $raw_values = $this->raw_values;
        $exercise_id = Arr::get($raw_values, 'exercise_id');
        $weight = Arr::get($raw_values, 'weight');
        $date = Arr::get($raw_values, 'date');
//        $index_number = Arr::get($raw_values, 'index_number');
        $check = Arr::get($raw_values, 'checked');
        $log = $this->user
            ->programs->where('activity', '=', 1)->find()
            ->logs->where('exercise_id', '=', $exercise_id)
            ->where('date', '=', $date)
//                    ->where('index_number','=',$index_number)
            ->find();
        if ($log->loaded()) {
            $log->status = $check;
        } else {
            $log->user_id = $this->user->id;
            $log->program_id = $this->user->programs->where('activity', '=', 1)->find()->id;
            $log->exercise_id = $exercise_id;
            $log->date = $date;
//            $log->index_number = $index_number;
            $log->status = $check;
            $log->weight = $weight;
        }
        try {
            $log->save();
        } catch (ORM_Validation_Exception $e) {
            $this->response(array('error' => $e->errors(0)));
        }
        $this->response('true');
    }
}