<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Document extends Site
{
    public function action_index(){
        //Регламенты
        $this->page->title(__('Документы'));
        $this->page->content(View::factory('confederation/documents')
            ->bind('pagination', $pagination)
            ->bind('model', $collection)
            ->bind('sport_types',$sport_types)
            ->bind('select_id',$select_id));
        $select_id=intval($this->request->post('sport_type'));
        $select_id=$select_id?$select_id:0;
        if ($select_id==0) {
            $model=ORM::factory('Antidoping');
        }
        else{
            $model=ORM::factory('Category')->where('id','=',$select_id)->find();
            if (!$model->loaded()){
                throw new HTTP_Exception_404;
            }
            $model = $model->antidoping;
        }
        $pagination=$this->pagination($model);
        $page = $this->request->param('page');
        $total_pages = $pagination->total_pages;
        $this->check_pages($page,$total_pages);
        View::bind_global('total_pages', $total_pages);
        $collection=$model->order_by('date', 'DESC')->find_all();
        $sport_types=array('0'=>__('Любой'))+ORM::factory('Category')->get_sef_sports()->select_options('id','title_'.I18n::$lang);
        $this->add_crumb(__('Документы'), '');
    }
}