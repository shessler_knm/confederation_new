<?php

class Controller_Redirect extends Site{

    public function action_view(){
        $id = $this->request->param('id');
        $controller = $this->request->param('model');
        $method = $this->request->param('method');
        if ($method!='player'){
            switch ($controller){
                case 'gallery': $model = ORM::factory('Photovideo',$id);
                    if (!$model->loaded()) throw new HTTP_Exception_404();
                    $this->redirect($model->view_url(true),301);
                    break;
                case 'interview':$model = ORM::factory('News',$id);
                    if (!$model->loaded()) throw new HTTP_Exception_404();
                    $this->redirect('confederation/'.$controller.'/'.$model->{'sef_'.I18n::$lang},301);
                    break;
                case 'mediaaboutus':$model = ORM::factory('News',$id);
                    if (!$model->loaded()) throw new HTTP_Exception_404();
                    $this->redirect('confederation/infocenter/aboutus/'.$model->{'sef_'.I18n::$lang},301);
                    break;
                case 'pressservice': $model = ORM::factory('News',$id);
                    if (!$model->loaded()) throw new HTTP_Exception_404();
                    $this->redirect('confederation/infocenter/'.$controller.'/'.$model->{'sef_'.I18n::$lang},301);
                    break;
                default: $model = ORM::factory(ucfirst($controller),$id);
                    if (!$model->loaded()) throw new HTTP_Exception_404();
                    $this->redirect($model->view_url(true),301);
                    break;
            }
        }else{
            $model = ORM::factory('Player',$id);
            if (!$model->loaded()) throw new HTTP_Exception_404();
            $this->redirect($model->view_url(true),301);
        }
    }
}