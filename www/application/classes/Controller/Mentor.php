<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Mentor extends Site{

    public function scripts(){
        return parent::scripts()+array(
            'comments' => 'js/comments.js',
        );
    }

    public function init_langs($model = NULL) {
        $langs = array('ru' => '/ru/','en' => '/en/', 'kz' => '/');
        if ($model === NULL) {
            parent::init_langs($model);
        } else {
                foreach ($langs as $lang => $val) {
                    if (empty($model->{'sef_'.$lang})) {
                        $this->array_langs[$lang] = $val.'confederation/mentor/' . $model->{'sef_'.I18n::$lang};
                    } else {
                        $this->array_langs[$lang] = $val.'confederation/mentor/'.$model->{'sef_'.$lang};
                    }
                }
        }
    }

    public function action_index()
    {
        $this->page->title(__('Наставник').' - '.__('Конфедерация спортивных единоборств и силовых видов спорта'));
        $this->add_crumb(__('Наставник'), '');
        $this->page->content(View::factory('mentor/list')
            ->set('counter',1)
            ->bind('pagination', $pagination)
            ->bind('federations', $federations)
            ->bind('model', $collection)
            ->bind('sport_types',$sport_types)
            ->bind('select_id',$select_id));
        $select_id=$this->request->query('sport_type');
        if ($select_id == 'all'){
            $this->redirect(URL::site(I18N::$lang.'/confederation/mentor'),301);
        }
        $select_id=$select_id?$select_id:'all';
        $model=ORM::factory('News');
        $category=ORM::factory('Category',array('slug'=>'mentors'));
        $model->join('news_categories','left')->on('news_categories.news_id','=','news.id');
        $model->where('news_categories.category_id','=',$category->id);
        $model->where('title_'.I18n::$lang,'!=','')->where('text_'.I18n::$lang,'!=','');
        $sport_types=array('all'=>__('Любой'))+ORM::factory('Category')->get_sef_sports()->select_options('id','title_'.I18n::$lang);
        if (!array_key_exists($select_id,$sport_types)&&$select_id){
            throw new HTTP_Exception_404();
        }
        if ($select_id!='all'){
            $model->join('news_sports','left')->on('news_sports.news_id','=','news.id');
            $model->where('news_sports.category_id','=',$select_id);
        }
        $pagination=$this->pagination($model);
        $total_pages = $pagination->total_pages;
        if ($this->check_pages($this->request->param('page'),$total_pages, $select_id!='all'?true:false)){
            $this->redirect(URL::site('confederation/mentor').'?sport_type='.$select_id,301);
        }

        View::bind_global('total_pages', $total_pages);
        $collection=$model->order_by('date','DESC')->find_all();
        $federations=array();
        foreach ($collection as $row){
            $federations[$row->id] = $row->federations->find_all()->as_array();
        }
    }

    public function action_view()
    {
        $sef=$this->request->param('sef');
        $model = ORM::factory('News')->where('sef_'.i18n::lang(),'=',$sef)->find();
        if (!$model->loaded()) {
            foreach ($this->array_langs as $lang => $url) {
                $model = ORM::factory('News')->where('sef_' . $lang, '=', $sef)->find();
                if ($model->loaded()) {
                    throw new HTTP_Exception_Translate();
                }
            }
            throw new HTTP_Exception_404('Страница не найдена');
        }
        if (!$model->{'text_' . I18n::$lang}) {
            throw new HTTP_Exception_404();
        }
        $this->init_langs($model);
        $this->view_counter($model);
        $this->page->title($model->title.' - '.__('Наставник').' - '.__('Конфедерация спортивных единоборств и силовых видов спорта'));
        $this->page->meta(Text::limit_chars($model->text,200),'description');
        $this->page->content(View::factory('mentor/view')
            ->bind('model',$model));
    }

    public function action_translit_message() {
        $this->render=false;
        $langs = array('ru' => '/ru/','en' => '/en/', 'kz' => '/');
        $this->write_lst_uri = false;
        $this->response->status(404);
        $this->session OR $this->session=Session::instance();
        $last_uri = '/'.ltrim($this->session->get('last_uri',URL::base()),'/');
        $model = ORM::factory('News');
        preg_match('"([^/]*$)"',$last_uri,$match);
        foreach ($langs as $lang => $val) {
            if ($model->where('sef_'.$lang,'=',$match[0])->find()->loaded()) {
                break;
            }
        }
        $url = 'confederation/mentor';
        Helper::currentURI();
        $view = View::factory('errors/translite')
            ->bind('url',$url)
            ->bind('model',$model)
            ->bind('model_name',$model_name)
            ->bind('last_uri',$last_uri);
        $this->response->body($view);
    }
}