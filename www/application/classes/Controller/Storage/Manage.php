<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Storage_Manage extends Site
{
    public function action_download_file(){
        $this->render = false;
        $file_id = $this->request->param('id');
        $file = ORM::factory('Storage', $file_id);
        if (!file_exists($file->full_path())){
            throw new HTTP_Exception_404;
        }
        $file_size = filesize($file->full_path());

        $resp = fread(fopen($file->full_path(), "rb"), $file_size);

        $this->response
            ->headers('Content-Disposition', 'attachment; filename="'.$file->original_name.'"')
            ->headers('Content-transfer-encoding', 'binary')
            ->headers('Content-Length', $file_size)
            ->headers('Content-type', 'application/octet-stream');

        $this->response->body($resp);

//        $file = Storage::getInstance()->get($this->request->param('id'))->file(ORM::factory('Storage'));
//        $this->response->send_file($file,'download_name');
    }
}
