<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Gallery extends Site
{
    public function before()
    {
        parent::before();
        Register::instance()->activemenu = 26;
    }

    public function scripts()
    {
        return parent::scripts() + array(
            'comments' => 'js/comments.js',
            'gallery' => 'js/gallery.js'
        );
    }

    public function init_langs($model = NULL)
    {
        $langs = array('ru' => '/ru/', 'en' => '/en/', 'kz' => '/');
        if ($model === NULL) {
            parent::init_langs($model);
        } else {
            foreach ($langs as $lang => $val) {
                if (empty($model->{'sef_' . $lang})||($model->type==2&&!$model->{'content_'.$lang})) {
                    $this->array_langs[$lang] = $val . $this->fed_id . '/gallery/'.$model->{'sef_' . I18n::$lang};
                } else {
                    $this->array_langs[$lang] = $val . $this->fed_id . '/gallery/' . $model->{'sef_' . $lang};
                }
            }
        }
    }

    public function action_index()
    {
        $federation = $this->federation;
        $photovideo = $federation->photovideos
            ->where('title_' . I18n::$lang, '!=', '')
            ->where('content_' . I18n::$lang, '!=', '')
            ->order_by('date','DESC');
        $pagination = $this->pagination($photovideo);
        $page = $this->request->param('page');
        $total_pages = $pagination->total_pages;
        $this->check_pages($page,$total_pages);
        View::bind_global('total_pages', $total_pages);
        $model = $photovideo->find_all();
        $photos = array();
        foreach ($model as $album) {
            if ($album->type == 1) {
                $jacket = ORM::factory('Photo')->where('album_id', '=', $album->id)->where('jacket', '=', 1)->find();
                if ($jacket->loaded()) {
                    $photos[$album->id] = $jacket;
                } else {
                    $photos[$album->id] = ORM::factory('Photo')->where('album_id', '=', $album->id)->find();
                }
            }
        }
        $this->page->title(__('Фото и Видео'));
        $this->page->content(View::factory('album/index')
                ->set('counter', 0)
                ->bind('model', $model)
                ->bind('pagination', $pagination)
                ->bind('photos', $photos)
        );
    }

    public function action_view()
    {
        $this->page->script('jquery.fancybox', "/js/fancybox/jquery.fancybox.js");
        $this->page->script('jquery.fancybox.pack', "/js/fancybox/jquery.fancybox.pack.js");
        $this->page->script('jquery.fancybox-buttons', "/js/fancybox/jquery.fancybox-buttons.js");
        $this->page->script('jquery.fancybox-thumbs', "/js/fancybox/jquery.fancybox-thumbs.js");
        $this->page->script('jquery.fancybox-media', "/js/fancybox/jquery.fancybox-media.js");
        $sef = $this->request->param('sef');
        $model = ORM::factory('Photovideo')->where('sef_' . i18n::lang(), '=', $sef)->find();
        if (!$model->loaded()) {
            foreach ($this->array_langs as $lang => $url) {
                $model = ORM::factory('Photovideo')->where('sef_' . $lang, '=', $sef)->find();
                if ($model->loaded()) {
                    throw new HTTP_Exception_Translate();
                }
            }
            throw new HTTP_Exception_404('Страница не найдена');
        }
        if ($model->type == 2) {
            if (!$model->{'content_' . I18n::$lang}) {
                throw new HTTP_Exception_404();
            }
            $this->page->content(View::factory('album/video')
                ->bind('model', $model));
        } else {
            $photoSelected = ORM::factory('Photo')->where('album_id', '=', $model->id)->find();
            $photos = ORM::factory('Photo')->where('album_id', '=', $model->id)->find_all();
            $this->page->content(View::factory('album/photo')
                    ->bind('photos', $photos)
                    ->bind('photoSelected', $photoSelected)
                    ->bind('model', $model)
            );
        }
        $this->view_counter($model);
        $this->init_langs($model);
        $this->page->title($model->title);
        $this->page->meta(Text::limit_chars($model->description,200), 'description');
    }

    public function action_translit_message()
    {
        $this->render = false;
        $langs = array('ru' => '/ru/', 'en' => '/en/', 'kz' => '/');
        $this->write_lst_uri = false;
        $this->response->status(404);
        $this->session OR $this->session = Session::instance();
        $last_uri = '/' . ltrim($this->session->get('last_uri', URL::base()), '/');
        $model = ORM::factory('Photovideo');
        preg_match('"([^/]*$)"', $last_uri, $match);
        foreach ($langs as $lang => $val) {
            if ($model->where('sef_' . $lang, '=', $match[0])->find()->loaded()) {
                break;
            }
        }
        $url = $this->fed_id.'/gallery';
        Helper::currentURI();
        $view = View::factory('errors/translite')
            ->bind('url', $url)
            ->bind('model', $model)
            ->bind('model_name', $model_name)
            ->bind('last_uri', $last_uri);
        $this->response->body($view);
    }

}
