<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Result extends Site
{
    public function action_index()
    {
        switch ($this->fed_id) {
            case 'box':
                $this->page->title(__('Результаты турниров и соревнований по боксу - Казахстанская федерация бокса'));
                $this->page->meta(__('Свежие результаты с крупных международных турниров и чемпионатов Казахстана по боксу'), 'description');
                break;
            case 'judo':
                $this->page->title(__('Результаты турниров и соревнований по дзюдо - Федерация дзюдо Казахстана'));
                $this->page->meta(__('Свежие результаты с крупных международных турниров и чемпионатов Казахстана по дзюдо'), 'description');
                break;
            case 'wrestling':
                $this->page->title(__('Результаты турниров и соревнований по вольной, греко-римской и женской борьбе - Федерация греко-римской, вольной и женской борьбы Республики Казахстан'));
                $this->page->meta(__('Свежие результаты с международных турниров и чемпионатов Казахстана по вольной, греко-римской и женской борьбе'), 'description');
                break;
            case 'taekwondo':
                $this->page->title(__('Результаты турниров и соревнований по таеквондо - Федерация таеквондо (WTF) Республики Казахстан'));
                $this->page->meta(__('Свежие результаты с международных турниров и чемпионатов Казахстана по таеквондо'), 'description');
                break;
            case 'weightlifting':
                $this->page->title(__('Результаты турниров и соревнований по тяжелой атлетике - Федерация тяжелой атлетики Казахстана'));
                $this->page->meta(__('Свежие результаты с крупных международных турниров и чемпионатов Казахстана по тяжелой атлетике'), 'description');
                break;
        }
        $model = $this->federation->results->order_by('date', 'ASC')->where('is_published','!=','2');
        if ($this->request->query('query')) {
            $this->render = false;
            $model = $model->where('photo'.i18n::$lang,'>',0)->limit(3)->find_all();
            $check = $model->count();
            $this->response->body(View::factory('results/preview')
                ->bind('check', $check)
                ->bind('model', $model)
            );
        } else {
            //просто список результатов
            $pagination = $this->pagination($model);
            $page = $this->request->param('page');
            $total_pages = $pagination->total_pages;
            $this->check_pages($page,$total_pages);
            View::bind_global('total_pages', $total_pages);
            $model = $model->find_all();
            $check = $model->count();
            $this->page->content(View::factory('results/list')
                ->bind('pagination', $pagination)
                ->bind('check', $check)
                ->bind('model', $model));
        }
    }
}