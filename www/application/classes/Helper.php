<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Igor Noskov <igor.noskov87@gmail.com>
 * Date: 18.01.12
 * Time: 18:39
 * To change this template use File | Settings | File Templates.
 */

class Helper
{
    /**
     * Определяет в какой среде выполняется кохана
     * @static
     *
     */
    public static function detect_environment()
    {
        if (getenv('KOHANA_ENV')) {
            if (strtoupper(getenv('KOHANA_ENV')) == 'DEVELOPMENT') {
                Kohana::$environment = constant('Kohana::DEVELOPMENT');
            } else {
                Kohana::$environment = constant('Kohana::PRODUCTION');
            }
            return;
        }
        if (preg_match('/\.dev$/i', Arr::get($_SERVER, 'HTTP_HOST', ''))) {
            Kohana::$environment = constant('Kohana::DEVELOPMENT');
        } else {
            Kohana::$environment = constant('Kohana::PRODUCTION');
        }
    }

    /**
     * Если среда выполенения Kohana::PRODUCTION, то вернет первый аргумент, иначе второй
     * @static
     * @param $production
     * @param $developer
     * @return mixed
     */
    public static function set_if_production($production, $developer)
    {
        return self::is_production() ? $production : $developer;
    }

    /**
     * Вернет TRUE, если среда выполнения Kohana::PRODUCTION
     * @static
     * @return bool
     */
    public static function is_production()
    {
        return Kohana::$environment == Kohana::PRODUCTION;
    }

    /**
     * Возвращает айпи адресс клиента
     * @static
     * @return string
     */
    public static function get_ip_address()
    {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                        return $ip;
                    }
                }
            }
        }
    }

    public static function is_link($link)
    {
        if (is_link($link)) return TRUE;
        return !preg_match('/(modules|application|system)/', $link) AND preg_match('/(modules|application|system)/', @readlink($link));
    }

    public static function translit($str)
    {
        $replace = array(
            'а' => 'a',
            'ә' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'g',
            'ғ' => 'g',
            'д' => 'd',
            'е' => 'e',
            'ё' => 'e',
            'ж' => 'zh',
            'з' => 'z',
            'й' => 'i',
            'и' => 'i',
            'к' => 'k',
            'қ' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'ң' => 'n',
            'о' => 'o',
            'ө' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ү' => 'u',
            'ұ' => 'u',
            'ф' => 'f',
            'х' => 'h',
            'һ' => 'h',
            'ц' => 'ts',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'shch',
            'ъ' => '',
            'ы' => 'i',
            'і' => 'i',
            'ь' => "'",
            'э' => 'e',
            'ю' => 'u',
            'я' => 'ya',
            ' ' => '-',
        );

//        $str = Utf8::substr($str, 0, 50);
        $str = UTF8::strtolower($str);
        $str = strtr($str, $replace);

        $str = preg_replace('/[^a-z0-9-_]/', '', $str);
        return $str;
    }

    public static function xml_entities($str)
    {
        $str = html_entity_decode($str, ENT_QUOTES, 'UTF-8');
        $str = str_replace(
            array('"', '\'', '&', '<', '>'),
            array('&quot;', '&apos;', '&amp;', '&lt;', '&gt;'),
            $str
        );
        return $str;
    }


    // Функция удаляет из переданного $uri префикс языка
    public static function currentURI($with_lang = false)
    {
        $_uri = preg_replace('%[^/-A-Za-zА-Яа-я0-9_/]%', '', Request::initial()->uri());
        $find = preg_match("/ru\/|en\/|kz\//", $_uri, $lang);
        $uri = false;
        if (!$find) {
            if (strlen($_uri) == 2) {
                $find = preg_match("/ru|en|kz/", $_uri, $lang);
            }
        }
        if ($find) {
            $lang = $lang[0];
            $ch4 = mb_strcut($_uri, 0, 4);
            $p = strpos($ch4, $lang);
            if ($p !== false || $lang != I18n::$lang) {
                switch ($p) {
                    case 0:
                        $uri = substr($_uri, 3);
                        break;
                    case 1:
                        $uri = substr($_uri, 4);
                        break;
                    default:
                        $uri = $_uri;
                }
            }
            $uri = preg_replace("'/page-.*'", '', $uri);
            if ($uri === false) $uri = '';
            return '/' . ltrim($uri, '/');
        } else {
            return URL::site('/' . $_uri);
        }
    }

    //Функция возвращает ссылку на оригинальный контент
    public static function canonical($fed_id)
    {
        $current_uri = Request::initial()->uri();
        preg_match('"([^/]*$)"', $current_uri, $sef);
        $sef = $sef[0];
        $array = null;
        //приоритет ссылок на федерации. это нужно, если контент имеет связь со многими федерациями
        $priority = array(
            'box',
            'judo',
            'wrestling',
            'weightlifting',
            'taekwondo'
        );
        //если мы на федерации
        if ($fed_id && $fed_id != 'box') {
            $controller = Request::$current->controller();
            switch ($controller) {
                case 'News':
                    $model_name = 'News';
                    $canonical = true;
                    break;
                case 'Gallery':
                    $model_name = 'Photovideo';
                    $canonical = true;
                    break;
                case 'Event':
                    $model_name = 'Event';
                    $canonical = true;
                    break;
                default:
                    $canonical = false;
            }
            if ($canonical) {
                $model = ORM::factory($model_name)->where('sef_' . I18n::$lang, '=', $sef)->find();
                $federations = $model->federations->find_all()->as_array(null, 'sef');
                $fed_count = count($federations);
                if ($fed_count > 1) {
                    foreach ($priority as $row) {
                        $key = array_keys($federations, $fed_id);
                        if ($federations[array_search($row, $federations)] != $fed_id && array_search($row, $federations) < $key[0]) {
                            $array[] = $federations[array_search($row, $federations)];
                        }
                    }
                    if (!empty($array)) {
                        $federation = min($array);
                        $url = $federation . '/' . strtolower($model_name) . '/' . $sef;
                        return '<link rel="canonical" href="' . URL::site($url) . '"/>';
                    }
                }
            }
        } else {
            //если на конфедерации
            preg_match('#([^/]+/)(?=[^/]*$)#', $current_uri, $match);
            $link = array_key_exists(0, $match) ? $match[0] : null;
            switch ($link) {
                case 'news/':
                    $model_name = 'News';
                    $canonical = true;
                    $has_many = true;
                    break;
                case 'gallery/':
                    $model_name = 'Photovideo';
                    $canonical = true;
                    $has_many = true;
                    break;
                case 'event/':
                    $model_name = 'Event';
                    $canonical = true;
                    $has_many = true;
                    break;
                case 'section/':
                    $model_name = 'Section';
                    $canonical = true;
                    $has_many = false;
                    break;
                default:
                    $canonical = false;
            }
            if ($canonical) {
                $model = ORM::factory($model_name)->where('sef_' . I18n::$lang, '=', $sef)->find();
                if ($has_many) {
                    $federations = $model->federations->find_all()->as_array(null, 'sef');
                    foreach ($priority as $row) {
                        if (array_search($row, $federations) !== null && !empty($federations)) {
                            $array[] = $federations[array_search($row, $federations)];
                        }
                    }
                    if (!empty($array)) {
                        $federation = min($array);
                        $url = $federation . '/' . $link . $sef;
                        return '<link rel="canonical" href="' . URL::site($url) . '"/>';
                    }
                } else {
                    $federation = $model->federation->sef;
                    $url = $federation . '/' . $link . $sef;
                    return '<link rel="canonical" href="' . URL::site($url) . '"/>';
                }
            }
        }
    }

    /*
     * $counter - число(число комментариев, просмотров и т.д.)
     * $multiple_genitive - родительный падеж, множественное число("просмотров", "комментариев", "подписчиков" и т.д)
     * $nominative - именительный падеж, единственное число("корова", "дом", "просмотр", "птица" и т.д.)
     * $single_genitive - родительный падеж, единственное число("розетки","окна", "подписчика" и т.д.)
     * */
    public static function word_cases($counter,$multiple_genitive,$nominative,$single_genitive)
    {
        if (substr($counter, -1) >= 5 && substr($counter, -1) <= 9 || substr($counter, -1) == 0) {
            return ($counter) . ' ' . __($multiple_genitive);
        } elseif (($counter > 10 && ($counter <= 14))) {
            return ($counter) . ' ' . __($multiple_genitive);
        } elseif (substr($counter, -1) == 1) {
            return ($counter) . ' ' . __($nominative);
        } elseif (substr($counter, -1) >= 2 && substr($counter, -1) <= 4) {
            return ($counter) . ' ' . __($single_genitive);
        }
    }
}
