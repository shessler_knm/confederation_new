<?php

class Admin extends Cms_Admin
{
    protected $need_auth = true;

    public function before()
    {
//        $this->need_auth=Helper::set_if_production(true,false);
        parent::before();
        i18n::lang('ru');
        if (isset($this->menu[Arr::get($this->option, 'model')])) {
            $this->menu[$this->option['model']]['active'] = true;
            $this->page->title(strip_tags($this->menu[$this->option['model']]['title']));
            $this->crumbs[] = array('title' => $this->page->title(), 'uri' => $this->menu[$this->option['model']]['url']);
        } else {
            $this->page->title("Панель управления");
//            $this->menu['main']['active'] = true;
        }

        $synch_errors = Session::instance()->get('synch_errors') ? Session::instance()->get_once('synch_errors') : array();
        foreach ($synch_errors as $error) {
            $this->page->message("Ошибка синхронизации: " . $error, Cms_Page::PAGE_MESSAGE_ERROR);
        }
    }

    public function init_menu()
    {
        return parent::init_menu() + array(
            'Album' => array(
                'active' => false,
                'title' => 'Альбомы',
                'url' => ORM::factory('Album')->list_url(),
            ),
            'Banner' => array(
                'active' => false,
                'title' => 'Баннер и партнеры',
                'url' => ORM::factory('Banner')->list_url(),
            ),
            'Video' => array(
                'active' => false,
                'title' => 'Видео',
                'url' => ORM::factory('Video')->list_url(),
            ),
            'City' => array(
                'active' => false,
                'title' => 'Города',
                'url' => ORM::factory('City')->list_url(),
            ),
            'Antidoping' => array(
                'active' => false,
                'title' => 'Документы',
                'url' => ORM::factory('Antidoping')->list_url(),
            ),

            'Fact' => array(
                'active' => false,
                'title' => 'Интересные факты',
                'url' => ORM::factory('Fact')->list_url(),
            ),
            'Infographic' => array(
                'active' => false,
                'title' => 'Инфографика',
                'url' => ORM::factory('Infographic')->list_url(),
            ),
            'Category' => array(
                'active' => false,
                'title' => 'Категории',
                'url' => ORM::factory('Category')->list_url(),
            ),
            'Team' => array(
                'active' => false,
                'title' => 'Команды',
                'url' => ORM::factory('Team')->list_url(),
            ),
            'Comment' => array(
                'active' => false,
                'title' => 'Комментарии',
                'url' => ORM::factory('Comment')->list_url(),
            ),
//            'Shop'=>array(
//                'active'=>false,
//                'title'=>'Магазины "Измени себя"',
//                'url'=>ORM::factory('Shop')->list_url(),
//            ),
            'Ingredient' => array(
                'active' => false,
                'title' => '<span style="color:purple">Ингредиенты</span>',
                'url' => ORM::factory('Ingredient')->list_url(),
            ),
            'Program' => array(
                'active' => false,
                'title' => '<span style="color:purple">Программы тренировок</span>',
                'url' => ORM::factory('Program')->list_url(),
            ),
            'Dish' => array(
                'active' => false,
                'title' => '<span style="color:purple">Рацион питания</span>',
                'url' => ORM::factory('Dish')->list_url(),
            ),
            'Article' => array(
                'active' => false,
                'title' => '<span style="color:purple">Статьи</span>',
                'url' => ORM::factory('Article')->list_url(),
            ),
            'Gym' => array(
                'active' => false,
                'title' => '<span style="color:purple">Тренажерные залы</span>',
                'url' => ORM::factory('Gym')->list_url(),
            ),
            'Exercise' => array(
                'active' => false,
                'title' => '<span style="color:purple">Упражнения</span>',
                'url' => ORM::factory('Exercise')->list_url(),
            ),
            'Mission' => array(
                'active' => false,
                'title' => '<span style="color:purple">Цели тренировок</span>',
                'url' => ORM::factory('Mission')->list_url(),
            ),
            'Menu' => array(
                'active' => false,
                'title' => 'Меню',
                'url' => ORM::factory('Menu')->list_url(),
            ),
            'News' => array(
                'active' => false,
                'title' => 'Новости',
                'url' => ORM::factory('News')->list_url(),
            ),
            'User' => array(
                'active' => false,
                'title' => 'Пользователи',
                'url' => ORM::factory('User')->list_url(),
            ),
            'Result' => array(
                'active' => false,
                'title' => 'Результаты',
                'url' => ORM::factory('Result')->list_url(),
            ),
            'Governance' => array(
                'active' => false,
                'title' => 'Руководство конфедерации',
                'url' => ORM::factory('Governance')->list_url(),
            ),
            'Fedgovernance' => array(
                'active' => false,
                'title' => 'Руководство федерации',
                'url' => ORM::factory('Fedgovernance')->list_url(),
            ),
            'Section' => array(
                'active' => false,
                'title' => 'Секции',
                'url' => ORM::factory('Section')->list_url(),
            ),
            'Event' => array(
                'active' => false,
                'title' => 'События',
                'url' => ORM::factory('Event')->list_url(),
            ),
            'Advice' => array(
                'active' => false,
                'title' => 'Советы',
                'url' => ORM::factory('Advice')->list_url(),
            ),
            'Sponsor' => array(
                'active' => false,
                'title' => 'Спонсоры и участвующие организации',
                'url' => ORM::factory('Sponsor')->list_url(),
            ),
            'Page' => array(
                'active' => false,
                'title' => 'Статичные страницы',
                'url' => ORM::factory('Page')->list_url(),
            ),
            'Translation' => array(
                'active' => false,
                'title' => 'Трансляции',
                'url' => ORM::factory('Translation')->list_url(),
            ),
            'Coach' => array(
                'active' => false,
                'title' => 'Тренеры',
                'url' => ORM::factory('Coach')->list_url(),
            ),
            'Federation' => array(
                'active' => false,
                'title' => 'Федерации',
                'url' => ORM::factory('Federation')->list_url(),
            ),
            'Quote' => array(
                'active' => false,
                'title' => 'Цитаты',
                'url' => ORM::factory('Quote')->list_url(),
            ),
            'Partner' => array(
                'active' => false,
                'title' => 'Партнеры',
                'url' => ORM::factory('Partner')->list_url(),
            ),
            'Pvideo' => array(
                'active' => false,
                'title' => 'Видео от споснора',
                'url' => ORM::factory('Pvideo')->list_url(),
            ),
        );
    }

    public function roles()
    {
        return array(
            'controller_roles' => array(
                'admin'
            ),
            'actions_roles' => array(),
        );
    }

    public function scripts()
    {
        $array = parent::scripts();
        unset($array['admin']);
        $array += array(
            'yandexmap' => 'http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU',
            'map' => 'js/map.js',
            'jcrop' => 'js/lib/jcrop/js/jquery.Jcrop.min.js',
            'admin' => 'js/admin.js',
        );
        return $array;
    }

    public function styles()
    {
        return parent::styles() + array(
            'jcrop' => 'js/lib/jcrop/css/jquery.Jcrop.min.css'
        );
    }

    public function extrl_validation($model)
    {
        $data = $this->request->post('model');
        return Validation::factory($data)
            ->rule('sports', 'Orm::must_selected')
            ->rule('federations', 'Orm::must_selected');
    }

    public function _search($model, $fields)
    {
        foreach ($fields as $name => $f) {
            $query = $this->request->query($name);
            if (isset($f['params']) && Arr::get($f['params'], 'widget') == 'multilang') {
                $name = $name . "_ru";
            }
            if (!$query) continue;
            if (Arr::get($f, 'search')) {
                switch ($f['type']) {
                    case 'text':
                    case 'strings':
                        $model->where($name, 'LIKE', "%{$query}%");
                        break;
                    case 'select':
                        if ($query == 'NULL') {
                            $model->where($name, 'IS', DB::expr('NULL'));
                        } else {
                            $model->where($name, '=', $query);
                        }
                        break;
                    case 'date':
                        $date = strtotime($query);
                        $model->where($name, '>=', date('Y-m-d 00:00:00', $date));
                        $model->where($name, '<=', date('Y-m-d 23:59:59', $date));
                        break;
                    default:
                        $model->where($name, '=', $query);
                }
            }
        }
    }

    public function action_list()
    {
        $this->page->content(
            View::factory("admin/list")
                ->bind('model', $model)
                ->bind('collection', $collection)
                ->bind('request', $this->request)
                ->bind('fields', $fields)
                ->bind('pagination', $pagination)
        );
        $this->_mass_delete();
        $collection = $this->factory_model();
        $this->menu($collection);
        $fields = $collection->fields_description();
        Event::instance()->dispatch("admin.list.init", $collection);
        $collection = $this->_list_init($collection);
        $this->_search($collection, $fields);
//        $ruri = $_SERVER['REQUEST_URI'];
//        if (strpos(URL::base(), $ruri) === 0) {
//            $ruri = substr($ruri, strlen(URL::base()));
//        }
        $pagination = Pagination::factory(array(
            'total_items' => $collection->reset(false)->count_all(),
            'current_page' => array('source' => 'query_string', 'key' => 'page'),
        )); //Критично для embeded полей
        $pagination->apply($collection);
        $model = $collection->order_by('id', 'DESC')->find_all();
    }

    public function after_values($model)
    {

        //Проверка для синхр.
        if ($model->loaded()) //только редактирование
        {
            if ($model instanceof Synchronize_ISynch) {
                $model_name = $model->object_name();
                //Delete all rows
                DB::delete('synchedits')
                    ->where('model_name', '=', $model_name)
                    ->where('uid', '=', $model->id)
                    ->execute();

                $has_many = $model->has_many();
                $belongs_to = $model->belongs_to();
                if (array_key_exists('federations', $has_many)) {
                    foreach ($model->federations->find_all() as $fed) {
                        $edit = ORM::factory('Synchedit');
                        $edit->model_name = $model->object_name();
                        $edit->uid = $model->id;
                        $edit->fed_id = $fed->id;
                        $edit->save();
                    }

                } elseif (array_key_exists('federation', $belongs_to)) {
                    $edit = ORM::factory('Synchedit');
                    $edit->model_name = $model->object_name();
                    $edit->uid = $model->id;
                    $edit->fed_id = $model->federation->id;
                    $edit->save();
                }
            }
        }
        return $model;
    }

    public function action_edit()
    {
        $this->page->content(
            View::factory("admin/edit")
                ->bind('model', $model)
                ->bind('fields', $fields)

        );

        $model = $this->factory_model();
        Event::instance()->dispatch("admin.edit.init", $model);
        $this->_edit_init($model);

        if ($this->request->param('id') != null) {
            $model->where("id", "=", (int)$this->request->param('id'))->find();
        }
        Event::instance()->dispatch("admin.edit.init.after", $model);
        /** @var Cms_ORM $model */
        $model = $this->_after_edit_init($model);
        $fields = $model->fields_description();
        if ($model instanceof Cms_Interface_Tree) {
            $id2 = $this->request->param('id2');
            if ($id2) {
                $parent = $this->factory_model()->where('id', '=', $id2)->find();
                if (!$parent->loaded()) {
                    $this->response->status(404);
                    throw new HTTP_Exception_404;
                }
                $model->{$model->tree_parent_field()} = $parent->id;
                $this->crumbs[] = array('title' => $parent->{$parent->tree_title_field()}, 'uri' => $parent->tree_url(true));
            }
        }
        if ($model->loaded()) {
            $this->crumbs[] = array('title' => 'Редактировать', 'uri' => '#');
        } else {
            $this->crumbs[] = array('title' => 'Создать', 'uri' => '#');
            if (Authority::cannot('create', $this->resource())) {
                $this->redirect('admin/panel/403');
            }
        }
        if ($this->request->post('save')) {
            Event::instance()->dispatch("admin.edit.save.before", $model);
            $this->_before_save($model);
            $this->values = $this->request->post('model');
            $model = $this->after_values($model);
            if ($this->_save($model, $this->values, $this->extrl_validation($model))) {
                Event::instance()->dispatch("admin.edit.save.after", $model);
                $this->_after_save($model);
                $this->page->message("Изменения сохранены");
                if ($model instanceof IC_Editable) {
                    $this->redirect($model->edit_url(true));
                } else {
                    $this->redirect($this->last_uri);
                }
            } else {
                Component::errors($this->errors);
                $this->page->message("Ошибка при сохранении", Cms_Page::PAGE_MESSAGE_ERROR);
            }
        }

        if ($this->request->post('cancel')) {
            Event::instance()->dispatch("admin.edit.cancel.before", $model);
            $this->_before_cancel($model);
            if ($model instanceof Cms_Interface_Tree) {
                $this->redirect($model->tree_url(true));
            } else {
                $this->redirect($model->list_url(true));
            }
        }
    }

    public function action_external_list()
    {
        $this->render = false;
        $this->page->content(
            View::factory("admin/external_list")
                ->bind('model', $model)
                ->bind('collection', $collection)
                ->bind('request', $this->request)
                ->bind('fields', $fields)
                ->bind('pagination', $pagination)
        );
        $collection = $this->factory_model();
        $fields = $collection->fields_description();
        Event::instance()->dispatch("admin.external_list.init", $collection);
        $collection = $this->_list_init($collection);
        $this->_search($collection, $fields);
        $pagination = Pagination::factory(array(
            'total_items' => $collection->has_group_by() ? $collection->reset(false)->group_count() : $collection->reset(false)->count_all(),
            'view' => 'pagination/ext_list_floating',
            'current_page' => array('source' => 'query_string', 'key' => 'page'),
        )); //Критично для embeded полей
        $pagination->apply($collection);
        $model = $collection->find_all();
        $this->response->body($this->page->content());
    }

    public function action_synchronize_model()
    {

        $tasks = array('task' => 'synchronize',
            'env' => Helper::set_if_production(Kohana::PRODUCTION, Kohana::DEVELOPMENT));


        $federations = ORM::factory('Federation')->find_all();
        foreach ($federations as $fed) {
            $model = $this->factory_model();
            if ($fed->url) {
                Task_Synchronize::factory($tasks + array(
                    'model' => $model->object_name(),
                    'parent_hasmany' => false,
                    'parent_id' => $fed->id,
                    'fed_id' => $fed->id
                ))->execute();
            }
        }

        Minion_Task::factory($tasks)->execute();

        $this->redirect($this->last_uri);
    }
}
