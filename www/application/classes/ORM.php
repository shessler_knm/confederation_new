<?php

class ORM extends Cms_ORM
{
    public $pagination;

    public static function clear_html($dirty_html)
    {
        require_once Kohana::find_file('vendors/htmlpurifier/library/', 'HTMLPurifier.auto');

        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.SafeIframe', true);
        $config->set('URI.SafeIframeRegexp', '%^http://(www.youtube(?:-nocookie)?.com/embed/|player.vimeo.com/video/)%'); //allow YouTube and Vimeo
        $config->set('Attr.AllowedFrameTargets', array("_blank"));
        $config->set('Attr.DefaultImageAlt', '');
        $purifier = new HTMLPurifier($config);
        $clean_html = $purifier->purify($dirty_html);
        return $clean_html;
    }

    public function user_save(Validation $validation = NULL)
    {
        return $this->loaded() ? $this->update($validation) : $this->create($validation);
    }

    /**
     * Фильтр который берет значение из существущего поля и транслитирирует его в текущее
     * @param $value
     * @param $model
     * @param $field
     * @return mixed
     */
    public static function filter_sef($value, $model, $field)
    {
        if ($value == '') {
            $value = Helper::translit($model->$field);
        }
        return $value;
    }

    public static function full_name_filter_sef($value, $text)
    {
        if ($value == '') {
            $value = Helper::translit($text);
        }
        return $value;
    }

    public static function filter_team_sef($value, $text, $id)
    {
        if ($value == '') {
            $value = Helper::translit($id . '_' . $text);
        }
        return $value;
    }

    /**
     * @static Для валидации выбраных категорий
     * @param $data массив данных компонента select
     * @return bool
     */
    public static function must_selected($data)
    {
        if (count($data) > 0 && is_array($data)) {
            if (count($data) == 1 && $data[0] == 0) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public function title()
    {
        $arg = func_get_args();
        if (!count($arg)) {
            return $this->title;
        } else {
            return $this->{'title_' . $arg[0]};
        }
    }

    public function short_text()
    {
        $arg = func_get_args();
        if (!count($arg)) {
            return Text::limit_chars(strip_tags($this->text));
        } else {
            return Text::limit_chars(strip_tags($this->{'text_' . $arg[0]}));
        }
    }

    protected function extended_index()
    {
        return;
    }

    public function search_target()
    {
        return $this->_object_name;
    }

    public function search_target_id()
    {
        return $this->pk();
    }

    public function search_extended_index()
    {
        $fields = $this->fields_description();
        $index = array();
        foreach ($fields as $f => $d) {
            if (Arr::get($d, 'extended_index')) {
                $index[] = $this->get_field($f);
            }
        }
        return implode("\n", $index);
    }


    public function search_fulltext_index()
    {
        $fields = $this->fields_description();
        $index = '';
        foreach ($fields as $f => $d) {
            if (Arr::get($d, 'fulltext', false)) {
                if (Arr::get($d['params'], 'widget') == 'multilang') {
                    $index .= " " . strip_tags($this->{$f . '_ru'}) . " " . strip_tags($this->{$f . '_kz'}) . " " . strip_tags($this->{$f . '_en'});
                } else {
                    $index .= " " . strip_tags($this->get_field($f));
                }
            }
        }
        return $index;
    }

    public function object_fields()
    {
        return $this->_object;
    }

    public function to_array()
    {
        $ret = array();
        foreach ($this->object_fields() as $field => $value) {
            $ret[$field] = $value;
        }
        return $ret;
    }

    public function has_row($row)
    {
        $fields = $this->object_fields();
        if (array_key_exists($row, $fields) === false) {
            return false;
        } else {
            return true;
        }
    }

    public function actions($user = null)
    {
        $edit = array(
            'title' => 'Редактировать',
            'uri' => $this->edit_url(true)
        );
        $delete = array(
            'title' => 'Удалить',
            'uri' => $this->delete_url(true)
        );
        $view = array(
            'title' => 'Просмотр',
            'uri' => $this->view_url(true)
        );
        $menu = array( //            $view,
//            $edit,
//            $delete,
        );
        Authority::can('view', $this->object_name(), $this) && $menu[] = $view;
        Authority::can('edit', $this->object_name(), $this) && $menu[] = $edit;
        Authority::can('delete', $this->object_name(), $this) && $menu[] = $delete;
        return $menu;
    }

    public static function alt_image($text)
    {
        $images = preg_match('"(\<(img[^\>]+)\>)"', $text, $matchs);
        if ($images) {
            foreach ($matchs as $match) {
                if (!strstr($match, 'alt') || strstr($match, 'alt=""')) {
                    return false;
                }
            }
        }
        return true;
    }

    public static function range_date($date_begin, $date_end)
    {
        return ($date_end > $date_begin);
    }

    public function last(){
        return $this->order_by('id','DESC')->limit(1)->find();
    }

    public function get_field($field)
    {
        if(array_key_exists($field,$this->has_many())){
            $data = array();
            foreach($this->{$field}->find_all() as $row){
                $data[] = $row->as_array_ext();
            }
            return $data;
        }
        return $this->__get($field);
    }

    public function as_array_ext($fields = array(),$action = null){
        $object = array();

        $fields_description = $this->fields_description();
        $fields_description = count($fields_description)>0?$fields_description:$this->_object;
        foreach ($fields_description as $column => $value)
        {
            if(count($fields) && in_array($column,$fields)){
                // Call __get for any user processing
                $object[$column] = $this->get_field($column);
            }elseif(count($fields)==0){
                $object[$column] = $this->get_field($column);
            }

        }

//        foreach ($this->_related as $column => $model)
//        {
//            // Include any related objects that are already loaded
//            $object[$column] = $model->as_array();
//        }

        return $object;
    }
}
