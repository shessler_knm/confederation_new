<?php defined('SYSPATH') or die('No direct script access.');

class ULogin
{
    public static $_errors = array();

    public static function widget_ulogin($count = null)
    {
        $config = Kohana::$config->load('ulogin');
        return '<div id="uLogin' . $count . '" data-ulogin="display=small;fields=email,' . $config->get('fields') . ';providers=' . $config->get('service') . ';hidden=' . $config->get('service_h') . ';redirect_uri=' . urlencode($config->get('redirect_uri')) . '"></div>';
    }

    public static function check_auth($token = null)
    {
        if (!$token) {
            $token = $_POST['token'];
        }
        $user = Auth::instance()->get_user();
        if ($user && !$user->loaded()) {
            $user = null;
        }
        if (!$user || $user->loaded()) {
            if ($token) {
                $s = file_get_contents('http://ulogin.ru/token.php?token=' . $token . '&host=' . getenv('HTTP_HOST'));
                $user = json_decode($s, true);
                if (!isset($user['error'])) {
                    $user = self::create_user($user);
                } else {
                    return $user['error'];
                }
            } else {
                return false;
            }
        }
        return $user;
    }

    public static function create_user($user)
    {
        $local_user = ORM::factory('User')->where('email', '=', Arr::get($user, 'email'))->find();
        if (!$local_user->loaded()) {
            $pass = mt_rand(100000, 999999);
            $local_user->values($user);
            $local_user->lastname = Arr::get($user, 'last_name');
            $local_user->firstname = Arr::get($user, 'first_name');
            $local_user->username = Arr::get($user, 'email');
            $local_user->password = Arr::get($user, 'password');
            $local_user->email = Arr::get($user, 'email');
            $local_user->socpage = Arr::get($user, 'identity');
            $local_user->reg_date = date('Y-m-d H:i:s');
            switch (Arr::get($user, 'sex')) {
                case 1:
                    $local_user->sex = 2;
                    break;
                case 2:
                    $local_user->sex = 1;
                    break;
                default:
                    $local_user->sex = null;
            }
            try {
                $local_user->save();
                $local_user->add('roles', ORM::factory('Role', array('name' => 'login')));
                Auth::instance()->force_login($local_user, TRUE);
            } catch (ORM_Validation_Exception $e) {
                self::$_errors = $e->errors('model');
                $page = new Cms_Page();
                if (Arr::get(self::$_errors, 'email')) {
                    $page->message(__("Указанный Вами email уже используется, попробуйте войти под своим логином/паролем на сайт, или восстановить пароль"), Cms_Page::PAGE_MESSAGE_ERROR);
                    unset(self::$_errors['email']);
                }
                foreach (self::$_errors as $error) {
                    $page->message($error, Cms_Page::PAGE_MESSAGE_ERROR);
                }
                self::$_errors = array();
                return false;
            }

        } else { //Иначе обнавляем инфу из соц сети
            $photo = $local_user->photo;
            $local_user->values($user);
            $local_user->photo = $photo;
            $local_user->save();
        }

        if (in_array('login', $local_user->roles->reset(false)->find_all()->as_array('id', 'name'))) {
            Auth::instance()->force_login($local_user, TRUE);
        }

        return $local_user;
    }
}

