<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Almas
 * Date: 07.10.13
 * Time: 10:03
 * To change this template use File | Settings | File Templates.
 */

class Pagination extends Kohana_Pagination
{

    /**
     * Применяет смещение в соответвии с вычеслиными параметрами
     * @param \IFiltered|\ORM $target
     * @return
     */
    public function apply(ORM $target)
    {
        return $target->offset($this->offset)->limit($this->items_per_page);
    }

    /**
     * Generates the full URL for a certain page.
     *
     * @param   integer  page number
     * @return  string   page URL
     */
    public function url($page = 1)
    {
        // Clean the page number
        $page = max(1, (int)$page);

        // No page number in URLs to first page
        if ($page === 1 AND !$this->config['first_page_in_url']) {
            $page = NULL;
        }

        switch ($this->config['current_page']['source']) {
            case 'query_string':

                return URL::site($this->_request->url() .
                $this->query(array($this->config['current_page']['key'] => $page)));

            case 'route':

                return URL::site($this->_route->uri(array_merge($this->_route_params,
                    array($this->config['current_page']['key'] => $page))) . $this->query());
        }

        return '#';
    }

}