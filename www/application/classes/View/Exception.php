<?php defined('SYSPATH') OR die('No direct script access.');

class View_Exception extends Kohana_View_Exception
{
    public function __construct() {
        throw new HTTP_Exception_404;
    }

}