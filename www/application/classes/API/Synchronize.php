<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Saidashev Marat
 * Date: 29.08.13
 * Time: 10:41
 * To change this template use File | Settings | File Templates.
 */

class API_Synchronize extends Synchronize_Receiver {

    /**
     * Формирует JSON ответ по массиву объектов
     * @param Database_MySQL_Result $collection
     * @param array $extra Дополнительные параметры которые нужно выслать вместе с массивом
     */
    public function response_collection($collection,$extra=array()) {
        $models=array();
        foreach ($collection as $model){
            $models[]=$this->collection_extra_for_model($model)+$this->as_array_ext($model);
        }
        $this->response->body(json_encode($models+$extra));
    }

    public function as_array_ext($model){
        $object = array();

        $fields_description = $model->object_fields();
        foreach ($fields_description as $column => $value)
        {
            // Call __get for any user processing
            $object[$column] = $model->$column;
        }


        return $object;
    }

    public function action_post_index() {
        if (!$this->request->post('items_available')) {
            parent::action_post_index();
        } else {
            $items_available = $this->request->post('items_available');
            $parent_id = $this->request->post('parent_id');
            $fed_id = $this->request->post('fed_id');
            $parent_model = $this->request->post('parent_model');
            $parent_hasmany = $this->request->post('parent_hasmany');
            $collection=null;
            $parent_model = ucfirst($parent_model); // На всякий случай.
            $model = ORM::factory($parent_model,$parent_id);

            if (is_array($items_available) && count($items_available) > 0) {
                if (count($items_available) > 1)
                    $collection=$model
                        ->$parent_hasmany
                        ->where(DB::expr('`'.$model->$parent_hasmany->object_name().'`.`id`'),DB::expr('NOT IN'),$items_available)
                        ->find_all();
                elseif (count($items_available) == 1)
                    $collection=$model
                        ->$parent_hasmany
                        ->where(DB::expr('`'.$model->$parent_hasmany->object_name().'`.`id`'),'!=',$items_available[0])
                        ->find_all();
            } else {
                $collection = $model->$parent_hasmany->find_all();
            }

            $collection = $this->create_collection($collection);
            $edits = $this->get_edits($model->$parent_hasmany->object_name(),$fed_id);
            $collection += $edits;
            $data = array();
            if ($collection) {
                $data = $this->create_tasks($collection);
            }

            //Отправляем
            $this->response_collection($collection, $data);

            //Очищаем таблицу edits
            $this->del_edits($edits);
        }
    }

    public function arr_fields($model, $types = array()) {
        $fields = array();
        foreach($model->fields_description() as $name => $settings) {
            if (isset($settings['type']) && in_array($settings['type'], $types)) {
                $fields[$name] = $settings['type'];
            }
        }
        return $fields;
    }

    /**
     * @param $model_name cur model name
     */
    public function get_edits($model_name, $fed_id) {
        $ret = array();
        $collection = ORM::factory('Synchedit')
            ->where('model_name','=',$model_name)
            ->where('fed_id','=',$fed_id)
            ->find_all();
        foreach ($collection as $item) {
            $ret[] = ORM::factory(ucfirst($item->model_name),(int)$item->uid);
        }
        return $ret;
    }

    public function del_edits($collection) {
        foreach ($collection as $item) {
            DB::delete('synchedits')->where('uid','=',$item->id)->where('model_name','=',$item->object_name())->execute();
        }
    }

    public function create_collection($collection) {
        $ret = array();
        foreach($collection as $item) {
            $ret[] = $item;
        }
        return $ret;
    }
}