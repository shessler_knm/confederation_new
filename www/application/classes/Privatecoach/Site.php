<?php

/**
 * Created by JetBrains PhpStorm.
 * User: doromor
 * Date: 30.12.13
 * Time: 10:58
 * To change this template use File | Settings | File Templates.
 */
class Privatecoach_Site extends Site
{
    protected $login_uri = "user/login";

    public function before()
    {
        parent::before();
        $this->ulogin = ULogin::widget_ulogin('PC');
        View::bind_global('ulogin', $this->ulogin);
    }

    public function current_lang()
    {
        $lang = $this->request->param('lang');
        switch ($lang) {
            case 'kz':
                I18n::lang('kz');
                break;
            case 'en':
                I18n::lang('en');
                break;
            default:
                I18n::lang('ru');
        }
    }
}