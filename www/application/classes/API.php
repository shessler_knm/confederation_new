<?php

/**
 * Created by PhpStorm.
 * User: sgm
 * Date: 10.02.14
 * Time: 10:18
 */
class API extends Cms_API
{
    protected $rest_login_uri = "/rest_api/user/no_user";
    public $raw_values;
    public $per_page;
    public $offset;
    public $sorting;
    public $filters = array();


    public function user_detection()
    {
        $this->user = Auth::instance()->get_user();
        if (!$this->user) {
            if ($token = $this->request->headers('Session')) {
                $token = ORM::factory('User_Token', array('token' => $token));
                if ($token->loaded() AND $token->user->loaded()) {
                    // Save the token to create a new unique token
                    $token->save();
                    $auth = Auth::instance();
                    // Complete the login with the found data
                    $auth->force_login($token->user);
                    // Automatic login was successful
                    $this->user = $token->user;
                }
            }
        }
    }


    public function before()
    {
        parent::before();
        $this->lang_detection('route');
//        HTTP::check_cache()
        $this->raw_values = $this->body_to_array();
        $this->per_page = Arr::get($this->raw_values, 'limit', $this->perpage);
        $this->offset = Arr::get($this->raw_values, 'offset', $this->offset);
        $this->sorting = Arr::get($this->raw_values, 'sorting', 'ASC');
        $this->filters = Arr::get($this->raw_values, 'option', array());
        $this->response->headers('Content-type', 'application/json');
    }

    public function body_to_array()
    {
        return (array)json_decode($this->request->body(), true);
    }

    protected function _login()
    {
//        Session::instance()->set('after_login', $this->request->uri());

//        $this->response(array('error' => 'need auth'));
        die(json_encode(array('error' => 'Invalid token')));
//        Request::factory($this->rest_login_uri)->execute();
//        $this->redirect($this->rest_login_uri);

    }

    public function parse_url_if_valid($url)
    {
        // Массив с компонентами URL, сгенерированный функцией parse_url()
        $arUrl = parse_url($url);
        // Возвращаемое значение. По умолчанию будет считать наш URL некорректным.
        $ret = null;

        // Если не был указан протокол, или
        // указанный протокол некорректен для url
        if (!array_key_exists("scheme", $arUrl)
            || !in_array($arUrl["scheme"], array("http", "https"))
        )
            // Задаем протокол по умолчанию - http
            $arUrl["scheme"] = "http";

        // Если функция parse_url смогла определить host
        if (array_key_exists("host", $arUrl) &&
            !empty($arUrl["host"])
        )
            // Собираем конечное значение url
            $ret = sprintf("%s://%s%s", $arUrl["scheme"],
                $arUrl["host"], $arUrl["path"]);

        // Если значение хоста не определено
        // (обычно так бывает, если не указан протокол),
        // Проверяем $arUrl["path"] на соответствие шаблона URL.
        else if (preg_match("/^\w+\.[\w\.]+(\/.*)?$/", $arUrl["path"]))
            // Собираем URL
            $ret = sprintf("%s://%s", $arUrl["scheme"], $arUrl["path"]);

        // Если url валидный и передана строка параметров запроса
        if ($ret && empty($ret["query"]))
            $ret .= sprintf("?%s", $arUrl["query"]);

        return $ret;
    }

    public function lang_detection($param = "param", $cookie = true)
    {
        if ($param == "query") {
            $lang = strval($this->request->query('lang'));
        } else {
            $lang = strval($this->request->param('lang'));
        }
        if ($lang AND preg_match("/^(ru|en|kz)$/", $lang)) {
            I18n::lang($lang);
        }
        if ($this->user) {
            $this->user->lang = I18n::$lang;
            $this->user->save();
        }
    }

    /**
     * Формирует JSON ответ по модели
     * @param ORM $model
     * @param array $extra Дополнительные параметры которые нужно выслать вместе с моделью
     */
    public function response_model($model, $extra = array())
    {
        $data = $extra + $model->as_array_ext();
        $this->response->body(json_encode($data));
    }

    public function response($object, $fields = array(), $code = 200, $full_response = true)
    {
        $data = array();
        $found = preg_match('#_(.*)#', $this->request->action(), $action);
        $action = $found ? $action[1] : null;
        if ($object instanceof Database_MySQL_Result || $object instanceof Database_Result_Cached) {
            foreach ($object as $row) {
                $data[] = $row->as_array_ext($fields, $action);
            }
        } elseif ($object instanceof ORM) {
            if ($object->loaded()) {
                $data = $object->as_array_ext($fields, $action);
            } else {
                try {
                    $object = $object->limit($this->per_page)->offset($this->offset)->find_all();
                    foreach ($object as $row) {
                        $data[] = $row->as_array_ext($fields, $action);
                    }
                } catch (Exception $e) {
                    die($e);
                }
            }
        } else {
            $data = $object;
        }
        $this->response->status($code);
        if ($full_response) {
            $this->response->body(json_encode($data));
        } else {
            $this->response->body("true");
        }
        $this->check_cache();
    }

    public function check_cache($etag = NULL)
    {
        return HTTP::check_cache($this->request, $this->response, $etag);
    }


} 