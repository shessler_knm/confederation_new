<?php

class Model_Article extends ORM
{
    protected $_route = 'article';
    protected $_has_many = array(
        'categories' => array(
            'model' => 'Category',
            'through' => 'article_categories',
        ),
        'article_scores' => array(
            'model' => 'Article_Score',
            'foreign_key' => 'target_id'
        )
//        'sport'=> array(
//            'model'=> 'Category',
//            'through'=>'news_sports',
//            'far_key'=>'category_id'
//        ),
//        'comments' => array(
//            'model' => 'comment',
//            'foreign_key' => 'news_id'
//        ),
    );
    protected $_belongs_to = array(
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
    );

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'fulltext' => true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'text' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'type' => 'text',
                'label' => 'Текст',
                'fulltext' => true,
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Изображение',
                'params' => array(
                    'need_help' => true,
                ),
            ),
            'date' => array(
                'edit' => true,
                'head' => false,
                'search' => false,
                'type' => 'datetime',
                'label' => 'Дата публикации',
            ),
            'main' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Вывод на главной',
                'params' => array(
                    'options' => array(
                        1 => 'Не отображать',
                        2 => 'Отобразить'
                    )
                )
            ),
            'score' => array(
                'edit' => false,
                'head' => false,
                'type' => 'strings',
                'label' => 'Оценка',
            ),
            'categories' => array(
                'edit' => true,
                'head' => true,
                'type' => 'external_select',
                'label' => 'Вид новости',
                'params' => array(
                    'src' => ORM::factory('Category')->get_sef_article(),
                    'value' => $this->categories->select_options('id', 'title_ru')
                )
            ),
            'is_published' => array(
                'edit' => true,
                'head' => true,
                'type' => 'select',
                'label' => 'Отображать на сайте',
                'params' => array(
                    'options' => array(
                        0 => 'Не опубликовать',
                        1 => 'Опубликовать'
                    )
                )
            ),
//            'sport'=>array(
//                'edit'=>true,
//                'head'=>true,
//                'search'=>false,
//                'type'=>'select_extended',
//                'label'=>'Вид спорта',
//                'params' => array(
//                    'options' => ORM::factory('Category')->get_sef_sports()->select_options('id', 'title_ru'),
//                    'value' => $this->sport->select_options('id', 'title_ru')
//                )
//            ),
//            'author' => array(
//                'edit' => true,
//                'head' => true,
//                'search'=>true,
//                'type' => 'strings',
//                'label' => 'Автор',
//            ),
//            'source' => array(
//                'edit' => true,
//                'head' => true,
//                'search'=>true,
//                'type' => 'strings',
//                'label' => 'Источник информации',
//            ),
        );
    }

    public function rules()
    {
        return array(
            'title_ru' => array(
                array('not_empty'),
            ),
            'text_ru' => array(
                array('not_empty')
            ),
            'date' => array(
                array('not_empty'),
                array('date')
            ),
//            'source'=>array(
//                array('Valid::url'),
//            ),
        );
    }

    public function get_field($field)
    {
        if ($field == 'title') {
            return Text::limit_chars(strip_tags($this->title), 100, " ...");
        }
        if ($field == 'categories') {
            $results = array();
            foreach ($this->categories->find_all() as $row) {
                $results[] = $row->title_ru;
            }
            $results = implode('<br>', $results);
            return $results;
        }
        if ($field == 'is_published') {
            $result = $this->is_published == 1 ? 'Опубликовано' : 'Сохранено';
            return $result;
        }
//        if ($field == 'sport') {
//            $results = array();
//            foreach ($this->categories->find_all() as $row) {
//                $results[] = $row->title_ru;
//            }
//            $results = implode('<br>', $results);
//            return $results;
//        }
        return parent::get_field($field);
    }


    public function filters()
    {
        return array(
            'title_ru' => array(
                array('strip_tags')
            ),
            'title_en' => array(
                array('strip_tags')
            ),
            'title_kz' => array(
                array('strip_tags')
            ),
            'text_ru' => array(
                array('ORM::clear_html')
            ),
            'text_en' => array(
                array('ORM::clear_html')
            ),
            'text_kz' => array(
                array('ORM::clear_html')
            ),
//            'author'=>array(
//                array('strip_tags')
//            ),
//            'source'=>array(
//                array('strip_tags')
//            ),
            'date' => array(
                array('Date::formatted_time'),
            ),
        );
    }


    public function get_title()
    {
        return $this->title;
    }

    public function view_url($uri = false)
    {
        $_uri = 'privatecoach/' . $this->_route . '/view/' . $this->id;
        return $uri ? $_uri : URL::site($_uri, 'http');
    }


    public function actions($user)
    {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
            array(
                'title' => 'Просмотр',
                'uri' => $this->view_url()
            ),
        );
        return $menu;
    }

    function date_diff($date1, $date2)
    {
        $diff = strtotime($date2) - strtotime($date1);
        return abs($diff);
    }

    public function as_array_ext($fields = array(), $action = null)
    {
        $data = parent::as_array_ext($fields, $action);
        $photo = null;
        $categories = array();
        if ($this->photo) {
            $photo = $this->photo_s->url('http');
        }
        $data['photo'] = $photo;
        $data['date'] = Date::formatted_time($this->date,'Y-m-d');
        $model_categories = $this->categories->find_all();
        foreach ($model_categories as $category) {
            $categories[] = $category->title;
        }
        $data['categories'] = $categories;
        $comments_count = Model_Comment::count_comments($this->table_name(), $this->id);
        $data['comments_count'] = $comments_count;
        $data['views_count'] = $this->views;
        if ($action == 'view') {
            $data['link'] = $this->view_url();
        }
        return $data;
    }
}