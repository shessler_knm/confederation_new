<?php
class Model_Referee extends ORM_Embeded implements ISearchable
{
    protected $_parent_field="federation_id";
    protected $_parent_model="federation";
    protected $_table_name = 'referees';
    protected $_belongs_to=array(
        'federation'=>array(),
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
    );
    protected $_route='referee';
    protected function get_fields_description(){
        return array(
            'id'=>array(
                'head'=>true,
                'label'=>'#',
            ),
            'firstname' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'fulltext'=>true,
                'type' => 'strings',
                'label' => 'Имя',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'middlename' => array(
                'edit' => true,
                'head' => true,
                'fulltext'=>true,
                'search'=>true,
                'type' => 'strings',
                'label' => 'Отчество',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'lastname' => array(
                'edit' => true,
                'head' => true,
                'fulltext'=>true,
                'search'=>true,
                'type' => 'strings',
                'label' => 'Фамилия',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'biography' => array(
                'edit' => true,
                'head' => false,
//                'search'=>true,
                'fulltext'=>true,
                'type' => 'text',
                'label' => 'Биография',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'post'=>array(
                'edit'=>true,
                'head'=>true,
                'search'=>true,
                'type'=>'strings',
                'label'=>'Пост',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'category' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'select',
                'label' => 'Категории',
                'params' => array(
                    'options' => array(
                        '1' => 'Международная',
                        '2' => 'Национальная',
                        '3'=> 'Высшая национальная',
                    )
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Изображение',
                'params' => array(
                    'need_help' => true,
                ),
            ),
            'federation_id'=>array(
                'edit'=>true,
                'head'=>true,
                'type'=>'select',
                'label'=>'Федерация',
                'params'=>array(
                    'options'=>ORM::factory('Federation')->select_options('id','title_ru')
                    )
            ),
            'address' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'search'=>true,
                'label' => 'Адрес',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
        )
        );
    }

    public function rules()
    {
        return array(
            'firstname_ru' => array(
                array('not_empty'),
            ),
            'lastname_ru' => array(
                array('not_empty'),
            ),
            'biography_ru' => array(
                array('not_empty'),
            ),
            'post_ru' => array(
                array('not_empty'),
            ),
            'category' => array(
                array('not_empty')
            ),
            'federation_id' => array(
                array('not_empty'),
            ),
        );
    }

    public function get_full_name()
    {
        return $this->lastname." ".$this->firstname." ".$this->middlename;
    }

    public function get_field($field)
    {
        if ($field == 'federation_id'){
        return ORM::factory('Federation',$this->federation_id)->title_ru;
        }
        if($field == 'category'){
        switch ($this->category) {
            case 1: $r = 'Международная';break;
            case 2: $r = 'Национальная';break;
            case 3: $r = 'Высшая национальная';break;
            default: $r = 'Не указано';
            break;
            }
        return $r;
        }
        return parent::get_field($field);
    }
    public function filters()
    {
        return array(
            'firstname_ru'=>array(
                array('strip_tags')
            ),
            'firstname_en'=>array(
                array('strip_tags')
            ),
            'firstname_kz'=>array(
                array('strip_tags')
            ),
            'lastname_ru'=>array(
                array('strip_tags')
            ),
            'lastname_en'=>array(
                array('strip_tags')
            ),
            'lastname_kz'=>array(
                array('strip_tags')
            ),
            'middlename_ru'=>array(
                array('strip_tags')
            ),
            'middlename_en'=>array(
                array('strip_tags')
            ),
            'middlename_kz'=>array(
                array('strip_tags')
            ),
            'post_ru'=>array(
                array('strip_tags')
            ),
            'post_en'=>array(
                array('strip_tags')
            ),
            'post_kz'=>array(
                array('strip_tags')
            ),
            'biography_ru'=>array(
                array('ORM::clear_html')
            ),
            'biography_en'=>array(
                array('ORM::clear_html')
            ),
            'biography_kz'=>array(
                array('ORM::clear_html')
            ),
            'sef'=>array(
                array('ORM::full_name_filter_sef',array(':value',':model','title_ru')),
            ),
        );
    }
    public function view_url($uri = false)
    {
        $_uri = URL::site($this->federation->sef . '/referee/view/' . $this->id);
        if(Register::instance()->search){
            $_uri = '/'.$this->_route.'/view/'.$this->id;
        }
        return $uri ? $_uri : URL::site($_uri);
    }

    public function actions($user) {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
            array(
                'title' => 'Просмотр',
                'uri'=>$this->view_url()
            ),
        );
        return $menu;
    }
    
     public function title(){
        $arg = func_get_args();
        if(!count($arg)){
            return $this->firstname.' '.$this->lastname.' '.$this->middlename;
        }else{
            return $this->{'firstname_'.$arg[0]}.' '.$this->{'lastname_'.$arg[0]}.' '.$this->{'middlename_'.$arg[0]};
        }
    }
    
    public function short_text() {
        $arg = func_get_args();
        if(!count($arg)){
            return $this->biography;
        }else{
            return $this->{'biography_'.$arg[0]};
        }
    }
    
    public function search_extended_index()
    {
        $ext = array();
        $ext[] = Search::escape_extend('fed_id',  $this->federation->sef);
        $ext[] = Search::escape_extend('fed_id','confederation');
        return implode('\n', $ext);
    }
}
?>
