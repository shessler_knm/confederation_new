<?php

/**
 * Created by PhpStorm.
 * User: id541rak
 * Date: 14.04.14
 * Time: 10:50
 */
class Model_Ingredient extends ORM
{

    protected $_route = 'ingredient';
    protected $_belongs_to = array(
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
    );
    protected $_has_many = array(
        'dish_ingredients' => array(
            'model' => 'Dish_Ingredient',
            'foreign_key' => 'ingredient_id'
        ),
    );

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'fulltext' => true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'description' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'type' => 'text',
                'label' => 'Текст',
                'fulltext' => true,
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Изображение',
                'params' => array(
                    'need_help' => true,
                ),
            ),
            'is_published' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'select',
                'label' => 'Отображать на сайте',
                'params' => array(
                    'options' => array(
                        '0' => 'Нет',
                        '1' => 'Да'
                    )
                )
            ),
            'protein' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'Белки, г(на 100г)',
            ),
            'carbohydrate' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'Углеводы, г(на 100г)',
            ),
            'fat' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'Жиры, г(на 100г)',
            ),
            'calories' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'Калории(на 100г)',
            ),
//            'sef' => array(
//                'edit' => true,
//                'head' => false,
//                'type' => 'strings',
//                'label' => 'ЧПУ',
//            ),
//            'sef_ru' => array(
//                'edit' => true,
//                'head' => false,
//                'type' => 'strings',
//                'label' => 'ЧПУ на русском языке',
//            ),
//            'sef_en' => array(
//                'edit' => true,
//                'head' => false,
//                'type' => 'strings',
//                'label' => 'ЧПУ на английском языке',
//            ),
//            'sef_kz' => array(
//                'edit' => true,
//                'head' => false,
//                'type' => 'strings',
//                'label' => 'ЧПУ на казахском языке',
//            ),
        );
    }

    public function as_array_ext($fields = array(), $action = 'index'){
        $data = parent::as_array_ext($fields);
        $data['description_text'] = $data['description'];
        unset($data['description']);
        if ($this->photo_s->loaded()) {
            $data['photo'] = $this->photo ? $this->photo_s->url(true) : null;
        }
        return $data;
    }

    public function rules()
    {
        return array(
            'title_ru' => array(
                array('not_empty'),
            ),
            'description_ru' => array(
                array('not_empty'),
            ),
            'protein' => array(
                array('not_empty'),
                array(array($this,'check_mass'))
            ),
            'carbohydrate' => array(
                array('not_empty'),
                array(array($this,'check_mass'))
            ),
            'fat' => array(
                array('not_empty'),
                array(array($this,'check_mass'))
            ),
            'calories' => array(
                array(array($this,'check_calories'))
            ),
        );
    }

    public function check_mass(){
        $check_sum = $this->protein + $this->carbohydrate + $this->fat;
        return $check_sum<=100;
    }

    public function check_calories($value){
        return $value<=900;
    }

    public function filter_calories($value){
        if (!$value){
            $value = 4*($this->protein + $this->carbohydrate) + 9 * $this->fat;
        }
        return $value;
    }

    public function get_field($field)
    {
        if ($field == 'title') {
            return Text::limit_chars(strip_tags($this->title), 100, " ...");
        }
        if ($field == 'category_id') {
            return $this->category->title_ru;
        }
        if ($field == 'is_published') {
            return $this->is_published==0?'Не опубликовано':'Опубликовано';
        }
        return parent::get_field($field);
    }


    public function filters()
    {
        return array(
            'title_ru' => array(
                array('strip_tags')
            ),
            'title_en' => array(
                array('strip_tags')
            ),
            'title_kz' => array(
                array('strip_tags')
            ),
            'text_ru' => array(
                array('ORM::clear_html')
            ),
            'text_en' => array(
                array('ORM::clear_html')
            ),
            'text_kz' => array(
                array('ORM::clear_html')
            ),
            'calories' => array(
                array(array($this, 'filter_calories'), array(':value')),
            ),

//            'sef' => array(
//                array('ORM::filter_sef', array(':value', ':model', 'title_ru')),
//            ),
//            'sef_ru' => array(
//                array('ORM::filter_sef', array(':value', ':model', 'title_ru')),
//            ),
//            'sef_en' => array(
//                array('ORM::filter_sef', array(':value', ':model', 'title_en')),
//            ),
//            'sef_kz' => array(
//                array('ORM::filter_sef', array(':value', ':model', 'title_kz')),
//            ),
        );
    }

    public function view_url($uri = false, $lang = null)
    {
        return 'confederation/privatecoach/diet/' . $this->id;
    }

    public function actions($user)
    {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
            array(
                'title' => 'Просмотр',
                'uri' => $this->view_url()
            ),
        );
        return $menu;
    }

} 