<?php
class Model_Result extends ORM implements Synchronize_ISynch
{
    protected $_belongs_to = array(
        'federation' => array(),
        'event' => array(
            'model' => 'Event',
            'foreign_key' => 'event_id'
        ),
        'photoru_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photoru'
        ),
        'photoen_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photoen'
        ),
        'photokz_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photokz'
        ),
        'city' => array(
            'model' => 'City',
            'foreign_key' => 'city_id'
        ),
    );

    protected $_route = 'result';

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'federation_id' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'select',
                'label' => 'Федерация',
                'params' => array(
                    'options' => ORM::factory('Federation')->select_options('id', 'title_ru')
                )
            ),
            'city_id' => array(
                'edit' => true,
                'head' => true,
//                'search'=>true,
                'type' => 'select',
                'label' => 'Город',
                'params' => array(
                    'options' => ORM::factory('City')->select_options('id', 'title_ru'),
                )
            ),
            'event_id' => array(
                'edit' => true,
                'head' => true,
                'type' => 'select',
                'label' => 'Событие',
                'params' => array(
                    'options' => array()
                )
            ),
            'date' => array(
                'edit' => true,
                'head' => true,
//                'search'=>true,
                'type' => 'date',
                'label' => 'Дата',
            ),
            'photoru' => array(
                'edit' => true,
                'type' => 'file',
                'label' => 'Результаты на русском языке',
            ),
            'photoen' => array(
                'edit' => true,
                'type' => 'file',
                'label' => 'Результаты на английском языке',
            ),
            'photokz' => array(
                'edit' => true,
                'type' => 'file',
                'label' => 'Результаты на казахском языке',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Название',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'is_published' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Отображать на сайте',
                'params' => array(
                    'options' => array(
                        1=>'Отображать',
                        2=>'Не отображать'
                    )
                )
            ),
        );
    }

    public function rules()
    {
        return array(
            'date' => array(
                array('not_empty'),
                array('date')
            ),
            'event_id' => array(
                array('not_empty')),

            'federation_id' => array(
                array('not_empty'),
            ),
            'title_ru' => array(
                array('not_empty'),
            ),
            'photoru' => array(
                array('not_empty'),
            ),
        );
    }

    public function get_field($field)
    {
        if ($field == 'federation_id') {
            return ORM::factory('Federation', $this->federation_id)->title_ru;
        }
        if ($field == 'city_id') {
            return ORM::factory('City', $this->city_id)->title_ru;
        }
        if ($field == 'event_id') {
            return ORM::factory('Event', $this->event_id)->title_ru;
        }
        if ($field == 'title') {
            return Text::limit_chars(strip_tags($this->title), 100, " ...");
        }
        return parent::get_field($field);
    }

    public function filters()
    {
        return array(
            'date' => array(
                array('Date::formatted_time', array(':value', 'Y-m-d')),
            ),
            'title_ru' => array(
                array('strip_tags')
            ),
            'title_en' => array(
                array('strip_tags')
            ),
            'title_kz' => array(
                array('strip_tags'),
            ),
            'photoru' => array(
                array('ORM::to_null')
            ),
            'photoen' => array(
                array('ORM::to_null')
            ),
            'photokz' => array(
                array('ORM::to_null')
            ),
        );
    }

    public function actions($user)
    {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
        );
        return $menu;
    }

    public function alt_file()
    {
        if (I18n::lang() == 'ru') {
            if ($this->photoru_s->loaded()) {
                return $this->photoru_s->download_file_url();
            } elseif ($this->photoen_s->loaded()) {
                return $this->photoen_s->download_file_url();
            } else {
                return $this->photokz_s->download_file_url();
            }
        } elseif (I18n::lang() == 'en') {
            if ($this->photoen_s->loaded()) {
                return $this->photoen_s->download_file_url();
            } elseif ($this->photoru_s->loaded()) {
                return $this->photoru_s->download_file_url();
            } else {
                return $this->photokz_s->download_file_url();
            }
        } elseif (I18n::lang() == 'kz') {
            if ($this->photokz_s->loaded()) {
                return $this->photokz_s->download_file_url();
            } elseif ($this->photoru_s->loaded()) {
                return $this->photoru_s->download_file_url();
            } else {
                return $this->photoen_s->download_file_url();
            }
        }
    }

    public function alt_size()
    {
        if (I18n::lang() == 'ru') {
            if ($this->photoru_s->loaded()) {
                return pathinfo($this->photoru_s->original_name, PATHINFO_EXTENSION).', '.$this->photoru_s->human_filesize($this->photoru_s->size);
            } elseif ($this->photoen_s->loaded()) {
                return pathinfo($this->photoen_s->original_name, PATHINFO_EXTENSION).', '.$this->photoen_s->human_filesize($this->photoen_s->size);
            } else {
                return pathinfo($this->photokz_s->original_name, PATHINFO_EXTENSION).', '.$this->photokz_s->human_filesize($this->photokz_s->size);
            }
        } elseif (I18n::lang() == 'en') {
            if ($this->photoen_s->loaded()) {
                return pathinfo($this->photoen_s->original_name, PATHINFO_EXTENSION).', '.$this->photoen_s->human_filesize($this->photoen_s->size);
            } elseif ($this->photoru_s->loaded()) {
                return pathinfo($this->photoru_s->original_name, PATHINFO_EXTENSION).', '.$this->photoru_s->human_filesize($this->photoru_s->size);
            } else {
                return pathinfo($this->photokz_s->original_name, PATHINFO_EXTENSION).', '.$this->photokz_s->human_filesize($this->photokz_s->size);
            }
        } elseif (I18n::lang() == 'kz') {
            if ($this->photokz_s->loaded()) {
                return pathinfo($this->photokz_s->original_name, PATHINFO_EXTENSION).', '.$this->photokz_s->human_filesize($this->photokz_s->size);
            } elseif ($this->photoru_s->loaded()) {
                return pathinfo($this->photoru_s->original_name, PATHINFO_EXTENSION).', '.$this->photoru_s->human_filesize($this->photoru_s->size);
            } else {
                return pathinfo($this->photoen_s->original_name, PATHINFO_EXTENSION).', '.$this->photoen_s->human_filesize($this->photoen_s->size);
            }
        }
    }

    public function alt_icon()
    {
        if (I18n::lang() == 'ru') {
            if ($this->photoru_s->loaded()) {
                return pathinfo($this->photoru_s->original_name, PATHINFO_EXTENSION);
            } elseif ($this->photoen_s->loaded()) {
                return pathinfo($this->photoen_s->original_name, PATHINFO_EXTENSION);
            } else {
                return pathinfo($this->photokz_s->original_name, PATHINFO_EXTENSION);
            }
        } elseif (I18n::lang() == 'en') {
            if ($this->photoen_s->loaded()) {
                return pathinfo($this->photoen_s->original_name, PATHINFO_EXTENSION);
            } elseif ($this->photoru_s->loaded()) {
                return pathinfo($this->photoru_s->original_name, PATHINFO_EXTENSION);
            } else {
                return pathinfo($this->photokz_s->original_name, PATHINFO_EXTENSION);
            }
        } elseif (I18n::lang() == 'kz') {
            if ($this->photokz_s->loaded()) {
                return pathinfo($this->photokz_s->original_name, PATHINFO_EXTENSION);
            } elseif ($this->photoru_s->loaded()) {
                return pathinfo($this->photoru_s->original_name, PATHINFO_EXTENSION);
            } else {
                return pathinfo($this->photoen_s->original_name, PATHINFO_EXTENSION);
            }
        }
    }

    public function form_list_button(Component $cp) {
        $cp->button_link(URL::site('admin/result/synchronize_model'), "Синхронизировать с Федерациями", array('class' => 'btn btn-primary'));
    }
}