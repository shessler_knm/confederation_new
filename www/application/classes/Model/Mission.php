<?php
class Model_Mission extends ORM
{
    protected $_route = 'mission';
    protected $_belongs_to = array(
        'user' => array(
            'model' => 'User',
            'foreign_key' => 'user_id'
        ),
    );

    public function filters()
    {
        return array(
            'title_ru' => array(
                array('strip_tags')
            ),
            'title_en' => array(
                array('strip_tags')
            ),
            'title_kz' => array(
                array('strip_tags')
            ),
            'sef' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_ru')),
            ),

        );
    }

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'fulltext' => true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'sef' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ',
            ),
        );
    }

    public function rules()
    {
        return array(
            'title_ru' => array(
                array('not_empty'),
            ),
        );
    }
}

?>
