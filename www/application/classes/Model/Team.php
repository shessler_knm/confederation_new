<?php

class Model_Team extends ORM implements Synchronize_ISynch
{
    protected $_route = 'team';
    protected $_belongs_to = array(
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
        'federation' => array(
            'model' => 'Federation',
            'foreign_key' => 'federation_id'
        ),
    );
    protected $_has_many = array(
        'players' => array(),
        'coaches' => array(
            'model' => 'Coach',
            'far_key' => 'team_id'
        ),
    );

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'fulltext' => true,
                'type' => 'strings',
                'label' => 'Название',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'text' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'fulltext' => true,
                'type' => 'strings',
                'label' => 'Описание',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'federation_id' => array(
                'edit' => true,
                'head' => true,
//                'search'=>true,
                'type' => 'select',
                'label' => 'Федерация',
                'params' => array(
                    'options' => ORM::factory('Federation')->select_options('id', 'title_ru')
                )
            ),
            'players' => array(
                'type' => 'embeded',
                'label' => 'Спортсмены',
                'edit' => true,
            ),
            'main' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Вывод на странице',
                'params' => array(
                    'options' => array(
                        1 => 'Не отображать',
                        2 => 'Отобразить'
                    )
                )
            ),
            'synch_site' => array(
                'edit' => false,
                'head' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Данные с сайта',
            ),
            'is_published' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Отображать на сайте',
                'params' => array(
                    'options' => array(
                        1 => 'Отображать',
                        2 => 'Не отображать'
                    )
                )
            ),
            'rio' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Рио 2016',
                'params' => array(
                    'options' => array(
                        0 => 'Нет',
                        1 => 'Да'
                    )
                )
            ),
            'sef' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ',
            ),
            'sef_ru' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на русском языке',
            ),
            'sef_en' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на английском языке',
            ),
            'sef_kz' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на казахском языке',
            ),
        );
    }

    public function rules()
    {
        return array(
            'title_ru' => array(
                array('not_empty'),
            ),
            'federation_id' => array(
                array('not_empty'),
            ),
        );
    }

    public function get_field($field)
    {
        if ($field == 'federation_id') {
            return ORM::factory('Federation', $this->federation_id)->title_ru;
        }
        return parent::get_field($field);
    }

    public function filters()
    {
        return array(
            'title_ru' => array(
                array('strip_tags')
            ),
            'title_en' => array(
                array('strip_tags')
            ),
            'title_kz' => array(
                array('strip_tags')
            ),
            'text_ru' => array(
                array('strip_tags')
            ),
            'text_en' => array(
                array('strip_tags')
            ),
            'text_kz' => array(
                array('strip_tags'),
            ),
            'sef' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_ru')),
            ),
            'sef_ru' => array(
                array('ORM::filter_team_sef', array(':value', ':model', 'title_ru','id')),
            ),
            'sef_en' => array(
                array('ORM::filter_team_sef', array(':value', ':model', 'title_en','id')),
            ),
            'sef_kz' => array(
                array('ORM::filter_team_sef', array(':value', ':model', 'title_kz','id')),
            ),

        );
    }

    public function view_url($uri = false)
    {
        $fed = $this->federation;
        $lang_route = I18n::$lang != 'kz' ? I18n::$lang . '/' : '/';
        $_uri = $lang_route . $fed->sef . '/' . $this->_route . '/' . $this->{'sef_' . I18n::$lang};
        if (Register::instance()->search) {
            $_uri = $lang_route . $fed->sef . '/' . $this->_route . '/' . $this->{'sef_' . I18n::$lang};
        }
        return $uri ? $_uri : URL::site($_uri);
    }

    public function map_url($lang = null, $coach = false)
    {
        if ($coach){
            $_uri = '/coach/' . $this->{'sef_' . $lang};
        }else{
            $_uri = '/' . $this->_route . '/' . $this->{'sef_' . $lang};
        }
        return $_uri;
    }

    public function actions($user)
    {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
            array(
                'title' => 'Просмотр',
                'uri' => $this->view_url()
            ),
        );
        return $menu;
    }

    public function form_list_button(Component $cp)
    {
        $cp->button_link(URL::site('admin/team/synchronize_model'), "Синхронизировать с Федерациями", array('class' => 'btn btn-primary'));
    }
}