<?php

class Model_Pedometer extends ORM
{
    protected $_belongs_to = array(
        'program' => array(
            'model' => 'User',
            'foreign_key' => 'user_id'
        ),
    );

    public function filters()
    {
        return array(
            'date' => array(
                array('Date::formatted_time', array(':value', 'Y-m-d'))
            ),
        );
    }

    public function as_array_ext($fields = array(), $action = null)
    {
        $data = parent::as_array_ext($fields);
        $date = Date::formatted_time($data['datetime'], 'Y-m-d');
        $time = Date::formatted_time($data['datetime'], 'H:i');
        $data['date'] = $date;
        if ($action != 'statistic') {
            $data['time'] = $time;
        }
        unset($data['datetime']);
        return $data;
    }
}