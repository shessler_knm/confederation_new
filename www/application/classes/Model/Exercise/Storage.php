<?php

class Model_Exercise_Storage extends ORM_Embeded
{
    protected $_parent_field = "exercise_id";
    protected $_parent_model = "Exercise";
    protected $_table_name = "exercise_storage";
    protected $_route = 'exercise_storage';
    protected $_belongs_to = array(
        'exercise' => array(
            'model' => 'Exercise',
            'foreign_key' => 'exercise_id'
        ),
        'storage_id_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'storage_id'
        ),
    );


    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'storage_id' => array(
                'edit' => true,
                'head' => true,
                'type' => 'image',
                'label' => 'Изображение',
            ),
//            'uploads' => array(
//                'type' => 'multifile',
//                'label' => 'Мультизагрузка файлов',
//                'edit' => true,
//            ),
        );
    }

    public function get($column)
    {
        if ($column == 'uploads') {
            return null;
        }
        return parent::get($column);
    }

    public function get_field($field)
    {
        if ($field == 'storage_id') {
            return $this->storage_id_s->html_img(240, 180);
        }
        return parent::get_field($field);
    }
}
