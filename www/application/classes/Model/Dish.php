<?php

/**
 * Created by PhpStorm.
 * User: id541rak
 * Date: 14.04.14
 * Time: 10:50
 */
class Model_Dish extends ORM
{

    protected $_table_name = 'dishes';
    protected $_route = 'dish';
    protected $_belongs_to = array(
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
        'category' => array(
            'model' => 'Category',
            'foreign_key' => 'category_id',
        ),
    );
    protected $_has_many = array(
        'dish_ingredients' => array(
            'Model' => 'Dish_Ingredient',
        ),
        'ingredients' => array(
            'model' => 'Ingredient',
            'through' => 'dish_ingredients'
        ),
    );

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'fulltext' => true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'description' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'type' => 'text',
                'label' => 'Текст',
                'fulltext' => true,
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Изображение',
                'params' => array(
                    'need_help' => true,
                ),
            ),
//            'ingredients' => array(
//                'edit' => true,
//                'head' => true,
//                'type' => 'external_select',
//                'label' => 'Ингредиенты',
//                'params' => array(
//                    'src' => ORM::factory('Ingredient')->external_list()
//                )
//            ),
            'category_id' => array(
                'edit' => true,
                'head' => true,
                'type' => 'select',
                'label' => 'Категория',
                'params' => array(
                    'options' => ORM::factory('Category')->get_sef_dishes()->find_all()->as_array('id', 'title_ru')
                )
            ),
            'is_published' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'select',
                'label' => 'Отображать на сайте',
                'params' => array(
                    'options' => array(
                        '0' => 'Не опубликовано',
                        '1' => 'Опубликовано'
                    )
                )
            ),
            'protein' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'Белки, г(на 100г)',
            ),
            'carbohydrate' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'Углеводы, г(на 100г)',
            ),
            'fat' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'Жиры, г(на 100г)',
            ),
            'calories' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'Калории (на 100г)',
            ),
            'dish_ingredients' => array(
                'type' => 'embeded',
                'label' => 'Ингредиенты',
                'edit' => true,
            ),

//            'sef' => array(
//                'edit' => true,
//                'head' => false,
//                'type' => 'strings',
//                'label' => 'ЧПУ',
//            ),
//            'sef_ru' => array(
//                'edit' => true,
//                'head' => false,
//                'type' => 'strings',
//                'label' => 'ЧПУ на русском языке',
//            ),
//            'sef_en' => array(
//                'edit' => true,
//                'head' => false,
//                'type' => 'strings',
//                'label' => 'ЧПУ на английском языке',
//            ),
//            'sef_kz' => array(
//                'edit' => true,
//                'head' => false,
//                'type' => 'strings',
//                'label' => 'ЧПУ на казахском языке',
//            ),
        );
    }

    public function rules()
    {
        return array(
            'title_ru' => array(
                array('not_empty'),
            ),
            'description_ru' => array(
                array('not_empty'),
            ),
            'protein' => array(
                array(array($this, 'check_mass'))
            ),
            'carbohydrate' => array(
                array(array($this, 'check_mass'))
            ),
            'fat' => array(
                array(array($this, 'check_mass'))
            ),
            'calories' => array(
                array(array($this, 'check_calories'))
            ),
        );
    }

    public function check_mass()
    {
        $check_sum = $this->protein + $this->carbohydrate + $this->fat;
        return $check_sum <= 100;
    }

    public function check_calories($value)
    {
        return $value <= 900;
    }

    public function get_field($field)
    {
        if ($field == 'title') {
            return Text::limit_chars(strip_tags($this->title), 100, " ...");
        }
        if ($field == 'category_id') {
            return $this->category->title_ru;
        }
        if ($field == 'is_published') {
            return $this->is_published ? 'Опубликован' : 'Не опубликован';
        }
        return parent::get_field($field);
    }


    public function filters()
    {
        return array(
            'title_ru' => array(
                array('strip_tags')
            ),
            'title_en' => array(
                array('strip_tags')
            ),
            'title_kz' => array(
                array('strip_tags')
            ),
            'text_ru' => array(
                array('ORM::clear_html')
            ),
            'text_en' => array(
                array('ORM::clear_html')
            ),
            'text_kz' => array(
                array('ORM::clear_html')
            ),
            'protein' => array(
                array(array($this, 'caloric_content'), array(':value', ':model', 'protein')),
            ),
            'carbohydrate' => array(
                array(array($this, 'caloric_content'), array(':value', ':model', 'carbohydrate')),
            ),
            'fat' => array(
                array(array($this, 'caloric_content'), array(':value', ':model', 'fat')),
            ),
            'calories' => array(
                array(array($this, 'caloric_content'), array(':value', ':model', 'calories')),
            ),

//            'sef' => array(
//                array('ORM::filter_sef', array(':value', ':model', 'title_ru')),
//            ),
//            'sef_ru' => array(
//                array('ORM::filter_sef', array(':value', ':model', 'title_ru')),
//            ),
//            'sef_en' => array(
//                array('ORM::filter_sef', array(':value', ':model', 'title_en')),
//            ),
//            'sef_kz' => array(
//                array('ORM::filter_sef', array(':value', ':model', 'title_kz')),
//            ),
        );
    }

    public function caloric_content($value, $model, $field)
    {
        if ($value == '' || $value == 0 || !$value) {
            $data = array();
            $weight = array();
            foreach ($model->ingredients->find_all() as $row) {
                $weight[$row->id] = $row->dish_ingredients->where('dish_id', '=', $this->id)->find()->weight;
                $data[] = $row->$field * $weight[$row->id];
            }
            if ($data && $weight) {
                $value = array_sum($data) / array_sum($weight);
            }
        }
        if ($value) {
            return $value;
        } else {
            return false;
        }

    }

    public function as_array_ext($fields = array(), $action = 'index'){
        $data = parent::as_array_ext($fields);
        $data['photo'] = $this->photo_s->url('http');
        $data['description_text'] = $data['description'];
        unset($data['description']);
        $data['category'] = $this->category->{'title_'.I18n::$lang};
        return $data;
    }

    public function view_url($uri = false, $lang = null)
    {
        return 'confederation/privatecoach/diet/' . $this->id;
    }

    public function actions($user)
    {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
            array(
                'title' => 'Просмотр',
                'uri' => $this->view_url()
            ),
        );
        return $menu;
    }

} 