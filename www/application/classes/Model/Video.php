<?php
/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 27.06.13
 * Time: 11:09
 * To change this template use File | Settings | File Templates.
 */

class Model_Video extends Model_Photovideo implements ISearchable, Synchronize_ISynch {
    protected $_table_name="photovideos";

    protected $_route="video";

//    protected $_object_name="photovideo";
//    protected $_object_plural="photovideos";


    public function rules()
    {
        return array(
            'title_ru' => array(
                array('not_empty')
            ),
            'url_ru' => array(
                array('not_empty'),
                array(array($this, 'unique'), array('url_ru', ':value'))
            ),
            'date'=>array(
                array('not_empty'),
                array('date')
            )
//            'description_ru' => array(
//                array('not_empty')
//            ),
        );
    }

    public function filters()
    {
        return parent::filters() + array(
            'date' => array(
                array('Date::formatted_time'),
            ),
            'sef'=>array(
                array('ORM::filter_sef',array(':value',':model','title_ru')),
            ),
            'sef_ru'=>array(
                array('ORM::filter_sef',array(':value',':model','title_ru')),
            ),
            'sef_en'=>array(
                array('ORM::filter_sef',array(':value',':model','title_en')),
            ),
            'sef_kz'=>array(
                array('ORM::filter_sef',array(':value',':model','title_kz')),
            ),

        );
    }


    protected function get_fields_description()
    {

        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'fulltext'=>true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'description' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'fulltext'=>true,
                'type' => 'text',
                'label' => 'Описание',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'date'=>array(
                'edit'=>true,
                'type'=>'datetime',
                'label'=>'Дата',
            ),
            'url' => array(
                'edit' => true,
                'type' => 'strings',
                'label' => 'URL видео',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),

            'federations' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select_extended',
                'label' => 'Федерация',
                'params' => array(
                    'options' => ORM::factory('Federation')->select_options('id', 'title_ru'),
                    'value' => $this->federations->select_options('id', 'title_ru')
                )
            ),
            'sport_id' => array(
                'edit' => true,
                'type' => 'select',
                'label' => 'Вид спорта',
                'params' => array(
                    'options'=>ORM::factory('Category')->get_sef_sports()->select_options('id', 'title_ru'),
                ),
            ),
            'sef'=>array(
                'edit'=>true,
                'head'=>false,
                'type'=>'strings',
                'label'=>'ЧПУ',
            ),
            'sef_ru'=>array(
                'edit'=>true,
                'head'=>false,
                'type'=>'strings',
                'label'=>'ЧПУ на русском языке',
            ),
            'sef_en'=>array(
                'edit'=>true,
                'head'=>false,
                'type'=>'strings',
                'label'=>'ЧПУ на английском языке',
            ),
            'sef_kz'=>array(
                'edit'=>true,
                'head'=>false,
                'type'=>'strings',
                'label'=>'ЧПУ на казахском языке',
            ),
        );
    }
}