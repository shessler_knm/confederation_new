<?php

class Model_Photovideo extends ORM implements Synchronize_ISynch
{
    protected $_has_many = array(
        'photos' => array(
            'model' => 'Photo',
            'foreign_key' => 'album_id',
        ),
        'federations' => array(
            'model' => 'Federation',
            'through' => 'federations_photovideos',
            'foreign_key'=>'photovideo_id'
        ),
        'comments' => array(
            'model' => 'comment',
            'foreign_key' => 'target_id'
        ),
    );
    protected $_belongs_to = array(
        'sport'=> array(
            'model'=> 'Category',
            'foreign_key' => 'sport_id'
        ),
    );






    public function get_field($field)
    {
        if ($field == 'title') {
            return Text::limit_chars(strip_tags($this->title), 100, " ...");
        }

        return parent::get_field($field);
    }

    public function filters()
    {
        return array(
            'title_ru' => array(
                array('strip_tags', array(':value', '<br>'))
            ),
            'title_en' => array(
                array('strip_tags', array(':value', '<br>'))
            ),
            'title_kz' => array(
                array('strip_tags', array(':value', '<br>'))
            ),
            'descrition_ru' => array(
                array('ORM::clear_html')
            ),
            'descrition_en' => array(
                array('ORM::clear_html')
            ),
            'descrition_kz' => array(
                array('ORM::clear_html')
            ),
            'url_ru' => array(
                array(array($this, 'parse_code_youtube'), array(':value','ru')),
            ),
            'url_en' => array(
                array(array($this, 'parse_code_youtube'), array(':value','en')),
            ),
            'url_kz' => array(
                array(array($this, 'parse_code_youtube'), array(':value','kz')),
            ),
//            'sef' => array(
//                array('ORM::filter_sef', array(':value', ':model', 'title_ru')),
//            )
        );
    }

    public function parse_code_youtube($value,$lang = 'kz'){
        if (stripos($value, 'youtube.com/embed') !== false) {
            preg_match('#embed\/([^\&]+)#is', $value, $id);
            if (count ($id) > 0) {
                $this->{'content_'.$lang} =   $id[1];
                return $value;
            }
        }
        if (stripos($value, 'youtube.com') !== false) {
            preg_match('#v=([^\&]+)#is', $value, $id);
            if (count ($id) > 0) {
                $this->{'content_'.$lang} =   $id[1];
                return $value;
            }
        }
        if (stripos($value, 'youtu.be') !== false) {
            preg_match('#\youtu.be/([^\&]+)#is', $value, $id);
            if (count ($id) > 0) {
                $this->{'content_'.$lang} =   $id[1];
                return $value;
            }
        }

        return '';
    }

    public function view_url($uri = false, $lang = null, $is_federation = false)
    {
        $_uri = '';
        if(Register::instance()->search){
            if ($is_federation){
                $_uri = '/gallery/';
            }else{
                $_uri = '/infocenter/gallery/';
            }
        }elseif($this->loaded()){
            $fed = Register::instance()->federation;
            $lang_route = I18n::$lang!='kz'?I18n::$lang.'/':'/';
            $_uri = $fed && $fed->loaded()?$lang_route.$fed->sef.'/gallery/'.$this->{'sef_'.I18n::$lang}:$lang_route.'confederation/infocenter/gallery/'.$this->{'sef_'.I18n::$lang};
        }
        return $uri ? $_uri : URL::site($_uri);
    }

    public function map_url($lang = null)
    {
        $_uri = '/gallery/' . $this->{'sef_' . $lang};
        return $_uri;
    }

    public function album_url($uri = false)
    {
        $_uri = URL::site('admin/album/list');
        return $uri ? $_uri : URL::site($_uri);
    }


    public function actions($user)
    {
        $submenu = array();
        foreach ($this->federations->find_all() as $fed) {
            $submenu[] = array(
                'title' => $fed->title,
                'uri' => URL::site($fed->sef . '/infocenter/gallery/' . $this->{'sef_'.I18n::$lang}),
            );
        }
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
            array(
                'title' => 'Просмотр',
                'type' => 'submenu',
                'submenu' => $submenu
            ),
        );
        return $menu;
    }

    public function search_extended_index()
    {
        $ext = array();
        foreach ($this->federations->find_all()->as_array('id','sef') as $id => $sef) {
            $ext[] = Search::escape_extend('fed_id',$sef);
        }
        $ext[] = Search::escape_extend('fed_id','confederation');
        return implode('\n', $ext);
    }

    public function short_text() {
        $arg = func_get_args();
        if(!count($arg)){
            return Text::limit_chars(strip_tags($this->description));
        }else{
            return Text::limit_chars(strip_tags($this->{'description_'.$arg[0]}));
        }
    }

}