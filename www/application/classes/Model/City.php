<?php
class Model_City extends ORM implements Synchronize_ISynch
{
    protected $_table_name = 'cities';
    protected $_belongs_to=array(
        'country' => array(
            'model' => 'Category',
            'foreign_key' => 'country_id'
        ),
    );
    protected $_route='city';
    protected function get_fields_description(){
        return array(
            'id'=>array(
                'head'=>true,
                'label'=>'#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'strings',
                'label' => 'Город',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'country_id' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'select',
                'label' => 'Страна',
                'params' => array(
                    'options'=>ORM::factory('Category')->get_sef_countries()->select_options('id','title_ru')
                ),
            ),
            'lat'=>array(
                'edit'=>true,
                'type'=>'hidden',
                'label'=>'Широта',
                'params'=>array(
                    'widget'=>'MapPoint'
                )
            ),
            'lng'=>array(
                'edit'=>true,
                'type'=>'hidden',
                'label'=>'Долгота',
                'params'=>array(
                    'widget'=>'MapPoint'
                )
            ),
            'sef'=>array(
                'edit'=>true,
                'head'=>false,
                'type'=>'strings',
                'label'=>'ЧПУ',
            ),
            'synch_site' => array(
                'edit' => false,
                'head' => true,
                'type' => 'strings',
                'label' => 'Источник данных'
            ),
        );
    }

    public function rules()
    {
        return array(
            'title_ru' => array(
                array('not_empty'),
            ),
//            'country' => array(
//                array('not_empty'),
//            ),

        );
    }

    public function get_field($field)
    {
        if ($field=='country_id'){
            return ORM::factory('Category',array('id'=>$this->country_id))->title_ru;
        }
        return parent::get_field($field);
    }


    public function filters()
    {
        return array(
            'title_ru'=>array(
                array('strip_tags')
            ),
            'title_kz'=>array(
                array('strip_tags')
            ),
            'title_en'=>array(
                array('strip_tags')
            ),
            'country'=>array(
                array('strip_tags')
            ),
            'sef'=>array(
                array('ORM::filter_sef',array(':value',':model','title_ru')),
            )
        );
   }

    public function actions($user) {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
        );
        return $menu;
    }

    public function form_list_button(Component $cp) {
        $cp->button_link(URL::site('admin/city/synchronize_model'), "Синхронизировать с Федерациями", array('class' => 'btn btn-primary'));
    }

}