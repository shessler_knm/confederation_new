<?php
class Model_Mission_Log extends ORM
{

    protected $_route = 'mission_log';
    protected $_belongs_to=array(
        'types' => array(
            'model' => 'Mission',
            'foreign_key' => 'type'
        ),
        'user' => array(
            'model' => 'User',
            'foreign_key' => 'user_id'
        ),
    );
    public function filters()
    {
        return array(
            'fat' => array(
                array('strip_tags')
            ),
            'weight' => array(
                array('strip_tags')
            ),
        );
    }

}
?>
