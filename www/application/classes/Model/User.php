<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_User extends Model_Auth_User
{

    protected $_route = 'user';
    protected $_has_many = array(
        'roles' => array(
            'model' => 'Role',
            'through' => 'roles_users',
            'foreign_key' => 'user_id'
        ),
        'tokens' => array(
            'model' => 'User_Token',

        ),
        'awards' => array(
            'model' => 'User_Award',
            'foreign_key' => 'user_id'
        ),
        'subscribers' => array(
            'foreign_key' => 'user_id',
            'model' => 'User',
            'far_key' => 'target_id',
            'through' => 'subscribes'
        ),
        'subscribes' => array(
            'foreign_key' => 'user_id',
            'model' => 'Subscribe',
//            'far_key' => 'target_id',
//            'through' => 'subscribes'
        ),
        'subscribes_second' => array(
            'foreign_key' => 'user_id',
            'model' => 'Subscribe',
            'far_key' => 'target_id',
            'through' => 'subscribes'
        ),
        'following' => array(
            'foreign_key' => 'target_id',
            'model' => 'User',
            'far_key' => 'user_id',
            'through' => 'subscribes'
        ),
        'followers' => array(
            'foreign_key' => 'target_id',
            'model' => 'Subscribe',
        ),
        'logs' => array(
            'model' => 'User_Log',
            'foreign_key' => 'user_id'
        ),
        'programs' => array(
            'model' => 'Program',
            'foreign_key' => 'user_id'
        ),
        'mission_logs' => array(
            'model' => 'Mission_Log',
            'foreign_key' => 'user_id'
        ),
        'feed_rules' => array(
            'model' => 'Feed_Rule',
            'foreign_key' => 'user_id'
        ),
        'albums' => array(
            'model' => 'User_Album',
            'foreign_key' => 'user_id'
        ),
        'devices' => array(
            'model' => 'Device',
            'foreign_key' => 'user_id'
        ),
        'rations' => array(
            'model' => 'Ration',
            'foreign_key' => 'user_id'
        ),
        'norms' => array(
            'model' => 'User_Norm',
            'foreign_key' => 'user_id'
        ),
        'favourites' => array(
            'foreign_key' => 'user_id',
            'model' => 'Favourite'
        ),
    );
    protected $_belongs_to = array(
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
        'mission_type' => array(
            'model' => 'Mission',
            'foreign_key' => 'mission_id'
        ),
        'top' => array(
            'model' => 'Top',
            'foreign_key' => 'user_id'
        ),
    );
    //экшены, которые возвращают дополнительные данные пользователя
    protected $_full_view = array('login', 'profile', 'data');

    // This class can be replaced or extended


    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'email' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Email',
            ),
            'username' => array(
                'edit' => true,
                'search' => true,
                //  'head'=>true,
                'type' => 'strings',
                'label' => 'Логин',
            ),
            'firstname' => array(
                'edit' => true,
                'search' => true,
                'head' => true,
                'type' => 'strings',
                'label' => 'Имя',
            ),
            'lastname' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Фамилия',
            ),
            'annotation' => array(
                'edit' => false,
                'head' => false,
                'search' => true,
                'type' => 'strings',
                'label' => 'Аннотация',
            ),
            'password' => array(
                'edit' => true,
                'type' => 'password',
                'label' => 'Пароль',
            ),
            'logins' => array(
                'type' => 'strings',
                'label' => 'logins',
            ),
            'last_login' => array(
                'head' => true,
                'type' => 'strings',
                'label' => 'Последний вход',
            ),
            'a_hash' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'Активация',
            ),
            'roles' => array(
                'head' => false,
                'edit' => true,
                'type' => 'multi_select',
                'label' => 'Роли',
                'params' => array(
                    'options' => ORM::factory('Role')->find_all()->as_array('id', 'description'),
                    'value' => $this->roles->find_all()->as_array('id', 'id'),
                )
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Изображение',
                'params' => array(
                    'need_help' => true,
                ),
            ),
            'sex' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'select',
                'label' => 'Пол',
                'params' => array(
                    'options' => array(
                        1 => 'мужской',
                        2 => 'женский'
                    )
                )
            ),
            'training' => array(
                'edit' => false,
                'head' => false,
                'search' => true,
                'type' => 'strings',
                'label' => 'Тренированность',
            ),
            'height' => array(
                'edit' => false,
                'head' => false,
                'type' => 'strings',
                'label' => 'Рост',
            ),
            'birthdate' => array(
                'edit' => true,
                'type' => 'date',
                'label' => 'Дата рождения',
            )
        );
    }


    public function get_field($field)
    {
        if ($field == 'sex') {
            return $this->sex == 1 ? 'Мужской' : 'Женский';
        }
        if ($field == 'last_login') {
            return $this->last_login == null ? 'Пользователь ни разу не входил' : date('Y-m-d', $this->last_login);
        }
        return parent::get_field($field);
    }

    public function as_array_ext($fields = array(), $action = null)
    {
        $data = parent::as_array_ext($fields);
        $current_user = Session::instance()->get('auth_user');
        if ($this->photo_s) {
            $data['photo'] = $this->photo ? $this->photo_s->url(true) : null;
        } else {
            $data['photo'] = null;
        }
        if ($current_user && $action == 'followers') {
            switch ($current_user->followers->where('user_id', '=', $this->id)->find()->status) {
                case '0':
                    $status = 'PENDING';
                    break;
                case '1':
                    $status = 'SIGNED';
                    break;
                default:
                    $status = 'NOT_SIGNED';
            }
            $data['subscribed'] = $status;
        }
        if ($current_user && $action == 'info') {
            switch ($current_user->subscribes->where('target_id', '=', $this->id)->find()->status) {
                case '0':
                    $status = 'PENDING';
                    break;
                case '1':
                    $status = 'SIGNED';
                    break;
                default:
                    $status = 'NOT_SIGNED';
            }
            $data['subscribed'] = $status;
        }
        if (array_search($action, $this->_full_view) !== false) {
            $mission_log = $this->mission_logs->order_by('id', 'DESC')->find();
            $user_log = $this->logs->order_by('id', 'DESC')->find();
            $target = $mission_log->type;
            $weight = $this->weight;
            $fat = $this->fat;
//            $available_targets = array('MUSCLE' => 'MUSCLE', 'SLIMMING' => 'SLIMMING');
            $place = $this->top->where('user_id', '=', $this->id)->find()->place;
//            $training = array('beginner', 'medium', 'advanced', 'professional', 'BEGINNER', 'MEDIUM', 'ADVANCED', 'PROFESSIONAL');
//            if (array_search($this->training, $training)) {
//                $data['training'] = $this->training;
//            } else {
//                $data['training'] = null;
//            }
            switch ($this->sex) {
                case 1:
                    $data['sex'] = 'M';
                    break;
                case 2:
                    $data['sex'] = 'F';
                    break;
                default:
                    $data['sex'] = null;
            }

            if ($current_user) {
                switch ($current_user->subscribes->where('target_id', '=', $this->id)->find()->status) {
                    case '0':
                        $status = 'PENDING';
                        break;
                    case '1':
                        $status = 'SIGNED';
                        break;
                    default:
                        $status = 'NOT_SIGNED';
                }
                $data['subscribed'] = $status;
            }
            $data['weight'] = $weight;
            $data['fat'] = $fat;
            $data['target'] = $target;
            $data['place'] = $place;

            if ($action == 'profile' || $action == 'login') {
                $log = $this->mission_logs->order_by('id', 'DESC')->find();
                $data['target_weight'] = $log->weight;
                $data['target_fat'] = $log->fat;
                //настройки профиля
                $settings = array(
                    'push_messages',
                    'email_messages',
                    'social_network',
                    'run_publication',
                    'training_publication',
                    'dish_publication',
                    'constant_publication',
                    'working_weight_request'
                );
                $data['settings'] = array();
                foreach ($settings as $setting) {
                    $data['settings'][$setting] = (bool)$this->$setting;
                }
            }
            /*
             * @var $program
             */
            $program = $this->programs->where('activity', '=', 1)->find();
            if ($program->loaded()) {
                $week_days = array();
                $exercises = $program->program_exercises->order_by('week_day', 'ASC')->find_all()->as_array();
                foreach ($exercises as $row) {
                    $week_days[$row->week_day] = $row->week_day;
                    $program_exercises[$row->week_day][] = $row;
                }
                $data['program']['id'] = $program->id;
                $data['program']['name'] = $program->title;
                $data['program']['parent_id'] = $program->parent_id;
                if ($week_days) {
                    $data['program']['subscribe_date'] = $program->start_date;
                    $data['program']['start_date'] = $program->start_date($week_days);
                    $data['program']['end_date'] = $program->end_date($week_days);
                } else {
                    $data['program']['subscribe_date'] = $program->start_date;
                    $data['program']['start_date'] = $program->start_date;
                    $data['program']['end_date'] = $program->count_date($program->duration, $program->start_date);
                }
            } else {
                $data['program'] = null;
            }
        }
        return $data;

    }

    public function get_roles()
    {
        $roles = array();
        foreach ($this->roles->find_all() as $role) {
            if (in_array($role->name, array(''))) {
                $roles[$role->name] = __(Utf8::strtolower($role->description));
            }
        }
        if (count($roles) > 1) {
            if (isset($roles['login'])) {
                unset($roles['login']);
            };
        }
        return $roles;

    }

    public function rules()
    {
        return array(
            'password' => array(
                array('not_empty'),
                array('min_length', array(':value', 8)),
            ),
            'email' => array(
                array('not_empty'),
                array('email'),
                array(array($this, 'unique'), array('email', ':value')),
            ),
        );
    }

    public function filters()
    {
        return array(
            'username' => array(
                array('strip_tags')
            ),
            'firstname' => array(
                array('strip_tags')
            ),
            'lastname' => array(
                array('strip_tags')
            ),
            'middlename' => array(
                array('strip_tags')
            ),
            'birthdate' => array(
                array('Date::formatted_time'),
                array('strip_tags')

            ),
            'email' => array(
                array('strip_tags')
            ),
            'weight' => array(
                array('strip_tags')
            ),
            'fat' => array(
                array('strip_tags')
            ),
            'annotation' => array(
                array('strip_tags')
            ),
        ) + parent::filters();
    }

//    public function username(){
//        return $this->username;
//    }

    public function get_username()
    {
        return $this->username;
    }

    public function get_full_name()
    {
        if ($this->firstname || $this->lastname) {
            return $this->firstname . " " . $this->lastname;
        } else {
            return $this->username;
        }
    }

    public static function after_reg($username, $hash, $mail)
    {
        $user = ORM::factory('User', array('username' => $username));
        $email = Email::factory('Активация пользователя', "Благодарим за регистрацию. Для подтверждения перейдите по ссылке: " . URL::base('http') . 'user/activation/' . $user->id . '?hash=' . $hash . ' ' . date('d.m.Y H:i:s'));
        $email->from('support@confederation.kz');
        $email->to($mail);
        $email->send();
        return true;
    }

    public function actions($user)
    {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
        );
        return $menu;
    }

    public function get_feed_username()
    {
        return $this->get_full_name();
    }

    public function get_photo()
    {
        return $this->photo_s->html_img(100, 100);
    }

    public function get_value()
    {
        return $this->fat;
    }

    public function get_mission()
    {
        return $this->mission->title;
    }

    public function get_age($birthday = null)
    {
        $birthday = $this->birthdate ? $this->birthdate : $birthday;
        if ($birthday) {
            $y = Date::formatted_time($birthday, 'Y');
            $m = Date::formatted_time($birthday, 'm');
            $d = Date::formatted_time($birthday, 'd');
//        list($y,$m,$d) = explode('/',$birthday);
            $m_age = (date("Y") - $y - ((intval($m . $d) - intval(date("m") . date("d")) > 0) ? 1 : 0));
            $d_var1 = $m_age % 10;
            $d_var2 = $m_age % 100;
            return $m_age;
        }
    }

    public static function _passremind($user_id, $passremind)
    {
        $user = ORM::factory('User', $user_id);
        $email = Email::factory('Восстановление пароля', 'Ваш пароль:' . $passremind);
        $email->from('support@confederation.kz');
        $email->to($user->email);
        $email->send();
        return true;
    }

    public function is_admin()
    {
        $user = $this->roles->find_all()->as_array('id', 'name');
        return array_search('admin', $user);
    }

//    Есть ли загруженный юзер в подписках у юзера, который пришел в метод
    public function is_sub($user)
    {
        $subscribes = $user->subscribers->find_all()->as_array('id', 'id');
        return array_search($this->id, $subscribes);
    }

    public function get_users_count($pers_data)
    {
        if ($pers_data) {
            return ORM::factory('User')->where('pers_data', '=', 1)->find_all()->count();
        } else {
            return ORM::factory('User')->count_all();
        }
    }

    public function as_array()
    {
        $object = array();

        foreach ($this->_object as $column => $value) {
            // Call __get for any user processing
            $object[$column] = $this->__get($column);
        }

        foreach ($this->_related as $column => $model) {
            // Include any related objects that are already loaded
            $object[$column] = $model->as_array();
        }
        unset($object['password']);
        return $object;
    }

    public function delivery()
    {
        $this->delivery = ($this->delivery == 1) ? 0 : 1;
        $this->save();
    }


} // End User Model