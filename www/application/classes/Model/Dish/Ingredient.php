<?php

/**
 * Created by PhpStorm.
 * User: id541rak
 * Date: 14.04.14
 * Time: 10:50
 */
class Model_Dish_Ingredient extends ORM_Embeded
{

    protected $_parent_field = "dish_id";
    protected $_parent_model = "Dish";
    protected $_table_name = 'dish_ingredients';
    protected $_route = 'dish_ingredient';

    protected $_belongs_to=array(
        'dish'=>array(),
        'ingredient'=>array(
            'model'=>'Ingredient',
            'foreign_key'=>'ingredient_id'
        ),
    );

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'ingredient_id' => array(
                'head' => true,
                'edit' => true,
                'type' => 'single_typeaheads',
                'label' => 'Ингредиент',
                'params' => array(
                    'options' => Url::site('privatecoach/ingredient/source'),
                    'value' => $this->title(),
                )
            ),
            'weight' => array(
                'head' => true,
                'edit' => true,
                'type' => 'strings',
                'label' => 'Масса, г',
            ),
        );
    }

    public function rules()
    {
        return array(
            'ingredient_id' => array(
                array('not_empty'),
            ),
            'weight' => array(
                array('not_empty'),
            ),
        );
    }

    public function title()
    {
        if ($this->loaded()) {
            return array($this->ingredient->title => $this->ingredient_id);
        } else {
            return array();
        }
    }

    public function get_field($field)
    {
        if ($field == 'ingredient_id') {
            return ORM::factory('Ingredient', $this->ingredient_id)->title;
        }
        return parent::get_field($field);
    }

    public function as_array_ext($fields = array(), $action = 'index'){
        $data = parent::as_array_ext($fields);
        $data['title'] = $data['ingredient_id'];
        unset($data['ingredient_id']);
        ksort($data);
        return $data;
    }

//    public function filters()
//    {
//        return array(
//            'title_ru' => array(
//                array('strip_tags')
//            ),
//            'title_en' => array(
//                array('strip_tags')
//            ),
//            'title_kz' => array(
//                array('strip_tags')
//            ),
//            'text_ru' => array(
//                array('ORM::clear_html')
//            ),
//            'text_en' => array(
//                array('ORM::clear_html')
//            ),
//            'text_kz' => array(
//                array('ORM::clear_html')
//            ),
//            'sef' => array(
//                array('ORM::filter_sef', array(':value', ':model', 'title_ru')),
//            ),
//            'sef_ru' => array(
//                array('ORM::filter_sef', array(':value', ':model', 'title_ru')),
//            ),
//            'sef_en' => array(
//                array('ORM::filter_sef', array(':value', ':model', 'title_en')),
//            ),
//            'sef_kz' => array(
//                array('ORM::filter_sef', array(':value', ':model', 'title_kz')),
//            ),
//        );
//    }

} 