<?php

class Model_Dish_Ration extends ORM
{
    protected $_table_name = 'dish_rations';

    protected $_belongs_to=array(
        'dish'=>array(),
        'ration'=>array(),
    );
}