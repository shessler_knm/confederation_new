<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Almas
 * Date: 26.07.13
 * Time: 11:12
 * To change this template use File | Settings | File Templates.
 */

class Model_Progress extends ORM
{
    protected $_route = 'progress';
    protected $_table_name = 'progresses';
    protected $_belongs_to = array(
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
        'user' => array(
            'model' => 'User',
            'foreign_key' => 'user_id'
        ),

    );
}