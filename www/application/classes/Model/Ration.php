<?php
class Model_Ration extends ORM
{

    protected $_route = 'ration';

    protected $_has_many = array(
        'dish_rations' => array(
            'Model' => 'Dish_ration',
        ),
        'dishes'=>array(
            'through'=>'dish_rations',
            'foreign_key'=>'ration_id'
        )
    );

}