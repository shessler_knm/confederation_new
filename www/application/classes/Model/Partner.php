<?php
class Model_Partner extends ORM
{

    protected $_route = 'partner';
    protected $_belongs_to = array(
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
        'category'  =>  array(
            'model' =>  'Category',
            'foreign_key'   =>  'category_id'
        )
    );

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'text' => array(
                'edit' => true,
                'head' => false,
                'type' => 'text',
                'label' => 'Текст',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'site' => array(
                'edit' => true,
                'head' => true,
                'type' => 'strings',
                'label' => 'Сайт',
            ),
            'category_id' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Категория',
                'params' => array(
                    'options' => ORM::factory('Category')->where('parent_id', '=', 321)->select_options('id', 'title_ru'),
                    //'value' => $this->federations->select_options('id', 'title_ru')
                )
            ),
            'priority' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'Приоритет',
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Изображение',
                'params' => array(
                    //'need_help' => true,
                    /*'aspect' => 2,
                    'size' => 200,*/
                ),
            ),
        );
    }

    public function get_field($field)
    {
        if ($field == 'title') {
            return Text::limit_chars(strip_tags($this->title), 100, " ...");
        }
        return parent::get_field($field);
    }

    public function filters()
    {
        return array(
            'title_ru' => array(
                array('strip_tags')
            ),
            'title_en' => array(
                array('strip_tags')
            ),
            'title_kz' => array(
                array('strip_tags')
            ),
        );
    }

    public function map_url($lang = null)
    {
        $_uri = '/' . $this->_route . '/' . $this->sef;
        return $_uri;
    }

    public function actions($user)
    {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
            array(
                'title' => 'Просмотр',
                'uri' => $this->view_url()
            ),
        );
        return $menu;
    }
}