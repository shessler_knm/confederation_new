<?php
class Model_Pulse_Type extends ORM
{

    protected $_route = 'pulse_type';
    protected $_belongs_to=array(
        'user' => array(
            'model' => 'User',
            'foreign_key' => 'user_id'
        ),
    );

    public function as_array_ext($fields=array(), $action = null){
        $langs = array('ru','en','kz');
        $data = array();
        $data['id'] = $this->id;
        if ($this->{'title_'.I18n::$lang}){
            $data['name']=$this->{'title_'.I18n::$lang};
        }else{
            foreach ($langs as $lang){
                if ($this->{'title_'.$lang}){
                    $data['name']=$this->{'title_'.$lang};
                    break;
                }
            }
        }
        return $data;
    }
}
?>
