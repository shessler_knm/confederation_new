<?php

class Model_Comment extends ORM_Embeded
{
    protected $_parent_field = "parent_id";
    protected $_parent_model = "Comment";
    protected $_route = 'comment';
    protected $_has_many = array(
        'children' => array(
            'model' => 'Comment',
            'foreign_key' => 'parent_id'
        ),
    );
    protected $_belongs_to = array(
        'user' => array(
            'model' => 'User',
            'foreign_key' => 'user_id'
        ),
        'parent' => array(
            'model' => 'Comment',
            'foreign_key' => 'parent_id'
        ),
    );

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'table_name' => array(
                'edit' => true,
                'head' => true,
//                'search'=>true,
                'type' => 'strings',
                'label' => 'Заголовок',
//                'params' => array(
//                    'options' => array(1 => 'Опубликовано', 0 => 'На рассмотрении'),
//                    'value' => ($this->status) ? $this->status : null,
//                )
            ),
            'text' => array(
                'edit' => true,
                'head' => true,
//                'search'=>true,
                'type' => 'text',
                'label' => 'Текст',
            ),

            'date' => array(
                'edit' => true,
                'head' => true,
                'search' => false,
                'type' => 'datetime',
                'label' => 'Дата публикации',
            ),
            'status' => array(
                'head' => false,
                'edit' => true,
                'type' => 'select',
                'label' => 'Статус',
                'search' => true,
                'params' => array(
                    'options' => array(1 => 'Опубликовано', 2 => 'На рассмотрении'),
                    'value' => ($this->status) ? $this->status : null,
                )
            ),
            'user_id' => array(
                'edit' => true,
                'head' => true,
                'type' => 'single_typeaheads',
                'label' => 'Пользователь',
                'params' => array(
                    'options' => Url::site('user/source'),
                    'value' => $this->author(),
                )
            ),
            'parent_id' => array(
                'edit' => false,
                'type' => 'strings',
                'label' => 'parent_id',
            ),
            'children' => array(
                'type' => 'embeded',
                'edit' => true,
                'loaded' => true,
                'label' => 'Дочернии комментарии'
            ),

        );
    }

    public function rules()
    {
        return array(
            'text' => array(
                array('not_empty')
            ),
            'date' => array(
                array('not_empty'),
            ),
        );
    }

    static function get_parent(Model_Comment $model = NULL)
    {
        if (is_null($model)) {
            return false;
        }
        if (is_null($model->table_name)) {
            return Model_Comment::get_parent($model->parent);
        } else {
            $r[0] = array('parent_id' => $model->parent_id, 'table_name' => $model->table_name);
            $r[1] = array('parent_id' => $model->id, 'table_name' => 'comment');
            return $r;
        }
    }

    public function parent_field()
    {
        return $this->_parent_field;
    }

    static function get_child_count(Model_Comment $model = NULL)
    {
        if (is_null($model)) {
            return 2;
        } else {
            $cnt = 1;
        }
        foreach ($model->children->find_all() as $children) {
            $cnt += Model_Comment::get_child_count($children);
        }
        return $cnt;
    }

    public static function count_comments($target, $target_id)
    {
        return ORM::factory('Comment')
            ->where('table_name', '=', $target)
            ->where('target_id', '=', $target_id)
            ->where('status', '=', 1)
            ->count_all();
    }

    public function children()
    {
        return ORM::factory('Comment')
            ->where('status', '=', 1)
            ->where('target_id', '=', $this->target_id)
            ->where('parent_id', '=', $this->id)
            ->where('table_name', '=', $this->table_name)
            ->order_by('date');
    }


    public function get_field($field)
    {
        if ($field == 'text') {
            return Text::limit_chars(strip_tags($this->text), 200, " ...");
        }
        if ($field == 'user_id') {
            return ORM::factory('User', $this->user_id)->username;
        }
        if ($field == 'children') {
            $data = array();
            $fields = array('id', 'table_name', 'text', 'date', 'children', 'parent_id');
            $collection = $this->children()->find_all();
            foreach ($collection as $row) {
                $data[] = $row->as_array_ext($fields);
            }
            return $data;
        }
        return parent::get_field($field);
    }

    public function filters()
    {
        return array(
            'text' => array(
                array('ORM::clear_html')
            ),
            'date' => array(
                array('Date::formatted_time'),
            ),

        );
    }

    public function news_title()
    {
        if ($this->loaded() && $this->news_id) {
            return array($this->news->get_title() => "$this->news_id");
        } else {
            return array();
        }
    }

    public function interview_title()
    {
        if ($this->loaded() && $this->interview_id) {
            return array($this->interview->get_title() => "$this->interview_id");
        } else {
            return array();
        }
    }

    public function as_array_ext($fields = array(), $action = 'view')
    {
        $data = parent::as_array_ext($fields);
        $data['user_name'] = $this->user->get_full_name();
        $data['user_id'] = $this->user->id;
        $data['user_photo'] = $this->user->photo ? $this->user->photo_s->url('http') : null;
        if ($this->parent->loaded()) {
            $data['parent']['id'] = $this->parent->id;
            $data['parent']['user_id'] = $this->parent->user_id;
            $data['parent']['user_name'] = $this->parent->user->get_full_name();
        }
        return $data;
    }

    public function author()
    {
        if ($this->loaded() && $this->user_id) {
            return array($this->user->get_username() => "$this->user_id");
        } else {
            return array();
        }
    }

//    public function title()
//    {
//        return $this->author->get_full_name();
//    }
//
//    protected function extended_index()
//    {
//        return Search::escape_extend('author_id', $this->user_id);
//    }
//
//    public function actions($user)
//    {
//        $menu = array(
//            array(
//                'title' => 'Редактировать',
//                'uri' => $this->edit_url()
//            ),
//            array(
//                'title' => 'Удалить',
//                'uri' => $this->delete_url()
//            ),
//        );
//        return $menu;
//    }
}

?>
