<?php
class Model_Page extends ORM implements ISearchable
{

    protected $_route = 'page';
    protected $_belongs_to = array(
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
    );

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'fulltext' => true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'text' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'fulltext' => true,
                'type' => 'text',
                'label' => 'Текст',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Изображение',
                'params' => array(
                    'need_help' => true,
                ),
            ),
            'created' => array(
                'edit' => true,
                'head' => false,
                'search' => false,
                'type' => 'datetime',
                'label' => 'Дата создания',
            ),
            'updated' => array(
                'edit' => true,
                'head' => false,
                'search' => false,
                'type' => 'datetime',
                'label' => 'Дата обновления',
            ),
            'sef' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ',
            ),
            'sef_ru' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на русском языке',
            ),
            'sef_en' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на английском языке',
            ),
            'sef_kz' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на казахском языке',
            ),
            'view_file' => array(
                'edit' => true,
                'type' => 'strings',
                'label' => 'Файл шаблона',
            )
        );
    }

    public function rules()
    {
        return array(
            'title_ru' => array(
                array('not_empty'),
            ),
            'text_ru' => array(
                array('not_empty'),
                array('ORM::alt_image', array('text' => $this->text_ru))
            ),
            'text_kz' => array(
                array('ORM::alt_image', array('text' => $this->text_kz))
            ),
            'text_en' => array(
                array('ORM::alt_image', array('text' => $this->text_en))
            ),
            'created' => array(
                array('not_empty'),
            ),
            'updated' => array(
                array('not_empty')
            ),
        );
    }

    public function get_field($field)
    {
        if ($field == 'title') {
            return Text::limit_chars(strip_tags($this->title), 100, " ...");
        }
        return parent::get_field($field);
    }

    public function filters()
    {
        return array(
            'title_ru' => array(
                array('strip_tags')
            ),
            'title_en' => array(
                array('strip_tags')
            ),
            'title_kz' => array(
                array('strip_tags')
            ),
            'text_ru' => array(
                array('ORM::clear_html')
            ),
            'text_en' => array(
                array('ORM::clear_html')
            ),
            'text_kz' => array(
                array('ORM::clear_html')
            ),
            'updated' => array(
                array('Date::formatted_time'),
            ),
            'created' => array(
                array('Date::formatted_time'),
            ),
            'sef' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title')),
            ),
            'sef_ru' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_ru')),
            ),
            'sef_en' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_en')),
            ),
            'sef_kz' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_kz')),
            ),
        );
    }

    public function view_url($uri = false)
    {
        $_uri = '';
        if ($this->loaded()) {
            $lang_route = I18n::$lang != 'kz' ? I18n::$lang . '/' : '/';
            $_uri = $lang_route . 'confederation/page/' . $this->sef;
        }
        return $uri ? $_uri : URL::site($_uri);
    }

    public function map_url($lang = null)
    {
        $_uri = '/' . $this->_route . '/' . $this->sef;
        return $_uri;
    }

    public function actions($user)
    {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
            array(
                'title' => 'Просмотр',
                'uri' => $this->view_url()
            ),
        );
        return $menu;
    }
}