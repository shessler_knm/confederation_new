<?php
class Model_Structure extends ORM
{
    protected $_belongs_to=array(
        'federation'=>array(),
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
    );
    protected $_route='structure';
    protected function get_fields_description(){
        return array(
            'id'=>array(
                'head'=>true,
                'label'=>'#',
            ),
            'firstname' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'strings',
                'label' => 'Имя',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'middlename' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'strings',
                'label' => 'Отчество',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'lastname' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'strings',
                'label' => 'Фамилия',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'post'=>array(
                'edit'=>true,
                'head'=>true,
                'search'=>true,
                'type'=>'strings',
                'label'=>'Пост',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Изображение',
                'params' => array(
                    'need_help' => true,
                ),
            ),
            'federation_id'=>array(
                'edit'=>true,
                'head'=>true,
                'search'=>true,
                'type'=>'select',
                'label'=>'Федерация',
                'params'=>array(
                    'options'=>ORM::factory('Federation')->select_options('id','title_ru')
                )
            ),
        );
    }

    public function rules()
    {
        return array(
            'firstname_ru' => array(
                array('not_empty'),
            ),
            'lastname_ru' => array(
                array('not_empty'),
            ),
            'post_ru' => array(
                array('not_empty'),
            ),
            'federation_id' => array(
                array('not_empty'),
            ),
        );
    }

    public function get_field($field)
    {
        if ($field == 'federation_id'){
            return ORM::factory('Federation',$this->federation_id)->title_ru;
        }
        return parent::get_field($field);
    }
    public function filters()
    {
        return array(
            'firstname_ru'=>array(
                array('strip_tags')
            ),
            'firstname_en'=>array(
                array('strip_tags')
            ),
            'firstname_kz'=>array(
                array('strip_tags')
            ),
            'lastname_ru'=>array(
                array('strip_tags')
            ),
            'lastname_en'=>array(
                array('strip_tags')
            ),
            'lastname_kz'=>array(
                array('strip_tags')
            ),
            'middlename_ru'=>array(
                array('strip_tags')
            ),
            'middlename_en'=>array(
                array('strip_tags')
            ),
            'middlename_kz'=>array(
                array('strip_tags')
            ),
            'post_ru'=>array(
                array('strip_tags')
            ),
            'post_en'=>array(
                array('strip_tags')
            ),
            'post_kz'=>array(
                array('strip_tags')
            ),
        );
    }
}
?>
