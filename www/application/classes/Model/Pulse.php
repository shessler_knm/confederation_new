<?php
/**
 * Created by PhpStorm.
 * User: id541rak
 * Date: 23.07.14
 * Time: 10:51
 */

class Model_Pulse extends ORM{

    protected $_belongs_to = array(
        'type' => array(
            'model' => 'Pulse_Type',
            'foreign_key' => 'type_id',
        ),
        'user' => array(
            'model' => 'User',
            'foreign_key' => 'user_id'
        ),
    );

    public function as_array_ext($fields=array(),$action=null){
        $data = parent::as_array_ext($fields,$action);
        $data['category_name'] = ORM::factory('Pulse_Type',$this->type_id)->title();
        $data['value'] = (int)$this->value;
        $data['category_id'] = $this->type_id;
        unset($data['type_id']);
        return $data;
    }

} 