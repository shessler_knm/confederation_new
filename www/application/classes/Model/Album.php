<?php
/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 27.06.13
 * Time: 11:08
 * To change this template use File | Settings | File Templates.
 */

class Model_Album extends Model_Photovideo implements Synchronize_ISynch {
    protected $_table_name='photovideos';

    protected $_route="album";

    public function rules()
    {
        return array(
            'title_ru'=>array(
                array('not_empty')
            ),
            'date'=>array(
                array('not_empty'),
                array('date')
            ),
            'author'=>array(
                array('not_empty')
            ),
        );
    }
    public function filters()
    {
        return array(
            'title_ru'=>array(
                array('strip_tags')
            ),
            'title_en'=>array(
                array('strip_tags')
            ),
            'title_kz'=>array(
                array('strip_tags')
            ),
            'description_ru'=>array(
                array('ORM::clear_html')
            ),
            'description_en'=>array(
                array('ORM::clear_html')
            ),
            'description_kz'=>array(
                array('ORM::clear_html')
            ),
            'author'=>array(
                array('strip_tags'),
            ),
            'date' => array(
                array('Date::formatted_time'),
            ),
            'sef'=>array(
                array('ORM::filter_sef',array(':value',':model','title_ru')),
            ),
            'sef_ru'=>array(
                array('ORM::filter_sef',array(':value',':model','title_ru')),
            ),
            'sef_en'=>array(
                array('ORM::filter_sef',array(':value',':model','title_en')),
            ),
            'sef_kz'=>array(
                array('ORM::filter_sef',array(':value',':model','title_kz')),
            ),
        );
    }

    public function edit_url($uri = false)
    {
        if ($this->loaded()){
            return parent::edit_url($uri);
        }
        return false;
    }

    public function form_list_button(Component $cp)
    {
        $cp->button_link(URL::site('admin/photovideo/synchfl'), "Синхронизировать с Flickr", array('class' => 'btn btn-primary'));
    }

    public function get_field($field){
        if ($field=='federations') {
            $results = array();
            $results=$this->federations->select_options('id','title_ru');
            $results = implode(';<br>', $results);
            return $results;
        }
        return parent::get_field($field);
    }

    protected function get_fields_description()
    {


        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'description' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'type' => 'text',
                'label' => 'Описание',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'date'=>array(
                'edit'=>true,
                'type'=>'datetime',
                'label'=>'Дата',
            ),
            'federations' => array(
                'edit' => true,
                'head' => true,
                'type' => 'select_extended',
                'label' => 'Федерация',
                'params' => array(
                    'options' => ORM::factory('Federation')->select_options('id', 'title_ru'),
                    'value' => $this->federations->select_options('id', 'title_ru')
                )
            ),
            'photos' => array(
                'edit' => true,
                'type' => 'embeded',
                'label' => 'Фотографии',
            ),
            'sport_id' => array(
                'edit' => true,
                'type' => 'select',
                'label' => 'Вид спорта',
                'params' => array(
                    'options'=>ORM::factory('Category')->get_sef_sports()->select_options('id', 'title_ru'),
                ),
            ),
            'author' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'strings',
                'label' => 'Автор фото',
            ),
            'sef'=>array(
                'edit'=>true,
                'head'=>false,
                'type'=>'strings',
                'label'=>'ЧПУ',
            ),
            'sef_ru'=>array(
                'edit'=>true,
                'head'=>false,
                'type'=>'strings',
                'label'=>'ЧПУ на русском языке',
            ),
            'sef_en'=>array(
                'edit'=>true,
                'head'=>false,
                'type'=>'strings',
                'label'=>'ЧПУ на английском языке',
            ),
            'sef_kz'=>array(
                'edit'=>true,
                'head'=>false,
                'type'=>'strings',
                'label'=>'ЧПУ на казахском языке',
            ),
        );
    }

    public function view_url($uri = false, $lang = null)
    {
        if (Register::instance()->search) {
            if ($this->{'sef_'.$lang})
                $_uri = '/gallery/' . $this->{'sef_' . $lang};
        }
        return $uri ? $_uri : URL::site($_uri);
    }

    public function actions($user){
        $submenu = array();
        $submenu[]=array(
            'title'=>  'Конфедерация',
            'uri'=>URL::site('confederation/infocenter/gallery/view/'.$this->id)
        );
        foreach ($this->federations->find_all() as $fed){
            $submenu[] = array(
                'title'=>  $fed->title,
                'uri'=>URL::site($fed->sef.'/gallery/view/'.$this->id)
            );
        }

        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
            array(
                'title' => 'Просмотр',
                'type'=>'submenu',
                'submenu' => $submenu
            ),
        );
        return $menu;
    }
}