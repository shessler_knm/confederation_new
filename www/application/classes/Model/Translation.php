<?php

class Model_Translation extends ORM
{
    protected $_route = 'translation';

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'fulltext' => true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'url' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'URL',
            ),
            'created_at' => array(
                'edit' => true,
                'head' => false,
                'search' => false,
                'type' => 'datetime',
                'label' => 'Дата публикации',
            ),
            'is_published' => array(
                'edit' => true,
                'head' => true,
                'type' => 'select',
                'label' => 'Отображать на сайте',
                'params' => array(
                    'options' => array(
                        0 => 'Не опубликовать',
                        1 => 'Опубликовать'
                    )
                )
            ),
//
        );
    }

    public function rules()
    {
        return array(
            'title_ru' => array(
                array('not_empty'),
            ),
            'created_at' => array(
                array('not_empty'),
                array('date')
            ),
            'url' => array(
                array('not_empty'),
                array(array($this, 'unique'), array('url', ':value'))
            ),
        );
    }

    public function get_field($field)
    {
        if ($field == 'title') {
            return Text::limit_chars(strip_tags($this->title), 100, " ...");
        }
        if ($field == 'is_published') {
            $result = $this->is_published == 1 ? 'Опубликовано' : 'Сохранено';
            return $result;
        }
        return parent::get_field($field);
    }


    public function filters()
    {
        return array(
            'title_ru' => array(
                array('strip_tags')
            ),
            'title_en' => array(
                array('strip_tags')
            ),
            'title_kz' => array(
                array('strip_tags')
            ),
            'created_at' => array(
                array('Date::formatted_time'),
            ),
            'url' => array(
                array(array($this, 'parse_code_youtube'), array(':value', 'ru')),
            ),
        );
    }


    public function get_title()
    {
        return $this->title;
    }

    public function actions($user)
    {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
        );
        return $menu;
    }

    public function parse_code_youtube($value)
    {
        if (stripos($value, 'youtube.com/embed') !== false) {
            preg_match('#embed\/([^\&]+)#is', $value, $id);
            if (count($id) > 0) {
                $this->content = $id[1];
                return $value;
            }
        }
        if (stripos($value, 'youtube.com') !== false) {
            preg_match('#v=([^\&]+)#is', $value, $id);
            if (count($id) > 0) {
                $this->content = $id[1];
                return $value;
            }
        }
        if (stripos($value, 'youtu.be') !== false) {
            preg_match('#\youtu.be/([^\&]+)#is', $value, $id);
            if (count($id) > 0) {
                $this->content = $id[1];
                return $value;
            }
        }

        return '';
    }
}