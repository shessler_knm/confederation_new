<?php

class Model_News extends ORM implements ISearchable, Synchronize_ISynch
{
    protected $_table_name = 'news';
    protected $_route = 'news';
    protected $_has_many = array(
        'federations' => array(
            'model' => 'Federation',
            'through' => 'federations_news',
        ),
        'categories' => array(
            'model' => 'Category',
            'through' => 'news_categories',
        ),
        'sport' => array(
            'model' => 'Category',
            'through' => 'news_sports',
            'far_key' => 'category_id'
        ),
        'comments' => array(
            'model' => 'comment',
            'foreign_key' => 'target_id'
        ),
    );
    protected $_belongs_to = array(
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
        'photo_slider_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo_slider'
        ),
    );


    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'fulltext' => true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'text' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'type' => 'text',
                'label' => 'Текст',
                'fulltext' => true,
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'announcement' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'type' => 'text',
                'label' => 'Анонс для главной',
                'fulltext' => true,
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Изображение для новости(1:2)',
                'params' => array(
                    'need_help' => true,
                    'aspect' => 2,
                    'size' => 200,
                ),
            ),
            'photo_slider' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Изображение для слайдера(1:2)',
                'params' => array(
                    'need_help' => true,
                    'aspect' => 2,
                    'size' => 200,
                ),
            ),
            'date' => array(
                'edit' => true,
                'head' => false,
                'search' => false,
                'type' => 'datetime',
                'label' => 'Дата публикации',
            ),
            'federations' => array(
                'edit' => true,
                'head' => true,
                'type' => 'external_select',
                'label' => 'Федерация',
                'params' => array(
                    'src' => ORM::factory('Federation')->external_list()
                )
            ),
            'slider' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Вывод в слайдере',
                'params' => array(
                    'options' => array(
                        1 => 'Не отображать',
                        2 => 'Отобразить'
                    )
                )
            ),
            'main' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Вывод на главной',
                'params' => array(
                    'options' => array(
                        1 => 'Не отображать',
                        2 => 'Отобразить'
                    )
                )
            ),
            'main_preview' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Главное за сутки',
                'params' => array(
                    'options' => array(
                        1 => 'Не отображать',
                        2 => 'Отобразить'
                    )
                )
            ),
            'categories' => array(
                'edit' => true,
                'head' => true,
                'type' => 'external_select',
                'label' => 'Вид новости',
                'params' => array(
                    'src' => ORM::factory('Category')->get_news_external(),
//                    'value' => $this->categories->select_options('id', 'title_ru')
                )
            ),
            'sport' => array(
                'edit' => true,
                'head' => true,
                'search' => false,
                'type' => 'select_extended',
                'label' => 'Вид спорта',
                'params' => array(
                    'options' => ORM::factory('Category')->get_sef_sports()->select_options('id', 'title_ru'),
                    'value' => $this->sport->select_options('id', 'title_ru')
                )
            ),
            'author' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'type' => 'strings',
                'label' => 'Автор',
            ),
            'source' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'type' => 'strings',
                'label' => 'Источник информации',
            ),
            'photograph' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'type' => 'strings',
                'label' => 'Автор фото',
            ),
            'synch_site' => array(
                'edit' => false,
                'head' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Данные с сайта',
            ),
            'is_published' => array(
                'edit' => true,
                'head' => true,
                'type' => 'select',
                'label' => 'Отображать на сайте',
                'params' => array(
                    'options' => array(
                        1 => 'Опубликовать',
                        2 => 'Не опубликовать'
                    )
                )
            ),
            'rio' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Рио 2016',
                'params' => array(
                    'options' => array(
                        0 => 'Нет',
                        1 => 'Да'
                    )
                )
            ),
            'sef' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ',
            ),
            'sef_ru' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на русском языке',
            ),
            'sef_en' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на английском языке',
            ),
            'sef_kz' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на казахском языке',
            ),
            'translation' => array(
                'edit' => false,
                'head' => true,
                'label' => 'Перевод'
            )
        );
    }

    public function rules()
    {
        return array(
//            'title_ru' => array(
//                array('not_empty'),
//            ),
            'text_ru' => array(
//                array('not_empty'),
                array('ORM::alt_image', array('text' => $this->text_ru))
            ),
            'text_kz' => array(
                array('ORM::alt_image', array('text' => $this->text_kz))
            ),
            'text_en' => array(
                array('ORM::alt_image', array('text' => $this->text_en))
            ),
            'date' => array(
                array('not_empty'),
                array('date')
            ),
            'source' => array(
                array('Valid::url'),
            ),

//            'announcement_ru'=>array(
//                array('not_empty')
//            ),
        );
    }

    public function get_field($field)
    {
        if ($field == 'title') {
            return Text::limit_chars(strip_tags($this->title), 100, " ...");
        }

        if ($field == 'federations') {
            $results = array();
            foreach ($this->federations->find_all() as $row) {
                $results[] = $row->title_ru;
            }
            $results = implode('<br>', $results);
            return $results;
        }
        if ($field == 'sport') {
            $results = array();
            foreach ($this->sport->find_all() as $row) {
                $results[] = $row->title_ru;
            }
            $results = implode('<br>', $results);
            return $results;
        }
        if ($field == 'categories') {
            $results = array();
            foreach ($this->categories->find_all() as $row) {
                $results[] = $row->title_ru;
            }
            $results = implode('<br>', $results);
            return $results;
        }
        if ($field == 'is_published') {
            $result = $this->is_published == 1 ? 'Опубликовано' : 'Сохранено';
            return $result;
        }
        if ($field == 'translation') {
            $results = array();
            $langs = array('ru' => 'русский', 'en' => 'английский', 'kz' => 'казахский');
            foreach ($langs as $key => $lang) {
                $results[] = $this->{'title_' . $key} && strip_tags($this->{'text_' . $key}) ? $lang : null;
            }
            $results = implode('<br>', $results);
            return $results;
        }
        return parent::get_field($field);
    }


    public function filters()
    {
        return array(
            'title_ru' => array(
                array('strip_tags')
            ),
            'title_en' => array(
                array('strip_tags')
            ),
            'title_kz' => array(
                array('strip_tags')
            ),
//            'text_ru' => array(
//                array('ORM::clear_html')
//            ),
//            'text_en' => array(
//                array('ORM::clear_html')
//            ),
//            'text_kz' => array(
//                array('ORM::clear_html'),
//            ),
            'announcement_ru' => array(
                array('ORM::clear_html')
            ),
            'announcement_en' => array(
                array('ORM::clear_html')
            ),
            'announcement_kz' => array(
                array('ORM::clear_html'),
            ),
//            'author'=>array(
//                array('strip_tags')
//            ),
//            'source'=>array(
//                array('strip_tags')
//            ),
//            'photograph'=>array(
//                array('strip_tags')
//            ),
            'date' => array(
                array('Date::formatted_time'),
            ),
            'sef' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_ru')),
            ),
            'sef_ru' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_ru')),
            ),
            'sef_en' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_en')),
            ),
            'sef_kz' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_kz')),
            ),
        );
    }

    public function get_announcement()
    {
        if ($this->announcement) {
            return $this->announcement;
        } elseif ($this->text) {
            return Text::limit_chars(strip_tags($this->text), 155);
        } else {
            return $this->announcement_ru;
        }
    }

    public function get_title()
    {
        return $this->title;
    }

    public function view_url($uri = false, $lang = false, $category = null, $is_federation = false)
    {
        $_uri = '';
        $lang = $lang ? $lang : I18n::$lang;
        if (Register::instance()->search) {
            if ($is_federation) {
                $_uri = '/news/';
            } else {
                switch ($category) {
                    case 'interviews':
                        $_uri = '/infocenter/interview/';
                        break;
                    case 'pressservice':
                        $_uri = '/infocenter/pressservice/';
                        break;
                    case 'mediaaboutus':
                        $_uri = '/infocenter/aboutus/';
                        break;
                    default:
                        $_uri = '/news/';
                }
            }
        } elseif ($this->loaded()) {
            $fed = Register::instance()->federation;
            $lang_route = $lang != 'kz' ? $lang . '/' : '/';
            if ($fed && $fed->loaded()) {
                    $_uri = $lang_route . $fed->sef . '/' . $this->_route . '/' . $this->{'sef_' . $lang};
            }else{
                if ($this->_route == 'news') {
                    $_uri = $lang_route . 'confederation/news/' . $this->{'sef_' . $lang};
                } else {
                    $_uri = $lang_route . 'confederation/infocenter/' . $this->_route . '/' . $this->{'sef_' . $lang};
                }
            }
        }
        return $uri ? $_uri : URL::site($_uri);
    }

    public function map_url($lang = null, $category = 'news')
    {
        $category = $category == 'mediaaboutus' ? 'aboutus' : $category;
        if ($category) {
            if ($category == 'interviews') {
                $_uri = '/interview/' . $this->{'sef_' . $lang};
            } else {
                $_uri = '/infocenter/' . $category . '/' . $this->{'sef_' . $lang};
            }
        } else {
            $_uri = '/news/' . $this->{'sef_' . $lang};
        }
        return $_uri;
    }


    public function search_extended_index()
    {
        $ext = array();
        foreach ($this->federations->find_all()->as_array('id', 'sef') as $id => $sef) {
            $ext[] = Search::escape_extend('fed_id', $sef);
        }
        $ext[] = Search::escape_extend('fed_id', 'confederation');
        return implode('\n', $ext);
    }

    public function actions($user)
    {
        $submenu = array();
        foreach ($this->federations->find_all() as $fed) {
            $submenu[] = array(
                'title' => $fed->title,
                'uri' => URL::site('ru/' . $fed->sef . '/news/' . $this->sef_ru)
            );
        }
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
            array(
                'title' => 'Просмотр',
                'type' => 'submenu',
                'submenu' => $submenu
            ),
        );
        return $menu;
    }

    function date_diff($date1, $date2)
    {
        $diff = strtotime($date2) - strtotime($date1);
        return abs($diff);
    }

    public function photo_loaded($photo_s)
    {
        $file = $photo_s->get_file();
        if ($file) {
            return Photo::translate2url($file->getRealPath());
        }
        return '#';
    }

    public function form_list_button(Component $cp)
    {
        $cp->button_link(URL::site('admin/news/synchronize_model'), "Синхронизировать с Федерациями", array('class' => 'btn btn-primary'));
    }
}