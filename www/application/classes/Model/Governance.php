<?php
class Model_Governance extends ORM
{
    protected $_belongs_to=array(
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
    );
    protected $_route='governance';
    protected function get_fields_description(){
        return array(
            'id'=>array(
                'head'=>true,
                'label'=>'#',
            ),
            'firstname' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'strings',
                'label' => 'Имя',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'middlename' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'strings',
                'label' => 'Отчество',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'lastname' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'strings',
                'label' => 'Фамилия',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'post'=>array(
                'edit'=>true,
                'head'=>true,
                'search'=>true,
                'type'=>'strings',
                'label'=>'Пост',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Изображение',
                'params' => array(
                    'need_help' => true,
                ),
            ),
            'priority' => array(
                'head'=>true,
                'edit' => true,
                'type' => 'strings',
                'label' => 'Приоритет',
            ),
        );
    }

    public function rules()
    {
        return array(
            'firstname_ru' => array(
                array('not_empty'),
            ),
            'lastname_ru' => array(
                array('not_empty')
            ),
            'post_ru' => array(
                array('not_empty')
            ),
        );
    }

    public function get_field($field)
    {

        return parent::get_field($field);
    }
    public function filters()
    {
        return array(
            'firstname_ru'=>array(
                array('strip_tags')
            ),
            'firstname_en'=>array(
                array('strip_tags')
            ),
            'firstname_kz'=>array(
                array('strip_tags')
            ),
            'lastname_ru'=>array(
                array('strip_tags')
            ),
            'lastname_en'=>array(
                array('strip_tags')
            ),
            'lastname_kz'=>array(
                array('strip_tags')
            ),
            'middlename_ru'=>array(
                array('strip_tags')
            ),
            'middlename_en'=>array(
                array('strip_tags')
            ),
            'middlename_kz'=>array(
                array('strip_tags')
            ),
            'post_ru'=>array(
                array('strip_tags')
            ),
            'post_en'=>array(
                array('strip_tags')
            ),
            'post_kz'=>array(
                array('strip_tags')
            ),
            'priority'=>array(
                array('strip_tags')
            ),
        );
    }
    public function get_full_name()
    {
        return $this->firstname." ".$this->lastname;
    }
    public function actions($user) {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
        );
        return $menu;
    }
}
?>