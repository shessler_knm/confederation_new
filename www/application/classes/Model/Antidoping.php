<?php

class Model_Antidoping extends ORM
{
    protected $_route = 'antidoping';
    protected $_belongs_to = array(
        'photoru_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photoru'
        ),
        'photoen_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photoen'
        ),
        'photokz_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photokz'
        ),

    );
    protected $_has_many = array(
        'children' => array(
            'model' => 'Category',
            'foreign_key' => 'parent_id'
        ),
        'sport' => array(
            'model' => 'Category',
            'through' => 'antidopings_sports',
            'far_key' => 'category_id'
        ),

    );

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'federation_id' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'select',
                'label' => 'Федерация',
                'params' => array(
                    'options' => ORM::factory('Federation')->select_options('id', 'title_ru')
                ),
            ),
            'sport' => array(
                'edit' => true,
                'head' => true,
                'search' => false,
                'type' => 'select_extended',
                'label' => 'Вид спорта',
                'params' => array(
                    'options' => ORM::factory('Category')->get_sef_sports()->select_options('id', 'title_ru'),
                    'value' => $this->sport->select_options('id', 'title_ru')
                )
            ),
            'photoru' => array(
                'edit' => true,
                'type' => 'file',
                'label' => 'Нормативы и правила на русском',
            ),
            'photoen' => array(
                'edit' => true,
                'type' => 'file',
                'label' => 'Нормативы и правила на английском',
            ),
            'photokz' => array(
                'edit' => true,
                'type' => 'file',
                'label' => 'Нормативы и правила на казахском',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'label' => 'Заголовок',
                'type' => 'strings',
                'params' => array('widget' => 'multilang'),
            ),
            'date' => array(
                'edit' => true,
                'head' => false,
                'search' => false,
                'type' => 'datetime',
                'label' => 'Дата публикации',
            ),
        );
    }

    public function get_field($field)
    {
        if ($field == 'federation_id') {
            return ORM::factory('Federation', $this->federation_id)->title_ru;
        }
        if ($field == 'sport') {
            $results = array();
            foreach ($this->sport->find_all() as $row) {
                $results[] = $row->title_ru;
            }
            $results = implode('<br>', $results);
            return $results;
        }
        return parent::get_field($field);
    }

    public function rules()
    {
        return array(
            'federation_id' => array(
                array('not_empty'),
            ),
            'photoru' => array(
                array('not_empty'),
            ),
            'date' => array(
                array('not_empty'),
                array('date')
            ),
            'title_ru' => array(
                array('not_empty'),
            )
        );
    }

    public function filters()
    {
        return array(
            'title_ru' => array(
                array('strip_tags')
            ),
            'title_en' => array(
                array('strip_tags')
            ),
            'title_kz' => array(
                array('strip_tags')
            ),
            'photoru'=>array(
                array('ORM::to_null')
            ),
            'photoen'=>array(
                array('ORM::to_null')
            ),
            'photokz'=>array(
                array('ORM::to_null')
            ),
            'date' => array(
                array('Date::formatted_time'),
            ),
        );
    }

    public function actions($user) {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
        );
        return $menu;
    }
}