<?php

class Model_Sponsor extends ORM
{
    protected $_route = 'sponsor';
    protected $_belongs_to = array(
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo',
        ),
    );
    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Изображение',
                'params' => array(
                    'need_help' => true,
                ),
            ),
            'sponsor' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'select',
                'label' => 'Спонсоры и участвующие организации',
                'params' => array(
                    'options' => array(
                        1 => 'Спонсор',
                        2 => 'Участвующая организация'
                    )
                )
            ),
            'url' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'URL',
            ),
        );
    }

    public function rules()
    {
        return array(
            'title_ru' => array(
                array('not_empty'),
            ),
            'sponsor'=>array(
                array('not_empty'),
            ),
            'url'=>array(
                array('Valid::url'),
            ),
        );
    }

    public function get_field($field)
    {
        if ($field == 'sponsor') {
            return $this->sponsor==1 ? 'Спонсор' : 'Участвующая организация';
        }
        if ($field == 'title') {
            return Text::limit_chars(strip_tags($this->title), 100, " ...");
        }
        return parent::get_field($field);
    }

    public function filters()
    {
        return array(
            'title_ru'=>array(
                array('strip_tags')
            ),
            'title_en'=>array(
                array('strip_tags')
            ),
            'title_kz'=>array(
                array('strip_tags')
            ),
        );
    }
    public function actions($user) {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
        );
        return $menu;
    }
}