<?php

class Model_Event extends ORM implements ISearchable, Synchronize_ISynch
{
    protected $_route = 'event';
    protected $_has_many = array(
        'categories' => array(
            'model' => 'Category',
            'through' => 'events_categories',
        ),
        'federations' => array(
            'model' => 'Federation',
            'through' => 'events_federations',
        ),
        'sponsors' => array(
            'model' => 'Sponsor',
            'through' => 'events_sponsors',
        ),
    );

    protected $_belongs_to = array(
        'city' => array(
            'model' => 'City',
            'foreign_key' => 'city_id'
        ),
    );

    protected function get_fields_description()
    {
        return array(
            'title' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'fulltext' => true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'text' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'type' => 'text',
                'fulltext' => true,
                'label' => 'Текст',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'date_begin' => array(
                'edit' => true,
                'head' => true,
                'type' => 'datetime',
                'label' => 'Дата начала события',
            ),
            'date_end' => array(
                'edit' => true,
                'head' => true,
                'type' => 'datetime',
                'label' => 'Дата конца события',
            ),
            'important' => array(
                'edit' => true,
                'type' => 'select',
                'label' => 'Тип события',
                'params' => array(
                    'options' => array(
                        '0' => 'Обычное событие',
                        '1' => 'Важное событие'
                    )
                )
            ),
            'federations' => array(
                'edit' => true,
                'head' => true,
                'type' => 'select_extended',
                'label' => 'Федерация',
                'params' => array(
                    'options' => ORM::factory('Federation')->select_options('id', 'title_ru'),
                    'value' => $this->federations->select_options('id', 'title_ru'),
                )
            ),
            'sponsors' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select_extended',
                'label' => 'Спонсоры и участвующие организации',
                'params' => array(
                    'options' => ORM::factory('Sponsor')->select_options('id', 'title_ru'),
                    'value' => $this->sponsors->select_options('id', 'title_ru'),
                )
            ),
            'categories' => array(
                'edit' => true,
                'head' => true,
                'type' => 'select_extended',
                'label' => 'Вид спорта',
                'params' => array(
                    'options' => ORM::factory('Category')->get_sef_sports()->select_options('id', 'title_ru'),
                    'value' => $this->categories->select_options('id', 'title_ru')
                )
            ),
            'city_id' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'select',
                'label' => 'Город',
                'params' => array(
                    'options' => ORM::factory('City')->select_options('id', 'title_ru'),
                )
            ),
            'address' => array(
                'edit' => true,
                'head' => true,
//                'search'=>true,
                'type' => 'strings',
                'label' => 'Адрес',
                'params' => array(
                    'widget' => 'MapPoint'
                )
            ),
            'telephone' => array(
                'edit' => true,
                'head' => false,
//                'search'=>true,
                'type' => 'strings',
                'label' => 'Телефон',
            ),
            'email' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'type' => 'strings',
                'label' => 'Email',
            ),
            'lat' => array(
                'edit' => true,
                'head' => false,
                'type' => 'hidden',
                'label' => 'Широта',
                'params' => array(
                    'widget' => 'MapPoint'
                )
            ),
            'lng' => array(
                'edit' => true,
                'head' => false,
                'type' => 'hidden',
                'label' => 'Долгота',
                'params' => array(
                    'widget' => 'MapPoint'
                )
            ),
            'sef' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ',
            ),
            'sef_ru' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на русском языке',
            ),
            'sef_en' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на английском языке',
            ),
            'sef_kz' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на казахском языке',
            ),
            'synch_site' => array(
                'edit' => false,
                'head' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Данные с сайта',
            ),
            'is_published' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Отображать на сайте',
                'params' => array(
                    'options' => array(
                        1 => 'Отображать',
                        2 => 'Не отображать'
                    )
                )
            ),
            'rio' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Рио 2016',
                'params' => array(
                    'options' => array(
                        0 => 'Нет',
                        1 => 'Да'
                    )
                )
            ),
        );
    }

//    public function rules()
//    {
//        return array(
//            'title_ru' => array(
//                array('not_empty'),
//            ),
//            'text_ru' => array(
//                array('ORM::alt_image',array('text_ru'=>$this->text_ru))
//            ),
//            'text_kz' => array(
//                array('ORM::alt_image',array('text_kz'=>$this->text_kz))
//            ),
//            'text_en' => array(
//                array('ORM::alt_image',array('text_en'=>$this->text_en))
//            ),
//            'date_begin' => array(
//                array('not_empty'),
//                array('date'),
//                array('ORM::range_date',array('date_begin'=>$this->date_begin,'date_end'=>$this->date_end))
//            ),
//            'date_end' => array(
//                array('not_empty'),
//                array('date'),
//            ),
//            'city_id' => array(
//                array('not_empty'),
//            ),
//        );
//    }

    public function get_field($field)
    {
        if ($field == 'title') {
            return Text::limit_chars(strip_tags($this->title), 100, " ...");
        }
        if ($field == 'date_begin') {
            return Date::formatted_time($this->date_begin,'d.m.Y H:i');
        }
        if ($field == 'date_end') {
            return Date::formatted_time($this->date_begin,'d.m.Y H:i');
        }
        if ($field == 'city_id') {
            return ORM::factory('City', $this->city)->title_ru;
        }
        if ($field == 'federations') {
            $results = array();
            foreach ($this->federations->find_all() as $row) {
                $results[] = $row->title_ru;
            }
            $results = implode('<br>', $results);
            return $results;
        }

        if ($field == 'categories') {
            $results = array();
            foreach ($this->categories->find_all() as $row) {
                $results[] = $row->title_ru;
            }
            $results = implode('<br>', $results);
            return $results;
        }
        return parent::get_field($field);
    }

    public function filters()
    {
        return array(
            'title_ru' => array(
                array('strip_tags')
            ),
            'title_en' => array(
                array('strip_tags')
            ),
            'title_kz' => array(
                array('strip_tags')
            ),
            'text_ru' => array(
                array('ORM::clear_html')
            ),
            'text_en' => array(
                array('ORM::clear_html')
            ),
            'text_kz' => array(
                array('ORM::clear_html')
            ),
            'telephone' => array(
                array('strip_tags')
            ),
            'sef' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_ru')),
            ),
            'sef_ru' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_ru')),
            ),
            'sef_en' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_en')),
            ),
            'sef_kz' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_kz')),
            ),
            'date_end' => array(
                array('Date::formatted_time'),
            ),
            'date_begin' => array(
                array('Date::formatted_time'),
            ),
        );
    }

    public function actions($user)
    {
        $submenu = array();
        foreach ($this->federations->find_all() as $fed) {
            $submenu[] = array(
                'title' => $fed->title,
                'uri' => URL::site($fed->sef . '/event/view/' . $this->id)
            );
        }
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
            array(
                'title' => 'Просмотр',
                'type' => 'submenu',
                'submenu' => $submenu
            ),
        );
        return $menu;
    }

    function rudate($format, $timestamp = 0, $nominative_month = true, $type = 'date_begin')
    {
        if (!$timestamp) $timestamp = mktime(Date::formatted_time($this->date_end, 'H'), Date::formatted_time($this->{$type}, 'i'), Date::formatted_time($this->{$type}, 's'), Date::formatted_time($this->{$type}, 'n'), Date::formatted_time($this->{$type}, 'j'), Date::formatted_time($this->{$type}, 'Y'));
        elseif (!preg_match("/^[0-9]+$/", $timestamp)) $timestamp = strtotime($timestamp);
        $F = $nominative_month ? array(1 => "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь") : array(1 => "Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря");
        $M = array(1 => "Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек");
        $M[1] = __('Янв');
        $M[2] = __('Фев');
        $M[3] = __('Мар');
        $M[4] = __('Апр');
        $M[5] = __('Май');
        $M[6] = __('Июн');
        $M[7] = __('Июл');
        $M[8] = __('Авг');
        $M[9] = __('Сен');
        $M[10] = __('Окт');
        $M[11] = __('Ноя');
        $M[12] = __('Дек');
        $format = str_replace("F", $F[date("n", $timestamp)], $format);
        $format = str_replace("M", $M[date("n", $timestamp)], $format);

        return date($format, $timestamp);
    }

    public function text_date($short_month = false)
    {
        if (I18n::$lang != 'en') {
            $short_month_begin = $short_month ? $this->rudate('M') : 'm';
            $short_month_end = $short_month ? $this->rudate('M', 0, true, 'date_end') : 'm';
            if (Date::formatted_time($this->date_begin, 'd n') != Date::formatted_time($this->date_end, 'd n')) {
                if (Date::formatted_time($this->date_begin, 'n') == Date::formatted_time($this->date_end, 'n')) {
                    return Date::textdate($this->date_begin, 'j') . ' - ' . Date::textdate($this->date_end, 'j ' . $short_month_end);
                } else {
                    return Date::textdate($this->date_begin, 'j ' . $short_month_begin) . ' - ' . Date::textdate($this->date_end, 'j ' . $short_month_end);
                }
            } else {
                return Date::textdate($this->date_begin, 'j ' . $short_month_begin);
            }
        } else {
            $short_month = $short_month ? 'M' : 'm';
            if (Date::formatted_time($this->date_begin, 'd n') != Date::formatted_time($this->date_end, 'd n')) {
                if (Date::formatted_time($this->date_begin, 'n') == Date::formatted_time($this->date_end, 'n')) {
                    return Date::textdate($this->date_begin, 'j') . ' - ' . Date::textdate($this->date_end, 'j ' . $short_month);
                } else {
                    return Date::textdate($this->date_begin, 'j ' . $short_month) . ' - ' . Date::textdate($this->date_end, 'j ' . $short_month);
                }
            } else {
                return Date::textdate($this->date_begin, 'd ' . $short_month);
            }
        }
    }

    public function search_extended_index()
    {
        $ext = array();
        foreach ($this->federations->find_all()->as_array('id', 'sef') as $id => $sef) {
            $ext[] = Search::escape_extend('fed_id', $sef);
        }
        $ext[] = Search::escape_extend('fed_id', 'confederation');
        return implode('\n', $ext);
    }

    public function view_url($uri = false, $lang = null)
    {
        $_uri = '';
        if (Register::instance()->search) {
            $_uri = '/event/';
        } elseif ($this->loaded()) {
            $fed = Register::instance()->federation;
            $lang_route = I18n::$lang != 'kz' ? I18n::$lang . '/' : '/';
            $_uri = $fed && $fed->loaded() ? $lang_route . $fed->sef . '/' . $this->_route . '/' . $this->{'sef_' . I18n::$lang} : $lang_route . 'confederation/' . $this->_route . '/' . $this->{'sef_' . I18n::$lang};
        }

        return $uri ? $_uri : URL::site($_uri);
    }

    public function map_url($lang = null)
    {
        $_uri = '/' . $this->_route . '/' . $this->{'sef_' . $lang};
        return $_uri;
    }

    public function form_list_button(Component $cp)
    {
        $cp->button_link(URL::site('admin/event/synchronize_model'), "Синхронизировать с Федерациями", array('class' => 'btn btn-primary'));
    }
}