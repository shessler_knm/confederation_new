<?php
/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 09.08.13
 * Time: 11:37
 * To change this template use File | Settings | File Templates.
 */

class Model_Program_Log extends ORM{

    protected $_belongs_to=array(
        'program'=>array(

        ),
        'exercise'=>array(),
        'user'=>array(),
    );

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'edit' => false,
                'search' => true,
                'head' => false,
                'label' => '',
                'type' => ''
            ),
            'program_id' => array(
                'edit' => true,
                'search' => true,
                'head' => false,
                'label' => '',
                'type' => 'strings'
            ),
            'exercise_id' => array(
                'edit' => true,
                'search' => true,
                'head' => false,
                'label' => '',
                'type' => 'stings'
            ),
            'date' => array(
                'edit' => true,
                'search' => true,
                'head' => false,
                'label' => '',
                'type' => 'strings'
            ),
            'status' => array(
                'edit' => true,
                'search' => true,
                'head' => false,
                'label' => '',
                'type' => 'strings'
            ),
        );
    }


}