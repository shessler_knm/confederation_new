<?php
/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 07.08.13
 * Time: 10:09
 * To change this template use File | Settings | File Templates.
 */

class Model_Program_Exercise extends ORM_Embeded {

    protected $_parent_field="program_id";
    protected $_parent_model="Program";
    protected $_table_name="program_exercises";
    protected $_belongs_to=array(
        'exercise'=>array(),
        'program'=>array(),
    );

    protected function get_fields_description(){
        return array(
            'id'=>array(
                'head'=>true,
                'label'=>'#',
            ),
            'exercise_id' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'strings',
                'label' => 'Упражнение',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'program_id' => array(
                'edit' => true,
                'head' => false,
//                'search'=>true,
                'fulltext'=>true,
                'type' => 'text',
                'label' => 'Программа',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'week_day'=>array(
                'edit'=>true,
                'head'=>true,
                'search'=>true,
                'type'=>'strings',
                'label'=>'День недели',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'sets' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'select',
                'label' => 'Подходы и повторения',
            )
        );
    }

//    public function get_field($field)
//    {
//        if ($field == 'program_id'){
//            return ORM::factory('Program',$this->program_id)->title_ru;
//        }
//        if($field == 'exercise_id'){
//            return ORM::factory('Exercise',$this->exercise_id)->title_ru;
//        }
//        return parent::get_field($field);
//    }

    public function as_array_ext($fields = array(), $action = null){
        $data = parent::as_array_ext($fields);
        $data['index'] =$this->index_number;
        preg_match_all('#(\d+)#',$data['sets'],$sets);
        $data['sets'] = $sets[1];
        return $data;
    }


}