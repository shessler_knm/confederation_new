<?php
class Model_Section extends ORM
{
    protected $_belongs_to = array(
        'federation' => array(),
        'city' => array(
            'model' => 'City',
            'foreign_key' => 'city_id'
        ),
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
    );

    protected $_has_many = array(
        'categories' => array(
            'model' => 'Category',
            'through' => 'sections_categories',
        ),
    );
    protected $_route = 'section';

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Секции',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'description' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'type' => 'text',
                'label' => 'Описание',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Изображение',
                'params' => array(
                    'need_help' => true,
                ),
            ),
            'sports_description' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'type' => 'text',
                'label' => 'Виды секций',
                'params' => array(
                    'widget' => 'multilang',
                )
            ),
            'categories' => array(
                'edit' => true,
                'head' => true,
                'type' => 'select_extended',
                'label' => 'Вид спорта',
                'params' => array(
                    'options' => ORM::factory('Category')->get_sef_sports()->select_options('id', 'title_ru'),
                    'value' => $this->categories->select_options('id', 'title_ru')
                )
            ),
            'city_id' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'select',
                'label' => 'Город',
                'params' => array(
                    'options' => ORM::factory('City')->select_options('id', 'title_ru'),
                )
            ),
            'address' => array(
                'edit' => true,
                'head' => true,
//                'search'=>true,
                'type' => 'strings',
                'label' => 'Адрес',
                'params' => array(
                    'widget' => 'MapPoint',
                )
            ),
            'address_en' => array(
                'edit' => true,
                'head' => false,
//                'search'=>true,
                'type' => 'strings',
                'label' => 'Адрес на английском языке',
            ),
            'address_kz' => array(
                'edit' => true,
                'head' => false,
//                'search'=>true,
                'type' => 'strings',
                'label' => 'Адрес на казахском языке',
            ),
            'telephone' => array(
                'edit' => true,
                'head' => true,
//                'search'=>true,
                'type' => 'strings',
                'label' => 'Телефон',
            ),
            'email' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'type' => 'strings',
                'label' => 'Email',
            ),
            'federation_id' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'select',
                'label' => 'Федерация',
                'params' => array(
                    'options' => ORM::factory('Federation')->select_options('id', 'title_ru')
                )
            ),
            'lat' => array(
                'edit' => true,
                'head' => false,
                'type' => 'hidden',
                'label' => 'Широта',
                'params' => array(
                    'widget' => 'MapPoint'
                )
            ),
            'lng' => array(
                'edit' => true,
                'head' => false,
                'type' => 'hidden',
                'label' => 'Долгота',
                'params' => array(
                    'widget' => 'MapPoint'
                )
            ),
            'sef' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ',
            ),
            'sef_ru' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на русском языке',
            ),
            'sef_en' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на английском языке',
            ),
            'sef_kz' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на казахском языке',
            ),

        );
    }

    public function rules()
    {
        return array(
            'title_ru' => array(
                array('not_empty')
            ),
            'address_en'=>array(
                array('not_empty')
            ),
            'address_kz'=>array(
                array('not_empty')
            ),
            'description_ru' => array(
                array('not_empty'),
                array('ORM::alt_image', array('text' => $this->description_ru))
            ),
            'description_kz' => array(
                array('ORM::alt_image', array('text' => $this->description_kz))
            ),
            'description_en' => array(
                array('ORM::alt_image', array('text' => $this->description_en))
            ),
            'sports_description_ru' => array(
                array('not_empty'),
                array('ORM::alt_image', array('text' => $this->sports_description_ru))
            ),
            'sports_description_kz' => array(
                array('ORM::alt_image', array('text' => $this->sports_description_kz))
            ),
            'sports_description_en' => array(
                array('ORM::alt_image', array('text' => $this->sports_description_en))
            ),
            'federation_id' => array(
                array('not_empty')
            ),
//            'email' => array(
//                array('not_empty'),
//                array('email'),
//                array('min_length', array(':value', 6)),
//                array(array($this, 'unique'), array('email', ':value'))
//            ),
        );
    }

    public function get_field($field)
    {
        if ($field == 'federation_id') {
            return ORM::factory('Federation', $this->federation_id)->title_ru;
        }
        if ($field == 'city_id') {
            return ORM::factory('City', $this->city_id)->title_ru;
        }
        if ($field == 'title') {
            return Text::limit_chars(strip_tags($this->title), 100, " ...");
        }
        if ($field == 'categories') {
            $results = array();
            foreach ($this->categories->find_all() as $row) {
                $results[] = $row->title_ru;
            }
            $results = implode('<br>', $results);
            return $results;
        }
        return parent::get_field($field);
    }

    public function filters()
    {
        return array(
            'title_ru' => array(
                array('strip_tags')
            ),
            'title_en' => array(
                array('strip_tags')
            ),
            'title_kz' => array(
                array('strip_tags')
            ),
            'description_ru' => array(
                array('ORM::clear_html')
            ),
            'description_en' => array(
                array('ORM::clear_html')
            ),
            'description_kz' => array(
                array('ORM::clear_html')
            ),
            'sports_description_ru' => array(
                array('ORM::clear_html')
            ),
            'sports_description_en' => array(
                array('ORM::clear_html')
            ),
            'sports_description_kz' => array(
                array('ORM::clear_html')
            ),
            'telephone' => array(
                array('strip_tags')
            ),
            'sef' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_ru')),
            ),
            'sef_ru' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_ru')),
            ),
            'sef_en' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_en')),
            ),
            'sef_kz' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_kz')),
            ),
            'address_en' => array(
                array('strip_tags')
            ),
            'address_kz' => array(
                array('strip_tags')
            ),
        );
    }

    public function view_url($uri = false)
    {
        $_uri = '';
        if($this->loaded()){
            $fed = Register::instance()->federation;
            $lang_route = I18n::$lang!='kz'?I18n::$lang.'/':'/';
            $_uri = $fed && $fed->loaded()?$lang_route.$fed->sef.'/'.$this->_route.'/'.$this->{'sef_'.I18n::$lang}:$lang_route.'confederation/'.$this->_route.'/'.$this->{'sef_'.I18n::$lang};
        }
        if(Register::instance()->search){
            $_uri = '/'.$this->_route.'/'.$this->{'sef_'.I18n::$lang};
        }
        return $uri ? $_uri : URL::site($_uri);
    }

    public function map_url($lang = null)
    {
        $_uri = '/'.$this->_route.'/' . $this->{'sef_' . $lang};
        return $_uri;
    }

    public function actions($user)
    {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
            array(
                'title' => 'Просмотр',
                'uri' => $this->view_url()
            ),
        );
        return $menu;
    }
}