<?php

class Model_Player extends ORM_Embeded implements ISearchable, Synchronize_ISynch
{
    protected $_parent_field = "team_id";
    protected $_parent_model = "Team";
    protected $_belongs_to = array(
        'team' => array(),
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
        'coach' => array(
            'model' => 'Coach',
            'foreign_key' => 'coach_id'
        ),
        'federation' => array(
            'model' => 'Federation',
        ),

    );
    protected $_route = 'player';

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'firstname' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'fulltext' => true,
                'type' => 'strings',
                'label' => 'Имя',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'middlename' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'fulltext' => true,
                'type' => 'strings',
                'label' => 'Отчество',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'lastname' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'fulltext' => true,
                'type' => 'strings',
                'label' => 'Фамилия',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Изображение',
                'params' => array(
                    'need_help' => true,
                ),
            ),
            'biography' => array(
                'edit' => true,
                'head' => false,
//                'search' => true,
                'type' => 'text',
                'fulltext' => true,
                'label' => 'Биография',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'sex' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'select',
                'label' => 'Пол',
                'params' => array(
                    'options' => array(
                        1 => 'мужской',
                        2 => 'женский'
                    )
                )
            ),
            'category' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'select',
                'label' => 'Категории',
                'params' => array(
                    'options' => array(
                        '0' => 'не указано',
                        '1' => 'до',
                        '2' => '=',
                        '3' => 'свыше',
                    ),
                ),
            ),
            'weight' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Весовая категория',
            ),
            'federation_id' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'select',
                'label' => 'Федерация',
                'params' => array(
                    'options' => ORM::factory('Federation')->select_options('id', 'title_ru')
                )
            ),
            'team_id' => array(
                'edit' => true,
//                'head' => true,
//                'search'=>true,
                'type' => 'select',
                'label' => 'Команда',
                'params' => array(
                    'options' => ORM::factory('Team')->select_options('id', 'title_ru'),
//                    'disabled'=>'disabled'
                )
            ),
            'coach_id' => array(
                'edit' => true,
                'head' => true,
//                'search'=>true,
                'type' => 'select',
                'label' => 'Тренер',
                'params' => array(
                    'options' => ORM::factory('Coach')->select_full_name()->find_all()->as_array('id', 'fullname')
                )
            ),
            'birthplace' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'search' => true,
                'label' => 'Место рождения',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'birthday' => array(
                'edit' => true,
                'head' => true,
//                'search'=>true,
                'type' => 'date',
                'label' => 'Дата рождения',
            ),
            'is_published' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Отображать на сайте',
                'params' => array(
                    'options' => array(
                        1 => 'Отображать',
                        2 => 'Не отображать'
                    )
                )
            ),
            'synch_site' => array(
                'edit' => false,
                'head' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Источник данных',
            ),
            'is_published' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Отображать на сайте',
                'params' => array(
                    'options' => array(
                        1 => 'Отображать',
                        2 => 'Не отображать'
                    )
                )
            ),
            'rio' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Рио 2016',
                'params' => array(
                    'options' => array(
                        0 => 'Нет',
                        1 => 'Да'
                    )
                )
            ),
            'sef' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ',
            ),
            'sef_ru' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на русском языке',
            ),
            'sef_en' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на английском языке',
            ),
            'sef_kz' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на казахском языке',
            ),

        );
    }

    public function rules()
    {
        return array(
            'firstname_ru' => array(
                array('not_empty'),
            ),
            'lastname_ru' => array(
                array('not_empty')
            ),
            'biography_ru' => array(
                array('ORM::alt_image', array('biography_ru' => $this->biography_ru))
            ),
            'biography_kz' => array(
                array('ORM::alt_image', array('biography_kz' => $this->biography_kz))
            ),
            'biography_en' => array(
                array('ORM::alt_image', array('biography_en' => $this->biography_en))
            ),
            'sex' => array(
                array('not_empty'),
            ),
            'weight' => array(
                array('not_empty'),
            ),
//            'coach_id' => array(
//                array('not_empty')
//            ),
//            'birthplace_ru' => array(
//                array('not_empty')
//            ),
            'birthday' => array(
                array('not_empty'),
                array('date')
            ),
        );
    }

    public function get_field($field)
    {
        if ($field == 'sex') {
            return $this->sex == 1 ? 'Мужской' : 'Женский';
        }
        if ($field == 'federation_id') {
            return ORM::factory('Federation', $this->federation_id)->title_ru;
        }
        if ($field == 'coach_id') {
            return ORM::factory('Coach', $this->coach_id)->lastname_ru;
        }
        if ($field == 'command_id') {
            return ORM::factory('Command', $this->command_id)->title;
        }
        if ($field == 'category') {
            switch ($this->category) {
                case 1:
                    $r = 'до';
                    break;
                case 2:
                    $r = '=';
                    break;
                case 3:
                    $r = 'свыше';
                    break;
                default:
                    $r = 'Не указано';
                    break;
            }
            return $r;
        }
        return parent::get_field($field);
    }

    public function filters()
    {
        return array(
            'firstname_ru' => array(
                array('strip_tags')
            ),
            'firstname_en' => array(
                array('strip_tags')
            ),
            'firstname_kz' => array(
                array('strip_tags')
            ),
            'lastname_ru' => array(
                array('strip_tags')
            ),
            'lastname_en' => array(
                array('strip_tags')
            ),
            'lastname_kz' => array(
                array('strip_tags')
            ),
            'middlename_ru' => array(
                array('strip_tags')
            ),
            'middlename_en' => array(
                array('strip_tags')
            ),
            'middlename_kz' => array(
                array('strip_tags')
            ),
            'biography_ru' => array(
                array('ORM::clear_html')
            ),
            'biography_en' => array(
                array('ORM::clear_html')
            ),
            'biography_kz' => array(
                array('ORM::clear_html')
            ),
            'birthplace_ru' => array(
                array('strip_tags')
            ),
            'birthplace_en' => array(
                array('strip_tags')
            ),
            'birthplace_kz' => array(
                array('strip_tags')
            ),
            'weight' => array(
                array('strip_tags'),
            ),
            'birthday' => array(
                array('Date::formatted_time', array(':value', 'Y-m-d'))
            ),
            'sef' => array(
                array('ORM::full_name_filter_sef', array(':value', $this->get_full_name())),
            ),
            'sef_ru' => array(
                array('ORM::full_name_filter_sef', array(':value', $this->get_full_name('ru'))),
            ),
            'sef_en' => array(
                array('ORM::full_name_filter_sef', array(':value', $this->get_full_name('en'))),
            ),
            'sef_kz' => array(
                array('ORM::full_name_filter_sef', array(':value', $this->get_full_name('kz'))),
            ),

        );
    }

    public function get_full_name($lang = NULL)
    {
        if ($lang !== NULL) {
            if ($this->{'lastname_' . $lang} && $this->{'firstname_' . $lang}) {
                return $this->{'lastname_' . $lang} . " " . $this->{'firstname_' . $lang};
            } else {
                return null;
            }
        } elseif ($this->lastname && $this->firstname) {
            return $this->lastname . " " . $this->firstname;
        } else {
            return null;
        }
    }

    public
    function view_url($uri = false, $lang = null)
    {
        $_uri = '';
        if (Register::instance()->search) {
            $_uri = '/team/person/';
        } elseif ($this->loaded()) {
            $fed = $this->federation;
            $_uri = I18n::$lang != 'kz' ? I18n::$lang . '/' . $fed->sef . '/team/person/' . $this->{'sef_' . I18n::$lang} : $fed->sef . '/team/person/' . $this->{'sef_' . I18n::$lang};
        }
        return $uri ? $_uri : URL::site($_uri);
    }

    public
    function map_url($lang = null)
    {
        $_uri = '/team/' . $this->{'sef_' . $lang};
        return $_uri;
    }

    public
    function actions($user)
    {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
            array(
                'title' => 'Просмотр',
                'uri' => $this->view_url()
            ),
        );
        return $menu;
    }

    public
    function title()
    {
        $arg = func_get_args();
        if (!count($arg)) {
            return $this->firstname . ' ' . $this->lastname . ' ' . $this->middlename;
        } else {
            return $this->{'firstname_' . $arg[0]} . ' ' . $this->{'lastname_' . $arg[0]} . ' ' . $this->{'middlename_' . $arg[0]};
        }
    }

    public
    function short_text()
    {
        $arg = func_get_args();
        if (!count($arg)) {
            return $this->biography;
        } else {
            return $this->{'biography_' . $arg[0]};
        }
    }

    public
    function form_list_button(Component $cp)
    {
        $parent = ORM::factory($this->parent_model(), $this->{$this->parent_field()});
        if ($parent->synch_id !== NULL) {
            $cp->button_link(URL::site('admin/player/synchronize_model/' . $parent->id), "Синхронизировать с Федерациями", array('class' => 'btn btn-primary'));
        }
    }

//    public function search_extended_index()
//    {
//        $ext = array();
//        $ext[] = Search::escape_extend('fed_id',  $this->federation->sef);
//        $ext[] = Search::escape_extend('fed_id','confederation');
//        return implode('\n', $ext);
//    }
}