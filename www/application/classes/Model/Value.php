<?php

class Model_Value extends ORM
{
    protected $_route = 'value';
    protected $_belongs_to=array(
        'key' => array(
            'model' => 'Key',
            'foreign_key' => 'key_id'
        ),
    );
}