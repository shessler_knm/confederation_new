<?php

class Model_Menu extends ORM_Embeded
{
    protected $_parent_field="parent_id";
    protected $_parent_model="Menu";
    protected $_route='menu';
    protected $_has_many = array(
        'federations'=>array(
            'model'=>'Federation',
            'through'=>'federations_menus',
        ),
        'children' => array(
            'model' => 'Menu',
            'foreign_key' => 'parent_id',
        ),
    );

    public function filters() {
        return array(
            'title_ru'=>array(
                array('trim'),
                array('strip_tags')
            ),
            'title_kz'=>array(
                array('trim'),
                array('strip_tags')
            ),
            'title_en'=>array(
                array('trim'),
                array('strip_tags')
            ),
            'menu'=>array(
                array('Model_Menu::paste_menu_value',array(':value'))
            )
        );
    }

    public static function paste_menu_value($value) {
        $id = Request::current()->param('id2');
        $parent = ORM::factory('Menu',array('id'=>$id));
        if($parent->loaded()){
            if($parent->menu != NULL){
                return $parent->menu;
            }
            return $id;
        }
    }

    public function rules() {
        if(!$this->is_embeded()){
            $r = array(
                'title_ru'=>array(
                    array('not_empty'),
                 ),
                 'sef'=>array(
                    array('not_empty')
                 )
            );
        }else{
            $r = array(
                 'title_ru'=>array(
                    array('not_empty'),
                 ),
            );
        }
        return $r;
    }

    public function get_field($field) {
        if($field == 'federations'){
            $return = '';
            $federations = $this->federations->find_all();
            if(count($federations) > 0){
                foreach ($federations as $fed){
                    $return .= $fed->title.' <br /><br />';
                }
            }
            return $return;
        }
        return parent::get_field($field);
    }


    protected function get_fields_description() {
        if(!$this->is_embeded()){
            $return = array(
                'id' => array(
                    'head' => true,
                    'label' => '#',
                ),
                'title_ru' => array(
                    'edit' => true,
                    'head' => true,
                    'search' => true,
                    'type' => 'strings',
                    'label' => 'Название',
                ),
                'sef' => array(
                    'edit' => true,
                    'head' => false,
                    'search' => true,
                    'type' => 'strings',
                    'label' => 'ЧПУ',
                ),
                'children' => array(
                    'edit' => true,
                    'type' => 'embeded',
                    'label' => 'Элементы меню',
                ),
            );
        } else{
            $return = array(
                'id' => array(
                    'head' => true,
                    'label' => '#',
                ),
                'title' => array(
                    'edit' => true,
                    'head' => true,
                    'search' => true,
                    'type' => 'strings',
                    'label' => 'Название',
                    'params' => array(
                        'widget' => 'multilang',
                    ),
                ),
                'link' => array(
                    'edit' => true,
                    'head' => true,
                    'search' => true,
                    'type' => 'strings',
                    'label' => 'Ссылка',
                ),
                'federations' => array(
                    'edit' => true,
                    'head' => true,
                    'type' => 'select_extended',
                    'label' => 'Федерация',
                    'params' => array(
                        'options' => ORM::factory('Federation')->select_options('id', 'title_ru'),
                        'value' => $this->federations->select_options('id', 'title_ru')
                    )
                ),
                'newblank' => array(
                    'edit' => true,
                    'type' => 'select',
                    'label' => 'Открывать ссылку в новом окне',
                    'params'=>array(
                        'options'=>array('0'=>'Нет','1'=>'Да')
                    )
                ),
                'priority' => array(
                    'head'=>true,
                    'edit' => true,
                    'type' => 'strings',
                    'label' => 'Приоритет',
                ),
                'menu'=>array(
                    'edit'=>true,
                    'type'=>'hidden',
                    'label'=>'Определяем к какому меню относится'
                ),
                'children' => array(
                    'edit' => true,
                    'type' => 'embeded',
                    'label' => 'Элементы меню',
                ),
            );
        }
        return $return;
    }

    public function link(){
        if(Valid::url($this->link)){
            return $this->link;
        } else{
            $link = ($this->link) ?
                    URL::site(trim(strtr($this->link,array(':federation'=> ($fed_id = Request::current()->param('fed_id')) ? $fed_id : 'confederation',':lang'=> I18n::lang())),'/'))
                    : '#';
            return $link;
        }
    }

    public function is_embeded(){
        $id = Request::current()->param('id');
        $id2 = Request::current()->param('id2');
        return ( $id === '0' && !$id2 || $id2 === '0' || (!$id2 && !$id)  ) ? false : true;
    }

    public function view_url($uri = false) {
        return FALSE;
    }

    public function actions($user = null) {
            $menu = array(
                array(
                    'title' => 'Редактировать',
                    'uri' => $this->edit_url(true)
                ),
                array(
                    'title' => 'Удалить',
                    'uri' => $this->delete_url(true)
                ),
            );
        return $menu;
    }
}
?>
