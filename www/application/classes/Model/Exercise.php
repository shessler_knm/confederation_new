<?php

class Model_Exercise extends ORM
{

    protected $_route = 'exercise';
    protected $_has_many = array(
        'categories' => array(
            'model' => 'Category',
            'through' => 'exercises_categories',
        ),
        'images' => array(
            'model' => 'Exercise_Storage'
        ),
        'exercise_scores' => array(
            'model' => 'Exercise_Score',
            'foreign_key' => 'target_id'
        )
    );
    protected $_belongs_to = array(
        'image1_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'image1'
        ),
        'image2_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'image2'
        ),
    );

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Название упражнения',
                'params' => array(
                    'widget' => 'multilang'
                )
            ),
            'text' => array(
                'edit' => true,
                'search' => true,
                'type' => 'text',
                'label' => 'Описание упражнения',
                'params' => array(
                    'widget' => 'multilang'
                )
            ),
//            'image1' => array(
//                'edit' => true,
//                'type' => 'image',
//                'label' => 'Изображение #1',
//                'params' => array(
//                    'need_help' => true,
//                    'aspect' => 2,
//                    'size' => 200,
//                ),
//            ),
//            'image2' => array(
//                'edit' => true,
//                'type' => 'image',
//                'label' => 'Изображение #2',
//                'params' => array(
//                    'need_help' => true,
//                    'aspect' => 2,
//                    'size' => 200,
//                ),
//            ),
            'video_url' => array(
                'edit' => true,
                'type' => 'strings',
                'label' => 'URL видео',
            ),
            'images' => array(
                'type' => 'embeded',
                'label' => 'Изображения',
                'edit' => true,
            ),
            'uploads' => array(
                'type' => 'multifile',
                'label' => 'Мультизагрузка файлов',
                'edit' => true,
            ),
            'categories' => array(
                'edit' => true,
                'head' => true,
                'type' => 'external_select',
                'label' => 'Группа мышц',
                'params' => array(
                    'src' => ORM::factory('Category')->get_muscle_external(),
                )
            ),

        );
    }

    public function get($column)
    {
        if ($column == 'uploads') {
            return null;
        }
        return parent::get($column);
    }

    public function rules()
    {
        return array(
            'title_ru' => array(
                array('not_empty'),
            ),
//            'link' => array(
//                array('not_empty')
//            ),
        );
    }

    public function as_array_ext($fields = array(), $action = null)
    {
        $data = parent::as_array_ext($fields);
        $image_array = array();
        $images = $this->images->find_all();
        if ($action == 'exercise_list') {
            $width = 200;
            $height = 140;
        } else {
            $width = null;
            $height = null;
        }
        foreach ($images as $image) {
            $image_array[] = $image->storage_id_s->url_image($width, $height, 'http');
        }
        $data['name'] = $data['title'];
        $data['images_url'] = $image_array;
        $data['video_code'] = $this->parse_code_youtube_value($this->video_url);
        preg_match('#(\d+)#', $data['sets'], $sets);
        unset($data['title']);
        return $data;
    }

    public function get_field($field)
    {
        if ($field == 'categories') {
            $results = array();
            foreach ($this->categories->find_all() as $row) {
                $results[] = $row->title_ru;
            }
            $results = implode('<br>', $results);
            return $results;
        }
        return parent::get_field($field);
    }

    public function filters()
    {
        return array(
            'title_ru' => array(
                array('strip_tags')
            ),
            'video_url' => array(
                array(array($this, 'parse_code_youtube'), array(':value')),
            ),
        );
    }

    public function parse_code_youtube($value)
    {
        if (stripos($value, 'youtube.com/embed') !== false) {
            preg_match('#embed\/([^\&]+)#is', $value, $id);
            if (count($id) > 0) {
                $this->content = $id[1];
                return $value;
            }
        }
        if (stripos($value, 'youtube.com') !== false) {
            preg_match('#v=([^\&]+)#is', $value, $id);
            if (count($id) > 0) {
                $this->content = $id[1];
                return $value;
            }
        }
        if (stripos($value, 'youtu.be') !== false) {
            preg_match('#\youtu.be/([^\&]+)#is', $value, $id);
            if (count($id) > 0) {
                $this->content = $id[1];
                return $value;
            }
        }
        return '';
    }

    public function parse_code_youtube_value($value)
    {
        if (stripos($value, 'youtube.com/embed') !== false) {
            preg_match('#embed\/([^\&]+)#is', $value, $id);
            if (count($id) > 0) {
                $value = $id[1];
                return $value;
            }
        }
        if (stripos($value, 'youtube.com') !== false) {
            preg_match('#v=([^\&]+)#is', $value, $id);
            if (count($id) > 0) {
                $value = $id[1];
                return $value;
            }
        }
        if (stripos($value, 'youtu.be') !== false) {
            preg_match('#\youtu.be/([^\&]+)#is', $value, $id);
            if (count($id) > 0) {
                $value = $id[1];
                return $value;
            }
        }
        return '';
    }
}