<?php

class Model_User_Log extends ORM
{
    protected $_route = 'user_log';
    protected $_table_name = 'user_logs';
    protected $_belongs_to = array(
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
        'album' => array(
            'model' => 'User_Album',
            'foreign_key' => 'album_id'
        ),
    );

    public function filters()
    {
        return array(
            'fat' => array(
                array('strip_tags')
            ),
            'weight' => array(
                array('strip_tags')
            ),
            'date' => array(
                array('strip_tags')
            ),
        );
    }

    public function as_array_ext($fields = array(), $action = null)
    {
        $data = parent::as_array_ext($fields, $action);
        if ($action != 'statistics') {
            $data['photo'] = $this->photo_s->url(true);
        }
//        $data['date'] = Date::formatted_time($this->date,'d.m.Y');
        return $data;
    }
}
