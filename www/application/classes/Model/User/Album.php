<?php
class Model_User_Album extends ORM
{
//    protected $_route = 'user_album';
    protected $_table_name = 'user_albums';
    protected $_has_many = array(
        'photos' => array(
            'model' => 'User_Log',
            'foreign_key' => 'album_id'
        ),
    );
}
?>
