<?php

class Model_Fact extends ORM implements ISearchable
{
    protected $_route = 'fact';
    protected $_belongs_to = array(
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
    );

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'fulltext' => true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'text' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'type' => 'text',
                'label' => 'Текст',
                'fulltext' => true,
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Изображение',
                'params' => array(
                    'need_help' => true,
                ),
            ),
            'date' => array(
                'edit' => true,
                'head' => false,
                'search' => false,
                'type' => 'datetime',
                'label' => 'Дата публикации',
            ),
            'is_published' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Отображать на сайте',
                'params' => array(
                    'options' => array(
                        1 => 'Отображать',
                        2 => 'Не отображать'
                    )
                )
            ),
            'sef' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ',
            ),
            'sef_ru' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на русском языке',
            ),
            'sef_en' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на английском языке',
            ),
            'sef_kz' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на казахском языке',
            ),
        );
    }

    public function rules()
    {
        return array(
            'title_ru' => array(
                array('not_empty'),
            ),
            'text_ru' => array(
                array('not_empty'),
                array('ORM::alt_image', array('text_ru' => $this->text_ru))
            ),
            'text_kz' => array(
                array('ORM::alt_image', array('text_kz' => $this->text_kz))
            ),
            'text_en' => array(
                array('ORM::alt_image', array('text_en' => $this->text_en))
            ),
            'date' => array(
                array('not_empty'),
                array('date')
            ),
        );
    }

    public function get_field($field)
    {
        if ($field == 'title') {
            return Text::limit_chars(strip_tags($this->title), 100, " ...");
        }
        return parent::get_field($field);
    }


    public function filters()
    {
        return array(
            'title_ru' => array(
                array('strip_tags')
            ),
            'title_en' => array(
                array('strip_tags')
            ),
            'title_kz' => array(
                array('strip_tags')
            ),
            'text_ru' => array(
                array('ORM::clear_html')
            ),
            'text_en' => array(
                array('ORM::clear_html')
            ),
            'text_kz' => array(
                array('ORM::clear_html')
            ),
            'date' => array(
                array('Date::formatted_time'),
            ),
            'sef' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_ru')),
            ),
            'sef_ru' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_ru')),
            ),
            'sef_en' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_en')),
            ),
            'sef_kz' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title_kz')),
            ),
        );
    }

    public function view_url($uri = false, $lang = null)
    {
        if (Register::instance()->search) {
            if ($this->{'sef_'.$lang})
                $_uri = '/gallery/' . $this->{'sef_' . $lang};
                return $uri ? $_uri : URL::site($_uri);
        }
        return 'confederation/infocenter/fact/' . $this->{'sef_' . I18n::$lang};
    }

    public function map_url($lang = null)
    {
        $_uri = '/infocenter/' . $this->_route . '/' . $this->{'sef_' . $lang};
        return $_uri;
    }

    public function actions($user)
    {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
            array(
                'title' => 'Просмотр',
                'uri' => $this->view_url()
            ),
        );
        return $menu;
    }
}