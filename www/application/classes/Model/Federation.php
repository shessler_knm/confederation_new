<?php

class Model_Federation extends ORM
{
    protected $_route = 'federation';

    protected $actions = array('governance', 'structure', 'history', 'contact');

    protected $_has_many = array(
        'teams' => array(
            'model' => 'Team',
            'foreign_key' => 'federation_id'
        ),
        'photovideos' => array(
            'model' => 'Photovideo',
            'through' => 'federations_photovideos'
        ),
        'players' => array(
            'model' => 'Player',
            'foreign_key' => 'federation_id'
        ),
        'results' => array(
            'model' => 'Result',
            'foreign_key' => 'federation_id'
        ),
        'coaches' => array(),
        'competitions' => array(
            'model' => 'Competition',
            'foreign_key' => 'federation_id'
        ),
        'governance' => array(
            'model' => 'Fedgovernance',
            'foreign_key' => 'federation_id',
        ),
        'referees' => array(),
        'sections' => array(
            'model' => 'Section',
            'foreign_key' => 'federation_id'
        ),
        'structures' => array(),
        'news' => array(
            'model' => 'News',
            'through' => 'federations_news',
        ),
        'events' => array(
            'model' => 'Event',
            'through' => 'events_federations'
        ),
        'photos' => array(
            'model' => 'Photo',
            'through' => 'federations_photos',
        ),
        'videos' => array(
            'model' => 'Video',
            'through' => 'federations_videos',
        ),
        'menu' => array(
            'model' => 'Menu',
            'through' => 'federations_menus',
        ),
    );
    protected $_belongs_to = array(
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
    );

    protected function get_fields_description()
    {
        return array(
            'title' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'contacts' => array(
                'edit' => true,
                'head' => false,
                'search' => true,
                'type' => 'text',
                'label' => 'Контакты',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Лого федерации',
                'params' => array(
                    'need_help' => true,
                ),
            ),
            'aboutfed' => array(
                'edit' => true,
                'head' => false,
//                'search'=>true,
                'type' => 'text',
                'label' => 'О федерации',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'history' => array(
                'edit' => true,
                'head' => false,
//                'search'=>true,
                'type' => 'text',
                'label' => 'История',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'referees' => array(
                'type' => 'embeded',
                'label' => 'Судьи',
                'edit' => true,
            ),
            'sef' => array(
                'type' => 'strings',
                'label' => 'URL',
                'edit' => false,
            ),
            'structure' => array(
                'edit' => true,
                'head' => false,
//                'search'=>true,
                'type' => 'text',
                'label' => 'Структура',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
        );
    }

    public function rules()
    {
        return array(
            'title_ru' => array(
                array('not_empty')
            ),
            'contacts_ru' => array(
                array('not_empty'),
                array('ORM::alt_image', array('contacts_ru' => $this->contacts_ru))
            ),
            'history_ru' => array(
                array('not_empty'),
                array('ORM::alt_image', array('text' => $this->history_ru))
            ),
            'aboutfed_ru' => array(
                array('not_empty'),
                array('ORM::alt_image', array('text' => $this->aboutfed_ru))
            ),
            'structure_ru' => array(
                array('not_empty'),
                array('ORM::alt_image', array('text' => $this->structure_ru))
            ),
            'contacts_en' => array(
                array('ORM::alt_image', array('text' => $this->contacts_en))
            ),
            'history_en' => array(
                array('ORM::alt_image', array('text' => $this->history_en))
            ),
            'aboutfed_en' => array(
                array('ORM::alt_image', array('text' => $this->aboutfed_en))
            ),
            'structure_en' => array(
                array('ORM::alt_image', array('text' => $this->structure_en))
            ),
            'contacts_kz' => array(
                array('ORM::alt_image', array('text' => $this->contacts_kz))
            ),
            'history_kz' => array(
                array('ORM::alt_image', array('text' => $this->history_kz))
            ),
            'aboutfed_kz' => array(
                array('ORM::alt_image', array('text' => $this->aboutfed_kz))
            ),
            'structure_kz' => array(
                array('ORM::alt_image', array('text' => $this->structure_kz))
            ),
//            'photo'=>array(
//                array('Storage::set_null')
//            ),
            'sef' => array(
                array('not_empty')
            )
        );
    }

    public function get_field($field)
    {
        if ($field == 'title') {
            return Text::limit_chars(strip_tags($this->title), 100, " ...");
        }

        return parent::get_field($field);
    }

    public function filters()
    {
        return array(
            'title_ru' => array(
                array('strip_tags', array(':value', '<br>'))
            ),
            'title_en' => array(
                array('strip_tags', array(':value', '<br>'))
            ),
            'title_kz' => array(
                array('strip_tags', array(':value', '<br>'))
            ),
            'contacts_ru' => array(
                array('ORM::clear_html')
            ),
            'contacts_en' => array(
                array('ORM::clear_html')
            ),
            'contacts_kz' => array(
                array('ORM::clear_html')
            ),
            'history_ru' => array(
                array('ORM::clear_html')
            ),
            'history_en' => array(
                array('ORM::clear_html')
            ),
            'history_kz' => array(
                array('ORM::clear_html')
            ),
            'aboutfed_ru' => array(
                array('ORM::clear_html')
            ),
            'aboutfed_en' => array(
                array('ORM::clear_html')
            ),
            'aboutfed_kz' => array(
                array('ORM::clear_html')
            ),
            'sef' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title')),
            )
        );
    }

    public function edit_url($uri = false)
    {
        if (!$this->loaded()) {
            return false;
        }
        return parent::edit_url($uri);
    }

    public function view_url($uri = false)
    {
        return URL::site($this->sef . '/history');
    }

    public function map_url($uri = false, $action = null)
    {
        return URL::site($this->sef . '/federation/', $action);
    }

    public function view_uri($uri = false)
    {
        $_uri = "wpoint/map/{$this->sef}";
        return $uri ? $_uri : URL::site($_uri);
    }

    public function delete_url($uri = false)
    {
        return false;
    }

    //возвращает экшены контроллера Federation, отвечающие за отображения полей модели
    public function functions()
    {
        return $this->actions;
    }

    public function actions($user)
    {
        $submenu = array();
        $submenu[0] = array(
            'title' => 'Структура федерации',
            'uri' => URL::site($this->sef . '/structure')
        );
        $submenu[1] = array(
            'title' => 'История федерации',
            'uri' => URL::site($this->sef . '/history')
        );
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Просмотр',
                'type' => 'submenu',
                'submenu' => $submenu
            ),
        );
        return $menu;
    }

    public function select_options($id = 'id', $title = null, $order_by = 'ASC')
    {
        $array = parent::select_options($id, $title, $order_by); // TODO: Change the autogenerated stub
        return array_map('strip_tags', $array);
    }

    public function title()
    {
        return strip_tags(parent::title());
    }
}