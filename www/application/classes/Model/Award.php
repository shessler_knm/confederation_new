<?php
class Model_Award extends ORM
{
    protected $_has_many = array(
        'users' => array(
            'model' => 'User',
            'through' => 'user_awards',
            'foreign_key' => 'award_id'
        ),
    );
    protected $_route='award';
    protected function get_fields_description(){
        return array(
            'id'=>array(
                'head'=>true,
                'label'=>'#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'strings',
                'label' => 'Город',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'type' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'strings',
                'label' => 'Тип',
            ),

        );
    }

    public function rules()
    {
        return array(
            'title_ru' => array(
                array('not_empty'),
            ),
            'type' => array(
                array('not_empty'),
            ),

        );
    }

    public function filters()
    {
        return array(
            'title_ru'=>array(
                array('strip_tags')
            ),
            'title_kz'=>array(
                array('strip_tags')
            ),
            'title_en'=>array(
                array('strip_tags')
            ),
        );
   }

}