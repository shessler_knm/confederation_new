<?php

class Model_Key extends ORM
{
    protected $_route = 'key';
    protected $_has_many=array(
        'values' => array(
            'model' => 'Value',
            'foreign_key' => 'key_id'
        ),
    );
}