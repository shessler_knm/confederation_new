<?php

class Model_Feed extends ORM
{
    protected $_route = 'feed';
    protected $_belongs_to = array(
        'feedtype' => array(
            'model' => 'Feedtype',
            'foreign_key' => 'type',
        ),
        'photo_s'=>array(
            'model' => 'Storage',
            'foreign_key' => 'value_1',
        ),
        'album'=>array(
            'model' => 'User_Album',
            'foreign_key' => 'value_1',
        ),
        'sub_user'=>array(
            'foreign_key' => 'value_1',
        ),
        'user'=>array(),
    );

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'strings',
            ),
            'user_id' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'strings',
            ),
            'date' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'date',
            ),
            'type' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
            ),
            'value_1' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
            ),
        );
    }



//    public function get_field($field)
//    {
//        if ($field == 'title') {
//            return Text::limit_chars(strip_tags($this->title), 100, " ...");
//        }
//        if ($field == 'city_id') {
//           return ORM::factory('Category',$this->city)->title_ru;
//        }
//        if ($field == 'federations') {
//            $results = array();
//            foreach ($this->federations->find_all() as $row) {
//                $results[] = $row->title_ru;
//            }
//            $results = implode('<br>', $results);
//            return $results;
//        }
//
//       if ($field == 'sport') {
//            $results = array();
//            foreach ($this->categories->find_all() as $row) {
//                $results[] = $row->title_ru;
//            }
//            $results = implode('<br>', $results);
//            return $results;
//        }
//        return parent::get_field($field);
//    }
    public function filters()
    {
        return array(
            'text_ru' => array(
                array('ORM::clear_html')
            ),
            'text_en' => array(
                array('ORM::clear_html')
            ),
            'text_kz' => array(
                array('ORM::clear_html')
            ),
            'sef' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title')),
            )
        );
    }

    public function actions($user)
    {
        $submenu = array();
        foreach ($this->federations->find_all() as $fed) {
            $submenu[] = array(
                'title' => $fed->title,
                'uri' => URL::site($fed->sef . '/event/view/' . $this->id)
            );
        }
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
            array(
                'title' => 'Просмотр',
                'type' => 'submenu',
                'submenu' => $submenu
            ),
        );
        return $menu;
    }

    public function view_url($uri = false)
    {
        $_uri = $this->_route . "/view/{$this->id}";
        if (Register::instance()->search) {
            $_uri = '/' . $this->_route . '/view/' . $this->id;
        }
        return $uri ? $_uri : URL::site($_uri);
    }
    public function event_feed($user,$sef){
        $subs=array();
        $subscribers = ORM::factory('Subscribe')->where('user_id', '=', $user)->find_all();
        $model = ORM::factory('User',array('id'=>$this->user_id));
        foreach ($subscribers as $sub)
        {
            $subs[] = $sub->target_id;
        }
        $subs[] = $user;
        $feedtype = ORM::factory('Feedtype')->where('sef', '=', $sef)->find();
        $text = $feedtype->text;
        preg_match_all('/:([a-zA-Z_]+):/',$text,$vars);
        if ($vars[1]){
                foreach ($vars[1] as $var){
                    $$var = call_user_func(array($model,'get_'.$var));
                    $text = strtr($text,array(':'.$var.':'=>$$var));
                }
            }
        return $text;
    }
    static function generate_event($user_id, $target_id, $model, $sef)
    {

        $feedtype = ORM::factory('Feedtype', array('sef' => $sef));
        if ($feedtype->loaded()) {
            $event = ORM::factory('Feed');
            $event->user_id = $user_id;
            $event->target_id = $target_id;
            $event->model = $model;
            $event->type = $feedtype->id;
            $event->date = date('Y-m-d');
            $event->save();
        }
    }


    //методы переменных в шаблонах



}