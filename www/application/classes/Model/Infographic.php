<?php
class Model_Infographic extends ORM
{

    protected $_route = 'infographic';
    protected $_belongs_to = array(
        'photoru_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photoru'
        ),
        'photoen_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photoen'
        ),
        'photokz_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photokz'
        ),

    );

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
//            'text'=>array(
//                'edit'=>true,
//                'head'=>false,
//                'search'=>true,
//                'type'=>'text',
//                'label'=>'Текст',
//                'params'=>array(
//                    'widget'=>'multilang',
//                ),
//            ),
            'photoru' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Инфографика на русском',
            ),
            'photoen' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Инфографика на английском',
            ),
            'photokz' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Инфографика на казахском',
            ),
            'sef' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ',
            ),
            'orientation' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Ориентация',
                'params' => array(
                    'options' => array(
                        '1' => 'Книжная',
                        '2' => 'Альбомная'
                    )
                )
            )
//            'height'=>array(
//                'edit'=>true,
//                'type'=>'strings',
//                'label'=>'Высота'
//            ),
//            'weight'=>array(
//                'edit'=>true,
//                'type'=>'strings',
//                'label'=>'Ширина'
//            ),
        );
    }

    public function rules()
    {
        return array(
            'title_ru' => array(
                array('not_empty'),
            ),
            'photoru' => array(
                array('not_empty'),
            ),
            'photoen'=>array(
                array('not_empty'),
            ),
            'photokz'=>array(
                array('not_empty'),
            ),
//            'text_ru' => array(
//                array('not_empty')),
        );
    }

    public function get_field($field)
    {
        if ($field == 'title') {
            return Text::limit_chars(strip_tags($this->title), 100, " ...");
        }
//        if ($field == 'text') {
//            return Text::limit_chars(strip_tags($this->text), 200, " ...");
//        }
        return parent::get_field($field);
    }

    public function filters()
    {
        return array(
            'title_ru' => array(
                array('strip_tags')
            ),
            'title_en' => array(
                array('strip_tags')
            ),
            'title_kz' => array(
                array('strip_tags')
            ),
//            'text_ru'=>array(
//                array('ORM::clear_html')
//            ),
//            'text_en'=>array(
//                array('ORM::clear_html')
//            ),
//            'text_kz'=>array(
//                array('ORM::clear_html')
//            ),
            'sef' => array(
                array('ORM::filter_sef', array(':value', ':model', 'title')),
            )
        );
    }
    public function view_url($uri = false)
    {
        $_uri = '';
        $lang_route = I18n::$lang!='kz'?I18n::$lang.'/':'/';
        if($this->loaded()){
            $_uri = $lang_route.'confederation/info/'.$this->_route.'/view/'.$this->id;
        }
        return $uri ? $_uri : URL::site($_uri);
    }

    public function actions($user) {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
            array(
                'title' => 'Просмотр',
                'uri' => $this->view_url()
            ),
        );
        return $menu;
    }
}

?>
