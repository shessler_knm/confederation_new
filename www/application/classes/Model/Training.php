<?php

class Model_Training extends ORM
{
    protected $_belongs_to = array(
        'program' => array(
            'model' => 'Program',
            'foreign_key' => 'program_id'
        ),
    );

//    public function filters()
//    {
//        return array(
//            'date' => array(
//                array('Date::formatted_time', array(':value', 'Y-m-d'))
//            ),
//            'spend_time' => array(
//                array('Date::formatted_time', array(':value', 'H:i:s'))
//            ),
//        );
//    }


    public function rules()
    {
        return array(
            'date' => array(
                array('not_empty'),
                array('date')
            ),
            'spend_min' => array(
                array('not_empty'),
            ),
        );
    }

}