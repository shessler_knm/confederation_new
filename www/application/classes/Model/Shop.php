<?php
class Model_Shop extends ORM
{
    protected $_belongs_to=array(
        'city'=>array(
            'model'=>'City',
            'foreign_key'=>'city_id'
        ),
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
    );
    protected $_route='shop';
    protected function get_fields_description(){
        return array(
            'id'=>array(
                'head'=>true,
                'label'=>'#',
            ),
            'title'=>array(
                'edit'=>true,
                'head'=>true,
                'search'=>true,
                'type'=>'strings',
                'label'=>'Название магазина',
                'params'=>array(
                    'widget'=>'multilang',
                ),
            ),
            'description'=>array(
                'edit'=>true,
                'head'=>false,
                'search'=>true,
                'type'=>'text',
                'label'=>'Описание',
                'params'=>array(
                    'widget'=>'multilang',
                ),
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Изображение',
                'params' => array(
                    'need_help' => true,
                ),
            ),
            'city_id'=>array(
                'edit'=>true,
                'head'=>true,
                'search'=>true,
                'type'=>'select',
                'label'=>'Город',
                'params' => array(
                    'options' => ORM::factory('City')->select_options('id', 'title_ru'),
                )
            ),
            'address'=>array(
                'edit'=>true,
                'head'=>true,
//                'search'=>true,
                'type'=>'strings',
                'label'=>'Адрес',
            ),
            'telephone'=>array(
                'edit'=>true,
                'head'=>true,
//                'search'=>true,
                'type'=>'strings',
                'label'=>'Телефон',
            ),
            'email'=>array(
                'edit'=>true,
                'head'=>true,
                'search'=>true,
                'type'=>'strings',
                'label'=>'Email',
            ),
            'sef'=>array(
                'edit'=>true,
                'head'=>false,
                'type'=>'strings',
                'label'=>'ЧПУ',
            ),
        );
    }

    public function rules(){
        return array(
            'title_ru'=>array(
                array('not_empty')
            ),
            'description_ru'=>array(
                array('not_empty')
            ),
//            'email' => array(
//                array('not_empty'),
//                array('email'),
//                array('min_length', array(':value', 6)),
//                array(array($this, 'unique'), array('email', ':value'))
//            ),
        );
    }

    public function get_field($field)
    {
        if ($field == 'city_id') {
            return ORM::factory('City',$this->city_id)->title_ru;
        }
        if ($field == 'title') {
            return Text::limit_chars(strip_tags($this->title),100, " ...");
        }
        return parent::get_field($field);
    }
    public function filters()
    {
        return array(
            'title_ru'=>array(
                array('strip_tags')
            ),
            'title_en'=>array(
                array('strip_tags')
            ),
            'title_kz'=>array(
                array('strip_tags')
            ),
            'telephone'=>array(
                array('strip_tags')
            ),
            'sef'=>array(
                array('ORM::filter_sef',array(':value',':model','title_ru')),
            )
        );
    }

    public function actions($user) {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
            array(
                'title' => 'Просмотр',
                'uri' => $this->view_url()
            ),
        );
        return $menu;
    }
}