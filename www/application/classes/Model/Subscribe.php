<?php

class Model_Subscribe extends ORM
{
    protected $_route = 'subscribe';

    public function find_subs($user,$target){
        return ORM::factory('Subscribe')->where('user_id','=',$user)->where('target_id','=',$target)->find();
    }
    //Получение полного имени подписанного
    public function get_target_name(){
        return ORM::factory('User',array('id'=>$this->target_id))->get_full_name();
    }
    //Получение полного имени подписчика
    public function get_subscriber_name(){
        return ORM::factory('User',array('id'=>$this->user_id))->get_full_name();
    }
}