<?php

class Model_Banner extends ORM
{
    protected $_route = 'banner';
    protected $_belongs_to = array(
        'photo_ru_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo_ru'
        ),
        'photo_en_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo_en'
        ),
        'photo_kz_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo_kz'
        ),
        'image_ru_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'image_ru',
        ),
        'image_en_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'image_en',
        ),
        'image_kz_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'image_kz',
        ),

    );
    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Не цветное изображение',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'image' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Цветное изображение',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'partner' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'select',
                'label' => 'Партнер',
                'params' => array(
                    'options' => array(
                        1 => 'Реклама',
                        2 => 'Партнер'
                    )
                )
            ),
            'url' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'URL',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'category' => array(
                'edit' => true,
                'head' => true,
                'search'=>true,
                'type' => 'select',
                'label' => 'Категории',
                'params' => array(
                    'options' => array(
                        '0'=>'Не указано',
                        '1' => 'Титульные спонсоры',
                        '2' => 'Спонсоры',
                        '3'=> 'Информационные партнеры',
                    )
                ),
            ),
            'priority' => array(
                'edit' => true,
                'type' => 'strings',
                'label' => 'Приоритет',
            ),
            'main' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Вывод на главной',
                'params' => array(
                    'options' => array(
                        1=>'Не отображать',
                        2=>'Отобразить'
                    )
                )
            ),
        );
    }

    public function rules()
    {
        return array(
            'title_ru' => array(
                array('not_empty'),
            ),
            'partner'=>array(
                array('not_empty'),
            ),
            'url_ru'=>array(
                array('Valid::url'),
            ),
        );
    }

    public function get_field($field)
    {
        if ($field == 'partner') {
            return $this->partner==1 ? 'Реклама' : 'Партнер';
        }
        if ($field == 'title') {
            return Text::limit_chars(strip_tags($this->title), 100, " ...");
        }
        if($field == 'category'){
            switch ($this->category) {
                case 1: $r = 'Титульные спонсоры';break;
                case 2: $r = 'Спонсоры';break;
                case 3: $r = 'Информационные партнеры';break;
                default: $r = 'Не указано';break;
            }
            return $r;
        }
        return parent::get_field($field);
    }

    public function filters()
    {
        return array(
            'title_ru'=>array(
                array('strip_tags')
            ),
            'title_en'=>array(
                array('strip_tags')
            ),
            'title_kz'=>array(
                array('strip_tags')
            ),
        );
    }
    public function actions($user) {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
        );
        return $menu;
    }
}