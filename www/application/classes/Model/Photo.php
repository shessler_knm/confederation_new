<?php
class Model_Photo extends ORM_Embeded implements Synchronize_ISynch
{
    protected $_parent_field="album_id";
    protected $_parent_model="Album";

    protected $_route = 'photo';
    protected $_created_column = array('column' => 'date', 'format' => 'Y-m-d H:i:s');
    protected $_belongs_to = array(
        'album' => array(
            'model' => 'Album',
            'foreign_key' => 'album_id',
        ),
    );

    public function object_fields() {
        return $this->_object;
    }

    public function get_field($field) {
        if($field == 'minisrc'){
            return "<img src='".$this->minisrc."' />";
        }
//        if($field == 'fed_id'){
//            $fed = ORM::factory('Federation')->find_all()->as_array('id','title_ru');
//            return (!is_array($fed)) ? $fed : 'Не обработано';
//        }
        return parent::get_field($field);
    }
    
    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',1
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'minisrc'=>array(
                'edit'=>true,
                'head'=>true,
                'type'=>'readonly_text',
                'label'=>'Миниатюра',
                'params'=>array(
                    'value'=>'<img src="'.$this->minisrc.'" />'
                 )
            ),
            'jacket' => array(
                'edit' => true,
                'type' => 'select',
                'label' => 'Использовать как обложку альбома ?',
                'params' => array(
                    'options' => array(1 => 'Да', 2 => 'Нет'),
                ),
            ),
            'date' => array(
                'edit' => true,
                'head' => true,
                'type' => 'date',
                'label'=> 'Дата'
            ),
        );
    }

    public function actions($user) {
       $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
        );
        return $menu;
    }
    public function filters()
    {
        return array(
            'title_ru'=>array(
                array('strip_tags')
            ),
            'title_en'=>array(
                array('strip_tags')
            ),
            'title_kz'=>array(
                array('strip_tags'),
            ),
            'jacket'=>array(
                array(array($this,'clear_jackets')),
            ),
            'date' => array(
                array('Date::formatted_time'),
            ),
        );
    }

    public function clear_jackets($val) {
        if ($val == 1) {
            DB::update('photos')
                ->set(array('jacket' => 2))
                ->where('album_id','=',$this->album_id)
                ->and_where('jacket','=',1)->execute();
        }
        return $val;
    }
}