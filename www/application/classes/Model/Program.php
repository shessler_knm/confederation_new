<?php

class Model_Program extends ORM
{

    protected $_route = 'program';
    protected $_belongs_to = array(
        'mission' => array(
            'model' => 'Mission',
            'foreign_key' => 'mission_id'
        ),
        'user' => array(
            'model' => 'User',
            'foreign_key' => 'user_id'
        ),
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
        'parent' => array(
            'model' => 'Program',
            'foreign_key' => 'parent_id'
        ),
    );

    protected $_has_many = array(
        'categories' => array(
            'model' => 'Category',
            'through' => 'program_categories'
        ),
        'exercises' => array(
            'model' => 'Exercise',
            'through' => 'program_exercises',
        ),
        'logs' => array(
            'model' => 'Program_Log',
            'foreign_key' => 'program_id',
        ),
        'program_exercises' => array(
            'model' => 'Program_Exercise',
//            'foreign_key' => 'program_id',
        ),
        'program_scores' => array(
            'model' => 'Program_Score',
            'foreign_key' => 'target_id'
        ),
        'program_forks' => array(
            'model' => 'Program',
            'foreign_key' => 'parent_id'
        ),
        'comments' => array(
            'model' => 'Comment',
            'foreign_key' => 'target_id'
        ),
        'user_logs' => array(
            'model' => 'User_Log',
            'foreign_key' => 'program_id',
        ),
    );

//    public function weekdays($day='1')
//    {
//        $weekDays = array(
//            1 => __('Понедельник'),
//            2 => __('Вторник'),
//            3 => __('Среда'),
//            4 => __('Четверг'),
//            5 => __('Пятница'),
//            6 => __('Суббота'),
//            7 => __('Воскресенье')
//        );
//        return Arr::get($weekDays,$day);
//    }

    public $weekDays = array(
        1 => 'Понедельник',
        2 => 'Вторник',
        3 => 'Среда',
        4 => 'Четверг',
        5 => 'Пятница',
        6 => 'Суббота',
        7 => 'Воскресенье',
    );

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'title' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Заголовок',
                'params' => array(
                    'widget' => 'multilang'
                )
            ),
            'text' => array(
                'edit' => true,
                'search' => true,
                'type' => 'text',
                'label' => 'Описание программы',
                'params' => array(
                    'widget' => 'multilang'
                )
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Изображение',
                'params' => array(
                    'need_help' => true,
                    'aspect' => 1.6,
//                    'size' => 200,
                ),
            ),
            'activity' => array(
                'edit' => false,
                'head' => false,
                'label' => 'Активность',
                'type' => 'select',
                'params' => array(
                    'options' => array(
                        0 => 'Не активна',
                        1 => 'Активна'
                    )
                )
            ),
            'mission_id' => array(
                'edit' => true,
                'search' => true,
                'head' => true,
                'type' => 'select',
                'label' => 'Цель программы',
                'params' => array(
                    'options' => ORM::factory('Mission')->select_options('id', 'title_ru'),
                )
            ),
            'place' => array(
                'edit' => true,
                'search' => true,
                'type' => 'select',
                'label' => 'Место',
                'params' => array(
                    'options' => array(
                        'all'=>'Оба варианта',
                        'home' => 'Дом',
                        'gym' => 'Тренажерный зал'
                    )
                )
            ),
            'sex' => array(
                'edit' => true,
                'search' => true,
                'type' => 'select',
                'label' => 'Пол',
                'params' => array(
                    'options' => array(
                        '0' => 'Оба варианта',
                        '1' => 'Муж',
                        '2' => 'Жен'
                    )
                )
            ),
            'categories' => array(
                'edit' => true,
                'type' => 'external_select',
                'label' => 'Части тела',
                'params' => array(
                    'src' => ORM::factory('Category')->get_muscle_external()
                )
            ),
//            'program_exercises' => array(
//                'type' => 'embeded',
//                'label' => 'Упражнения',
//                'edit' => true,
//            ),
        );
    }

//    public function rules()
//    {
//        return array(
//            'title_ru' => array(
//                array('not_empty'),
//            ),
//            'text_ru' => array(
//                array('not_empty')
//            ),
//        );
//    }

    public function view_url($uri = false)
    {
        $_uri = 'privatecoach/' . $this->_route . "/view/{$this->id}";
        return $uri ? $_uri : URL::site($_uri);
    }

    public function actions($user)
    {
        $menu = array(
            array(
                'title' => 'Просмотр',
                'uri' => $this->view_url()
            ),
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
        );
        return $menu;
    }

    public function get_field($field)
    {
        if ($field == 'mission') {
            return $this->mission->title;
        }
        if ($field == 'created_by') {
            if ($this->created_by == 'admin') {
                return 'Родительская';
            } else {
                return 'Пользовательская';
            }
        }
        if ($field == 'activity') {
            switch ($this->activity) {
                case 0:
                    return 'NEW';
                    break;
                case 1:
                    return 'INPROGRESS';
                    break;
                case 2:
                    return 'COMPLETED';
                    break;
            }
        }
        if ($field == 'mission_id') {
            return $this->mission->title_ru;
        }
        if ($field == 'sex') {
            switch ($this->sex) {
                case 0:
                    return 'A';
                    break;
                case 1:
                    return 'M';
                    break;
                case 2:
                    return 'F';
                    break;
            }
        }
        if ($field == 'photo') {
            if ($this->photo) {
                return $this->photo_s->url(true);
            }
        }
        if ($field == 'categories') {
            $data = array();
            foreach ($this->categories->find_all() as $row) {
                $data[] = $row->id;
            }
            return $data;
        }

        return parent::get_field($field);
    }

    public function filters()
    {
        return array(
            'title' => array(
                array('strip_tags')
            ),
            'text' => array(
                array('strip_tags')
            ),
            'start_date' => array(
                array('Date::formatted_time', array(':value', 'Y-m-d'))
            ),
            'end_date' => array(
                array('Date::formatted_time', array(':value', 'Y-m-d'))
            ),
        );
    }

    public function days_as_array()
    {
        $pe = $this->program_exercises->find_all();
        $days = array();
        foreach ($pe as $ex) {
            $days[$ex->week_day] = Arr::get($days, $ex->week_day,
                array(
                    'value' => $ex->week_day,
                    'title' => __($this->weekDays[$ex->week_day]),
                    'day' => $ex->week_day,
                    'exercises' => array()
                )
            );
            $exercise = array('id' => $ex->exercise_id, 'title' => $ex->exercise->title, 'index' => $ex->index_number, 'efforts' => array());
            $efforts = explode(',', $ex->sets);
            foreach ($efforts as $repeat) {
                $exercise['efforts'][] = array('repeat' => $repeat);
            }
            $days[$ex->week_day]['exercises'][] = $exercise;
        }

        return array_values($days);
    }

    public function form_button($cp)
    {
        if ($this->loaded()) {
            $cp->button_link(URL::site('privatecoach/admin/program/edit/' . $this->id), "Редактировать программу", array('class' => 'btn i_btn', 'target' => '_blank'));
        }
    }

    public function end_date($week_days, $format = 'Y-m-d')
    {
        $prev_day = 0;
        $i = 0;
        foreach ($week_days as $day) {
            if ($day == Date::formatted_time($this->start_date($week_days), 'N')) {
                $end_day = $day - $prev_day;
                break;
            }
            $prev_day = $day;
        }
        if (!$end_day) {
            $end_day = max($week_days);
        }
        $dt = new DateTime($this->start_date($week_days));
        $dt->add(new DateInterval('P' . ($this->duration * 7 - $end_day) . 'D'));
        return $dt->format($format);
    }

    public function count_user_logs($user)
    {
        $logs = $user->logs->where('program_id', '=', $this->id)->find_all()->count();
        return $logs;
    }

    public function as_array_ext($fields = array(), $action = 'view')
    {
        $data = parent::as_array_ext($fields);
        $photo = null;
        $week_days = array();
        $program_exercises = array();
        $trainings_array = array();
        $buffer = 0;
        $trainings = ORM::factory('Training')->where('program_id', '=', $this->id)->find_all();
        foreach ($trainings as $training) {
            $trainings_array[$training->date] = $training->spend_min;
        }
        unset($data['mission_id']);
        if ($this->type == 'copy') {
            if ($this->photo) {
                $photo = $this->photo_s->url('http');
            } elseif ($this->parent->photo) {
                $photo = $this->parent->photo_s->url('http');
            }
        } else {
            if ($this->photo) {
                $photo = $this->photo_s->url('http');
            }
        }
        $data['photo'] = $photo;
//            $data['text'] = $this->parent->text;
        $exercises = $this->program_exercises->order_by('week_day', 'ASC')->find_all()->as_array();
        foreach ($exercises as $row) {
            $week_days[$row->week_day] = $row->week_day;
            $program_exercises[$row->week_day][] = $row;
        }
        $scores = $this->program_scores->where('target_id', '=', $this->id)->find_all()->as_array('id', 'score');
        if ($scores) {
            $score = array_sum($scores) / count($scores);
        } else {
            $score = 0;
        }
        $data['score'] = $score;
        $data['target'] = $this->mission->sef;
        $data['subscribers'] = (int)$this->subscribers;

        if ($this->type != 'free' && $this->user_id) {
            if ($week_days) {
                $data['start_date'] = $this->start_date($week_days);
                $data['end_date'] = $this->end_date($week_days);
            } else {
                $data['start_date'] = $this->start_date;
                $data['end_date'] = $this->count_date($this->duration, $this->start_date);
            }
        }
        if ($action == 'new') {
            $data['subscribe_date'] = $this->start_date;
        }
        if ($action == 'view' || $action == 'create_user_program') {
            $current_user = Auth::instance()->get_user();
            $favourite = ORM::factory('Favourite')->where('user_id', '=', $current_user->id)->where('program_id', '=', $this->id)->find();
            $data['is_favourite'] = $favourite->loaded();
            foreach ($exercises as $row) {
                $data['exercises'][] = $row->as_array_ext();
            }
        }
        if ($action == 'current') {
            $counter = -1;
            $data_logs = array();
            $logs = $this->logs->find_all();
            foreach ($logs as $log) {
                $data_logs[$log->exercise_id][$log->user_id][$log->program_id][Date::formatted_time($log->date, 'ymd')] = $log->status;
            }
            foreach ($week_days as $week_day) {
                $prev_day = isset($start_day) ? $start_day : null;
                if ($week_day >= Date::formatted_time($this->start_date($week_days), 'N')) {
                    $start_day = $week_day;
                    break;
                }
            }
            $duration = $start_day == min($week_days) ? $this->duration : $this->duration + 1;
            $all_days = $this->duration * count($week_days);
            $program_days = $this->all_days();
            for ($i = 1; $i <= $duration; $i++) {
                foreach ($week_days as $day) {
                    foreach ($program_exercises[$day] as $row) {
                        if (!(($row->week_day < $start_day && $i == 1)) || ($i == $duration && $day < $prev_day)) {
                            if ($buffer != $row->week_day) {
                                $counter++;
                                $count_date = $this->count_date($i, $this->start_date, $week_days, $row->week_day, 'Y-m-d');
                                $data['days'][$counter] = array(
                                    'date' => $this->count_date($i, $this->start_date, $week_days, $row->week_day),
                                    'day_number' => $row->week_day,
                                    'can_edit' => (($count_date <= date('Y-m-d')) && !array_key_exists($count_date, $trainings_array)),
                                    'spend_time' => Arr::get($trainings_array, $count_date)
                                );
                            }
                            $data['days'][$counter]['exercises'][] = $row->as_array_ext($fields) + array('is_completed' => (bool)$data_logs[$row->exercise_id][$this->user_id][$this->id][$this->count_date($i, $this->start_date, $week_days, $row->week_day, 'ymd')]);
                            $buffer = $row->week_day;
                        }
                    }
                    if (count($data['days']) == $all_days) {
                        break;
                    }
                }

            }


        }
        $data['parent_id'] = $this->parent_id;
        $data['comments_count'] = $this->comments->where('status', '=', 1)->count_all();
        return $data;
    }

    public function count_date($i, $start_date, $week_days = null, $week_day = null, $format = 'Y-m-d')
    {
        if ($week_day) {
            if (Date::formatted_time($this->start_date($week_days), 'N') <= max($week_days) && $i == 1) {
                $day = $week_day - Date::formatted_time($this->start_date($week_days), 'N');
            } else {
                $day = (7 - Date::formatted_time($this->start_date($week_days), 'N') + $week_day) + 7 * ($i - 2);
            }
            $date = new DateTime($this->start_date($week_days));
        } else {
            $day = $start_date + ($i - 1) * 7;
            $date = new DateTime($this->start_date);
        }
        $date->add(new DateInterval('P' . $day . 'D'));
        $result = $date->format($format);
        return $result;
    }

    public function start_date($week_days)
    {
        $dif_days = 0;
        $date = new DateTime($this->start_date);
        if (max($week_days) >= Date::formatted_time($this->start_date, 'N')) {
            foreach ($week_days as $day) {
                if ($day >= Date::formatted_time($this->start_date, 'N')) {
                    $dif_days = $day - Date::formatted_time($this->start_date, 'N');
                    break;
                }
            }
        } else {
            $dif_days = 7 + min($week_days) - Date::formatted_time($this->start_date, 'N');
        }
        $date->add(new DateInterval('P' . $dif_days . 'D'));
        $result = $date->format('Y-m-d');
        return $result;
    }

    public function all_days()
    {
        $week_days = array();
        $data = array();
        $program_exercises = array();
        $counter = -1;
        $buffer = 0;
        $exercises = $this->program_exercises->order_by('week_day', 'ASC')->find_all()->as_array();

        foreach ($exercises as $row) {
            $week_days[$row->week_day] = $row->week_day;
            $program_exercises[$row->week_day][] = $row;
        }
        foreach ($week_days as $week_day) {
            $prev_day = isset($start_day) ? $start_day : null;
            if ($week_day >= Date::formatted_time($this->start_date($week_days), 'N')) {
                $start_day = $week_day;
                break;
            }
        }
        $duration = $start_day == min($week_days) ? $this->duration : $this->duration + 1;
        $all_days = $this->duration * count($week_days);
        for ($i = 1; $i <= $duration; $i++) {
            foreach ($week_days as $day) {
                foreach ($program_exercises[$day] as $row) {
                    if (!(($row->week_day < $start_day && $i == 1)) || ($i == $duration && $day < $prev_day)) {
                        if ($buffer != $row->week_day) {
                            $counter++;
                            $data[] = $this->count_date($i, $this->start_date, $week_days, $row->week_day, 'Y-m-d');
                        }
                        $buffer = $row->week_day;
                    }
                }
                if (count($data['days']) == $all_days) {
                    break;
                }
            }

        }
        return $data;
    }
}