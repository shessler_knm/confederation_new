<?php
class Model_Coach extends ORM implements ISearchable, Synchronize_ISynch
{
    protected $_table_name = 'coaches';
    protected $_belongs_to = array(
        'federation' => array(),
        'team' => array(
            'Model'=>'Team',
            'foreign_key'=>'team_id'
        ),
        'photo_s' => array(
            'model' => 'Storage',
            'foreign_key' => 'photo'
        ),
    );
    protected $_route = 'coach';

    protected function get_fields_description()
    {
        return array(
            'id' => array(
                'head' => true,
                'label' => '#',
            ),
            'firstname' => array(
                'edit' => true,
                'head' => true,
                'fulltext' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Имя',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'middlename' => array(
                'edit' => true,
                'head' => true,
                'fulltext' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Отчество',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'lastname' => array(
                'edit' => true,
                'head' => true,
                'fulltext' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Фамилия',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'post' => array(
                'edit' => true,
                'head' => true,
                'search' => true,
                'type' => 'strings',
                'label' => 'Пост',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),

            'biography' => array(
                'edit' => true,
                'head' => false,
//                'search'=>true,
                'type' => 'text',
                'fulltext' => true,
                'label' => 'Биография',
                'params' => array(
                    'widget' => 'multilang',
                ),
            ),
            'photo' => array(
                'edit' => true,
                'type' => 'image',
                'label' => 'Изображение',
                'params' => array(
                    'need_help' => true,
                    'aspect' => 1,
                    'size' => 200,
                ),
            ),
            'federation_id' => array(
                'edit' => true,
                'search' => true,
                'head' => true,
                'type' => 'select',
                'label' => 'Федерация',
                'params' => array(
                    'options' => ORM::factory('Federation')->select_options('id', 'title_ru'),
                )
            ),
            'team_id' => array(
                'edit' => true,
                'head' => true,
//                'search'=>true,
                'type' => 'select',
                'label' => 'Команда',
                'params' => array(
                    'options' => ORM::factory('Team')->select_options('id', 'title_ru'),
//                    'disabled'=>'disabled'
                )
            ),
            'is_published' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Отображать на сайте',
                'params' => array(
                    'options' => array(
                        1 => 'Отображать',
                        2 => 'Не отображать'
                    )
                )
            ),
            'rio' => array(
                'edit' => true,
                'head' => false,
                'type' => 'select',
                'label' => 'Рио 2016',
                'params' => array(
                    'options' => array(
                        0 => 'Нет',
                        1 => 'Да'
                    )
                )
            ),
            'sef' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ',
            ),
            'sef_ru' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на русском языке',
            ),
            'sef_en' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на английском языке',
            ),
            'sef_kz' => array(
                'edit' => true,
                'head' => false,
                'type' => 'strings',
                'label' => 'ЧПУ на казахском языке',
            ),
        );
    }

    public function rules()
    {
        return array(
            'firstname_ru' => array(
                array('not_empty'),
            ),
            'lastname_ru' => array(
                array('not_empty'),
            ),
            'post_ru' => array(
                array('not_empty')
            ),
            'federation_id' => array(
                array('not_empty'),
            ),
        );
    }

    public function get_field($field)
    {
        if ($field == 'federation_id') {
            return ORM::factory('Federation', $this->federation_id)->title;
        }
        if ($field == 'team_id') {
            return ORM::factory('Team', $this->team_id)->title;
        }

        return parent::get_field($field);
    }

    public function select_full_name()
    {
        $this->select(array(Db::expr('CONCAT(lastname_ru," ",firstname_ru," ",middlename_ru)'), 'fullname'));
        return $this;
    }

    public function get_full_name($lang = NULL)
    {
        if ($lang !== NULL) {
            $firstname = mb_substr($this->{'firstname_' . $lang}, 0, 1);
            $firstname = str_pad($firstname, 3, ".");
            if ($this->{'middlename_' . $lang}) {
                $middlename = mb_substr($this->{'middlename_' . $lang}, 0, 1);
                $middlename = str_pad($middlename, 3, ".");
                $result = $this->{'lastname_' . $lang} . " " . $firstname . " " . $middlename;
            } else {
                $result = $this->lastname . " " . $firstname;
            }
            return $result;
        } else {
            $firstname = mb_substr($this->firstname, 0, 1);
            $firstname = str_pad($firstname, 3, ".");
            if ($this->middlename) {
                $middlename = mb_substr($this->middlename, 0, 1);
                $middlename = str_pad($middlename, 3, ".");
                $result = $this->lastname . " " . $firstname . " " . $middlename;
            } else {
                $result = $this->lastname . " " . $firstname;
            }
            return $result;
        }
    }

    public function filters()
    {
        return array(
            'firstname_ru' => array(
                array('strip_tags')
            ),
            'firstname_en' => array(
                array('strip_tags')
            ),
            'firstname_kz' => array(
                array('strip_tags')
            ),
            'lastname_ru' => array(
                array('strip_tags')
            ),
            'lastname_en' => array(
                array('strip_tags')
            ),
            'lastname_kz' => array(
                array('strip_tags')
            ),
            'middlename_ru' => array(
                array('strip_tags')
            ),
            'middlename_en' => array(
                array('strip_tags')
            ),
            'middlename_kz' => array(
                array('strip_tags')
            ),
            'post_ru' => array(
                array('strip_tags')
            ),
            'post_en' => array(
                array('strip_tags')
            ),
            'post_kz' => array(
                array('strip_tags')
            ),
            'biography_ru' => array(
                array('ORM::clear_html')
            ),
            'biography_en' => array(
                array('ORM::clear_html')
            ),
            'biography_kz' => array(
                array('ORM::clear_html'),
            ),
            'sef' => array(
                array('ORM::full_name_filter_sef', array(':value', $this->get_full_name())),
            ),
            'sef_ru' => array(
                array('ORM::full_name_filter_sef', array(':value', $this->get_full_name('ru'))),
            ),
            'sef_en' => array(
                array('ORM::full_name_filter_sef', array(':value', $this->get_full_name('en'))),
            ),
            'sef_kz' => array(
                array('ORM::full_name_filter_sef', array(':value', $this->get_full_name('kz'))),
            ),
        );
    }

    public function view_url($uri = false)
    {
        $_uri = '';
        if (Register::instance()->search) {
            $_uri = '/coach/person/';
            return $_uri;
        } elseif ($this->loaded()) {
            $_uri = I18n::$lang != 'kz' ? I18n::$lang . '/' . $this->federation->sef . '/coach/person/' . $this->{'sef_' . I18n::$lang} : $this->federation->sef . '/coach/person/' . $this->{'sef_' . I18n::$lang};
        }
        return $uri ? $_uri : URL::site($_uri);
    }

    public function map_url($lang = null)
    {
        $_uri = '/' . $this->_route . '/person/' . $this->{'sef_' . $lang};
        return $_uri;
    }

    public function actions($user)
    {
        $menu = array(
            array(
                'title' => 'Редактировать',
                'uri' => $this->edit_url()
            ),
            array(
                'title' => 'Удалить',
                'uri' => $this->delete_url()
            ),
            array(
                'title' => 'Просмотр',
                'uri' => $this->view_url()
            ),
        );
        return $menu;
    }

    public function title()
    {
        $arg = func_get_args();
        if (!count($arg)) {
            return $this->firstname . ' ' . $this->lastname . ' ' . $this->middlename;
        } else {
            return $this->{'firstname_' . $arg[0]} . ' ' . $this->{'lastname_' . $arg[0]} . ' ' . $this->{'middlename_' . $arg[0]};
        }
    }

    public function short_text()
    {
        $arg = func_get_args();
        if (!count($arg)) {
            return $this->biography;
        } else {
            return $this->{'biography_' . $arg[0]};
        }
    }

    public function search_extended_index()
    {
        $ext = array();
        $ext[] = Search::escape_extend('fed_id', $this->federation->sef);
        $ext[] = Search::escape_extend('fed_id', 'confederation');
        return implode('\n', $ext);
    }

    public function form_list_button(Component $cp)
    {
        $cp->button_link(URL::site('admin/coach/synchronize_model'), "Синхронизировать с Федерациями", array('class' => 'btn btn-primary'));
    }
}
