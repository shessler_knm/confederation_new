<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Saidashev Marat
 * Date: 16.08.13
 * Time: 11:46
 * To change this template use File | Settings | File Templates.
 */

class Synchronize_Receiver extends Cms_API {

    public $need_auth = true;

    private $xKey = 's1231sfaqlm$q23#fgmred';

    public function before() {
        if ($this->request->headers('x-key') != $this->xKey) {throw new HTTP_Exception_404();}
        if (!$this->request->headers('x-pass') || !$this->request->headers('x-user')) {throw new HTTP_Exception_404();}
        $pass = $this->request->headers('x-pass');
        $username = $this->request->headers('x-user');
        Auth::instance()->login($username, $pass);

        parent::before();
    }

    /**
     *
     * Методы для массовой синхронизации
     *
     */


    public function create_tasks($collection) {
        $data = array();
        foreach($collection as $key => $item) {
            $model = $item;
            $files = array();
            //Выбераем файлы, для отложенных тасков. ($data['tasks'][])
            foreach($this->arr_fields($model, array('image','file')) as $field => $type) {
                $storage = $model->{$field.'_s'};
                if ($storage->loaded()) {
                    $path = str_replace('\\','/',DOCROOT);
                    $path = rtrim($path,'/').$storage->url();
                    if (!is_file($path)) {
                        continue;
                    }
                    $size = filesize($path);
                    $files[$field] = array(
                        'download_url' => url::site($storage->download_file_url(), 'http'),
                        'filename' => $storage->original_name,
                        'hash' => md5($size)
                    );
                }
            }
            if (count($files) > 0) {
                $data['tasks'][$key]['files'] = $files;
            }

            //Выбираем поля типа text для отложенных тасков.
            $content_files= array();
            $fields_desc = $model->fields_description();
            foreach($this->arr_fields($model, array('text')) as $field => $type) {
                $multi = isset($fields_desc[$field]['params']) AND
                    isset($fields_desc[$field]['params']['widget']) AND
                        $fields_desc[$field]['params']['widget'] == 'multilang';

                if ($multi)
                    $content_files[$field] = array('type' => 'multilang');
                else
                    $content_files[$field] = array('type' => 'nea');

            }
            if (count($content_files) > 0) {
                $data['tasks'][$key]['content_files'] = $content_files;
            }
        }
        return $data;
    }

    /**
     *
     * Методы для единичного сохранения при синхронизации
     * (в принципе не задействованы находяться на "всякий случай")
     *
     */

    public function save_model($model, $raw_values, $extra_valid=NULL) {

        try {
            $model->where('synch_id','=',$raw_values['synch_id'])->find();

            parent::save_model($model, $raw_values, $extra_valid);

            $tasks = $this->request->post('tasks');

            if ($tasks) {
                $s = ORM::factory('Synchronize');
                $s->model_name = $model->table_name();
                $s->uid = $model->id;
                $s->tasks = json_encode($tasks);
                $s->save();
            }

            if ($model instanceof ORM_Embeded && isset($raw_values['parent_model'])) {
                $raw_parent = $raw_values['parent_model'];
                $parent = ORM::factory($model->parent_model());
                $parent->where('synch_id','=',$raw_parent['synch_id'])->find();
                $parent->values($raw_parent);
                $parent->save();
                $model->{$model->parent_field()} = $parent->id;
                $model->save();
            }
        }
        catch (Exception $e) {
                $this->errors += array('RUNTIME ERROR' => $e->getMessage().$e->getLine().$e->getFile());
                if ($e instanceof ORM_Validation_Exception)
                {
                    $this->errors += $e->errors(i18n::lang());
                }
                return $model;
        }

        return $model;
    }

    public function body_to_array() {
        return $this->request->post();
    }

    public function editable_keys($model) {
        return NULL;
    }

    public function csrf_check() {
        return true;
    }

}