<?php

class Rio_Site extends Cms_Controller_Site
{

    public $federations;
    public $federation;
    public $fed_id;
    public $template = 'basic_rio';
    public $ulogin;
    public $full_size = false;
    public $array_langs = array();
    public $breadcrumbs = array();

//    public $partners;

    public function before()
    {
        Register::instance()->users = array();
        if ($this->request->query('site_version') == 'full') {
            Cookie::set('full_size', true);
            $this->full_size = true;
        } elseif ($this->request->query('site_version') == 'mobile') {
            Cookie::delete('full_size');
            $this->full_size = false;
        } else {
            $this->full_size = !!Cookie::get('full_size');
        }
        View::bind_global('full_size', $this->full_size);
        parent::before();

        $this->fed_id = $this->request->param('fed_id');

        if (Register::instance()->federations) { //оптимизировано
            $this->federations = Register::instance()->federations;
            $this->federation = Register::instance()->federation;
        } else {
            $this->federations = ORM::factory('Federation')
                ->where('sef', '!=', $this->fed_id)
                ->order_by('title_ru', 'ASC')
                ->find_all();
            $this->federation = ORM::factory('Federation')
                ->where('sef', '=', $this->fed_id)
                ->find();
            Register::instance()->federations = $this->federations;
            Register::instance()->federation = $this->federation;
        }

        $this->init_langs();
        $this->ulogin = ULogin::widget_ulogin();
        /**
         * потому что по дизайну цитаты на каждой странице
         */
        $quotes = ORM::factory('Quote')
            /*->where('rio', '=', 1)*/
            ->find_all();
        View::bind_global('quotes', $quotes);
        View::bind_global('federations', $this->federations);
        View::bind_global('federation', $this->federation);
        View::bind_global('menu', $this->menu);
        View::bind_global('ulogin', $this->ulogin);
        View::bind_global('conmenu', $this->conmenu);
        View::bind_global('array_langs', $this->array_langs);
        View::bind_global('fed_id', $this->fed_id);
        View::set_global('current_uri', Request::initial()->uri());
//        View::bind_global('partners',$fed_id);
    }


    public function init_langs($model = NULL)
    {
        $this->array_langs['kz'] = Helper::currentURI();
        $this->array_langs['ru'] = '/ru' . Helper::currentURI();
        $this->array_langs['en'] = '/en' . Helper::currentURI();
    }

    public function scripts()
    {
        $arr = parent::scripts()/* + array(
                'jquery' => 'js/lib/jquery-1.9.0.min.js',
                'index' => 'js/index_rio.js',

            )*/;
        if ($this->user && $this->user->loaded()) {
            unset($arr['ulogin']);
        }
        return $arr;
    }

    /* index.js и style.css всегда должны быть последними.
        но сейчас responsive.css должен быть последним, т.к. он должен перетирать основной стиль
    */

    public function styles()
    {
        $arr = parent::styles()/* + array(
                'style' => 'css/style_rio.css',
                'bootstrap' => 'js/lib/bootstrap/css/bootstrap.min.css',
            )*/;

        if ($this->full_size) {
            unset($arr['css/responsive.css']);
            unset($arr['bootstrap-responsive']);
        }

        return $arr;
    }

    public function view_counter($model, $field = 'views')
    {
        $views = $this->check_view_counter($model);

        if ($views !== false) {
            $model->$field += 1;
            try {
                $model->save();
            } catch (Exception $e) {
            }
            Session::instance()->set('view_counter', array_merge($views, array('News' => $model->id)));
        }
    }

    public function check_view_counter($model)
    {
        $view_counter = Session::instance()->get('view_counter', array());
        foreach ($view_counter as $model_name => $_target_id) {
            if ($model_name == 'News' && $_target_id == $model->id) {
                return false;
            }
        }
        return $view_counter;
    }


    public function lang_detection($param = "param", $cookie = true)
    {
        if ($param == "query") {
            $lang = strval($this->request->query('lang'));
        } else {
            $lang = strval($this->request->param('lang'));
        }
        if ($lang AND preg_match("/^(ru|en|kz)$/", $lang)) {
            I18n::lang($lang);
        }
    }

    public static function pagination($model, $config = 'pagination.default')
    {
        $config = Kohana::$config->load($config);
        $config['total_items'] = $model->reset(false)->count_all();
        if ($config['current_page']['source'] == 'route') {
            $pagination = Pagination::factory($config);
            $pagination->route_params(
                $pagination->route_params()
                +
                array(
                    'controller' => strtolower(Request::current()->controller()),
                    'action' => strtolower(Request::current()->action()),
                    'lang' => Request::current()->param('lang'),
                    'fed_id' => Request::current()->param('fed_id'),
                )
            );
        } else {
            $pagination = Pagination::factory($config);
        }
        $model->limit($pagination->{'items_per_page'})->offset($pagination->{'offset'});
        return $pagination;
    }

    public function check_pages($page, $total_pages, $redirect = null)
    {
        if ($page && ($page > $total_pages || preg_match('#[a-zA-Zа-яА-Я]#', $page, $match))) {
            if ($redirect) {
                return 'redirect';
            } else {
                throw new HTTP_Exception_404();
            }
        }
        $current_uri = Request::initial()->current()->uri();
        if (preg_match('#page-1$#', $current_uri)) {
            $this->redirect(preg_replace('#page-1#', '', $current_uri), 301);
        }
    }

    public function check_personal_data($id)
    {
        if ($this->user) {
            if ($this->user->id == $id) {
                if ($this->user->pers_data == 0) {
                    $this->redirect(URL::site('privatecoach/information/personal_data'), 302);
                }
            }
        }
    }

    public function add_crumb($title, $url)
    {
        $this->breadcrumbs[] = array('title' => $title, 'url' => $url);
        View::bind_global('_breadcrumbs', $this->breadcrumbs);
        return $this;
    }
}