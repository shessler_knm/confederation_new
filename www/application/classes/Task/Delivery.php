<?php defined('SYSPATH') or die('No direct script access.');

class Task_Delivery extends Minion_Task
{
    protected $_options = array(
        'env' => 'prod',
    );

    protected function _execute(array $params)
    {
        if ($params['env']=='prod'){
            Kohana::$environment=Kohana::PRODUCTION;
        } else {
            Kohana::$environment=Kohana::DEVELOPMENT;
        }
        $users = ORM::factory('User')->where('delivery', '=', 1)->find_all();
        $end_date = new DateTime('now');
        $start_date = clone $end_date;
        $start_date = $start_date->sub(new DateInterval('P1D'));
        $news = ORM::factory('News')
            ->where('is_published', '=', 1)
            ->and_where('date', 'BETWEEN', array($start_date->format('Y-m-d H:i:s'), $end_date->format('Y-m-d H:i:s')))
            ->find_all()
            ->as_array();
        if (count($news)) {
            foreach($users as $user) {
                $text = View::factory('news_delivery')->set('news', $news)->set('user', $user);
                $email = Email::factory(__('Рассылка'), $text, 'text/html');
                $email->from('support@confederation.kz');
                $email->to($user->email);
                $email->send();
            }
        }

    }

}