<?php defined('SYSPATH') or die('No direct script access.');

class Task_Sitemap extends Minion_Task
{
    protected $_options = array(
        'env' => 'prod',
    );
    protected $langs = array(
        'ru',
        'kz',
        'en'
    );

    protected $objects = array(
        'box'=>'box',
        'judo'=>'judo',
        'wrestling'=>'wrestling',
        'weightlifting'=>'weightlifting',
        'taekwondo'=>'taekwondo'
);

    protected function _execute(array $params)
    {
        if ($params['env'] == 'prod') {
            Kohana::$environment = Kohana::PRODUCTION;
        } else {
            Kohana::$environment = Kohana::DEVELOPMENT;
        }
        $model_names = array(
            'News',
            'Photovideo',
            'Event',
            'Section',
            'Federation',
            'Coach',
            'Team',
            'Result',
            'Other',
        );
        $all_records = 0;
//        $mail = 'Запущена задача Sitemap.php';
        foreach ($this->objects as $object) {
            $text = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n";
            $counter = 0;
//            $filename = DOCROOT . 'sitemap' . $object . '.xml';
            $filename = DOCROOT . 'sitemap' . $object . '.xml';
            $fp = fopen($filename, 'w');
            $federation = ORM::factory('Federation')->where('sef', '=', $object)->find();
            Minion_CLI::write(strtoupper($object) . ': ');
//            $mail .= strtoupper($object) . ': <br>';
            foreach ($model_names as $model_name) {
                $result = $this->{$model_name}($federation);
                $text .= $result['text'];
                $counter += $result['counter'];
                if (array_key_exists('team_text', $result)) {
                    $text .= $result['team_text'];
                    $counter += $result['team_counter'];
                }
                Minion_CLI::write($model_name . ': ' . $counter . ' записей');
//                $mail .= $model_name . ': ' . $counter . ' записей<br>';
                $all_records += $counter;
                $counter = 0;

            }
            $text .= '</urlset>';
            $test = fwrite($fp, $text);
            if ($test) Minion_CLI::write('Данные в файл успешно занесены.');
            else Minion_CLI::write('Ошибка при записи в файл.');
            Minion_CLI::write($filename . '<br>');
//            $mail .= $filename;
            Minion_CLI::write('--------------------------------------');
            fclose($fp);
        }
        Minion_CLI::write('Всего по федерациям ' . $all_records . ' записей');
//        $mail .= 'Всего по федерациям ' . $all_records . ' записей<br>';
//        $email = Email::factory('Логи', $mail);
//        $email->from('support@confederation.kz');
//        $email->to('id541rak@gmail.com');
//        $email->send();

    }

    public function Xml($model, $changefreq, $priority, $federation, $lang, $route = null, $canonical = false)
    {
        $text = null;
        $i = 0;
        if ($route) {
            $this->text('route',$lang,$federation, $route,$changefreq,$priority);
            $i++;
        }
        if ($model) {
            foreach ($model as $row) {
                if ($canonical) {
                    foreach ($this->objects as $object) {
                        $fed = ORM::factory('Federation')->where('sef', '=', $object)->find();
                        if ($row->has('federations', $fed)) {
                            $canonical_federation = $object;
                            break;
                        }
                    }
                    $current_federation = array_search($federation->sef, $this->objects);
                    if ($canonical_federation == $current_federation) {
                        $this->text('model', $lang, $federation, $route, 'monthly', 0.5, $row);
                        $i++;
                    }
                } else {
                    $this->text('model', $lang, $federation, $route, $changefreq, $priority, $row);
                    $i++;
                }
            }
        }
        if (!$route && !$model && $federation) {
            $this->text(null, $lang, $federation, null, 'monthly', $priority);
            $i++;
        }
        $result = array('text' => $text, 'counter' => $i);
        return $result;
    }

    public function text($type, $lang, $federation, $route, $changefreq, $priority, $row = null)
    {
        switch ($type) {
            case 'route':
                $text = "<url>\r\n";
                $text .= '<loc>' . 'http://confederation.kz/' . $this->check_kz($lang) . $federation->sef . '/' . $route . "</loc>\r\n";
                $text .= '<changefreq>' . $changefreq . "</changefreq>\r\n";
                $text .= '<priority>' . $priority . "</priority>\r\n";
                $text .= "</url>\r\n";
                break;
            case 'model':
                $text = "<url>\r\n";
                $text .= '<loc>' . 'http://confederation.kz/' . $this->check_kz($lang) . $federation->sef . $row->map_url($lang) . "</loc>\r\n";
                $text .= '<changefreq>monthly' . "</changefreq>\r\n";
                $text .= '<priority>0.5' . "</priority>\r\n";
                $text .= "</url>\r\n";
                break;
            default:
                $text = "<url>\r\n";
                $text .= '<loc>' . 'http://confederation.kz/' . $this->check_kz($lang) . $federation->sef . "</loc>\r\n";
                $text .= '<changefreq>' . $changefreq . "</changefreq>\r\n";
                $text .= '<priority>' . $priority . "</priority>\r\n";
                $text .= "</url>\r\n";
        }

        return $text;
    }

//новости
    public function News($federation)
    {
        $changefreq = 'hourly';
        $priority = 1;
        $text = null;
        $counter = 0;
        foreach ($this->langs as $lang) {
            $cat = ORM::factory('Category')->where('slug', '=', 'news')->find();
            $model = $federation->news;
            $model->join('news_categories', 'left')->on('news_categories.news_id', '=', 'news.id');
            $model->where('news_categories.category_id', '=', $cat->id);
            $model = $model
                ->where('is_published', '!=', '2')
                ->where('main', '=', '2')
                ->where('text_' . $lang, '!=', '')
                ->where('title_' . $lang, '!=', '')
                ->order_by('date', 'DESC')
                ->find_all();
            $result = $this->xml($model, $changefreq, $priority, $federation, $lang, 'news', true);
            $text .= $result['text'];
            $counter += $result['counter'];
        }
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

//cудьи и команды судей
    public function Coach($federation)
    {
        $changefreq = 'monthly';
        $priority = 0.5;
        $text = null;
        $counter = 0;
        $team_result = array();
        $team_text = null;
        $team_counter = null;
        foreach ($this->langs as $lang) {
            if ($federation->sef == 'wrestling') {
                $teams = $federation->teams->find_all();
                foreach ($teams as $team) {
                    $coaches = $team->coaches->where('is_published', '!=', '2')->find_all();
                    //команды и страницы команд
                    $team_result[$lang][$team->id] = $this->xml(null, $changefreq, $priority, $federation, $lang, 'coach/' . $team->{'sef_' . $lang});
                    $team_text .= $team_result[$lang][$team->id]['text'];
                    $team_counter += $team_result[$lang][$team->id]['counter'];
                }
                $model = ORM::factory('Coach')->where('federation_id', '=', $federation->id)->where('biography_' . $lang, '!=', '')->find_all();
                //вьюшки
                $result = $this->xml($model, $changefreq, $priority, $federation, $lang);
                $text .= $result['text'];
                $counter += $result['counter'];
            } else {
                $model = ORM::factory('Coach');
                $view_model = $model->where('federation_id', '=', $federation->id)->where('biography_' . $lang, '!=', '')->find_all();
                $whole_model = $model->where('federation_id', '=', $federation->id)->find_all();
                $result = $this->xml($view_model, $changefreq, $priority, $federation, $lang, 'coach');
                $text .= $result['text'];
                $counter += $result['counter'];
            }
        }
        $result = array('text' => $text, 'counter' => $counter, 'team_text' => !empty($team_text) ? $team_text : null, 'team_counter' => !empty($team_counter) ? $team_counter : null);
        return $result;
    }

//команды игроков
    public function Team($federation)
    {
        $changefreq = 'monthly';
        $priority = 0.5;
        $text = null;
        $counter = 0;
        foreach ($this->langs as $lang) {

            $model = $federation->teams->where('main', '=', '2')->find_all();
            foreach ($model as $row) {
                $players = $row->players->find_all();
                $result = $this->xml(null, $changefreq, $priority, $federation, $lang, 'team/' . $row->{'sef_' . $lang});
                $text .= $result['text'];
                $counter += $result['counter'];
            }
        }
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

//события
    public function Event($federation)
    {
        $changefreq = 'weekly';
        $priority = 0.6;
        $text = null;
        $counter = 0;
        foreach ($this->langs as $lang) {
            $model = $federation->events->find_all();
            $result = $this->xml($model, $changefreq, $priority, $federation, $lang, 'event', true);
            $text .= $result['text'];
            $counter += $result['counter'];
        }
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

//фото и видео
    public function Photovideo($federation)
    {
        $changefreq = 'daily';
        $priority = 0.8;
        $text = null;
        $counter = 0;
        foreach ($this->langs as $lang) {
            $model = $federation->photovideos->where('title_' . $lang, '!=', '')->where('content_' . $lang, '!=', '')->find_all();
            $result = $this->xml($model, $changefreq, $priority, $federation, $lang, 'gallery', true);
            $text .= $result['text'];
            $counter += $result['counter'];
        }
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

//секции
    public function Section($federation)
    {
        $changefreq = 'monthly';
        $priority = 0.5;
        $text = null;
        $counter = 0;
        foreach ($this->langs as $lang) {
            $model = $federation->sections->where('title_' . $lang, '!=', '')->find_all();
            $result = $this->xml($model, $changefreq, $priority, $federation, $lang, 'section');
            $text .= $result['text'];
            $counter += $result['counter'];
        }
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

    public function Player($federation)
    {
        $changefreq = 'monthly';
        $priority = 0.5;
        $text = null;
        $counter = 0;
        foreach ($this->langs as $lang) {
            $teams = $federation->teams->where('main', '=', '2')->find_all()->as_array('id', 'id');
            $players = $federation->players->where('team_id', 'in', $teams)->find_all();
            $result = $this->xml($players, $changefreq, $priority, $federation, $lang);
            $text .= $result['text'];
            $counter += $result['counter'];
        }
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

    public function Federation($federation)
    {
        $changefreq = 'monthly';
        $priority = 0.5;
        $text = null;
        $actions = $federation->functions();
        $counter = 0;
        foreach ($this->langs as $lang) {
            foreach ($actions as $action) {
                $text .= "<url>\r\n";
                $text .= '<loc>' . 'http://confederation.kz/' . $this->check_kz($lang) . $federation->sef . '/federation/' . $action . "</loc>\r\n";
                $text .= '<changefreq>' . $changefreq . "</changefreq>\r\n";
                $text .= '<priority>' . $priority . "</priority>\r\n";
                $text .= "</url>\r\n";
                $counter++;
            }
            $text .= "<url>\r\n";
            $text .= '<loc>' . 'http://confederation.kz/' . $this->check_kz($lang) . $federation->sef . "</loc>\r\n";

            $text .= '<changefreq>' . $changefreq . "</changefreq>\r\n";
            $text .= '<priority>' . $priority . "</priority>\r\n";
            $text .= "</url>\r\n";
            $counter++;
        }
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

    public function Result($federation)
    {
        $changefreq = 'weekly';
        $priority = 0.6;
        $text = null;
        $counter = 0;
        foreach ($this->langs as $lang) {
            $model = $federation->results->where('is_published', '!=', '2')->find_all();
            $result = $this->xml(null, $changefreq, $priority, $federation, $lang, 'result');
            $text .= $result['text'];
            $counter += $result['counter'];
        }
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

    public function Other($federation)
    {
        $changefreq = 'monthly';
        $priority = 0.5;
        $text = null;
        $counter = 0;
        foreach ($this->langs as $lang) {
            $result = $this->xml(null, $changefreq, $priority, $federation, $lang);
            $text .= $result['text'];
            $counter += $result['counter'];
        }
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

    public function check_kz($lang)
    {
        if ($lang == 'kz') {
            return '';
        } else {
            return $lang . '/';
        }
    }

}