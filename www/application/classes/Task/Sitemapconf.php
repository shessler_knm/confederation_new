<?php defined('SYSPATH') or die('No direct script access.');

class Task_Sitemapconf extends Minion_Task
{
    protected $_options = array(
        'env' => 'prod',
    );
    protected $langs = array(
        'ru',
        'kz',
        'en'
    );

    protected $report = null;

    protected function _execute(array $params)
    {
        if ($params['env'] == 'prod') {
            Kohana::$environment = Kohana::PRODUCTION;
        } else {
            Kohana::$environment = Kohana::DEVELOPMENT;
        }
        $model_names = array(
            'News',
            'Photovideo',
            'Event',
            'Section',
            'Fact',
            'Antidoping',
            'Infographic',
            'Page',
            'Forum',
            'Other'
        );
        $text = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n";
        $counter = 0;
        $all_counter = 0;
        foreach ($model_names as $model_name) {
            $result = $this->{$model_name}();
            $text .= $result['text'];
            $counter += $result['counter'];
            if (array_key_exists('team_text', $result)) {
                $text .= $result['team_text'];
                $counter += $result['team_counter'];
            }
            Minion_CLI::write($model_name . ': ' . $counter . ' записей');
            $all_counter += $counter;
            $counter = 0;
        }
        $text .= '</urlset>';
        $fp = fopen(DOCROOT . "sitemapconfederation.xml", 'w');
        $test = fwrite($fp, $text);
        Minion_CLI::write('----------------------------------');
        Minion_CLI::write('Всего ' . $all_counter . ' записей');
        if ($test) Minion_CLI::write('Данные в файл успешно занесены.');
        else Minion_CLI::write('Ошибка при записи в файл.');
        fclose($fp);

    }

    public function Xml($model = null, $changefreq, $priority, $lang, $route = null, $category = null, $canonical = false)
    {
        $text = null;
        $i = 0;
        if ($route) {
            $text .= $this->text('route', $category, null, $lang, $route, $changefreq, $priority);
            $i++;
        }
        if ($model) {
            foreach ($model as $row) {
                //тип текста, категория, загруженная модель, язык, роут, частота, приоритет
                $text .= $this->text('model', $category, $row, $lang, $route, $changefreq, $priority);
                $i++;
            }
        }
        if (!$route && !$model) {
            //тип текста, категория, загруженная модель, язык, роут, частота, приоритет
            $text .= $this->text(null, $category, null, $lang, $route, $changefreq, $priority);
            //здесь было $i += 2; не знаю зачем, но может быть верным решением
            $i += 1;
        }
        $result = array('text' => $text, 'counter' => $i);
        return $result;
    }

    //новости
    public function News()
    {
        $categories = array(
            'confederation/infocenter/news' => 'news',
            'confederation/interview' => 'interviews',
            'confederation/infocenter/pressservice' => 'pressservice',
            'confederation/infocenter/aboutus' => 'mediaaboutus'
        );
        $text = null;
        $counter = 0;
        $model = ORM::factory('News');

        foreach ($categories as $key => $category) {
            switch ($category) {
                case 'news':
                    $changefreq = 'hourly';
                    $priority = 1;
                    break;
                case 'interviews':
                    $changefreq = 'daily';
                    $priority = 0.8;
                    break;
                case 'pressservice':
                case 'mediaaboutus':
                    $changefreq = 'weekly';
                    $priority = 0.6;
                    break;
            }
            $new_cat = ORM::factory('Category')->where('slug', '=', $category)->find();
            foreach ($this->langs as $lang) {
                $model->join('news_categories', 'left')->on('news_categories.news_id', '=', 'news.id');
                $model->where('news_categories.category_id', '=', $new_cat->id);
                $collection = $model
                    ->where('is_published', '=', '1')
                    ->where('main', '=', '2')
                    ->where('text_' . $lang, '!=', '')
                    ->where('title_' . $lang, '!=', '')
                    ->order_by('date', 'DESC')
                    ->find_all();
                $result = $this->xml($collection, $changefreq, $priority, $lang, $key, $category, true);
                $text .= $result['text'];
                $counter += $result['counter'];
            }
        }
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

    //события
    public function Event()
    {
        $text = null;
        $counter = 0;
        $model = ORM::factory('Event');
        foreach ($this->langs as $lang) {
            $collection = $model
                ->where('is_published', '=', '1')
                ->where('title_' . $lang, '!=', '')
                ->where('text_' . $lang, '!=', '')
                ->find_all();
            $result = $this->xml($collection, 'weekly', 0.6, $lang, 'confederation/event', false, true);
            $text .= $result['text'];
            $counter += $result['counter'];
        }
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

    //фото и видео
    public function Photovideo()
    {
        $text = null;
        $counter = 0;
        $model = ORM::factory('Photovideo');
        foreach ($this->langs as $lang) {
            $collection = $model->where('title_' . $lang, '!=', '')->where('content_' . $lang, '!=', '')->find_all();
            $result = $this->xml($collection, 'daily', 0.8, $lang, 'confederation/infocenter/gallery', false, true);
            $text .= $result['text'];
            $counter += $result['counter'];
        }
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

    //секции
    public function Section()
    {
        $text = null;
        $counter = 0;
        $model = ORM::factory('Section');
        foreach ($this->langs as $lang) {
            $collection = $model->where('title_' . $lang, '!=', '')->find_all();
            $result = $this->xml($collection, 'monthly', 0.5, $lang, 'confederation/section', false, true);
            $text .= $result['text'];
            $counter += $result['counter'];
        }
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

    public function Fact()
    {
        $text = null;
        $counter = 0;
        $model = ORM::factory('Fact');
        foreach ($this->langs as $lang) {
            $collection = $model->where('title_' . $lang, '!=', '')->where('text_' . $lang, '!=', '')->find_all();
            $result = $this->xml($collection, 'weekly', 0.6, $lang, 'confederation/infocenter/fact');
            $text .= $result['text'];
            $counter += $result['counter'];
        }
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

    public function Antidoping()
    {
        $text = null;
        $counter = 0;
        foreach ($this->langs as $lang) {
            $result = $this->xml(null, 'weekly', 0.6, $lang, 'confederation/document');
            $text .= $result['text'];
            $counter += $result['counter'];
        }
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

    public function Infographic()
    {
        $text = null;
        $counter = 0;
        foreach ($this->langs as $lang) {
            $result = $this->xml(null, 'monthly', 0.2, $lang, 'confederation/infocenter/infographic');
            $text .= $result['text'];
            $counter += $result['counter'];
        }
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

    public function Page()
    {
        $text = null;
        $counter = 0;
        $model = ORM::factory('Page');
        foreach ($this->langs as $lang) {
            $collection = $model->find_all();
            $result = $this->xml($collection, 'monthly', 0.5, $lang);
            $text .= $result['text'];
            $counter += $result['counter'];
        }
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

    public function Other()
    {
        $text = null;
        $counter = 0;
        foreach ($this->langs as $lang) {
            $result = $this->xml(null, 'monthly', 0.5, $lang);
            $text .= $result['text'];
            $counter += $result['counter'];
        }
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

    public function Forum()
    {
        $counter = 0;
        $result = $this->xml(null, 'daily', 0.8, null, 1, 'forum');
        $text = $result['text'];
        $counter += $result['counter'];
        $result = array('text' => $text, 'counter' => $counter);
        return $result;
    }

    public function check_kz($lang = null)
    {
        if ($lang) {
            return $lang == 'kz' ? '' : $lang . '/';
        } else {
            return null;
        }
    }

    public function text($type, $category, $row, $lang, $route, $changefreq, $priority)
    {
        switch ($type) {
            case 'route':
                $text = "<url>\r\n";
                $text .= '<loc>' . 'http://confederation.kz/' . $this->check_kz($lang) . $route . "</loc>\r\n";
                $text .= '<changefreq>' . $changefreq . "</changefreq>\r\n";
                $text .= '<priority>' . $priority . "</priority>\r\n";
                $text .= "</url>\r\n";
                break;
            case 'model':
                $text = "<url>\r\n";
                if ($category) {
                    $text .= '<loc>' . 'http://confederation.kz/' . $this->check_kz($lang) . 'confederation' . $row->map_url($lang, $category) . "</loc>\r\n";
                } else {
                    $text .= '<loc>' . 'http://confederation.kz/' . $this->check_kz($lang) . 'confederation' . $row->map_url($lang) . "</loc>\r\n";
                }
                $text .= '<changefreq>' . $changefreq . "</changefreq>\r\n";
                $text .= '<priority>' . $priority . "</priority>\r\n";
                $text .= "</url>\r\n";
                break;
            default:
                $text = "<url>\r\n";
                $text .= '<loc>' . 'http://confederation.kz/' . $this->check_kz($lang) . "</loc>\r\n";
                $text .= '<changefreq>' . $changefreq . "</changefreq>\r\n";
                $text .= '<priority>' . $priority . "</priority>\r\n";
                $text .= "</url>\r\n";
                $text .= "<url>\r\n";
                $text .= '<loc>' . 'http://confederation.kz/' . $this->check_kz($lang) . 'confederation' . "</loc>\r\n";
                $text .= '<changefreq>' . $changefreq . "</changefreq>\r\n";
                $text .= '<priority>' . $priority . "</priority>\r\n";
                $text .= "</url>\r\n";
        }
        return $text;
    }

}