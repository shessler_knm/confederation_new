<?php defined('SYSPATH') or die('No direct script access.');

class Task_Image extends Minion_Task
{
    protected $_options = array(
        'env' => 'prod',
    );

    protected function _execute(array $params)
    {
        if ($params['env']=='prod'){
            Kohana::$environment=Kohana::PRODUCTION;
        } else {
            Kohana::$environment=Kohana::DEVELOPMENT;
        }
        $model_names = array(
            'News' => 'text_',
            'Fact' => 'text_',
            'Federation' => 'structure_',
            'Event' => 'text_',
            'Coach' => 'biography_',
            'Player' => 'biography_',
            'Section' => 'description_',
            'Page' => 'text_',
        );
        $langs = array('kz', 'en', 'ru');
        $x = 0;
        foreach ($model_names as $model_name => $text) {
            $model = ORM::factory($model_name)->find_all();
            foreach ($model as $row) {
                foreach ($langs as $lang) {
                    $find_imgs_{$lang} = preg_match_all('#(\<(img[^\>]+)\>)#', $row->{$text . $lang}, $match_{$lang});
                    //Ищем все изображения в тексте
                    if ($find_imgs_{$lang}) {
                        //Если изображение найдено в тексте
                        foreach ($match_{$lang}[0] as $img_{$lang}) {
                            //Проходим по всем изображениям в цикле
                            if (!strstr($img_{$lang}, 'alt="')) {
                                //Если alt не найден
                                $result = str_replace('>', 'alt="' . HTML::chars($row->{'title_' . $lang}) . '">', $img_{$lang});
                                $row->{$text . $lang} = preg_replace('#(\<(img[^\>]+)\>)#', $result, $row->{$text . $lang});
                                $row->save();
                                $x++;
                            } elseif (strstr($img_{$lang}, 'alt=""')) {
                                //Если alt пустой
                                $result = preg_replace('#alt=\".*>#', 'alt="' . HTML::chars($row->{'title_' . $lang}) . '">', $img_{$lang});
                                $row->{$text . $lang} = preg_replace('#alt=\".*?\"#', $result, $row->{$text . $lang});
                                $row->save();
                                $x++;
                            } elseif (preg_match('#alt=\".*\.(JPG|jpg|png|jpeg|gif)\"#', $img_{$lang})) {
                                //Если в alt'e название файла
                                $result = preg_replace('#alt=\".*\.(JPG|jpg|png|jpeg|gif)\"#', 'alt="' . HTML::chars($row->{'title_' . $lang}) . '">', $img_{$lang});
                                $row->{$text . $lang} = preg_replace('#alt=\".*?\"#', $result, $row->{$text . $lang});
                                $row->save();
                                $x++;
                            }
                        }
                    }
                }
            }
            Minion_CLI::write($model_name . ': обработано ' . $x . ' изображений');
            $x = 0;
        }
    }

}