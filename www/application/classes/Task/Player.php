<?php defined('SYSPATH') or die('No direct script access.');

class Task_Player extends Minion_Task
{
    protected $_options = array(
        'env' => 'prod',
    );

    protected function _execute(array $params)
    {
        if ($params['env'] == 'prod') {
            Kohana::$environment = Kohana::PRODUCTION;
        } else {
            Kohana::$environment = Kohana::DEVELOPMENT;
        }
        $langs = array('kz', 'en', 'ru');
        $players = ORM::factory('Player')->find_all();
        foreach ($players as $player) {
            foreach ($langs as $lang) {
                if ($player->{'sef_' . $lang} == '-') {
                    $player->{'sef_' . $lang} = ORM::full_name_filter_sef('', $player->get_full_name($lang));
                    $player->save();
                }
            }
        }
    }

}