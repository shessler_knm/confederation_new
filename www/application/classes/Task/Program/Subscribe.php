<?php
/**
 * Created by PhpStorm.
 * User: sgm
 * Date: 14.02.14
 * Time: 16:13
 */

class Task_Program_Subscribe extends Minion_Task{
    protected $_options = array(
        'env' => 'prod',
    );
    protected function _execute(array $params){
        if ($params['env'] == 'prod') {
            Kohana::$environment = Kohana::PRODUCTION;
        } else {
            Kohana::$environment = Kohana::DEVELOPMENT;
        }
        Minion_CLI::write(Minion_CLI::color('Collecting all programs....','yellow'));
        $collection = ORM::factory('Program')->where('type', '=', 'free');
        foreach($collection->find_all() as $row){
            try{
                $row->subscribers = $row->program_forks->where('activity','=',1)->count_all();
                $row->save();
                Minion_CLI::write(Minion_CLI::color('Program id:'.$row->id.' saved:'.$row->subscribers,'green'));
            }catch (ORM_Validation_Exception $e){
                Minion_CLI::write(Minion_CLI::color('Error while saving program id:'.$row->id.' '.$row->title_ru,'red'));
                Minion_CLI::write(Minion_CLI::color(implode("\n",$e->errors()),'red'));
            }

        }
        Minion_CLI::write(Minion_CLI::color('Done!','yellow'));
    }
} 