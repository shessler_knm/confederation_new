<?php defined('SYSPATH') or die('No direct script access.');
 
class Task_Synchflickr extends Minion_Task {
 
	protected function _execute(array $params)
	{
            Kohana::$environment = Kohana::PRODUCTION;
            $f = new phpFlickr(Kohana::$config->load('phpflickr')->get('api_keys'));
            $db_photo = ORM::factory('Photo')->find_all()->as_array('id','unique_id');
            $db_album = ORM::factory('Photovideo')->where('type','=','1')->find_all()->as_array('id','content');

            $count_new_album = 0; //Счетчик новых фотографий
            $count_new_photo = 0; //Счетчик новых фотографий
            $deleted_photo = 0;

            $albums = $f->photosets_getList(Kohana::$config->load('phpflickr')->get('user'));

            if (!$albums) {
                return false;
            }

            foreach ($albums['photoset'] as $album) {
                $al_id = array_search($album['id'], $db_album); //id альбома в бд, идентичный пришедшей
                $new_album = ORM::factory('Photovideo');
                if (!$al_id) { //Поиск идентичного альбома, если такого нет, то происходит запись
                    $new_album->title_ru = $album['title'];
                    $new_album->description_ru = $album['description'];
                    $new_album->content_ru = $album['id'];
                    $new_album->date = date('Y-m-d H:i:s', $album['date_create']);
                    $new_album->type = 1;
                    $new_album->sef_ru = 'ru-'.Helper::translit($album['title']);
                    $new_album->sef_kz = 'kz-'.Helper::translit($album['title']);
                    $new_album->sef_en = 'en-'.Helper::translit($album['title']);
                    $new_album->views = $album['count_views'];
                    $new_album->save();
                    $new_album->reload();
                } else {
                    unset($db_album[$al_id]); //удаление идентичного альбома из массива
                }
                $photos = $f->photosets_getPhotos($album['id']);

                foreach ($photos['photoset']['photo'] as $photo) {
                    $ph_id = array_search($photo['id'], $db_photo); //Поиск идентичной фотографии, если такого нет, то происходит запись
                    if (!$ph_id) {
                        $new_photo = ORM::factory('Photo');
                        $new_photo->title_ru = $photo['title'];
                        $new_photo->date = date('Y-m-d H:i:s');
                        $new_photo->unique_id = $photo['id'];
                        $new_photo->src = $f->buildPhotoURL($photo, 'medium');
                        $new_photo->minisrc = $f->buildPhotoURL($photo, 'small');
                        $new_photo->largesrc = $f->buildPhotoURL($photo, 'large');
                        $new_photo->album_id = (!$al_id) ? $new_album->id : $al_id;
                        $new_photo->save();
                        $count_new_photo++;
                    } else {
                        unset($db_photo[$ph_id]); //удаление идентичной фотографии из массива
                    }
                }

    //            $count_new_album++;
            }

            foreach ($db_album as $i => $al_id) {
                $not_found = ORM::factory('Photovideo', array('id' => $i));
                $not_found->delete();
                $deleted_photo++;

            }
            foreach ($db_photo as $i => $ph_id) {
                $not_found = ORM::factory('Photo', array('id' => $i));
                $not_found->delete();
                $deleted_photo++;
            }

    //        $this->page->message('Добавлено ' . $count_new_album . ' новых альбомов');
    //        $this->page->message('Добавлено ' . $count_new_photo . ' новых фотографий');
    //        if ($deleted_photo) {
    //            $this->page->message('Найдено не существующих фото ' . $deleted_photo);
    //        }
	}
 
}
?>
