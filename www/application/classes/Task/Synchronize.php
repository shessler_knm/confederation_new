<?php defined('SYSPATH') or die('No direct script access.');

class Task_Synchronize extends Minion_Task {


    const PARENT_SYNCH = 1.03; // Синхронизация Embeded моделей
    const FEDER_SYNCH = 2.03; // Синхронизация моделей через has_many модели Federation
    const FREE_SYNCH = 3.03; // Синхронизация прямамая. synch_id == id (На федерации)

    private $type = self::FEDER_SYNCH;

    protected $_options = array(
        // param name => default value
        'env' => Kohana::PRODUCTION,
        'model'   => false,
        'fed_id' => false,
        'parent_id' => false,
        'parent_model' => 'Federation',
        'parent_field' => false,
        'parent_hasmany' => false
    );

    private $xKey = 's1231sfaqlm$q23#fgmred';

    protected function _execute(array $params)
    {
        set_time_limit(0);
        Kohana::$environment = Arr::get($params,'env');

        /**
         * Синхронизация
         */

        if ($params['model']) {
            try {
                $this->allSynchronization($params);
                Kohana::$log->add(Log::ERROR,'Синхронизация успешно завершена');
            } catch (Exception $e) { //ORM_Validation_Exception
                Kohana::$log->add(Log::ERROR,'Ошибка синхронизации params = '.implode(' , ',$params).' message->'. $e->getMessage());
                if ($e instanceof ORM_Validation_Exception)
                {Kohana::$log->add(Log::ERROR, 'Errors: '.var_dump($e->errors()));}
            }
            return; // !!!
        }


        /**
         * Выполнение тасков после синхронизации
         */
        Kohana::$log->add(Log::ERROR,'Начало выполнения синхронизации тасков ');
        try {
            $collection = $this->lock_synch_tasks();
            while(count($collection) > 0) {
                foreach($collection as $item) {
                    try {
                        $model = ORM::factory(ucfirst($item->model_name),$item->uid);
                        $tasks = json_decode($item->tasks);

                        foreach($tasks as $func => $obj_params) {

                            $this->{$func}($obj_params, $model);
                        }

                        $item->delete();
                    } catch (Exception $e) {
                        Kohana::$log->add(Log::ERROR,'Ошибка синхронизации таска `id` -> '.$item->id.' Error message -> `'.$e->getMessage().'`');
                        continue;
                    }
                }

                $collection = $this->lock_synch_tasks();
            }
        }
        catch(Exception $e) {
            Kohana::$log->add(Log::ERROR,'Синхронизация тасков FAILD mess: '.$e->getMessage().
                ' line: '.$e->getLine());
        }
        Kohana::$log->add(Log::ERROR,'Конец выполнения синхронизации тасков ');
    }

    //Полная синхронизация данных
    private function allSynchronization($params) {

        //Переменные для синх
        $name = Arr::get($params,'model');
        $parent_id = Arr::get($params,'parent_id');
        $fed_id = Arr::get($params,'fed_id');
        $parent_model = Arr::get($params,'parent_model');
        $parent_field = Arr::get($params,'parent_field');
        $parent_hasmany = Arr::get($params,'parent_hasmany');

        $fed = ORM::factory('Federation',$fed_id);
        $model = ORM::factory(ucfirst($name));
        $config = Kohana::$config->load('synch');
        $config['confederation']['xUser'] = 'mara';
        $config['confederation']['xPass'] = '21482148';

        //Формируем Request
        $conf_url = parse_url($fed->url);
        $conf_url = rtrim($conf_url['host'],'/');
        $request=Request::factory('http://'.$conf_url.'/synch/'.$model->object_name());
        $request->method(Request::POST);

        //Заголовки
        $request->headers('x-key',$this->xKey);
        $request->headers('x-user',$config['confederation']['xUser']);
        $request->headers('x-pass',$config['confederation']['xPass']);
        //$request->cookie('XDEBUG_SESSION','knms2');

        //Уст. POST данные
        $request->post('parent_id',$parent_id);
        $request->post('model_name',$name);
        $request->post('fed_id',$fed_id);
        $request->post('parent_model',$parent_model);
        $request->post('parent_field',$parent_field);
        $request->post('parent_hasmany',$parent_hasmany);

        //Проверяем на наличие synch_id / synch_site
        $arr = $model->object_fields();
        if (array_key_exists('synch_id',$arr) === false &&
            array_key_exists('synch_site',$arr) === false) {
            DB::query(Database::UPDATE,
                "
                ALTER TABLE `".$model->table_name()."`
                ADD COLUMN `synch_id`  int NULL,
                ADD COLUMN `synch_site`  varchar(255) NULL;
                "
            )->execute();
        }
        if (array_key_exists('is_published',$arr) === false) {
            DB::query(Database::UPDATE,
            "ALTER TABLE `".$model->table_name()."`
            ADD COLUMN `is_published`  int NULL DEFAULT 1 COMMENT '1 - da 2 net';"
            )->execute();
        }

        //Собираем готовые синх. данные
        $items_available = array();
        if ($parent_hasmany == false AND $parent_model == 'Federation') {
            $this->type = self::FEDER_SYNCH;
            foreach ($model->where('federation_id','=',$fed_id)->find_all() as $item) {
                if ($item->synch_id !== NULL) {
                    $items_available[] =  $item->synch_id;
                }
            }
        }
        elseif ($parent_hasmany == false AND $parent_model == false) {
            $this->type = self::FREE_SYNCH;
            foreach ($model->where('synch_id','!=',NULL)
                            ->where('synch_site','=',$fed->url)->find_all() as $item) {
                    $items_available[] =  $item->synch_id;
            }
        }
        else
        {
            $this->type = self::PARENT_SYNCH;
            if (!$parent_id && $parent_model == 'Federation') {$parent_id = $fed_id;}
            $parent = ORM::factory($parent_model)->where('id','=',$parent_id)->find();
            if ($parent_model != 'Federation') {$request->post('parent_id',$parent->synch_id);}
            $request->post('parent_obj',json_encode($parent->to_array()));
            foreach ($parent->$parent_hasmany->find_all() as $item) {
                if ($item->synch_id !== NULL AND $item->synch_site == $fed->url) {
                    $items_available[] =  $item->synch_id;
                }
            }
        }

        $request->post('items_available',
            count($items_available) > 0 ? $items_available : 'false'
        );

        Kohana::$log->add(Log::ERROR,'ITEMS AVAILABLE - '. count($items_available).' model_name='.$name.' fed='.$fed->title_ru);

        //Отправляем
        $response = $request->execute();
		
		Kohana::$log->add(Log::ERROR,'RSPONSE STATUS - '. $response->status());

        /**
         *
         * Принимаем данные :
         *
         */

        $data = json_decode($response);

		Kohana::$log->add(Log::ERROR,'Кол-во полученных данных - '.count($data));
		
        if (isset($data->errors)) {return;}

        if ($data) {
            foreach($data as $key => $model) {
                if ($key !== 'tasks' && $key !== 'belongs') {
                    $new_model = ORM::factory(ucfirst($name));
                    $new_model
                        ->where('synch_id','=',$model->id)
                        ->where('synch_site','=',$fed->url)
                        ->find(); //Подгружаем если есть

                    //Записываем все поля
                    foreach($new_model->object_fields() as $field => $value) {
                        if (isset($model->{$field}) AND $field != 'id' AND $field != 'synch_site' AND $field != 'synch_id') {
                            $new_model->{$field} = $model->{$field};
                        }
                    }

                    //Перезаписываем нужные поля.
                    $new_model->synch_id = $model->id;
                    $new_model->synch_site = $fed->url;
                    $new_model->is_published = 2;

                    try {
                        $new_model->federation_id = $fed_id;
                    } catch (Exception $e) {}

                    //Устанавливаем белонгсы
                    if (isset($data->belongs) && isset($data->belongs->{$key})) {
                        $this->setup_belongs($new_model,$data->belongs->{$key}, $fed);
                    }

                    //Пытаемся сохранить
                    if($new_model->date == null) continue;
                    try {
                        $new_model->save();
                    } catch (Exception $e) {
                        Kohana::$log->add(Log::ERROR,'Ошибка сохранения: '.$e->getMesssage);
                    }

                    //Добавляем HAS_MANY
                    if ($this->type == self::PARENT_SYNCH) {

                        $this->setup_hasmany($new_model, $parent, $parent_hasmany);
                    }

                    if (isset($data->tasks) && isset($data->tasks[$key])) {
                        $tasks = $data->tasks[$key];
                        if ($tasks) {
                            $s = ORM::factory('Synchronize');
                            $s->model_name = $new_model->object_name();
                            $s->uid = $new_model->id;
                            $s->tasks = json_encode($tasks);
                            $s->save();
                        }
                    }
                }
            }
        }
    }

    private function setup_hasmany($model, $parent, $alias) {
        $has = $parent->has_many();
        if (array_key_exists($alias,$has)) {
            if (isset($has[$alias]['through'])) {
                if (!$parent->has($alias,$model->id)) {
                    $parent->add($alias, $model->id);
                }
            }
        }
    }

    private function setup_belongs($model, $belongs_data, $fed) {

        $belongs = $model->belongs_to();

        foreach($belongs as $field => $settings) {
            if (!isset($settings['model'])) {
                $belongs[$field]['model'] = ucfirst($field);
            }
            if (!isset($settings['foreign_key'])) {
                $belongs[$field]['foreign_key'] = $field.'_id';
            }
            if ($settings['model'] == 'Storage') {
                unset($belongs[$field]);
            }
            if ($settings['model'] == 'Federation') {
                unset($belongs[$field]);
            }
        }


        //Остаються только белонгсы !

        foreach($belongs as $field => $settings) {

            $belong = ORM::factory($settings['model']);
            $arr_fields = $belong->object_fields();
            if (array_key_exists('synch_id',$arr_fields) === false ||
                array_key_exists('synch_site',$arr_fields) === false ||
                !isset($belongs_data->$field)) {
                $this->mess_err("Не синхронизированы зависимые данные: ".$this->name($belong->object_name()).".");
                throw new Exception('Ошибка сихроинзации belongs to не имеют полей synch_id и synch_site belongs = '.$belong->object_name());
            }

            $belong
                ->where('synch_site','=',$fed->url)
                ->where('synch_id','=',$belongs_data->$field->id)
                ->find();

            if (!$belong->loaded()) {
                $belong
                    ->where('synch_site','=',NULL)
                    ->where('id','=',$belongs_data->$field->synch_id)
                    ->find();
            }

            if (!$belong->loaded()) {
                Kohana::$log->add(Log::ERROR,'Ошибка сихроинзации belong модель не синхронизирована "'.$belong->object_name().
                    '" $model->id = "'.$model->id.'"');

                $this->mess_err("Не синхронизированы зависимые данные: ".$this->name($belong->object_name()).".");
                return false;
            }
            $model->$settings['foreign_key'] = $belong->id;
        }

        return true;
    }

    public function name($object_name) {
        $names = array(
            'city' => __('Города'),
            'category' => __('Категории'),
            'event' => __('События'),
            'result' => __('Результаты'),
            'coach' => __('Тренера'),
            'player' => __('Спортсмены'),
            'team' => __('Команды'),

        );
        return array_key_exists($object_name,$names) ? $names[$object_name] : $object_name;
    }

    /**
     * @param $collection - массив полей синхронизациии
     * @return bool
     * true - если процесс синхронизации не запущен.
     * false - если процесс синхронизации уже запущен
     * или он не корректно отработал.
     */
    public function lock_synch_tasks() {

        $locked = ORM::factory('Synchronize')->where('locked','=',1)
            ->find_all();

        if (count($locked) > 0) {
            foreach($locked as $item) {
                $now = time();
                $lock = strtotime($item->date);
                $diff = $now - $lock;
                if ($diff > Date::HOUR * 3) {
                    Kohana::$log->add(Log::ERROR,
                        'ВНИМАНИЕ: Ошибка синхронизации. Время синхронизации превышает 3 часа. Проверить таблицу синхронизаций.');
                }
            }
        }

        $collection = ORM::factory('Synchronize')
            ->where('locked','=',0)
            ->limit(10)
            ->order_by('date','ASC');

        $return = array();
        foreach($collection->reset(false)->find_all() as $item) {
            $item->locked = 1;
            $item->save();
            $return[] = $item;
        }

        return $return;
    }

    public function arr_fields($model, $types = array()) {
        $fields = array();
        foreach($model->fields_description() as $name => $settings) {
            if (isset($settings['type']) && in_array($settings['type'], $types)) {
                $fields[$name] = $settings['type'];
            }
        }
        return $fields;
    }

    private function change_src($text,$site) {

        $url = rtrim($site,'/');
        $return = preg_replace('/<img(.*)src="(.*)/','<img$1src="'.$url.'$2',$text);
        return $return;
    }



    private function files ($params, $model) {

        foreach($params as $field => $data) {

            $belongs = $model->belongs_to();

            if (array_key_exists($field.'_s',$belongs)) {
                if ($model->{$field.'_s'}->loaded()) {
                    $path = str_replace('\\','/',DOCROOT);
                    $path = rtrim($path,'/').$model->{$field.'_s'}->url();
                    if (is_file($path)) {
                        $hash_file = md5(filesize($path));
                        if ($data->hash == $hash_file) {continue;}
                        else {
                            $model->{$field.'_s'}->links == 0 AND $model->{$field.'_s'}->delete();
                        }
                    }
                }
                $tmp_path = DOCROOT.'storage/tmp';
                if(!is_dir($tmp_path)) {
                    mkdir($tmp_path);
                }
                try {
                    $file = file_get_contents($data->download_url);
                    $file_name = $data->filename;
                    file_put_contents($tmp_path.'/'.$file_name,$file);
                    $storage = Cms_Storage::instance()->add($tmp_path.'/'.$file_name,$file_name);
                    $model->$field = $storage->id;
                    $model->save();
                    @unlink($tmp_path.'/'.$file_name);
                } catch (Exception $e) {Kohana::$log->add(Log::ERROR,'Ошибка files: '.$e->getMesssage);}
            }
        }
    }

    private function content_files ($params, $model) {
        $langs = array('ru','kz','en');

        foreach($params as $field => $desc) {

            try {
                if ($desc->type == 'multilang') {
                    foreach($langs as $l) {
                        $model->{$field.'_'.$l} = $this->change_src($model->{$field.'_'.$l},$model->synch_site);
                    }
                } else {
                    $model->$field = $this->change_src($model->$field,$model->synch_site);
                }
                $model->save();

            } catch (Exception $e) {Kohana::$log->add(Log::ERROR,'Ошибка content_files: '.$e->getMesssage);}
        }
    }

    public function mess_err ($text) {
        $errors = Session::instance()->get_once('synch_errors');
        if (!$errors) $errors = array();
        Session::instance()->set('synch_errors', $errors + array($text));
    }
}
?>
