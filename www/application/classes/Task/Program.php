<?php defined('SYSPATH') or die('No direct script access.');

class Task_Program extends Minion_Task
{
    protected $_options = array(
        'env' => 'prod',
    );
    protected $_device_messages = array(
        'ru' => 'Сегодня у Вас запланирована тренировка',
        'en' => 'You have a training scheduled today',
        'kz' => 'Бүгін кесте бойынша жаттығу орындауыңыз қажет'
    );

    protected function _execute(array $params)
    {
        if ($params['env'] == 'prod') {
            Kohana::$environment = Kohana::PRODUCTION;
        } else {
            Kohana::$environment = Kohana::DEVELOPMENT;
        }
        $programs = ORM::factory('Program')->where('activity', '=', 1)->where('type', '!=', 'free')->find_all();
        $text = ORM::factory('Page', array('sef' => 'workout'))->text_ru;
        foreach ($programs as $program) {
            $week_days = array();
            $exercises = $program->program_exercises->order_by('week_day', 'ASC')->find_all()->as_array();
            foreach ($exercises as $row) {
                $week_days[$row->week_day] = $row->week_day;
                $program_exercises[$row->week_day][] = $row;
            }
            $program_days = array_unique($program->program_exercises->find_all()->as_array('id', 'week_day'));
            $update_subscribers = $program->subscribers - 1;
            if (date('Ymd') >= $program->end_date($week_days, 'Ymd')) {
                DB::update('programs')->set(array('activity' => 2))->where('id', '=', $program->id)->execute();
                DB::update('programs')->set(array('subscribers' => $update_subscribers))->where('id', '=', $program->id)->execute();
                $ex_done = $program->logs->where('status', '=', 1)->find_all()->count();
                $all_exercises = $program->duration * $program->program_exercises->find_all()->count();
                Feed::awards($ex_done, $all_exercises, $program->user_id);
            }
            if (array_search(date('w'), $program_days)) {
                $user = ORM::factory('User', $program->user_id);
                if ($user->loaded()) {
                    if ($user->push_messages) {
                        $clients_tokens = array();
                        $message = array('message' => Arr::get($this->_device_messages, $user->lang, 'ru'));
                        $devices = $user->devices->find_all();
                        foreach ($devices as $device) {
                            $clients_tokens[$device->client][] = $device->push_token;
                        }
                        $this->send_message($clients_tokens, $message);
                    }
                    if ($user->email_messages) {
                        $mail = $user->email;
                        $email = Email::factory(__('Тренировка'), $text, 'text/html');
                        $email->from('support@confederation.kz');
                        $email->to($mail);
                        $email->send();
                    }
                }
            }
        }
    }

    public function send_message($clients_tokens, $message)
    {
        foreach ($clients_tokens as $client => $tokens) {
            if ($client == 'Android') {
                $this->sendGoogleCloudMessage($tokens, $message);
            } elseif ($client == 'iOS') {
                $this->send_ios_push($tokens, $message);
            }
        }
    }

    function sendGoogleCloudMessage($push_tokens, $text)
    {
        $config = Kohana::$config->load('gcm');
        $apiKey = Arr::get((array)$config, 'api_key');
        $url = Arr::get((array)$config, 'url');
        $post = array(
            'registration_ids' => $push_tokens,
            'data' => $text,
        );
        $request = new Request($url);
        $request->method(Request::POST);
        $request->headers('Authorization', 'key=' . $apiKey);
        $request->headers('Content-Type', 'application/json');
        $request->body(json_encode($post));
        $request->execute();
    }

    function replace_unicode_escape_sequence($match)
    {
        return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
    }

    function send_ios_push($push_tokens, $message)
    {
        $sound = 'default';
        $config = Kohana::$config->load('apn');
        $config = Arr::get((array)$config, 'production');
        $payload = array();
        $payload["aps"] = array('alert' => $message["message"], 'sound' => $sound);
        $payload = json_encode($payload);
        $payload = preg_replace_callback('/\\\\u([0-9a-f]{4})/i', array($this, 'replace_unicode_escape_sequence'), $payload);
        $apns_url = Arr::get((array)$config, 'server_url');
        $apns_cert = Arr::get((array)$config, 'apns_cert');
        $success = 0;
        $stream_context = stream_context_create();
        stream_context_set_option($stream_context, 'ssl', 'local_cert', Kohana::find_file("certificates", $apns_cert, false));
        stream_context_set_option($stream_context, 'ssl', 'passphrase', Arr::get($config, 'passphrase'));
        $apns = stream_socket_client($apns_url, $error, $error_string, Arr::get($config, 'connect_timeout'), STREAM_CLIENT_CONNECT, $stream_context);
        foreach ($push_tokens as $device) {
            $apns_message = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $device)) . chr(0) . chr(strlen($payload)) . $payload;
            fwrite($apns, $apns_message);
        }
        @socket_close($apns);
        fclose($apns);
        return $success;
    }

}