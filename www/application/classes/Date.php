<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of date
 * @package DEV
 * @author Igor Noskov <igor.noskov87@gmail.com>
 */
class Date extends Kohana_Date
{
    public static function  eng_months()
    {
        return array(
            'full' => array(
                '01' => __('january'),
                '02' => __('february'),
                '03' => __('march'),
                '04' => __('april'),
                '05' => __('may'),
                '06' => __('june'),
                '07' => __('july'),
                '08' => __('august'),
                '09' => __('september'),
                '10' => __('october'),
                '11' => __('november'),
                '12' => __('december')
            ),
            'single' => array(
                '01' => __('january'),
                '02' => __('february'),
                '03' => __('march'),
                '04' => __('april'),
                '05' => __('may'),
                '06' => __('june'),
                '07' => __('july'),
                '08' => __('august'),
                '09' => __('september'),
                '10' => __('october'),
                '11' => __('november'),
                '12' => __('december')
            )
        );
    }

    public static function  kaz_months()
    {
        return array(
            'full' => array(
                '01' => __('қаңтар'),
                '02' => __('ақпан'),
                '03' => __('наурыз'),
                '04' => __('сәуір'),
                '05' => __('мамыр'),
                '06' => __('маусым'),
                '07' => __('шілде'),
                '08' => __('тамыз'),
                '09' => __('қыркүйек'),
                '10' => __('қазан'),
                '11' => __('қараша'),
                '12' => __('желтоқсан')
            ),
            'single' => array(
                '01' => __('қаңтар'),
                '02' => __('ақпан'),
                '03' => __('наурыз'),
                '04' => __('сәуір'),
                '05' => __('мамыр'),
                '06' => __('маусым'),
                '07' => __('шілде'),
                '08' => __('тамыз'),
                '09' => __('қыркүйек'),
                '10' => __('қазан'),
                '11' => __('қараша'),
                '12' => __('желтоқсан')
            )
        );
    }

    public static function  rus_months()
    {
        return array(
            'full' => array(
                '01' => __('января'),
                '02' => __('февраля'),
                '03' => __('марта'),
                '04' => __('апреля'),
                '05' => __('мая'),
                '06' => __('июня'),
                '07' => __('июля'),
                '08' => __('августа'),
                '09' => __('сентября'),
                '10' => __('октября'),
                '11' => __('ноября'),
                '12' => __('декабря')
            ),
            'single' => array(
                '01' => __('январь'),
                '02' => __('февраль'),
                '03' => __('март'),
                '04' => __('апрель'),
                '05' => __('май'),
                '06' => __('июнь'),
                '07' => __('июль'),
                '08' => __('август'),
                '09' => __('сентябрь'),
                '10' => __('октябрь'),
                '11' => __('ноябрь'),
                '12' => __('декабрь')
            )
        );
    }

    /**
     * Извлекает информацию о дате из строки
     * @static
     * @param $strdate
     * @return array
     */
    static function strinfo($strdate)
    {
        $info = array();
        $m = self::rus_months();
        $month_detect = "/(".implode('|', $m['root']).")/i";
        $day_detect = '/(\d\d?)/i';
        $time_detect = '/(\d\d?\.\d\d?)/i';
        $period_type = '/(с|С|по|ПО|По|пО)\s/i';
        if (preg_match_all($month_detect, $strdate, $detected)) {
            $rm = array_flip($m['root']);
            if (count($detected[0]) > 1) {
                $info['type'] = "period";
                $info['month'] = array(
                    0 => $rm[$detected[0][0]],
                    1 => $rm[$detected[0][1]],
                );
            } else {
                $info['type'] = "day";
                $info['month'] = $rm[$detected[0][0]];
            }


        } else {
            $info['type'] = "text";
            $info['text'] = $strdate;
            return $info;
        }
        if (preg_match($time_detect, $strdate, $time)) {
            $info['time'] = explode('.', $time[0]);
        }
        if (preg_match_all($day_detect, $strdate, $days)) {
            $day_count = count($days[0]) - (isset($info['time']) ? 2 : 0);
            if ($day_count == 2) {
                $info['days'] = array(
                    0 => $days[0][0],
                    1 => $days[0][1],
                );
                $info['type'] = "period";
            } elseif ($day_count == 1) {
                $info['days'] = $days[0][0];
                if (preg_match_all($period_type, $strdate, $period)) {
                    if (count($period[0]) == 1) {
                        if (preg_match('/(с|С)/i', $period[0][0])) {
                            $info['type'] = "begin";
                        } elseif (preg_match('/(по|ПО|пО|По)/i', $period[0][0])) {
                            $info['type'] = "end";
                        }
                    }
                }
            }
        }
        return $info;

    }


    /**
     * Изменения выводимой даты
     * @param <type> $date Дата в строковом виде
     * @param <type> $format Формат даты в который надо преобразовать
     */
    static function reformat($date, $format = 'Y.m.d H:i:s')
    {
        return date($format, strtotime($date));
    }

    /**
     * форматирование даты, месяц прописью
     * @param string $date текстовая строка, дата
     * @param string $format формат отображения даты
     * @param boolean $single_month флаг отдельного месяца (т.е. "февраль", а не "февраля")
     * @return string
     */
    static function textdate($date, $format = 'd m Y', $single_month = false)
    {
        $month = date('m', strtotime($date));
        switch (I18n::lang()) {
            case "kz":$text_month = self::kaz_months(); break;
            case "ru":$text_month = self::rus_months(); break;
            case "en":$text_month = self::eng_months(); break;
        }
//        $text_month = self::rus_months();
        $month_reformat = $single_month ? strtr($month, $text_month['single']) : strtr($month, $text_month['full']);
        $format = $single_month ? str_replace('m', '!!!@', $format) : str_replace('m', '!!!', $format);
        $format = __($format);
        $format = str_replace('@', '', $format);
        return str_replace('!!!', $month_reformat, date($format, strtotime($date)));
    }

    static function site_date($date)
    {
        if (i18n::lang() == 'en') {
            return Date::formatted_time($date, 'm.d.Y');
        } else {
            return Date::formatted_time($date, 'd.m.Y');
        }
    }

    static function site_datetime($date)
    {
        if (i18n::lang() == 'en') {
            return Date::formatted_time($date, 'm.d.Y H:i');
        } else {
            return Date::formatted_time($date, 'd.m.Y H:i');
        }
    }


}

?>
