<?php
/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 10.06.13
 * Time: 14:34
 * To change this template use File | Settings | File Templates.
 */

class HTTP_Exception_404 extends Kohana_HTTP_Exception_404
{
    protected $session;

    public function get_response()
    {
        if ($this->_request) {
            $lang = $this->_request->param('lang');
            $request = Request::factory($lang . '/exception');
        } else {
            preg_match("/ru\/|en\/|kz\//", Request::initial()->uri(), $lang);
            $request = Request::factory((isset($lang[0]) ? $lang[0] : null) . 'exception');
        }
        $response = $request->execute();
        $response->status(404);
        return $response;
    }

}