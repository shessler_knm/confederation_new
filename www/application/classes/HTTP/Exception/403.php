<?php
/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 10.06.13
 * Time: 14:34
 * To change this template use File | Settings | File Templates.
 */

class HTTP_Exception_403 extends Kohana_HTTP_Exception_403 {
    public function get_response()
    {
        $response = Response::factory();
        $response->status(403);
        $view = View::factory('errors/template');
        $view->content = View::factory('errors/403');

        $response->body($view->render());

        return $response;
    }

}