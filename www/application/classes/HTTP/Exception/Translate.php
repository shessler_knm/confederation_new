<?php
/**
 * Created by JetBrains PhpStorm.
 * User: igor
 * Date: 10.06.13
 * Time: 14:34
 * To change this template use File | Settings | File Templates.
 */

class HTTP_Exception_Translate extends Kohana_HTTP_Exception_404 {
    protected  $session;

    public function get_response()
    {
        $request = Request::factory('exception/translate');
        $response=$request->query('lang',Arr::get($_GET,'lang',I18n::$lang))->execute();
        $response->status(404);
        return $response;
    }

}