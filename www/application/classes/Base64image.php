<?php
/**
 * Created by PhpStorm.
 * User: sgm
 * Date: 18.03.14
 * Time: 11:29
 */

class Base64image {

    protected $data;
    protected $type;
    protected $tmp;
    public function __construct($dataUrl){
        if (!preg_match('/^data:image\/(jpg|jpeg|png|gif);base64,/i',$dataUrl,$match)){
            throw new \Exception();
//            return false;
        };
        $this->type = $match[1];
        $img = str_replace('data:image/'. $this->type .';base64,', '', $dataUrl);
        $img = str_replace(' ', '+', $img);
        $this->data = base64_decode($img);
    }

    public function type(){
        return $this->type;
    }

    public function data(){
        return $this->data;
    }

    public function createTmp(){
        $name=tempnam(sys_get_temp_dir(),'IMG');
        file_put_contents($name,$this->data());
        $pathinfo = pathinfo($name);
        $new_name = $pathinfo['dirname'].DIRECTORY_SEPARATOR.$pathinfo['filename'] . '.'.$this->type();
        rename($name, $new_name);
        $this->tmp=$new_name;
        return $new_name;
    }
    public function unlinkTmp(){
        unlink($this->tmp);
    }

} 