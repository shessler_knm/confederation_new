<?php defined('SYSPATH') OR die('No direct script access.');

class Kohana_Exception extends Kohana_Kohana_Exception {
    public function __construct($message = "", array $variables = NULL, $code = 0, Exception $previous = NULL)
    {
        parent::__construct();
    }
}