<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Almas
 * Date: 29.07.13
 * Time: 15:01
 * To change this template use File | Settings | File Templates.
 */
class Feed extends Kohana_Feed
{

    public static $users = array();

    static function new_photo($user, $value1)
    {
        $feed = ORM::factory('Feed');
        $feed->user_id = $user->id;
        $feed->date = date('Y-m-d H:i:s');
        $feed->value_1 = $value1;
        $feed->type = "new_photo";
        $feed->value_2 = null;
        $feed->save();
    }

    static function new_fat($user, $value1)
    {
        $feed = ORM::factory('Feed');
        $feed->user_id = $user->id;
        $feed->date = date('Y-m-d H:i:s');
        $feed->value_1 = $value1;
        $feed->type = "new_fat";
        $feed->value_2 = null;
        $feed->save();
    }

    static function new_sub($user, $target_user)
    {
        $feed = ORM::factory('Feed');
        $feed->user_id = $target_user;
        $feed->date = date('Y-m-d H:i:s');
        $feed->type = "new_sub";
        $feed->value_1 = $user;
        $feed->save();
    }

    static function close_mission($user, $all_users, $this_top_user, $type)
    {
        $feed = ORM::factory('Feed');
        $feed->user_id = $user;
        $feed->date = date('Y-m-d H:i:s');
        $feed->type = "close_mission";
        $feed->value_1 = $type;
        $feed->save();

        if (!$this_top_user->loaded()) {
            $this_top_user->user_id = $user;
            $this_top_user->place = 1;
            $this_top_user->save();
            foreach ($all_users as $top_user) {
                $top_user->place += 1;
                if ($top_user->place > 5) {
                    $top_user->delete();
                } else {
                    $top_user->save();
                }
            }
        } else {
            foreach ($all_users as $top_user) {
                if ($top_user->place < $this_top_user->place) {
                    $top_user->place += 1;
                    $top_user->save();
                }
            }
            $this_top_user->place = 1;
            $this_top_user->save();
        }
    }

    static function update_album($user, $album_id)
    {
        $feed = ORM::factory('Feed')->where('value_1', '=', $album_id)->find();
        if (!$feed->loaded()) {
            $feed->user_id = $user;
            $feed->date = date('Y-m-d H:i:s');
            $feed->value_1 = $album_id;
            $feed->type = "update_album";
            $feed->save();
        }
    }

    static function feed_photo($value)
    {
        $photo = ORM::factory('Storage', array('id' => $value));
        return $photo->html_img(null, 200);
    }

    static function feed_user_photo($id)
    {
        $user = Arr::get(static::$users, $id);
        if (!$user) {
            $user = ORM::factory('User', array('id' => $id));
        }
        static::$users[$id] = $user;
        return $user->photo_s->html_img(200, 200);
    }

    static function feed_training_result($user, $id)
    {
        $feed = ORM::factory('Feed');
        $feed->user_id = $user;
        $feed->date = date('Y-m-d H:i:s');
        $feed->type = "training";
        $feed->value_1 = $id;
        $feed->save();
    }

    static function feed_pedometer_result($user, $id)
    {
        $feed = ORM::factory('Feed');
        $feed->user_id = $user;
        $feed->date = date('Y-m-d H:i:s');
        $feed->type = "pedometer";
        $feed->value_1 = $id;
        $feed->save();
        // время, калории, ср, макс скорость и дистанцию.
    }

    static function delete_event($type, $user_id, $value_1)
    {
        $feed = ORM::factory('Feed', array('type' => $type, 'user_id' => $user_id, 'value_1' => $value_1));
        if ($feed->loaded()) {
            $feed->delete();
        }
    }

    static function awards($ex_done, $exercises, $user_id)
    {
        $progress = round(($ex_done / $exercises) * 100);
        $this_top_user = ORM::factory('Top')->reset(true)->where('user_id', '=', $user_id)->find();
        $all_users = ORM::factory('Top')->where('user_id', '!=', $user_id)->find_all();
        $done_programs = ORM::factory('Program')->where('user_id', '=', $user_id)->where('activity', '=', 2)->find_all();
        $all_ex_done = null;
        $all_exercises = null;
        $type = null;
        //Считаем упражнения
        foreach ($done_programs as $program) {
            $all_ex_done += $program->logs->where('status', '=', 1)->find_all()->count();
            $all_exercises += $program->duration * $program->program_exercises->find_all()->count();
        }
        $user = ORM::factory('User', $user_id);
        $user_awards = ORM::factory('User_Award');
        //Страшные условия для определения типа. Этот код уныл чуть более, чем полностью. Сделайте с ним что-нибудь.
        if (($done_programs->count() >= 5 && $all_ex_done > round(0.5 * $all_exercises) && $user->sex == 1 && $user->fat <= 10) || ($done_programs->count() >= 5 && $all_ex_done > round(0.5 * $all_exercises) && $user->sex == 2 && $user->fat <= 15)) {
            $type = 5;
        } elseif (($done_programs->count() >= 2 && $all_ex_done > round(0.5 * $all_exercises) && $user->sex == 1 && $user->fat <= 15) || ($done_programs->count() >= 2 && $all_ex_done > round(0.5 * $all_exercises) && $user->sex == 2 && $user->fat <= 20)) {
            $type = 4;
        } elseif (($done_programs->count() >= 2 && $all_ex_done > round(0.5 * $all_exercises) && $user->sex == 1 && $user->fat <= 25) || ($done_programs->count() >= 2 && $all_ex_done > round(0.5 * $all_exercises) && $user->sex == 2 && $user->fat <= 30)) {
            $type = 3;
        }

        if ($ex_done > round(0.5 * $exercises)) {
            $award = ORM::factory('Award')->where('sex', '=', $user->sex)->where('type', '=', 1)->find();
            $user_awards->award_id = $award->id;
            $user_awards->user_id = $user_id;
            $user_awards->save();
            Feed::close_mission($user_id, $all_users, $this_top_user, 1);
        } elseif ($ex_done > round(0.8 * $exercises)) {
            $award = ORM::factory('Award')->where('sex', '=', $user->sex)->where('type', '=', 2)->find();
            $user_awards->award_id = $award->id;
            $user_awards->user_id = $user_id;
            $user_awards->save();
            Feed::close_mission($user_id, $all_users, $this_top_user, 2);
        } elseif ($type) {
            $award = ORM::factory('Award')->where('sex', '=', $user->sex)->where('type', '=', $type)->find();
            $user_awards->award_id = $award->id;
            $user_awards->user_id = $user_id;
            $user_awards->save();
            Feed::close_mission($user_id, $all_users, $this_top_user, $type);
        }
    }

    static function award_text($user, $value_1, $close_mission_text = null)
    {
        switch (true) {
            case ($value_1 == 1 && $user->sex == 1):
                $close_mission_text = __('Бодибилдер Уровень 1');
                break;
            case ($value_1 == 2 && $user->sex == 1):
                $close_mission_text = __('Бодибилдер Уровень 2');
                break;
            case ($value_1 == 3 && $user->sex == 1):
                $close_mission_text = __('Бодибилдер Уровень 3');
                break;
            case ($value_1 == 4 && $user->sex == 1):
                $close_mission_text = __('Мистер Мира');
                break;
            case ($value_1 == 5 && $user->sex == 1):
                $close_mission_text = __('Мистер Вселенная');
                break;
            case ($value_1 == 1 && $user->sex == 2):
                $close_mission_text = __('Первый уровень стройности');
                break;
            case ($value_1 == 2 && $user->sex == 2):
                $close_mission_text = __('Второй уровень стройности');
                break;
            case ($value_1 == 3 && $user->sex == 2):
                $close_mission_text = __('Третий уровень стройности');
                break;
            case ($value_1 == 4 && $user->sex == 2):
                $close_mission_text = __('Мисс Мира');
                break;
            case ($value_1 == 5 && $user->sex == 2):
                $close_mission_text = __('Мисс Вселенная');
                break;
        }
        return $close_mission_text;
    }

    static function feed_text($user_id, $current_user, $type, $number, $value_1)
    {
        $model = Arr::get(static::$users, $user_id);
        $sub_user = ORM::factory('User', $value_1);
        if (!$model) {
            $model = ORM::factory('User', array('id' => $user_id));
        }
        static::$users[$user_id] = $model;
        if ($type == "close_mission") {
            $close_mission_text = Feed::award_text($model, $value_1);
        }
        switch (true) {
            case ($type == "new_fat" && $user_id == $current_user->id):
                return __('Подкожный жир сокращен на :value %', array(':value' => $value_1));
                break;
            case ($type == "close_mission" && $user_id == $current_user->id):
                return __('Вы получили награду :award', array(':award' => $close_mission_text));
                break;
            case ($type == "close_mission" && $user_id != $current_user->id && $model->sex == 1):
                return __(':username получил награду :award', array(':username' => HTML::anchor(URL::site('privatecoach/profile/' . $current_user->id), $model->get_full_name()), ':award' => $close_mission_text));
                break;
            case ($type == "close_mission" && $user_id != $current_user->id && $model->sex == 2):
                return __(':username получила награду :award', array(':username' => HTML::anchor(URL::site('privatecoach/profile/' . $current_user->id), $model->get_full_name()), ':award' => $close_mission_text));
                break;
            case ($type == "update_album" && $user_id == $current_user->id):
                return __('Вы загрузили :number в альбом :album', array(':album' => HTML::anchor(URL::site('privatecoach/user/album'), __('«До и после»')), ':number' => Helper::word_cases($number, 'фотографий', 'фотографию', 'фотографии')));
                break;
            case ($type == "update_album" && $user_id != $current_user->id && $model->sex == 1):
                return __(':username добавил фотографию в раздел :album', array(':username' => HTML::anchor(URL::site('privatecoach/profile/' . $user_id), $model->get_full_name()), ':album' => /*HTML::anchor(URL::site('privatecoach/profile/album/' . $user_id),*/
                    __('До и после')));
                break;
            case ($type == "update_album" && $user_id != $current_user->id && $model->sex == 2):
                return __(':username добавила фотографию в раздел :album', array(':username' => HTML::anchor(URL::site('privatecoach/profile/' . $user_id), $model->get_full_name()), ':album' => /* HTML::anchor(URL::site('privatecoach/profile/album/' . $user_id),*/
                    __('До и после')));
                break;
            case ($type == "new_sub" && $user_id == $current_user->id && $sub_user->sex == 1):
                return __(':username оформил подписку на Ваши обновления', array(':username' => HTML::anchor(URL::site('privatecoach/profile/' . $sub_user->id), $sub_user->get_full_name())));
                break;
            case ($type == "new_sub" && $user_id == $current_user->id && $sub_user->sex == 2):
                return __(':username оформила подписку на Ваши обновления', array(':username' => HTML::anchor(URL::site('privatecoach/profile/' . $sub_user->id), $sub_user->get_full_name())));
                break;
        }
    }
}